package com.sharethatdata.kobotcontrol_webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.jsonlibrary.JSONParserForPassword;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cActionsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cIpRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cParametersRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cQueuesRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotControl;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cOutputsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotOutput;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotPosition;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotPositions;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotResult;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotResults;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotSetting;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotSettings;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobots;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cSensorsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cStatusRobot;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tudur on 05-Feb-19.
 */

public class WSDataProvider {

    private Context context;
    private SharedPreferences sharedPrefs;

    JSONParser jsonParser = new JSONParser();
    JSONParserForPassword jsonParserForPassword = new JSONParserForPassword();

    private String url_login = "login.php";
    //private String url_secure_login = "secure_login.php";
    private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    private String url_create_item = "create_item.php";
    private String url_update_item = "update_item.php";
    private String url_delete_item = "delete_item.php";
    private String create_script_url = "addScript.php";
    private String request_reset_password = "requestReset.php";
    private String process_reset_password = "processResetRequest.php";

    private static final String TAG_SUCCESS = "ws_success";
    private static final String TAG_ERROR = "ws_error";
    private static final String TAG_MESSAGE = "ws_error_message";
    //private static final String TAG_UPDATE = "update";

    private static final String CONNECTION_ERROR = "Web service not found, please check WIFI/Network connection.";

    //private String webservice_url_robot = "https://www.sharethatdata.com/robot-ws";
    private String webservice_url_robot = "https://robot-ws.sharethatdata.com/";
    //private String webservice_url = "https://www.sharethatdata.com/robot-ws";
    private String webservice_url = "https://robot-ws.sharethatdata.com/";
    //private String webservice_url = "no web service";
    private String myUser = "";
    private String myPass = "";
    private String myApp = "";

    private String ip = "";
    private String android_id = "";
    private String appname = "";

    public String last_error_nr = "0";
    public String last_error = "";
    //public String update = "";
    public boolean json_connected = false;

    public WSDataProvider(Context context)
    {
        this.context = context;
    }

    public WSDataProvider(String new_user, String new_pass)
    {
        myUser = new_user;
        myPass = new_pass;

        String currentSESSION = WSSession.getInstance().getString();
        if(!currentSESSION.equals("")){

            myPass = WSSession.getInstance().getPassword();
            myApp = WSSession.getInstance().getAppName();
            android_id = WSSession.getInstance().getDevice();
            ip = WSSession.getInstance().getIP();
            android_id = WSSession.getInstance().getDevice();
        }
    }

    public void SetWebServiceUrl(String new_url)
    {
        if (new_url != ""){
            webservice_url = new_url;
        }else{
            webservice_url = webservice_url_robot;
        }

        System.out.println("URL: " + webservice_url);
    }

    public String getWebServiceUrl()
    {
        return webservice_url;

    }

    public void SetDeviceIdentification(String new_ip, String new_android_id, String new_appname)
    {
        ip = new_ip;
        appname = new_appname;
        android_id = new_android_id;
    }

    public static String MD5_Hash(String input) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);

            while (md5.length() < 32)
                md5 = "0" + md5;

            return md5;
        } catch (NoSuchAlgorithmException e) {
            Log.e("MD5", e.getLocalizedMessage());
            return null;
        }
    }

    public boolean LogEvent(String event)
    {
        // no more logging from client
        String user = myUser;
        String datetime = "";

        try
        {
            Date d = new Date();
            datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        } catch (Exception e)
        {
        }

        // get values from device

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));

        return true;

    }

    /*public boolean CheckSecurekLogin(String login, String password, String app, String version)
    {
        // clean variables
        last_error = "";
        json_connected = true;
        String myPassword = MD5_Hash(password);

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("version", version));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));

        myApp = app;
        myUser = login;
        myPass = myPassword;

        if(context != null){
            // save user and password for notifications
	        SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
	   	     editor.putString("USER", String.valueOf(myUser));
	   	     editor.putString("PASS", String.valueOf(myPass));
	   	     editor.putString("URL", String.valueOf(webservice_url));
	   	     editor.commit();

            System.out.println("CheckLogin myUser: "+ String.valueOf(myUser));
            System.out.println("CheckLogin myPass: "+ String.valueOf(myPass));
            System.out.println("CheckLogin webservice_url: "+ String.valueOf(webservice_url));
        }else{
            System.out.println("context null CheckLogin ------------------------------------- ");
            System.out.println("CheckLogin myUser: "+ String.valueOf(myUser));
            System.out.println("CheckLogin myPass: "+ String.valueOf(myPass));
            System.out.println("CheckLogin webservice_url: "+ String.valueOf(webservice_url));
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_secure_login, "GET", params);


        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    String token = json.getString("ws_token");
                    WSSession.getInstance().setString(token);
                    update = json.getString(TAG_UPDATE);

                    return true;
                } else {
                    last_error_nr = json.getString(TAG_ERROR);
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();

                last_error = CONNECTION_ERROR;
                json_connected = false;
                return false;
            }
        } else {
            last_error = CONNECTION_ERROR;
            json_connected = false;
            return false;
        }
    }*/

    public boolean CheckLogin(String login, String password, String app)
    {
        // clean variables
        last_error = "";
        json_connected = true;
        String myPassword = MD5_Hash(password);

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));

        myApp = app;
        myUser = login;
        myPass = myPassword;

        if(context != null){
            // save user and password for notifications
            SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
            editor.putString("USER", String.valueOf(myUser));
            editor.putString("PASS", String.valueOf(myPass));
            editor.putString("URL", String.valueOf(webservice_url));
            editor.commit();

            System.out.println("CheckLogin myUser: "+ String.valueOf(myUser));
            System.out.println("CheckLogin myPass: "+ String.valueOf(myPass));
            System.out.println("CheckLogin webservice_url: "+ String.valueOf(webservice_url));
        }else{
            System.out.println("context null CheckLogin ------------------------------------- ");
            System.out.println("CheckLogin myUser: "+ String.valueOf(myUser));
            System.out.println("CheckLogin myPass: "+ String.valueOf(myPass));
            System.out.println("CheckLogin webservice_url: "+ String.valueOf(webservice_url));
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_login, "GET", params);


        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    String token = json.getString("ws_token");
                    WSSession.getInstance().setString(token);

                    return true;
                } else {
                    last_error_nr = json.getString(TAG_ERROR);
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();

                last_error = CONNECTION_ERROR;
                json_connected = false;
                return false;
            }
        } else {
            last_error = CONNECTION_ERROR;
            json_connected = false;
            return false;
        }
    }

    // General Method for calling new web service
    // gets a list of json items in array
    // when something goes wrong, an empty array is returned
    private JSONArray CallWebService(List<NameValuePair> new_params, String url, boolean doPost)
    {
        // clean variables
        last_error = "";
        json_connected = true;

        String currentSESSION = WSSession.getInstance().getString();

        // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
                SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
                String user = prefs.getString("USER", "");
                String pass = prefs.getString("PASS", "");
                System.out.println("user: " + user);
                System.out.println("pass: " + pass);

                myUser = user;
                myPass = pass;
            }
        }


        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
            params.add(p);
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        String post_str = "GET";
        if (doPost)
        {
            post_str = "POST";
        }

        JSONObject json = jsonParser.makeHttpRequest(url, post_str, params);
        JSONArray items = new JSONArray();
        if (json != null)
        {
            try {

                items = json.optJSONArray("items");

                // Check your log cat for JSON reponse
                Log.d("All items: ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    if (items == null){
                        items = new JSONArray();
                    }else{
                        System.out.println("items: " + items);
                    }
                } else {
                    // record error
                    items = new JSONArray();
                    String error = json.getString(TAG_MESSAGE);
                    String error_nr = json.getString(TAG_ERROR);
                    if (error.equals("")){
                        last_error = "unknown error";
                        last_error_nr = "0";
                    } else {
                        last_error = error;
                        last_error_nr = error_nr;
                    }

		        	if(error.contains("Security token incorrect.")){
		        		System.out.println("TOKEN EXPIRED");
		        		System.out.println("TOKEN EXPIRED myUser: " + myUser);
		        		System.out.println("TOKEN EXPIRED myPass: " + myPass);

		        		boolean bool = CheckLogin(myUser, myPass, myApp);

		        		System.out.println("bool: " + bool);

		        		if(bool){
		        			JSONArray items_again = CallWebService(new_params, url, false);
		        			items = items_again;

		        		}
		        	}

                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        } else {

            last_error = CONNECTION_ERROR;
            json_connected = false;

        }
        return items;
    }

    private JSONArray CallWebServiceForPassword(List<NameValuePair> new_params, String url, boolean doPost)
    {
        // clean variables
        last_error = "";
        json_connected = true;

        String currentSESSION = WSSession.getInstance().getString();

        // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
                SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
                String user = prefs.getString("USER", "");
                String pass = prefs.getString("PASS", "");

                myUser = user;
                myPass = pass;
            }
        }


        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
            params.add(p);
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        String post_str = "GET";
        if (doPost)
        {
            post_str = "POST";
        }

        JSONObject json = jsonParserForPassword.makeHttpRequest(url, post_str, params);
        JSONArray items = new JSONArray();
        if (json != null)
        {
            try {

                items = json.optJSONArray("items");

                // Check your log cat for JSON reponse
                Log.d("All items: ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    if (items == null){
                        items = new JSONArray();

                        String error = json.getString(TAG_MESSAGE);
                        items = new JSONArray();
                        items.put(error);

                    }else{
                        System.out.println("items: " + items);
                    }
                } else {
                    // record error
                    items = new JSONArray();
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;

                    if(error.contains("Security token incorrect.")){
                        System.out.println("TOKEN EXPIRED");
                        System.out.println("TOKEN EXPIRED myUser: " + myUser);
                        System.out.println("TOKEN EXPIRED myPass: " + myPass);

                        boolean bool = CheckLogin(myUser, myPass, myApp);

                        System.out.println("bool: " + bool);

                        if(bool){
                            JSONArray items_again = CallWebService(new_params, url, false);
                            items = items_again;

                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        } else {

            last_error = CONNECTION_ERROR;
            json_connected = false;

        }
        return items;
    }



    //////////////////////////////////////////////////////////////////////////////////

    /**
     * Get List
     */

    public List<cRobots> getRobots()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cRobots> returnList = new ArrayList<cRobots>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cRobots robot = new cRobots();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.code = c.getString("code");
                    robot.description = c.getString("description");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }


    public List<cStatusRobot> getStatusRobots()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_status"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cStatusRobot> returnList = new ArrayList<cStatusRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cStatusRobot robot = new cStatusRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.datetime = c.getString("datetime");
                    robot.status_text = c.getString("status_text");
                    robot.orders = c.getString("orders");
                    robot.errors = c.getString("errors");
                    robot.last_error = c.getString("last_error");
                    robot.online = c.getString("online");

                    if(robot.online.equals("0")){
                        // online=0 = offline = gray
                        robot.status = "0";
                    }else {
                        // online=1, use status (green, yellow, red)
                        robot.status = c.getString("status");
                    }

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }


    public List<cSensorsRobot> getSensorsRobots(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "sensor"));
        params.add(new BasicNameValuePair("robot", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cSensorsRobot> returnList = new ArrayList<cSensorsRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cSensorsRobot robot = new cSensorsRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.sensor = c.getString("sensor");
                    robot.value = c.getString("value");
                    robot.datetime = c.getString("datetime");
                    robot.description = c.getString("description");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cSensorsRobot> getSensorsRobotsIP()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "sensor"));
        //params.add(new BasicNameValuePair("robot", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + "sensors", false);

        // result list
        List<cSensorsRobot> returnList = new ArrayList<cSensorsRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cSensorsRobot robot = new cSensorsRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.sensor = c.getString("sensor");
                    robot.value = c.getString("value");
                    robot.datetime = c.getString("datetime");
                    robot.description = c.getString("description");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cActionsRobot> getActionsRobots(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action"));
        params.add(new BasicNameValuePair("robot", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cActionsRobot> returnList = new ArrayList<cActionsRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cActionsRobot robot = new cActionsRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.parameters = c.getString("parameters");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cActionsRobot> getActionsRobotsIP()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "robot_action"));
        //params.add(new BasicNameValuePair("robot", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + "actions", false);

        // result list
        List<cActionsRobot> returnList = new ArrayList<cActionsRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cActionsRobot robot = new cActionsRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.parameters = c.getString("parameters");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cOutputsRobot> getOutputsRobot()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_output"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cOutputsRobot> returnList = new ArrayList<cOutputsRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cOutputsRobot robot = new cOutputsRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.value = c.getString("value");
                    robot.datetime = c.getString("datetime");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cQueuesRobot> getQueuesRobots(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_queue"));
        params.add(new BasicNameValuePair("robot", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cQueuesRobot> returnList = new ArrayList<cQueuesRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cQueuesRobot robot = new cQueuesRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.created = c.getString("created");
                    robot.updated = c.getString("updated");
                    robot.finished = c.getString("finished");
                    robot.status = c.getString("status");
                    robot.result = c.getString("result");
                    robot.error = c.getString("error");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cParametersRobot> getActionParameters(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_parameter"));
        params.add(new BasicNameValuePair("action", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cParametersRobot> returnList = new ArrayList<cParametersRobot>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cParametersRobot robot = new cParametersRobot();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.required = c.getString("required");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cRobotSettings> getRobotSettings(String robotid)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("robot", robotid));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cRobotSettings> returnList = new ArrayList<cRobotSettings>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cRobotSettings robot = new cRobotSettings();

                    robot.id = c.getString("id");
                    robot.robot = c.getString("robot");
                    robot.categorie = c.getString("categorie");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.value = c.getString("value");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cRobotPositions> getRobotPositions()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_position"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cRobotPositions> returnList = new ArrayList<cRobotPositions>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cRobotPositions robot = new cRobotPositions();

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.x = c.getString("x");
                    robot.y = c.getString("y");
                    robot.z = c.getString("z");
                    robot.rx = c.getString("rx");
                    robot.ry = c.getString("ry");
                    robot.rz = c.getString("rz");
                    robot.r = c.getString("r");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cRobotResults> getRobotResults(String action)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_result"));
        params.add(new BasicNameValuePair("action", action));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cRobotResults> returnList = new ArrayList<cRobotResults>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cRobotResults robot = new cRobotResults();

                    robot.id = c.getString("id");
                    robot.action = c.getString("action");
                    robot.datetime = c.getString("datetime");
                    robot.result = c.getString("result");
                    robot.probability = c.getString("probability");

                    returnList.add(robot);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    /**
     * Get Item
     */

    public cIpRobot getIpRobot(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_control"));
        params.add(new BasicNameValuePair("id", id));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cIpRobot robot = new cIpRobot();

        try {
            if (items.length() > 0)
            {

                JSONObject c = items.getJSONObject(0);

                robot.id = c.getString("id");
                robot.reset = c.getString("reset");
                robot.start = c.getString("start");
                robot.stop = c.getString("stop");
                robot.monitor = c.getString("monitor");
                robot.local_ip = c.getString("local_ip");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobots getDetailRobots(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot"));
        params.add(new BasicNameValuePair("id", id));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobots robot = new cRobots();

        try {
            if (items.length() > 0)
            {

                JSONObject c = items.getJSONObject(0);

                robot.id = c.getString("id");
                robot.name = c.getString("name");
                robot.code = c.getString("code");
                robot.description = c.getString("description");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobots getDetailRobotsIP()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "robot"));
        //params.add(new BasicNameValuePair("id", id));

        JSONArray items = CallWebService(params, webservice_url + "/" + "robot", false);

        cRobots robot = new cRobots();

        try {
            if (items.length() > 0)
            {

                JSONObject c = items.getJSONObject(0);

                robot.id = c.getString("id");
                robot.name = c.getString("name");
                robot.code = c.getString("code");
                robot.description = c.getString("description");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }


    public cRobot getStatusRobot(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_status"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobot robot = new cRobot();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.datetime = c.getString("datetime");
                    robot.status = c.getString("status");
                    robot.status_text = c.getString("status_text");
                    robot.orders = c.getString("orders");
                    robot.errors = c.getString("errors");
                    robot.last_error = c.getString("last_error");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobot getStatusRobotIP()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "robot_status"));
        //params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + "status", false);

        cRobot robot = new cRobot();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.datetime = c.getString("datetime");
                    robot.status = c.getString("status");
                    robot.status_text = c.getString("status_text");
                    robot.orders = c.getString("orders");
                    robot.errors = c.getString("errors");
                    robot.last_error = c.getString("last_error");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobotControl getControlRobot(String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_control"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobotControl robot = new cRobotControl();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.reset = c.getString("reset");
                    robot.start = c.getString("start");
                    robot.stop = c.getString("stop");
                    robot.monitor = c.getString("monitor");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobotControl getControlRobotIP()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "robot_control"));
        //params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + "robot_control", false);

        cRobotControl robot = new cRobotControl();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.reset = c.getString("reset");
                    robot.start = c.getString("start");
                    robot.stop = c.getString("stop");
                    robot.monitor = c.getString("monitor");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }


    public cRobotSetting getRobotSetting (String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobotSetting robot = new cRobotSetting();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.robot = c.getString("robot");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.value = c.getString("value");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobotPosition getRobotPosition (String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_position"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobotPosition robot = new cRobotPosition();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.x = c.getString("x");
                    robot.y = c.getString("y");
                    robot.z = c.getString("z");
                    robot.rx = c.getString("rx");
                    robot.ry = c.getString("ry");
                    robot.rz = c.getString("rz");
                    robot.r = c.getString("r");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobotOutput getRobotOutput (String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_output"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobotOutput robot = new cRobotOutput();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.datetime = c.getString("datetime");
                    robot.name = c.getString("name");
                    robot.description = c.getString("description");
                    robot.value = c.getString("value");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public cRobotResult getRobotResult (String id)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_result"));
        params.add(new BasicNameValuePair("id", id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cRobotResult robot = new cRobotResult();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    robot.id = c.getString("id");
                    robot.action = c.getString("action");
                    robot.datetime = c.getString("datetime");
                    robot.result = c.getString("result");
                    robot.probability = c.getString("probability");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return robot;
    }

    public String getImage(String id)
    {
        String result = "";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_image"));
        if(!id.equals("")) params.add(new BasicNameValuePair("robot", id));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        try
        {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);
                result = c.getString("image");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return result;
    }


    public String getImageIP()
    {
        String result = "";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "robot_image"));
        //if(!id.equals("")) params.add(new BasicNameValuePair("robot", id));

        JSONArray items = CallWebService(params, webservice_url + "/" + "image", false);

        try
        {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);
                result = c.getString("image");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return result;
    }



    /**
     * Insert
     */

    public boolean insertActionQueue(String idrobot, String idaction, String value)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_queue"));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("action", idaction));

        if(!value.equals("")) params.add(new BasicNameValuePair("status", value));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean insertParameterActionQueue(String idrobot, String idaction, String parameter, String value)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_action_queue_parameter"));
        //params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("action", idaction));
        params.add(new BasicNameValuePair("parameter", parameter));
        if(!value.equals("")) params.add(new BasicNameValuePair("status", value));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean createRobotSetting(String idrobot, String name, String value, String description)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("value", value));
        params.add(new BasicNameValuePair("description", description));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean createRobotPosition(String idrobot, String name, String description, String x, String y,
                                       String z, String rx, String ry, String rz, String r)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_position"));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("x", x));
        params.add(new BasicNameValuePair("y", y));
        params.add(new BasicNameValuePair("z", z));
        params.add(new BasicNameValuePair("rx", rx));
        params.add(new BasicNameValuePair("ry", ry));
        params.add(new BasicNameValuePair("rz", rz));
        params.add(new BasicNameValuePair("r", r));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean createRobotOutput(String idrobot, String name, String value, String description)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_output"));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("value", value));
        params.add(new BasicNameValuePair("description", description));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean createRobotResult(String idrobot, String name, String value, String description)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("value", value));
        params.add(new BasicNameValuePair("description", description));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    /**
     * Update
     */

    public boolean updateRobotSetting(String idsetting, String idrobot, String name, String value, String description)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("id", idsetting));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("value", value));
        params.add(new BasicNameValuePair("description", description));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean updateRobotPosition(String idsetting, String idrobot, String name, String description,
                                      String x, String y, String z, String rx, String ry, String rz, String r)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_position"));
        params.add(new BasicNameValuePair("id", idsetting));
        params.add(new BasicNameValuePair("robot", idrobot));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("x", x));
        params.add(new BasicNameValuePair("y", y));
        params.add(new BasicNameValuePair("z", z));
        params.add(new BasicNameValuePair("rx", rx));
        params.add(new BasicNameValuePair("ry", ry));
        params.add(new BasicNameValuePair("rz", rz));
        params.add(new BasicNameValuePair("r", r));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean updateRobotOutput(String idoutput, String name, String value, String description)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("id", idoutput));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("value", value));
        params.add(new BasicNameValuePair("description", description));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean updateRobotControl(String idrobot, String reset, String start, String stop, String monitor)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_control"));
        params.add(new BasicNameValuePair("id", idrobot));

        if(!reset.equals("")) params.add(new BasicNameValuePair("reset", reset));
        if(!start.equals("")) params.add(new BasicNameValuePair("start", start));
        if(!stop.equals("")) params.add(new BasicNameValuePair("stop", stop));
        if(!monitor.equals("")) params.add(new BasicNameValuePair("monitor", monitor));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean updateRobotControlIP(String action)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        /*params.add(new BasicNameValuePair("object", "robot_control"));
        params.add(new BasicNameValuePair("id", idrobot));

        if(!reset.equals("")) params.add(new BasicNameValuePair("reset", reset));
        if(!start.equals("")) params.add(new BasicNameValuePair("start", start));
        if(!stop.equals("")) params.add(new BasicNameValuePair("stop", stop));
        if(!monitor.equals("")) params.add(new BasicNameValuePair("monitor", monitor));*/

        JSONArray items = CallWebService(params, webservice_url + "/" + action, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    /**
     * Delete
     */

    public boolean deleteRobotOutput(String idoutput)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_output"));
        params.add(new BasicNameValuePair("id", idoutput));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean deleteRobotPosition(String idposition)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_position"));
        params.add(new BasicNameValuePair("id", idposition));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

    public boolean deleteRobotSetting(String idsetting)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "robot_setting"));
        params.add(new BasicNameValuePair("id", idsetting));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

}
