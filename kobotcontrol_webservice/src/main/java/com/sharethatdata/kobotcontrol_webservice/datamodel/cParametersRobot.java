package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 21-Mar-19.
 */

public class cParametersRobot {

    public String id;
    public String name;
    public String description;
    public String required;

    public cParametersRobot(){
        id = "";
        name = "";
        description = "";
        required = "";
    }
}
