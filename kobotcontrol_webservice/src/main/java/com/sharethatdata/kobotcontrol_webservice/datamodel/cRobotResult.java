package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 03-Aug-19.
 */

public class cRobotResult {

    public String id;
    public String action;
    public String datetime;
    public String result;
    public String probability;


    public cRobotResult(){

        id = "";
        action = "";
        datetime = "";
        result = "";
        probability = "";
    }

}
