package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 15-May-19.
 */

public class cIpRobot {

    public String id;
    public String reset;
    public String start;
    public String stop;
    public String monitor;
    public String local_ip;

    public cIpRobot(){
        id = "";
        reset = "";
        start = "";
        stop = "";
        monitor = "";
        local_ip = "";
    }
}
