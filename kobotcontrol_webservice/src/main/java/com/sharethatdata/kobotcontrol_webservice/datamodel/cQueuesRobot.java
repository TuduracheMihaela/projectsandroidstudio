package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 17-Mar-19.
 */

public class cQueuesRobot {

    public String id;
    public String name;
    public String description;
    public String created;
    public String updated;
    public String finished;
    public String status;
    public String result;
    public String error;

    public cQueuesRobot(){
        id = "";
        name = "";
        description = "";
        created = "";
        updated = "";
        finished = "";
        status = "";
        result = "";
        error = "";
    }
}
