package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 08-Feb-19.
 */

public class cRobot {

    public String id;
    public String name;
    public String datetime;
    public String status;
    public String status_text;
    public String orders;
    public String errors;
    public String last_error;

    public cRobot() {
        id = "";
        name = "";
        datetime = "";
        status = "";
        status_text = "";
        orders = "";
        errors = "";
        last_error = "";
    }

}
