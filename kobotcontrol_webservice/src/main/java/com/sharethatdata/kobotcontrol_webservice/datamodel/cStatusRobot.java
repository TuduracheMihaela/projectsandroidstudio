package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 20-Mar-19.
 */

public class cStatusRobot {

    public String id;
    public String name;
    public String description;
    public String datetime;
    public String status;
    public String status_text;
    public String orders;
    public String errors;
    public String last_error;
    public String online;

    public cStatusRobot(){
        id = "";
        name = "";
        description = "";
        datetime = "";
        status = "";
        status_text = "";
        orders = "";
        errors = "";
        last_error = "";
        online = "";
    }
}
