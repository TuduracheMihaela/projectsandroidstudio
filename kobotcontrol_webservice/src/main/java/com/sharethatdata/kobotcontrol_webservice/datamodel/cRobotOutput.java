package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 07-Aug-19.
 */

public class cRobotOutput {

    public String id;
    public String datetime;
    public String name;
    public String description;
    public String value;


    public cRobotOutput(){

        id = "";
        datetime = "";
        name = "";
        description = "";
        value = "";
    }

}
