package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 06-Mar-19.
 */

public class cRobotControl {

    public String id;
    public String reset;
    public String start;
    public String stop;
    public String monitor;

    public cRobotControl(){
        id = "";
        reset = "";
        start = "";
        stop = "";
        monitor = "";
    }
}
