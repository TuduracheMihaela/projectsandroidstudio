package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 08-Feb-19.
 */

public class cActionsRobot {

    public String id;
    public String name;
    public String description;
    public String parameters;

    public cActionsRobot() {
        id = "";
        name = "";
        description = "";
        parameters = "";
    }

    public cActionsRobot(String id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }
}
