package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 05-Feb-19.
 */

public class cRobots {

    public String id;
    public String name;
    public String code;
    public String description;

    public String imageid;

    public cRobots() {
        id = "";
        name = "";
        code = "";
        description = "";

        imageid = "0";
    }
}
