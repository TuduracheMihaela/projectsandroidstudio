package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 31-Jul-19.
 */

public class cRobotSettings {

    public String id;
    public String robot;
    public String categorie;
    public String name;
    public String description;
    public String value;


    public cRobotSettings(){

        id = "";
        robot = "";
        categorie = "";
        name = "";
        description = "";
        value = "";
    }


}
