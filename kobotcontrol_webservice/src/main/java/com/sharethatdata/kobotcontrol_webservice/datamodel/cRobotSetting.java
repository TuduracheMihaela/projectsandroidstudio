package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 03-Aug-19.
 */

public class cRobotSetting {

    public String id;
    public String robot;
    public String categorie;
    public String name;
    public String description;
    public String value;


    public cRobotSetting(){

        id = "";
        robot = "";
        categorie = "";
        name = "";
        description = "";
        value = "";
    }

}
