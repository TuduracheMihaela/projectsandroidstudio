package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 03-Aug-19.
 */

public class cRobotPositions {

    public String id;
    public String name;
    public String description;
    public String x;
    public String y;
    public String z;
    public String rx;
    public String ry;
    public String rz;
    public String r;


    public cRobotPositions(){

        id = "";
        name = "";
        description = "";
        x = "";
        y = "";
        z = "";
        rx = "";
        ry = "";
        rz = "";
        r = "";
    }
}
