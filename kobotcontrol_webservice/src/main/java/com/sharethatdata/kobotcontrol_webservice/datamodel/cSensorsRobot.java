package com.sharethatdata.kobotcontrol_webservice.datamodel;

/**
 * Created by tudur on 08-Feb-19.
 */

public class cSensorsRobot {

    public String id;
    public String name;
    public String datetime;
    public String sensor;
    public String value;
    public String description;

    public cSensorsRobot() {
        id = "";
        name = "";
        datetime = "";
        sensor = "";
        value = "";
        description = "";
    }

    public cSensorsRobot(String id, String name, String value, String description){
        this.id = id;
        this.name = name;
        this.value = value;
        this.description = description;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setValue(String value){
        this.value = value;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getValue(){
        return value;
    }

    public String getDescription(){
        return description;
    }
}
