package com.sharethatdata.sharedactivity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	public int getFormattedMilisecondsToSeconds(long intNum)
	{
		int fseconds = (int) ((intNum / 1000) );

       	return fseconds;
	}

	public String getFormattedHourMinutes(int intNum)
	{
		String retString = "";

		int fminute = (intNum/60)%60;
		int fhour = (intNum/3600);

		if(fhour >= 1){
			retString = ( fhour + " h " + fminute + " min " );
		}else{
			retString = ( fminute + " min " );
		}

       	return retString;
	}

	public String getFormattedKilometers(int intNum)
	{
		String retString = "";

		float fkilometer = (intNum / 1000);
		float fmeter = (((float) intNum / 1000) - fkilometer) * 1000;

		if (fmeter > 0)
			retString = String.format("%.0f", fkilometer) + " km " + String.format("%.0f", fmeter) + " m";
		else
			retString = String.format("%.0f", fkilometer) + " km ";

		return retString;
	}

	public long getDifferenceStoppedTime(String startTime, String endTime){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		Date startDate = null;
		Date endDate = null;
		try {
			//startDate = simpleDateFormat.parse("08:00");
			startDate = simpleDateFormat.parse(startTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		try {
			//endDate = simpleDateFormat.parse("10:00");
			endDate = simpleDateFormat.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		long difference;
		if(startTime.equals("00:00:00")){
			difference = 0;
		}else{
			difference = startDate.getTime() - endDate.getTime();
		}


		return difference;
	}

	public String getTotalTimeStoppedInMiliseconds(long difference){

		int days = (int) (difference / (1000*60*60*24));
		int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
		int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

		String stopped_time = "";
		if(hours > 0){
			if(min > 0){
				stopped_time = String.valueOf(hours) + " h " + String.valueOf(min) + " min ";
			}else{
				stopped_time = String.valueOf(hours) + " h ";
			}
		}else{
			stopped_time = String.valueOf(min) + " min ";
		}

		return stopped_time;
	}

	public double tabletSize(Context context) {
		double size = 0;
		try {
			// Compute screen size
			DisplayMetrics dm = context.getResources().getDisplayMetrics();
			float screenWidth  = dm.widthPixels / dm.xdpi;
			float screenHeight = dm.heightPixels / dm.ydpi;
			size = Math.sqrt(Math.pow(screenWidth, 2) +
					Math.pow(screenHeight, 2));
		} catch(Throwable t) {

		}
		return size;
	}

	public void setOrientation(Context context, Activity activity){
		double size_tablet = tabletSize(context);
		if (size_tablet > 9) {
			//Device is a 10" tablet
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		}else if (size_tablet > 6) {
			//Device is a 7" tablet
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		}else{
			// Device is a phone
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
	}

}
