package com.sharethatdata.sharedactivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.TimeUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cLocationCurrent;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


/**
 * Created by mihu__000 on 5/25/2017.
 */

public class WhereIsActivity extends AppCompatActivity {

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    // Progress Dialog
    private ProgressDialog pDialog;

    String myImageData = "";

    private String report_user = "";
    private String report_user_id = "";

    private String myUser = "";
    private String myPass = "";
    private String myUserID = "";
    private String myUserName = "";
    private String ws_url = "";

    cLocationCurrent myLocation;

    public static final String inputFormat = "HH:mm";
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);

    ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();

    private boolean userIsInteracting = false;

    TextView textViewDate, textViewTime, textViewLocation, textViewProbability;
    Spinner userSpinner = null;

    private int mStartHour,mStartMinute;
    private int mStartYear,mStartMonth,mStartDay;

    private int spinnerPositionAfterRotateScreen = 0;
    boolean spinnerPositionRotateScreen = false;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where_is);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent myIntent = getIntent();
        myUser = myIntent.getStringExtra("myUser");
        myPass = myIntent.getStringExtra("myPass");
        myUserID = myIntent.getStringExtra("myUserID");
        myUserName = myIntent.getStringExtra("myUserName");
        ws_url = myIntent.getStringExtra("ws_url");

        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        textViewProbability = (TextView) findViewById(R.id.textViewProbability);
        //Button buttonClose = (Button) findViewById(R.id.buttonClose);
        userSpinner = (Spinner) findViewById(R.id.spinnerUser);
        myLocation = new cLocationCurrent();

        MyProvider = new WSDataProvider(myUser, myPass);

        // set calendar
        Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);

        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
        String weekdayname1 = weekdays[c.get(Calendar.DAY_OF_WEEK)];

        textViewDate.setText(new StringBuilder().append(weekdayname1).append(" ").append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
        //textViewTime.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
        TextView startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(weekdayname1).append(" ").append(pad(mStartDay)).append("-")
                .append(pad(mStartMonth + 1)).append("-")
                .append(pad(mStartYear)));

        /*buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (userIsInteracting) {

                    int myIndex = userSpinner.getSelectedItemPosition();
                    report_user = userkeyArray.get(myIndex);
                    report_user_id = userkeyArray.get(myIndex);

                    new LoadLocation().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        new LoadUserList().execute();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(!report_user_id.equals("")){
            savedInstanceState.putString("report_user_id", report_user_id);
        }else{
            savedInstanceState.putString("report_user_id", myUserID);
        }

        savedInstanceState.putString("location_name", myLocation.location_name);
        savedInstanceState.putString("probabillity", myLocation.probabillity);
        savedInstanceState.putString("myImageData", myImageData);
        savedInstanceState.putInt("userSpinner", userSpinner.getSelectedItemPosition());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        report_user_id = savedInstanceState.getString("report_user_id");
        myLocation.location_name = savedInstanceState.getString("location_name");
        myLocation.probabillity = savedInstanceState.getString("probabillity");
        myImageData = savedInstanceState.getString("myImageData");
        spinnerPositionAfterRotateScreen = savedInstanceState.getInt("userSpinner");
        spinnerPositionRotateScreen = true;
        //userSpinner.setSelection(savedInstanceState.getInt("userSpinner", 0));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mStartYear = year;
            mStartMonth = month;
            mStartDay = day;

            String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();

            TextView startDateView = (TextView) findViewById(R.id.dateStartText);

            System.out.println("DatePickerDialog before: " + mStartYear + " " + mStartMonth + " " + mStartDay);

            startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("-")
                    .append(pad(mStartMonth + 1)).append("-")
                    .append(pad(mStartYear)));
            textViewDate.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));

            System.out.println("DatePickerDialog after: " + mStartYear + " " + mStartMonth + " " + mStartDay);

            new LoadLocation().execute();
        }
    };

    private TimePickerDialog.OnTimeSetListener mStartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mStartHour = hourOfDay;
            mStartMinute = minute;

            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
                    .append(pad(mStartMinute)));

            new LoadLocation().execute();
        }
    };

    public void startDateClick(View v)
    {
        DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);

        System.out.println("startDateClick: " + mStartYear + " " + mStartMonth + " " + mStartDay);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dp1.setTitle(sdf.format(d));
        dp1.show();
    }

    public void startTimeClick(View v)
    {
//	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
//	    tp1.show();

        TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
        String startTime = startTimeView.getText().toString();
        String[] tokens = startTime.split(":");
        mStartHour = Integer.valueOf(tokens[0]);
        mStartMinute = Integer.valueOf(tokens[1]);

        //	CustomTimePickerDialog stp1 = new CustomTimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
        TimePickerDialog stp1 = new TimePickerDialog(this, mStartTimeSetListener, mStartHour, mStartMinute, true);
        stp1.show();
    }

    private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    /**
     * Background Async Task to Load all users by making HTTP Request
     * */
    class LoadUserList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));

        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(ws_url);

            if (report_user == "")
            {
                userList = new ArrayList<HashMap<String, String>>();

                // get Users List for spinner //
                //List<cContact> List = MyProvider.getMyUsers();
                List<cContact> List = MyProvider.getAllUsers();

                for(cContact entry : List)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("pid", entry.id);
                    map.put("name", entry.name);

                    // adding HashList to ArrayList
                    userList.add(map);
                }

                ///////////////////////////////////////////////////////////
                // get Image User //
                if(!spinnerPositionRotateScreen){
                    final cContact contact = MyProvider.getContact(myUserID);

                    if (contact != null)
                    {
                        if (contact.image_id != "0")
                        {
                            myImageData = MyProvider.getImage(contact.image_id);
                        }
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ImageView imageView1 = (ImageView) findViewById(R.id.imageViewUser);

                        Bitmap d = ConvertByteArrayToBitmap(myImageData);
                        if (d != null)
                        {
                            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                            imageView1.setImageBitmap(scaled);
                        }
                    }
                });

                //////////////////////////////////////////////////////////////////////////
                // get User Location //
                String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
                String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
                if(!spinnerPositionRotateScreen){
                    myLocation = MyProvider.getUserCurrentLocation(myUserID, myDateTime + " " + myStartTime);
                }

                report_user = myUser;
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> userArray =  new ArrayList<String>();
                    for (HashMap<String, String> map : userList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") userArray.add(entry.getValue());
                        }

                    ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(WhereIsActivity.this, android.R.layout.simple_spinner_item, userArray);
                    user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    userSpinner.setAdapter(user_adapter);

                    int spinnerPosition = user_adapter.getPosition(myUserName);
                    //set the default according to value
                    if(spinnerPositionRotateScreen){
                        userSpinner.setSelection(spinnerPositionAfterRotateScreen);
                    }else{
                        userSpinner.setSelection(spinnerPosition);
                    }

                    textViewLocation.setText(myLocation.location_name);
                    if(myLocation.probabillity.equals("100.0000")){
                        textViewProbability.setText(myLocation.probabillity.substring(0,3)+"%");
                    }else{
                        if(myLocation.probabillity.equals("")){
                            textViewProbability.setText(myLocation.probabillity.substring(0,0)+"%");
                        }else {
                            textViewProbability.setText(myLocation.probabillity.substring(0,4)+"%");
                        }

                    }

                    userIsInteracting = false;

                }
            });

            hideProgressDialog();

        }
    }

    private class LoadLocation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));

        }

        protected String doInBackground(String... urls) {

            // get User Location //
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(ws_url);

            String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
            String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();

            if(report_user_id.equals("")){
                myLocation = MyProvider.getUserCurrentLocation(myUserID, myDateTime + " " + myStartTime);
            }else{
                myLocation = MyProvider.getUserCurrentLocation(report_user_id, myDateTime + " " + myStartTime);
            }

            //////////////////////////////////////////////////////////////////////
            // get Image User //
            MyProvider.SetWebServiceUrl(ws_url);

            final cContact contact = MyProvider.getContact(report_user_id);

            if (contact != null)
            {
                if (contact.image_id != "0")
                {
                    myImageData = MyProvider.getImage(contact.image_id);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ImageView imageView1 = (ImageView) findViewById(R.id.imageViewUser);

                    Bitmap d = ConvertByteArrayToBitmap(myImageData);
                    if (d != null)
                    {
                        int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                        Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                        imageView1.setImageBitmap(scaled);
                    }
                }
            });

            return "";
        }

        protected void onPostExecute(String result) {

            if (myLocation != null)
            {
                textViewLocation.setText(myLocation.location_name);
                if(myLocation.probabillity.equals("100.0000")){
                    textViewProbability.setText(myLocation.probabillity.substring(0,3)+"%");
                }else{
                    if(!myLocation.probabillity.equals("")){
                        textViewProbability.setText(myLocation.probabillity.substring(0,4)+"%");
                    } else{
                        myLocation.probabillity = "0";
                        textViewProbability.setText(myLocation.probabillity.toString()+"%");
                    }

                }
                userIsInteracting = false;
            }

            hideProgressDialog();
        }
    }

    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }
}
