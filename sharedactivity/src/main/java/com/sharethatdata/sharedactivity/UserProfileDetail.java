package com.sharethatdata.sharedactivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class UserProfileDetail extends AppCompatActivity {
	
	WSDataProvider MyProvider = null;

	private String myUser = "";
	private String myPass = "";
	private String myUserID = "";
	private String myUserName = "";
	private String ws_url = "";
	
	ArrayList<HashMap<String, String>> userList;
	
	private static ProgressDialog pDialog;
	
	String myImageData = "";
	String userId = "";
	String profileId = "";
	//String statusId = "";
	String statusName = "";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_user);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		Intent myIntent = getIntent();
		myUser = myIntent.getStringExtra("myUser");
		myPass = myIntent.getStringExtra("myPass");
		myUserID = myIntent.getStringExtra("myUserID");
		myUserName = myIntent.getStringExtra("myUserName");
		ws_url = myIntent.getStringExtra("ws_url");
		
		// Initializing
        MyProvider = new WSDataProvider(myUser, myPass);
		
		//getActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState == null) {
		    Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	userId= null;
		    } else {
		    	userId= extras.getString("myUserID");
		    }
		} else {
			userId= (String) savedInstanceState.getSerializable("idUser");
		}
		
		SharedPreferences prefContactID = getSharedPreferences("PREF_ID_CONTACT", 0); 
        String id_user_logged = prefContactID.getString("idContact", ""); // ID of the user logged
        //String isMan = Globals.getValue("manager");
        
        // owner user selected from users list
        /*if(userId.equals(id_user_logged) || isMan == "yes"){
        	Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.VISIBLE);
        }else{
        	Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.INVISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);
        }
		
		if (isMan == "yes")
		{	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.VISIBLE);	
		} else {	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);	
		}
		
		Button btnDelete = (Button) findViewById(R.id.btnDelete);*/
  	    
	    // delete button listener
	    /*btnDelete.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {
		
				  new AlertDialog.Builder(UserProfileDetail.this)
				  .setTitle("Delete employee")
				  .setMessage("Do you really want to delete this employee?")
				  .setIcon(android.R.drawable.ic_dialog_alert)
				  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	
				      public void onClick(DialogInterface dialog, int whichButton) {

				          new DeleteUserTask(userId).execute();
				          
				      }})
				   .setNegativeButton(android.R.string.no, null).show();
			  
		  }
		 
		});
	    
	    Button btnEdit = (Button) findViewById(R.id.btnEdit);
  	    
	    // edit button listener
	    btnEdit.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {

				  Intent intent = new Intent(v.getContext(), UserProfileDetailEditActivity.class); 
				  intent.putExtra("idUser", userId);
				  intent.putExtra("idProfile", profileId);
				  startActivityForResult(intent, 2); 
		  }
		 
		});*/
		
		new LoadUser().execute();
        
        
	}
	
	// save values if phone rotate
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	  savedInstanceState.putString("idUser", userId);    
	  super.onSaveInstanceState(savedInstanceState);  
	}  
 
	// load values after phone rotate
	@Override  
	public void onRestoreInstanceState(Bundle savedInstanceState) {  
	  super.onRestoreInstanceState(savedInstanceState);  
	  userId = savedInstanceState.getString("idUser");  
	}
	
	// load data again, just if turn back from edit. Is like this,because, onResume don't stop  ProgressDialog, it keeps running
	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {     
	  super.onActivityResult(requestCode, resultCode, data); 
		  if (resultCode == Activity.RESULT_OK) { 
	    	  new LoadUser().execute();
		  }
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   
			   finish();
			   
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	
	// when get and show the image
		private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
	
	/**
	 * Background Async Task to Load User by making HTTP Request
  	 * */
	class LoadUser extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(UserProfileDetail.this);
         pDialog.setTitle("");
         pDialog.setMessage("");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.setCanceledOnTouchOutside(false);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
		    MyProvider.SetWebServiceUrl(ws_url);
			
	    	final cContact contact = MyProvider.getContact(userId);
	    	
	    	if (contact != null)
			  {
				  if (contact.image_id != "0")
				  {
					  myImageData = MyProvider.getImage(contact.image_id);
				  }
			  }
	    	
	    	runOnUiThread(new Runnable() {
	               @Override
	               public void run() {
	            	   
	            	  profileId = contact.id;
	            	  TextView nameView = (TextView) findViewById(R.id.fullname);
	            	  nameView.setText(contact.surname + " " + contact.lastname);
	            	  TextView functionView = (TextView) findViewById(R.id.functionText);
	 				  functionView.setText(contact.function);
	 				  TextView emailView = (TextView) findViewById(R.id.emailText);
	 				  emailView.setText(contact.email);
	 				  TextView phoneView = (TextView) findViewById(R.id.phoneText);
	 				  phoneView.setText(contact.phone);
	 				  TextView statusView = (TextView) findViewById(R.id.statusText);
	 			      statusView.setText(String.valueOf(contact.status_name));
	 				 // statusId = contact.status_id;
	 				  
	 				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
	 				  descriptionView.setText(String.valueOf(contact.description));
    
	   		    		
	   		    		ImageView imageView1 = (ImageView) findViewById(R.id.imageViewPic);
	  				  
		  				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
		  				  if (d != null)
		  				  {
		  					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
		  					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
		  					  imageView1.setImageBitmap(scaled);
		  					  				  
		  				  }
	               }
	            });
	    	 
	    	
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);
	    	
	    	if (result != null)
			  {
	    		//new LoadUserStatusTask(statusId).execute();
			  }

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        
	  	      
	    }
	  

	}
	
	
/*	private class LoadUserStatusTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public LoadUserStatusTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  
			  ArrayList<HashMap<String, String>> statusList = new ArrayList<HashMap<String, String>>();

		    		// convert objects to hashmap for list
		    		List<cUserStatus> List = MyProvider.getUserStatus();
		    	
		    		for(cUserStatus entry : List)
	             	{
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		
		                map.put("pid", entry.id);
		                map.put("name", entry.name);
		                
		                if(myID.contains(entry.id)){
		                	statusName = entry.name;
		                }
		                 
		                // adding HashList to ArrayList
		                statusList.add(map);
		             }
			
			  return statusName;
		  }

		  protected void onPostExecute(String result) { 
			  if(result != null){
				  TextView statusView = (TextView) findViewById(R.id.StatusText);
		          statusView.setText(String.valueOf(statusName));
			  }
			  
		  }
	}*/
	
	private class DeleteUserTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public DeleteUserTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  			  
			  boolean suc = MyProvider.deleteContact(myID);
			  
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  setResult(RESULT_OK);
			  finish();
			  
		  }
	}

}
