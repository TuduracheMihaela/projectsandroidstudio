package com.sharethatdata.sharedactivity.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sharethatdata.sharedactivity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihu__000 on 5/12/2017.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {
    // Initialise custom font, for example:
    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    private List<String> adapter=new ArrayList<String>();
    private LayoutInflater mInflater;


    public SpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.adapter=items;
        mInflater = LayoutInflater.from(context);
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String adapter_current;
        final ListContent holder;
        View v = convertView;

        if (v == null) {
            v = mInflater.inflate(R.layout.spinner_adapter, null);
            holder = new ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {

            holder = (ListContent) v.getTag();
        }

        adapter_current=new String();
        adapter_current=adapter.get(position);

        holder.name.setText(adapter_current);
        holder.name.setTextColor(getContext().getResources().getColor(R.color.grey07));
        holder.name.setTypeface(font1);

        return v;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        String adapter_current;
        final ListContent holder;
        View v = convertView;

        if (v == null) {
            v = mInflater.inflate(R.layout.spinner_adapter_dropdown, null);
            holder = new ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {

            holder = (ListContent) v.getTag();
        }

        adapter_current=new String();
        adapter_current=adapter.get(position);

        holder.name.setText(adapter_current);
        holder.name.setTextColor(getContext().getResources().getColor(R.color.grey07));
        holder.name.setTypeface(font1);
        return v;
    }

    static class ListContent {
        TextView name;
    }
}
