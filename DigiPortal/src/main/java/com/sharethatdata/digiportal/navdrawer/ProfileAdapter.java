package com.sharethatdata.digiportal.navdrawer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.MyGlobals;
import com.sharethatdata.digiportal.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cUserStatus;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class ProfileAdapter extends ArrayAdapter<ProfileItem>{
	
	final int image_user=R.drawable.user;
	 
    public ProfileAdapter(Context context, int layoutResourceID,
                int textViewResourceId, List<ProfileItem> spinnerDataList) {
          super(context, layoutResourceID, textViewResourceId, spinnerDataList);

          this.context=context;
          this.layoutResID=layoutResourceID;
          this.spinnerData=spinnerDataList;

    }

    Context context;
    int layoutResID;
    List<ProfileItem> spinnerData;
    
    private Activity activity;
    public MyImageLoader loadMyImg;

    public ProfileAdapter(Context context, int layoutResourceID,
                List<ProfileItem> spinnerDataList) {
          super(context, layoutResourceID, spinnerDataList);

          this.context=context;
          this.layoutResID=layoutResourceID;
          this.spinnerData=spinnerDataList;
          
          loadMyImg = new MyImageLoader(context.getApplicationContext());

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

    	SpinnerHolder holder;
    	View row = convertView;
          if(row == null)
          {
                LayoutInflater inflater=((Activity)context).getLayoutInflater();

                row=inflater.inflate(layoutResID, parent, false);
                holder=new SpinnerHolder();

                holder.userImage=(ImageView)row.findViewById(R.id.left_pic);
                holder.name=(TextView)row.findViewById(R.id.text_main_name);
                holder.location=(TextView)row.findViewById(R.id.sub_text_location);
                //holder.email=(TextView)row.findViewById(R.id.sub_text_email);

                row.setTag(holder);
          }
          else
          {
                holder=(SpinnerHolder)row.getTag();

          }

          ProfileItem spinnerItem=spinnerData.get(position);
          
          SharedPreferences prefImage = context.getSharedPreferences("PREF_IMAGE", 0); 
          String current_image = prefImage.getString("image", "");
          
          SharedPreferences prefLocation = context.getSharedPreferences("PREF_LOCATION", 0); 
	      String location = prefLocation.getString("location", ""); 

          //holder.userImage.setImageDrawable(row.getResources().getDrawable(spinnerItem.getDrawableResID()));
          //holder.userImage.setImageDrawable(Environment.getExternalStorageDirectory().getAbsolutePath(),spinnerItem.getDrawableResID());
          //imageLoader.DisplayImage(current_image, holder.userImage);

        	  loadMyImg.DisplayImage(current_image, holder.userImage); // image from database
	      
        	  /*new DownloadImageTask((ImageView) holder.userImage)
              .execute("http://dev-ws.sharethatdata.com/art/resources/resource.jpg");*/ // image path

          //loadMyImg.DisplayImage(current_image, holder.userImage);
          holder.name.setText(spinnerItem.getName());
          holder.location.setText("I am " + location);
          //holder.email.setText(spinnerItem.getEmail());

          return row;

    }

    private static class SpinnerHolder
    {
          ImageView userImage;
          TextView  name,location,email;

    }

}
