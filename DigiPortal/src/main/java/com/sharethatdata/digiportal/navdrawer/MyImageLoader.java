package com.sharethatdata.digiportal.navdrawer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sharethatdata.digiportal.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

	public class MyImageLoader {
	
		MemoryCache memoryCache=new MemoryCache();
		FileCache fileCache;
		private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
		ExecutorService executorService; 
	
		public MyImageLoader(Context context){
		    fileCache=new FileCache(context);
		    executorService=Executors.newFixedThreadPool(5);
		}
	
		final int stub_id=R.drawable.user;
		public void DisplayImage(String url, ImageView imageView)
		{
		    imageViews.put(imageView, url);
		    Bitmap bitmap=memoryCache.get(url);
		   // if(bitmap!=null){
	
			    /* *** */
	            
	            if(url != "null" & url != ""){
	
	                byte[] decodedImage = Base64.decode(url, Base64.DEFAULT);
	
	
	                InputStream inputStream = new ByteArrayInputStream(decodedImage);
	                //Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);
	                bitmap = BitmapFactory.decodeStream(inputStream);
	                
	
	                String mBaseFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Images/" + "User";
	                if(!new File(mBaseFolderPath).exists()){
	                    Log.v("!new File(mBaseFolderPath).exists()","Folders not exists");
	
	                    if(new File(mBaseFolderPath).mkdirs())
	                        Log.v("new File(mBaseFolderPath).mkdir()","Folders Created");
	                    else
	                        Log.v("new File(mBaseFolderPath).mkdir()","Folders not Created");
	                }else
	                    Log.v("!new File(mBaseFolderPath).exists()","Folders exists");
	
	
	                String mFilePath = mBaseFolderPath + "/" + "User" + ".png";
	
	                File file = new File(mFilePath);
	
	                try{
	
	                    if (!file.exists()){
	
	                        Log.v("!file.exists()","file not exists");
	
	                        if(file.createNewFile())
	                            Log.v("createNewFile()","FileCreated");
	                        else
	                            Log.v("createNewFile()","FileNotCreated");
	                    }
	                    else
	                        Log.v("!file.exists()","file exists");
	
	                    FileOutputStream stream = new FileOutputStream(file);
	
	                    //bitmapImage.compress(Bitmap.CompressFormat.PNG, 50, stream);
	                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
	
	                    inputStream.close();
	                    //bitmapImage.recycle();
	                    bitmap.recycle();
	
	                    stream.flush();
	                    stream.close();
	
	                    Log.v("Image Saved ",""+file);   
	                     
	                }
	                catch(Exception e)
	                {
	                    Log.d("SaveFile",""+e.toString());
	                    e.printStackTrace();
	                }
	
	                Log.v("Photo id + ",""+file);
	                
	                if(file.exists()){
	
	                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
	
	                    imageView.setImageBitmap(myBitmap);
	
	            }
	            
	            /* *** */
			    
			   
	
	            }
	            else
			    {
			        queuePhoto(url, imageView);
			        imageView.setImageResource(stub_id);
			    }
		    /*}
		    
		    else
		    {
		        queuePhoto(url, imageView);
		        imageView.setImageResource(stub_id);
		    }*/
		}
		
		private void queuePhoto(String url, ImageView imageView)
		{
		    PhotoToLoad p=new PhotoToLoad(url, imageView);
		    executorService.submit(new PhotosLoader(p));
		}
		
		private Bitmap getBitmap(String url) 
		{
		    File f=fileCache.getFile(url);
		
		    //from SD cache
		    Bitmap b = decodeFile(f);
		    if(b!=null)
		        return b;
		
		    //from web
		    try {
		        Bitmap bitmap=null;
		        URL imageUrl = new URL(url);
		        HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
		        conn.setConnectTimeout(30000);
		        conn.setReadTimeout(30000);
		        conn.setInstanceFollowRedirects(true);
		        InputStream is=conn.getInputStream();
		        OutputStream os = new FileOutputStream(f);
		        Utils.CopyStream(is, os);
		        os.close();
		        bitmap = decodeFile(f);
		        return bitmap;
		    } catch (Throwable ex){
		       ex.printStackTrace();
		       if(ex instanceof OutOfMemoryError)
		           memoryCache.clear();
		       return null;
		    }
		}
		
		//decodes image and scales it to reduce memory consumption
		private Bitmap decodeFile(File f){
		    try {
		        //decode image size
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
		        BitmapFactory.decodeStream(new FileInputStream(f),null,o);
		
		        //Find the correct scale value. It should be the power of 2.
		        final int REQUIRED_SIZE=210;
		        int width_tmp=o.outWidth, height_tmp=o.outHeight;
		        int scale=1;
		        while(true){
		            if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
		                break;
		            width_tmp/=2;
		            height_tmp/=2;
		            scale*=2;
		        }
		
		        //decode with inSampleSize
		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize=scale;
		        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		    } catch (FileNotFoundException e) {}
		    return null;
		}
		
		//Task for the queue
		private class PhotoToLoad
		{
		    public String url;
		    public ImageView imageView;
		    public PhotoToLoad(String u, ImageView i){
		        url=u; 
		        imageView=i;
		    }
		}
		
		class PhotosLoader implements Runnable {
		    PhotoToLoad photoToLoad;
		    PhotosLoader(PhotoToLoad photoToLoad){
		        this.photoToLoad=photoToLoad;
		    }
		
		    @Override
		    public void run() {
		        if(imageViewReused(photoToLoad))
		            return;
		        Bitmap bmp=getBitmap(photoToLoad.url);
		        memoryCache.put(photoToLoad.url, bmp);
		        if(imageViewReused(photoToLoad))
		            return;
		        BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
		        Activity a=(Activity)photoToLoad.imageView.getContext();
		        a.runOnUiThread(bd);
		    }
		}
		
		boolean imageViewReused(PhotoToLoad photoToLoad){
		    String tag=imageViews.get(photoToLoad.imageView);
		    if(tag==null || !tag.equals(photoToLoad.url))
		        return true;
		    return false;
		}
		
		//Used to display bitmap in the UI thread
		class BitmapDisplayer implements Runnable
		{
		    Bitmap bitmap;
		    PhotoToLoad photoToLoad;
		    public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
		    public void run()
		    {
		        if(imageViewReused(photoToLoad))
		            return;
		        if(bitmap!=null)
		            photoToLoad.imageView.setImageBitmap(bitmap);
		        else
		            photoToLoad.imageView.setImageResource(stub_id);
		    }
		}
		
		public void clearCache() {
		    memoryCache.clear();
		    fileCache.clear();
		}
	
	}
