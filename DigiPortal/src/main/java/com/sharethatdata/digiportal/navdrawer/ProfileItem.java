package com.sharethatdata.digiportal.navdrawer;

import java.io.File;

import android.app.Activity;
import android.graphics.Bitmap;

public class ProfileItem{
	 int drawableResID;
	 //File fileResID;
	 String bitmapString;
	 String fileResID;
	 Bitmap imageBitmap; 
     String name;
     String email;
     
     private Activity activity;

     public ProfileItem(int drawableResID, String name, String email) {
           super();
           this.drawableResID = drawableResID; 
           this.name = name;
           this.email = email;
     }
     
     public ProfileItem(String bitmapString, String nameUserString, String emailUserString) {
         super();
         this.bitmapString = bitmapString;
         this.name = nameUserString;
         this.email = emailUserString; 
   }
     
     public ProfileItem(Bitmap imageBitmap, String email, String fileResID) {
         super();
         this.imageBitmap = imageBitmap;
         this.email = email;
         this.fileResID = fileResID;
   }

     public int getDrawableResID() {
           return drawableResID;
     }
     public void setDrawableResID(int drawableResID) {
           this.drawableResID = drawableResID;
     }
     public String getName() {
           return name;
     }
     public void setName(String name) {
           this.name = name;
     }
     public String getEmail() {
           return email;
     }
     public void setEmail(String email) {
           this.email = email;
     }
}
