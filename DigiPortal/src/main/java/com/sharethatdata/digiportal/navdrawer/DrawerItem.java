package com.sharethatdata.digiportal.navdrawer;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class DrawerItem extends Application{
 
	String ItemName;
    int imgResID;
    String title;
    boolean isSpinner;
    public String counter;
    
    public DrawerItem(String itemName, String imgResID, Context context, String counter) {
        //super();
        ItemName = itemName;
        this.imgResID = context.getResources().getIdentifier( imgResID, "drawable", context.getPackageName());
        this.counter = counter;
    }

    /*public DrawerItem(String itemName, int imgResID) {
          super();
          ItemName = itemName;
          this.imgResID = imgResID;
    }*/
    
    public DrawerItem(String itemName, String imgResID, Context context) {
        //super();
        ItemName = itemName;
        this.imgResID = context.getResources().getIdentifier( imgResID, "drawable", context.getPackageName());
    }
    
    public DrawerItem(boolean isSpinner) {
        //this(null, "", null);
        this.isSpinner = isSpinner;
    }

    public DrawerItem(String title) {
       //this(null, "", null);
        this.title = title;
  	}

    public String getItemName() {
          return ItemName;
    }
    public void setItemName(String itemName) {
          ItemName = itemName;
    }
    public int getImgResID() {
          return imgResID;
    }
    public void setImgResID(int imgResID) {
          this.imgResID = imgResID;
    }
    
      public String getTitle() {
        return title;
	  }
	  public void setTitle(String title) {
	        this.title = title;
	  }
	
	  public boolean isSpinner() {
	        return isSpinner;
	  }

}
