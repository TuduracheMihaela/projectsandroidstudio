package com.sharethatdata.digiportal.navdrawer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.UserProfileDetail;
import com.sharethatdata.digiportal.MyGlobals;
import com.sharethatdata.digiportal.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cUserStatus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.sax.StartElementListener;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	private String myUser = "";
	private String myPass = "";
	List<String> statuskeyArray =  new ArrayList<String>();
	List<String> statusnameArray =  new ArrayList<String>();
	List<String> locationkeyArray =  new ArrayList<String>();
	List<String> locationnameArray =  new ArrayList<String>();
	 
    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;
    
    private String myContactID = "";
    private String myContact = "";
    private String mySurname = "";
    private String myLastname = "";
    private String myStatusID = "";
    private String myStatusName = "";
    private String myLocationID = "";
    private String myLocationName = "";
    private String myImage = "";
    private String MyIdImage = "";
    
    private int check=0;
    
    private int old_position;
    
    public DrawerAdapter(Context context, int layoutResourceID,
                List<DrawerItem> listItems, List<String> statusKey, List<String> statusName, List<String> locationKey, List<String> locationName) {
          super(context, layoutResourceID, listItems);
          this.context = context;
          this.drawerItemList = listItems;
          this.layoutResID = layoutResourceID;
          this.statuskeyArray = statusKey;
          this.statusnameArray = statusName;
          this.locationkeyArray = locationKey;
          this.locationnameArray = locationName;
          
          Globals = ((MyGlobals)context.getApplicationContext());
          myUser = Globals.getValue("user");
          myPass = Globals.getValue("pass");
          MyProvider = new WSDataProvider(myUser, myPass);

          
          /*SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", 0 );
          String current_user = sharedPrefs.getString("prefUsername", "");*/
          SharedPreferences prefs = context.getSharedPreferences("defaults", 0 ); 
          String current_user = prefs.getString("Name", "");
          myContact = current_user;
          
          SharedPreferences prefContactID = context.getSharedPreferences("PREF_ID_CONTACT", 0); 
          String current_id_user = prefContactID.getString("idContact", "");
          myContactID = current_id_user;
          
          SharedPreferences prefContact = context.getSharedPreferences("PREF_CONTACT", 0); 
          String current_surname = prefContact.getString("surname", "");
          String current_lastname = prefContact.getString("lastname", "");
          mySurname = current_surname;
          myLastname = current_lastname;
          
          SharedPreferences prefStatus = context.getSharedPreferences("PREF_STATUS", 0);
          String current_status = prefStatus.getString("status_name", "");
          myStatusName = current_status;
          
          SharedPreferences prefLocation = context.getSharedPreferences("PREF_USER_LOCATION", 0);
          String current_location = prefLocation.getString("location_name", "");
          myLocationName = current_location;
    }

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub

    	  DrawerItem menuItem = getItem(position) ;
          final DrawerItemHolder drawerHolder;
          View view = convertView;
          
          

          if (view == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                drawerHolder = new DrawerItemHolder();

                view = inflater.inflate(layoutResID, parent, false);
                drawerHolder.ItemName = (TextView) view.findViewById(R.id.drawer_itemName);
                drawerHolder.ItemName_description = (TextView) view.findViewById(R.id.drawer_itemName_description);
                drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);

                /*drawerHolder.spinner = (Spinner) view
                        .findViewById(R.id.drawerSpinner);*/
                
                drawerHolder.list = (ListView) view.findViewById(R.id.drawerListProfiles);

	            drawerHolder.title = (TextView) view.findViewById(R.id.drawerTitle);
	            
	            drawerHolder.textCounter = (TextView) view.findViewById(R.id.drawer_itemCounter); // counter
	
	            drawerHolder.headerLayout = (LinearLayout) view
	                        .findViewById(R.id.headerLayout);
	            drawerHolder.itemLayout = (LinearLayout) view
	                        .findViewById(R.id.itemLayout);
	            drawerHolder.profileLayout = (LinearLayout) view
	                        .findViewById(R.id.profileLayout);
	            drawerHolder.spinnerStatus = (Spinner) view
	            		    .findViewById(R.id.statusSpinner);
	            drawerHolder.spinnerLocation = (Spinner) view
            		    .findViewById(R.id.locationSpinner);

            view.setTag(drawerHolder);

          } else {
                drawerHolder = (DrawerItemHolder) view.getTag();

          }
          
          DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);

          if (dItem.isSpinner()) {
              drawerHolder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.profileLayout.setVisibility(LinearLayout.VISIBLE);

              List<ProfileItem> userList = new ArrayList<ProfileItem>();

                /*userList.add(new ProfileItem(R.drawable.ic_launcher, "Ahamed Ishak",
                          "ishakgmail.com"));

                userList.add(new ProfileItem(R.drawable.ic_launcher, "Brain Jekob",
                          "brain.jgmail.com"));*/
          
                userList.add(new ProfileItem("", mySurname + " " + myLastname, ""));

              ProfileAdapter adapter = new ProfileAdapter(context,
                          R.layout.navdrawer_profile, userList);

              //drawerHolder.spinner.setAdapter(adapter);
              drawerHolder.list.setAdapter(adapter);

             /* drawerHolder.spinner
                          .setOnItemSelectedListener(new OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> arg0,
                                            View arg1, int arg2, long arg3) {

                                      Toast.makeText(context, "User Changed",
                                                  Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> arg0) {
                                      // TODO Auto-generated method stub

                                }
                          });*/
              
              drawerHolder.list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					
					 //Toast.makeText(context, "User Clicked " + myContactID,Toast.LENGTH_SHORT).show();
					 
					 Activity activity = (Activity) context;
					 Intent intent = new Intent(context, UserProfileDetail.class);
					 intent.putExtra("idUser", myContactID);
					 activity.startActivityForResult(intent, 1);
					 //activity.startActivity(intent);
					 activity.overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
					 
				}
			});
              
              loadStatusList(drawerHolder.spinnerStatus);
              loadLocationList(drawerHolder.spinnerLocation);
              
              SpinnerInteractionListener listenerStatus = new SpinnerInteractionListener(drawerHolder.spinnerStatus, "status");
              drawerHolder.spinnerStatus.setOnTouchListener(listenerStatus);
              drawerHolder.spinnerStatus.setOnItemSelectedListener(listenerStatus);
              
              SpinnerInteractionListener listenerLocation = new SpinnerInteractionListener(drawerHolder.spinnerLocation, "location");
              drawerHolder.spinnerLocation.setOnTouchListener(listenerLocation);
              drawerHolder.spinnerLocation.setOnItemSelectedListener(listenerLocation);
              
/*              drawerHolder.spinnerStatus.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					  int myIndex = drawerHolder.spinnerStatus.getSelectedItemPosition();
					  String myStatus = "";
					  
					  if(drawerHolder.spinnerStatus != null && drawerHolder.spinnerStatus.getSelectedItem() !=null ) {
						  myStatus = (String)drawerHolder.spinnerStatus.getSelectedItem();
						  System.out.println("status: " + myStatus);
						} else  { 

						}
					  
					  if(myIndex != -1){
						  myStatus = statuskeyArray.get(myIndex);	
						  
						  System.out.println("status: " + myStatus);
						 
					  }
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					
					
				}
			});*/


        } else if (dItem.getTitle() != null) {
              drawerHolder.headerLayout.setVisibility(LinearLayout.VISIBLE);
              drawerHolder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.profileLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.title.setText(dItem.getTitle());

        } else {

              drawerHolder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.profileLayout.setVisibility(LinearLayout.INVISIBLE);
              drawerHolder.itemLayout.setVisibility(LinearLayout.VISIBLE);

              /*drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(
                          dItem.getImgResID()));*/
              drawerHolder.icon.setImageResource(menuItem.imgResID);
              drawerHolder.ItemName.setText(dItem.getItemName());
              if(drawerHolder.ItemName.getText() == "Dashboard" || drawerHolder.ItemName.getText() == "News" || drawerHolder.ItemName.getText() == "Messages" ){
            	  drawerHolder.ItemName_description.setVisibility(View.GONE);
            	  
              }else if(drawerHolder.ItemName.getText() == "DigiWorkTime" ){
            	  drawerHolder.ItemName_description.setText(getContext().getString(R.string.drawer_description_digi_time));
            	  
              }else if(drawerHolder.ItemName.getText() == "DigiVisitor" ){
            	  drawerHolder.ItemName_description.setText(getContext().getString(R.string.drawer_description_digi_visitor));
            	  
              }else if(drawerHolder.ItemName.getText() == "DigiStock" ){
            	  drawerHolder.ItemName_description.setText(getContext().getString(R.string.drawer_description_digi_stock));
            	  
              }else if(drawerHolder.ItemName.getText() == "DigiSupport" ){
            	  drawerHolder.ItemName_description.setText(getContext().getString(R.string.drawer_description_digi_support));
            	  
              }
              
              drawerHolder.textCounter.setText(" "+dItem.counter);
              if(dItem.counter == null){
            	  drawerHolder.textCounter.setVisibility(View.GONE);
              }else{
            	  drawerHolder.textCounter.setVisibility(View.VISIBLE);
            	  }

        }
        return view;
    }

    private static class DrawerItemHolder {
    	 TextView ItemName, ItemName_description, title, textCounter;
         ImageView icon;
         LinearLayout headerLayout, itemLayout, profileLayout;
         ListView list;
         Spinner spinnerStatus, spinnerLocation;
    }
    
    private void loadStatusList(Spinner spinnerStatus){
 	 
	 	final ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, statusnameArray);
	 	status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	spinnerStatus.setAdapter(status_adapter);
	 	
	 	int spinnerPosition = status_adapter.getPosition(myStatusName);
			//set the default according to value
	 	spinnerStatus.setSelection(spinnerPosition);
    }
    
    private void loadLocationList(Spinner spinnerLocation){
    	 
	 	final ArrayAdapter<String> location_adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, locationnameArray);
	 	location_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	spinnerLocation.setAdapter(location_adapter);
	 	
	 	int spinnerPosition = location_adapter.getPosition(myLocationName);
			//set the default according to value
	 	spinnerLocation.setSelection(spinnerPosition);
    }
 	
 	public class SpinnerInteractionListener implements OnItemSelectedListener, View.OnTouchListener {
 		
 		boolean userSelect = false;
 		
 		Spinner spinner;
 		String whichSpinner;

 	      public SpinnerInteractionListener(Spinner spinner, String wspinner) {
 				this.spinner = spinner;
 				this.whichSpinner = wspinner;
 				
 			}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			userSelect = true;
	        return false;
		}

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			 if (userSelect) { 
				 if(whichSpinner.contains("status")){
					  String myStatusId = "";
					  String myStatusName = "";
					  
					  myStatusName = (String) spinner.getSelectedItem();
					  System.out.println("status name: " + myStatusName);

					  
					  int myIndex = spinner.getSelectedItemPosition();
					  if(statuskeyArray.size() == 0){
						  Log.e("Adapter", "Summary array is null");	  
					  }else{
						  myStatusId = statuskeyArray.get(myIndex);	
						  Log.e("Adapter", "Summary array id: " + myStatusId);
						  
						  MyProvider = new WSDataProvider(myUser, myPass);
						  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
						  try {
						  
							  new UpdateTask(myStatusId, "status").execute();
						  }catch (Exception e) {
					            System.out.println(e.toString());

					        }
					  }
				 }else if(whichSpinner.contains("location")){
					 	String myLocationId = "";
					 	String myLocationName = "";
					  
					  myLocationName = (String) spinner.getSelectedItem();
					  System.out.println("location name: " + myLocationName);

					  
					  int myIndex = spinner.getSelectedItemPosition();
					  if(locationkeyArray.size() == 0){
						  Log.e("Adapter", "Summary array is null");	  
					  }else{
						  myLocationId = statuskeyArray.get(myIndex);	
						  Log.e("Adapter", "Summary array id: " + myLocationId);
						  
						  MyProvider = new WSDataProvider(myUser, myPass);
						  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
						  try {
						  
							  new UpdateTask(myLocationId, "location").execute();
						  }catch (Exception e) {
					            System.out.println(e.toString());    
						  }
					  }
				 }
					  
					     userSelect = false;
		    }
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}

 	   

 	}
 	
 	
	/** AsyncTask update status sick day  */
	private class UpdateTask extends AsyncTask<String, String, Boolean>{
		String id, whichSpinner;
		
	
	    public UpdateTask(String id, String whichSpinner) {
	    	this.id = id;
	    	this.whichSpinner = whichSpinner;
		}

		@Override
	    protected Boolean doInBackground(String... args) {	
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;
			  
			  if(whichSpinner.contains("status")){
			  		MyProvider.createUserCurrentStatus(myContact, id);
			  		
			  }else if(whichSpinner.contains("location")){
				  	MyProvider.createUserCurrentLocation(myContact, id);
			  }
	
	    	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	if(success){
	    		
	    	}
	    }
	}

	
}
