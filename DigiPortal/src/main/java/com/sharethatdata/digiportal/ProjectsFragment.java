package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import android.app.Fragment;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.digiportal.R;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

*//**
 * Created by mihu__000 on 7/30/2015.
 *//*
public class ProjectsFragment extends Fragment {

    private static ProgressDialog pDialog;

    private static String url = "http://dev-ws.sharethatdata.com/get_items.php?object=app_project&token=bl123&user=mverbeek&filtered=true";

    private static final String TAG_ITEMS = "items";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_IMAGE = "image";

    // projects JSONArray
    JSONArray projects = null;
    public ArrayList<HashMap<String, Object>> projectsList;

    GridView gridView;
    ImageView imageName;
    final int image_id = R.drawable.ic_launcher;

    public ProjectsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_projects, container, false);

        gridView = (GridView)rootView.findViewById(R.id.gridViewProjects);

        imageName = new ImageView(getActivity());
      //  imageName = (ImageView)rootView.findViewById(R.id.imageName);

        //new GetProjects().execute();

        return rootView;
    }


    class GetProjects extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Loading Projects");
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            projectsList = new ArrayList<HashMap<String, Object>>();

            // Creating service handler class instance
            JSONParser jParser = new JSONParser();

            // Making a request to url and getting response
            String jsonStr = jParser.makeServiceCall(url, JSONParser.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    projects = jsonObj.getJSONArray(TAG_ITEMS);

                    // looping through All Contacts
                    for (int i = 0; i < projects.length(); i++) {
                        JSONObject c = projects.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String image = c.getString(TAG_IMAGE);

                        HashMap<String, Object> project = new HashMap<String, Object>();


                        // adding each child node to HashMap key => value

                        project.put(TAG_NAME, id);
                        project.put(TAG_NAME, name);

                        if(image != "null"){

                            byte[] decodedImage = Base64.decode(image, Base64.DEFAULT);


                            InputStream inputStream = new ByteArrayInputStream(decodedImage);
                            Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);

                            String mBaseFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Images/" + id;
                            if(!new File(mBaseFolderPath).exists()){
                                Log.v("!new File(mBaseFolderPath).exists()","Folders not exists");

                                if(new File(mBaseFolderPath).mkdirs())
                                    Log.v("new File(mBaseFolderPath).mkdir()","Folders Created");
                                else
                                    Log.v("new File(mBaseFolderPath).mkdir()","Folders not Created");
                            }else
                                Log.v("!new File(mBaseFolderPath).exists()","Folders exists");


                            String mFilePath = mBaseFolderPath + "/" + id + ".png";

                            File file = new File(mFilePath);

                            try{

                                if (!file.exists()){

                                    Log.v("!file.exists()","file not exists");

                                    if(file.createNewFile())
                                        Log.v("createNewFile()","FileCreated");
                                    else
                                        Log.v("createNewFile()","FileNotCreated");
                                }
                                else
                                    Log.v("!file.exists()","file exists");

                                FileOutputStream stream = new FileOutputStream(file);

                                bitmapImage.compress(Bitmap.CompressFormat.PNG, 50, stream);

                                inputStream.close();
                                bitmapImage.recycle();

                                stream.flush();
                                stream.close();

                                Log.v("Image Saved ",""+file);
                            }
                            catch(Exception e)
                            {
                                Log.v("SaveFile",""+e);
                                e.printStackTrace();
                            }

                            Log.v("Photo id"+i,""+file);


                            project.put(TAG_IMAGE, file);
                        }else{
                            project.put(TAG_IMAGE, image_id);
                        }


//                        if(image!=null){
//                            Byte[] ImageFotoBase64 = Convert.FromBase64String(image);
//                            image.Source = ImageSource.FromStream(() => new MemoryStream(ImageFotoBase64));
//                        }else{
//                            image.Source = "projects.png";
//                        }


                        projectsList.add(project);
                    }

                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }

    //                  if(contactList != null){
    //                      for(HashMap<String, String> map:   contactList)
    //                          for(Entry<String, String> mapEntry: map.entrySet()) {
    //                          String key = mapEntry.getKey();
    //                          String value = mapEntry.getValue();
    //                          System.out.println(" key is >>>>> "+key);
    //                          System.out.println("value is <<<<< "+value);
    //                          }
    //
    //                  }

                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            } else {
                Log.e("JSONParser", "Couldn't get any data from the url");
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);





            *//**
             * Updating parsed JSON data into ListView
             * *//*

            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), projectsList,
                    R.layout.fragment_projects_list, new String[] { TAG_NAME,TAG_IMAGE}, new int[] { R.id.txtName, R.id.imgName});

           gridView.setAdapter(adapter);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        }


    //}
}
*/