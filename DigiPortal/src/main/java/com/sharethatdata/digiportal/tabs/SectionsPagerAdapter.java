package com.sharethatdata.digiportal.tabs;/*package com.sharethatdata.digiportal.tabs;

import java.util.Locale;

import com.sharethatdata.digiportal.MessagesComposeFragment;
import com.sharethatdata.digiportal.MessagesInboxFragment;
import com.sharethatdata.digiportal.MessagesSentFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {  
	  
    public SectionsPagerAdapter(FragmentManager fm) {  
        super(fm);  
    }  

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new MessagesInboxFragment();
            case 1:
            	return new MessagesSentFragment();
            case 2: 
            	return new MessagesComposeFragment();
        }
		return null;
    } 

    @Override  
    public int getCount() {  
        // Show 3 total pages.  
        return 3;  
    }  

    @Override
    public CharSequence getPageTitle(int position) {
    	 Locale l = Locale.getDefault();  
         switch (position) {  
         case 0:  
             return ("Inbox").toUpperCase(l);  
         case 1:  
             return ("Sent").toUpperCase(l);  
         case 2:  
             return ("Compose").toUpperCase(l);  
         }  
         return null; 
        return "Section " + (position + 1);
    }
 
}

*/