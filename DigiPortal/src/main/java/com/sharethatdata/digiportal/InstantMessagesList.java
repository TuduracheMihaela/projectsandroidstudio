package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.message.inbox.ListViewAdapter;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cImage;
import com.sharethatdata.webservice.datamodel.cMessage;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import androidx.fragment.app.Fragment;

public class InstantMessagesList extends Fragment {
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;
	
	private ActionBar actionBar;
	
	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> userList;
	List<String> userkeyArray =  new ArrayList<String>();
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	
	private static ProgressDialog pDialog;
	
	public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    
    ImageView ivIcon;
    TextView textTitle;
    private ImageView btSelectUser;
    
    String statusRead = "";
    String userName = "";
    
    int indexTab = 0;
    
    String idMessage = "";
    String idContact = "";
   // String idImage = "";	
    
    private String error_message = "";
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.chat_fragment_listview_user_message, container, false);
        setHasOptionsMenu(true);
        
        actionBar = getActivity().getActionBar();
        
        Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
	    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
        
        lv = (ListView)rootView.findViewById(R.id.listView);
        textTitle = (TextView)rootView.findViewById(R.id.textTitle);
        btSelectUser = (ImageView)rootView.findViewById(R.id.btSelectUser);

        //new LoadContact().execute();
        

        //tvItemName.setText(getArguments().getString(ITEM_NAME));
       /* ivIcon.setImageDrawable(rootView.getResources().getDrawable(
                    getArguments().getInt(IMAGE_RESOURCE_ID)));*/
    	
        
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				HashMap<String, String> itemsList = (HashMap<String, String>) itemList.get(position);
				 String itemId = (String) itemsList.get("id");
				 String itemNameId = (String) itemsList.get("last_message_user");
				 String itemName = (String) itemsList.get("title");
				 String imageContact = (String) itemsList.get("image");
				 
				 SharedPreferences.Editor editor = getActivity().getSharedPreferences("PREF_IMAGE_CONTACT",0).edit();
        	    	editor.clear().commit();
     	   	     editor.putString("image", imageContact);
     	   	     editor.putString("name", itemName);
     	   	     editor.commit();
				 
				 Intent intent = new Intent(getActivity().getApplicationContext(), InstantMessagesListDetail.class);
				 intent.putExtra("ID", itemId);
				 intent.putExtra("NameId", itemNameId);
				 intent.putExtra("Name", itemName);
				 intent.putExtra("image", imageContact);
				 startActivity(intent);
				
				 // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);	
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				 
				  idMessage = itemId;
				
				  /*Toast.makeText(getActivity().getApplicationContext(),
					      "Message: " + itemId + " Position: " + position + " Name: " + itemName, Toast.LENGTH_SHORT)
					      .show();*/
				
			}
		});
        
        
        btSelectUser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity().getApplicationContext(), InstantMessagesListUsers.class);
				startActivity(intent);
				
			}
		});
    
        return rootView;
    }
    
    
    @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     Log.d("TAG", "onCreate Inbox");
	     setHasOptionsMenu(true);
	 }
   
   @Override
	public void onResume(){
		super.onResume();
		Log.d("TAG", "onResume Inbox");
		setHasOptionsMenu(true);  
		//actionBar.removeAllTabs();
	    //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		new LoadListMessages().execute();
		// Toast.makeText(getActivity(), "onResume Inbox", Toast.LENGTH_SHORT).show();

	}
   
   @Override
  	public void onPause() {
     	super.onPause();
     	Log.d("TAG", "onPause Inbox");

     }
   
   @Override
	public void onDestroy() {
	   	super.onDestroy();
	   	Log.d("TAG", "onDestroy Inbox");
   }
   
   @Override
   public void onAttach(Activity activity){
	   super.onAttach(activity);
   }
   
   public void myRefresh(){
	   Log.d("TAG", "onResume Inbox");
	   Globals = ((MyGlobals)getActivity().getApplicationContext());
       myUser = Globals.getValue("user");
       MyProvider = new WSDataProvider(myUser, myPass);
	   new LoadListMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
   }

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_refresh:
	        // new LoadListMessages().execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }
	 
    
    /**
	 * Background Async Task to Load all holidays by making HTTP Request
  	 * */
	class LoadListMessages extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading Messages");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.setCanceledOnTouchOutside(false);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cMessage> message = MyProvider.getMessages();
	    	 for(cMessage entry : message)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("titleID", entry.user_from);
                map.put("message_for_me", entry.user_to);
                map.put("last_message_user", entry.user_last_message);
                map.put("created", entry.datetime);
                map.put("description", entry.message);
                map.put("status", entry.status_read);
                
                statusRead = entry.status_read;
                idContact = entry.user_last_message;
                
                    	System.out.println(" " + idContact);  	
                    	cContact contact = MyProvider.getContact(idContact);
              	    	 String surname = contact.surname;
              	    	 String lastname = contact.lastname;
 
              	    // get image from MyRoboProvider getImage with String
              	    	 String image = MyRoboProvider.getImageThumnail(contact.image_id);              	    	 
              	    	 
      	    	 map.put("title", surname + " " + lastname);
      	    	 map.put("image", image);

                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	    	super.onPostExecute(result);
	    	pDialog.dismiss();
	    	 // here you check the value of getActivity() and break up if needed
	        if(getActivity() == null)
	            return;
            /**
             * Updating parsed JSON data into ListView
             * */
	    	getActivity().runOnUiThread(new Runnable() {
	            public void run() {

	            	int total = 0;
	            	int total_unread = 0;
	            	int total_read = 0;
	            	int myRow = 0;
	            	for (HashMap<String, String> map : itemList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "status")
	           	        	{
	           	        		
	           	        		try {
	           	        			myRow = Integer.parseInt(entry.getValue().toString());
	           	        			String row = String.valueOf(myRow);
	           	        			
	           	        			if(row.contains("0")){
	           	     	        	System.out.println("hello in if 0: " + myRow);
	           	     	       		//textTitle.setTypeface(null, Typeface.BOLD);
	           	     	       		//textTitle.setText("#000");
	           	     	        	//lv.setBackgroundResource(R.color.orange5);
	           	     	        	total_unread++;
	           	     	       	}
	           	     	        if(row.contains("1")){
	           	     	        	System.out.println("hello in if 1: " + myRow);
	           	     	       		//textTitle.setTypeface(null, Typeface.BOLD);
	           	     	       		//textTitle.setText("#000");
	           	     	        	//lv.setBackgroundResource(R.color.yellow1);
	           	     	        	total_read++;
	           	     	       	}
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myRow = 0;
	           	        		}
	           	        		total = total_unread + total_read;
	           	        	}
	           	        }
	            	System.out.println("Total read: " + total_read);
	           		System.out.println("Total unread: " + total_unread);
	           		System.out.println("Total : " + total);

	           		SharedPreferences.Editor editor = getActivity().getSharedPreferences("PREF_TOTAL_UNREAD_MESSAGES", 0).edit();
		   	   	     editor.putString("unreadMessages", String.valueOf(total_unread));
		   	   	     editor.commit();
	            }
	        });
	    	
	    	SharedPreferences.Editor editor = getActivity().getSharedPreferences("PREF_CHECK", 0).edit();
  	   	     editor.putString("check", "chat");
  	   	     editor.commit();
	        
	    	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, itemList);
	        lv.setAdapter(customAdapter);
	    	
            /*ListAdapter adapter = new SimpleAdapter(
                    getActivity(), itemList,
                    R.layout.fragment_listview_items, new String[] { "id", "title","created","description"}, new int[] { R.id.textViewID,R.id.textTitle, R.id.textDateTime, R.id.textDescription});

           lv.setAdapter(adapter);*/
           
           

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
	    }
	}
	


}
