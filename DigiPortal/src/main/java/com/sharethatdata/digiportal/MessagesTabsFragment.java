package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;

import com.sharethatdata.digiportal.tabs.SectionsPagerAdapter;

public class MessagesTabsFragment extends Fragment implements ActionBar.TabListener{

	
	private SectionsPagerAdapter mSectionsPagerAdapter; 
	private ViewPager mViewPager; 
	private ActionBar actionBar;
	private View view;
	
	// Tab titles
    private String[] tabs = { "Inbox", "Sent", "Compose"};
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_messages_tabs, container, false);
		
		
		// Initilization
 		actionBar = getActivity().getActionBar();
 		//mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());
 		
 		mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

 		mSectionsPagerAdapter.notifyDataSetChanged();
 		mViewPager = (ViewPager) view.findViewById(R.id.pager);  
 		
         mViewPager.setAdapter(mSectionsPagerAdapter);
         actionBar.setHomeButtonEnabled(true);
         getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
         getActivity().getActionBar().setHomeButtonEnabled(true);
         actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
         
      // Adding Tabs
         for (String tab_name : tabs) {
             actionBar.addTab(actionBar.newTab().setText(tab_name)
                     .setTabListener(this));
         }
         
         *//**
          * on swiping the viewpager make respective tab selected
          * *//*
         mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
  
             @Override
             public void onPageSelected(int position) {
            	 //getActivity().invalidateOptionsMenu();
                 // on changing the page
                 // make respected tab selected
                 actionBar.setSelectedNavigationItem(position);
                 
                 switch (position) {
       	        case 0:
       	        	//Toast.makeText(getActivity(), "Inbox", Toast.LENGTH_SHORT).show();
       	        	MessagesInboxFragment fragmentI = (MessagesInboxFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 0);
                    if (fragmentI != null) {
                   	 fragmentI.myRefresh();
                    }
       	            break;
       	        case 1: {
       	        	//Toast.makeText(getActivity(), "Sent", Toast.LENGTH_SHORT).show();
	       	         MessagesSentFragment fragmentS = (MessagesSentFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 1);
	                 if (fragmentS != null) {
	                	 fragmentS.myRefresh();
	                 }
       	            break;
       	        }
       	        case 2:
       	        	//Toast.makeText(getActivity(), "Compose", Toast.LENGTH_SHORT).show();
	       	         MessagesComposeFragment fragmentC = (MessagesComposeFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 2);
	                 if (fragmentC != null) {
	                	 fragmentC.myRefresh();
	                 }
       	            break;
       	        }
                 
                 MessagesInboxFragment fragmentI = (MessagesInboxFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 0);
                 if (fragmentI != null) {
                	 fragmentI.myRefresh();
                 }
                 MessagesSentFragment fragmentS = (MessagesSentFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 1);
                 if (fragmentS != null) {
                	 fragmentS.myRefresh();
                 }
                 
                 MessagesComposeFragment fragmentC = (MessagesComposeFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, 2);
                 if (fragmentC != null) {
                	 fragmentC.myRefresh();
                 }
             }
  
             @Override
             public void onPageScrolled(int arg0, float arg1, int arg2) {
             }
  
             @Override
             public void onPageScrollStateChanged(int arg0) {
             }
         });
         
			
         Intent i = getActivity().getIntent();
         int tabToOpen = i.getIntExtra("ComposeTab", -1);
         if (tabToOpen!=-1) {
             // Open the right tab
        	 Toast.makeText(getActivity(), "RigthTab", Toast.LENGTH_SHORT).show();
        	 ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager);
 			viewPager.setCurrentItem(1);
         }
         
         Bundle bundle = this.getArguments();
         int myInt = bundle.getInt("ComposeTab", 2);
         if (myInt!=2) {
             // Open the right tab
        	 Toast.makeText(getActivity(), "RigthTab", Toast.LENGTH_SHORT).show();
        	 ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager);
 			viewPager.setCurrentItem(1);
         }

		return view;  
	}

	
	
	public void refresh(){
		
		
	}

	
	@Override
  	public void onPause() {
     	super.onPause();
     	actionBar.removeAllTabs();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
     }
	
	@Override
  	public void onResume() {
     	super.onResume();
     	
     	SharedPreferences pref = getActivity().getSharedPreferences("PREF_REPLY", 0); 
        int i = pref.getInt("ComposeTab", 0);
        if(i == 2){
	    		ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager);
	 			viewPager.setCurrentItem(2);
	    	}

     }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	    actionBar.removeAllTabs();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}
	
    @Override
    public void onAttach( Activity activity) {
        super.onAttach(activity);
        setHasOptionsMenu(true); 
    }
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    super.onCreateOptionsMenu(menu, inflater);
	    inflater.inflate(R.menu.refresh, menu);
	}

	
	 @Override
	 public void onPrepareOptionsMenu(Menu menu) {
	     super.onPrepareOptionsMenu(menu);
	     int page = mViewPager.getCurrentItem();
	     switch(page) {
	         case 0:
	             menu.findItem(R.id.action_refresh).setVisible(true);
	             break;
	         case 1:
	             menu.findItem(R.id.action_refresh).setVisible(true);
	             break;
	         case 2:
	             menu.findItem(R.id.action_refresh).setVisible(false);
	             break;  
	     }
	     return;
	 }

	 
	 @Override
	    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	    }

	    @Override
	    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	        // When the given tab is selected, switch to the corresponding page in the ViewPager.
	        mViewPager.setCurrentItem(tab.getPosition());
	        
	     // If we're trying to select this already-selected tab, bail out.
	        
	    }

	    @Override
	    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	    	int position = tab.getPosition();
	    	 switch (position) {
  	        case 0:
  	        	MessagesInboxFragment fragment = new MessagesInboxFragment();
                  fragment.myRefresh();

  	            break;
  	        case 1: {
  	        	//Toast.makeText(getActivity(), "Sent", Toast.LENGTH_SHORT).show();
  	            break;
  	        }
  	        case 2:
  	        	//Toast.makeText(getActivity(), "Compose", Toast.LENGTH_SHORT).show();
  	            break;
  	        }
	    }


}
*/