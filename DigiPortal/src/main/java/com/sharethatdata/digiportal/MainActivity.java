package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
//import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.digiportal.R;
import com.sharethatdata.digiportal.LoginActivity;
import com.sharethatdata.digiportal.navdrawer.DrawerAdapter;
import com.sharethatdata.digiportal.navdrawer.DrawerItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends FragmentActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] mNavigationDrawerItemTitles;
   // private ActionBarDrawerToggle mDrawerToggle;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    DrawerAdapter adapter;
    List<DrawerItem> dataList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
     // Initializing
        dataList = new ArrayList<DrawerItem>();

        mTitle = mDrawerTitle = getTitle();

       // mNavigationDrawerItemTitles = getResources().getStringArray(R.array.menu_slider_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ObjectDrawerItem[] drawerItem = new ObjectDrawerItem[3];
        drawerItem[0] = new ObjectDrawerItem(R.drawable.ic_launcher, "DashBoard");
        drawerItem[1] = new ObjectDrawerItem(R.drawable.ic_launcher, "Projects");
        drawerItem[2] = new ObjectDrawerItem(R.drawable.ic_launcher, "Contacts");

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.drawer_list_item, drawerItem);

        mDrawerList.setAdapter(adapter);
        
     // Add Drawer Item to dataList

          dataList.add(new DrawerItem(true)); // adding a spinner to the list
        
         dataList.add(new DrawerItem("My Favorites")); // adding a header to the list
        dataList.add(new DrawerItem("Message", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Likes", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Games", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Lables", R.drawable.ic_launcher));

         dataList.add(new DrawerItem("Main Options"));// adding a header to the list
        dataList.add(new DrawerItem("Search", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Cloud", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Camara", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Video", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Groups", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Import & Export",
                    R.drawable.ic_launcher));

         dataList.add(new DrawerItem("Other Option")); // adding a header to the list
        dataList.add(new DrawerItem("About", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Settings", R.drawable.ic_launcher));
        dataList.add(new DrawerItem("Help", R.drawable.ic_launcher));

       // adapter = new DrawerAdapter(this, R.layout.navdrawer_item,dataList);
        mDrawerList.setAdapter(adapter);
        
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
 
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            *//** Called when a drawer has settled in a completely closed state. *//*
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            *//** Called when a drawer has settled in a completely open state. *//*
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        
        if (savedInstanceState == null) {
        	 
            if (dataList.get(0).isSpinner()
                        & dataList.get(1).getTitle() != null) {
                  selectItem(2);
            } else if (dataList.get(0).getTitle() != null) {
                  selectItem(1);
            } else {
                  selectItem(0);
            }
      }

    }


     The click listner for ListView in the navigation drawer 
    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	if (dataList.get(position).getTitle() == null) {
                selectItem(position);
          }
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        Bundle args = new Bundle();
        switch (position) {
            case 2:
            	fragment = new NewsFragment();
                args.putString(NewsFragment.ITEM_NAME, dataList.get(position)
                            .getItemName());
                args.putInt(NewsFragment.IMAGE_RESOURCE_ID, dataList.get(position)
                            .getImgResID());
                break;
            case 3:
                fragment = new MessagesFragment();
                args.putString(MessagesFragment.ITEM_NAME, dataList.get(position)
                        .getItemName());
            args.putInt(MessagesFragment.IMAGE_RESOURCE_ID, dataList.get(position)
                        .getImgResID());
                break;
            case 4:
            	Toast.makeText(getApplicationContext(), "case 2", Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }

        if (fragment != null) {
        	fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            //mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            setTitle(dataList.get(position).getItemName());
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    *//**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     *//*

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    public class DrawerItemCustomAdapter extends ArrayAdapter<ObjectDrawerItem> {

        Context mContext;
        int layoutResourceId;
        ObjectDrawerItem data[] = null;

        public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, ObjectDrawerItem[] data) {

            super(mContext, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View listItem = convertView;

            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            listItem = inflater.inflate(layoutResourceId, parent, false);

            ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
            TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);

            ObjectDrawerItem folder = data[position];


            imageViewIcon.setImageResource(folder.icon);
            textViewName.setText(folder.name);

            return listItem;
        }

    }


    public class ObjectDrawerItem {

        public int icon;
        public String name;

        // Constructor.
        public ObjectDrawerItem(int icon, String name) {

            this.icon = icon;
            this.name = name;
        }
    }

}


*/