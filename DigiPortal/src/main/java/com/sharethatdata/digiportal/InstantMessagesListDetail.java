package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiportal.chat.ChatListAdapter;
import com.sharethatdata.digiportal.chat.Message;
import com.sharethatdata.digiportal.message.inbox.ListViewAdapter;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cMessage;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

public class InstantMessagesListDetail extends FragmentActivity {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	private static ProgressDialog pDialog;
	
	ArrayList<HashMap<String, String>> itemList;
	
	//private ArrayList<Message> mMessages;
	List<Message> mMessages =  new ArrayList<Message>();
	List<String> userkeyArray =  new ArrayList<String>();
	
	private String myUser = "";
	private String myPass = "";
	// vars for remember report
    private String report_user = "";
    // var id
    private String id = "";
    private String nameId = "";
    private String name = "";
    
    String idMessage = "";
    
    private String error_message = "";
    
    private EditText etMessage;
    private ImageView imgSend;
    private ListView lvChat;
    
    Fragment fragment = null;
    
 // Create a handler which can run code periodically
    private Handler handler = new Handler();
	
    private int preLast;
    
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.chat_listview_detail);
	        
	        Globals = ((MyGlobals)getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        
			
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
			    String valueID = extras.getString("ID");    
			    	id = valueID; 
			    String valueNameId = extras.getString("NameId");
			    	nameId = valueNameId;
			    String valueName = extras.getString("Name");
			    	name = valueName;
			}

			etMessage = (EditText) findViewById(R.id.etMessage);
			imgSend = (ImageView) findViewById(R.id.btSend);
		    lvChat = (ListView) findViewById(R.id.lvChat);
		    
		    new LoadMessagesItems().execute();
		    
		 // Automatically scroll to the bottom .
	        lvChat.setTranscriptMode(lvChat.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	        lvChat.setStackFromBottom(true);

			
			getActionBar().setDisplayHomeAsUpEnabled(true);
			setTitle(name);
			
			imgSend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					TextView messageEditText = (TextView)findViewById(R.id.etMessage);
					if(messageEditText.getText().toString().trim().length() == 0){
						Toast.makeText(InstantMessagesListDetail.this, getResources().getString(R.string.message_not_empty), Toast.LENGTH_SHORT).show();
					}else{
						RegisterTaskMessage messageTask;
						messageTask = new RegisterTaskMessage();
						messageTask.execute();
					}
		        	  
					/*mMessages.clear();
					
					String body = etMessage.getText().toString();
					// Use Message model to create new messages now      
	                Message message = new Message(nameId, body);
	                message.setUserId(nameId);
	                message.setBody(body);
	                
	                etMessage.setText("");
	                InputMethodManager inputManager = 
	                        (InputMethodManager) getApplicationContext().
	                            getSystemService(Context.INPUT_METHOD_SERVICE); 
	                inputManager.hideSoftInputFromWindow(InstantMessagesListDetail.this.getCurrentFocus().getWindowToken(),
	                        InputMethodManager.HIDE_NOT_ALWAYS); 
	                
	                lvChat.setTranscriptMode(lvChat.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	    	        lvChat.setStackFromBottom(true);*/

				}
			});
			
			// Run the runnable object defined every 30000 ms
		        //handler.postDelayed(runnable, 1500);
			
			lvChat.setOnScrollListener(new OnScrollListener() {
				
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
			            Log.i("a", "i am scrolling...");
			            handler.removeCallbacks(runnable);
			        }
					
					int threshold = 1;
					int count = lvChat.getCount();

					if (scrollState == SCROLL_STATE_IDLE) {
						if (lvChat.getLastVisiblePosition() >= count - threshold) {
							Log.i("b", "loading more data");
							// Execute LoadMoreDataTask AsyncTask
							handler.postDelayed(runnable, 5100);
						}
					}
					
				}
				
				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
				}
			});

	 }
	 
	 @Override
	 protected void onResume(){
		 super.onResume();
		 Log.e("onResume runnable","resume runnable");
		// Run the runnable object defined every 30000 ms
	        handler.postDelayed(runnable, 5100);
		 lvChat.setTranscriptMode(lvChat.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	     lvChat.setStackFromBottom(true);
	 }
	 
	 @Override
	 protected void onPause(){
		 super.onPause();
		 Log.e("onPause runnable","pause runnable");
		 handler.removeCallbacks(runnable);
	 }


	// This is working just if application is running
	 // Defines a runnable which is run every 30000 ms
	    private Runnable runnable = new Runnable() {
	        @Override
	        public void run() { 
	        	refreshMessages();
	           handler.postDelayed(this, 15000);
	           
	           System.out.println("refresh");

	        }
	    };
	    
	   /* public void loadData() {

	        // send your request
	       // receive it by callback or asynctask
	       yourDataList.addAll(mMessages);
	       adapter.notifyDataSetChanged();
	       isLoading = false;
	    }*/
	    
	    private void refreshMessages() {
	    	new LoadMessagesItems().execute();  
	    }
 
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   
			 //  fragment = new MessagesTabsFragment();
			   
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	 
	 
	 /**
		 * Background Async Task to Load all holidays by making HTTP Request
	  	 * */
		class LoadMessagesItems extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	      // Showing progress dialog
	         /*pDialog = new ProgressDialog(getActivity());
	         pDialog.setTitle("Loading Messages Inbox");
	         pDialog.setMessage("Please wait...");
	         pDialog.setIndeterminate(false);
	         pDialog.setCancelable(true);
	         pDialog.show();*/
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		    	
		    	Globals = ((MyGlobals) getApplicationContext());	
				
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
		    	 // load data from provider
		    	 itemList = new ArrayList<HashMap<String, String>>(); 
		    	 mMessages.clear();
		    	
		    	 List<cMessage> message = MyProvider.getMessageInstant(nameId);
		    	 for(cMessage entry : message)
		    	 {
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                
	                map.put("id",  String.valueOf(entry.id));
	                map.put("from", entry.user_from);
	                
	                // time start
    		        StringTokenizer tkTimeStart = new StringTokenizer(entry.datetime);
    		        String dateTokenizer = tkTimeStart.nextToken();  // <---  yyyy-mm-dd
    		        String timeTokenizer = tkTimeStart.nextToken();  // <---  hh:mm:ss
	                
	                map.put("created", entry.datetime);
	                map.put("description", entry.message);
	                map.put("status", entry.status_read);
	                
	                mMessages.add(new Message(entry.user_from, entry.message, timeTokenizer, dateTokenizer));
	    
	                // adding HashList to ArrayList
	                itemList.add(map);
		    	 }
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String result) {

		    	runOnUiThread(new Runnable() {
		            public void run() { 
		            	
		            	for (HashMap<String, String> map : itemList)
		          	        for (Entry<String, String> entry : map.entrySet())
		          	        {
		          	        	if(entry.getKey() == "status"){
		          	        		if(entry.getValue().equals("0")){
		          	        				for (Entry<String, String> entry1 : map.entrySet())
		        		          	        {
		          	        					if (entry1.getKey() == "from") {
		          	        						if(entry1.getValue().equals(nameId)){
		          	        							for (Entry<String, String> entry2 : map.entrySet())
		    		        		          	        {
		          	        								if (entry2.getKey() == "id") userkeyArray.add(entry2.getValue());
							          	        			System.out.println("USERKEYARRAY " + userkeyArray);
		    		        		          	        }
		          	        						}
		          	        					}
		        		          	        }	
		          	        		}
		          	        	}
		          	        	
		          	        } 

		        
		        ChatListAdapter customAdapter = new ChatListAdapter(InstantMessagesListDetail.this, nameId, mMessages);
		        customAdapter.notifyDataSetChanged();
		        lvChat.setAdapter(customAdapter);
		        

		        
		        new UpdateTaskMessage().execute();

	            // Dismiss the progress dialog
	            /*if (pDialog.isShowing())
	                pDialog.dismiss();*/
	        
		            } 
		    	});
		    }
		  

		}

		
		/** AsyncTask register message  */
	    private class RegisterTaskMessage extends AsyncTask<String, String, Boolean>{
			//String message;

			protected void onPreExecuted(){
				super.onPreExecute();

			}
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
			      
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

				// extract data
				TextView messageEditText = (TextView)findViewById(R.id.etMessage);
				String message = messageEditText.getText().toString();
				//String htmlStringMessage=Html.toHtml((Spanned) messageEditText.getText());
				  
				  boolean suc = false;

					   suc = MyProvider.createMessage(nameId, message);

	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
	        	// updating UI from Background Thread
	        	
	        	runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(InstantMessagesListDetail.this, getString(R.string.text_error), error_message);
			    		}
						else
						{
							Toast.makeText(InstantMessagesListDetail.this, getResources().getString(R.string.message_sent), Toast.LENGTH_SHORT).show();
							
							mMessages.clear();
							
							String body = etMessage.getText().toString();
							// Use Message model to create new messages now      
			                Message message = new Message(nameId, body, "", "");
			                message.setUserId(nameId);
			                message.setBody(body);
			                
			                etMessage.setText("");
			                InputMethodManager inputManager = 
			                        (InputMethodManager) getApplicationContext().
			                            getSystemService(Context.INPUT_METHOD_SERVICE); 
			                inputManager.hideSoftInputFromWindow(InstantMessagesListDetail.this.getCurrentFocus().getWindowToken(),
			                        InputMethodManager.HIDE_NOT_ALWAYS); 
			                
			                lvChat.setTranscriptMode(lvChat.TRANSCRIPT_MODE_ALWAYS_SCROLL);
			    	        lvChat.setStackFromBottom(true);
							
							// refresh
							new LoadMessagesItems().execute();	
				            
						}
					}
				});
     
		    }
	    }
	    
		/** AsyncTask update status message  */
	    private class UpdateTaskMessage extends AsyncTask<String, String, Boolean>{
	 
	        @Override
	        protected Boolean doInBackground(String... args) {	
				 
			      //String id = idMessage;
			      
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				  boolean suc = false;

				  for(String i : userkeyArray){
					  suc = MyProvider.updateMessageStatusRead(i);
		  }
					   

	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
	        	// updating UI from Background Thread
	        	
	        	runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (success == false)
		            	{
		            		 /*error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(InstantMessagesListDetail.this, getString(R.string.text_error), error_message);	*/    			 	 
			    		}
						else
						{
							//Toast.makeText(getActivity(), "Meesage update", Toast.LENGTH_SHORT).show();
						}
					}
				});
	 
		    }
	    }

}