package com.sharethatdata.digiportal;

import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.R;
import com.sharethatdata.digiportal.message.inbox.ListViewAdapter;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cNewsStatus;

import android.renderscript.Sampler.Value;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import androidx.fragment.app.Fragment;

/**
 * Created by mihu__000 on 7/30/2015.
 */
public class NewsFragment extends Fragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList;
	
	// List News Status
	ArrayList<HashMap<String, String>> newsListStatus;
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	
	private static ProgressDialog pDialog;
	
	public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    
    ImageView ivIcon;
    TextView tvItemName;
    
    private ActionBar actionBar;
    
    String idContact = "";
    
    String idNewsForStatus = "";
    
    private String error_message = "";
    
    ArrayList<HashMap<String, String>> keyArray = new ArrayList<HashMap<String, String>>();  
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_messages_inbox, container, false);
        setHasOptionsMenu(false);
        
        actionBar = getActivity().getActionBar();
        
        getActivity().setTitle("News");
        
        Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        
        lv = (ListView)rootView.findViewById(R.id.listView);
        
        SharedPreferences prefIdContact = getActivity().getSharedPreferences("PREF_ID_CONTACT", 0); 
        idContact = prefIdContact.getString("idContact", ""); 
        
        //new LoadListNews().execute();
        new LoadListNewsStatus().execute();
        

        /*tvItemName.setText(getArguments().getString(ITEM_NAME));
        ivIcon.setImageDrawable(rootView.getResources().getDrawable(
                    getArguments().getInt(IMAGE_RESOURCE_ID)));*/
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				// id for detail news
				HashMap<String, String> itemsList = (HashMap<String, String>) newsListStatus.get(position);
				 String itemId = (String) itemsList.get("id_news");
				 String itemIdStatus = (String) itemsList.get("id");
				 
				 Intent intent = new Intent(getActivity().getApplicationContext(), NewsDetail.class);
				 intent.putExtra("ID", itemId);
				 startActivity(intent);
				
				
				 idNewsForStatus = itemIdStatus;
				 new UpdateTaskNews().execute();
				 
				  Toast.makeText(getActivity().getApplicationContext(),
					      "Click List Item Number " + itemId, Toast.LENGTH_SHORT)
					      .show();
				
			}
		});

        return rootView;
    }
    
    @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     Log.d("TAG", "onCreate News");
	     //new LoadListNewsStatus().execute();
	     setHasOptionsMenu(false);
	 }
    
    @Override
    public void onResume(){
    	Log.d("TAG", "onResume News");
    	super.onResume();  	
    	setHasOptionsMenu(false); 
    	actionBar.removeAllTabs();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	    new LoadListNewsStatus().execute();
    }
    
    @Override
    public void onAttach(Activity activity){
 	   super.onAttach(activity);
    }
    
/*    
    @Override
	 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	     // TODO Add your menu entries here
	     super.onCreateOptionsMenu(menu, inflater);
	     
	     //menu.clear();
	     inflater.inflate(R.menu.refresh, menu);
	 }

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_refresh:
	         new LoadListNews().execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }*/
    
    
    
    /**
	 * Background Async Task to Load all News by making HTTP Request
  	 * */
/*	class LoadListNews extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading News");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cNews> news = MyProvider.getNewsItems();
	    	 for(cNews entry : news)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.title);
                map.put("description", entry.description);
                map.put("created", entry.created);
                 
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);

            *//**
             * Updating parsed JSON data into ListView
             * *//*

            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), itemList,
                    R.layout.fragment_listview_items, new String[] { "id", "title","created","description"}, new int[] { R.id.textViewID,R.id.textTitle, R.id.textDateTime, R.id.textDescription});

           lv.setAdapter(adapter);
	    	
	    	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, itemList);
	        lv.setAdapter(customAdapter);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();   
	    }
	}*/
	
	
	
	/**
	 * Background Async Task to Load all News for status by making HTTP Request
  	 * */
	class LoadListNewsStatus extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
    		Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
		    newsListStatus = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cNewsStatus> status = MyProvider.getNewsStatus(idContact);
	    	 for(cNewsStatus entry : status)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("id_news", entry.id_news);
                map.put("id_user", entry.id_user);
                map.put("status", entry.status);  
                map.put("title", entry.title);  
                map.put("description", entry.description);  
                map.put("created", entry.created);  
                 
                // adding HashList to ArrayList
                newsListStatus.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	    	// here you check the value of getActivity() and break up if needed
	        if(getActivity() == null)
	            return;
            /**
             * Updating parsed JSON data into ListView
             * */
	    	getActivity().runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	/*for(HashMap<String, String> map1 : itemList){
	            		for (Entry<String, String> entry1 : map1.entrySet())
	           	        {
	            			for(HashMap<String, String> map2 : newsListStatus){
	            				for(Entry<String, String> entry2 : map2.entrySet()){
	            					if(entry1.getValue().equals(entry2.getValue())){
	            						System.out.println("Equal");
	            						keyArray.add(map1);
	            						keyArray.add(map2);

	            						System.out.println("Equal " + keyArray.toString());
	            						//keyArray.add(entry.getValue());
	            						
	            						
	            					}
	            				}
	            			}
	           	        }
	            	}*/
	            	
	            	SharedPreferences.Editor editor = getActivity().getSharedPreferences("PREF_CHECK", 0).edit();
		   	   	     editor.putString("check", "news");
		   	   	     editor.commit();
	            	
	            	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, newsListStatus);
	    	        lv.setAdapter(customAdapter);
		            
	            }
	        });  
	    }
	}
	
	
	/** AsyncTask update status news  */
    private class UpdateTaskNews extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {	
			 
		      String id = idNewsForStatus;
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;

				   suc = MyProvider.updateNewsStatusRead(id);

        	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
        	// updating UI from Background Thread
        	
        	getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(getActivity(), getString(R.string.text_error), error_message);	    			 	 
		    		}
					else
					{
						//Toast.makeText(getActivity(), "News update id " + idNewsForStatus, Toast.LENGTH_SHORT).show();
			            
					}
				}
			});
 
	    }
    }
    
    
	
	
}
