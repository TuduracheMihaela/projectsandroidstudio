package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.InstantMessagesListDetail.LoadMessagesItems;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cLocationCurrent;
import com.sharethatdata.webservice.datamodel.cMessage;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LoginMyService extends Service{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	private String user = "";
	private String pass = "";
	private String myUser = "";
	private String myPass = "";
	private String userID = "";
	private String webservice_url = "";
	
	private String report_user = "";
	
	// List Messages
	ArrayList<HashMap<String, String>> locationsList;
	
	String idLocationOffice = "";
	String idLocationHome = "";
	String idLocationTravelling = "";
	String idLocationOther = "";

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
    
		 // Toast.makeText(MyService.this, "Service started!", Toast.LENGTH_SHORT).show();
		  
		  Log.d("TAG", "onStartCommand");
		  
		// Initializing
	        Globals = ((MyGlobals)getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        webservice_url = Globals.getValue("ws_url");
	        
	        if(myUser == ""){
	   	    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
	    	        user = prefUser.getString("USER", ""); 
	    	        pass = prefUser.getString("PASS", ""); 
	    	       
	   	    	Log.d("TAG", "user == null: " + user);
	   	    	Log.d("TAG", "pass == null: " + pass);
	   	    	
	   	    	MyProvider = new WSDataProvider(user, pass);
	   	       }else{
	   	    	SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
		   	     editor.putString("USER", String.valueOf(myUser));
		   	     editor.putString("PASS", String.valueOf(myPass));
		   	     editor.putString("URL", String.valueOf(webservice_url));
		   	     editor.commit();
		   	     
	   	    	Log.d("TAG", "myUser != null: " + myUser);
	   	    	Log.d("TAG", "myPass == null: " + myPass);

		        MyProvider = new WSDataProvider(myUser, myPass);
	   	       }
	        
	        
	        SharedPreferences sharedPrefsIdUser = getApplicationContext().getSharedPreferences("PREF_ID_CONTACT", Context.MODE_PRIVATE );
			String id_user = sharedPrefsIdUser.getString("idContact", "");
			userID = id_user;
			
			
	        new LoadLocationsList().execute();
	        
	        RegisterTaskLocation messageTask;
			messageTask = new RegisterTaskLocation();
			messageTask.execute();
			
			new LoadLocationItem().execute();
	  
	  //TODO do something useful
	  return Service.START_NOT_STICKY;
	  
  }
	
	
	
	*//**
	 * Background Async Task to Load all locations by making HTTP Request
  	 * *//*
	class LoadLocationsList extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
     }

    *//**
     * getting items from url
     * *//*
    protected String doInBackground(String... args) {
	   	    	
    	Globals = ((MyGlobals) getApplicationContext());	
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			locationsList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cUserLocation> list = MyProvider.getUserLocation();
    	
    		for(cUserLocation entry : list)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", Long.toString(entry.id));
                map.put("name", entry.name);
                
                if(entry.name.contains("Travelling")){
                	idLocationTravelling = Long.toString(entry.id);	
                }else if(entry.name.contains("In Office")){
                	idLocationOffice = Long.toString(entry.id);
                }else if(entry.name.contains("At Home")){
                	idLocationHome = Long.toString(entry.id);
                }else if(entry.name.contains("Other")){
                	idLocationOther = Long.toString(entry.id);
                }
                             
                // adding HashList to ArrayList
                locationsList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   *//**
    * After completing background task Dismiss the progress dialog
    * **//*
   protected void onPostExecute(String file_url) { 

    	}
	}
	
	
	*//** AsyncTask register message  *//*
    private class RegisterTaskLocation extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {

			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		        
		        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("defaults", Context.MODE_PRIVATE );
				String ssid_name = sharedPrefs.getString("prefNameSsid", "");

				 boolean suc = false;
				
				String ssid = null;
				  ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
				  // WIFI
				  NetworkInfo networkInfoWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				  if (networkInfoWifi.isConnected()) {
				    final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
				    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
				    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
				      ssid = connectionInfo.getSSID();
				      String ssidResult = ssid.replace("\"", "");
				      Log.d("LOGIN Wifi : ", "Connected : " + ssidResult);
				      
				      if(ssidResult.equals(ssid_name)){
				    	  Log.d("SSID Name Wifi : ", ssid_name);
				    	  Log.d("LOGIN Wifi : ", "i am in area wifi");

						   suc = MyProvider.createUserCurrentLocation(myUser, idLocationOffice);
				  	      
				      }else{
				    	  Log.d("LOGIN Wifi : ", "Connected: i am NOT in area wifi"); 

						   suc = MyProvider.createUserCurrentLocation( myUser, idLocationOther);
				      }
				      
				    }
				  }else{
					  Log.d("LOGIN Wifi : ", "Connected: I DON'T have wifi on"); 
					    
				  }
			  
			 
				  
				// Mobile Data
				  
				  if(connManager != null){
					  NetworkInfo networkInfoMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
					  if (networkInfoMobile != null && networkInfoMobile.isConnectedOrConnecting()) {
					    final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
					    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
					    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
					      ssid = connectionInfo.getSSID();
					      String ssidResult = ssid.replace("\"", "");
					      Log.d("LOGIN Mobile : ", "Connected : " + ssidResult);
					      
					      if(ssidResult.equals(ssid_name)){
					    	  Log.d("SSID Name : ", ssid_name);
					    	  Log.d("LOGIN Mobile : ", "i am in area wifi");
	
							   suc = MyProvider.createUserCurrentLocation( myUser, idLocationOther);
					  	      
					      }else{
					    	  Log.d("LOGIN Mobile : ", "Connected: i am NOT in area wifi"); 
	
							   suc = MyProvider.createUserCurrentLocation(myUser, idLocationOther);
					      }
					      
					    }
					  }else{
						  Log.d("LOGIN Mobile : ", "Connected: I DON'T have mobile data on"); 
					  }
				  }
				  

        	  return suc;
        }
           
        *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
        protected void onPostExecute(final Boolean success) {
        	
        	// updating UI from Background Thread
 
	    }
    }
    
    
    *//**
	 * Background Async Task to Load location by making HTTP Request
  	 * *//*
	class LoadLocationItem extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	final cLocationCurrent location = MyProvider.getUserCurrentLocation(userID);

        	SharedPreferences.Editor pref = getSharedPreferences("PREF_LOCATION", MODE_PRIVATE).edit();
        	pref.putString("location", location.location_name);
        	pref.commit();

	    	 return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String result) {
	    	super.onPostExecute(result);
	    }
	  

	}
	
    

}
*/