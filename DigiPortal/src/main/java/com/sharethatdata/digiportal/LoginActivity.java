package com.sharethatdata.digiportal;

import java.util.ArrayList;

import org.json.JSONArray;

import com.sharethatdata.registration.form.RegistrationActivity;
import com.sharethatdata.webservice.WSDataProvider;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
    MyGlobals Globals;
	
    WSDataProvider MyProvider = null;
    WSDataProvider MyProvider2 = null;
    
    JSONArray order = null;
    
    ArrayList<String> roles = null;

//	public String override_url = "";
	
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.11
	 */
//	private static final String[] DUMMY_CREDENTIALS = new String[] {
//			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
//	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	private UserRolesTask mRolesTask = null;
	
	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	private String myUser = "";
	private String myPass = "";

	private String webservice_url = "";
	
	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	
	private PendingIntent pendingIntent;
	
	public static final String BROADCAST = "com.sharethatdata.digiportal.LoginMyReceiver";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_new);
		
		// Set up the login form.
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		
		String current_user = sharedPrefs.getString("prefUsername", "");
		String current_password = sharedPrefs.getString("prefUserPassword", "");
		boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
	
		// get webservice url
		webservice_url = sharedPrefs.getString("prefWebServiceServer", "");
		
		myUser = current_user;
		myPass = current_password;
		
		boolean suc = InitApp();
		
		if (!suc) Toast.makeText(LoginActivity.this, "Something went wrong with initializing the app!", Toast.LENGTH_SHORT).show();
		
		// Set up the login form.
		mEmail = current_user;
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPassword = current_password;
		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setText(mPassword);
		
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						if(isNetworkAvailable() == true){
                            attemptLogin();
                        }else {
                            Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_SHORT).show();
                        }
					}
				});
		
		if (blAutoLogin)
		{
            if(isNetworkAvailable() == true){
                attemptLogin();
            }else {
                Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_SHORT).show();
            }
		}
		
		
		
		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifiManager.getConnectionInfo();
		String address = info.getMacAddress();
		    
		    System.out.println("Adresse MAC enabled: " + address);
		    
	    WifiManager wifiManagerSSID = (WifiManager) getApplicationContext().getSystemService (Context.WIFI_SERVICE);
	    WifiInfo infoSSID = wifiManagerSSID.getConnectionInfo ();
	    String ssid = infoSSID.getBSSID ();
	    
	    	System.out.println("Adresse SSID enabled: " + ssid);
	    	
    	/*IntentFilter intentFilter = new IntentFilter(BROADCAST);
    	registerReceiver( myReceiver , intentFilter);*/
	    	
    	/*Intent intent = new Intent(BROADCAST);  
        Bundle extras = new Bundle();  
        extras.putString("send_data", "test");  
        intent.putExtras(extras);  
        sendBroadcast(intent);*/
	    	
	    	
	    	
	    	//getCurrentSsid(LoginActivity.this);
		
		
	}
	
	/*private BroadcastReceiver myReceiver = new BroadcastReceiver() {

	        public void onReceive(Context context, Intent intent) {

	    		Log.d("LOGIN : ", "LoginMyReceiver");
	    		
	    		if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals (intent.getAction())) {
	    	        NetworkInfo netInfo = intent.getParcelableExtra (WifiManager.EXTRA_NETWORK_INFO);
	    	        if (ConnectivityManager.TYPE_WIFI == netInfo.getType ()) {
	    	        	System.out.println("Connected");
	    	        }
	    		}
	    }
	};*/
	
	/*public static String getCurrentSsid(Context context) {
		 String ssid = null;
		  ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		  if (networkInfo.isConnected()) {
		    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
		    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
		      ssid = connectionInfo.getSSID();
		      Log.d("LOGIN : ", "Connected: " + ssid);
		      
		      
		    }
		  }
		  return ssid;
		}*/
	
	private boolean InitApp()
	{
		Globals = ((MyGlobals)getApplication());
	
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", false);
		String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
		String endDay = sharedPrefs.getString("prefNotificationEndDay", "");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider2 = new WSDataProvider(myUser, myPass);
				
		if (webservice_url != "")
		{
			// store in globals
			Globals.setValue("ws_url", webservice_url);
			
			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider2.SetWebServiceUrl(webservice_url);
		}
	
		try
		{
			// read device values
			WifiManager wifiMan = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInf = wifiMan.getConnectionInfo();
			int ipAddress = wifiInf.getIpAddress();
			String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
			Globals.setValue("ip", ip);
			
			String android_id = Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID); 
			Globals.setValue("device", android_id);
			
			String appname = (String) getApplicationInfo().loadLabel(getBaseContext().getPackageManager());
			Globals.setValue("module", appname);
					
			MyProvider.SetDeviceIdentification(ip, android_id, appname);
			MyProvider2.SetDeviceIdentification(ip, android_id, appname);
			
			// add here also mac address
		
			return  true;
		} catch (Exception e) {
            e.printStackTrace();
            return false;
        }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
        	  Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        case R.id.action_create_user:
        	Intent intent_registration = new Intent(this, RegistrationActivity.class); startActivity(intent_registration); 
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	
	
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}
	
	protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			
			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl(webservice_url);
			boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "DigiPortal");
			if (suc)
			{				
				// store current user in globals
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("user", mEmail);
			    Globals.setValue("pass", mPassword);
			    
		//	    MyProvider = new IntraDataProvider(mEmail);
			    MyProvider.LogEvent(getString(R.string.text6_log_event));
			    // "user logged in."
			    
			    /*SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
		   	     editor.putString("USER", String.valueOf(myUser));
		   	     editor.putString("PASS", String.valueOf(myPass));
		   	     editor.putString("URL", String.valueOf(webservice_url));
		   	     editor.commit();*/
			 
			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				
				/*
				 * Name for profile
				 */
				SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
				SharedPreferences.Editor editor = pref.edit();
				editor.putString("Name",mEmail);
				editor.putString("Pass",mPassword);
				editor.apply();
						   
				// register roles
				mRolesTask = new UserRolesTask();
				mRolesTask.execute((Void) null);
				
				// call service to check new message and new news
				//MyProvider = new WSDataProvider(LoginActivity.this);
				//MyProvider.startServicefromNonActivity();
							
			    finish();
			    
				Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class); 
			 	startActivity(intent); 
				
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	/**
	 * Represents an asynchronous task used to check the roles of
	 * the user.
	 */
	public class UserRolesTask extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(Void... params) {
			
		    MyProvider2 = new WSDataProvider(mEmail, mPassword);
		    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
			roles = MyProvider2.getUserRoles(mEmail);
			
			if (roles != null)
			{
				MyGlobals Globals = ((MyGlobals)getApplication());
				
				if (roles.contains("Administrator"))
				{				
				    Globals.setValue("administrator", "yes");
				} else {
				    Globals.setValue("administrator", "no");
				}
				if (roles.contains("Developer"))
				{				
				    Globals.setValue("developer", "yes");
				} else {
				    Globals.setValue("developer", "no");
				}
				if (roles.contains("Manager"))
				{				
				    Globals.setValue("manager", "yes");
				} else {
				    Globals.setValue("manager", "no");
				}
				if (roles.contains("Employee"))
				{				
				    Globals.setValue("employee", "yes");
				} else {
				    Globals.setValue("employee", "no");
				}
			}
			
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			
			runOnUiThread(new Runnable() {
			      @Override
			          public void run() {
			    	  

			    	  	if (roles.size() == 0)
						{
							String data_error = MyProvider2.last_error;
							Toast.makeText(getApplication(), data_error, Toast.LENGTH_LONG).show();
						} 
		
			    	  	showProgress(false);

			      }
			});
					

			if (success) {
						   
			   
			} else {
				
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
}

