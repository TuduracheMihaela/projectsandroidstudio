package com.sharethatdata.digiportal.message.inbox;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.R;
import com.sharethatdata.digiportal.R.id;
import com.sharethatdata.digiportal.navdrawer.MyImageLoader;

import android.app.Activity;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapter extends ArrayAdapter<HashMap<String, String>>{
	    private ArrayList<HashMap<String, String>> MessageAdapter=new ArrayList<HashMap<String,String>>();
	 	private final Activity context;
	 	private final int layoutResourceId;
	 	String myContactLoggedID;

	    public ListViewAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> MessageAdapter)
	    {
	        super(context, layoutResourceId, MessageAdapter);
	        this.MessageAdapter=MessageAdapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        
	        SharedPreferences prefContactID = context.getSharedPreferences("PREF_ID_CONTACT", 0); 
	          String logged_id_user = prefContactID.getString("idContact", "");
	          myContactLoggedID = logged_id_user;

	    }
	  
	    @Override
	    public View getView(int position, View convertView, ViewGroup parrent)
	    {
	        HashMap<String, String>hashmap_Current;
	        View view=null;
	        convertView = null;
	        ViewHolder holder;
	        
	        if (convertView == null)
	        {
	            LayoutInflater layoutInflater=context.getLayoutInflater();
	            view=layoutInflater.inflate(layoutResourceId, null);
		        holder=new ViewHolder();
		        holder.lbl_MessageID = (TextView)view.findViewById(id.textViewID);
		        holder.lbl_MessageTitle = (TextView)view.findViewById(id.textTitle);        
		        holder.lbl_DateTimeAdd = (TextView)view.findViewById(id.textDateTime);
		        holder.lbl_Description = (TextView)view.findViewById(id.textDescription);
		        holder.imageContact = (com.sharethatdata.digiportal.navdrawer.CircularImageView)view.findViewById(id.imgContact);
		        view.setTag(holder);
	    }else{

	        holder=(ViewHolder)view.getTag();
	    }
	        hashmap_Current=new HashMap<String, String>();
	        hashmap_Current=MessageAdapter.get(position);

	        Log.e("Zdit", hashmap_Current.toString());
	        
	        holder.lbl_MessageID.setText(hashmap_Current.get("id").toString());       
 	        holder.lbl_MessageTitle.setText(Html.fromHtml(hashmap_Current.get("title")));       
 	        holder.lbl_DateTimeAdd.setText(Html.fromHtml(hashmap_Current.get("created")));       
 	        holder.lbl_Description.setText(Html.fromHtml(hashmap_Current.get("description")));
 	        SharedPreferences pref = context.getSharedPreferences("PREF_CHECK", 0); 
 	        String check = pref.getString("check", ""); 
 	        if(check.equals("news")){
 	        	holder.imageContact.setImageResource(R.drawable.news);
 	        	//loadMyImg.DisplayImage(hashmap_Current.get("image"), holder.imageContact); // image from database with base64
 	        }else if(check.equals("chat")){
 	        	 /*new DownloadImageTask((ImageView) holder.imageContact)
 	              .execute("http://dev-ws.sharethatdata.com/art/resources/resource.jpg");*/ // image path
 	        	
 	        	//loadMyImg.DisplayImage(hashmap_Current.get("image"), holder.imageContact); // image from database with base64
 	        	//holder.imageContact.setImageResource(R.drawable.user); 
 	        	
 	        	Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image"));
				  if (d != null)
				  {
					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
					  holder.imageContact.setImageBitmap(scaled);
					  				  
				  }else{
					  holder.imageContact.setImageResource(R.drawable.user); 
				  }
 	        }
 	        
	        
	        int myRow = 0;
	        
	      // for(int i=0 ; i<=MessageAdapter.get(position).size() ; i++){
	        for (Entry<String, String> entry1 : hashmap_Current.entrySet())
   	        {
	        	if (entry1.getKey() == "message_for_me")
   	        	{
	        		if(entry1.getValue().equals(myContactLoggedID))
	        		{
	        			 for (Entry<String, String> entry2 : hashmap_Current.entrySet())
	        	   	        {
	        				 if (entry2.getKey() == "status")
	 	       	        	  {
	 	        				try {
	 	       	        			myRow = Integer.parseInt(entry2.getValue().toString());
	 	       	        			String row = String.valueOf(myRow);
	 	       	        			
	 	       	        			if(row.contains("0")){
	 	       	        				// if was not read
	 	       	     	        	System.out.println("in if 0: " + myRow);
	 	       	     	        	holder.lbl_MessageTitle.setTypeface(null, Typeface.BOLD);
	 	       	     	        	holder.lbl_DateTimeAdd.setTypeface(null, Typeface.BOLD);
	 	       	     	        	holder.lbl_Description.setTypeface(null, Typeface.BOLD);
	 	       	     	        	
	 	       	        			}
		 	       	     	        if(row.contains("1")){
		 	       	     	        		// if was read
		 	       	     	        	System.out.println("in if 1: " + myRow);
		 	       	     	       	}
	 	       	        			
		 	       	        		} catch(NumberFormatException nfe) {
	
		 	       	        		}
	 	       	        		}
	        	   	        }
	        			 
	        			
	        		}
   	        	}
   	        	/*if (entry.getKey() == "status")
   	        	{
   	        		
   	        		try {
   	        			myRow = Integer.parseInt(entry.getValue().toString());
   	        			String row = String.valueOf(myRow);
   	        			
   	        			if(row.contains("0")){
   	        				// if was not read
   	     	        	System.out.println("in if 0: " + myRow);
   	     	        	holder.lbl_MessageTitle.setTypeface(null, Typeface.BOLD);
   	     	        	holder.lbl_DateTimeAdd.setTypeface(null, Typeface.BOLD);
   	     	        	holder.lbl_Description.setTypeface(null, Typeface.BOLD);
   	     	        	
   	        			}
   	     	        if(row.contains("1")){
   	     	        		// if was read
   	     	        	System.out.println("in if 1: " + myRow);
   	     	       	}
   	        			
   	        		} catch(NumberFormatException nfe) {

   	        		}

   	        	}*/
   	        }
	       //}

	        return view;
	    }

	    static class ViewHolder {

	        TextView lbl_MessageID, lbl_MessageTitle,lbl_DateTimeAdd,lbl_Description;
	        com.sharethatdata.digiportal.navdrawer.CircularImageView imageContact;

	    }
	    
	    private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}

}
