package com.sharethatdata.digiportal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.LoginActivity.UserLoginTask;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cNewsLastId;

import android.R.id;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

public class NewsCreateActivity extends FragmentActivity {
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;
	
	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	
	private String myUser = "";
	private String myPass = "";
	
	private String error_message = "";
	
	private String idContact = "";
	private String id = "";
	
	private static ProgressDialog pDialog;
	
	private String report_user = "";
	ArrayList<HashMap<String, String>> userList;
	List<String> userkeyArray =  new ArrayList<String>();
	
	Button btnCreate, btnEdit;
	
	ImageView imageView1;
	Button btnCreateImage;
	Button btnSearchImage;
	
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_create);
        
     // Initializing
        Globals = ((MyGlobals)getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        
        SharedPreferences pref = getSharedPreferences("PREF_ID_CONTACT", 0);
        idContact = pref.getString("idContact", "");
        String x = idContact;
        
        btnCreate = (Button) findViewById(R.id.buttonSubmit);
        btnEdit = (Button) findViewById(R.id.buttonEdit);
        
        imageView1 = (ImageView) findViewById(R.id.imageView1);
	    btnCreateImage = (Button) findViewById(R.id.btnCreateImage);
	    btnSearchImage = (Button) findViewById(R.id.btnSearchImage);
		
	    imageView1.setVisibility(View.GONE);
        
        String editNews = Globals.getValue("edit");
        if(editNews != ""){
        	String id = Globals.getValue("newsid");
        	String textTitle =  Globals.getValue("textTitle");
        	String descriptionText =  Globals.getValue("descriptionText");
        	String myImageData = Globals.getValue("myImageData");
    		
		    TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
		    titleEditText.setText(textTitle);

       	    TextView descriptionEditText = (TextView) findViewById(R.id.editTextDescription);
       	    descriptionEditText.setText(descriptionText);
       	    
       	    btnEdit.setVisibility(View.VISIBLE);
       	    btnCreate.setVisibility(View.GONE);
       	    
	       	 Bitmap d = ConvertByteArrayToBitmap(myImageData);
			  if (d != null)
			  {
				  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
				  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
				  imageView1.setVisibility(View.VISIBLE);
				  imageView1.setImageBitmap(scaled);
				  				  
			  }
        }else{
        	btnEdit.setVisibility(View.GONE);
        	btnCreate.setVisibility(View.VISIBLE);
        }
		
        
        
	}

			
	public void onCreateNews(View v){
		TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
	      String title = titleEditText.getText().toString();

  	    TextView descriptionEditText = (TextView) findViewById(R.id.editTextDescription);
	      String description = descriptionEditText.getText().toString();
	      
	      boolean cancel = false;
	      View focusView = null;

			// Check for title.
			if (TextUtils.isEmpty(title)) {
				titleEditText.setError(getString(R.string.error_field_required));
				focusView = titleEditText;
				cancel = true;
			}

			// Check for description.
			if (TextUtils.isEmpty(description)) {
				descriptionEditText.setError(getString(R.string.error_field_required));
				focusView = descriptionEditText;
				cancel = true;
			}

			if (cancel) {
				// There was an error; 
				focusView.requestFocus();
			} else {
				// call web method
				new RegisterTaskNews().execute();
			}
		
	}
	
	public void onEditNews(View v){
		TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
	      String title = titleEditText.getText().toString();

	    TextView descriptionEditText = (TextView) findViewById(R.id.editTextDescription);
	      String description = descriptionEditText.getText().toString();
	      
	      boolean cancel = false;
	      View focusView = null;

			// Check for title.
			if (TextUtils.isEmpty(title)) {
				titleEditText.setError(getString(R.string.error_field_required));
				focusView = titleEditText;
				cancel = true;
			}

			// Check for description.
			if (TextUtils.isEmpty(description)) {
				descriptionEditText.setError(getString(R.string.error_field_required));
				focusView = descriptionEditText;
				cancel = true;
			}

			if (cancel) {
				// There was an error; 
				focusView.requestFocus();
			} else {
				// call web method
				String id = Globals.getValue("newsid");
				new EditNewsTask(id).execute();
			}
	}
	
    private class EditNewsTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public EditNewsTask(String id) {
		     myID = id;
		  }

		  protected Boolean doInBackground(String... urls) {
        	
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			// extract data
	        	
        	  TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
		      String title = titleEditText.getText().toString();
 
        	  TextView descriptionEditText = (TextView) findViewById(R.id.editTextDescription);
		      String description = descriptionEditText.getText().toString();
		      
		      int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_news", bitmapByteArray, bitmapByteArrayThumbnail);
			  }		  
			  
			  suc = true;
			  
			  if (suc)
			  {
				  suc = MyProvider.updateNews(myID, idContact, title, description, "" + BitmapID);
			  }			  
		  
        	  return suc;
  			  
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "News updated", Toast.LENGTH_SHORT).show();
				  Globals.setValue("news_update", "news_update");
				  finish();
				  
			  }else{
				  Toast.makeText(getApplicationContext(), "Was not updated news", Toast.LENGTH_SHORT).show();
				  Globals.setValue("news_update", "news_update");
				  finish();
			  }
			  
			  
		  }
	}
	
	
	/** AsyncTask register news  */
    private class RegisterTaskNews extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {
        	
        	  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        		// extract data
        	
        	  TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
		      String title = titleEditText.getText().toString();
 
        	  TextView descriptionEditText = (TextView) findViewById(R.id.editTextDescription);
		      String description = descriptionEditText.getText().toString();
		      
		      int BitmapID = 0;
		      int NewsID = 0;
			  boolean sucImage = false;
			  boolean sucNews = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_news", bitmapByteArray, bitmapByteArrayThumbnail);
			  }		  
			  
			  sucImage = true;
			  
			  if (sucImage)
			  {
				  NewsID = MyProvider.createNews(idContact, title, description, "" + BitmapID);
				  id = String.valueOf(NewsID);
			  }			  
		  
        	  return sucImage;

        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
        	// updating UI from Background Thread
        	
        	runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(NewsCreateActivity.this, getString(R.string.text_error), error_message);
		    			 
		    			 finish();
		    		}
					else
					{
						Toast.makeText(NewsCreateActivity.this, "News created", Toast.LENGTH_SHORT).show(); 
						 new LoadUserList().execute();			            
					}
				}
			});
 
	    }
    }
	
	/** AsyncTask register news  */
    private class RegisterTaskNewsStatusRead extends AsyncTask<String, String, Boolean>{
    	
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
         // Showing progress dialog
            pDialog = new ProgressDialog(NewsCreateActivity.this);
            pDialog.setTitle("Creating news");
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
 
        @Override
        protected Boolean doInBackground(String... args) {
        	
        		// extract data
	
	
		      // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;
			  
			  /*for (HashMap<String, String> map : userList) {
				  for (Entry<String, String> entry : map.entrySet())
        	        {*/
				  //suc = 
					  	//if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
					  	//String i = entry.getValue();
			  
			  for(String i : userkeyArray){
					suc = MyProvider.createNewsStatusRead( id, i, "0");
			  }
        	/*        }
			}*/
        	 // return suc;
			return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean result) {
        	runOnUiThread(new Runnable() {
				public void run() {
					
					if(result == true){
						Toast.makeText(NewsCreateActivity.this, "News status created", Toast.LENGTH_SHORT).show();
						
						pDialog.dismiss();
						
						finish();
						
						Globals.setValue("news_create", "news_create");
					}else{
						Toast.makeText(NewsCreateActivity.this, "News status was not created", Toast.LENGTH_SHORT).show();
						
						pDialog.dismiss();
						
						finish();
					}
					
					// Dismiss the progress dialog
		            if (pDialog.isShowing())
		                pDialog.dismiss();
				}
			});
 
	    }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////
    
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         // Showing progress dialog
         pDialog = new ProgressDialog(NewsCreateActivity.this);
         pDialog.setTitle("Creating news");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.setCanceledOnTouchOutside(false);
         pDialog.show();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	Globals = ((MyGlobals) getApplicationContext());	
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			//userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider.getContacts();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                //HashMap<String, String> map = new HashMap<String, String>();

               // map.put("pid", entry.id);              
                // adding HashList to ArrayList
                
    			if(idContact.equals(entry.id)){
    				MyProvider.createNewsStatusRead( id, entry.id, "1");
    				
    				 TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
    			     String title = titleEditText.getText().toString();
    				//MyProvider.createStatusNotification(id, entry.id, title, "", "news");
    			}else{
    				MyProvider.createNewsStatusRead( id, entry.id, "0");
    				
    				 TextView titleEditText = (TextView) findViewById(R.id.editTextTitle);
   			     	 String title = titleEditText.getText().toString();
    				//MyProvider.createStatusNotification(id, entry.id, title, "", "news");
    			}
                
                
               // userList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) { 
	   runOnUiThread(new Runnable() {
           public void run() {
        	   
					Toast.makeText(NewsCreateActivity.this, "News status created", Toast.LENGTH_SHORT).show();
					
					pDialog.dismiss();
					
					finish();
					
					Globals.setValue("news_create", "news_create");

        	   

           	/*for (HashMap<String, String> map : userList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
          	        }*/  
           	
           	//new RegisterTaskNewsStatusRead().execute();
           		}
       		});
    	}
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// when get and show the image
		private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
		
		// button create image
		public void onCreateImage(View v){
			Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();
			
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		    
		    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
		    	  // Create the File where the photo should go
		        File photoFile = null;
		        try
		        {
		            photoFile = createImageFile();
		        } 
		        catch (IOException ex) 
		        {

		        }

		        if (photoFile != null) 
		        {
		            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
		                    Uri.fromFile(photoFile));
		            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
		        }

		        //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		    }
		}
		private File createImageFile() throws IOException {
		    // Create an image file name
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		    String imageFileName = "JPEG_" + timeStamp + "_";
		    File storageDir = Environment.getExternalStoragePublicDirectory(
		            Environment.DIRECTORY_PICTURES);
		    File image = File.createTempFile(
		        imageFileName,  /* prefix */
		        ".jpg",         /* suffix */
		        storageDir      /* directory */
		    );

		    // Save a file: path for use with ACTION_VIEW intents
		    mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		    mCurrentPhotoPathAbsolute = image.getAbsolutePath();
		    return image;
		}
		
		// button search image
		public void onSearchImage(View v){
			Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
			
			Intent intent = new Intent();

			intent.setAction(Intent.ACTION_GET_CONTENT);

			intent.addCategory(Intent.CATEGORY_OPENABLE);

			intent.setType("image/*");

			startActivityForResult(intent, REQUEST__BROWSE_PHOTO);
		}
		
		// Methods - set visible the preview image
			private void AttachBitmapToReport()
			{
			    int photoW = myBitmap.getWidth();
			    int photoH = myBitmap.getHeight();   	
			    int scaleFactor = Math.min(photoW/100, photoH/100);
			    int NewW = photoW/scaleFactor;
			    int NewH = photoH/scaleFactor;
			    
				imageView1.setVisibility(View.VISIBLE);		
		    	imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
			}
			
			
			@SuppressLint("MissingSuperCall")
			@Override
			protected void onActivityResult(int requestCode, int resultCode, Intent data) 
			{
				InputStream stream = null;

			    if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
			    {
			    	myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			    	AttachBitmapToReport();
			    }
			    	    
		        if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK) 
		        {
		        	
		            try 
		            {
		            	stream = getContentResolver().openInputStream(data.getData());
		            	myBitmap = BitmapFactory.decodeStream(stream);
		            	AttachBitmapToReport();
		            } 
		            catch (Exception e)
		            {
		            	e.printStackTrace();
		            }

		            if (stream != null) 
		            {
		            	try
		            	{
		            		stream.close();
		            	} 
		            	catch (Exception e) 
		            	{
		            		e.printStackTrace();
		            	}
		            }            
		        }
			}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
}
