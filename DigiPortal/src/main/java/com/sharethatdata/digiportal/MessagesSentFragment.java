package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sharethatdata.digiportal.MessagesInboxFragment.LoadListMessages;
import com.sharethatdata.digiportal.message.inbox.ListViewAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cMessage;


*//**
 * Created by mihu__000 on 7/30/2015.
 *//*
public class MessagesSentFragment extends Fragment {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList;
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	
	private static ProgressDialog pDialog;
	
	public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    
    ImageView ivIcon;
    TextView tvItemName;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_messages_sent, container, false);
        
        
        Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        
        lv = (ListView)rootView.findViewById(R.id.listView);
        
	     new LoadListMessages().execute();

        //tvItemName.setText(getArguments().getString(ITEM_NAME));
        ivIcon.setImageDrawable(rootView.getResources().getDrawable(
                    getArguments().getInt(IMAGE_RESOURCE_ID)));
        
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				HashMap<String, String> itemsList = (HashMap<String, String>) itemList.get(position);
				 String item = (String) itemsList.get("id");
				 
				 Intent intent = new Intent(getActivity().getApplicationContext(), MessagesDetail.class);
				 intent.putExtra("ID", item);
				 intent.putExtra("IndexTab", "Sent");
				 startActivity(intent);
				
				
				  Toast.makeText(getActivity().getApplicationContext(),
					      "Click ListItem Number " + item, Toast.LENGTH_SHORT)
					      .show();
				
			}
		});
        


        return rootView;
    }
    
    
    @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     Log.d("TAG", "onCreate Sent");
	     setHasOptionsMenu(true);    
	 }
    
    @Override
	public void onResume(){
		super.onResume();
		Log.d("TAG", "onResume Sent");
		setHasOptionsMenu(true);  
		new LoadListMessages().execute();

	}
    
    @Override
  	public void onPause() {
     	super.onPause();
     	Log.d("TAG", "onPause Sent");
     	
     }
    
    @Override
	public void onDestroy() {
    	super.onDestroy();
    	Log.d("TAG", "onDestroy Sent");
    }
    
    public void myRefresh(){
 	   Log.d("TAG", "onResume Sent");
 	   new LoadListMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_refresh:
	         new LoadListMessages().execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }
	 
    
    *//**
	 * Background Async Task to Load all holidays by making HTTP Request
  	 * *//*
	class LoadListMessages extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading Messages Sent");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cMessage> message = MyProvider.getMessagesSent();
	    	 for(cMessage entry : message)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.user_from);
                map.put("created", entry.datetime);
                map.put("description", entry.message);
    
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);

            *//**
             * Updating parsed JSON data into ListView
             * *//*

            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), itemList,
                    R.layout.fragment_listview_items, new String[] { "id", "title","created","description"}, new int[] { R.id.textViewID,R.id.textTitle, R.id.textDateTime, R.id.textDescription});

           lv.setAdapter(adapter);
	    	
	    	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, itemList);
	        lv.setAdapter(customAdapter);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        
	  	      
	    }
	  

	}
	
    
}
*/