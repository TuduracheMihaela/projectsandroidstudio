package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiportal.UserProfileDetail.LoadUser;
import com.sharethatdata.digiportal.chat.UsersAdapter;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cVisitEntries;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class UsersListActivity extends Activity{
	
	// Progress Dialog
    private ProgressDialog pDialog;

	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> userList;
	
	ListView lv = null;
	
	private String myUser = "";
    private String myPass = "";
    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_users);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    lv = (ListView) findViewById(R.id.listView);
	
	    lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String idUser = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
				
			    // start detail activity
				Intent intent = new Intent(getApplicationContext(), UserProfileDetail.class); 
				intent.putExtra("idUser", idUser);
				startActivityForResult(intent, 1);
				
			}

		});	
	    
	    new LoadList().execute();
    }
    
 // after edit or delete, load data again
 	@Override 
 	protected void onActivityResult(int requestCode, int resultCode, Intent data) {     
 	  super.onActivityResult(requestCode, resultCode, data); 
	   	  if (resultCode == Activity.RESULT_OK) { 
		    	  new LoadList().execute();
	   	  }
 	}
    
    public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:   
			   finish();
			   		super.onBackPressed();
		        return true;
		   case R.id.action_new: 
	            Intent intent = new Intent(this, UserProfileDetailEditActivity.class); 
	            startActivityForResult(intent, 3);
	            return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
			getMenuInflater().inflate(R.menu.contacts, menu);
		
		return true;
	}
    
	/**
	 * Background Async Task Users by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(UsersListActivity.this);
         pDialog.setMessage( getString(R.string.dialog_loading_wait));
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	userList = new ArrayList<HashMap<String, String>>();

	    		// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getContactsWithThumbImage();
	    	
	    		for(cContact entry : List)
             	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("id", entry.id);
	                map.put("name", entry.name);
	                map.put("subtitle", entry.function);
	                map.put("status", entry.status_id);
	                map.put("image_id", entry.image_id);
	                 
	                // adding HashList to ArrayList
	                userList.add(map);
	             }
	 
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	        pDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	lv.setAdapter(new UsersAdapter(UsersListActivity.this, R.layout.activity_list_users_items, userList, "users_list"));

	            }
	        });
	         
	    }

	}

}
