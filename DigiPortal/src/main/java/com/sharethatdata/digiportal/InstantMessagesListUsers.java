package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.chat.UsersAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;


public class InstantMessagesListUsers extends FragmentActivity {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	private static ProgressDialog pDialog;
	
	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> userList;
	List<String> userkeyArray =  new ArrayList<String>();
	List<String> userArray;
	
	private String myUser = "";
	private String myPass = "";
	// vars for remember report
    private String report_user = "";
    
    private String error_message = "";

    private ListView listView;

	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.chat_fragment_listview_users);
	        
	        setTitle("Select user");
	        
	        Globals = ((MyGlobals)getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);

	        listView = (ListView) findViewById(R.id.listView);
	        
	        new LoadUserList().execute();
	        
	        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
					HashMap<String, String> itemsList = (HashMap<String, String>) userList.get(position);
					 String itemId = (String) itemsList.get("id");
					 String itemName = (String) itemsList.get("name");
					 
					 System.out.println(itemId);
	                 System.out.println(itemName);
					
					Intent intent = new Intent(InstantMessagesListUsers.this, InstantMessagesListDetail.class);
					intent.putExtra("NameId", itemId);
					intent.putExtra("Name", itemName);
					startActivity(intent);
					
					finish();
					
				}
			});
	        
	 }
	 

	 /**
		 * Background Async Task to Load all users by making HTTP Request
	  	 * */
		class LoadUserList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	 
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
		   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				userList = new ArrayList<HashMap<String, String>>();
				
				// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getContacts();
	    	
	    		for(cContact entry : List)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("id", entry.id);
	                map.put("name", entry.name);
	                map.put("function", entry.function);
	                map.put("image_id", entry.image_id);
	          
	                // adding HashList to ArrayList
	                userList.add(map);
	             }
				
				report_user = myUser;
			}
	    	
			return "";
	   }

	   /**
	    * After completing background task Dismiss the progress dialog
	    * **/
	   protected void onPostExecute(String file_url) {
	       // dismiss the dialog after getting all products

	   		// updating UI from Background Thread
	       runOnUiThread(new Runnable() {
	           public void run() {
	        	   
	        	   ListAdapter adapter = null;
	    	     /*userArray =  new ArrayList<String>();
	           	for (HashMap<String, String> map : userList)
	          	        for (Entry<String, String> entry : map.entrySet())
	          	        {
	          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
	          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
	          	        }*/
	           	
	           	/*adapter = new SimpleAdapter(
                		InstantMessagesListUsers.this
                		, userList
                		, R.layout.chat_fragment_listview_users_items, new String[] { "name", "function", "id" },
                        new int[] { R.id.textTitle, R.id.textFunction ,R.id.textViewID });
	           		listView.setAdapter(adapter);*/
	        	   
	        	   listView.setAdapter(new UsersAdapter(InstantMessagesListUsers.this, R.layout.chat_fragment_listview_users_items, userList, "chat_users_list"));
	      	    	
	        	   
	           }
	       	});
	 	      
	    	}
		}
}


