package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.sharethatdata.digiportal.R;
import com.sharethatdata.digiportal.MessagesSentFragment.LoadListMessages;
import com.sharethatdata.digiportal.message.inbox.ListViewAdapter;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cNews;

import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

*//**
 * Created by mihu__000 on 7/30/2015.
 *//*
public class MessagesInboxFragment extends Fragment{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	
	ArrayList<HashMap<String, String>> itemList;
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	
	private static ProgressDialog pDialog;
	
	public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    
    ImageView ivIcon;
    TextView textTitle;
    
    String statusRead = "";
    String userName = "";
    
    int indexTab = 0;
    
    String idMessage = "";
    
    private String error_message = "";
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_messages_inbox, container, false);
        
        
        Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        
        lv = (ListView)rootView.findViewById(R.id.listView);
        textTitle = (TextView)rootView.findViewById(R.id.textTitle);

        new LoadListMessages().execute();

        //tvItemName.setText(getArguments().getString(ITEM_NAME));
        ivIcon.setImageDrawable(rootView.getResources().getDrawable(
                    getArguments().getInt(IMAGE_RESOURCE_ID)));
    	
        
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				HashMap<String, String> itemsList = (HashMap<String, String>) itemList.get(position);
				 String itemId = (String) itemsList.get("id");
				 String itemName = (String) itemsList.get("title");
				 
				 Intent intent = new Intent(getActivity().getApplicationContext(), MessagesDetail.class);
				 intent.putExtra("ID", itemId);
				 intent.putExtra("Name", itemName);
				 intent.putExtra("IndexTab", "Inbox");
				 startActivity(intent);
				
				 // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);	
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				 
				  idMessage = itemId;
				  new UpdateTaskMessage().execute();
				
				  Toast.makeText(getActivity().getApplicationContext(),
					      "Message: " + itemId + " Position: " + position + " Name: " + itemName, Toast.LENGTH_SHORT)
					      .show();
				
			}
		});
    
        return rootView;
    }
    
    
    @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     Log.d("TAG", "onCreate Inbox");
	     setHasOptionsMenu(true);
	 }
   
   @Override
	public void onResume(){
		super.onResume();
		Log.d("TAG", "onResume Inbox");
		setHasOptionsMenu(true);  
		new LoadListMessages().execute();
		// Toast.makeText(getActivity(), "onResume Inbox", Toast.LENGTH_SHORT).show();

	}
   
   @Override
  	public void onPause() {
     	super.onPause();
     	Log.d("TAG", "onPause Inbox");

     }
   
   @Override
	public void onDestroy() {
	   	super.onDestroy();
	   	Log.d("TAG", "onDestroy Inbox");
   }
   
   @Override
   public void onAttach(Activity activity){
	   super.onAttach(activity);
   }
   
   public void myRefresh(){
	   Log.d("TAG", "onResume Inbox");
	   Globals = ((MyGlobals)getActivity().getApplicationContext());
       myUser = Globals.getValue("user");
       MyProvider = new WSDataProvider(myUser, myPass);
	   new LoadListMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
   }

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_refresh:
	         new LoadListMessages().execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }
	 
    
    *//**
	 * Background Async Task to Load all holidays by making HTTP Request
  	 * *//*
	class LoadListMessages extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading Messages Inbox");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cMessage> message = MyProvider.getMessagesInbox();
	    	 for(cMessage entry : message)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.user_from);
                map.put("created", entry.datetime);
                map.put("description", entry.message);
                map.put("status", entry.status_read);
                
                statusRead = entry.status_read;
    
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String result) {
	    	 // here you check the value of getActivity() and break up if needed
	        if(getActivity() == null)
	            return;
            *//**
             * Updating parsed JSON data into ListView
             * *//*
	    	getActivity().runOnUiThread(new Runnable() {
	            public void run() {

	            	int total = 0;
	            	int total_unread = 0;
	            	int total_read = 0;
	            	int myRow = 0;
	            	for (HashMap<String, String> map : itemList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "status")
	           	        	{
	           	        		
	           	        		try {
	           	        			myRow = Integer.parseInt(entry.getValue().toString());
	           	        			String row = String.valueOf(myRow);
	           	        			
	           	        			if(row.contains("0")){
	           	     	        	System.out.println("hello in if 0: " + myRow);
	           	     	       		//textTitle.setTypeface(null, Typeface.BOLD);
	           	     	       		//textTitle.setText("#000");
	           	     	        	//lv.setBackgroundResource(R.color.orange5);
	           	     	        	total_unread++;
	           	     	       	}
	           	     	        if(row.contains("1")){
	           	     	        	System.out.println("hello in if 1: " + myRow);
	           	     	       		//textTitle.setTypeface(null, Typeface.BOLD);
	           	     	       		//textTitle.setText("#000");
	           	     	        	//lv.setBackgroundResource(R.color.yellow1);
	           	     	        	total_read++;
	           	     	       	}
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myRow = 0;
	           	        		}
	           	        		total = total_unread + total_read;
	           	        	}
	           	        }
	            	System.out.println("Total read: " + total_read);
	           		System.out.println("Total unread: " + total_unread);
	           		System.out.println("Total : " + total);

	           		SharedPreferences.Editor editor = getActivity().getSharedPreferences("PREF_TOTAL_UNREAD_MESSAGES", 0).edit();
		   	   	     editor.putString("unreadMessages", String.valueOf(total_unread));
		   	   	     editor.commit();
	            }
	        });
	        
	    	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, itemList);
	        lv.setAdapter(customAdapter);
	    	
            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), itemList,
                    R.layout.fragment_listview_items, new String[] { "id", "title","created","description"}, new int[] { R.id.textViewID,R.id.textTitle, R.id.textDateTime, R.id.textDescription});

           lv.setAdapter(adapter);
           
           

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        
	  	      
	    }
	  

	}
	
	
	*//** AsyncTask update status message  *//*
    private class UpdateTaskMessage extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {	
			 
		      String id = idMessage;
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;

				   suc = MyProvider.updateMessageStatusRead(id);

        	  return suc;
        }
           
        *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
        protected void onPostExecute(final Boolean success) {
        	
        	// updating UI from Background Thread
        	
        	getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(getActivity(), getString(R.string.text_error), error_message);	    			 	 
		    		}
					else
					{
						//Toast.makeText(getActivity(), "Meesage update", Toast.LENGTH_SHORT).show();
					}
				}
			});
 
	    }
    }
    
	
    
}


*/