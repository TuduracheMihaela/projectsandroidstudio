package com.sharethatdata.digiportal;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cHolidays;
import com.sharethatdata.webservice.datamodel.cNews;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class DashBoardFragment extends Fragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	private static ProgressDialog pDialog;
	
	private String myUser = "";
	private String myPass = "";
    
    ImageView picCompany;
    TextView txtNameCompany;
    TextView txtAddress;
    TextView txtFax;
    TextView txtTelephone;
    TextView txtEmail;
    TextView txtWebsite;
    
    TextView lblAddress;
    TextView lblFax;
    TextView lblTelephone;
    TextView lblEmail;
    TextView lblWebsite;
    
    private ActionBar actionBar;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        
        actionBar = getActivity().getActionBar();
        
        Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);	
        
        picCompany = (ImageView) rootView.findViewById(R.id.picture_company);
        txtNameCompany = (TextView) rootView.findViewById(R.id.name_company);
        txtAddress = (TextView) rootView.findViewById(R.id.address_company);
        txtFax = (TextView) rootView.findViewById(R.id.fax_company);
        txtTelephone = (TextView) rootView.findViewById(R.id.telephone_company);
        txtEmail = (TextView) rootView.findViewById(R.id.email_company);
        txtWebsite = (TextView) rootView.findViewById(R.id.website_company);
        
        lblAddress = (TextView) rootView.findViewById(R.id.address_company_label);
        lblFax = (TextView) rootView.findViewById(R.id.fax_company_label);
        lblTelephone = (TextView) rootView.findViewById(R.id.telephone_company_label);
        lblEmail = (TextView) rootView.findViewById(R.id.email_company_label);
        lblWebsite = (TextView) rootView.findViewById(R.id.website_company_label);
        
        new LoadCompanyItem().execute();

        /*txtNameCompany.setText("RoboPharma");
        picCompany.setImageDrawable(rootView.getResources().getDrawable(R.drawable.logo_rp));
        
        lblAddress.setText("Addresse:");
        txtAddress.setText("Havenweg 14 5145 \nNJ Waalwijk \nPostbus 653 \n5140 AR Waalwijk \nThe Netherlands");
        txtAddress.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        
        lblTelephone.setText("Telephone:");
        txtTelephone.setText("+31416347172");
        txtTelephone.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        
        lblEmail.setText("E-mail:");
        txtEmail.setText("sales@robopharma.com");
        txtEmail.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        
        lblWebsite.setText("Website:");
        txtWebsite.setText("www.robopharma.com");
        txtWebsite.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);*/
        
        txtAddress.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent addressIntent = new Intent(Intent.ACTION_VIEW);
				//addressIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				addressIntent.setData(Uri.parse("geo:0,0?q="+txtAddress.getText().toString().trim())); //lat lng or address query
				if (addressIntent.resolveActivity(getActivity().getPackageManager()) != null) {
				    startActivity(addressIntent);
				}
				
			}
		});
        
        txtTelephone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent callIntent = new Intent(Intent.ACTION_CALL);
				 callIntent.setData(Uri.parse("tel:" + txtTelephone.getText().toString().trim()));
				 startActivity(callIntent);
			}
		});
        
        txtEmail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto" ,txtEmail.getText().toString().trim(), null)); 
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject text here");
				emailIntent.putExtra(Intent.EXTRA_TEXT,"Body text here");

				startActivity(Intent.createChooser(emailIntent, "Send email..."));
				
			}
		});
        
        txtWebsite.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent websiteIntent = new Intent(Intent.ACTION_VIEW);
				 websiteIntent.setData(Uri.parse("http://"+txtWebsite.getText().toString().trim()));
				 startActivity(websiteIntent);
				
			}
		});

        return rootView;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }
    
    @Override
    public void onAttach( Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(false); 
    }
    
    @Override
    public void onResume(){
    	Log.d("TAG", "onResume News");
    	super.onResume();  	
    	setHasOptionsMenu(false); 
    	actionBar.removeAllTabs();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

/*    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        int action = 0;
        action = R.id.action_refresh;
        System.out.println("action: " + action);
        
        if(action == 2131427374){
        	MenuItem item  = menu.findItem(R.id.action_refresh);
        	item.setVisible(false);
        }
    }*/
    
    
    /**
	 * Background Async Task to Company Info by making HTTP Request
  	 * */
	class LoadCompanyItem extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         /*pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading Info");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();*/
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	final cCompany company = MyProvider.getCompanyInfo("1");
	    	
	    	getActivity().runOnUiThread(new Runnable() {
	               @Override
	               public void run() {
	            	   txtNameCompany.setText(company.name);
	                   picCompany.setImageDrawable(getView().getResources().getDrawable(R.drawable.logo_rp));
	                   
	                   lblAddress.setText("Addresse:");
	                   txtAddress.setText(company.address + " " + company.postcode + "\n" + company.city + "\n" + company.address_mail + "\n"
	                		   + company.postcode_mail + " " + company.city_mail + "\n" + "The " + company.country);
	                   txtAddress.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
	                   
	                   //txtAddress.setText("Havenweg 14 5145 \nNJ Waalwijk \nPostbus 653 \n5140 AR Waalwijk \nThe Netherlands");
	                   
	                   lblTelephone.setText("Telephone:");
	                   txtTelephone.setText(company.phone);
	                   txtTelephone.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
	                   
	                   lblFax.setText("Fax:");
	                   txtFax.setText(company.fax);
	                   
	                   lblEmail.setText("E-mail:");
	                   txtEmail.setText(company.email);
	                   txtEmail.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
	                   
	                   lblWebsite.setText("Website:");
	                   txtWebsite.setText(company.website);
	                   txtWebsite.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);     
	               }
	            });
	    	 
	    	
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);

            // Dismiss the progress dialog
            /*if (pDialog.isShowing())
                pDialog.dismiss();*/      
	    }
	}

	
}
