package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiportal.MessagesInboxFragment.LoadListMessages;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MessagesComposeFragment extends Fragment {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	Spinner userSpinner = null;
	
	List<String> userkeyArray =  new ArrayList<String>();
	ArrayList<HashMap<String, String>> userList;
	List<String> userArray;
	
	private boolean userIsInteracting = false;
    private int userSpinnerIndex = -1;
    
    private String myUser = "";
    private String myPass = "";
    private String report_user = "";
    private String user_name = "";
    
    private String error_message = "";

    private EditText editText;
	private Button btnSubmit;
	

	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

	        final View rootView = inflater.inflate(R.layout.fragment_messages_compose, container, false);
	        
	        Globals = ((MyGlobals)getActivity().getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        
	        userSpinner = (Spinner) rootView.findViewById(R.id.spinnerUser);
	        editText = (EditText) rootView.findViewById(R.id.editTextMessage);
	        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);
	        
	        LoadUserList InitUserList;
			
			InitUserList = new LoadUserList();
			InitUserList.execute();
			
			
			
	        
	        
			
			userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
						int position, long id) {
					
					
					Spinner userSpinner = (Spinner) rootView.findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user = userkeyArray.get(myIndex);
		  	   		user_name = userArray.get(myIndex);
		  	   
		  	   		userSpinnerIndex = myIndex;
		  	   		
					System.out.println("Selected: " + position); // get id position from spinner
					System.out.println("Selected: " + myIndex); // get id position from spinner
					System.out.println("Selected: " + report_user); // get id user for compose message
					System.out.println("Selected: " + user_name); // get user name for compose message
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
			
			 // Clear text when clicked
			editText.setOnClickListener(new OnClickListener() {
		        @Override
		        public void onClick(View v) {
		        	editText.setText("");
		        }
		    });
			
			btnSubmit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					RegisterTaskMessage messageTask;
					messageTask = new RegisterTaskMessage();
					messageTask.execute();
				} });
	        
	        return rootView;
	 }
	 
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     Log.d("TAG", "onCreate Compose");
	 }
	 
	 @Override
	 public void onResume(){
		 super.onResume();
		 editText.setText("");
		// Toast.makeText(getActivity(), "onResume Compose", Toast.LENGTH_SHORT).show();
	 }
	 
	 public void myRefresh(){
		   Log.d("TAG", "onResume Compose");
		   EditText editTextOnResume;
		   editTextOnResume = (EditText) getView().findViewById(R.id.editTextMessage);
		   editTextOnResume.setText("");
	   }
	 
	 
		*//**
		 * Background Async Task to Load all users by making HTTP Request
	  	 * *//*
		class LoadUserList extends AsyncTask<String, String, String> {

	     *//**
	      * Before starting background thread Show Progress Dialog
	      * *//*
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	 
	     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
		   	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				userList = new ArrayList<HashMap<String, String>>();
				
				// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getContacts();
	    	
	    		for(cContact entry : List)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("pid", entry.id);
	                map.put("name", entry.name);
	          
	                // adding HashList to ArrayList
	                userList.add(map);
	             }
				
				report_user = myUser;
			}
	    	
			return "";
	   }

	   *//**
	    * After completing background task Dismiss the progress dialog
	    * **//*
	   protected void onPostExecute(String file_url) {
	       // dismiss the dialog after getting all products

	   		// updating UI from Background Thread
	       getActivity().runOnUiThread(new Runnable() {
	           public void run() {
	           
	    	     userArray =  new ArrayList<String>();
	           	for (HashMap<String, String> map : userList)
	          	        for (Entry<String, String> entry : map.entrySet())
	          	        {
	          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
	          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
	          	        }
	           	
	           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, userArray);
	      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      	    	Spinner sUsers = (Spinner) getActivity().findViewById(R.id.spinnerUser);
	      	    	sUsers.setAdapter(user_adapter);
	      	    	
	      	    	if(userSpinnerIndex != -1) {
	      	    	  // set the value of the spinner 
	      	    		sUsers.setSelection(userSpinnerIndex);
	      	    	}
	      	    	
	      	    	
	      	    	// get name from reply and set name to spinner
	      	    	SharedPreferences pref = getActivity().getSharedPreferences("PREF_REPLY", 0); 
	    	        int i = pref.getInt("ComposeTab", 0);
	    	        String name = pref.getString("Name", "");
	    	        String description = pref.getString("Description", "");
	    	        String reply = "Reply ";
	    	        if(i == 2){
	    		    		int spinnerPosition = user_adapter.getPosition(name);
	    		    		//set the default according to value
	    		    		sUsers.setSelection(spinnerPosition);
	    		    		
	    		    		//editText.setText("Reply " + '"' + description + '"' + ": ");
	    		    		editText.setText(Html.fromHtml("<font color='#980000'><b>" + reply + "</b>" +  "<br />" + 
	    		    	            "<small>" + '"' + description + '"' + ": " + "</small></font>" + "<br />"));
	    		    		editText.setSelection(editText.getText().length());
	    		 			
	    		 			SharedPreferences.Editor editor = pref.edit();
	    		 			editor.clear(); 
	    		 			editor.commit();
	    		    	}
	      	
	      	    	userIsInteracting = false;
	        	   
	           }
	       	});
	 	      
	    	}
		}
		
		
		
		
		
		*//** AsyncTask register message  *//*
	    private class RegisterTaskMessage extends AsyncTask<String, String, Boolean>{
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	        	
	        	// extract data
	 
	        	  TextView messageEditText = (TextView) getActivity().findViewById(R.id.editTextMessage);
			      String message = messageEditText.getText().toString();
			      String htmlString=Html.toHtml((Spanned) messageEditText.getText());
				 
			      String user_to = report_user;
			      
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				  boolean suc = false;

					   suc = MyProvider.createMessage(user_to, htmlString);

	        	  return suc;
	        }
	           
	        *//**
		     * After completing background task Dismiss the progress dialog
		     * **//*
	        protected void onPostExecute(final Boolean success) {
	        	
	        	// updating UI from Background Thread
	        	
	        	getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(getActivity(), getString(R.string.text_error), error_message);
			    		}
						else
						{
							Toast.makeText(getActivity(), "Meesage sent", Toast.LENGTH_SHORT).show();
							
							ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager);
							viewPager.setCurrentItem(1); 
				            
						}
					}
				});
     
		    }
	    }
	    
	   
	 
}
*/