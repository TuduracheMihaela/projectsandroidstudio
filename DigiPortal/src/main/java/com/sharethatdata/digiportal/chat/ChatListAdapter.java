package com.sharethatdata.digiportal.chat;

import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import com.sharethatdata.digiportal.R;
import com.sharethatdata.digiportal.navdrawer.MyImageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChatListAdapter extends ArrayAdapter<Message> {    
    private String mUserId;
    Context context;
    
    public MyImageLoader loadMyImg;
    
    private LayoutInflater inflater;

    public ChatListAdapter(Context context, String userId, List<Message> messages) {
            super(context, 0, messages);
            this.mUserId = userId;
            this.context=context;
            
            loadMyImg = new MyImageLoader(context.getApplicationContext());
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                inflate(R.layout.chat_item, parent, false);
            final ViewHolder holder = new ViewHolder();
            holder.imageLeft = (com.sharethatdata.digiportal.navdrawer.CircularImageView)convertView.findViewById(R.id.ivProfileLeft);
            holder.imageRight = (ImageView)convertView.findViewById(R.id.ivProfileRight);
            holder.bodyText = (TextView)convertView.findViewById(R.id.tvBodyText);
            holder.nameLeft = (TextView)convertView.findViewById(R.id.tvNameLeft);
            holder.nameRight = (TextView)convertView.findViewById(R.id.tvNameRight);
            holder.date = (TextView)convertView.findViewById(R.id.tvDate);
            holder.content = (LinearLayout) convertView.findViewById(R.id.linearLayout);
            convertView.setTag(holder);
        }
        /*HashMap<String, String>hashmap_Current;
        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=messages.get(position);*/
        final Message message = (Message)getItem(position);
        final ViewHolder holder = (ViewHolder)convertView.getTag();
        
        SharedPreferences prefIdContact = getContext().getSharedPreferences("PREF_ID_CONTACT", 0); 
        String idContact = prefIdContact.getString("idContact", ""); 
        final boolean isMe = message.getUserId().equals(idContact);
        
        LinearLayout.LayoutParams paramsRight = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
                LayoutParams.WRAP_CONTENT);  
        
        LinearLayout.LayoutParams paramsLeft = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
                LayoutParams.WRAP_CONTENT);  
        paramsLeft.width = 100;
        paramsLeft.height = 100;
        
        SharedPreferences prefImage = context.getSharedPreferences("PREF_IMAGE_CONTACT", 0); 
        String current_image = prefImage.getString("image", "");
        String current_name = prefImage.getString("name", "");
        
        
        //final boolean isMe = idContact.equals(mUserId);
        // Show-hide image based on the logged-in user. 
                // Display the profile image to the right for our user, left for other users.
        if (isMe) {
            holder.imageRight.setVisibility(View.VISIBLE);
            holder.imageLeft.setVisibility(View.GONE);
           // holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            paramsRight.gravity = Gravity.RIGHT; 
            holder.bodyText.setLayoutParams(paramsRight);  
            holder.bodyText.setBackgroundResource(R.drawable.chat_in_bg);
            holder.nameRight.setVisibility(View.VISIBLE); 
            holder.nameLeft.setVisibility(View.GONE);
            //holder.nameRight.setText("Me"); -> setText Name Right Side
            holder.nameRight.setLayoutParams(paramsRight);
            
        } else {
            holder.imageLeft.setVisibility(View.VISIBLE);
            holder.imageRight.setVisibility(View.GONE);
            //holder.imageLeft.setImageResource(R.drawable.user);
            //loadMyImg.DisplayImage(current_image, holder.imageLeft); // image from database
            /*new DownloadImageTask((ImageView) holder.imageLeft)
            .execute("http://dev-ws.sharethatdata.com/art/resources/resource.jpg");*/ // image path
            //holder.imageLeft.setLayoutParams(paramsRight);
            //holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            paramsLeft.gravity = Gravity.LEFT; 
            holder.bodyText.setLayoutParams(paramsRight);  
            holder.bodyText.setBackgroundResource(R.drawable.chat_out_bg);
            //holder.nameLeft.setText(current_name);
            holder.nameLeft.setVisibility(View.VISIBLE);          
            holder.nameRight.setVisibility(View.GONE);
            
            //holder.content.setOrientation(LinearLayout.HORIZONTAL);
        }
        final ImageView profileView = isMe ? holder.imageRight : holder.imageLeft;
        
        // get previous date to compare 
        String previousTs = "";
        if(position>1){
            Message pm = (Message)getItem(position-1);
            previousTs = pm.getDate();
        }
        setTimeTextVisibility(message.getDate(), previousTs, holder.date);
        
        String bodyTextTime = " \t " + "<font color='#708090'><small><sub>"+message.getTime()+"</sub></small></font>";
        holder.bodyText.setText(Html.fromHtml(message.getBody() + bodyTextTime));
        return convertView;
    }

    final class ViewHolder {
        public com.sharethatdata.digiportal.navdrawer.CircularImageView imageLeft;
        public ImageView imageRight;
        public TextView bodyText, nameLeft, nameRight, date;
        public LinearLayout content;
        //public RelativeLayout layoutBody;
    }
    
    // display date once, if repeat date set visibility gone
    private void setTimeTextVisibility(String ts1, String ts2, TextView timeText){
    	
    	try{
	        if(ts2.equals("")){
	            timeText.setVisibility(View.VISIBLE);
	            timeText.setText(formatDate(ts1));
	        }else {
	            Calendar cal1 = Calendar.getInstance();
	            Calendar cal2 = Calendar.getInstance();
	            
	            cal1.clear();
	            cal2.clear();
	            
	            StringTokenizer tkDate1 = new StringTokenizer(ts1);
	            String dateTokenizer1 = tkDate1.nextToken();  // <---  dd-mm-yyyy
	            String[] separatedDate1 = dateTokenizer1.split("-");
	            String day1 = separatedDate1[0];
	            String month1 = separatedDate1[1];
	            String year1 = separatedDate1[2];
	            cal1.set(Integer.valueOf(year1), Integer.valueOf(month1), Integer.valueOf(day1));
	            
	            StringTokenizer tkDate2 = new StringTokenizer(ts2);
	            String dateTokenizer2 = tkDate2.nextToken();  // <---  dd-mm-yyyy
	            String[] separatedDate2 = dateTokenizer2.split("-");
	            String day2 = separatedDate2[0];
	            String month2 = separatedDate2[1];
	            String year2 = separatedDate2[2];
	            cal2.set(Integer.valueOf(year2), Integer.valueOf(month2), Integer.valueOf(day2));
	            
	            int y1 = cal1.get(Calendar.YEAR) ;
	            int m1 = cal1.get(Calendar.MONTH) ;
	            int y2 = cal2.get(Calendar.YEAR) ;
	            int m2 = cal2.get(Calendar.MONTH) ;
	
	            boolean sameMonth = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
	                    cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
	    	                    cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH) ;
	
	            if(sameMonth){
	                timeText.setVisibility(View.GONE);
	                timeText.setText("");
	            }else {
	                timeText.setVisibility(View.VISIBLE);
	                timeText.setText(formatDate(ts1));
	            }
	            
	            
	
	        }
    	}catch(NumberFormatException nfe){
    		System.out.println("NumberFormatException: " + nfe.getMessage());
    	}
    }
    
    public String formatDate(String ts11){
    	StringTokenizer tkDate = new StringTokenizer(ts11);
        String dateTokenizer = tkDate.nextToken();  // <---  yyyy-mm-dd
        String[] separatedDate = dateTokenizer.split("-");
        String year = separatedDate[0];
        String month = separatedDate[1];
        String day = separatedDate[2];
        
    	String month_text = theMonth(Integer.valueOf(month)-1);
    	
    	String date = month_text + " " + day + ", " + year;
    	
		return date;
    	
    }
    
    public static String theMonth(int month){
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }
    
    // Disable Click Event for Messages ListView Items
    @Override
    public boolean isEnabled(int position) {
        return false;
    }
    

}
