package com.sharethatdata.digiportal.chat;

public class Message{
	
	private String userId;
	private String body;
	private String time;
	private String date;
	
	public Message(String userId, String body, String time, String date){
		this.userId = userId;
		this.body = body;
		this.time = time;
		this.date = date;
	}
	
	public String getUserId() {
        return userId;
    }

    public String getBody() {
        return body;
    }
    
    public String getTime(){
    	return time;
    }
    
    public String getDate(){
    	return date;
    }

    public void setUserId(String userId) {
        this.userId = userId;  
    }

    public void setBody(String body) {
        this.body = body;
    }
    
    public void setTime(String time){
    	this.time = time;
    } 
    
    public void setDate(String date){
    	this.date = date;
    } 
}
