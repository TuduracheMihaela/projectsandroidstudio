package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cNews;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

public class NewsDetail extends FragmentActivity {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;
	
	private static ProgressDialog pDialog;
	
	private String myUser = "";
	private String myPass = "";
	// vars for remember report
    private String report_user = "";
    // var id
    private String id = "";
    
    String myImageData = "";
    
    TextView titleText;
    TextView datetimeText;
    TextView descriptionText;
    
    Button editNews;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.fragment_listview_detail);
	        
	        Globals = ((MyGlobals)getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	        
			
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
			    String value = extras.getString("ID");    
			    id = value; 
	    	}
			
			
			titleText = (TextView) findViewById(R.id.textTitle);
			datetimeText = (TextView) findViewById(R.id.textDateTime);
			descriptionText = (TextView) findViewById(R.id.textDescription);
			editNews = (Button) findViewById(R.id.btnEdit);
			
			String isManager = Globals.getValue("manager");
	 		String isEmployee = Globals.getValue("employee");
	 		//if (isManager == "yes" || isEmployee == "yes"){
			if(isManager == "yes"){
				editNews.setVisibility(View.VISIBLE);
			}else{
				editNews.setVisibility(View.GONE);;
			}
			
			new LoadNewsItem().execute();
			
			getActionBar().setDisplayHomeAsUpEnabled(true);

	 }
	 
	 @Override
	 protected void onResume() {
		 super.onResume();
		 Log.e("TAG_RESUME_NEWS_DETAIL","TAG_RESUME_NEWS_DETAIL");
		 
		  String come_back_from_news_update = Globals.getValue("news_update");
	        if(come_back_from_news_update != ""){
	        	 Globals.setValue("news_update", "");
	        	 finish();
	        }
	        
	 }
	 
	 public void onEditNews(View v){

		Globals.setValue("edit", "edit");
		Globals.setValue("newsid", id.toString());
		Globals.setValue("textTitle", titleText.getText().toString());
		Globals.setValue("datetimeText", datetimeText.getText().toString());
		Globals.setValue("descriptionText", descriptionText.getText().toString());
		Globals.setValue("myImageData", String.valueOf(myImageData));
		
		Intent i = new Intent(NewsDetail.this, NewsCreateActivity.class);
		startActivity(i);
	 }
	 
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	 
	 private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
	 
	 /**
		 * Background Async Task to Load News Item by making HTTP Request
	  	 * */
		class LoadNewsItem extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	      // Showing progress dialog
	         pDialog = new ProgressDialog(NewsDetail.this);
	         pDialog.setTitle("Loading News");
	         pDialog.setMessage("Please wait...");
	         pDialog.setIndeterminate(false);
	         pDialog.setCancelable(true);
	         pDialog.setCanceledOnTouchOutside(false);
	         pDialog.show();
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		    	
		    	Globals = ((MyGlobals) getApplicationContext());	
				
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
		    	final cNews news = MyProvider.getNewsItem(id);
		    	
		    	if (news != null)
				  {
					  if (news.picture != "0")
					  {
						  myImageData = MyRoboProvider.getImage(news.picture);
					  }
				  }
		    	
		    	runOnUiThread(new Runnable() {
		               @Override
		               public void run() {
		            	   	titleText.setText(Html.fromHtml(news.title));
		   		    		datetimeText.setText(Html.fromHtml(news.created));
		   		    		descriptionText.setText(Html.fromHtml(news.description));    
		   		    		
		   		    		ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
		  				  
			  				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
			  				  if (d != null)
			  				  {
			  					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
			  					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
			  					  imageView1.setImageBitmap(scaled);
			  					  				  
			  				  }
		               }
		            });
		    	 
		    	
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String result) {
		        // dismiss the dialog after getting all products
		
		    	super.onPostExecute(result);

	            // Dismiss the progress dialog
	            if (pDialog.isShowing())
	                pDialog.dismiss();
	        
		  	      
		    }
		  

		}

}
