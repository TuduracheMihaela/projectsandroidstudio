package com.sharethatdata.digiportal;/*package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cNews;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class MessagesDetail extends FragmentActivity{
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	private static ProgressDialog pDialog;
	
	private String myUser = "";
	private String myPass = "";
	// vars for remember report
    private String report_user = "";
    // var id
    private String id = "";
    private String name = "";
    
    private String indexTab = "";
    
    TextView titleText;
    TextView datetimeText;
    TextView descriptionText;
    Button btnReply;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.fragment_listview_detail);
	        
	        Globals = ((MyGlobals)getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        
			
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
			    String valueID = extras.getString("ID");    
			    	id = valueID; 
			    String valueName = extras.getString("Name");
			    	name = valueName;
			    String tab = extras.getString("IndexTab");
			    	indexTab = tab;}
			
			
			titleText = (TextView) findViewById(R.id.textTitle);
			datetimeText = (TextView) findViewById(R.id.textDateTime);
			descriptionText = (TextView) findViewById(R.id.textDescription);
			btnReply = (Button) findViewById(R.id.btnReply);
			
			new LoadMessagesItem().execute();			
			
			getActionBar().setDisplayHomeAsUpEnabled(true);
			
			if(indexTab.contains("Inbox")){
				btnReply.setVisibility(View.VISIBLE);
			}else if(indexTab.contains("Sent")){
				btnReply.setVisibility(View.GONE);
			}
			
	 }

	 Fragment fragment = null;
 
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   
			   fragment = new MessagesTabsFragment();
			   
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	 
	 
	 *//**
		 * Background Async Task to Load all holidays by making HTTP Request
	  	 * *//*
		class LoadMessagesItem extends AsyncTask<String, String, String> {

	     *//**
	      * Before starting background thread Show Progress Dialog
	      * *//*
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	      // Showing progress dialog
	         pDialog = new ProgressDialog(MessagesDetail.this);
	         pDialog.setTitle("Loading Message");
	         pDialog.setMessage("Please wait...");
	         pDialog.setIndeterminate(false);
	         pDialog.setCancelable(true);
	         pDialog.setCanceledOnTouchOutside(false);
	         pDialog.show();
	     }

		    *//**
		     * getting items from url
		     * *//*
		    protected String doInBackground(String... args) {
		    	
		    	Globals = ((MyGlobals) getApplicationContext());	
				
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
		    	final cMessage message = MyProvider.getMessage(id);
		    	
		    	runOnUiThread(new Runnable() {
		               @Override
		               public void run() {
		            	   	titleText.setText(Html.fromHtml(message.user_from));
		   		    		datetimeText.setText(Html.fromHtml(message.datetime));
		   		    		descriptionText.setText(Html.fromHtml(message.message));      
		               }
		            });
		    	 
		    	
		    	
		    	 return "";
		    }
		
		    *//**
		     * After completing background task Dismiss the progress dialog
		     * **//*
		    protected void onPostExecute(String result) {
		        // dismiss the dialog after getting all products
		
		    	super.onPostExecute(result);

	            // Dismiss the progress dialog
	            if (pDialog.isShowing())
	                pDialog.dismiss();
	        
		  	      
		    }
		  

		}
		
		public void onReplyMessage(View v){
			SharedPreferences.Editor pref = getSharedPreferences("PREF_REPLY", 0).edit();
			pref.putInt("ComposeTab", 2);
			pref.putString("Name", name);
			pref.putString("Description", descriptionText.getText().toString());
	   	    pref.commit();
			
			finish();
		}

}
*/