package com.sharethatdata.digiportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.sharethatdata.digiportal.navdrawer.DrawerAdapter;
import com.sharethatdata.digiportal.navdrawer.DrawerItem;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cLocationCurrent;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cNewsStatus;
import com.sharethatdata.webservice.datamodel.cUserId;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cUserStatus;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import static android.content.Context.MODE_PRIVATE;

public class DashBoardActivity extends FragmentActivity {
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;
	
	private String myUser = "";
	private String myPass = "";
	 private String report_user = "";
	
	// List Contacts
	List<String> userkeyArray =  new ArrayList<String>();
	ArrayList<HashMap<String, String>> userList;
	String idContact = "";
	
	// List Messages
	ArrayList<HashMap<String, String>> messageList;
	
	// List News
	ArrayList<HashMap<String, String>> newsList;
	
	// List News Status
	ArrayList<HashMap<String, String>> newsListStatus;

	// List Contacts
	ArrayList<HashMap<String, String>> usersList;
	
	String idImage = "";	
	
	// Status for user
	ArrayList<HashMap<String, String>> statusList;
	List<String> statuskeyArray =  new ArrayList<String>();
	List<String> statusArray =  new ArrayList<String>();
	// Location for user
	ArrayList<HashMap<String, String>> locationList;
	List<String> locationkeyArray =  new ArrayList<String>();
	List<String> locationArray =  new ArrayList<String>();
		
	
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
   // private ActionBarDrawerToggle mDrawerToggle;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    DrawerAdapter adapter;
    List<DrawerItem> dataList;
    
    Context context;
    
    String unreadMessages = "";
    String unreadNews = "";
    
    int nrUnreadMessage = 0;
    String statusReadMessage = "";
    
    int nrUnreadNews = 0;
    String idUserNews = "";
    
    private boolean isDrawerOpen = false;
    
 // Create a handler which can run code periodically
    private Handler mHandler = new Handler();
    private Runnable mPendingRunnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        
     // Initializing
        Globals = ((MyGlobals)getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
	    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		context = getApplicationContext();
        
        new LoadUserId().execute(); // load id by username(user logged)
        new LoadContact().execute(); // load info about user logged (surname, lastname, image_id)
        new LoadImage().execute(); // load image string user logged
        new LoadLocationsAndStatusList().execute(); // location and status list for user to drawer adapter
        new LoadListStatus().execute(); // load messages, news, status name, current location
        
        final SharedPreferences prefUnread = getSharedPreferences("PREF_TOTAL_UNREAD_MESSAGES", 0); 
        unreadMessages = prefUnread.getString("unreadMessages", ""); 
        String u = unreadMessages;
        System.out.println(u);

        dataList = new ArrayList<DrawerItem>();

        mTitle = mDrawerTitle = getTitle();

       // mNavigationDrawerItemTitles = getResources().getStringArray(R.array.menu_slider_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        
        
        getList(String.valueOf(nrUnreadMessage), String.valueOf(nrUnreadNews));

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
 
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                //R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                unreadMessages = prefUnread.getString("unreadMessages", ""); 
                String uu = unreadMessages;
                System.out.println(uu);
                
                adapter.notifyDataSetChanged();
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
                
             // If mPendingRunnable is not null, then add to the message queue 
/*                if (mPendingRunnable != null) {
                    mHandler.post(mPendingRunnable);
                    mPendingRunnable = null;
                }*/
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                unreadMessages = prefUnread.getString("unreadMessages", ""); 
                String uuu = unreadMessages;
                System.out.println(uuu);
                
                new LoadListStatus().execute();
                String uuuu = String.valueOf(nrUnreadMessage);
                System.out.println(uuuu);
                String uuuuu = String.valueOf(nrUnreadNews);
                System.out.println(uuuuu);
  
                adapter.notifyDataSetChanged();
                if (mDrawerLayout != null)
                {
                	//getList(uuu);
          	  	}
                
                
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
            
            
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if(slideOffset > 0 && !isDrawerOpen){
                    onDrawerOpened(drawerView);
                    isDrawerOpen = true;
                } else if(slideOffset < 0 && isDrawerOpen) {
                    onDrawerClosed(drawerView);
                    isDrawerOpen = false;
                }
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        
        if (savedInstanceState == null) {
        	 
           /* if (dataList.get(0).isSpinner()
                        & dataList.get(1).getTitle() != null) {
                  selectItem(2);
            } else if (dataList.get(0).getTitle() != null) {
                  selectItem(1);
            } else {
                  selectItem(0);
            }*/
            
            selectItem(1);
      }
        
        supportInvalidateOptionsMenu();
        
     // Run the runnable object defined every 30000 ms
       // handler.postDelayed(runnable, 30000);
        
        /**
         * SSID
         */   
        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		String ssid_interval = sharedPrefs.getString("prefIntervalSsid", "5");
		if(ssid_interval.equals("")){
			ssid_interval = "5";
		}
		long interval_milis = TimeUnit.MINUTES.toMillis(Integer.valueOf(ssid_interval));
		
		System.out.println("SSID min: " + ssid_interval + " milis: " + interval_milis);
		
/*		// start service to check location
        AlarmManager alarmManagerSSID=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intentSSID = new Intent(DashBoardActivity.this, LoginMyReceiver.class);
        PendingIntent pendingIntentSSID = PendingIntent.getBroadcast(DashBoardActivity.this, 0, intentSSID, 0);
        //alarmManagerSSID.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),Long.valueOf(interval_milis), pendingIntentSSID);
        alarmManagerSSID.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntentSSID);*/
        
    }
    
    
    public void cancelAlarmSSID(){
    	
    }
    
    // This is working just if application is running
 // Defines a runnable which is run every 30000 ms
/*    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
           refreshMessages(); 
           handler.postDelayed(this, 30000);
           
           System.out.println("refresh");
           Toast.makeText(getApplicationContext(), "Refresh", Toast.LENGTH_SHORT).show();
           
           if(nrUnreadMessage > 0){
        	   NotificationCompat.Builder mBuilder =
        			    new NotificationCompat.Builder(DashBoardActivity.this)
        			    .setSmallIcon(R.drawable.ic_launcher)
        			    .setContentTitle("My notification")
        			    .setContentText("Hello World!" + unreadMessages);
        	   
        	// Sets an ID for the notification
        	   int mNotificationId = 001;
        	   // Gets an instance of the NotificationManager service
        	   NotificationManager mNotifyMgr = 
        	           (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        	   // Builds the notification and issues it.
        	   mNotifyMgr.notify(mNotificationId, mBuilder.build());
           }
        }
    };*/
    
    private void refreshMessages() {
    	new LoadListStatus().execute();  
    	
    }
    
    /*@Override
    protected void onPause(){
    	super.onPause();
    	
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int i = 0;
                    while (true) {
                        i++;
                        Thread.sleep(2000);
                        Log.i("not dead", "not dead onPause" + i);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println(e.toString());
                }
            }
        }).start();
    }*/
    

    @Override
    protected void onResume() {
        super.onResume();
        
        Log.i("onResume", String.valueOf("onResume"));

        String come_back_from_created_news = Globals.getValue("news_create");
        if(come_back_from_created_news != ""){
        	selectItem(2);
        	 Globals.setValue("news_create", "");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("onPause", String.valueOf("onPause"));
    }
    
    public void getList(String unreadMessage, String unreadNews){
    	 // Add Drawer Item to dataList
    	
    	adapter = new DrawerAdapter(this, R.layout.navdrawer_item,dataList, statuskeyArray, statusArray, locationkeyArray, locationArray);
    	
    	dataList.clear();

        dataList.add(new DrawerItem(true)); // adding a spinner to the list
      
       //dataList.add(new DrawerItem("My Favorites")); // adding a header to the list
      dataList.add(new DrawerItem("Dashboard", "home", this));
      
      if(unreadNews.contains("0")){
    	  dataList.add(new DrawerItem("News", "news", this));
      }else{
    	  dataList.add(new DrawerItem("News", "news", this, String.valueOf(unreadNews)));
      }
      
      if(unreadMessage.contains("0")){
    	  dataList.add(new DrawerItem("Messages", "messages", this));
      }else{
    	  dataList.add(new DrawerItem("Messages", "messages", this, String.valueOf(unreadMessage)));
      }
     // dataList.add(new DrawerItem("Instant Messages", "messages", this));
      
       dataList.add(new DrawerItem("Modules"));// adding a header to the list
      dataList.add(new DrawerItem("DigiWorkTime", "", this));
      dataList.add(new DrawerItem("DigiVisitor", "", this));
      dataList.add(new DrawerItem("DigiStock", "", this));
      dataList.add(new DrawerItem("DigiSupport", "", this));

      
      adapter.notifyDataSetChanged();
      mDrawerList.setAdapter(adapter);
    	  
    }


    /* The click listner for ListView in the navigation drawer */
    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        	if (dataList.get(position).getTitle() == null) {
                    	selectItem(position);
          }
        }
    }

    public void selectItem(final int position) {
/*    	mPendingRunnable = new Runnable() {
            @Override
            public void run() {*/
		        // update the main content by replacing fragments
		        Fragment fragment = null;
		        Bundle args = new Bundle();
		        switch (position) {
			        case 1:
			        	fragment = new DashBoardFragment();
			            break;
		            case 2:
		            	fragment = new NewsFragment();
		                break;
		            /*case 3:
		                fragment = new MessagesTabsFragment();
		                break;*/
		                
		            case 3:
		                fragment = new InstantMessagesList();
		                break;
		            case 5:
		            	Intent launchIntentDigitime = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digitime");
		            	if(launchIntentDigitime != null){
		            		// We found the activity now start the activity
		            		startActivity(launchIntentDigitime);
		            	}else{
		            		Toast.makeText(getApplicationContext(), getResources().getString(R.string.DigiTime_not_installed), Toast.LENGTH_SHORT).show(); 
		            	}
		            	
		                break;
		            case 6:
		            	//Toast.makeText(getApplicationContext(), "case " + position, Toast.LENGTH_SHORT).show(); 
		            	Intent launchIntentDigivisitor = getPackageManager().getLaunchIntentForPackage("com.robopharma.digivisitor");
		            	if(launchIntentDigivisitor != null){
		            		// We found the activity now start the activity
		            		startActivity(launchIntentDigivisitor);
		            	}else{
		            		Toast.makeText(getApplicationContext(), getResources().getString(R.string.DigiVisitor_not_installed), Toast.LENGTH_SHORT).show(); 
		            	}
		            	
		            	break;
		            case 7:
		            	Intent launchIntentDigistock = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digistock");
		            	if(launchIntentDigistock != null){
		            		// We found the activity now start the activity
		            		startActivity(launchIntentDigistock);
		            	}else{
		            		Toast.makeText(getApplicationContext(), getResources().getString(R.string.DigiStock_not_installed), Toast.LENGTH_SHORT).show(); 
		            	}
		            	break;
		            case 8:
		            	Intent launchIntentDigisupport = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digisupport");
		            	if(launchIntentDigisupport != null){
		            		// We found the activity now start the activity
		            		startActivity(launchIntentDigisupport);
		            	}else{
		            		Toast.makeText(getApplicationContext(), getResources().getString(R.string.DigiSupport_not_installed), Toast.LENGTH_SHORT).show(); 
		            	}
		            	break;
		
		            default:
		                break;
		        }
		
		        if (fragment != null) {
		        	fragment.setArguments(args);
		        	FragmentManager fragmentManager = getSupportFragmentManager();
		            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

		        } else {
		            Log.e("MainActivity", "Error in creating fragment");
		        }
/*            }
        };*/
        
        mDrawerList.setItemChecked(position, true);
        //mDrawerList.setSelection(position);
        /*setTitle(mNavigationDrawerItemTitles[position]);*/
        setTitle(dataList.get(position).getItemName());
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        }

		 if (item.getItemId() == R.id.action_create_news) {
            Intent intent = new Intent(this, NewsCreateActivity.class); startActivity(intent); 
            return true;
        }
		 
		 if (item.getItemId() == R.id.action_users) {
	            Intent intent = new Intent(this, UsersListActivity.class); startActivity(intent); 
	            return true;
	        }
 		

        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//menu.clear();
		
		// check manager
		 		String isManager = Globals.getValue("manager");
		 		String isEmployee = Globals.getValue("employee");
		//if (isManager == "yes" || isEmployee == "yes")
		if (isManager == "yes")
 		{
			getMenuInflater().inflate(R.menu.dashboard_manager, menu);
 		}else{
 			getMenuInflater().inflate(R.menu.dashboard, menu);
 			//getMenuInflater().inflate(R.menu.dashboard_manager, menu);
 		}
		return true;
	}
    

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserId extends AsyncTask<String, String, String> {

     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	Globals = ((MyGlobals) getApplicationContext());	
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    List<cContact> List = MyProvider.getUserIdbyUsername(myUser); // id of user logged
	    
	    for(cContact entry : List)
     	{
			SharedPreferences.Editor editor = getSharedPreferences("PREF_ID_CONTACT", 0).edit();
	   	     editor.putString("idContact", String.valueOf(entry.id));
	   	     editor.putString("name", String.valueOf(entry.name));
	   	     editor.commit();
     	}
		
	    SharedPreferences pref = getSharedPreferences("PREF_ID_CONTACT", 0); 
        String myId = pref.getString("idContact", "");
	    idContact = myId;
 	
		return "";
   }

   protected void onPostExecute(String file_url) { 
    	}
	}
    
    /**
   	 * Background Async Task to Load a contact by making HTTP Request
     	 * */
   	class LoadContact extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

   	    /**
   	     * getting items from url
   	     * */
   	    protected String doInBackground(String... args) {
   	    	
   	    	Globals = ((MyGlobals) getApplicationContext());	
   			
   		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
   	    	
   	    	 cContact contact = MyProvider.getContact(idContact);
   	    	 String surname = contact.surname;
   	    	 String lastname = contact.lastname;
   	    	 String image_id = contact.image_id;
   	    	 
   	    	 idImage = image_id;
   	    	 
   	    	SharedPreferences.Editor editor = getSharedPreferences("PREF_CONTACT", MODE_PRIVATE).edit();
	   	     editor.putString("surname", surname);
	   	     editor.putString("lastname", lastname);
	   	     editor.commit();
 
   	    	 return "";
   	    }
   	
   	    /**
   	     * After completing background task Dismiss the progress dialog
   	     * **/
   	    protected void onPostExecute(String result) {
   	    	super.onPostExecute(result);   
   	    }
   	}

    /**
   	 * Background Async Task to Load a image by making HTTP Request
     	 * */
   	class LoadImage extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

   	    /**
   	     * getting items from url
   	     * */
   	    protected String doInBackground(String... args) {
   	    	
   	    	Globals = ((MyGlobals) getApplicationContext());	
   			
   		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
   	    	
   	    	 //cImage image = MyProvider.getImage(idImage);
   	    	 //String Base64image = image.image;
   	    	 
   	    	 String image = MyRoboProvider.getImageThumnail(idImage);
   	    	 
   	    	SharedPreferences.Editor editor = getSharedPreferences("PREF_IMAGE", MODE_PRIVATE).edit();
   	    	editor.clear().commit();
	   	     editor.putString("image", image);
	   	     editor.putString("imageID", idImage);
	   	     editor.commit();
   	    	 
   	    	System.out.println("Base64image " + image);
   	    	 
   	    	 
   	    	 return "";
   	    }
   	
   	    /**
   	     * After completing background task Dismiss the progress dialog
   	     * **/
   	    protected void onPostExecute(String result) {
   	    	super.onPostExecute(result);   
   	    }
   	}
   	
    /**
	 * Background Async Task to Load all messages, news for status read, location and status user(sick, busy, etc) by making HTTP Request
  	 * */
	class LoadListStatus extends AsyncTask<String, String, String> {

     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			//////////////////////////////////////////////////////////////////
			// Status = busy, sick, free , etc
			
			final cContact contact = MyProvider.getContact(idContact);
			
			SharedPreferences.Editor editorStatus = context.getSharedPreferences("PREF_STATUS", MODE_PRIVATE).edit();
			editorStatus.putString("status_name", contact.status_name);
			editorStatus.commit();
			
			//////////////////////////////////////////////////////////////////////////
			// Location
			final cLocationCurrent location = MyProvider.getUserCurrentLocation(idContact, "");
			
			SharedPreferences.Editor editorLocation = context.getSharedPreferences("PREF_USER_LOCATION", MODE_PRIVATE).edit();
			editorLocation.putString("location_name", location.location_name);
			editorLocation.commit();
			
		    /////////////////////////////////////////////////////////////
		    // message
		    messageList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cMessage> message = MyProvider.getMessagesInbox();
	    	 for(cMessage entry : message)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.user_from);
                map.put("created", entry.datetime);
                map.put("description", entry.message);
                map.put("status", entry.status_read);
                
                statusReadMessage = entry.status_read;
    
                // adding HashList to ArrayList
                messageList.add(map);
	    	 }
	    	 
	    	 //////////////////////////////////////////////////////////////
	    	 // news
			    newsListStatus = new ArrayList<HashMap<String, String>>();  
		    	
		    	 List<cNewsStatus> status = MyProvider.getNewsUnread(idContact);
		    	 for(cNewsStatus entry : status)
		    	 {
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                
	                map.put("id",  String.valueOf(entry.id));
	                map.put("id_news", entry.id_news);
	                map.put("id_user", entry.id_user);
	                map.put("status", entry.status);
	                 
	                // adding HashList to ArrayList
	                newsListStatus.add(map);
		    	 }

	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	    	 // here you check the value of getActivity() and break up if needed
	        if(getApplicationContext() == null)
	            return;
            /**
             * Updating parsed JSON data into ListView
             * */
	    	runOnUiThread(new Runnable() {
	            public void run() {

	            	// message
	            	int total = 0;
	            	int total_unread = 0;
	            	int total_read = 0;
	            	int myRow = 0;
	            	for (HashMap<String, String> map : messageList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "status")
	           	        	{
	           	        		
	           	        		try {
	           	        			myRow = Integer.parseInt(entry.getValue().toString());
	           	        			String row = String.valueOf(myRow);
	           	        			
	           	        			if(row.contains("0")){
	           	     	        	total_unread++;
	           	     	       	}
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myRow = 0;
	           	        		}
	           	        	}
	           	        }
	           		System.out.println("Total unread messages : " + total_unread);

	           		nrUnreadMessage = total_unread;
	           		
	           		///////////////////////////////////////////////////////////////////
	           		// news
	           		for (HashMap<String, String> map : newsListStatus)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {

	           	        	if (entry.getKey() == "status")
	           	        	{
	           	        		
	           	        		try {
	           	        			myRow = Integer.parseInt(entry.getValue().toString());
	           	        			String row = String.valueOf(myRow);
	           	        			
	           	        			if(row.contains("0")){
	           	     	        	total_unread++;
	           	     	       	}
	           	     	        if(row.contains("1")){
	           	     	        	total_read++;
	           	     	       	}
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myRow = 0;
	           	        		}
	           	        		total = total_unread + total_read;
	           	        	}
	           	        }
	            	System.out.println("Total read news : " + total_read);
	           		System.out.println("Total unread news : " + total_unread);
	           		System.out.println("Total news : " + total);

	           		nrUnreadNews = total_unread;

	           		getList(String.valueOf(nrUnreadMessage) , String.valueOf(nrUnreadNews));
	            }
	        });    
	    }
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Location for user
	/**
	 * Background Async Task to Load all locations and user status list by making HTTP Request
  	 * */
	class LoadLocationsAndStatusList extends AsyncTask<String, String, String> {

     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	Globals = ((MyGlobals) getApplicationContext());	
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			locationList = new ArrayList<HashMap<String, String>>();
			statusList = new ArrayList<HashMap<String, String>>();
			
    		List<cUserLocation> list = MyProvider.getUserLocation(myUser);
    		for(cUserLocation entry : list)
         	{
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", Long.toString(entry.id));
                map.put("name", entry.name);               
                             
                // adding HashList to ArrayList
                locationList.add(map);
             }
			
			List<cUserStatus> List = MyProvider.getUserStatus();
			for(cUserStatus entry : List)
			{
				HashMap<String, String> map = new HashMap<String, String>();
				
				map.put("id", entry.id);
				map.put("name", entry.name); 
			
			statusList.add(map);
			}
    		
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) { 
	   locationkeyArray =  new ArrayList<String>();
	   locationArray =  new ArrayList<String>();
	   statuskeyArray =  new ArrayList<String>();
	   statusArray =  new ArrayList<String>();
	
      	 for (HashMap<String, String> map : locationList)
         	        for (Entry<String, String> entry : map.entrySet())
         	        {
         	        	if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
         	        	if (entry.getKey() == "name") locationArray.add(entry.getValue());
         	        } 

		for (HashMap<String, String> map : statusList)
			for (Entry<String, String> entry : map.entrySet())
			{
				if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
				if (entry.getKey() == "name") statusArray.add(entry.getValue());
			} 
      	 
      	 getList(String.valueOf(nrUnreadMessage), String.valueOf(nrUnreadNews));
    	}
	}
 	
 	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		//Handle the back button
	    if(keyCode == KeyEvent.KEYCODE_BACK) {
	        //Ask the user if they want to quit
	        new AlertDialog.Builder(this)
	        .setIcon(android.R.drawable.ic_dialog_alert)
	        .setTitle("Quit DigiPortal")
	        .setMessage("Are you sure?")
	        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	            @Override
	            public void onClick(DialogInterface dialog, int which) {

	                //Stop the activity
	                DashBoardActivity.this.finish();    
	            }
	        })
	        .setNegativeButton("No", null)
	        .show();

	        return true;
	    }
	    else {
	        return super.onKeyDown(keyCode, event);
	    }
	}

}


