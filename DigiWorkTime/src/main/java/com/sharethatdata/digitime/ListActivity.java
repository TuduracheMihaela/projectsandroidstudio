package com.sharethatdata.digitime;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sharethatdata.digitime.customer.CustomersActivity;
import com.sharethatdata.digitime.management.AddOptionToUserActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cTimeRecord;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import static com.sharethatdata.digitime.MyGlobals.getContext;

public class ListActivity extends AppCompatActivity {

	// Progress Dialog
    private ProgressDialog progressDialog;

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider2 = null;
	
	TimeUtils MyTimeUtils = null;
	Utility MyUtility = null;
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;

	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> itemListReport;
	ArrayList<HashMap<String, String>> userList;
    java.util.List<String> userkeyArray =  new ArrayList<String>();
	  
    Spinner userSpinner = null;
	ListView lv = null;
	Button prevButton = null;
	Button nextButton = null;
	Button backButton = null;

    private boolean no_network = false;
    private String error_message = "";
    
    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    private static String myUserName = "";
    private String myUserID = "";
    
    private int maxwork_hours = 8;
    
    private boolean userIsInteracting = false;
    private int userSpinnerIndex = -1;
    
    // vars for remember report
    private String report_user = "";
	private String report_user_id = "";
    
    TextView startDateView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
				
		lv = (ListView) findViewById(R.id.listView);
        userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		
        prevButton = (Button) findViewById(R.id.btnPrev);
        nextButton = (Button) findViewById(R.id.btnNext);
		backButton = (Button) findViewById(R.id.btnBack);

        Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // id of user logged
	
		MyTimeUtils = new TimeUtils();
		MyUtility = new Utility();

		MyTimeUtils.setOrientation(ListActivity.this, ListActivity.this);
		
		// check manager
		String isManager = Globals.getValue("manager");
		if (isManager == "yes")
		{	
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.VISIBLE);
			
			LoadUserList InitUserList;
			
			InitUserList = new LoadUserList();
			InitUserList.execute();
			
		} else {
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.INVISIBLE);
		}
		
		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
		String weekdayname;
		Calendar c = Calendar.getInstance();
		Bundle extras = getIntent().getExtras();
		if(extras !=null) {
			mStartYear = Integer.valueOf(extras.getString("year"));
			mStartMonth = Integer.valueOf(extras.getString("month"));
			mStartDay = Integer.valueOf(extras.getString("day"));
			c.set(mStartYear, mStartMonth, mStartDay);
			weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
		}else{
			// set calendar
		    mStartYear = c.get(Calendar.YEAR);
		    mStartMonth = c.get(Calendar.MONTH);
		    mStartDay = c.get(Calendar.DAY_OF_MONTH);
		    weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
		}

	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);  
	    	    
        startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
        TextView startDayView = (TextView) findViewById(R.id.dateDayText);
        startDayView.setText(weekdayname);
                
		TextView titleText = (TextView) findViewById(R.id.title);
		
		String list_type = "Time Reports";
		
	//	LoadList InitList;
		
		titleText.setText(list_type);
	
	//	InitList = new LoadList();
	//	InitList.execute();
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    String strText = ((TextView) itemClicked.findViewById(R.id.textViewName)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("pid", pid);
			    Globals.setValue("name", strText);
				Globals.setValue("report_user", report_user_id);
			    Globals.setValue("action", "time_record");

				if(!pid.equals("")){
					// start detail activity
					Intent intent = new Intent(getApplicationContext(), DetailActivity.class); startActivity(intent);
				}
			}

		});	
		
		
		userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user = userkeyArray.get(myIndex);
					report_user_id = userkeyArray.get(myIndex);
		  	   
		  	   		userSpinnerIndex = myIndex;
		  	   		
		  		    LoadReports();
		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                
            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        		startDateView = (TextView) findViewById(R.id.dateStartText);
        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
        		String weekdayname = "";
        		
                try {
                	Date myDate = dateFormat.parse(startDateView.getText().toString());
                    
                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, -1); 
            	    mStartYear = c.get(Calendar.YEAR);
            		mStartMonth = c.get(Calendar.MONTH);
            		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
            		    	        	       
            		
                } catch (ParseException e) {
                    System.out.println(e.toString());	
                }
              
                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);
                
        	    LoadReports();
        	    
            }

        });
		
		nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            
            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        		startDateView = (TextView) findViewById(R.id.dateStartText);
        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                String weekdayname = "";
        				
                try {
                	Date myDate = dateFormat.parse(startDateView.getText().toString());
                    
                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, 1); 
            	    mStartYear = c.get(Calendar.YEAR);
            		mStartMonth = c.get(Calendar.MONTH);
            		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
            	
            		
                } catch (ParseException e) {
                	 System.out.println(e.toString());
                }
              
                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);
                
        	    LoadReports();
            }

        });

		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public void onUserInteraction() {
	    super.onUserInteraction();
	    userIsInteracting = true;
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    LoadReports();
	    
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// check manager
		String isManager = Globals.getValue("manager");
		if (isManager == "yes")
		{
			getMenuInflater().inflate(R.menu.list_records_manager, menu);
		}else
		{
			getMenuInflater().inflate(R.menu.list_records, menu);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_enter_report:
				finish();
				return true;
			case R.id.action_getPDF_report:
				// generate pdf
				createPDF();
				redirectEmail();
				return true;
			case R.id.action_stats:
				Intent intent2 = new Intent(this, StatisticsActivity.class); startActivity(intent2);
				return true;
			case R.id.action_progress:
				Intent intent3 = new Intent(this, ProgressActivity.class); startActivity(intent3);
				return true;
			case R.id.action_holiday:
				Intent intent4 = new Intent(this, HolidaysActivity.class); startActivity(intent4);
				return true;
			case R.id.action_sickday:
				Intent intent5 = new Intent(this, SickdaysActivity.class); startActivity(intent5);
				return true;
			case R.id.action_stop_watch:
				Intent intent6 = new Intent(this, StopWatchActivity.class); startActivity(intent6);
				return true;
			case R.id.action_customers:
				Intent intent7 = new Intent(this, CustomersActivity.class); startActivity(intent7);
				return true;
			case R.id.action_manage_activities:
				Intent intent8 = new Intent(this, AddOptionToUserActivity.class);
				intent8.putExtra("option","Activity");
				startActivity(intent8);
				return true;
			case R.id.action_manage_costplaces:
				Intent intent9 = new Intent(this, AddOptionToUserActivity.class);
				intent9.putExtra("option","Costplace");
				startActivity(intent9);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	private void LoadReports()
	{

        String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);

		LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
		
	}
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	        	TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	        	
	        	String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
				  
			    startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));
			    
			    startDayView.setText(dayOfWeekStart);
	                
		  
	            LoadReports();
	    	
	            
		  }
	};

	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
	  	Date d = new Date();
	  	dp1.setTitle(sdf.format(d));
	    dp1.show();
	    
	   
	}
	
	// set time from mysql datetime
		private String SetTime(String mysqldate)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			StringBuilder setText = null;
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    mStartMinute = c.get(Calendar.MINUTE);
			    
			    setText = new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute));
					
			} catch (ParseException e) {
			    	
			}
			
			return setText.toString();

	    }
		
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

		 showProgressDialog(getString(R.string.loading_alert_dialog));
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider2 = new WSDataProvider(myUser, myPass);
		
		Globals = ((MyGlobals)getApplication());
		
	    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		java.util.List<cContact> List = MyProvider2.getMyUsers();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.name);
          
                // adding HashList to ArrayList
                userList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products
	   if (ListActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
		   return;
	   }
	   hideProgressDialog();

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
           
    	    java.util.List<String> userArray =  new ArrayList<String>();
           	for (HashMap<String, String> map : userList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
          	        }
           	
           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(ListActivity.this, android.R.layout.simple_spinner_item, userArray);
      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
      	    	sUsers.setAdapter(user_adapter);
    	        
    	        int spinnerPosition = user_adapter.getPosition(myUserName);
	    		//set the default according to value
	    		sUsers.setSelection(spinnerPosition);
      	
      	    	userIsInteracting = false;

           		}
       		});

    	}
	}

	class IgnoreCaseComparator implements Comparator<cTimeRecord> {
		public int compare(cTimeRecord o1, cTimeRecord o2) {
            return o1.starttime.compareTo(o2.starttime);
        }
	}
	
	/**
	 * Background Async Task to Load all time records by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

         showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	Globals = ((MyGlobals)getApplication());	
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();
			 itemListReport = new ArrayList<HashMap<String, String>>();

	    	 java.util.List<cTimeRecord> timeRecords = MyProvider.getTimeRecords(report_date_start, report_date_end, report_user, false, "0");
	    	 
	    	 // re-order by starttime
	    	 IgnoreCaseComparator icc = new IgnoreCaseComparator();
	    	 java.util.Collections.sort(timeRecords, icc);
	    	 
	    	 for(cTimeRecord entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
				LinkedHashMap<String, String> map1 = new LinkedHashMap<String, String>();

                //map.put("date", entry.starttime.substring(0, 16));
                
                String[] tokens = entry.starttime.split("\\s");
                String values = tokens[0]+tokens[1];
                String date = tokens[0];
                String time = tokens[1];
                
                System.out.println("Values: " + values);
                
               // map.put("date", date);
                map.put("date", SetTime(entry.starttime));
                map.put("time" , SetTime(entry.endtime));

                map.put("pid",  entry.id);
                map.put("name", entry.project);
                map.put("subtitle", entry.activity + ": " + entry.description);
                map.put("hours", Integer.toString(entry.minutes));

				 map1.put("date_time", SetTime(entry.starttime) + "\n" + SetTime(entry.endtime));
				 map1.put("name", entry.project);
				 map1.put("task", entry.task);
				 map1.put("activity", entry.activity);
				 map1.put("location", entry.location);
				 map1.put("costplace", entry.costplace);
				 map1.put("description", entry.description);
				 map1.put("hours", Integer.toString(entry.minutes));
                // adding HashList to ArrayList
                itemList.add(map);
                itemListReport.add(map1);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;
	            
	            	// total
	            	int total = 0;
	            	int myNum = 0;
	            	for (HashMap<String, String> map : itemList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                HashMap<String, String> hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", MyTimeUtils.getFormattedHourMinutes(total) );
	             	hmap.put("subtitle", "");
	            	hmap.put("date", "");
	            	hmap.put("time", "");
	                hmap.put("name", "total");
	           	                
	            	itemList.add(hmap);
	            	
	            	adapter = new SimpleAdapter(
		                		ListActivity.this
		                		, itemList
		                		, R.layout.sublistview_item, new String[] { "date", "time", "pid", "name", "subtitle", "hours" },
	                            new int[] { R.id.textViewDate, R.id.textViewTime, R.id.textViewID, R.id.textViewName, R.id.textViewFunc, R.id.textViewHours });
		        	            
	                
	                // updating listview
	                lv.setAdapter(adapter);
					MyUtility.setListViewHeightBasedOnChildren(lv);
	            }
	        });
	        
	        hideProgressDialog();
	  	      
	    }
	  

	}

	public  void redirectEmail(){
		String filename="Report.pdf";
		//File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Documents/" + "Reports", filename);
		String mBaseFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Documents/" + "Reports";
		String mFilePath = mBaseFolderPath + "/" + "Report" + ".pdf";
		File filelocation = new File(mFilePath);
		//Uri path = Uri.fromFile(filelocation);
		Uri path = FileProvider.getUriForFile(ListActivity.this, BuildConfig.APPLICATION_ID + ".provider", filelocation);
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		// set the type to 'email'
		emailIntent .setType("vnd.android.cursor.dir/email");
		String to[] = {"mverbeek@sharethatdata.com"};
		emailIntent .putExtra(Intent.EXTRA_EMAIL, to);
		// the attachment
		emailIntent .putExtra(Intent.EXTRA_STREAM, path);
		// the mail subject
		emailIntent .putExtra(Intent.EXTRA_SUBJECT, "Report Document");
		startActivity(Intent.createChooser(emailIntent , "Send email..."));
	}

	////////////////////////////////////////////////////////////////////////
    // create PDF //

	private static Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
	private static  Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public void createPDF(){
        try {

			String mBaseFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Documents/" + "Reports";
			if(!new File(mBaseFolderPath).exists()){
				Log.v("!(mBaseFolder).exists()","Folders not exists");

				if(new File(mBaseFolderPath).mkdirs())
					Log.v("(mBaseFolder).mkdir()","Folders Created");
				else
					Log.v("(mBaseFolder).mkdir()","Folders not Created");
			}else
				Log.v("!(mBaseFolder).exists()","Folders exists");

			String mFilePath = mBaseFolderPath + "/" + "Report" + ".pdf";
			Log.v("mFilePath", mFilePath);

			File file = new File(mFilePath);

			FileOutputStream stream = new FileOutputStream(file);

            Document document = new Document();
            PdfWriter.getInstance(document, stream);
            document.open();
            addMetaData(document);
            addTitlePage(document);
            //addContent(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // iText allows to add metadata to the PDF which can be viewed in your Adobe
    // Reader
    // under File -> Properties
    private void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Miha");
        document.addCreator("Miha");
    }

    private void addTitlePage(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph("Time report", catFont));

        addEmptyLine(preface, 1);
        // Will create: Report generated by: _name, _date
		TextView startDateView = (TextView) findViewById(R.id.dateStartText);
		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
		String date = startDateView.getText().toString();
		String day = startDayView.getText().toString();
        preface.add(new Paragraph(
                "Report generated by: " + myUserName + ", " + day + " " + date, smallBold));
        addEmptyLine(preface, 3);



        document.add(preface);


		Anchor anchor = new Anchor("", catFont);
		anchor.setName("");
		// Second parameter is the number of the chapter
		Chapter catPart = new Chapter(new Paragraph(anchor), 1);
		Paragraph subPara = new Paragraph("", subFont);
		Section subCatPart = catPart.addSection(subPara);
		// add a list
		//createList(subCatPart);
		//Paragraph paragraph = new Paragraph();
		//addEmptyLine(paragraph, 1);
		//subCatPart.add(paragraph);
		// add a table
		createTable(subCatPart);

		// now add all this to the document
		document.add(catPart);

        // Start a new page
        document.newPage();
    }

    private void addContent(Document document) throws DocumentException {
        Anchor anchor = new Anchor("First Chapter", catFont);
        anchor.setName("First Chapter");

        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 1);

        Paragraph subPara = new Paragraph("Subcategory 1", subFont);
        Section subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("Hello"));

        subPara = new Paragraph("Subcategory 2", subFont);
        subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("Paragraph 1"));
        subCatPart.add(new Paragraph("Paragraph 2"));
        subCatPart.add(new Paragraph("Paragraph 3"));

        // add a list
        createList(subCatPart);
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph, 5);
        subCatPart.add(paragraph);

        // add a table
        createTable(subCatPart);

        // now add all this to the document
        document.add(catPart);

        // Next section
        anchor = new Anchor("Second Chapter", catFont);
        anchor.setName("Second Chapter");

        // Second parameter is the number of the chapter
        catPart = new Chapter(new Paragraph(anchor), 1);

        subPara = new Paragraph("Subcategory", subFont);
        subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("This is a very important message"));

        // now add all this to the document
        document.add(catPart);

    }

    private void createTable(Section subCatPart) throws BadElementException {
        PdfPTable table = new PdfPTable(8);

        // t.setBorderColor(BaseColor.GRAY);
        // t.setPadding(4);
        // t.setSpacing(4);
        // t.setBorderWidth(1);

        PdfPCell c1 = new PdfPCell(new Phrase("Time"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Project"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Task"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Activity"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Location"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Costplace"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Description"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Hours"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
        table.setHeaderRows(1);

		int total = 0;
		int myNum = 0;
		String value = "";
		for (HashMap<String, String> map : itemListReport)
			for (Entry<String, String> entry : map.entrySet())
			{
				if (entry.getKey() == "date_time")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "name")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "task")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "activity")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "location")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "costplace")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "description")
				{
					try {
						value = entry.getValue().toString();
						table.addCell(value);
					} catch(NumberFormatException nfe) {
					}
				}
				if (entry.getKey() == "hours")
				{
					try {
						myNum = Integer.parseInt(entry.getValue().toString());
						table.addCell(String.valueOf(MyTimeUtils.getFormattedHourMinutes(myNum)));
					} catch(NumberFormatException nfe) {
						myNum = 0;
					}
					total = total + myNum;
				}
			}


		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("Total: " + MyTimeUtils.getFormattedHourMinutes(total));

        subCatPart.add(table);
    }

    private static void createList(Section subCatPart) {
        com.itextpdf.text.List list = new com.itextpdf.text.List(true, false, 10);
        list.add(new ListItem("First point"));
        list.add(new ListItem("Second point"));
        list.add(new ListItem("Third point"));
        subCatPart.add(list);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }




	/**
	 * Shows a Progress Dialog 
	 *
	 * @param msg
	 */
	public void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	
}
