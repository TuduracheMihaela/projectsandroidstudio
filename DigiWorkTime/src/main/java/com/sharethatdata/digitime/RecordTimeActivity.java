package com.sharethatdata.digitime;


import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import com.sharethatdata.digitime.customsearchspinner.SearchableSpinner;
import com.sharethatdata.digitime.news.NewsActivity;
import com.sharethatdata.digitime.notify.timerecord.MyReceiverProgress;
import com.sharethatdata.digitime.notify.timerecord.MyReceiverToday;
import com.sharethatdata.digitime.notify.timerecord.MyReceiverYesterday;
import com.sharethatdata.digitime.projects.MyProjectsListActivity;
import com.sharethatdata.digitime.tasks.MyTasksListActivity;
import com.sharethatdata.digitime.users.UsersListActivity;
import com.sharethatdata.sharedactivity.UserProfileDetail;
import com.sharethatdata.sharedactivity.WhereIsActivity;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cTaskProject;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cTask;
import com.sharethatdata.webservice.datamodel.cTimeRecord;
import com.sharethatdata.webservice.datamodel.cCustomer;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class RecordTimeActivity extends AppCompatActivity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider2 = null;
	WSDataProvider MyProvider3 = null;
	WSDataProvider MyProvider4 = null;

	TimeUtils MyTimeUtils = null;
	Utility MyUtility = null;
	
	private PendingIntent pendingIntentYesterday;
	private PendingIntent pendingIntentToday;
	private PendingIntent pendingIntentProgress;
		
	private int mStartHour,mStartMinute;
	private int mEndHour,mEndMinute;
	private int mStartYear,mStartMonth,mStartDay;
	
	ArrayList<HashMap<String, String>> taskList;
	ArrayList<HashMap<String, String>> customerList;
	ArrayList<HashMap<String, String>> projectList;
	ArrayList<HashMap<String, String>> locationList;
	ArrayList<HashMap<String, String>> activityList;
	ArrayList<HashMap<String, String>> costplacesList;
	
	List<String> taskkeyArray =  new ArrayList<String>();
	List<String> customerkeyArray =  new ArrayList<String>();
	List<String> projectkeyArray =  new ArrayList<String>();
    List<String> locationkeyArray =  new ArrayList<String>();
    List<String> activitykeyArray =  new ArrayList<String>();
    List<String> costplacekeyArray =  new ArrayList<String>();
    
    cTimeRecord myRecord;
    
	Spinner spinner = null;
	ProgressDialog progressDialog;
	
	public static final String inputFormat = "HH:mm";
	SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);
	
    private boolean no_network = false;
    private boolean no_register = false;
    private boolean input_correct = false;
    private boolean duplicate = false;
    private String error_message = "";

    private String projectID = "";
    private int indexProject = 0;
	private String project_costplace = "";
    
    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";
	private String myReportUser = "";

	private String last_location = "";

	boolean edit_once = true;
     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_record);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		// avoid automatically appear android keyboard when activity start
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
		boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);
		 // time in morning from settings
		String startDay = sharedPrefs.getString("prefWorkDayStart", "");
		 // time in evening from settings
		String endDay = sharedPrefs.getString("prefWorkDayEnd", "");
						
		Globals = ((MyGlobals) getApplicationContext());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider2 = new WSDataProvider(myUser, myPass);
		MyProvider3 = new WSDataProvider(myUser, myPass);
		MyProvider4 = new WSDataProvider(myUser, myPass);

	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyProvider3.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyProvider4.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		MyTimeUtils = new TimeUtils();
		MyUtility = new Utility();

		MyUtility.setOrientation(RecordTimeActivity.this, RecordTimeActivity.this);
		
    	// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    mEndHour = c.get(Calendar.HOUR_OF_DAY);
	    mEndMinute = c.get(Calendar.MINUTE);
	    
	    
	 // time in morning from settings
	    String hourS = null;
        String minS = null;
		
		if(!startDay.isEmpty()){
			String[] tokens = startDay.split(":");
            hourS = tokens[0];
            minS = tokens[1];
            
            mStartHour = Integer.parseInt(hourS);
            mStartMinute = Integer.parseInt(minS);
            
            System.out.println("SH " + mStartHour);
            System.out.println("SM " + mStartMinute);
            System.out.println("SSH " + hourS);
            System.out.println("SSM " + minS);
		}
		
		 // time in evening from settings
		String hourE = null;
        String minE = null;
		
		if(!endDay.isEmpty()){
			String[] tokens = endDay.split(":");
            hourE = tokens[0];
            minE = tokens[1];
            
            if(mEndHour >= Integer.parseInt(hourE) && mEndMinute > Integer.parseInt(minE)){
    			mEndHour =  Integer.parseInt(hourE);
    			mEndMinute = Integer.parseInt(minE);	
    		}
		}
		
	    
	    TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

	    TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
        
        calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
        
        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
        
        TextView startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
            .append(pad(mStartMonth + 1)).append("-")
            .append(pad(mStartYear)));
        
        EditText editDesc = (EditText) findViewById(R.id.textDescription);       
	    editDesc.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.textDescription) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                    }
                }
                return false;
            }
        });
	    
	    Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
	    String edit = Globals.getValue("edit");
	    final String id = Globals.getValue("id");
	    if(edit != ""){
	    	btnSubmit.setText("Submit edit");
	    }
	  
	    // start service
	    Intent i= new Intent(getBaseContext(), MyService.class);
	    // potentially add data to the intent
	    i.putExtra("WAIT", "100");
	    getBaseContext().startService(i); 
	
		btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				
				  input_correct = IsInputCorrect();
				  
				  String edit = Globals.getValue("edit");
				  if(edit != "" && input_correct){
					  EditRecordTask update;
					  update = new EditRecordTask(id);
					  update.execute();
					  
				  }else if (edit == "" && input_correct){ 
					  RegisterTask register;
					  register = new RegisterTask();
					  register.execute();

				  }
		
			  }
		 
		});

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		// notification checked from settings
		if (blNotifications)
		{

			if (!prefs.getBoolean("firstTime", false)) {
				// run one time code here
				notificationYesterday();
				notificationToday();
				notificationProgress();

				// mark first time has run.
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean("firstTime", true);
				editor.commit();
			}else{

				//checking if alarm is working - yesterday
				//boolean alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
						//new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);

				boolean alarmUpYesterday = false;
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
					//checking if alarm is working - yesterday
					alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
							new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_MUTABLE) != null);
				}
				else
				{
					//checking if alarm is working - yesterday
					alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
							new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
				}

				if (alarmUpYesterday) {
					System.out.print("k yesterday");
				}

				//checking if alarm is working - today
				//boolean alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
						//new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);


				boolean alarmUpToday = false;
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
					//checking if alarm is working - today
					alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
							new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_MUTABLE) != null);
				}
				else
				{
					//checking if alarm is working - today
					alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
							new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
				}

				if (alarmUpToday) {
					System.out.print("k today");
				}

				//checking if alarm is working - progress
				//boolean alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
						//new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);

				boolean alarmUpProgress = false;
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
					//checking if alarm is working - progress
					alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
							new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_MUTABLE) != null);
				}
				else
				{
					//checking if alarm is working - progress
					alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
							new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
				}

				if (alarmUpProgress) {
					System.out.print("k progress");
				}



			}
	
		}else{
			// notification unchecked from settings
			try {
				//Intent intent1 = new Intent(RecordTimeActivity.this, MyReceiverYesterday.class);
				//pendingIntentYesterday = PendingIntent.getBroadcast(RecordTimeActivity.this, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

				//Intent intent2 = new Intent(RecordTimeActivity.this, MyReceiverToday.class);
				//pendingIntentToday = PendingIntent.getBroadcast(RecordTimeActivity.this, 2, intent2, PendingIntent.FLAG_UPDATE_CURRENT);

				//Intent intent3 = new Intent(RecordTimeActivity.this, MyReceiverProgress.class);
				//pendingIntentProgress = PendingIntent.getBroadcast(RecordTimeActivity.this, 3, intent3, PendingIntent.FLAG_UPDATE_CURRENT);

      		  AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                  	manager.cancel(pendingIntentYesterday);
                  	manager.cancel(pendingIntentToday);
                  	manager.cancel(pendingIntentProgress);
                  System.out.println("Alarm canceled ");
      		} catch (Exception e) {
      		    System.out.println("Error when cancelling: "+e.toString());
      		}
			
			boolean isAlarmActiveYesterday = (pendingIntentYesterday != null);
			boolean isAlarmActiveToday = (pendingIntentYesterday != null);
			boolean isAlarmActiveProgress = (pendingIntentProgress!= null);

			if(!isAlarmActiveYesterday && !isAlarmActiveToday && !isAlarmActiveProgress){
				// all alarms are canceled, so set SharedPreferences
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean("firstTime", false);
				editor.commit();
			}

     	   if (isAlarmActiveYesterday ) {
     	     Log.d("TAG", "Alarm Yesterday is already active ");
     	   }else{
     		   Log.d("TAG", "Alarm Yesterday is not active ");
     	   }
     	  if (isAlarmActiveToday ) {
      	     Log.d("TAG", "Alarm Today is already active ");
      	   }else{
      		   Log.d("TAG", "Alarm Today is not active ");
      	   }
     	 if (isAlarmActiveProgress ) {
      	     Log.d("TAG", "Alarm Progress is already active ");
      	   }else{
      		   Log.d("TAG", "Alarm Progress is not active ");
      	   }

      	   // Not working properly - I cancel the alarm, but still return true the alarm, should be false
			//checking if alarm is working - yesterday
			//boolean alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
			//		new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);

			boolean alarmUpYesterday = false;

			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
						new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_MUTABLE) != null);
			}else{
				alarmUpYesterday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 1,
						new Intent(RecordTimeActivity.this, MyReceiverYesterday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
			}

			if (alarmUpYesterday) {
				System.out.print("k yesterday");
			}

			//checking if alarm is working - today
			//boolean alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
					//new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
			boolean alarmUpToday = false;

			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
						new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_MUTABLE) != null);
			}else{
				alarmUpToday = (PendingIntent.getBroadcast(RecordTimeActivity.this, 2,
						new Intent(RecordTimeActivity.this, MyReceiverToday.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
			}

			if (alarmUpToday) {
				System.out.print("k today");
			}

			//checking if alarm is working - progress
			//boolean alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
					//new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);

			boolean alarmUpProgress = false;

			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
						new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_MUTABLE) != null);
			}else{
				alarmUpProgress = (PendingIntent.getBroadcast(RecordTimeActivity.this, 3,
						new Intent(RecordTimeActivity.this, MyReceiverProgress.class), PendingIntent.FLAG_UPDATE_CURRENT) != null);
			}

			if (alarmUpProgress) {
				System.out.print("k progress");
			}
		}

	    new LoadPreviousDataTask("0").execute();

	    	// load spinner data
			new LoadList().execute();

		//throw new RuntimeException("This is a crash");
	}

////////////////////////////////////////////////////////////////////////////

	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String edit = Globals.getValue("edit");
		String load_project_spinner = Globals.getValue("load_project_spinner");
	    if(edit == ""){
			if(load_project_spinner.equals("true")){
				Globals.setValue("load_project_spinner", "");
				new LoadList().execute();
			}else{

				EditText editText = (EditText) findViewById(R.id.textDescription);
				editText.setText(null);

				new LoadPreviousDataTask("0").execute();
			}

	    }

	    String refresh = Globals.getValue("refresh_option");
		if(!refresh.equals("")){
			Globals.setValue("refresh_option","");
			new LoadList().execute();
		}
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();

		super.onDestroy();
	}

	public void notificationYesterday(){
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		String startDay = sharedPrefs.getString("prefWorkDayStart", "");
		
		final Calendar date = Calendar.getInstance();

        date.clear();
        date.set(mStartYear, mStartMonth, mStartDay);
        
        String hourS = null;
        String minS = null;
		
    		if(!startDay.isEmpty()){
    			String[] tokens = startDay.split(":");
                hourS = tokens[0];
                minS = tokens[1];
    		}else{
    			hourS = "09";
    			minS = "00";
    		}

          	  Calendar calendar = Calendar.getInstance();
          	  calendar.clear();
	           	     
    	      calendar.get(Calendar.MONTH);
    	      calendar.get(Calendar.YEAR);
    	      calendar.get(Calendar.DAY_OF_MONTH);
    	      
    	      calendar.setTimeInMillis(System.currentTimeMillis());
    	 
    	      calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourS));
    	      calendar.set(Calendar.MINUTE, Integer.parseInt(minS));

    	      //calendar.set(Calendar.HOUR_OF_DAY, 12);
    	      //calendar.set(Calendar.MINUTE, 28);
    	      calendar.set(Calendar.SECOND, 00);
    	      //calendar.set(Calendar.AM_PM, Calendar.PM);
    	      
    	      System.out.println("Time morning set: " + hourS + ":" + minS);
        	      
        	      long when = calendar.getTimeInMillis(); 
          	  
        	    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(RecordTimeActivity.this, MyReceiverYesterday.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //pendingIntentYesterday = PendingIntent.getBroadcast(RecordTimeActivity.this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

				//PendingIntent pendingIntent = null;
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
					pendingIntentYesterday = PendingIntent.getBroadcast
							(RecordTimeActivity.this, 1, intent, PendingIntent.FLAG_MUTABLE);
				}
				else
				{
					pendingIntentYesterday = PendingIntent.getBroadcast
							(RecordTimeActivity.this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				}

				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntentYesterday);

				//alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntentYesterday);
	}
	
	public void notificationToday(){
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		String endDay = sharedPrefs.getString("prefWorkDayEnd", "");
		
		final Calendar date = Calendar.getInstance();

        date.clear();
        date.set(mStartYear, mStartMonth, mStartDay);
        
        String hourE = null;
        String minE = null;
		
    		if(!endDay.isEmpty()){
    			String[] tokens = endDay.split(":");
                hourE = tokens[0];
                minE = tokens[1];
    		}else{
    			hourE = "18";
    			minE = "00";
    		}

          	  Calendar calendar = Calendar.getInstance();
          	  calendar.clear();
	           	     
    	      calendar.get(Calendar.MONTH);
    	      calendar.get(Calendar.YEAR);
    	      calendar.get(Calendar.DAY_OF_MONTH);
    	      
    	      calendar.setTimeInMillis(System.currentTimeMillis());
    	 
    	      calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourE));
    	      calendar.set(Calendar.MINUTE, Integer.parseInt(minE));

          	  //calendar.set(Calendar.HOUR_OF_DAY, 12);
    	      //calendar.set(Calendar.MINUTE, 28);
    	      calendar.set(Calendar.SECOND, 00);
    	      //calendar.set(Calendar.AM_PM, Calendar.PM);
        	      
        	    System.out.println("Time morning set: " + hourE + ":" + minE);
        	      
        	      long when = calendar.getTimeInMillis(); 
          	  
          	AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(RecordTimeActivity.this, MyReceiverToday.class);
            //pendingIntentToday = PendingIntent.getBroadcast(RecordTimeActivity.this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			//PendingIntent pendingIntent = null;
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				pendingIntentToday = PendingIntent.getBroadcast
						(RecordTimeActivity.this, 2, intent, PendingIntent.FLAG_MUTABLE);
			}
			else
			{
				pendingIntentToday = PendingIntent.getBroadcast
						(RecordTimeActivity.this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			}

			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntentToday);
			//alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntentToday);
		
	}
	
	public void notificationProgress(){
		
		final Calendar date1 = Calendar.getInstance();

        date1.clear();
        date1.set(mStartYear, mStartMonth, mStartDay);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
       	     
        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.DAY_OF_MONTH);
	      
        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
	    //calendarToday.set(Calendar.AM_PM, Calendar.PM);

        	      
	      long when = calendar.getTimeInMillis(); 
  	  
	      AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
	      Intent intent = new Intent(RecordTimeActivity.this, MyReceiverProgress.class);
	      //pendingIntentProgress = PendingIntent.getBroadcast(RecordTimeActivity.this, 3, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			//PendingIntent pendingIntent = null;
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				pendingIntentProgress = PendingIntent.getBroadcast
						(RecordTimeActivity.this, 3, intent, PendingIntent.FLAG_MUTABLE);
			}
			else
			{
				pendingIntentProgress = PendingIntent.getBroadcast
						(RecordTimeActivity.this, 3, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			}

		  alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntentProgress);

		//Intent intent = new Intent(RecordTimeActivity.this, MyReceiverProgress.class);
		//sendBroadcast(intent);

	}
	
	private boolean IsInputCorrect()
	{
		// fields filled
		boolean fields_filled = true;
			
		// user selected
		return fields_filled;
		
	}

	
	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         showProgressDialog(getString(R.string.loading_alert_dialog) + list_name);
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {

	    	customerList = new ArrayList<HashMap<String, String>>();
	    	projectList = new ArrayList<HashMap<String, String>>();
	    	locationList = new ArrayList<HashMap<String, String>>();
	    	activityList = new ArrayList<HashMap<String, String>>();
	    	costplacesList = new ArrayList<HashMap<String, String>>();
	    	taskList = new ArrayList<HashMap<String, String>>();

			String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
			myReportUser = report_user;
			if(myReportUser.equals("")){
				report_user = myUserID;
				myReportUser = myUserID;
			}

			// load data from provider
			List<cCustomer> custList = MyProvider.getCustomerSpinner(report_user);
			for(cCustomer entry : custList)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  Long.toString(entry.id));
				map.put("name", entry.name);

				// adding HashList to ArrayList
				customerList.add(map);
			}

	    	// load data from provider
    		List<cProject> projList = MyProvider.getProjectsSpinner(report_user);
    		for(cProject entry : projList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
				map.put("costplace", entry.costplace);
              
                // adding HashList to ArrayList
                projectList.add(map);
             }
    
    		/*List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
    		for(cUserLocation entry : locList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                locationList.add(map);
             }*/
    
    		/*List<cActivity> actList = MyProvider.getUserActivity(report_user);
    		for(cActivity entry : actList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                activityList.add(map);
             }*/
    
    		/*List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
    		for(cCostplace entry : costList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                costplacesList.add(map);
             }*/

			/*List<cTask> newtaskList = null;
			if(!report_user.equals(""))
				newtaskList = MyProvider.getProjectTasks(report_user);
			else
				newtaskList = MyProvider.getProjectTasks(myUserID);


    		for(cTask entry : newtaskList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                taskList.add(map);
             }*/

	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.error_network);
	         	
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
			if (RecordTimeActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
					public void run() {
	            	
	            	/*if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"
	    			 	            		   	 
	            	} else {*/
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	
		            	// store the id of the item in a separate array
						final String edit = Globals.getValue("edit");

		           	    List<String> locationArray =  new ArrayList<String>();
						List<String> projectArray =  new ArrayList<String>();
						List<String> customerArray =  new ArrayList<String>();
						final List<String> projectCostplaceArray =  new ArrayList<String>();
						List<String> activityArray =  new ArrayList<String>();
						List<String> costplaceArray =  new ArrayList<String>();
						List<String> taskArray =  new ArrayList<String>();

						projectkeyArray.clear();

						if(edit == "") {
							locationArray.add(0, "Select location");
							locationkeyArray.add(0, "");

							projectArray.add(0, "Select a project");
							projectkeyArray.add(0, "");

							activityArray.add(0, "Select activity");
							activitykeyArray.add(0, "");

							costplaceArray.add(0, "Select costplace");
							costplacekeyArray.add(0, "");

							projectCostplaceArray.add(0, "");
						}

							taskArray.add(0, "No task");
							taskkeyArray.add(0, "");

		           	    // load spinner data from dataprovider
		           	    for (HashMap<String, String> map : projectList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") projectArray.add(entry.getValue());
								if (entry.getKey() == "costplace") projectCostplaceArray.add(entry.getValue());
		           	        }
		           	    for (HashMap<String, String> map : locationList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") locationArray.add(entry.getValue());
		           	        }
		           	  	for (HashMap<String, String> map : activityList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") activityArray.add(entry.getValue());
		           	        }
		           		for (HashMap<String, String> map : costplacesList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
		           	        }
						for (HashMap<String, String> map : taskList)
							for (Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "id") taskkeyArray.add(entry.getValue());
								if (entry.getKey() == "name") taskArray.add(entry.getValue());
							}

					   	for (HashMap<String, String> map : customerList)
						   for (Entry<String, String> entry : map.entrySet())
						   {
							if (entry.getKey() == "id") customerkeyArray.add(entry.getValue());
							if (entry.getKey() == "name") customerArray.add(entry.getValue());
						   }
		           		
	           	    	final ArrayAdapter<String> location_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, locationArray);
	           	    	location_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	final Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
	           	       	sLocations.setAdapter(location_adapter);


						int spinnerPositionLocation = location_adapter.getPosition(last_location);
						//set the default according to value
						sLocations.setSelection(spinnerPositionLocation);

	           	    
	           	    	final ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, projectArray);
	           	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final SearchableSpinner sProjects = (SearchableSpinner) findViewById(R.id.spinnerProject);
	           	    	sProjects.setAdapter(project_adapter);
	           	    
	           	    	final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, activityArray);
	           	    	activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
	           	    	sActivities.setAdapter(activity_adapter);
	           	       
	           	    	final ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
	           	    	costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
	           	    	sCostPlaces.setAdapter(costplaces_adapter);
	           	       
	           	    	ArrayAdapter<String> task_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, taskArray);
	           	    	task_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
	           	    	sTasks.setAdapter(task_adapter);

						final ArrayAdapter<String> customer_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, customerArray);
						customer_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						final SearchableSpinner sCustomers = (SearchableSpinner) findViewById(R.id.spinnerCustomer);
						sCustomers.setAdapter(customer_adapter);

	            	//}
	           	    	
	           	  // set edit parameters for edit time report

	        	      //  if(edit == ""){
	           	    	
		           	    	sProjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		        			    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		        			        // Your code here
		        				    Spinner sProjects = (Spinner) findViewById(R.id.spinnerProject);
		        				    String mIndex = sProjects.getSelectedItem().toString();
		        				    
		        				    if(mIndex.contains("Select a project")){
		        				    	
		        				    	
		        				    }else{
	
			        			    	int myIndex = sProjects.getSelectedItemPosition();
			    						indexProject = myIndex;
			    						System.out.println("Position: " + i);
										projectID = projectkeyArray.get(myIndex);
										project_costplace = projectCostplaceArray.get(myIndex);

										//if(edit == "") {
											// load spinner data
											new LoadListIfProjectSelected().execute();
										//}
		        				    }
			        			    	
		        			    } 
	
		        			    public void onNothingSelected(AdapterView<?> adapterView) {
		        			        return;
		        			    } 
		        			});
	        	      //  }
	           	    	
	           	    	
	           	     // set edit parameters for edit time report
	        	 	    String id = Globals.getValue("id");
	        	 	    String date = Globals.getValue("date");
	        			String starttime = Globals.getValue("starttime");
	        			String endtime = Globals.getValue("endtime");
	        			String locationid = Globals.getValue("locationid");
	        			String location = Globals.getValue("location");
	        			String projectid = Globals.getValue("projectid");
	        			String project = Globals.getValue("project");
	        			String customerid = Globals.getValue("customerid");
	        			String customer = Globals.getValue("customer");
	        			String taskid = Globals.getValue("taskid");
	        			String task = Globals.getValue("task");
	        			String activityid = Globals.getValue("activityid");
	        			String activity = Globals.getValue("activity");
	        			String costplaceid = Globals.getValue("costplaceid");
	        			String costplace = Globals.getValue("costplace");
	        			String description = Globals.getValue("description");
	        			
	        			if(edit != ""){
	        				 // date
	        		        StringTokenizer tkDate = new StringTokenizer(date);
        		        	String dateTokenizer = tkDate.nextToken();  // <---  yyyy-mm-dd
	        		        String[] separatedDate = dateTokenizer.split("-");
	        		        String year = separatedDate[0];
	        		        String month = separatedDate[1];
	        		        String day = separatedDate[2];
	        		        

	        		        // time start
	        		        StringTokenizer tkTimeStart = new StringTokenizer(starttime);
	        		        String dateStartTokenizer = tkTimeStart.nextToken();  // <---  yyyy-mm-dd
	        		        String timeStartTokenizer = tkTimeStart.nextToken();  // <---  hh:mm:ss
	        		        String[] separatedTimeStart = timeStartTokenizer.split(":");
	        		        String hourStart = separatedTimeStart[0];
	        		        String minuteStart = separatedTimeStart[1];
	        		        
	        		        // time end
	        		        StringTokenizer tkTimeEnd = new StringTokenizer(endtime);
	        		        String dateEndTokenizer = tkTimeEnd.nextToken();  // <---  yyyy-mm-dd
	        		        String timeEndTokenizer = tkTimeEnd.nextToken();  // <---  hh:mm:ss
	        		        String[] separatedTimeEnd = timeEndTokenizer.split(":");
	        		        String hourEnd = separatedTimeEnd[0];
	        		        String minuteEnd = separatedTimeEnd[1];
	        		        
	        				
	        				Calendar c = new GregorianCalendar();
	        				c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
	        				c.set(Calendar.MONTH, Integer.valueOf(month)-1);
	        				c.set(Calendar.YEAR, Integer.valueOf(year));
	        				
	        			    mStartYear = Integer.valueOf(year);
	        			    mStartMonth = Integer.valueOf(month);
	        			    mStartDay = Integer.valueOf(day);
	        			    mStartHour = Integer.valueOf(hourStart);
	        			    mStartMinute = Integer.valueOf(minuteStart);
	        			    mEndHour = Integer.valueOf(hourEnd);
	        			    mEndMinute = Integer.valueOf(minuteEnd);
	        			    
	        			    TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	        		        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

	        			    TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	        		        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
	        		        
	        		        //  String weekdayname = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-6)).toString();
	        		        
	        		        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	        		        /*String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];*/
	        			    
	        			    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
	        			    String weekdayname = weekdays[dayOfWeek];
	        			    System.out.println("dayOfWeek: " + dayOfWeek);
	        			    System.out.println("weekdays[dayOfWeek]: " + weekdays[dayOfWeek]);
	        			    
	        			    System.out.println(c.get(Calendar.DAY_OF_WEEK)-Calendar.SUNDAY);
	        		        
	        		        TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	        		        startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
	        		            .append(pad(mStartMonth)).append("-")
	        		            .append(pad(mStartYear)));
	        		        
	        		        calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);

	        		       
	        		        System.out.println(mStartYear + " " + mStartMonth + " " + mStartDay + " " + mStartHour + " " + 
	        		        mStartMinute + " " + mEndHour + " " + mEndMinute);
	        		        
	        		        System.out.println(startTimeView.getText().toString());
	        		        System.out.println(endTimeView.getText().toString());
	        		        System.out.println(startDateView.getText().toString());
	        		        
	           		    	int spinnerPosition1 = location_adapter.getPosition(location);
	           		    	//set the default according to value
	           		    	sLocations.setSelection(spinnerPosition1);

	           		    	int spinnerPosition2 = project_adapter.getPosition(project);
	        	    		//set the default according to value
	           		    	sProjects.setSelection(spinnerPosition2);

	           		    	int spinnerPosition3 = activity_adapter.getPosition(activity);
	        	    		//set the default according to value
	           		    	sActivities.setSelection(spinnerPosition3);

	           		    	int spinnerPosition4 = costplaces_adapter.getPosition(costplace);
	        	    		//set the default according to value
	           		    	sCostPlaces.setSelection(spinnerPosition4);

							int spinnerPosition5 = customer_adapter.getPosition(customer);
							//set the default according to value
							sCustomers.setSelection(spinnerPosition5);

							int spinnerPosition6 = task_adapter.getPosition(task);
							//set the default according to value
							sTasks.setSelection(spinnerPosition6);

							TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
							DescriptionView.setText(description);



							//int myIndex = sProjects.getSelectedItemPosition();

    						//indexProject = myIndex;
    						indexProject = spinnerPosition2;
							//projectID = projectkeyArray.get(myIndex);
							projectID = projectid;
							//project_costplace = projectCostplaceArray.get(myIndex);
							project_costplace = costplaceid;

    							// load spinner data
    								//new LoadListIfProjectSelected().execute();
	           		    }
	              }
	        });
	              
	    }

	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadListIfProjectSelected extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	//projectList = new ArrayList<HashMap<String, String>>();
	    	String edit = Globals.getValue("edit");
	        //if(edit == ""){
	        	// I am not editing
		    	locationList = new ArrayList<HashMap<String, String>>();
		    	activityList = new ArrayList<HashMap<String, String>>();
		    	costplacesList = new ArrayList<HashMap<String, String>>();

				locationkeyArray.clear();
				activitykeyArray.clear();
				costplacekeyArray.clear();
	        //}
	    	taskList = new ArrayList<HashMap<String, String>>();

	    	taskkeyArray.clear();
	    	
	    	// load data from provider
/*    		List<cProject> projList = MyProvider.getProjects();
    		for(cProject entry : projList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                projectList.add(map);
             } */


			String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
	    	if(report_user.equals("")){
				report_user = myUserID;
			}
			edit = Globals.getValue("edit");
	        //if(edit == ""){
	        	// I am not editing

				List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
				for(cUserLocation entry : locList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					locationList.add(map);
				}

	    		List<cActivity> actList = MyProvider.getUserActivity(report_user);
	    		for(cActivity entry : actList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("id",  entry.id);
	                map.put("name", entry.name);
	              
	                // adding HashList to ArrayList
	                activityList.add(map);
	             }
	    
	    		List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
	    		for(cCostplace entry : costList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("id",  Long.toString(entry.id));
	                map.put("name", entry.name);
	              
	                // adding HashList to ArrayList
	                costplacesList.add(map);
	             }
	        //}

				List<cTask> newtaskListFiltered = null;
				if(!report_user.equals(""))
					newtaskListFiltered = MyProvider.getTasksProject(projectID, report_user);
				else
    				newtaskListFiltered = MyProvider.getTasksProject(projectID, myUserID);

        		for(cTask entry : newtaskListFiltered)
             	{
        			// creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("id",  Long.toString(entry.id));
                    map.put("name", entry.name);
                  
                    // adding HashList to ArrayList
                    taskList.add(map);
                 }
	   		
	    	//if (itemList.isEmpty())
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.error_network);
	         	
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
			if (RecordTimeActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	/*if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"
	    			 	            		   	 
	            	} else {*/
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
	            	
		            	// store the id of the item in a separate array
		             
		           	    List<String> locationArray =  new ArrayList<String>();
		           	 /* List<String> projectArray =  new ArrayList<String>();*/
		           	    List<String> activityArray =  new ArrayList<String>();
		           	    List<String> costplaceArray =  new ArrayList<String>();
		           	    List<String> taskArray =  new ArrayList<String>();
   
		           	    // load spinner data from dataprovider
		           	 /*  for (HashMap<String, String> map : projectList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") projectArray.add(entry.getValue());
		           	        } */
		           	    for (HashMap<String, String> map : locationList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") locationArray.add(entry.getValue());
		           	        }
		           	 String edit = Globals.getValue("edit");
	        	     //   if(edit == ""){
	        	        	// I am not editing 
			           	  	for (HashMap<String, String> map : activityList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") activityArray.add(entry.getValue());
			           	        }
			           		for (HashMap<String, String> map : costplacesList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
			           	        }
	        	       // }
		           		
			           		if(taskList.size() > 0){
			           			taskArray.add(0, "Select a task");
				           	    taskkeyArray.add(0, "");
				           	    
			           			for (HashMap<String, String> map : taskList)
				           	        for (Entry<String, String> entry : map.entrySet())
				           	        {
				           	        	if (entry.getKey() == "id") taskkeyArray.add(entry.getValue());
				           	        	if (entry.getKey() == "name") taskArray.add(entry.getValue());
				           	        }	
				           		for (String i : taskkeyArray){
				           			System.out.println("taskKeyArray " + i);
				           		}
			           		}else{
			           			taskArray.add(0, "No task");
				           	    taskkeyArray.add(0, "");
			           		}
		           		
		           		
		           		
		           		final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, activityArray);
	           	    	activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
	           	    	sActivities.setAdapter(activity_adapter);
	           	       
	           	    	final ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
	           	    	costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
	           	    	sCostPlaces.setAdapter(costplaces_adapter);
		           		
		           		
	           	    	final ArrayAdapter<String> loc_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, locationArray);
	           	    	loc_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	final Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
	           	       	sLocations.setAdapter(loc_adapter);
	           	    
	           	     /* final ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, projectArray);
	           	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final Spinner sProjects = (Spinner) findViewById(R.id.spinnerProject);
	           	    	sProjects.setAdapter(project_adapter); */
	           	       
	           	    	ArrayAdapter<String> task_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, taskArray);
	           	    	task_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
	           	    	sTasks.setAdapter(task_adapter);

						if(project_costplace != ""){
							int spinnerPositionCostPlace = costplaces_adapter.getPosition(project_costplace);
							sCostPlaces.setSelection(spinnerPositionCostPlace);
						}
	           	    	
	           	    	edit = Globals.getValue("edit");
	        	        if(edit == ""){
	        	        	// I am not editing 

			           	    	sTasks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		        			    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		        			        // Your code here

									Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
									String mIndex = sTasks.getSelectedItem().toString();

									if(mIndex.contains("Select a task") || mIndex.contains("No task")){

									}else{
										int myIndex = sTasks.getSelectedItemPosition();
										String task = String.valueOf(myIndex);
										String taskid = taskkeyArray.get(myIndex);

										new LoadListIfTaskSelected(taskid).execute();
									}



		        			    	/*String compareValueLocation = "In Office";
	        				    	int spinnerPositionLocation = loc_adapter.getPosition(compareValueLocation);
		        			    	sLocations.setSelection(spinnerPositionLocation);
		        			    	
		        			    	String compareValueActivity = "Activity from task";
		        			    	int spinnerPositionActivity = activity_adapter.getPosition(compareValueActivity);
		        			    	sActivities.setSelection(spinnerPositionActivity);

									if(project_costplace == "") {
										String compareValueCostPlace = "Projects";
										int spinnerPositionCostPlace = costplaces_adapter.getPosition(compareValueCostPlace);
										sCostPlaces.setSelection(spinnerPositionCostPlace);
									}*/

		        			    } 
		
		        			    public void onNothingSelected(AdapterView<?> adapterView) {
		        			        return;
		        			    } 
		        			}); 
	           	       
	        	        }
	        	        
	        	        
	        	        
	        	     // set edit parameters for edit time report
	        	 	    String id = Globals.getValue("id");
	        	 	    String date = Globals.getValue("date");
	        			String starttime = Globals.getValue("starttime");
	        			String endtime = Globals.getValue("endtime");
	        			String location = Globals.getValue("location");
	        			String project = Globals.getValue("project");
	        			String task = Globals.getValue("task");
	        			String activity = Globals.getValue("activity");
	        			String costplace = Globals.getValue("costplace");
	        			String description = Globals.getValue("description");


	        			if(edit != ""){
							if(edit_once){
	        				
								 // date
								StringTokenizer tkDate = new StringTokenizer(date);
								String dateTokenizer = tkDate.nextToken();  // <---  yyyy-mm-dd
								String[] separatedDate = dateTokenizer.split("-");
								String year = separatedDate[0];
								String month = separatedDate[1];
								String day = separatedDate[2];


								// time start
								StringTokenizer tkTimeStart = new StringTokenizer(starttime);
								String dateStartTokenizer = tkTimeStart.nextToken();  // <---  yyyy-mm-dd
								String timeStartTokenizer = tkTimeStart.nextToken();  // <---  hh:mm:ss
								String[] separatedTimeStart = timeStartTokenizer.split(":");
								String hourStart = separatedTimeStart[0];
								String minuteStart = separatedTimeStart[1];

								// time end
								StringTokenizer tkTimeEnd = new StringTokenizer(endtime);
								String dateEndTokenizer = tkTimeEnd.nextToken();  // <---  yyyy-mm-dd
								String timeEndTokenizer = tkTimeEnd.nextToken();  // <---  hh:mm:ss
								String[] separatedTimeEnd = timeEndTokenizer.split(":");
								String hourEnd = separatedTimeEnd[0];
								String minuteEnd = separatedTimeEnd[1];


								Calendar c = new GregorianCalendar();
								c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
								c.set(Calendar.MONTH, Integer.valueOf(month)-1);
								c.set(Calendar.YEAR, Integer.valueOf(year));

								mStartYear = Integer.valueOf(year);
								mStartMonth = Integer.valueOf(month);
								mStartDay = Integer.valueOf(day);
								mStartHour = Integer.valueOf(hourStart);
								mStartMinute = Integer.valueOf(minuteStart);
								mEndHour = Integer.valueOf(hourEnd);
								mEndMinute = Integer.valueOf(minuteEnd);

								TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
								startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

								TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
								endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));

								String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
								//String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
								int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
								String weekdayname = weekdays[dayOfWeek];
								System.out.println("dayOfWeek: " + dayOfWeek);
								System.out.println("weekdays[dayOfWeek]: " + weekdays[dayOfWeek]);

								TextView startDateView = (TextView) findViewById(R.id.dateStartText);
								startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
									.append(pad(mStartMonth)).append("-")
									.append(pad(mStartYear)));

								mStartMonth = mStartMonth - 1;


								System.out.println(mStartYear + " " + mStartMonth + " " + mStartDay + " " + mStartHour + " " +
								mStartMinute + " " + mEndHour + " " + mEndMinute);

								System.out.println(startTimeView.getText().toString());
								System.out.println(endTimeView.getText().toString());
								System.out.println(startDateView.getText().toString());

								int spinnerPosition1 = loc_adapter.getPosition(location);
								//set the default according to value
								sLocations.setSelection(spinnerPosition1);

								int spinnerPosition3 = activity_adapter.getPosition(activity);
								//set the default according to value
								sActivities.setSelection(spinnerPosition3);

								int spinnerPosition4 = costplaces_adapter.getPosition(costplace);
								//set the default according to value
								sCostPlaces.setSelection(spinnerPosition4);

								int spinnerPosition5 = task_adapter.getPosition(task);
								//set the default according to value
								sTasks.setSelection(spinnerPosition5);

								TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
								DescriptionView.setText(description);

								edit_once = false;
	           		    }
	            	}

	              }
	        });
	    }

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
	 * */
	class LoadListIfTaskSelected extends AsyncTask<String, String, String> {

		String taskid;
		String activity;

		public LoadListIfTaskSelected(String taskid){
			this.taskid = taskid;
		}
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			//projectList = new ArrayList<HashMap<String, String>>();
			String edit = Globals.getValue("edit");
			if(edit == ""){
				// I am not editing
				locationList = new ArrayList<HashMap<String, String>>();
				activityList = new ArrayList<HashMap<String, String>>();
				costplacesList = new ArrayList<HashMap<String, String>>();

				locationkeyArray.clear();
				activitykeyArray.clear();
				costplacekeyArray.clear();
			}


			String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
			if(report_user.equals("")){
				report_user = myUserID;
			}
			edit = Globals.getValue("edit");
			if(edit == ""){
				// I am not editing

				List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
				for(cUserLocation entry : locList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					locationList.add(map);
				}

				List<cActivity> actList = MyProvider.getUserActivity(report_user);
				for(cActivity entry : actList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name", entry.name);

					// adding HashList to ArrayList
					activityList.add(map);
				}

				List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
				for(cCostplace entry : costList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					costplacesList.add(map);
				}
			}

			cTaskProject taskProject = MyProvider.getTaskProject(taskid);
			activity = taskProject.activity;

			//if (itemList.isEmpty())
			if (MyProvider.json_connected == false)
			{
				no_network = true;
				error_message = getString(R.string.error_network);

			}
			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			if (RecordTimeActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					// store the id of the item in a separate array

					List<String> locationArray = new ArrayList<String>();
					List<String> activityArray = new ArrayList<String>();
					List<String> costplaceArray = new ArrayList<String>();

					for (HashMap<String, String> map : locationList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
							if (entry.getKey() == "name") locationArray.add(entry.getValue());
						}
					String edit = Globals.getValue("edit");
					//   if(edit == ""){
					// I am not editing
					for (HashMap<String, String> map : activityList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
							if (entry.getKey() == "name") activityArray.add(entry.getValue());
						}
					for (HashMap<String, String> map : costplacesList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
							if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
						}
					// }


					final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, activityArray);
					activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
					sActivities.setAdapter(activity_adapter);

					final ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
					costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
					sCostPlaces.setAdapter(costplaces_adapter);

					final ArrayAdapter<String> loc_adapter = new ArrayAdapter<String>(RecordTimeActivity.this, android.R.layout.simple_spinner_item, locationArray);
					loc_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
					sLocations.setAdapter(loc_adapter);

					if (project_costplace != "") {
						int spinnerPositionCostPlace = costplaces_adapter.getPosition(project_costplace);
						sCostPlaces.setSelection(spinnerPositionCostPlace);
					}

					edit = Globals.getValue("edit");
					if (edit == "") {
						// I am not editing

						String compareValueLocation = "In Office";
						int spinnerPositionLocation = loc_adapter.getPosition(compareValueLocation);
						sLocations.setSelection(spinnerPositionLocation);

						int spinnerPositionActivity = activity_adapter.getPosition(activity);
						sActivities.setSelection(spinnerPositionActivity);

						if (project_costplace == "") {
							String compareValueCostPlace = "Projects";
							int spinnerPositionCostPlace = costplaces_adapter.getPosition(compareValueCostPlace);
							sCostPlaces.setSelection(spinnerPositionCostPlace);
						}


						// set edit parameters for edit time report
						String id = Globals.getValue("id");
						String date = Globals.getValue("date");
						String starttime = Globals.getValue("starttime");
						String endtime = Globals.getValue("endtime");
						String location = Globals.getValue("location");
						String project = Globals.getValue("project");
						String task = Globals.getValue("task");
						String activity = Globals.getValue("activity");
						String costplace = Globals.getValue("costplace");
						String customer = Globals.getValue("customer");
						String description = Globals.getValue("description");


						if (edit != "") {
							if (edit_once) {

								// date
								StringTokenizer tkDate = new StringTokenizer(date);
								String dateTokenizer = tkDate.nextToken();  // <---  yyyy-mm-dd
								String[] separatedDate = dateTokenizer.split("-");
								String year = separatedDate[0];
								String month = separatedDate[1];
								String day = separatedDate[2];


								// time start
								StringTokenizer tkTimeStart = new StringTokenizer(starttime);
								String dateStartTokenizer = tkTimeStart.nextToken();  // <---  yyyy-mm-dd
								String timeStartTokenizer = tkTimeStart.nextToken();  // <---  hh:mm:ss
								String[] separatedTimeStart = timeStartTokenizer.split(":");
								String hourStart = separatedTimeStart[0];
								String minuteStart = separatedTimeStart[1];

								// time end
								StringTokenizer tkTimeEnd = new StringTokenizer(endtime);
								String dateEndTokenizer = tkTimeEnd.nextToken();  // <---  yyyy-mm-dd
								String timeEndTokenizer = tkTimeEnd.nextToken();  // <---  hh:mm:ss
								String[] separatedTimeEnd = timeEndTokenizer.split(":");
								String hourEnd = separatedTimeEnd[0];
								String minuteEnd = separatedTimeEnd[1];


								Calendar c = new GregorianCalendar();
								c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
								c.set(Calendar.MONTH, Integer.valueOf(month) - 1);
								c.set(Calendar.YEAR, Integer.valueOf(year));

								mStartYear = Integer.valueOf(year);
								mStartMonth = Integer.valueOf(month);
								mStartDay = Integer.valueOf(day);
								mStartHour = Integer.valueOf(hourStart);
								mStartMinute = Integer.valueOf(minuteStart);
								mEndHour = Integer.valueOf(hourEnd);
								mEndMinute = Integer.valueOf(minuteEnd);

								TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
								startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

								TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
								endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));

								String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
								//String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
								int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
								String weekdayname = weekdays[dayOfWeek];
								System.out.println("dayOfWeek: " + dayOfWeek);
								System.out.println("weekdays[dayOfWeek]: " + weekdays[dayOfWeek]);

								TextView startDateView = (TextView) findViewById(R.id.dateStartText);
								startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
										.append(pad(mStartMonth)).append("-")
										.append(pad(mStartYear)));

								mStartMonth = mStartMonth - 1;


								System.out.println(mStartYear + " " + mStartMonth + " " + mStartDay + " " + mStartHour + " " +
										mStartMinute + " " + mEndHour + " " + mEndMinute);

								System.out.println(startTimeView.getText().toString());
								System.out.println(endTimeView.getText().toString());
								System.out.println(startDateView.getText().toString());

								int spinnerPosition1 = loc_adapter.getPosition(location);
								//set the default according to value
								sLocations.setSelection(spinnerPosition1);

								int spinnerPosition3 = activity_adapter.getPosition(activity);
								//set the default according to value
								sActivities.setSelection(spinnerPosition3);

								int spinnerPosition4 = costplaces_adapter.getPosition(costplace);
								//set the default according to value
								sCostPlaces.setSelection(spinnerPosition4);


								TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
								DescriptionView.setText(description);

								edit_once = false;
							}
						}

					}
				}
			});
		}

	}
	
	/**
	 * Shows a Progress Dialog 
	 *  
	 * @param msg
	 */
	public void showProgressDialog(String msg) 
	{
		
		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);
			
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			
			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();		
	}	
	
	
	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {
		
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}
	
	
	
////////////////////////////////////////////////////////////////////////////
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.time_record, menu);
		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        case R.id.action_reports:
            Intent intent2 = new Intent(this, ListActivity.class); startActivity(intent2); 
            return true;
        case R.id.action_projects:
            Intent intent6 = new Intent(this, MyProjectsListActivity.class); startActivity(intent6); 
            return true;
        case R.id.action_users:
            Intent intent7 = new Intent(this, UsersListActivity.class); startActivity(intent7);
            return true;
        case R.id.action_news:
            Intent intent8 = new Intent(this, NewsActivity.class); startActivity(intent8);
            return true;
		case R.id.action_whereis:
        	Intent intent10 = new Intent(this, WhereIsActivity.class);
			intent10.putExtra("myUser",myUser);
			intent10.putExtra("myPass",myPass);
			intent10.putExtra("myUserID",myUserID);
			intent10.putExtra("myUserName",myUserName);
			intent10.putExtra("ws_url",Globals.getValue("ws_url"));
			startActivity(intent10);
            return true;
		case R.id.action_profile:
			Intent intent11 = new Intent(this, UserProfileDetail.class);
			intent11.putExtra("myUser",myUser);
			intent11.putExtra("myPass",myPass);
			intent11.putExtra("myUserID",myUserID);
			intent11.putExtra("myUserName",myUserName);
			intent11.putExtra("ws_url",Globals.getValue("ws_url"));
			startActivity(intent11);
			return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	
	private Date parseDate(String date) {

	    try {
	        return inputParser.parse(date);
	    } catch (ParseException e) {
	        return new Date(0);
	    }
	}
	
	private TimePickerDialog.OnTimeSetListener mStartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mStartHour = hourOfDay;
            mStartMinute = minute;
            
            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);		        
            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
                    .append(pad(mStartMinute)));
            
            Date dateCompareStart = parseDate(String.valueOf(mStartHour) + ":" + String.valueOf(mStartMinute));
            Date dateCompareEnd = parseDate(String.valueOf(mEndHour) + ":" + String.valueOf(mEndMinute));
            
            if(dateCompareEnd.before( dateCompareStart )){
            	// start time have to be smaller than end time
				TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
				endTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
						.append(pad(mStartMinute)));
				calculateTotalHours(mStartHour, mStartMinute, mStartHour, mStartMinute);
            }else{
				TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
				String endTime = endTimeView.getText().toString();
				String[] tokens = endTime.split(":");
				mEndHour = Integer.valueOf(tokens[0]);
				mEndMinute = Integer.valueOf(tokens[1]);
				calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
			}
            

        }
    };
    
    private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mEndHour = hourOfDay;
            mEndMinute = minute;
            
            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
                    .append(pad(mEndMinute)));
            
            Date dateCompareStart = parseDate(String.valueOf(mStartHour) + ":" + String.valueOf(mStartMinute));
            Date dateCompareEnd = parseDate(String.valueOf(mEndHour) + ":" + String.valueOf(mEndMinute));
            
            if(dateCompareEnd.before( dateCompareStart )){
            	// start time have to be smaller than end time
				TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
				startTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
						.append(pad(mEndMinute)));
				calculateTotalHours(mEndHour, mEndMinute, mEndHour, mEndMinute);
            }else{
				TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
				String startTime = startTimeView.getText().toString();
				String[] tokens = startTime.split(":");
				mStartHour = Integer.valueOf(tokens[0]);
				mStartMinute = Integer.valueOf(tokens[1]);
				calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
			}
            

        }
    };

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
	 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            
	            String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
	            		
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	            
	            System.out.println("DatePickerDialog before: " + mStartYear + " " + mStartMonth + " " + mStartDay);
	    		        
	            startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("-")
	                .append(pad(mStartMonth + 1)).append("-")
	                .append(pad(mStartYear)));
	            
	            System.out.println("DatePickerDialog after: " + mStartYear + " " + mStartMonth + " " + mStartDay);
	    }
	};

		
	public void startTimeClick(View v) 
	{
//	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
//	    tp1.show();

		TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
		String startTime = startTimeView.getText().toString();
		String[] tokens = startTime.split(":");
		mStartHour = Integer.valueOf(tokens[0]);
		mStartMinute = Integer.valueOf(tokens[1]);
		
	//	CustomTimePickerDialog stp1 = new CustomTimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
		TimePickerDialog stp1 = new TimePickerDialog(this, mStartTimeSetListener, mStartHour, mStartMinute, true);
		stp1.show();
	}
	
	public void endTimeClick(View v) 
	{
//	    TimePickerDialog tp2 = new TimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
//	    tp2.show();

		TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
		String endTime = endTimeView.getText().toString();
		String[] tokens = endTime.split(":");
		mEndHour = Integer.valueOf(tokens[0]);
		mEndMinute = Integer.valueOf(tokens[1]);

		//CustomTimePickerDialog stp2 = new CustomTimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
		TimePickerDialog stp2 = new TimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
		stp2.show();
	}
	
	public void startDateClick(View v) 
	{
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
		
		System.out.println("startDateClick: " + mStartYear + " " + mStartMonth + " " + mStartDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
	    dp1.show();
	}
	
	public boolean SendEmail(String email, String subject, String emailtext)
	{
		Intent intent = new Intent(Intent.ACTION_SENDTO); 
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, emailtext);
		intent.setData(Uri.parse("mailto:"+email)); 
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
		this.startActivity(intent);	
		return true;
	}
	
	
	public boolean isSameDay(Calendar c1, Calendar c2){
	    final int DAY=1000*60*60*24;
	    return ((c1.getTimeInMillis()/DAY)==(c2.getTimeInMillis()/DAY));
	  } // end isSameDay
	
	private void SetStartTime(String startdate)
	{
		TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
		try {
			Date myDate = dateFormat.parse(startdate);
		    
			Calendar currentDate = Calendar.getInstance();
		    Calendar c = Calendar.getInstance();
		    
		    c.setTime(myDate);
		    
		    // only set start time when day is the same, else start at 9:00
		    if (isSameDay(c, currentDate))
		    {
			    mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    mStartMinute = c.get(Calendar.MINUTE);
		    
		    } else {
		    	
		    	//mStartHour = 9;
				//mStartMinute = 0;
			    
		    }
			
		    startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
		    
		    calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
			
				
		} catch (ParseException e) {
		    	
		}

    }
	
	private void calculateTotalHours(int startHour, int startMinute, int endHour, int endMinute){
		Date dateCompareStart = parseDate(String.valueOf(startHour) + ":" + String.valueOf(startMinute));;
		Date dateCompareEnd = parseDate(String.valueOf(endHour) + ":" + String.valueOf(endMinute));
		
		//milliseconds
		long different = dateCompareEnd.getTime() - dateCompareStart.getTime();
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		
		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;
		
		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;
		
		TextView numberOfHoursText = (TextView) findViewById(R.id.hoursText);
		numberOfHoursText.setText(" " + elapsedHours + " hours " + elapsedMinutes + " minutes " );
	}
	

  
   
	
	  /** AsyncTask register time record  */
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {
 
        	  String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        
        	  String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
        	  String myEndTime = (new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();
        	
        	  // extract data
		 
			  Spinner locationSpinner = (Spinner) findViewById(R.id.spinnerLocation);
			  int myIndex = locationSpinner.getSelectedItemPosition();
			  String myLocation = "";
			  if(myIndex != -1){
				  myLocation = locationkeyArray.get(myIndex);		  
			  }
			  			  			  
			  Spinner taskSpinner = (Spinner) findViewById(R.id.spinnerTask);
			  myIndex = taskSpinner.getSelectedItemPosition();
			  String myTask = "";
			  if(myIndex != -1){
				  myTask = taskkeyArray.get(myIndex);
			  }
			  
			  			  			  			  
			  Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
			  myIndex = projectSpinner.getSelectedItemPosition();
			  String myProject = "";
			  if(myIndex != -1){
				  myProject = projectkeyArray.get(myIndex);
			  }
			  
			  
			  Spinner activitySpinner = (Spinner) findViewById(R.id.spinnerActivity);
			  myIndex = activitySpinner.getSelectedItemPosition();
			  String myActivity = "";
			  if(myIndex != -1){
				  myActivity = activitykeyArray.get(myIndex);
			  }
			  
			  
			  Spinner costplaceSpinner = (Spinner) findViewById(R.id.spinnerCostplace);
			  myIndex = costplaceSpinner.getSelectedItemPosition();
			  String myCostplace = "";
			  if(myIndex != -1){
				  myCostplace = costplacekeyArray.get(myIndex);
			  }

			  Spinner customerSpinner = (Spinner) findViewById(R.id.spinnerCustomer);
			  myIndex = customerSpinner.getSelectedItemPosition();

			  String myCustomer = "";
			  if(myIndex != -1){
			  	myCustomer = customerkeyArray.get(myIndex);
			  }

			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();

			  // call dataprovider
			  String emailmessage = "New Time Record " + "\r\n";
			  emailmessage = emailmessage + "Date = " + myDateTime + "\r\n";
			  emailmessage = emailmessage + "Start = " + myStartTime + "\r\n";
			  emailmessage = emailmessage + "End = " + myEndTime + "\r\n";
			  
			  emailmessage = emailmessage + "Location = " + myLocation + "\r\n";
			  emailmessage = emailmessage + "Project = " + myProject + "\r\n";
			  emailmessage = emailmessage + "Activity = " + myActivity + "\r\n";
			  
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;
			 // ArrayList<String> nr = null;
			 // nr = MyProvider.getDuplicate(myDateTime, myStartTime, myEndTime);
			 // if(nr.contains("0")){
				   suc = MyProvider.createTimeRecord(myDateTime, myStartTime, myEndTime, myProject, myLocation, myTask, myActivity, myCostplace, description, description, "", myCustomer);
				   
					 /* System.out.println("....................... Corect time");

				   duplicate = true;

			  }else{
				  	  System.out.println("....................... Duplicate time");
				  
				  duplicate = false;
				  
			  }*/
			  	
			  
		//	  SendEmail("mverbeek@sharethatdata.com", "New Time record. ", emailmessage );
			  
        	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		 
	            		 if(error_message.equals("Duplicate")){
	            			 error_message = getString(R.string.timerecord_duplicate);
	            		 }
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(RecordTimeActivity.this, getString(R.string.text_error), error_message);
		    		}else{
		    			Toast.makeText(getApplicationContext(), "Report created", Toast.LENGTH_SHORT).show();
		    			Intent intent = new Intent(getApplicationContext(), ListActivity.class); 
		    			intent.putExtra("year", String.valueOf(mStartYear));
		    			intent.putExtra("month", String.valueOf(mStartMonth));
		    			intent.putExtra("day", String.valueOf(mStartDay));
		    			startActivity(intent); 
		    		}
	            	
	            	
	            		/*boolean d = duplicate;
						if(d == true){
							  Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
							  
						  }else{
							  Toast.makeText(getApplicationContext(), "Duplicate time", Toast.LENGTH_SHORT).show();
						  }*/
	            	
	            	         	   	
	              }
	        });
	         
	    }
    }
    
    
    private class EditRecordTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public EditRecordTask(String id) {
		     myID = id;
		  }
		  
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	         showProgressDialog(getString(R.string.loading_alert_dialog));
	     }

		  protected Boolean doInBackground(String... urls) {
			  
			  String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
		        
        	  String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
        	  String myEndTime = (new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();
        	
        	  // extract data
		 
			  Spinner locationSpinner = (Spinner) findViewById(R.id.spinnerLocation);
			  int myIndex = locationSpinner.getSelectedItemPosition();
			  String myLocation = "";
			  if(myIndex != -1){
				  myLocation = locationkeyArray.get(myIndex);		  
			  }
			  			  			  
			  Spinner taskSpinner = (Spinner) findViewById(R.id.spinnerTask);
			  myIndex = taskSpinner.getSelectedItemPosition();
			  String myTask = "";
			  if(myIndex != -1){	  
				  myTask = taskkeyArray.get(myIndex);
			  }
			  			  			  			  
			  Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
			  myIndex = projectSpinner.getSelectedItemPosition();
			  String myProject = "";
			  if(myIndex != -1){	
				  myProject = projectkeyArray.get(myIndex);
			  }
			  
			  Spinner activitySpinner = (Spinner) findViewById(R.id.spinnerActivity);
			  myIndex = activitySpinner.getSelectedItemPosition();
			  String myActivity = "";
			  if(myIndex != -1){	
				  myActivity = activitykeyArray.get(myIndex);		  
			  }
			  
			  Spinner costplaceSpinner = (Spinner) findViewById(R.id.spinnerCostplace);
			  myIndex = costplaceSpinner.getSelectedItemPosition();
			  String myCostplace = "";
			  if(myIndex != -1){	
				  myCostplace = costplacekeyArray.get(myIndex);
			  }

			  Spinner customerSpinner = (Spinner) findViewById(R.id.spinnerCustomer);
			  myIndex = customerSpinner.getSelectedItemPosition();
			  String myCustomer = "";
			  if(myIndex != -1) {
				  myCustomer = customerkeyArray.get(myIndex);
			  }

			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);

		      String description = DescriptionView.getText().toString();
			  
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
  			  
			  boolean suc = false;

			  if(!myReportUser.equals(""))
			  	suc = MyProvider.updateTimeRecord(myID, myDateTime, myStartTime, myEndTime, myLocation, myActivity, myProject, myTask, myCostplace, description, myReportUser, myUser, myCustomer);
			  else
				  suc = MyProvider.updateTimeRecord(myID, myDateTime, myStartTime, myEndTime, myLocation, myActivity, myProject, myTask, myCostplace, description, myUserID, myUser, myCustomer);

			  return suc;
		  }

		  protected void onPostExecute(Boolean result) {
			  if (RecordTimeActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				  return;
			  }
			  hideProgressDialog();
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Time report updated", Toast.LENGTH_SHORT).show();
				  Globals.setValue("edit", "");
				  Globals.setValue("report_user","");
				  Intent intent = new Intent(RecordTimeActivity.this, ListActivity.class);
				  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  startActivity(intent);
			  }else{
				  error_message = MyProvider.last_error;

				  if(error_message.equals("Duplicate")){
					  error_message = getString(R.string.timerecord_duplicate);
				  }
          		  
	          	  UIHelper myUIHelper = new UIHelper();
		    			    
		    	  myUIHelper.ShowDialog(RecordTimeActivity.this, getString(R.string.text_error), error_message);
		    			 
				  Toast.makeText(getApplicationContext(), "Was not updated time report", Toast.LENGTH_SHORT).show();
				  Intent intent = new Intent(RecordTimeActivity.this, ListActivity.class);
				  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 // startActivity(intent);

				  error_message = MyProvider.last_error;
			  }
			  
		  }
	}
    
    
    private class LoadPreviousDataTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public LoadPreviousDataTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  			  
			  myRecord = MyProvider.getTimeRecord(myID, true);
			  last_location = myRecord.location;
			  
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  if (myRecord != null)
			  {

				  SetStartTime(myRecord.endtime);
	
				  // maybe for default in future...
			  }
		  }
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		//Handle the back button

		if(keyCode == KeyEvent.KEYCODE_BACK) {
			final String edit = Globals.getValue("edit");
			if(edit == ""){
				// don't quit when edit time report
				//Ask the user if they want to quit
				new AlertDialog.Builder(this)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setTitle("Quit DigiWorkTime")
						.setMessage("Are you sure?")
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

								//Stop the activity
								RecordTimeActivity.this.finish();
							}
						})
						.setNegativeButton("No", null)
						.show();
			}else{
				finish();
			}

			return true;
		}
		else {
			return super.onKeyDown(keyCode, event);
		}
	}
	
	/*// Check Time Record Hours
		ArrayList<HashMap<String, String>> itemList;
		// Reminder
		
		*//**
		 * Background Async Task to Load all time records by making HTTP Request
	  	 * *//*
		class LoadHour extends AsyncTask<String, String, String> {

	     *//**
	      * Before starting background thread Show Progress Dialog
	      * *//*
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();

	     }

		    *//**
		     * getting items from url
		     * *//*
		    protected String doInBackground(String... args) {
		   	    	
		    	MyProvider = new WSDataProvider(myUser);
				
				Globals = ((MyGlobals)getApplication());
				
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			    
		        String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
			    
				String report_date_end = myDateTimeEnd;
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    String strDate = report_date_end;
			    Date date1 = null;
				try {
					date1 = sdf.parse(strDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    Calendar calendar = Calendar.getInstance();
			    calendar.setTime(date1);
			    calendar.add(Calendar.DATE, -1);
			    Date yesterday = calendar.getTime();

			    String dateYesterday = sdf.format(yesterday);
			    
				
				if (report_user == "") 
				{
					report_user = myUser;
				}
				
		    	 // load data from provider
		    	 itemList = new ArrayList<HashMap<String, String>>();  
		    	
		    	 List<cTimeRecord> timeRecords = MyProvider.getTimeRecords(dateYesterday, report_date_end, report_user, false);
		    	 for(cTimeRecord entry : timeRecords)
		    	 {
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                //map.put("date", entry.starttime.substring(0, 16));
	                
	                String[] tokens = entry.starttime.split("\\s");
	                String values = tokens[0]+tokens[1];
	                String date = tokens[0];
	                String time = tokens[1];
	                
	                System.out.println("Values: " + values);
	                
	                map.put("date", date);
	                map.put("time" , time );

	                map.put("hours", Integer.toString(entry.minutes));
	                 
	                // adding HashList to ArrayList
	                itemList.add(map);
		    	 }
		    	 
		    	 System.out.println("Date: " + dateYesterday + " " + report_date_end);
		    	
		    	 return "";
		    }
		
		    *//**
		     * After completing background task Dismiss the progress dialog
		     * **//*
		    protected void onPostExecute(String file_url) {
		        // dismiss the dialog after getting all products
		
		    	// updating UI from Background Thread
		        runOnUiThread(new Runnable() {
		            public void run() {
		            
		            	// total
		            	int total = 0;
		            	int myNum = 0;
		            	for (HashMap<String, String> map : itemList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "hours")
		           	        	{
		           	        		
		           	        		try {
		           	        			myNum = Integer.parseInt(entry.getValue().toString());
		           	        		
		           	        			entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum));
		           	        			
		           	        		} catch(NumberFormatException nfe) {
		           	        			myNum = 0;
		           	        		}
		           	        		total = total + myNum;
		           	        	}
		           	        }
		            	
		                HashMap<String, String> hmap = new HashMap<String, String>();
		          		
		            	hmap.put("hours", MyTimeUtils.getFormattedHourMinutes(total) );
		            	hmap.put("date", "");
		            	hmap.put("time", "");
		                hmap.put("name", "total");
		           	                
		            	itemList.add(hmap);
		            	
		            	float totalFormatted = (float)total / 60;
		            	
		            	// time in morning
		            	SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
	            		String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
	            		String endDay = sharedPrefs.getString("prefNotificationEndDay", "");
	            		boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", false);
	            		
	            		System.out.println("start and end time from settings: " + startDay + " " + endDay);
	            		
	            		int currentHour, currentMin = 0;
	            		
	            		Calendar currentTime = Calendar.getInstance();
	            		currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
	            		currentMin = currentTime.get(Calendar.MINUTE);
	            		currentTime.get(Calendar.SECOND);
	            		currentTime.get(Calendar.AM_PM);
	            		
	            		StringBuilder stringCurrentTime = new StringBuilder().append(pad(currentHour)).append(":").append(pad(currentMin));
	            		
	            		System.out.println("Curent time: " + stringCurrentTime);
	            		
	            		System.out.println("Curent time in milliseconds: " + System.currentTimeMillis() );
	            		
	            		convertSecondsToHMmSs(System.currentTimeMillis());
	            		
	            		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm +a");

	                    Date resultdate = new Date(System.currentTimeMillis());
	                    System.out.println(sdf.format(resultdate)); 
	                    
	                    System.out.println("------------------------------------------"); 
	                    
	                    
		            	
	            		// First Notification in morning
		            	//if(stringCurrentTime.equals(startDay)){
		            		
	                 // if notification check box checked = true => set reminder
						if (blNotifications){
		            		   Log.d("notify", "Notification on");

		            		// if 8 hours was not recorded yesterday
			            	if(totalFormatted < 8)
			            	{
			            		Toast.makeText(getApplicationContext(), "Hours registered yesterday: " + String.valueOf(totalFormatted), Toast.LENGTH_SHORT).show();
			            	
			            		// incorrect, setting the time of alarm at current, so will fire right after.
			            		
			            		String hourS = null;
			                    String minS = null;
			            		
			            		if(!startDay.isEmpty()){
			            			String[] tokens = startDay.split(":");
				                    hourS = tokens[0];
				                    minS = tokens[1];
			            		}else{
			            			hourS = "09";
			            			minS = "00";
			            		}

				            	  Calendar calendar = Calendar.getInstance();
				            	  
				            	  calendar.setTimeInMillis(System.currentTimeMillis());
				          	 
				          	      calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourS));
				          	      calendar.set(Calendar.MINUTE, Integer.parseInt(minS));
				          	      //calendar.set(Calendar.HOUR_OF_DAY, 11);
				          	      //calendar.set(Calendar.MINUTE, 58);
				          	      calendar.set(Calendar.SECOND, 00);
				          	      //calendar.set(Calendar.AM_PM, Calendar.PM);
				          	      
				          	    System.out.println("Time morning set: " + hourS + ":" + minS);
			            		
			          	      // get start and end time from settings
			          		int startHour = 9;
							int startMinute = 0;
							
							// taken from login activity. marko try this before.
							
							String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
							String endDay = sharedPrefs.getString("prefNotificationEndDay", "");
							
							if (startDay != "")
							{
								try
								{
									String[] start_time = startDay.split(":");
									startHour = Integer.parseInt(start_time[0]);
									startMinute = Integer.parseInt(start_time[1]);
						
									// is current time before first time
									if (calendar.HOUR_OF_DAY >= startHour && calendar.MINUTE >= startMinute)
									{
										String[] end_time = endDay.split(":");
										startHour = Integer.parseInt(end_time[0]);
										startMinute = Integer.parseInt(end_time[1]);
									
									}
										
								} catch (NumberFormatException nfe) {
									
								}
								calendar.set(Calendar.HOUR_OF_DAY, startHour);
								calendar.set(Calendar.MINUTE, startMinute);
			          	      
							
							 
							
							
			            		   
			            		 //Date now = new Date();
								     //long uniqueId = now.getTime();//use date to generate an unique id to differentiate the notifications.
									AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE); 
									Intent myIntent = new Intent(RecordTimeActivity.this, MyReceiverYesterday.class);
									//myIntent.setType("intent1");
									//myIntent.setAction("intent1" + uniqueId);
									//Notification notification = null;
									//myIntent.putExtra(MyAlarmServiceYesterday.NOTIFICATION_ID, 0);
									//myIntent.putExtra(MyAlarmServiceYesterday.NOTIFICATION, notification);
					            	pendingIntent = PendingIntent.getBroadcast(RecordTimeActivity.this, 1, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					            	

				            	  boolean isAlarmActive = (pendingIntent != null);
				            	   if (isAlarmActive ) {
				            	     Log.d("TAG", "Alarm is already active for today");
				            	   }else{
				            		   Log.d("TAG", "Alarm is not active for today");

					          	       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent); 
				            	   //}
			            	   
			            	
			            	}
			            	   
			          	       alarmManager.cancel(PendingIntent.getBroadcast(getBaseContext(), 1, myIntent, PendingIntent.FLAG_UPDATE_CURRENT));
			          	     
			          	       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
					
			            	// if notification check box checked = true => cancel reminder
			            	}else{
			            		Log.d("notify", "Notification off");
			            		AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			                    manager.cancel(pendingIntent);
			            	   }
	            		

		          //  	}
		            	
		            	
		            	// Second Notification in afternoon
		            	//if(stringCurrentTime.equals(endDay)){
						
						
						// if notification check box checked = true => set reminder
						if (blNotifications){
		            		   Log.d("notify", "Notification on");
						
		            		// if 8 hours was not recorded today (16 because 8 from yesterday and 8 for today)
			            	if(totalFormatted < 16)
			            	{
			            		Toast.makeText(getApplicationContext(), "Hours registered today: " + String.valueOf(totalFormatted), Toast.LENGTH_SHORT).show();
				            	
			            		// incorrect, setting the time of alarm at current, so will fire right after.
			            		
			            		String hourE = null;
			                    String minE = null;
			            		
			            		if(!endDay.isEmpty()){
			            			String[] tokens = endDay.split(":");
				                    hourE = tokens[0];
				                    minE = tokens[1];
			            		}else{
			            			hourE = "18";
			            			minE = "00";
			            		}

				            	  Calendar calendar = Calendar.getInstance();
				            	  calendar.clear();
					           	     
				          	      calendar.get(Calendar.MONTH);
				          	      calendar.get(Calendar.YEAR);
				          	      calendar.get(Calendar.DAY_OF_MONTH);
				          	      
				          	      calendar.setTimeInMillis(System.currentTimeMillis());
				          	 
				          	      calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourE));
				          	      calendar.set(Calendar.MINUTE, Integer.parseInt(minE));

				          	      //calendar.set(Calendar.HOUR_OF_DAY, 11);
				          	      //calendar.set(Calendar.MINUTE, 59);
				          	      calendar.set(Calendar.SECOND, 00);
				          	      //calendar.set(Calendar.AM_PM, Calendar.PM);
				          	      
				          	    System.out.println("Time afternoon set: " + hourE + ":" + minE);
				          	      
				          	    long when = calendar.getTimeInMillis(); 
				          	    
				          	      	System.out.println("Calendar second: " + when);
				          	      	
				          	      convertSecondsToHMmSs(when);   	

				            		   
				            		// Date now = new Date();
									    //long uniqueId = now.getTime();//use date to generate an unique id to differentiate the notifications.
					            	   Intent myIntent = new Intent(RecordTimeActivity.this, MyReceiverToday.class);
					            	  // myIntent.setType("intent2");
					            	  // myIntent.setAction("intent1" + uniqueId);
					          	       pendingIntent = PendingIntent.getBroadcast(RecordTimeActivity.this, 2, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					          	      // Notification notification = null;
										//myIntent.putExtra(MyAlarmServiceToday.NOTIFICATION_ID, 1);
										//myIntent.putExtra(MyAlarmServiceToday.NOTIFICATION, notification);
					          	       AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
				            		   
				            		   boolean isAlarmActive = (pendingIntent != null);
					            	   if (isAlarmActive) {
					            	     Log.d("TAG", "Alarm is already active for yesterday");
					            	   }else{
					            		   Log.d("TAG", "Alarm is not active for yesterday");
	
						          	       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, when, AlarmManager.INTERVAL_DAY, pendingIntent);

					            	   //}
								
								// if notification check box checked = true => cancel reminder
								
			            		}
			          	      	
		            
			          	       //alarmManager.cancel(pendingIntent);
			        	       
			            	}else{
		            			Log.d("notify", "Notification off");
		            			NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
			            		notifManager.cancel(2);
			            		notifManager.cancelAll();
			            		AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			                    manager.cancel(pendingIntent);
			            		
		            	   }
	            		}
	                    
	                  //}
	
		            	
		            
		        });
		  	      
		    }
		  

		}
	
		public static String convertSecondsToHMmSs(long millis) {
		    
			String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS +a";
			// Create a DateFormatter object for displaying date in specified format.
		    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		    // Create a calendar object that will convert the date and time value in milliseconds to date. 
		     Calendar calendar = Calendar.getInstance();
		     calendar.setTimeInMillis(millis);
		    
		    //String formatTime = String.format("%d:%02d:%02d", h,m,s);
		    System.out.println("............. " + formatter.format(calendar.getTime()));
		    
		    return formatter.format(calendar.getTime());
		}*/
		
		
		/*public void setReminder(int hour,int min, String title, String description){

			int notificationCount = 0;
			
		            notificationCount  = notificationCount+1;
		               
		               Calendar calendar = Calendar.getInstance();
		            	  calendar.clear();
			           	     
		          	      calendar.get(Calendar.MONTH);
		          	      calendar.get(Calendar.YEAR);
		          	      calendar.get(Calendar.DAY_OF_MONTH);
		          	      
		          	      calendar.setTimeInMillis(System.currentTimeMillis());
		          	 
		          	      //calendar.set(Calendar.HOUR, Integer.parseInt(hourE));
		          	      //calendar.set(Calendar.MINUTE, Integer.parseInt(minE));
		          	      
		          	      calendar.set(Calendar.HOUR_OF_DAY, 2);
		          	      calendar.set(Calendar.MINUTE, 26);
		          	      //calendar.set(Calendar.AM_PM, Calendar.PM);
		          	      
		          	      long when = calendar.getTimeInMillis(); 
		          	    
		          	      System.out.println("Calendar second: " + when);
		          	      	
		          	      convertSecondsToHMmSs(when);   
 
		        AlarmManager mgr = (AlarmManager)  getSystemService(Context.ALARM_SERVICE);
		        Intent notificationIntent = new Intent(RecordTimeActivity.this, MyReceiverToday.class);
		        notificationIntent.putExtra("Name", title);
		        notificationIntent.putExtra("Description", description);
		        notificationIntent.putExtra("NotifyCount",notificationCount );

		        PendingIntent pi = PendingIntent.getBroadcast(RecordTimeActivity.this, notificationCount, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		        mgr.setRepeating(AlarmManager.RTC_WAKEUP, when, 5000, pi);


		   }*/

		
}
