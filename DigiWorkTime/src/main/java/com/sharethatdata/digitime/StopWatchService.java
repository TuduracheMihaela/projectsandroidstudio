package com.sharethatdata.digitime;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

/**
 * Created by mihu__000 on 8/20/2015.
 */
public class StopWatchService extends Service {

    private static final String TAG = "StopwatchService";
    private static final int NOTIFICATION_ID = 1;
    String CHANNEL_ID_01 = "my_channel_01";// The id of the channel.

    public class LocalBinder extends Binder {
        StopWatchService getService() {
            return StopWatchService.this;
        }
    }

    private StopWatch m_stopwatch;
    private LocalBinder m_binder = new LocalBinder();
    //private NotificationManager m_notificationMgr;
    NotificationManagerCompat notificationManagerCompat;
    private Notification m_notification;

    private final static AtomicInteger c = new AtomicInteger(0);

    // Timer to update the ongoing notification
    private final long mFrequency = 100;    // milliseconds
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            updateNotification();
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "bound");

        return m_binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "created");

        m_stopwatch = new StopWatch();
        notificationManagerCompat = NotificationManagerCompat.from(StopWatchService.this);
        //m_notificationMgr = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        createNotification();
    }
    
    @Override
    public void onDestroy() {
    	Log.d(TAG, "destroyed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    public static int getID() {
        return c.incrementAndGet();
    }

    public void createNotification() {
        Log.d(TAG, "creating notification");

        int icon = R.drawable.ic_launcher;
        CharSequence textAppName = getText(R.string.app_name);
        long when = System.currentTimeMillis();

        m_notification = new Notification(icon, textAppName, when);
        m_notification.flags |= Notification.FLAG_ONGOING_EVENT;
        m_notification.flags |= Notification.FLAG_NO_CLEAR;

        Intent launchIntentDigiTime = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digitime");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, getID(), launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID_01);
        builder.setContentTitle(textAppName)
                .setContentText("Counting:")
                .setSmallIcon(icon)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(when)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        Notification notification = builder.build();
        m_notification = notification;
    }

    public void updateNotification() {
        // Log.d(TAG, "updating notification");

        int icon = R.drawable.ic_launcher;
        Context context = getApplicationContext();
        CharSequence textAppName = getText(R.string.app_name);
        CharSequence contentText = getText(R.string.notification_subtitle) + " " + getFormattedElapsedTime();

        Intent notificationIntent = new Intent(this, StopWatchActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        Intent launchIntentDigiTime = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digitime");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, getID(), launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID_01);
        builder.setContentTitle(textAppName)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        Notification notification = builder.build();
        m_notification = notification;


        // the next two lines initialize the Notification, using the configurations above
        //m_notification.setLatestEventInfo(context, textAppName, contentText, contentIntent);
        try{
            notificationManagerCompat.notify(NOTIFICATION_ID, m_notification);
            //m_notificationMgr.notify(NOTIFICATION_ID, m_notification);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showNotification() {
        Log.d(TAG, "showing notification");

        updateNotification();
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
    }

    public void hideNotification() {
        Log.d(TAG, "removing notification");

        //m_notificationMgr.cancel(NOTIFICATION_ID);
        notificationManagerCompat.cancel(NOTIFICATION_ID);
        mHandler.removeMessages(TICK_WHAT);
    }

    public void start() {
        Log.d(TAG, "start");
        m_stopwatch.start();

        showNotification();
    }

    public void pause() {
        Log.d(TAG, "pause");
        m_stopwatch.pause();

        hideNotification();
    }

    public void stop() {
        Log.d(TAG, "stop");
        m_stopwatch.stop();

        hideNotification();
    }

    public void reset() {
        Log.d(TAG, "reset");
        m_stopwatch.reset();

        hideNotification();
    }

    public long getElapsedTime() {
        return m_stopwatch.getElapsedTime();
    }

    public String getFormattedElapsedTime() {
        return formatElapsedTime(getElapsedTime());
    }

    public boolean isStopWatchRunning() {
        return m_stopwatch.isRunning();
    }

    /***
     * Given the time elapsed in tenths of seconds, returns the string
     * representation of that time.
     *
     * @param now, the current time in tenths of seconds
     * @return 	String with the current time in the format MM:SS.T or
     * 			HH:MM:SS.T, depending on elapsed time.
     */
    private String formatElapsedTime(long now) {
        long hours=0, minutes=0, seconds=0, tenths=0;
        StringBuilder sb = new StringBuilder();

            hours = now / 3600000;
            now -= hours * 3600000;
            minutes = now / 60000;
            now -= minutes * 60000;
            seconds = now / 1000;
            now -= seconds * 1000;
            tenths = (now / 100);

            sb.append(hours).append(":")
                    .append(formatDigits(minutes)).append(":")
                    .append(formatDigits(seconds));//.append(".").append(tenths);


        return sb.toString();
    }

    private String formatDigits(long num) {
        return (num < 10) ? "0" + num : new Long(num).toString();
    }


}
