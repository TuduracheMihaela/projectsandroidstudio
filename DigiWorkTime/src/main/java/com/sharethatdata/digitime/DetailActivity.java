package com.sharethatdata.digitime;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.protocol.HTTP;

import com.sharethatdata.webservice.datamodel.*;
import com.sharethatdata.webservice.*;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class DetailActivity extends AppCompatActivity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
    private ProgressDialog pDialog;
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndHour,mEndMinute;
	
	cTimeRecord myRecord;
	 
	ListView lv = null;
	
	String myUser = "";
	String myPass = "";
	
	String pid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_timerecord);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		pid = Globals.getValue("pid");
		String action = Globals.getValue("action");
		
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(DetailActivity.this, DetailActivity.this);
		
		TextView titleText = (TextView) findViewById(R.id.titleText);
		
		titleText.setText("time report");
		
		// load data
		new LoadDataTask(pid).execute();
						
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	   
	    mEndHour = c.get(Calendar.HOUR_OF_DAY);
	    mEndMinute = c.get(Calendar.MINUTE);
	    
		String isDev = Globals.getValue("developer");
/*		if (isDev == "yes")
		{	*/
			Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.VISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.VISIBLE);	
/*		} else {
			Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.INVISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);	
			Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.VISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.VISIBLE);
		}*/
	    
	    /*Button btnDelete = (Button) findViewById(R.id.btnDelete);
	    Button btnEdit = (Button) findViewById(R.id.btnEdit);*/
		  
	    
	    // add button listener
	    btnDelete.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
			
				  new AlertDialog.Builder(DetailActivity.this)
				  .setTitle("Delete time record")
				  .setMessage("Do you really want to delete this record?")
				  .setIcon(android.R.drawable.ic_dialog_alert)
				  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

				      public void onClick(DialogInterface dialog, int whichButton) {
				    	  				      
				          new DeleteRecordTask(myRecord.id).execute();
				          
				      }})
				   .setNegativeButton(android.R.string.no, null).show();
					  
			  }
		 
		});
	    
	    
	 // add button listener
	    btnEdit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
				    	  				      
				         // new EditRecordTask(myRecord.id).execute();

				    	    Globals.setValue("edit", "edit");
				    	    Globals.setValue("id", String.valueOf(pid));
							Globals.setValue("date", String.valueOf(myRecord.date));
							Globals.setValue("starttime", String.valueOf(myRecord.starttime));
							Globals.setValue("endtime", String.valueOf(myRecord.endtime));
							Globals.setValue("locationid", String.valueOf(myRecord.locationid));
							Globals.setValue("location", String.valueOf(myRecord.location));
							Globals.setValue("projectid", String.valueOf(myRecord.projectid));
							Globals.setValue("project", String.valueOf(myRecord.project));
							Globals.setValue("taskid", String.valueOf(myRecord.taskid));
							Globals.setValue("task", String.valueOf(myRecord.task));
							Globals.setValue("activityid", String.valueOf(myRecord.activityid));
							Globals.setValue("activity", String.valueOf(myRecord.activity));
							Globals.setValue("costplaceid", String.valueOf(myRecord.costplaceid));
							Globals.setValue("costplace", String.valueOf(myRecord.costplace));
							Globals.setValue("customerid", String.valueOf(myRecord.customerid));
							Globals.setValue("customer", String.valueOf(myRecord.customer));
							Globals.setValue("description", String.valueOf(myRecord.description));
							
							Intent intent = new Intent(DetailActivity.this, RecordTimeActivity.class);
							startActivity(intent);
			  }
		 
		});
	    
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String edit = Globals.getValue("edit");
	    if(edit != ""){
	    	Globals.setValue("edit", "");
	    	new LoadDataTask(pid).execute();
	    }
	}
		
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	    		        
	            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	        }
	    };
	    
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
	 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	    		        
	            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	                    .append(pad(mStartMonth + 1)).append("-")
	                    .append(pad(mStartYear)));
	        }
	    };

	private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mEndHour = hourOfDay;
	            mEndMinute = minute;
	            
	            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	    		        
	            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
	                    .append(pad(mEndMinute)));
	        }
	    };
	     
	public void startTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
	    tp1.show();
	}
	
	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    dp1.show();
	}
	
	public void endTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
	    tp1.show();
	}
	
	// set time from mysql datetime
	private void SetTime(View v, String mysqldate)
	{
		TextView TimeView = (TextView) v;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
		try {
			Date myDate = dateFormat.parse(mysqldate);
		    
		    Calendar c = Calendar.getInstance();
		    c.setTime(myDate);
		    mStartHour = c.get(Calendar.HOUR_OF_DAY);
		    mStartMinute = c.get(Calendar.MINUTE);
					
		    TimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
				
		} catch (ParseException e) {
		    	
		}

    }
  
	// set time from mysql datetime
	private void SetDate(View v, String mysqldate)
	{
		TextView DateView = (TextView) v;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
		try {
			Date myDate = dateFormat.parse(mysqldate);
		    
		    Calendar c = Calendar.getInstance();
		    c.setTime(myDate);
		    mStartYear = c.get(Calendar.YEAR);
			mStartMonth = c.get(Calendar.MONTH);
			mStartDay = c.get(Calendar.DAY_OF_MONTH);
					
			DateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
                    .append(pad(mStartMonth + 1)).append("-")
                    .append(pad(mStartYear)));
		   				
		} catch (ParseException e) {
		    	
		}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}


	private class LoadDataTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public LoadDataTask(String id) {
		     myID = id;
		  }
		  
		  @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		 
		         pDialog = new ProgressDialog(DetailActivity.this);
		         pDialog.setMessage( getString(R.string.loading_alert_dialog));
		      
		         pDialog.setIndeterminate(false);
		         pDialog.setCancelable(false);
		         pDialog.setCanceledOnTouchOutside(false);
		         pDialog.show();
		         
		     }

		  protected String doInBackground(String... urls) {
			  	
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  myRecord = MyProvider.getTimeRecord(myID, false);
			  
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  if (myRecord != null)
			  {
				
				  TextView dateView = (TextView) findViewById(R.id.dateStartText);
				  SetDate(dateView,myRecord.date);
				  TextView starttimeView = (TextView) findViewById(R.id.startTimeText);
				  SetTime(starttimeView,myRecord.starttime);
				  TextView endtimeView = (TextView) findViewById(R.id.endTimeText);
				  SetTime(endtimeView,myRecord.endtime);
				
				  int minutes = myRecord.minutes;
				  // calc hours format
				  float fhour = (minutes / 60);
	              float fminute = (((float) minutes / 60) - fhour) * 60;
	              String hours = "";
	              if (fminute > 0)
	              {
		            	hours = String.format("%.0f", fhour) + "h" + String.format("%.0f", fminute) + "m";
	              } else {
		            	hours = String.format("%.0f", fhour) + "h";
	              }
	              TextView hoursView = (TextView) findViewById(R.id.hoursText);
	              hoursView.setText(hours);
	              
				  TextView locationView = (TextView) findViewById(R.id.locationText);
				  locationView.setText(myRecord.location);
				  TextView customerView = (TextView) findViewById(R.id.customerText);
				  customerView.setText(myRecord.customer);
				  TextView projectView = (TextView) findViewById(R.id.projectText);
				  projectView.setText(myRecord.project);
				  TextView taskView = (TextView) findViewById(R.id.taskText);
				  taskView.setText(myRecord.task);
				  TextView activityView = (TextView) findViewById(R.id.activityText);
				  activityView.setText(myRecord.activity);
				  TextView costplaceView = (TextView) findViewById(R.id.costplaceText);
				  costplaceView.setText(myRecord.costplace);
				 
				
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(myRecord.description);
				
			  }
			  
			  pDialog.dismiss();
		  }
	}
	
	private class DeleteRecordTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public DeleteRecordTask(String id) {
		     myID = id;
		  }

		  protected Boolean doInBackground(String... urls) {
			  			  
			  boolean suc = false;
			  
			  	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				suc = MyProvider.deleteTimeRecord(myID);
			  
			  return suc;
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Time report deleted", Toast.LENGTH_SHORT).show();
				  finish();
				  
			  }else{
				  Toast.makeText(getApplicationContext(), "Was not deleted time report", Toast.LENGTH_SHORT).show();
				  finish();
			  }
			  
			  
		  }
	}
		
}


