package com.sharethatdata.digitime.management;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by miha on 10/12/2017.
 */

public class ManageCostplacesActivity extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    ArrayList<HashMap<String, String>> costplaceList;
    ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();

    Spinner userSpinner = null;
    ListView lv = null;

    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";

    private String report_user = "";
    private String report_user_id = "";

    private boolean userIsInteracting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_costplaces);

        Globals = ((MyGlobals)getApplication());
        Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // id of user logged

        lv = (ListView) findViewById(R.id.listView);
        userSpinner = (Spinner) findViewById(R.id.spinnerUser);

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (userIsInteracting) {

                    Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
                    int myIndex = userSpinner.getSelectedItemPosition();
                    report_user = userkeyArray.get(myIndex);
                    report_user_id = userkeyArray.get(myIndex);

                    new LoadList().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        new LoadUserList().execute();

        new LoadList().execute();

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.manage, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_manage:
                Intent intent = new Intent(this, AddOptionToUserActivity.class);
                intent.putExtra("option","Costplace");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Background Async Task to Load all users by making HTTP Request
     * */
    class LoadUserList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            Globals = ((MyGlobals)getApplication());

            if (report_user == "")
            {
                userList = new ArrayList<HashMap<String, String>>();

                // convert objects to hashmap for list
                List<cContact> List = MyProvider.getMyUsers();

                for(cContact entry : List)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("pid", entry.id);
                    map.put("name", entry.name);

                    // adding HashList to ArrayList
                    userList.add(map);
                }

                report_user = myUser;
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> userArray =  new ArrayList<String>();
                    for (HashMap<String, String> map : userList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") userArray.add(entry.getValue());
                        }

                    ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(ManageCostplacesActivity.this, android.R.layout.simple_spinner_item, userArray);
                    user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                    sUsers.setAdapter(user_adapter);

                    int spinnerPosition = user_adapter.getPosition(myUserName);
                    //set the default according to value
                    sUsers.setSelection(spinnerPosition);

                    userIsInteracting = false;
                }
            });

            hideProgressDialog();

        }
    }


    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {
            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            if (report_user_id == "")
            {
                report_user_id = myUserID;
            }

            // load data from provider
            costplaceList = new ArrayList<HashMap<String, String>>();

            List<cCostplace> costList = MyProvider.getCostPlaces(report_user_id);
            for(cCostplace entry : costList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);

                // adding HashList to ArrayList
                costplaceList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = null;

                    adapter = new SimpleAdapter(
                            ManageCostplacesActivity.this
                            , costplaceList
                            , R.layout.activity_manage_sublistview_item, new String[] { "id", "name" },
                            new int[] { R.id.textViewId, R.id.textViewName });

                    // updating listview
                    lv.setAdapter(adapter);
                }
            });

            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }
}
