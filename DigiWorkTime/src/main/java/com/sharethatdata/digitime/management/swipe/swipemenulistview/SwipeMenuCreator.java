package com.sharethatdata.digitime.management.swipe.swipemenulistview;


public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
