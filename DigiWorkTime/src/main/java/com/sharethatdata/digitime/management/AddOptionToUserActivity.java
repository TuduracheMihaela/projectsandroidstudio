package com.sharethatdata.digitime.management;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.AddOptionAdapter;
import com.sharethatdata.digitime.management.swipe.SwipeDismissList;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.BaseSwipeListAdapter;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.SwipeMenu;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.SwipeMenuCreator;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.SwipeMenuItem;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.SwipeMenuListView;
import com.sharethatdata.digitime.management.swipe.swipemenulistview.SwipeMenuUndo;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cModuleItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by miha on 10/12/2017.
 */

public class AddOptionToUserActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    //SwipeDismissList swipeList;

    // Progress Dialog
    private ProgressDialog pDialog;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    //AddOptionAdapter adapter;

    List<String> item_user_list;

    ArrayList<HashMap<String, String>> itemList;
    ArrayList<HashMap<String, String>> itemUserList;
    ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();
    ArrayList<HashMap<String, String>> moduleList;
    List<String> modulekeyArray =  new ArrayList<String>();
    ArrayList<HashMap<String, String>> optionList;

    /*List<String> activityIdArray =  new ArrayList<String>();
    List<String> activityNameArray =  new ArrayList<String>();
    List<String> activityOptionArray =  new ArrayList<String>();*/

    Spinner userSpinner = null;
    Spinner moduleSpinner = null;
    //ListView lv = null;
    private SwipeMenuListView lv;
    SearchView searchView;

    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";

    private String report_user = "";
    private String report_user_id = "";
    private String report_module = "";
    private String report_module_id = "";
    private String report_option = "";
    private String report_option_id = "";

    private String option;

    private boolean userIsInteracting = false;
    private boolean moduleIsInteracting = false;
    private boolean optionIsInteracting = false;

    String deletedItemId;
    String deletedItemName;
    String deletedItemOption;
    int positionItem;

    private AppAdapter mAdapter;

    int index, top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_add_option);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            option = null;
        } else {
            option = extras.getString("option");
        }

        Globals = ((MyGlobals)getApplication());
        Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // id of user logged

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(AddOptionToUserActivity.this, AddOptionToUserActivity.this);

        userSpinner = (Spinner) findViewById(R.id.spinnerUser);
        moduleSpinner = (Spinner) findViewById(R.id.spinnerModule);
        //lv = (ListView) findViewById(R.id.listView);
        lv = (SwipeMenuListView) findViewById(R.id.listView);

        if(option.equals("Activity")){
            setTitle("Manage activities");
        }else if(option.equals("Costplace")){
            setTitle("Manage costplaces");
        }

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (userIsInteracting) {

                    Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
                    int myIndex = userSpinner.getSelectedItemPosition();
                    report_user = userkeyArray.get(myIndex);
                    report_user_id = userkeyArray.get(myIndex);

                    new LoadList(option).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

/*

        SwipeDismissList.OnDismissCallback callback = new SwipeDismissList.OnDismissCallback() {
            // Gets called whenever the user deletes an item.
            public SwipeDismissList.Undoable onDismiss(AbsListView listView, final int position) {
                // Get your item from the adapter (mAdapter being an adapter for MyItem objects)
                final String deletedItemId = String.valueOf(adapter.getItemId(position));
                final String deletedItemName = String.valueOf(adapter.getItemName(position));
                final String deletedItemOption = String.valueOf(adapter.getItemOption(position));

                final HashMap<String, String> map = new HashMap<String, String>();
                map.put("id",  deletedItemId);
                map.put("name", deletedItemName);
                map.put("option", deletedItemOption);

                // Delete item from adapter
                itemList.remove(position);
                adapter.notifyDataSetChanged();
                // Return an Undoable implementing every method
                return new SwipeDismissList.Undoable() {

                    // Method is called when user undoes this deletion
                    public void undo() {
                        // Reinsert item to list
                        itemList.add(position, map);
                        adapter.notifyDataSetChanged();
                    }

                    // Return an undo message for that item
                    public String getTitle() {
                        return deletedItemId.toString() + " deleted";
                    }

                    // Called when user cannot undo the action anymore
                    public void discard() {
                        // Use this place to e.g. delete the item from database
                        //new DeleteOption(deletedItemId).execute();
                        Toast.makeText(AddOptionToUserActivity.this, "DISCARD, item " + deletedItemId + " now finally discarded", Toast.LENGTH_SHORT).show();
                    }
                };
            }
        };

        SwipeDismissList.UndoMode mode = SwipeDismissList.UndoMode.SINGLE_UNDO;

        swipeList = new SwipeDismissList(lv, callback, mode, this);
        swipeList.setSwipeDirection(SwipeDismissList.SwipeDirection.START);
        swipeList.setAutoHideDelay(10000); //milliseconds until hide popup
        swipeList.setRequireTouchBeforeDismiss(true); //a touch is required(true - default) or no(false) to hide the popup

*/


//////////////////////////////////////////////////////////////////////////////////////////////////


        //Swipe with menu
        // 1.create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        lv.setMenuCreator(creator);

        SwipeMenuListView.OnDismissCallback callback = new SwipeMenuListView.OnDismissCallback() {
            // Gets called whenever the user deletes an item.
            public SwipeMenuListView.Undoable onDismiss(AbsListView listView, final int position) {

                final HashMap<String, String> map = new HashMap<String, String>();
                map.put("id",  deletedItemId);
                map.put("name", deletedItemName);
                map.put("option", deletedItemOption);

                // Return an Undoable implementing every method
                return new SwipeMenuListView.Undoable() {

                    // Method is called when user undoes this deletion
                    public void undo() {
                        // Reinsert item to list
                        itemList.add(positionItem, map);
                        mAdapter.notifyDataSetChanged();
                    }

                    // Return an undo message for that item
                    public String getTitle() {
                        return deletedItemId.toString() + " deleted";
                    }

                    // Called when user cannot undo the action anymore
                    public void discard() {
                        // Use this place to e.g. delete the item from database
                        new DeleteOption(deletedItemId).execute();
                        //Toast.makeText(AddOptionToUserActivity.this, "DISCARD, item " + deletedItemId + " now finally discarded", Toast.LENGTH_SHORT).show();
                    }
                };
            }
        };

        SwipeMenuListView.UndoMode mode = SwipeMenuListView.UndoMode.SINGLE_UNDO;
        lv.SwipeUndo(lv, callback, mode);

        // 2. listener item click event
        lv.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            //public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
            public boolean onMenuItemClick(View view, final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        positionItem = position;

                        deletedItemId = String.valueOf(mAdapter.getItemId(position));
                        deletedItemName = String.valueOf(mAdapter.getItem(position));
                        deletedItemOption = String.valueOf(mAdapter.getItemOption(position));

                        final HashMap<String, String> map = new HashMap<String, String>();
                        map.put("id",  deletedItemId);
                        map.put("name", deletedItemName);
                        map.put("option", deletedItemOption);

					    // Delete item from mAdapter
                        //Toast.makeText(AddOptionToUserActivity.this, "DISCARD, item " + position , Toast.LENGTH_SHORT).show();
                        //itemList.remove(position);
                        itemList.remove(position);
                        mAdapter.notifyDataSetChanged();

                        // Gets called when the user deletes an item.
                        lv.performDismiss(view, position);


//                        lv.mUndoButton.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Toast.makeText(AddOptionToUserActivity.this, "Undo, item " + deletedItemId
//                                        + " ... position " + position, Toast.LENGTH_SHORT).show();
//
//                                itemList.add(position, map);
//                                mAdapter.notifyDataSetChanged();
//
//                                lv.mUndoPopup.dismiss();
//                                lv.mDelayedMsgId++;
//                            }
//                        });



                        break;
                }
                return false;
            }
        });



        // Right directions
        lv.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        // set SwipeListener
        lv.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        lv.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });




        new LoadUserList().execute();

    } // onCreate()

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void onStop() {
        super.onStop();
        //swipeList.discardUndo();
        lv.discardUndo();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
        moduleIsInteracting = true;
        optionIsInteracting = true;
    }

    /**
     * Background Async Task to Load all users by making HTTP Request
     * */
    class LoadUserList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            Globals = ((MyGlobals)getApplication());

                userList = new ArrayList<HashMap<String, String>>();
                moduleList = new ArrayList<HashMap<String, String>>();
                optionList = new ArrayList<HashMap<String, String>>();

            List<cContact> ListUser;

                // convert objects to hashmap for list
            String isManager = Globals.getValue("manager");
            String isAdministrator = Globals.getValue("administrator");
            if (isAdministrator == "yes")
            {
                ListUser = MyProvider.getAllUsers();
            }else{
                ListUser = MyProvider.getMyUsers();
            }
                //List<cContact> ListUser = MyProvider.getMyUsers();
                for(cContact entry : ListUser)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("pid", entry.id);
                    map.put("name", entry.name);

                    // adding HashList to ArrayList
                    userList.add(map);

            }

                // convert objects to hashmap for list
                List<cModuleItems> ListModule = MyProvider.getModuleItems("Timesheet");
                for(cModuleItems entry : ListModule)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("pid", entry.id);
                    map.put("name", entry.name);

                    // adding HashList to ArrayList
                    moduleList.add(map);

                }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> userArray =  new ArrayList<String>();
                    List<String> moduleArray =  new ArrayList<String>();

                    userArray.add(0, getString(R.string.select_user));
                    userkeyArray.add(0,"");

                    if(userList.size()>0){
                        //userArray.add(0, getString(R.string.select_user));
                        //userkeyArray.add(0, getString(R.string.select_user));
                    }else{
                        userArray.add(0, "No user");
                        userkeyArray.add(0, "No user");
                    }

                    for (HashMap<String, String> map : userList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") userArray.add(entry.getValue());
                        }

                    for (HashMap<String, String> map : moduleList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "pid") modulekeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") moduleArray.add(entry.getValue());
                        }

                    ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(AddOptionToUserActivity.this, android.R.layout.simple_spinner_item, userArray);
                    user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                    sUsers.setAdapter(user_adapter);

                    ArrayAdapter<String> module_adapter = new ArrayAdapter<String>(AddOptionToUserActivity.this, android.R.layout.simple_spinner_item, moduleArray);
                    module_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner sModule = (Spinner) findViewById(R.id.spinnerModule);
                    sModule.setAdapter(module_adapter);


                    int selectUserIndex = user_adapter.getPosition(myUserName);
                    sUsers.setSelection(selectUserIndex); //set the default according to value
                    //report_user_id = String.valueOf(selectUserIndex);

                    int selectModuleIndex = module_adapter.getPosition(option);
                    sModule.setSelection(selectModuleIndex); //set the default according to value
                    report_module_id = String.valueOf(selectModuleIndex);

                    userIsInteracting = false;
                    moduleIsInteracting = false;

                    new LoadList(option).execute();

                }
            });

            hideProgressDialog();

        }
    }

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        String option;

        public LoadList(String option){
            this.option = option;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {
            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            itemList = new ArrayList<HashMap<String, String>>();
            itemUserList = new ArrayList<HashMap<String, String>>();

            item_user_list = new ArrayList<String>();
            //item_list = new ArrayList<String>();

            arraylistActivity = new ArrayList<cActivity>();
            arraylistCostplace = new ArrayList<cCostplace>();

            if(option.equals("Activity")){

                // ---- LOAD USER ACTIVITIES --- //
                if (report_user_id == "")
                {
                    report_user_id = myUserID;
                }

                // load data from provider
                itemUserList = new ArrayList<HashMap<String, String>>();

                List<cActivity> actListUser = MyProvider.getUserActivity(report_user_id);
                for(cActivity entry : actListUser)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("id",  entry.id);
                    map.put("name", entry.name);

                    item_user_list.add(entry.name);

                    // adding HashList to ArrayList
                    //itemUserList.add(map);
                }


                // ---- LOAD ALL ACTIVITIES --- //
                List<cActivity> actList = MyProvider.getUserActivity("");
                for(cActivity entry : actList)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("id",  entry.id);
                    map.put("name", entry.name);

                    if(item_user_list.size() > 0){
                        for(String v : item_user_list){
                            if(v.toString().equals(entry.name)){
                                map.put("option", "true");
                                break;
                            }else {
                                map.put("option", "false");
                            }
                        }
                    }else{
                        map.put("option", "false");
                    }


                    // adding HashList to ArrayList
                    itemList.add(map);
                }

                arraylistActivity.addAll(actList);

            } else if(option.equals("Costplace")){

                // ---- LOAD USER COSTPLACES --- //
                if (report_user_id == "")
                {
                    report_user_id = myUserID;
                }

                // load data from provider
                itemUserList = new ArrayList<HashMap<String, String>>();

                List<cCostplace> costListUser = MyProvider.getCostPlaces(report_user_id);
                for(cCostplace entry : costListUser)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("id",  Long.toString(entry.id));
                    map.put("name", entry.name);

                    item_user_list.add(entry.name);

                    // adding HashList to ArrayList
                    //itemUserList.add(map);
                }

                // ---- LOAD ALL COSTPLACES --- //
                List<cCostplace> costList = MyProvider.getCostPlaces("");
                for(cCostplace entry : costList)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("id",  String.valueOf(entry.id));
                    map.put("name", entry.name);

                    if(item_user_list.size() > 0){
                        for(String v : item_user_list){
                            if(v.toString().equals(entry.name)){
                                map.put("option", "true");
                                break;
                            }else {
                                map.put("option", "false");
                            }
                        }
                    }else{
                        map.put("option", "false");
                    }


                    // adding HashList to ArrayList
                    itemList.add(map);
                }

                arraylistCostplace.addAll(costList);
            }



            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    mAdapter = new AppAdapter();
                    lv.setAdapter(mAdapter);

                    //adapter = new AddOptionAdapter(AddOptionToUserActivity.this, R.layout.activity_manage_sublistview_item, itemList);
                    //lv.setAdapter(adapter);

                    // restore index and position
                    lv.setSelectionFromTop(index, top);
                }
            });

            hideProgressDialog();
        }
    }

    public class AddOptionToUserTask extends AsyncTask<String, String, Boolean> {

        String check;
        String selected;

        public AddOptionToUserTask(String check, String selected){
            this.check = check;
            this.selected = selected;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // extract data

            Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
            int myIndex = userSpinner.getSelectedItemPosition();
            String myUserid = "";
            if(myIndex != -1){
                myUserid = userkeyArray.get(myIndex);
            }

            Spinner moduleSpinner = (Spinner) findViewById(R.id.spinnerModule);
            myIndex = moduleSpinner.getSelectedItemPosition();
            String myModule = "";
            if(myIndex != -1){
                myModule = modulekeyArray.get(myIndex);
            }

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            //ArrayList<String> selectedItems = mAdapter.getSelectedItems();
            //for(String selected : selectedItems){
                //System.out.println(selected.toString());

            if(check.equals("true")){
                suc = MyProvider.addOptionToUser(myUserid, myModule, selected);
            }

            //}

            //ArrayList<String> unSelectedItems = mAdapter.getUnSelectedItems();
            //for(String unselected : unSelectedItems){
               // System.out.println(unselected.toString());

            if(check.equals("false")){
                suc = MyProvider.deleteOptionFromUser(myUserid, myModule, selected);
            }

            //}

            return suc;
        }

        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(getApplicationContext(), MyProvider.last_error, Toast.LENGTH_SHORT).show();

                        //finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();

                        // save index and top position
                        index = lv.getFirstVisiblePosition();
                        View v = lv.getChildAt(0);
                        top = (v == null) ? 0 : (v.getTop() - lv.getPaddingTop());

                        new LoadList(option).execute();

                        Globals.setValue("refresh_option","refresh_option");

                        //finish();
                    }
                }
            });
        }
    }

    private class CreateOptionToUserTask extends AsyncTask<String, String, Boolean> {

        String name;

        public CreateOptionToUserTask(String name) {
            this.name = name;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            if(option.equals("Activity")){
                suc = MyProvider.create_activity(name);
            }else if(option.equals("Costplace")){
                suc = MyProvider.create_costplace(name);
            }

            return suc;
        }

        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(getApplicationContext(), MyProvider.last_error, Toast.LENGTH_SHORT).show();


                    }else{
                        Toast.makeText(getApplicationContext(), "Successfully created!", Toast.LENGTH_SHORT).show();

                        new LoadList(option).execute();
                    }
                }
            });
        }
    }

    private class DeleteOption extends AsyncTask<String, String, Boolean> {

        String id;

        public DeleteOption(String id) {
            this.id = id;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            if(option.equals("Activity")){
                suc = MyProvider.delete_activity(id);
            }else if(option.equals("Costplace")){
                suc = MyProvider.delete_costplace(id);
            }

            return suc;
        }

        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(getApplicationContext(), MyProvider.last_error, Toast.LENGTH_SHORT).show();

                        if(!MyProvider.last_error.equals("")){
                            final HashMap<String, String> map = new HashMap<String, String>();
                            map.put("id",  deletedItemId);
                            map.put("name", deletedItemName);
                            map.put("option", deletedItemOption);
                            // Reinsert item to list
                            itemList.add(positionItem, map);
                            mAdapter.notifyDataSetChanged();
                        }

                    }else{
                        Toast.makeText(getApplicationContext(), "Successfully deleted!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    static List<cActivity> listActivityFiltered;
    static private ArrayList<cActivity> arraylistActivity;
    static List<cCostplace> listCostplaceFiltered;
    static private ArrayList<cCostplace> arraylistCostplace;

    public static List<cActivity> filterActivity(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listActivityFiltered = new ArrayList<cActivity>();
        listActivityFiltered.clear();
        if (charText.length() == 0) {
            listActivityFiltered.addAll(arraylistActivity);
        } else {
            for (cActivity activity : arraylistActivity) {
                if (activity.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listActivityFiltered.add(activity);
                }
            }
        }
        return listActivityFiltered;
    }

    public static List<cCostplace> filterCostplace(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listCostplaceFiltered = new ArrayList<cCostplace>();
        listCostplaceFiltered.clear();
        if (charText.length() == 0) {
            listCostplaceFiltered.addAll(arraylistCostplace);
        } else {
            for (cCostplace costplace : arraylistCostplace) {
                if (costplace.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listCostplaceFiltered.add(costplace);
                }
            }
        }
        return listCostplaceFiltered;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text

        String text = newText.toString().toLowerCase(Locale.getDefault());

        if(option.equals("Activity")){

            List<cActivity> filter_projects = filterActivity(text.toString());
            itemList.clear(); // <--- clear the list before add

            for(cActivity entry : filter_projects)
            {

                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  (entry.id));
                map.put("name", entry.name);

                if(item_user_list.size() > 0){
                    for(String v : item_user_list){
                        if(v.toString().equals(entry.name)){
                            map.put("option", "true");
                            break;
                        }else {
                            map.put("option", "false");
                        }
                    }
                }else{
                    map.put("option", "false");
                }

                itemList.add(map);
            }

            mAdapter = new AppAdapter();
            //lv.setAdapter(null);
            lv.setAdapter(mAdapter);

        }else if (option.equals("Costplace")){

            List<cCostplace> filter_projects = filterCostplace(text.toString());
            itemList.clear(); // <--- clear the list before add

            for(cCostplace entry : filter_projects)
            {

                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);

                if(item_user_list.size() > 0){
                    for(String v : item_user_list){
                        if(v.toString().equals(entry.name)){
                            map.put("option", "true");
                            break;
                        }else {
                            map.put("option", "false");
                        }
                    }
                }else{
                    map.put("option", "false");
                }

                itemList.add(map);
            }

            mAdapter = new AppAdapter();
            //lv.setAdapter(null);
            lv.setAdapter(mAdapter);
        }


        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_option, menu);

        if(option.equals("Activity")){
            searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setIconifiedByDefault(false);
            searchView.setOnQueryTextListener(this);
            searchView.setSubmitButtonEnabled(false);
            searchView.setQueryHint(getString(R.string.search_title_activity));

        }else if (option.equals("Costplace")){
            searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setIconifiedByDefault(false);
            searchView.setOnQueryTextListener(this);
            searchView.setSubmitButtonEnabled(false);
            searchView.setQueryHint(getString(R.string.search_title_costplace));

        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_create_option:
                alertCreateOption();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void alertCreateOption() {

        // get prompt_password_reset view
        LayoutInflater li = LayoutInflater.from(AddOptionToUserActivity.this);
        View promptView = li.inflate(R.layout.prompt_create_option, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddOptionToUserActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptView);

        final TextView title_option = (TextView) promptView.findViewById(R.id.textView1);
        final EditText option_name = (EditText) promptView.findViewById(R.id.editTextOption);

        if(option.equals("Activity")){
            title_option.setText("Create an activity");
        }else if(option.equals("Costplace")){
            title_option.setText("Create a costplace");
        }

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //Do nothing here because we override this button later to change the close behaviour.
                                //However, we still need this because on older versions of Android unless we
                                //pass a handler the button doesn't get instantiated

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        //Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alertDialog.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.

                boolean failFlag = false;
                if(option_name.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    option_name.setError("Name is required!");
                }

                if(failFlag == false){

                    new CreateOptionToUserTask(option_name.getText().toString()).execute();

                    alertDialog.dismiss();
                }

            }
        });

    }

/*    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK){

            new AddOptionToUserTask().execute();
            finish();

            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }*/

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }


    class AppAdapter extends BaseSwipeListAdapter {

        //final ArrayList<String> mSelectedItems = new ArrayList<String>();
        //final ArrayList<String> mUnSelectedItems = new ArrayList<String>();
        final boolean[] checkBoxState = new boolean[itemList.size()];

        @Override
        public int getCount() {
            return itemList.size();
        }

        @Override
        public String getItem(int position) {
            HashMap<String, String> item = itemList.get(position);
            String name = Html.fromHtml(item.get("name")).toString();
            return name;

        }

        @Override
        public long getItemId(int position) {
            HashMap<String, String> item = itemList.get(position);
            String id = Html.fromHtml(item.get("id")).toString();
            return Long.valueOf(id);
        }

        @Override
        public boolean getItemOption(int position){
            HashMap<String, String> item = itemList.get(position);
            String id = Html.fromHtml(item.get("option")).toString();
            if(id.equals("true")){
                return true;
            }else{
                return false;
            }
        }

        class ViewHolder {
            TextView id;
            TextView name;
            CheckBox option_check;

            public ViewHolder(View view) {
                id = (TextView) view.findViewById(R.id.textViewId);
                name = (TextView) view.findViewById(R.id.textViewName);
                option_check = (CheckBox) view.findViewById(R.id.checkbox);
                view.setTag(this);
            }
        }

/*        public ArrayList getSelectedItems(){
            return mSelectedItems;
        }

        public ArrayList getUnSelectedItems(){
            return mUnSelectedItems;
        }*/

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(),
                        R.layout.activity_manage_sublistview_item, null);
                new ViewHolder(convertView);
            }
            final ViewHolder holder = (ViewHolder) convertView.getTag();

            String item_name = getItem(position);
            String item_id = String.valueOf(getItemId(position));
            boolean item_option = getItemOption(position);

            Log.e("Zdit id", item_id.toString());
            Log.e("Zdit name", item_name.toString());

            holder.id.setText(item_id);
            holder.name.setText(item_name);
            holder.option_check.setChecked(checkBoxState[position]);

            HashMap<String, String>hashmap_Current;
            hashmap_Current=new HashMap<String, String>();
            hashmap_Current=itemList.get(position);

            for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
            {
                if (entry.getKey() == "option")
                {
                    try {
                        String myRow = entry.getValue().toString();

                        if(myRow.contains("true")){
                            //holder.option_check.setChecked(checkBoxState[position] = true);
                            holder.option_check.setChecked(checkBoxState[position] = item_option);
                        }else{
                            //holder.option_check.setChecked(checkBoxState[position] = false);
                            holder.option_check.setChecked(checkBoxState[position] = item_option);
                        }

                    } catch(NumberFormatException nfe) {

                    }
                }
            }

            holder.option_check.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()){
                        checkBoxState[position]=true;
                        //mSelectedItems.add(holder.id.getText().toString());
                        //mUnSelectedItems.remove(holder.id.getText().toString());
                        new AddOptionToUserTask("true", holder.id.getText().toString()).execute();
                    }else{
                        checkBoxState[position]=false;
                        //mSelectedItems.remove(holder.id.getText().toString());
                        //mUnSelectedItems.add(holder.id.getText().toString());
                        new AddOptionToUserTask("false", holder.id.getText().toString()).execute();
                    }
                }
            });

            return convertView;
        }


        @Override
        public boolean getSwipeEnableByPosition(int position) {
            if(position % 1 == 0){
                return true;
            }
            return false;
        }
    }
}
