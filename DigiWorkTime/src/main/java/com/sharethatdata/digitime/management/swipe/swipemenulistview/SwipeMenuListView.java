package com.sharethatdata.digitime.management.swipe.swipemenulistview;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sharethatdata.digitime.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


public class SwipeMenuListView extends ListView {

    private static final int TOUCH_STATE_NONE = 0;
    private static final int TOUCH_STATE_X = 1;
    private static final int TOUCH_STATE_Y = 2;

    public static final int DIRECTION_LEFT = 1;
    public static final int DIRECTION_RIGHT = -1;
    private int mDirection = 1;//swipe from right to left by default

    private int MAX_Y = 5;
    private int MAX_X = 3;
    private float mDownX;
    private float mDownY;
    private int mTouchState;
    private int mTouchPosition;
    private SwipeMenuLayout mTouchView;
    private OnSwipeListener mOnSwipeListener;

    private SwipeMenuCreator mMenuCreator;
    private OnMenuItemClickListener mOnMenuItemClickListener;
    private OnMenuStateChangeListener mOnMenuStateChangeListener;
    private Interpolator mCloseInterpolator;
    private Interpolator mOpenInterpolator;

    public SwipeMenuListView(Context context) {
        super(context);
        init();
    }

    public SwipeMenuListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SwipeMenuListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        MAX_X = dp2px(MAX_X);
        MAX_Y = dp2px(MAX_Y);
        mTouchState = TOUCH_STATE_NONE;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(new SwipeMenuAdapter(getContext(), adapter) {
            @Override
            public void createMenu(SwipeMenu menu) {
                if (mMenuCreator != null) {
                    mMenuCreator.create(menu);
                }
            }

            @Override
            public void onItemClick(SwipeMenuView view, SwipeMenu menu,
                                    int index) {
                boolean flag = false;
                if (mOnMenuItemClickListener != null) {
                   // flag = mOnMenuItemClickListener.onMenuItemClick(view.getPosition(), menu, index);
                    flag = mOnMenuItemClickListener.onMenuItemClick(view, view.getPosition(), menu, index);
                }
                if (mTouchView != null && !flag) {
                    mTouchView.smoothCloseMenu();
                }
            }
        });
    }

    public void setCloseInterpolator(Interpolator interpolator) {
        mCloseInterpolator = interpolator;
    }

    public void setOpenInterpolator(Interpolator interpolator) {
        mOpenInterpolator = interpolator;
    }

    public Interpolator getOpenInterpolator() {
        return mOpenInterpolator;
    }

    public Interpolator getCloseInterpolator() {
        return mCloseInterpolator;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //In the interception of dealing with the slide in the set where the event can also swipe,
        // click and can not affect the original click event
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mDownX = ev.getX();
                mDownY = ev.getY();
                boolean handled = super.onInterceptTouchEvent(ev);
                mTouchState = TOUCH_STATE_NONE;
                mTouchPosition = pointToPosition((int) ev.getX(), (int) ev.getY());
                View view = getChildAt(mTouchPosition - getFirstVisiblePosition());

                //Only in the empty when the assignment to avoid each touch are assigned, there will be multiple open state
                if (view instanceof SwipeMenuLayout) {
                    //If there is open to intercept.
                    if (mTouchView != null && mTouchView.isOpen() && !inRangeOfView(mTouchView.getMenuView(), ev)) {
                        return true;
                    }
                    mTouchView = (SwipeMenuLayout) view;
                    mTouchView.setSwipeDirection(mDirection);
                }
                //If you touch another view
                if (mTouchView != null && mTouchView.isOpen() && view != mTouchView) {
                    handled = true;
                }

                if (mTouchView != null) {
                    mTouchView.onSwipe(ev);
                }
                return handled;
            case MotionEvent.ACTION_MOVE:
                float dy = Math.abs((ev.getY() - mDownY));
                float dx = Math.abs((ev.getX() - mDownX));
                if (Math.abs(dy) > MAX_Y || Math.abs(dx) > MAX_X) {
                    //Each time the block down the touch state is set to TOUCH_STATE_NONE only
                    // return true will go onTouchEvent so write here is enough
                    if (mTouchState == TOUCH_STATE_NONE) {
                        if (Math.abs(dy) > MAX_Y) {
                            mTouchState = TOUCH_STATE_Y;
                        } else if (dx > MAX_X) {
                            mTouchState = TOUCH_STATE_X;
                            if (mOnSwipeListener != null) {
                                mOnSwipeListener.onSwipeStart(mTouchPosition);
                            }
                        }
                    }
                    return true;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getAction() != MotionEvent.ACTION_DOWN && mTouchView == null)
            return super.onTouchEvent(ev);
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                int oldPos = mTouchPosition;
                mDownX = ev.getX();
                mDownY = ev.getY();
                mTouchState = TOUCH_STATE_NONE;

                mTouchPosition = pointToPosition((int) ev.getX(), (int) ev.getY());

                if (mTouchPosition == oldPos && mTouchView != null
                        && mTouchView.isOpen()) {
                    mTouchState = TOUCH_STATE_X;
                    mTouchView.onSwipe(ev);
                    return true;
                }

                View view = getChildAt(mTouchPosition - getFirstVisiblePosition());

                if (mTouchView != null && mTouchView.isOpen()) {
                    mTouchView.smoothCloseMenu();
                    mTouchView = null;
                    // return super.onTouchEvent(ev);
                    // try to cancel the touch event
                    MotionEvent cancelEvent = MotionEvent.obtain(ev);
                    cancelEvent.setAction(MotionEvent.ACTION_CANCEL);
                    onTouchEvent(cancelEvent);
                    if (mOnMenuStateChangeListener != null) {
                        mOnMenuStateChangeListener.onMenuClose(oldPos);
                    }
                    return true;
                }
                if (view instanceof SwipeMenuLayout) {
                    mTouchView = (SwipeMenuLayout) view;
                    mTouchView.setSwipeDirection(mDirection);
                }
                if (mTouchView != null) {
                    mTouchView.onSwipe(ev);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(mTouchBeforeAutoHide && mUndoPopup.isShowing()) {
                    // Send a delayed message to hide popup
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(mDelayedMsgId),
                            mAutoHideDelay);
                }
                //Some may have a header, to reduce the header and then judge
                mTouchPosition = pointToPosition((int) ev.getX(), (int) ev.getY()) - getHeaderViewsCount();
                //If the slide did not fully show, to take back, this time mTouchView has been assigned,
                // and then slide the other can not swip the view will lead to mTouchView swipe.
                // So use the location to determine whether the slide is a view
                if (!mTouchView.getSwipeEnable() || mTouchPosition != mTouchView.getPosition()) {
                    break;
                }
                float dy = Math.abs((ev.getY() - mDownY));
                float dx = Math.abs((ev.getX() - mDownX));
                if (mTouchState == TOUCH_STATE_X) {
                    if (mTouchView != null) {
                        mTouchView.onSwipe(ev);
                    }
                    getSelector().setState(new int[]{0});
                    ev.setAction(MotionEvent.ACTION_CANCEL);
                    super.onTouchEvent(ev);
                    return true;
                } else if (mTouchState == TOUCH_STATE_NONE) {
                    if (Math.abs(dy) > MAX_Y) {
                        mTouchState = TOUCH_STATE_Y;
                    } else if (dx > MAX_X) {
                        mTouchState = TOUCH_STATE_X;
                        if (mOnSwipeListener != null) {
                            mOnSwipeListener.onSwipeStart(mTouchPosition);
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mTouchState == TOUCH_STATE_X) {
                    if (mTouchView != null) {
                        boolean isBeforeOpen = mTouchView.isOpen();
                        mTouchView.onSwipe(ev);
                        boolean isAfterOpen = mTouchView.isOpen();
                        if (isBeforeOpen != isAfterOpen && mOnMenuStateChangeListener != null) {
                            if (isAfterOpen) {
                                // slide opened
                                mOnMenuStateChangeListener.onMenuOpen(mTouchPosition);
                            } else {
                                // slide closed
                                mOnMenuStateChangeListener.onMenuClose(mTouchPosition);
                            }
                        }
                        if (!isAfterOpen) {
                            // slide closed
                            mTouchPosition = -1;
                            mTouchView = null;
                        }
                    }
                    if (mOnSwipeListener != null) {
                        mOnSwipeListener.onSwipeEnd(mTouchPosition);
                    }
                    ev.setAction(MotionEvent.ACTION_CANCEL);
                    super.onTouchEvent(ev);
                    return true;
                }
                break;
        }
        return super.onTouchEvent(ev);
    }

    public void smoothOpenMenu(int position) {
        if (position >= getFirstVisiblePosition()
                && position <= getLastVisiblePosition()) {
            View view = getChildAt(position - getFirstVisiblePosition());
            if (view instanceof SwipeMenuLayout) {
                mTouchPosition = position;
                if (mTouchView != null && mTouchView.isOpen()) {
                    mTouchView.smoothCloseMenu();
                }
                mTouchView = (SwipeMenuLayout) view;
                mTouchView.setSwipeDirection(mDirection);
                mTouchView.smoothOpenMenu();
            }
        }
    }

    public void smoothCloseMenu(){
        if (mTouchView != null && mTouchView.isOpen()) {
            mTouchView.smoothCloseMenu();
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getContext().getResources().getDisplayMetrics());
    }

    public void setMenuCreator(SwipeMenuCreator menuCreator) {
        this.mMenuCreator = menuCreator;
    }

    public void setOnMenuItemClickListener(
            OnMenuItemClickListener onMenuItemClickListener) {
        this.mOnMenuItemClickListener = onMenuItemClickListener;
    }

    public void setOnSwipeListener(OnSwipeListener onSwipeListener) {
        this.mOnSwipeListener = onSwipeListener;
    }

    public void setOnMenuStateChangeListener(OnMenuStateChangeListener onMenuStateChangeListener) {
        mOnMenuStateChangeListener = onMenuStateChangeListener;
    }

    public static interface OnMenuItemClickListener {
        //boolean onMenuItemClick(int position, SwipeMenu menu, int index);
        boolean onMenuItemClick(View view, int position, SwipeMenu menu, int index);
    }

    public static interface OnSwipeListener {
        void onSwipeStart(int position);

        void onSwipeEnd(int position);
    }

    public static interface OnMenuStateChangeListener {
        void onMenuOpen(int position);

        void onMenuClose(int position);
    }

    public void setSwipeDirection(int direction) {
        mDirection = direction;
    }

    /**
     * To determine whether the click event is within a view
     *
     * @param view
     * @param ev
     * @return
     */
    public static boolean inRangeOfView(View view, MotionEvent ev) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        if (ev.getRawX() < x || ev.getRawX() > (x + view.getWidth()) || ev.getRawY() < y || ev.getRawY() > (y + view.getHeight())) {
            return false;
        }
        return true;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////
    // UNDO //

    // Fixed properties
    private AbsListView mListView;
    private OnDismissCallback mCallback;

    // Transient properties
    private SortedSet<PendingDismissData> mPendingDismisses = new TreeSet<PendingDismissData>();

    private float mDensity;

    private UndoMode mMode;
    private List<Undoable> mUndoActions;
    private Handler mHandler;

    public PopupWindow mUndoPopup;
    private TextView mUndoText;
    public Button mUndoButton;

    private int mAutoHideDelay = 5000;
    private String mDeleteString = "Item deleted";
    private boolean mTouchBeforeAutoHide = true;

    public int mDelayedMsgId;

    class PendingDismissData implements Comparable<PendingDismissData> {

        public int position;
        public View view;

        public PendingDismissData(int position, View view) {
            this.position = position;
            this.view = view;
        }

        @Override
        public int compareTo(PendingDismissData other) {
            // Sort by descending position
            return other.position - position;
        }
    }

    /**
     * Constructs a new swipe-to-dismiss touch listener for the given list view.
     *
     * @param listView The list view whose items should be dismissable.
     * she would like to dismiss one or more list items.
     */
    public void SwipeUndo(AbsListView listView, OnDismissCallback callback, UndoMode mode) {

        if(listView == null) {
            throw new IllegalArgumentException("listview must not be null.");
        }

        mHandler = new HideUndoPopupHandler();
        mListView = listView;
        mCallback = callback;
        mMode = mode;

        mDensity = mListView.getResources().getDisplayMetrics().density;

        // -- Load undo popup --
        LayoutInflater inflater = (LayoutInflater) mListView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.undo_popup, null);
        mUndoButton = (Button)v.findViewById(R.id.undo);
        mUndoButton.setOnClickListener(new UndoHandler());
        mUndoButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // If user tabs "undo" button, reset delay time to remove popup
                mDelayedMsgId++;
                return false;
            }
        });
        mUndoText = (TextView)v.findViewById(R.id.text);

        mUndoPopup = new PopupWindow(v);
        mUndoPopup.setAnimationStyle(R.style.fade_animation);
        // Get scren width in dp and set width respectively
        int xdensity = (int)(mListView.getContext().getResources().getDisplayMetrics().widthPixels / mDensity);
        if(xdensity < 300) {
            mUndoPopup.setWidth((int)(mDensity * 280));
        } else if(xdensity < 350) {
            mUndoPopup.setWidth((int)(mDensity * 300));
        } else if(xdensity < 500) {
            mUndoPopup.setWidth((int)(mDensity * 330));
        } else {
            mUndoPopup.setWidth((int)(mDensity * 450));
        }
        mUndoPopup.setHeight((int)(mDensity * 56));
        // -- END Load undo popup --

        switch(mode) {
            case SINGLE_UNDO:
                mUndoActions = new ArrayList<Undoable>(1);
                break;
            default:
                //mUndoActions = new ArrayList<Undoable>(10);
                break;
        }

    }

    /**
     * Handler used to hide the undo popup after a special delay.
     */
    public class HideUndoPopupHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == mDelayedMsgId) {
                // Call discard on any element
                for(Undoable undo : mUndoActions) {
                    undo.discard();
                }
                mUndoActions.clear();
                mUndoPopup.dismiss();
            }
        }
    }

    /**
     * Takes care of undoing a dismiss. This will be added as a
     * { View.OnClickListener} to the undo button in the undo popup.
     */
    public class UndoHandler implements View.OnClickListener {

        public void onClick(View v) {

            if(!mUndoActions.isEmpty()) {
                switch(mMode) {
                    case SINGLE_UNDO:
                        mUndoActions.get(0).undo();
                        mUndoActions.clear();
                        break;
                }
            }

            // Dismiss dialog or change text
            if(mUndoActions.isEmpty()) {
                mUndoPopup.dismiss();
            }

            mDelayedMsgId++;
        }

    }

    public void performDismiss(final View dismissView, final int dismissPosition) {
        // Animate the dismissed list item to zero-height and fire the dismiss callback when
        // all dismissed list item animations have completed. This triggers layout on each animation
        // frame; in the future we may want to do something smarter and more performant.

        mPendingDismisses.add(new PendingDismissData(dismissPosition, dismissView));

        final ViewGroup.LayoutParams lp1 = dismissView.getLayoutParams();
        final int originalHeight = dismissView.getHeight();

        for(PendingDismissData dismiss : mPendingDismisses) {
            if(mMode == UndoMode.SINGLE_UNDO) {
                for(Undoable undoable : mUndoActions) {
                    undoable.discard();
                }
                mUndoActions.clear();
            }
            Undoable undoable = mCallback.onDismiss(mListView, dismiss.position);
            if(undoable != null) {
                mUndoActions.add(undoable);
            }
            mDelayedMsgId++;
        }

        if(!mUndoActions.isEmpty()) {
            mUndoText.setText(mDeleteString);
            mUndoButton.setText("Undo");

            // Show undo popup
            mUndoPopup.showAtLocation(mListView,
                    Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
                    0, (int)(mDensity * 15));

            // Queue the dismiss only if required
            if(!mTouchBeforeAutoHide) {
                // Send a delayed message to hide popup
                mHandler.sendMessageDelayed(mHandler.obtainMessage(mDelayedMsgId),
                        mAutoHideDelay);
            }
        }

        ViewGroup.LayoutParams lp;
        for (PendingDismissData pendingDismiss : mPendingDismisses) {
            // Reset view presentation
            lp = pendingDismiss.view.getLayoutParams();
            lp.height = originalHeight;
            pendingDismiss.view.setLayoutParams(lp);
        }

        mPendingDismisses.clear();



    }

    /**
     * Sets whether another touch on the view is required before the popup counts down to
     * dismiss. By default this is set to true.
     *
     * @param require Whether a touch is required before starting the auto-dismiss timer.
     */
    public void setRequireTouchBeforeDismiss(boolean require) {
        mTouchBeforeAutoHide = require;
    }

    /**
     * Sets the string shown in the undo popup. This will only show if
     * the { Undoable} returned by the OnDismissCallback} returns
     * {@code null} from its { Undoable#getTitle()} method.
     *
     * @param msg The string shown in the undo popup.
     */
    public void setUndoString(String msg) {
        mDeleteString = msg;
    }

    /**
     * Sets the time in milliseconds after which the undo popup automatically
     * disappears.
     *
     * @param delay Delay in milliseconds.
     */
    public void setAutoHideDelay(int delay) {
        mAutoHideDelay = delay;
    }

    /**
     * Discard all stored undos and hide the undo popup dialog.
     */
    public void discardUndo() {
        for(Undoable undoable : mUndoActions) {
            undoable.discard();
        }
        mUndoActions.clear();
        mUndoPopup.dismiss();
    }


    /**
     * The callback interface used by { SwipeDismissListViewTouchListener}
     * to inform its client about a successful dismissal of one or more list
     * item positions.
     */
    public interface OnDismissCallback {

        /**
         * Called when the user has indicated they she would like to dismiss one
         * or more list item positions.
         *
         * @param listView The originating { ListView}.
         * @param position The position of the item to dismiss.
         */
        Undoable onDismiss(AbsListView listView, int position);
    }

    /**
     * An implementation of this abstract class must be returned by the
     * { OnDismissCallback#onDismiss(android.widget.ListView, int)} method,
     * if the user should be able to undo that dismiss. If the action will be undone
     * by the user { #undo()} will be called. That method should undo the previous
     * deletion of the item and add it back to the adapter. Read the README file for
     * more details. If you implement the { #getTitle()} method, the undo popup
     * will show an individual title for that item. Otherwise the default title
     * (set via { #setUndoString(java.lang.String)}) will be shown.
     */
    public abstract static class Undoable {

        /**
         * Returns the individual undo message for this item shown in the
         * popup dialog.
         *
         * @return The individual undo message.
         */
        public String getTitle() {
            return null;
        }

        /**
         * Undoes the deletion.
         */
        public abstract void undo();

        /**
         * Will be called when this Undoable won't be able to undo anymore,
         * meaning the undo popup has disappeared from the screen.
         */
        public void discard() { }

    }

    /**
     * Defines the mode a { SwipeDismissList} handles multiple undos.
     */
    public enum UndoMode {
        /**
         * Only give the user the possibility to undo the last action.
         * As soon as another item is deleted, there is no chance to undo
         * the previous deletion.
         */
        SINGLE_UNDO
    };


}
