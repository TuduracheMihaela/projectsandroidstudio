package com.sharethatdata.digitime;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.R.layout;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

public class SickdaysRequestFragment extends DialogFragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	public static final String inputFormat = "HH:mm";
	SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);
	
	private int mStartHour,mStartMinute, mEndHour,mEndMinute;
	private int mStartYear,mStartMonth,mStartDay, mEndYear,mEndMonth,mEndDay;
	
	private TextView startTimeView, endTimeView, startDateView, endDateView; 
	private TextView numberOfDaysText;
	private EditText descriptionEditText;
	private Button btnSubmit;
	
	private double getNumberOfDays = 1;
	
	private String myUser = "";
	private String myPass = "";
	
	private String error_message = "";
	
	//long elapsedHoursEndBefore = 0; long elapsedHoursStartBefore = 0;
	//private int startHourBefore, endHourBefore;
	//boolean beforeEnd = true; boolean beforeStart = true;

	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.fragment_sickdays_request, container, false);
	        	        
	        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("defaults", Context.MODE_PRIVATE );
			myUser = sharedPrefs.getString("prefUsername", "");
							
			Globals = ((MyGlobals) getActivity().getApplicationContext());
		    Globals.backgroundProcess();
			myUser = Globals.getValue("user");
			myPass = Globals.getValue("pass");
	        
	        MyProvider = new WSDataProvider(myUser, myPass);

		    MyTimeUtils = new TimeUtils();
		    MyTimeUtils.setOrientation(getActivity().getApplicationContext(), getActivity());
	        
	     // set calendar
			Calendar c = Calendar.getInstance();
		    mStartYear = c.get(Calendar.YEAR);
		    mStartMonth = c.get(Calendar.MONTH);
		    mStartDay = c.get(Calendar.DAY_OF_MONTH);
		    mEndYear = c.get(Calendar.YEAR);
		    mEndMonth = c.get(Calendar.MONTH);
		    mEndDay = c.get(Calendar.DAY_OF_MONTH);
		    mStartHour = c.get(Calendar.HOUR_OF_DAY);
		    //mStartMinute = c.get(Calendar.MINUTE);
		    mEndHour = c.get(Calendar.HOUR_OF_DAY);
		    //mEndMinute = c.get(Calendar.MINUTE);
		    
		    mStartMinute = 00;
		    mEndMinute = 00;
		    
		    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
		    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	        
	        startDateView = (TextView) rootView.findViewById(R.id.dateStartText);
	        startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
	            .append(pad(mStartMonth + 1)).append("-")
	            .append(pad(mStartYear)));
	        
	        endDateView = (TextView) rootView.findViewById(R.id.dateEndText);
	        endDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mEndDay)).append("-")
	            .append(pad(mEndMonth + 1)).append("-")
	            .append(pad(mEndYear)));
		    
		    startTimeView = (TextView) rootView.findViewById(R.id.timeStartText);
	        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

		    endTimeView = (TextView) rootView.findViewById(R.id.timeEndText);
	        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
	        
	        numberOfDaysText = (TextView) rootView.findViewById(R.id.numberOfDaysText);
	        numberOfDaysText.setText("1");
	        	        		
	        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);

	        startDateView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showStartDatePicker();
				} }); 
	        
	        endDateView.setOnClickListener(new OnClickListener() {		
				@Override
				public void onClick(View v) {
					showEndDatePicker();
				} }); 
	        
	        startTimeView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showStartTimePicker();
					//showDialogHours();
				} });
			
	        endTimeView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showEndTimePicker();
					//showDialogHours();
				} });
	        
	        btnSubmit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					RegisterTaskSickday sickdayTask;
					sickdayTask = new RegisterTaskSickday();
					sickdayTask.execute();
				} });
	        
	        //calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
			
	        
	        return rootView;  
	 }
	 
	 private Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
	 
	 
	      /* *** Start Date *** */
	 private void showStartDatePicker() {
		  DatePickerFragment date = new DatePickerFragment();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("year", mStartYear);
		  args.putInt("month", mStartMonth);
		  args.putInt("day", mStartDay);
		  date.setArguments(args);
		  /**
		   * Set Call back to capture selected date
		   */
		  date.setCallBack(onStartDate);
		  date.show(getFragmentManager(), "Start Date Picker");
		 }
	 
	 		/* *** End Date *** */
	 private void showEndDatePicker() {
		  DatePickerFragment date = new DatePickerFragment();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("year", mEndYear);
		  args.putInt("month", mEndMonth);
		  args.putInt("day", mEndDay);
		  date.setArguments(args);
		  /**
		   * Set Call back to capture selected date
		   */
		  date.setCallBack(onEndDate);
		  date.show(getFragmentManager(), "End Date Picker");
		 }
	 
	 		/* *** Start Time *** */
	 private void showStartTimePicker() {
		TimePickerFragment time = new TimePickerFragment();
		 //CustomHourPickerDialog time = new CustomHourPickerDialog();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("hours", mStartHour);
		  args.putInt("minutes", mStartMinute);
		  time.setArguments(args);
		  /**
		   * Set Call back to capture selected date
		   */
		  time.setCallBack(onStartTime);
		  time.show(getFragmentManager(), "Start Time Picker");
		 }
	 
	 		/* *** End Time *** */
	 private void showEndTimePicker() {
		 TimePickerFragment time = new TimePickerFragment();
		 //CustomHourPickerDialog time = new CustomHourPickerDialog();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("hours", mEndHour);
		  args.putInt("minutes", mEndMinute);
		  time.setArguments(args);
		 /**
		   * Set Call back to capture selected date
		   */
		  time.setCallBack(onEndTime);
		  time.show(getFragmentManager(), "End Time Picker");
		 }
	 
	 		/* *** Start Date *** */
	 OnDateSetListener onStartDate = new OnDateSetListener() {
		  @Override
		  public void onDateSet(DatePicker view, int year, int monthOfYear,
		    int dayOfMonth) {
			  
			  	mStartYear = year;
	            mStartMonth = monthOfYear;
	            mStartDay = dayOfMonth; 
			  
			  String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
			  
			  startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));

			  		/* numberOfDaysText */
			  final Calendar date1 = Calendar.getInstance();
			  final Calendar date2 = Calendar.getInstance();

			  date1.clear();
			  date1.set(mStartYear, mStartMonth, mStartDay);
			  date2.clear();
			  date2.set(mEndYear, mEndMonth, mEndDay);

			  int numberOfDays = 1;
			  while (date1.before(date2)) {
				  date1.add(Calendar.DATE,1);
				  if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
						  &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
					  numberOfDays++;
				  }else {
					  date1.add(Calendar.DATE,1);
				  }
			  }
			  getNumberOfDays = numberOfDays;
			  numberOfDaysText.setText(String.valueOf(numberOfDays));
			  
			  /*beforeStart = true;
			  beforeEnd = true;
	          calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);*/
		  }
		 };

		  /* *** End Date *** */
	 OnDateSetListener onEndDate = new OnDateSetListener() {
		  @Override
		  public void onDateSet(DatePicker view, int year, int monthOfYear,
		    int dayOfMonth) {
			  
			  	mEndYear = year;
	            mEndMonth = monthOfYear;
	            mEndDay = dayOfMonth; 
			  
			  String dayOfWeekStart = DateFormat.format("EEEE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();
			  
			  endDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mEndDay)).append("-")
		                .append(pad(mEndMonth + 1)).append("-")
		                .append(pad(mEndYear)));

			  /* numberOfDaysText */
			  final Calendar date1 = Calendar.getInstance();
			  final Calendar date2 = Calendar.getInstance();

			  date1.clear();
			  date1.set(mStartYear, mStartMonth, mStartDay);
			  date2.clear();
			  date2.set(mEndYear, mEndMonth, mEndDay);

			  int numberOfDays = 1;
			  while (date1.before(date2)) {
				  date1.add(Calendar.DATE,1);
				  if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
						  &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
					  numberOfDays++;
				  }else {
					  date1.add(Calendar.DATE,1);
				  }
			  }

			  // long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

			  //float dayCount = (float) diff / (24 * 60 * 60 * 1000);
			  getNumberOfDays = numberOfDays;
			  numberOfDaysText.setText(String.valueOf(numberOfDays));

			  /*beforeStart = true;
			  beforeEnd = true;
	          calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);*/
		  }
		 };
		 
		 	/* *** Start Time *** */
	OnTimeSetListener onStartTime = new OnTimeSetListener() {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mStartHour = hourOfDay;
			mStartMinute = minute;

			 startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
			 
			 /*beforeStart = true;
			 beforeEnd = true;
			 calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);*/
			
		}
	};
		 
		/* ***  *** End Time *** *** */
	OnTimeSetListener onEndTime = new OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				mEndHour = hourOfDay;
				mEndMinute = minute;
				
				 endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
		                    .append(pad(mEndMinute)));
				 
				 /*beforeStart = true;
				 beforeEnd = true;
				 calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);*/
				
			}
		};	
		
/*	private void calculateTotalHours(int startHour, int startMinute, int endHour, int endMinute){
		
		SharedPreferences sharedPrefs = getActivity().getSharedPreferences("defaults", Context.MODE_PRIVATE );
		String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
		String endDay = sharedPrefs.getString("prefNotificationEndDay", "");
		
		String hourS = null;
        String minS = null;
        String hourE = null;
        String minE = null;
        
		if(!startDay.isEmpty()){
			String[] tokens = startDay.split(":");
            hourS = tokens[0];
            minS = tokens[1];
		}else{
			hourS = "09";
			minS = "00";
		}
		
		if(!endDay.isEmpty()){
			String[] tokens = endDay.split(":");
            hourE = tokens[0];
            minE = tokens[1];
		}else{
			hourE = "17";
			minE = "00";
		}
		
		String dateStart = startDateView.getText().toString();
		String dateEnd = endDateView.getText().toString();
		// same day, calculate difference between hours
		if(dateStart.equals(dateEnd)){
			Date dateCompareStart = parseDate(String.valueOf(startHour) + ":" + String.valueOf(startMinute));
			Date dateCompareEnd = parseDate(String.valueOf(endHour) + ":" + String.valueOf(endMinute));
			
			//milliseconds
			long difference = dateCompareEnd.getTime() - dateCompareStart.getTime();
			long secondsInMilli = 1000;
			long minutesInMilli = secondsInMilli * 60;
			long hoursInMilli = minutesInMilli * 60;
			
			long elapsedHours = difference / hoursInMilli;
			
			if(elapsedHours <= 0){
				getNumberOfDays=0;
				numberOfDaysText.setText(String.valueOf(getNumberOfDays));
			}else if(elapsedHours <= 4){
				getNumberOfDays=0;
				getNumberOfDays = getNumberOfDays + 0.5;
				numberOfDaysText.setText(String.valueOf(getNumberOfDays));
			}else if(elapsedHours > 4){
				getNumberOfDays=0;
				getNumberOfDays=getNumberOfDays+1;
				numberOfDaysText.setText(String.valueOf(getNumberOfDays));
			}
		}
		// different days, 
		else{
			
			// Start
			  final Calendar dateS1 = Calendar.getInstance();
	          final Calendar dateS2 = Calendar.getInstance();

	       // Start
	          dateS1.clear();
	          dateS1.set(mStartYear, mStartMonth, mStartDay);
	          dateS2.clear();
	          dateS2.set(mEndYear, mEndMonth, mEndDay);
	          
	          double numberOfDays = 0;
	          
	          while (dateS1.before(dateS2) || dateS1.equals(dateS2)) {
	        	  
	              if ((Calendar.SATURDAY != dateS1.get(Calendar.DAY_OF_WEEK))
	                 &&(Calendar.SUNDAY != dateS1.get(Calendar.DAY_OF_WEEK))) {

	            	Date dateCompareStart = parseDate(String.valueOf(startHour) + ":" + String.valueOf(startMinute));
	      			Date dateDefaultEnd = parseDate(String.valueOf(hourE) + ":" + String.valueOf(minE));
	      			
	      			Date dateCompareEnd = parseDate(String.valueOf(endHour) + ":" + String.valueOf(endMinute));
	        		Date dateDefaultStart = parseDate(String.valueOf(hourS) + ":" + String.valueOf(minS));
	      			
	      			//milliseconds Start
	    			long differenceStart = dateDefaultEnd.getTime() - dateCompareStart.getTime();
	    			long secondsInMilliStart = 1000;
	    			long minutesInMilliStart = secondsInMilliStart * 60;
	    			long hoursInMilliStart = minutesInMilliStart * 60;
	    			
	    			long elapsedHoursStart = differenceStart / hoursInMilliStart;
	    			if(beforeStart){
	    				elapsedHoursStartBefore = elapsedHoursStart;
	    				startHourBefore = startHour;
	    			}
	    			
	    			//milliseconds End
	    			long differenceEnd = dateCompareEnd.getTime() - dateDefaultStart.getTime();
	    			long secondsInMilliEnd = 1000;
	    			long minutesInMilliEnd = secondsInMilliEnd * 60;
	    			long hoursInMilliEnd = minutesInMilliEnd * 60;
	    			
	    			long elapsedHoursEnd = differenceEnd / hoursInMilliEnd;
	    			if(beforeEnd){
	    				elapsedHoursEndBefore = elapsedHoursEnd;
	    				endHourBefore = endHour;
	    			}
	    			
	    		// Start
	    			if(beforeStart){
	      				if(elapsedHoursStart <= 0){
	      					
	      				}else if(elapsedHoursStart <= 4 ){
	    							numberOfDays = numberOfDays + 0.5;
	    							numberOfDaysText.setText(String.valueOf(numberOfDays));
	    							dateS1.add(Calendar.DATE,1);
	    						}else{
	    							numberOfDays = numberOfDays + 1;
	    							numberOfDaysText.setText(String.valueOf(numberOfDays));
	    							dateS1.add(Calendar.DATE,1);
	    						}
	      				beforeStart = false;
	    			}else{
	    				numberOfDays++;
	    				dateS1.add(Calendar.DATE,1);
	    			}

	    				elapsedHoursStartBefore = elapsedHoursStart;
	    				startHourBefore = startHour;
	    				getNumberOfDays = numberOfDays;
	    				numberOfDaysText.setText(String.valueOf(getNumberOfDays));
	    				
	    			// End
	    				if(beforeEnd){
		    				
		    				if(elapsedHoursEnd <= 0){
		    					
		    				}else if(elapsedHoursEnd <= 4 ){
		    						numberOfDays = numberOfDays + 0.5;
	    							numberOfDaysText.setText(String.valueOf(numberOfDays));
	    							dateS1.add(Calendar.DATE,1);
	    						}else{
	    							numberOfDays = numberOfDays + 1;
	    							numberOfDaysText.setText(String.valueOf(numberOfDays));
	    							dateS1.add(Calendar.DATE,1);
	    						}
		    				beforeEnd = false;
		    			}else{
		    				//numberOfDays++;
		    				//dateS1.add(Calendar.DATE,1);
		    			}

			    			elapsedHoursEndBefore = elapsedHoursEnd;
			    			endHourBefore = endHour;
			    			getNumberOfDays = numberOfDays; 
			    			numberOfDaysText.setText(String.valueOf(getNumberOfDays));
			    			
			    	///////////////////////////////////////////////
	    				
		              }else {
		            	  dateS1.add(Calendar.DATE,1);
		              }
		          }
		

		}
		
	}*/
	
	private Date parseDate(String date) {

	    try {
	        return inputParser.parse(date);
	    } catch (java.text.ParseException e) {
	        return new Date(0);
	    }
	}
		

		/** AsyncTask register sick day  */
	    private class RegisterTaskSickday extends AsyncTask<String, String, Boolean>{
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	        	
	        	// extract data
	 
	        	  String myStartDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay)).append(" ")
	        			  .append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
		        
	        	  String myEndDateTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay)).append(" ")
	        			  .append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();
	        	
	        	  String hoursText = null;
	        	  	try{
	        	  		String days = numberOfDaysText.getText().toString();
	        		  int hours = (int) (Double.valueOf(days) * 8);
	            	  double d =  Double.valueOf(days);
	            	  hoursText = String.valueOf(hours);
	        	  	}catch (Exception e) {
	                    e.printStackTrace();	
	                }

	        	  TextView DescriptionView = (TextView) getActivity().findViewById(R.id.textDescription);
			      String description = DescriptionView.getText().toString();	
				 
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				  int sickdayID;
				  boolean suc = false;

					   sickdayID = MyProvider.create_sickday(myStartDateTime, myEndDateTime, hoursText, description);
					   if(sickdayID != 0){suc = true;}

	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
	        	// updating UI from Background Thread
	        	
	        	getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (success == false)
		            	{
		            		 /*error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(getActivity(), getString(R.string.text_error), error_message);*/
							Toast.makeText(getActivity(), "Request sickday could not be sent", Toast.LENGTH_SHORT).show();
			    		}
						else
						{
							Toast.makeText(getActivity(), "Request sickday sent", Toast.LENGTH_SHORT).show();
							
							ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
							viewPager.setCurrentItem(0); 
				            
						}
					}
				});
     
		    }
	    }

	 
}
