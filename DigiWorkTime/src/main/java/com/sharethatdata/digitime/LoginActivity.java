package com.sharethatdata.digitime;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity;
import com.sharethatdata.help.form.HelpActivity;
import com.sharethatdata.registration.form.RegistrationActivity;
import com.sharethatdata.webservice.*;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cWebService;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends AppCompatActivity {
	
    MyGlobals Globals;
	
    WSDataProvider MyProvider = null;
    WSDataProvider MyProvider2 = null;

	TimeUtils MyTimeUtils = null;
    
    JSONArray order = null;
    
    ArrayList<String> roles = null;

//	public String override_url = "";
	
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.11
	 */
//	private static final String[] DUMMY_CREDENTIALS = new String[] {
//			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
//	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private DeviceRegisterDeviceTask mRegDevTask = null; // for save settings
	private UserLoginTask mAuthTask = null;
	private UserRolesTask mRolesTask = null;
	
	// Values for email and password at the time of the login attempt.
	private String mApikey; // // for save settings
	private String mEmail;
	private String mPassword;

	private String myUser = "";
	private String myPass = "";

	private String webservice_url = "";

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	TextInputLayout emailInputLayout;
	TextInputLayout passInputLayout;

	private PendingIntent pendingIntent;

	SharedPreferences prefs;

	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_new);

		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(LoginActivity.this, LoginActivity.this);

		// Set up the login form.
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		
		String current_user = sharedPrefs.getString("prefUsername", "");
		String current_password = sharedPrefs.getString("prefUserPassword", "");
		boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
	
		// get webservice url
		webservice_url = sharedPrefs.getString("prefWebServiceServer", "");
		
		myUser = current_user;
		myPass = current_password;
		
		boolean suc = InitApp();
		
		if (!suc) Toast.makeText(LoginActivity.this, "Something went wrong with initializing the app!", Toast.LENGTH_SHORT).show();
		
		// Set up the login form.
		mEmail = current_user;
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPassword = current_password;
		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setText(mPassword);

		emailInputLayout = (TextInputLayout) findViewById(R.id.email_text_input_layout);
		passInputLayout = (TextInputLayout) findViewById(R.id.password_text_input_layout);

		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						if(isNetworkAvailable() == true){
							boolean cancelEmail = false;
							boolean cancelPass = false;
							View focusViewEmail = null;
							View focusViewPass = null;

							boolean emailLenght = shouldShowErrorEmail();
							boolean passLenght = shouldShowErrorPass();

							if (emailLenght) {
								showErrorEmail();
								focusViewEmail = mEmailView;
								cancelEmail = true;
							}

							if (passLenght) {
								showErrorPass();
								focusViewPass = mPasswordView;
								cancelPass = true;
							}

							if(cancelEmail){
								focusViewEmail.requestFocus();
							}else{
								hideErrorEmail();
							}

							if(cancelPass){
								focusViewPass.requestFocus();
							}else{
								hideErrorPass();
							}

							if(!cancelEmail && !cancelPass){
								hideKeyboard();

								//attemptLogin();

								// first attempt to login - check for device registration - save settings
								if (!prefs.getBoolean("apikey_saved", false)){
									// <---- run one time code here
									if(myUser.equals("")) myUser = mEmailView.getText().toString();
									if(myPass.equals("")) myPass = mPasswordView.getText().toString();
										new checkUsername(myUser, myPass).execute();
									// check username - then register device
								}else{
									if(prefs.getString("ws_url", "").equals("")){
										// the list of web services NOT taken yet -> the device is NOT yet registered by the administrator
										new LoadListURL().execute();
									}else {
										// the default url was saved -> if the device was activated by the administrator
										attemptLogin();
									}
								}

							}
                        }else {
                            Toast.makeText(getApplicationContext(), "No internet connnection available", Toast.LENGTH_LONG).show();
                        }
					}
				});
		
		if (blAutoLogin)
		{
            if(isNetworkAvailable() == true){
                attemptLogin();
            }else {
                Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
            }
		}
		

		WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifiManager.getConnectionInfo();
		String address = info.getMacAddress();
		    
		    System.out.println("Adresse MAC enabled: " + address);

	}

	private boolean shouldShowErrorEmail() {
		int emailLength = mEmailView.getText().length();
		return emailLength <= 0 ;
	}

	private boolean shouldShowErrorPass() {
		int passLength = mPasswordView.getText().length();
		return passLength <= 0;
	}

	private void showErrorEmail() {
		emailInputLayout.setErrorEnabled(true);
		emailInputLayout.setError(getString(R.string.text_username_required));
	}

	private void showErrorPass() {
		passInputLayout.setErrorEnabled(true);
		passInputLayout.setError(getString(R.string.text_password_required));
	}

	private void hideErrorEmail() {
		emailInputLayout.setErrorEnabled(false);
		emailInputLayout.setError("");
	}

	private void hideErrorPass() {
		passInputLayout.setErrorEnabled(false);
		passInputLayout.setError("");
	}

	private void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
					hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
	
	private boolean InitApp()
	{
		Globals = ((MyGlobals)getApplication());
	
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", false);
		String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
		String endDay = sharedPrefs.getString("prefNotificationEndDay", "");

		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider2 = new WSDataProvider(myUser, myPass);
				
		if (webservice_url != "")
		{
			// store in globals
			Globals.setValue("ws_url", webservice_url);
			
			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider2.SetWebServiceUrl(webservice_url);
		}
	
		try
		{
			// read device values
			WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInf = wifiMan.getConnectionInfo();
			int ipAddress = wifiInf.getIpAddress();
			String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
			Globals.setValue("ip", ip);
			
			String android_id = Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID); 
			//Globals.setValue("device", android_id);
			Globals.setValue("device", myUser);

			String manufacturer = Build.MANUFACTURER;
			String model = Build.MODEL;
			String device_name = manufacturer + " " + model;
			Globals.setValue("device_name", device_name);
			
			String appname = (String) getApplicationInfo().loadLabel(getBaseContext().getPackageManager());
			Globals.setValue("module", appname);
					
			MyProvider.SetDeviceIdentification(ip, android_id, appname);
			MyProvider2.SetDeviceIdentification(ip, android_id, appname);
			
			// add here also mac address
		
			return  true;
		} catch (Exception e) {
            e.printStackTrace();
            return false;
        }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        case R.id.action_help:
            Intent intent_help = new Intent(this, HelpActivity.class); startActivity(intent_help); 
            return true;
        case R.id.action_create_user:
        	Intent intent_registration = new Intent(this, RegistrationActivity.class); startActivity(intent_registration); 
        	return true;
		case R.id.action_forgot_password:
			Intent intent_reset = new Intent(this, ResetPasswordActivity.class); startActivity(intent_reset);
			return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	
	
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		//mEmailView.setError(null);
		//mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			//mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			//mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			//mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	public void attemptRegisterDevice() {
		if (mRegDevTask != null) {
			return;
		}

		// Store values at the time of the login attempt.
		mApikey = "SHRb300e589e31f41702344"; // robopharma apikey

		// Show a progress spinner, and kick off a background task to
		// perform the user login attempt.
		//mLoginStatusMessageView.setText(R.string.login_progress_registration);
		//showProgress(true);
		mRegDevTask = new DeviceRegisterDeviceTask(mApikey);
		mRegDevTask.execute((Void) null);

	}
	
	protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			//showProgressDialog(getString(R.string.loading_alert_dialog));
			showProgressDialog("Loading: check login credentials");
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			
			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl(webservice_url);
			boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "DigiTime");
			if (suc)
			{				
				// store current user in globals
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("user", mEmail);
			    Globals.setValue("pass", mPassword);
			    
		//	    MyProvider = new IntraDataProvider(mEmail);
			    MyProvider.LogEvent(getString(R.string.text6_log_event));
			    // "user logged in."
			    
			    String ws_url;
			    if (webservice_url != ""){
			    	ws_url = webservice_url;
				}else{
					//ws_url = "http://dev-ws.sharethatdata.com";
					ws_url = "https://newtonrp-ws.sharethatdata.com";
				}
			    
			    // save user, pass and url for service
			    SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
		   	     editor.putString("USER", String.valueOf(mEmail));
		   	     editor.putString("PASS", String.valueOf(mPassword));
		   	     editor.putString("URL", String.valueOf(ws_url));
		   	     editor.commit();
		   	     
		   	     Log.d("Save for service ", " mEmail: " + mEmail + " ; mPassword: " + mPassword + " ; ws_url: " + ws_url);
			 
			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
						   
				// register roles
				mRolesTask = new UserRolesTask();
				mRolesTask.execute((Void) null);



					// call service to check new message and new news, notifications for resources, visit, etc
					MyProvider = new WSDataProvider(LoginActivity.this);
					MyProvider.startServicefromNonActivity();


				
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}

			hideProgressDialog();
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	/**
	 * Represents an asynchronous task used to check the roles of
	 * the user.
	 */
	public class UserRolesTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			//showProgressDialog(getString(R.string.loading_alert_dialog));
			showProgressDialog("Loading: get roles");
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			
		    MyProvider2 = new WSDataProvider(mEmail, mPassword);
		    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
			roles = MyProvider2.getUserRoles(mEmail);
			
			if (roles != null)
			{
				MyGlobals Globals = ((MyGlobals)getApplication());
				
				if (roles.contains("Administrator"))
				{				
				    Globals.setValue("administrator", "yes");
				} else {
				    Globals.setValue("administrator", "no");
				}
				if (roles.contains("Developer"))
				{				
				    Globals.setValue("developer", "yes");
				} else {
				    Globals.setValue("developer", "no");
				}
				if (roles.contains("Manager"))
				{				
				    Globals.setValue("manager", "yes");
				} else {
				    Globals.setValue("manager", "no");
				}
				if (roles.contains("Employee"))
				{				
				    Globals.setValue("employee", "yes");
				} else {
				    Globals.setValue("employee", "no");
				}
				if (roles.contains("Publisher"))
				{
				    Globals.setValue("publisher", "yes");
				} else {
				    Globals.setValue("publisher", "no");
				}
			}
			
			// get id and name for user logged
			
			List<cContact> List = MyProvider2.getUserIdbyUsername(mEmail);
	    	
    		for(cContact entry : List)
         	{

					String id = entry.id;
					String name = entry.name;
					Globals.setValue("idContact", id); // id of user logged
					Globals.setValue("nameContact", name); // name of user logged
					
					SharedPreferences.Editor pref = getSharedPreferences("PREF_CONTACT", MODE_PRIVATE).edit();
                	pref.putString("idContact", id); // id user logged
                	pref.putString("nameContact", name); // name of user logged
                	pref.commit();

             } 
			
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			
			runOnUiThread(new Runnable() {
			      @Override
			          public void run() {
			    	  

			    	  	if (roles.size() == 0)
						{
							String data_error = MyProvider2.last_error;
							Toast.makeText(getApplication(), data_error, Toast.LENGTH_LONG).show();
						} 
		
			    	  	showProgress(false);

			      }
			});
					

			if (success) {
						   
			   
			} else {
				
			}
			
			// go to next activity, after you save roles, id and name for user logged
			
			finish();
			
			String id_project = null;
			String project_progress = null;
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
			    String projectID = extras.getString("ID");  // get id project from MyService
			    	id_project = projectID; 
			    String projectProgress = extras.getString("project_progress"); // get data from MyServiceProgress
			    	project_progress = projectProgress;
			    	
			}
			if(id_project != null){
				Intent intent = new Intent(LoginActivity.this, MyProjectsDetailActivity.class); 
				intent.putExtra("ID", id_project);
			 	startActivity(intent); 
			}else if(project_progress != null){
				Intent intent = new Intent(LoginActivity.this, ProgressActivity.class); 
			 	startActivity(intent);
			}else{
				Intent intent = new Intent(LoginActivity.this, RecordTimeActivity.class); 
			 	startActivity(intent); 
			}
		    
			hideProgressDialog();
		 	
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	public class checkUsername extends AsyncTask<Void, Void, Boolean>{

		String username;
		String password;

		public checkUsername(String username, String password){
			this.username = username;
			this.password = password;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			//showProgressDialog(getString(R.string.loading_alert_dialog));
			showProgressDialog("Loading: check user");
		}

		@Override
		protected Boolean doInBackground(Void... voids) {

			MyProvider = new WSDataProvider(username, password);

			String myDeviceID = username; // instead of device id (which can be changed by system), I will use username as unique device registration

			// check username to exist - will represent unique id, instead of device id
			boolean existUsername = MyProvider.existUsername(myDeviceID);

			return existUsername;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			if(success){
				attemptRegisterDevice();
			}else{
				Toast.makeText(getApplicationContext(), getString(R.string.toast_text_check_username) ,Toast.LENGTH_SHORT);
			}

			hideProgressDialog();
		}
	}


	/**
	 * Represents an asynchronous registration task used to register device
	 * of the user.
	 */
	public class DeviceRegisterDeviceTask extends AsyncTask<Void, Void, Boolean> {

		String apikey;

		public DeviceRegisterDeviceTask(String apikey){
			this.apikey = apikey;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			//showProgressDialog(getString(R.string.loading_alert_dialog));
			showProgressDialog("Loading: register device");
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

			//String myDeviceID = Globals.getValue("device");
			String myDeviceID = myUser; // instead of device id (which can be changed by system), I will use username as unique device registration
			String myDeviceName = Globals.getValue("device_name");

			boolean suc = MyProvider.registerDeviceCerberus(apikey, myDeviceID, myDeviceName);
			if (suc)
			{
				MyProvider.LogEvent(getString(R.string.text6_log_event));
				// "user logged in."
				SharedPreferences.Editor editor1 = prefs.edit();
				editor1.putBoolean("apikey_saved", true);
				editor1.commit();

				// save apikey
				// mark first time has runned.
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("apikey", apikey);
				editor.commit();

			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mRegDevTask = null;
			showProgress(false);

			if (success) {
				new LoadListURL().execute(); // get list urls
			}else{
				// 20003 = Device is already registered.
				String data_error = MyProvider.last_error;
				if(data_error.equals("Device is already registered.")){

					SharedPreferences.Editor editor1 = prefs.edit();
					editor1.putBoolean("apikey_saved", true);
					editor1.commit();

					// save apikey
					// mark first time has runned.
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("apikey", apikey);
					editor.commit();

					new LoadListURL().execute(); // get list urls
				}

				Toast.makeText(getApplicationContext(), getString(R.string.toast_text_register_device_failed), Toast.LENGTH_SHORT);
			}

			hideProgressDialog();
		}

		@Override
		protected void onCancelled() {
			mRegDevTask = null;
			showProgress(false);
		}
	}

	/**
	 * Background Async Task to Load my sent request by making HTTP Request
	 * */
	class LoadListURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//showProgressDialog(getString(R.string.loading_alert_dialog));
			showProgressDialog("Loading: get link webservice");
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

			//String myDeviceID = Globals.getValue("device");
			String myDeviceID = myUser;
			String myDeviceName = Globals.getValue("device_name");

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
			String apikey = prefs.getString("apikey","");
			//if(apikey.equals("") || apikey == "") apikey = "SHRb300e589e31f41702344"; // robopharma apikey

			List<cWebService> myService = MyProvider.getAllWebServices(apikey, myDeviceID, "1");
			for(cWebService entry : myService)
			{
				if(entry.default_url.equals("1")){
					Globals.setValue("ws_url_id", entry.id);
					Globals.setValue("ws_url", entry.url);
					Globals.setValue("access_key", entry.access_key);

					// get list of web services one time and get,set the default url -> if the device was activated by the administrator
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("ws_url_id", entry.id);
					editor.putString("ws_url", entry.url);
					editor.putString("access_key", entry.access_key);
					editor.commit();

					break;
				}else{
					// it will crash if I receive list urls with all default = 0
					// which web service should I set in this situation????

					Globals.setValue("ws_url_id", entry.id);
					Globals.setValue("ws_url", entry.url);
					Globals.setValue("access_key", entry.access_key);

					// get list of web services one time and set the default url -> if the device was activated by the administrator
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("ws_url_id", entry.id);
					editor.putString("ws_url", entry.url);
					editor.putString("access_key", entry.access_key);
					editor.commit();

					break;
				}
					/*HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name",  entry.name);
					map.put("type",  entry.type);
					map.put("url",  entry.url);
					map.put("access_key",  entry.access_key);*/
			}

			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					//pDialog.dismiss();
					attemptLogin();
				}
			});

			hideProgressDialog();
		}
	}


	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	public void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}

		//progressDialog = null;
	}


	/*public void startServicefromNonActivityForAndroidOreo(){

		mServiceResultReceiver = new ServiceResultReceiver(new Handler());
		mServiceResultReceiver.setReceiver(this);
		showDataFromBackground(LoginActivity.this, mServiceResultReceiver);

	}

	private void showDataFromBackground(LoginActivity mainActivity, ServiceResultReceiver mResultReceiver) {
		JobService.enqueueWork(mainActivity, mResultReceiver);
	}

	public void showData(String data) {
		TextView mTextView = new TextView(this);
		mTextView.setText(String.format("%s\n%s", mTextView.getText(), data));

        Toast.makeText(LoginActivity.this, mTextView.getText().toString(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		switch (resultCode) {
			case SHOW_RESULT:
				if (resultData != null) {
					showData(resultData.getString("data"));
				}
				break;
		}
	}*/

}
