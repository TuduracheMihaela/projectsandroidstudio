package com.sharethatdata.digitime;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import com.sharethatdata.digitime.RecordTimeActivity.LoadList;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cCustomer;
import com.sharethatdata.webservice.datamodel.cTaskProject;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cTask;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Contacts.People;
import android.sax.EndTextElementListener;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class StopWatchActivity extends Activity {
	
	static final String STATE_DESCRIPTION = "description";
	SharedPreferences prefs;
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	ProgressDialog progressDialog;
	
	private String list_name = "";
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	private String myReportUser = "";

	private int indexProject = 0;
	private String projectID = "";
	private String project_costplace = "";
	
	private boolean pause_button = false;
	private boolean stop_button = false;
	private boolean input_correct = false;
	private boolean no_network = false;
	private String error_message = "";

	ArrayList<HashMap<String, String>> customerList;
	ArrayList<HashMap<String, String>> taskList;
	ArrayList<HashMap<String, String>> projectList;
	ArrayList<HashMap<String, String>> locationList;
	ArrayList<HashMap<String, String>> activityList;
	ArrayList<HashMap<String, String>> costplacesList;

	List<String> customerkeyArray =  new ArrayList<String>();
	List<String> taskkeyArray =  new ArrayList<String>();
	List<String> projectkeyArray =  new ArrayList<String>();
    List<String> locationkeyArray =  new ArrayList<String>();
    List<String> activitykeyArray =  new ArrayList<String>();
    List<String> costplacekeyArray =  new ArrayList<String>();
    
    private int mStartHour,mStartMinute;
	private int mEndHour,mEndMinute;
	private int mStartYear,mStartMonth,mStartDay;
	
		private TextView timeStartText;
		private TextView timeEndText;
		private TextView dateText;
		private EditText descriptionText;
		
		
	// TIMER

		private static String TAG = "StopWatchActivity";

		private TextView textTimer;
		private Button startButton;
		private Button pauseButton;
		private Button resumeButton;
		private Button stopButton;
		private Button resetButton;
		// Timer to update the elapsedTime display
		private final long mFrequency = 100;    // milliseconds
		private final int TICK_WHAT = 2;
		
		
	// Reminder
		
		 private PendingIntent pendingIntent;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stop_watch);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
						
		Globals = ((MyGlobals) getApplicationContext());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged

		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(StopWatchActivity.this, StopWatchActivity.this);
		
		timeStartText = (TextView) findViewById(R.id.timeStartText);
		timeEndText = (TextView) findViewById(R.id.timeEndText);
		dateText = (TextView) findViewById(R.id.dateStartText);
		descriptionText = (EditText) findViewById(R.id.textDescription);
		
		Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    mEndHour = c.get(Calendar.HOUR_OF_DAY);
	    mEndMinute = c.get(Calendar.MINUTE);

	    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	    
	    // set Date
	    dateText.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
            .append(pad(mStartMonth + 1)).append("-")
            .append(pad(mStartYear)));
		
		// TIMER

		startService(new Intent(this, StopWatchService.class));
		bindStopWatchService();

		textTimer = (TextView) findViewById(R.id.textTimer);

		startButton = (Button) findViewById(R.id.btnStart);
		pauseButton = (Button) findViewById(R.id.btnPause);
		resumeButton = (Button ) findViewById(R.id.btnResume);
		stopButton = (Button) findViewById(R.id.btnStop);
		resetButton = (Button) findViewById(R.id.btnReset);
		mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
			
		// load spinner data
		new LoadList().execute();
		
		Calendar calendar = Calendar.getInstance();
	     
	      calendar.get(Calendar.MONTH);
	      calendar.get(Calendar.YEAR);
	      calendar.get(Calendar.DAY_OF_MONTH);
	 
	      calendar.set(Calendar.HOUR_OF_DAY, 16);
	      calendar.set(Calendar.MINUTE, 59);
	      calendar.set(Calendar.SECOND, 0);
	      calendar.set(Calendar.AM_PM,Calendar.PM);
	     
	      //Intent myIntent = new Intent(StopWatchActivity.this, MyReceiverToday.class);
	      //pendingIntent = PendingIntent.getBroadcast(StopWatchActivity.this, 0, myIntent,0);
	     
	      //AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
	      //alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
	      //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 86400000 , pendingIntent);

	} 
	
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }
    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        
     // Restore preferences
        SharedPreferences settings = getSharedPreferences(STATE_DESCRIPTION, 0);
        String getTextForDescription = settings.getString("desc", "");

	    descriptionText.setText(getTextForDescription);
	    
	    System.out.println("Text description in resume: " + getTextForDescription);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
        
        String getTextForDescription = descriptionText.getText().toString();
         // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
         prefs = getSharedPreferences(STATE_DESCRIPTION, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("desc", getTextForDescription);

        // Commit the edits!
        editor.commit();
        
	    System.out.println("Text description in on pause: " + getTextForDescription);
	    
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        
        bindStopWatchService();
        
        String getTextForDescription = "";
        
        // Save preferences
        prefs = getSharedPreferences(STATE_DESCRIPTION, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("desc", getTextForDescription);

        // Commit the edits!
        editor.commit();
    }

	
////////////////////////////////////////////////////////////////////////////

// TIMER

	private Handler mHandler = new Handler() {
		public void handleMessage(Message m) {
				updateElapsedTime();
				sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
		}
	};
	
	// Connection to the background StopWatchService
		private StopWatchService m_stopwatchService;
		private ServiceConnection m_stopwatchServiceConn = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				Log.d(TAG, "Service connected: " + name);
				
				m_stopwatchService = ((StopWatchService.LocalBinder)service).getService();
				showCorrectButtons();
			}

			@Override
			public void onServiceDisconnected(ComponentName name) {
				Log.d(TAG, "Service disconnected: " + name);
				
				m_stopwatchService = null;
			}
		};

		private void bindStopWatchService() {
			bindService(new Intent(this, StopWatchService.class),
					m_stopwatchServiceConn, Context.BIND_AUTO_CREATE);
		}

		private void unbindStopWatchService() {
			if ( m_stopwatchService != null ) {
				unbindService(m_stopwatchServiceConn);
			}
		}

		private void showCorrectButtons() {
			Log.d(TAG, "showCorrectButtons");

			if ( m_stopwatchService != null ) {
				if ( m_stopwatchService.isStopWatchRunning() ) {
					showPauseButtons();
				} else {
						showStartButtons();
				}
			}
		}
		
		
		// onPause clicked 
		private void showPauseButtons() {
			Log.d(TAG, "showPauseButtons");

			startButton.setVisibility(View.GONE);
			stopButton.setVisibility(View.GONE);
			resetButton.setVisibility(View.GONE);
			pauseButton.setVisibility(View.VISIBLE);
			resumeButton.setVisibility(View.GONE);
			
			pause_button = true;
		}
		
		// onResume clicked 
		private void showResumeButtons() {
			Log.d(TAG, "showResumeButtons");

			stopButton.setVisibility(View.VISIBLE);
			resetButton.setVisibility(View.VISIBLE);
			resumeButton.setVisibility(View.VISIBLE);
			startButton.setVisibility(View.GONE);
			pauseButton.setVisibility(View.GONE);
		}

		// onStart clicked 
		private void showStartButtons() {
			Log.d(TAG, "showStartResetButtons");

			startButton.setVisibility(View.VISIBLE);
			stopButton.setVisibility(View.GONE);
			resetButton.setVisibility(View.VISIBLE);
			pauseButton.setVisibility(View.GONE);
			resumeButton.setVisibility(View.GONE);

		}

		// onStop clicked 
		private void showStopButtons() {
			Log.d(TAG, "showStartResetButtons");

			startButton.setVisibility(View.GONE);
			stopButton.setVisibility(View.GONE);
			resetButton.setVisibility(View.VISIBLE);
			pauseButton.setVisibility(View.GONE);
			resumeButton.setVisibility(View.GONE);
			
			pause_button = true;
			stop_button = true;
		}

		// onStart clicked disable stopButton, pauseButton
		private void showResetButtons() {
			Log.d(TAG, "showResetButtons");

			startButton.setVisibility(View.VISIBLE);
			stopButton.setVisibility(View.GONE);
			resetButton.setVisibility(View.VISIBLE);
			pauseButton.setVisibility(View.GONE);
			resumeButton.setVisibility(View.GONE);
		}

		// Start
		public void onStartClicked(View v) {
			Log.d(TAG, "start button clicked");
			m_stopwatchService.start();

			showPauseButtons();
			
//			String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
//			timeStartText.setText(currentDateTimeString);
			
			Date curDate = new Date();
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			
			String DateToStr = format.format(curDate);
			
			timeStartText.setText(DateToStr);
			
			// Tell the user we started.
			Toast.makeText(StopWatchActivity.this, getText(R.string.notification_toast_started), Toast.LENGTH_SHORT).show();
		}
		
		// Resume
		public void onResumeClicked(View v){
			Log.d(TAG, "resume button clicked");
			m_stopwatchService.start();
			
			showPauseButtons();
		}

		// Pause
		public void onPauseClicked(View v) {
			Log.d(TAG, "pause button clicked");
			m_stopwatchService.pause();

			showResumeButtons();
			
//			String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
//			timeEndText.setText(currentDateTimeString);
			
			Date curDate = new Date();
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			
			String DateToStr = format.format(curDate);
			
			timeEndText.setText(DateToStr);

			// Tell the user we started.
			Toast.makeText(StopWatchActivity.this, getText(R.string.notification_toast_paused), Toast.LENGTH_SHORT).show();
		}

		// Stop
		public void onStopClicked(View v) {
			Log.d(TAG, "stop button clicked");
			m_stopwatchService.pause();

			showStopButtons();

			// Tell the user we started.
			Toast.makeText(StopWatchActivity.this, getText(R.string.notification_toast_stopped), Toast.LENGTH_SHORT).show();
		}

		public void onResetClicked(View v) {
			Log.d(TAG, "reset button clicked");
			m_stopwatchService.reset();

			showResetButtons();
		}

		public void updateElapsedTime() {
			if ( m_stopwatchService != null )
				textTimer.setText(m_stopwatchService.getFormattedElapsedTime());
		}
		
		private String formatDigits(long num) {
	        return (num < 10) ? "0" + num : new Long(num).toString();
	    }

		
		private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
		            mStartYear = year;
		            mStartMonth = month;
		            mStartDay = day; 
		            		
		            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
		    		        
		            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));
		    }
		};
		
		private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	    		        
	            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	        }
	    };
	    
	    private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mEndHour = hourOfDay;
	            mEndMinute = minute;
	            
	            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	    		        
	            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
	                    .append(pad(mEndMinute)));
	        }
	    };
	    
		public void startTimeClick(View v) 
		{
			CustomTimePickerDialog stp1 = new CustomTimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
			stp1.show();
		}
		
		public void endTimeClick(View v) 
		{
			CustomTimePickerDialog stp2 = new CustomTimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
			stp2.show();
		}
		
		public void startDateClick(View v) 
		{
			DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
			Date d = new Date();
			dp1.setTitle(sdf.format(d));
		    dp1.show();
		}
		
		
		/**
		 * Background Async Task to Load all spinner data by making HTTP Request
	  	 * */
		class LoadList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
			 showProgressDialog(getString(R.string.loading_alert_dialog) + list_name);
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {

				customerList = new ArrayList<HashMap<String, String>>();
				projectList = new ArrayList<HashMap<String, String>>();
				locationList = new ArrayList<HashMap<String, String>>();
				activityList = new ArrayList<HashMap<String, String>>();
				costplacesList = new ArrayList<HashMap<String, String>>();
				taskList = new ArrayList<HashMap<String, String>>();

				MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
				myReportUser = report_user;
				if(myReportUser.equals("")){
					report_user = myUserID;
					myReportUser = myUserID;
				}

				// load data from provider
				List<cCustomer> custList = MyProvider.getCustomerSpinner(report_user);
				for(cCustomer entry : custList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					customerList.add(map);
				}

				// load data from provider
				List<cProject> projList = MyProvider.getProjectsSpinner(report_user);
				for(cProject entry : projList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);
					map.put("costplace", entry.costplace);

					// adding HashList to ArrayList
					projectList.add(map);
				}

				List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
				for(cUserLocation entry : locList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					locationList.add(map);
				}

				List<cActivity> actList = MyProvider.getUserActivity(report_user);
				for(cActivity entry : actList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name", entry.name);

					// adding HashList to ArrayList
					activityList.add(map);
				}

				List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
				for(cCostplace entry : costList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					costplacesList.add(map);
				}

				List<cTask> newtaskList = null;
				if(!report_user.equals(""))
					newtaskList = MyProvider.getProjectTasks(report_user);
				else
					newtaskList = MyProvider.getProjectTasks(myUserID);


				for(cTask entry : newtaskList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					taskList.add(map);
				}
		   		
		    	//if (itemList.isEmpty())
		    	if (MyProvider.json_connected == false)
		    	{
		        	no_network = true;
		         	error_message = getString(R.string.error_network);
		         	
		    	}	
		    	return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String file_url) {
		        // dismiss the dialog after getting all products
		        hideProgressDialog();
		        // updating UI from Background Thread
		        runOnUiThread(new Runnable() {
		            public void run() {
		            	
		            	/*if (no_network == true)
		            	{
		            	    UIHelper myUIHelper = new UIHelper();
		    			    
		    			    myUIHelper.ShowDialog(StopWatchActivity.this,getString(R.string.text_error), error_message);
		    			    //  "Error"
		    			 	            		   	 
		            	} else {*/
			            
			         	   /**
			                 * Updating parsed JSON data into ListView
			                 * */
			            	
			            	// store the id of the item in a separate array

							List<String> locationArray =  new ArrayList<String>();
							List<String> projectArray =  new ArrayList<String>();
							List<String> customerArray =  new ArrayList<String>();
							List<String> activityArray =  new ArrayList<String>();
							List<String> costplaceArray =  new ArrayList<String>();
							List<String> taskArray =  new ArrayList<String>();
							final List<String> projectCostplaceArray =  new ArrayList<String>();

							projectkeyArray.clear();

							locationArray.add(0, "Select location");
							locationkeyArray.add(0, "");

							projectArray.add(0, "Select a project");
							projectkeyArray.add(0, "");

							activityArray.add(0, "Select activity");
							activitykeyArray.add(0, "");

							costplaceArray.add(0, "Select costplace");
							costplacekeyArray.add(0, "");

							projectCostplaceArray.add(0, "");

							taskArray.add(0, "No task");
							taskkeyArray.add(0, "");


						// load spinner data from dataprovider
							for (HashMap<String, String> map : projectList)
								for (Entry<String, String> entry : map.entrySet())
								{
									if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
									if (entry.getKey() == "name") projectArray.add(entry.getValue());
									if (entry.getKey() == "costplace") projectCostplaceArray.add(entry.getValue());
								}
			           	    for (HashMap<String, String> map : locationList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") locationArray.add(entry.getValue());
			           	        }
			           	  	for (HashMap<String, String> map : activityList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") activityArray.add(entry.getValue());
			           	        }
			           		for (HashMap<String, String> map : costplacesList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
			           	        }
			           		for (HashMap<String, String> map : taskList)
			           	        for (Entry<String, String> entry : map.entrySet())
			           	        {
			           	        	if (entry.getKey() == "id") taskkeyArray.add(entry.getValue());
			           	        	if (entry.getKey() == "name") taskArray.add(entry.getValue());
			           	        }

						for (HashMap<String, String> map : customerList)
							for (Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "id") customerkeyArray.add(entry.getValue());
								if (entry.getKey() == "name") customerArray.add(entry.getValue());
							}


							ArrayAdapter<String> loc_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, locationArray);
		           	    	loc_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		          	       	Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
		           	       	sLocations.setAdapter(loc_adapter);

		           	    	ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, projectArray);
		           	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		           	       	Spinner sProjects = (Spinner) findViewById(R.id.spinnerProject);
		           	    	sProjects.setAdapter(project_adapter);

		           	    	ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, activityArray);
		           	    	activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		           	    	Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
		           	    	sActivities.setAdapter(activity_adapter);

		           	    	ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
		           	    	costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		           	    	Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
		           	    	sCostPlaces.setAdapter(costplaces_adapter);

		           	    	ArrayAdapter<String> task_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, taskArray);
							task_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		           	    	Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
		           	    	sTasks.setAdapter(task_adapter);

							ArrayAdapter<String> customer_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, customerArray);
							customer_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							Spinner sCustomers = (Spinner) findViewById(R.id.spinnerCustomer);
							sCustomers.setAdapter(customer_adapter);


						sProjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
							public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
								// Your code here
								Spinner sProjects = (Spinner) findViewById(R.id.spinnerProject);
								String mIndex = sProjects.getSelectedItem().toString();

								if(mIndex.contains("Select a project")){


								}else{

									int myIndex = sProjects.getSelectedItemPosition();
									indexProject = myIndex;
									System.out.println("Position: " + i);
									projectID = projectkeyArray.get(myIndex);
									project_costplace = projectCostplaceArray.get(myIndex);

									//if(edit == "") {
									// load spinner data
									new LoadListIfProjectSelected().execute();
									//}
								}

							}

							public void onNothingSelected(AdapterView<?> adapterView) {
								return;
							}
						});
		            	//}
		              }
		        });
		    }
		}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
	 * */
	class LoadListIfProjectSelected extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			//projectList = new ArrayList<HashMap<String, String>>();
			String edit = Globals.getValue("edit");
			if(edit == ""){
				// I am not editing
				locationList = new ArrayList<HashMap<String, String>>();
				activityList = new ArrayList<HashMap<String, String>>();
				costplacesList = new ArrayList<HashMap<String, String>>();

				locationkeyArray.clear();
				activitykeyArray.clear();
				costplacekeyArray.clear();
			}
			taskList = new ArrayList<HashMap<String, String>>();

			taskkeyArray.clear();

			// load data from provider
/*    		List<cProject> projList = MyProvider.getProjects();
    		for(cProject entry : projList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);

                // adding HashList to ArrayList
                projectList.add(map);
             } */


			String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
			if(report_user.equals("")){
				report_user = myUserID;
			}
			edit = Globals.getValue("edit");
			if(edit == ""){
				// I am not editing

				List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
				for(cUserLocation entry : locList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					locationList.add(map);
				}

				List<cActivity> actList = MyProvider.getUserActivity(report_user);
				for(cActivity entry : actList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name", entry.name);

					// adding HashList to ArrayList
					activityList.add(map);
				}

				List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
				for(cCostplace entry : costList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					costplacesList.add(map);
				}
			}

			List<cTask> newtaskListFiltered = null;
			if(!report_user.equals(""))
				newtaskListFiltered = MyProvider.getTasksProject(projectID, report_user);
			else
				newtaskListFiltered = MyProvider.getTasksProject(projectID, myUserID);

			for(cTask entry : newtaskListFiltered)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  Long.toString(entry.id));
				map.put("name", entry.name);

				// adding HashList to ArrayList
				taskList.add(map);
			}

			//if (itemList.isEmpty())
			if (MyProvider.json_connected == false)
			{
				no_network = true;
				error_message = getString(R.string.error_network);

			}
			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			if (StopWatchActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

	            	/*if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();

	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"

	            	} else {*/

					/**
					 * Updating parsed JSON data into ListView
					 * */

					// store the id of the item in a separate array

					List<String> locationArray =  new ArrayList<String>();
					List<String> activityArray =  new ArrayList<String>();
					List<String> costplaceArray =  new ArrayList<String>();
					List<String> taskArray =  new ArrayList<String>();


					for (HashMap<String, String> map : locationList)
						for (Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
							if (entry.getKey() == "name") locationArray.add(entry.getValue());
						}

					for (HashMap<String, String> map : activityList)
						for (Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
							if (entry.getKey() == "name") activityArray.add(entry.getValue());
						}
					for (HashMap<String, String> map : costplacesList)
						for (Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
							if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
						}


					if(taskList.size() > 0){
						taskArray.add(0, "Select a task");
						taskkeyArray.add(0, "");

						for (HashMap<String, String> map : taskList)
							for (Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "id") taskkeyArray.add(entry.getValue());
								if (entry.getKey() == "name") taskArray.add(entry.getValue());
							}
						for (String i : taskkeyArray){
							System.out.println("taskKeyArray " + i);
						}
					}else{
						taskArray.add(0, "No task");
						taskkeyArray.add(0, "");
					}



					final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, activityArray);
					activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
					sActivities.setAdapter(activity_adapter);

					final ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
					costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
					sCostPlaces.setAdapter(costplaces_adapter);


					final ArrayAdapter<String> loc_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, locationArray);
					loc_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
					sLocations.setAdapter(loc_adapter);

					ArrayAdapter<String> task_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, taskArray);
					task_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
					sTasks.setAdapter(task_adapter);

					if(project_costplace != ""){
						int spinnerPositionCostPlace = costplaces_adapter.getPosition(project_costplace);
						sCostPlaces.setSelection(spinnerPositionCostPlace);
					}


						sTasks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
							public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
								// Your code here

								Spinner sTasks = (Spinner) findViewById(R.id.spinnerTask);
								String mIndex = sTasks.getSelectedItem().toString();

								if(mIndex.contains("Select a task") || mIndex.contains("No task")){

								}else{
									int myIndex = sTasks.getSelectedItemPosition();
									String task = String.valueOf(myIndex);
									String taskid = taskkeyArray.get(myIndex);

									new LoadListIfTaskSelected(taskid).execute();
								}
							}
							public void onNothingSelected(AdapterView<?> adapterView) {
								return;
							}
						});

				}
			});
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
	 * */
	class LoadListIfTaskSelected extends AsyncTask<String, String, String> {

		String taskid;
		String activity;

		public LoadListIfTaskSelected(String taskid){
			this.taskid = taskid;
		}
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			//projectList = new ArrayList<HashMap<String, String>>();
				locationList = new ArrayList<HashMap<String, String>>();
				activityList = new ArrayList<HashMap<String, String>>();
				costplacesList = new ArrayList<HashMap<String, String>>();

				locationkeyArray.clear();
				activitykeyArray.clear();
				costplacekeyArray.clear();



			String report_user = Globals.getValue("report_user"); // if manager select someone else from spinner from ListActivity
			if(report_user.equals("")){
				report_user = myUserID;
			}

				List<cUserLocation> locList = MyProvider.getUserLocation(report_user);
				for(cUserLocation entry : locList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					locationList.add(map);
				}

				List<cActivity> actList = MyProvider.getUserActivity(report_user);
				for(cActivity entry : actList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name", entry.name);

					// adding HashList to ArrayList
					activityList.add(map);
				}

				List<cCostplace> costList = MyProvider.getCostPlaces(report_user);
				for(cCostplace entry : costList)
				{
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  Long.toString(entry.id));
					map.put("name", entry.name);

					// adding HashList to ArrayList
					costplacesList.add(map);
				}

			cTaskProject taskProject = MyProvider.getTaskProject(taskid);
			activity = taskProject.activity;

			//if (itemList.isEmpty())
			if (MyProvider.json_connected == false)
			{
				no_network = true;
				error_message = getString(R.string.error_network);

			}
			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			if (StopWatchActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					// store the id of the item in a separate array

					List<String> locationArray = new ArrayList<String>();
					List<String> activityArray = new ArrayList<String>();
					List<String> costplaceArray = new ArrayList<String>();

					for (HashMap<String, String> map : locationList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
							if (entry.getKey() == "name") locationArray.add(entry.getValue());
						}

					for (HashMap<String, String> map : activityList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
							if (entry.getKey() == "name") activityArray.add(entry.getValue());
						}
					for (HashMap<String, String> map : costplacesList)
						for (Entry<String, String> entry : map.entrySet()) {
							if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
							if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
						}



					final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, activityArray);
					activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
					sActivities.setAdapter(activity_adapter);

					final ArrayAdapter<String> costplaces_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
					costplaces_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sCostPlaces = (Spinner) findViewById(R.id.spinnerCostplace);
					sCostPlaces.setAdapter(costplaces_adapter);

					final ArrayAdapter<String> loc_adapter = new ArrayAdapter<String>(StopWatchActivity.this, android.R.layout.simple_spinner_item, locationArray);
					loc_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					final Spinner sLocations = (Spinner) findViewById(R.id.spinnerLocation);
					sLocations.setAdapter(loc_adapter);

					if (project_costplace != "") {
						int spinnerPositionCostPlace = costplaces_adapter.getPosition(project_costplace);
						sCostPlaces.setSelection(spinnerPositionCostPlace);
					}

						String compareValueLocation = "In Office";
						int spinnerPositionLocation = loc_adapter.getPosition(compareValueLocation);
						sLocations.setSelection(spinnerPositionLocation);

						int spinnerPositionActivity = activity_adapter.getPosition(activity);
						sActivities.setSelection(spinnerPositionActivity);

						if (project_costplace == "") {
							String compareValueCostPlace = "Projects";
							int spinnerPositionCostPlace = costplaces_adapter.getPosition(compareValueCostPlace);
							sCostPlaces.setSelection(spinnerPositionCostPlace);
						}
				}
			});
		}
	}



	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	public void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}
		
		
		private Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
		
		
		/** AsyncTask register time record  */
	    private class RegisterTask extends AsyncTask<String, String, Boolean>{
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	 
	        	  String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        	
	        	  String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
	        	  String myEndTime = (new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();
	        	  
	        	  // extract data

	        	  TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
			      String startTime = startTimeView.getText().toString();	
	        	  
	        	  TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
			      String endTime = endTimeView.getText().toString();	
			      
			      TextView TimeRecordView = (TextView) findViewById(R.id.textTimer);
				  String timeRecord = TimeRecordView.getText().toString();
			      
				  Spinner locationSpinner = (Spinner) findViewById(R.id.spinnerLocation);
				  int myIndex = locationSpinner.getSelectedItemPosition();
				  String myLocation = locationkeyArray.get(myIndex);
				  			  			  
				  Spinner taskSpinner = (Spinner) findViewById(R.id.spinnerTask);
				  myIndex = taskSpinner.getSelectedItemPosition();
				  String myTask = taskkeyArray.get(myIndex);
				  			  			  			  
				  Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
				  myIndex = projectSpinner.getSelectedItemPosition();
				  String myProject = projectkeyArray.get(myIndex);
				  
				  Spinner activitySpinner = (Spinner) findViewById(R.id.spinnerActivity);
				  myIndex = activitySpinner.getSelectedItemPosition();
				  String myActivity = activitykeyArray.get(myIndex);
				  
				  Spinner costplaceSpinner = (Spinner) findViewById(R.id.spinnerCostplace);
				  myIndex = costplaceSpinner.getSelectedItemPosition();
				  String myCostplace = costplacekeyArray.get(myIndex);

				  Spinner customerSpinner = (Spinner) findViewById(R.id.spinnerCustomer);
				  myIndex = customerSpinner.getSelectedItemPosition();
				  String myCustomer = customerkeyArray.get(myIndex);


				  			  
				  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
			      String description = DescriptionView.getText().toString();	
				  
				  // call dataprovider
				  String emailmessage = "New Time Record " + "\r\n";
				  emailmessage = emailmessage + "Date = " + myDateTime + "\r\n";
				  emailmessage = emailmessage + "Start = " + myStartTime + "\r\n";
				  emailmessage = emailmessage + "End = " + myEndTime + "\r\n";
				  
				  emailmessage = emailmessage + "Location = " + myLocation + "\r\n";
				  emailmessage = emailmessage + "Project = " + myProject + "\r\n";
				  emailmessage = emailmessage + "Activity = " + myActivity + "\r\n";
				  
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				  boolean suc = MyProvider.createTimeRecord(myDateTime, startTime, endTime, myProject, myLocation, myTask, myActivity, myCostplace, description, timeRecord, "", myCustomer);
				  
			//	  SendEmail("mverbeek@sharethatdata.com", "New Time record. ", emailmessage );
				  
	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
		    	// updating UI from Background Thread
		        runOnUiThread(new Runnable() {
		        	
		            public void run() {
		            
		            	if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(StopWatchActivity.this, getString(R.string.text_error), error_message);
			    		}
		            	         	   	
		              }
		        });
		         
		    }
	    }
	    
	    private boolean IsInputCorrect()
		{
			// fields filled
			boolean fields_filled = true;
			
			final EditText DescriptionView = (EditText) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();	
		      
		      if(TextUtils.isEmpty(description) == fields_filled) {
		    	  DescriptionView.requestFocus();
		    	  DescriptionView.setError("Please fill description");
		    	  
		    	  DescriptionView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						DescriptionView.setError(null);
					}
				});
		    	  
		    	  return fields_filled = false;
		    	 }
		      else {
		    	  return fields_filled = true;
		    	 }
		}
	    
	    public void onSubmitButton(View v){
	    	Log.d(TAG, "submit button clicked");
	    	
	    	input_correct = IsInputCorrect();
	    	
	    	if(pause_button == true){
		    	if(stop_button == true){
		    		if (input_correct == true)
					  {
						  new RegisterTask().execute();
						  
						  Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
						  
						  Toast.makeText(getApplicationContext(), "Your work was recorded", Toast.LENGTH_SHORT).show();
					  }
		    	}
		    	else{
		    		Toast.makeText(getApplicationContext(), "Please pause your work", Toast.LENGTH_SHORT).show();
		    		pause_button = false;
		    	}
		    	
	    	}
	    	else{
	    		Toast.makeText(getApplicationContext(), "Please stop your work", Toast.LENGTH_SHORT).show();
	    		stop_button = false;
	    	}  
	    	
	    }

}
