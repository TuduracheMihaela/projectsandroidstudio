package com.sharethatdata.digitime;

import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class HolidaysRequestFragment extends Fragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;

	private int mStartHour,mStartMinute, mEndHour,mEndMinute;
	private int mStartYear,mStartMonth,mStartDay, mEndYear,mEndMonth,mEndDay;
	private int getNumberOfDays = 1;
	
	private TextView startTimeView, endTimeView, startDateView, endDateView; 
	private TextView numberOfDaysText;
	private Button btnSubmit;
	
	private String myUser = "";
	private String myPass = "";
	
	private String error_message = "";

	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.fragment_holidays_request, container, false);
	        
	        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("defaults", Context.MODE_PRIVATE );
			myUser = sharedPrefs.getString("prefUsername", "");
							
			Globals = ((MyGlobals) getActivity().getApplicationContext());
		 	Globals.backgroundProcess();
			myUser = Globals.getValue("user");
			myPass = Globals.getValue("pass");
	        
	        MyProvider = new WSDataProvider(myUser, myPass);

		 	MyTimeUtils = new TimeUtils();
		 	MyTimeUtils.setOrientation(getActivity().getApplicationContext(), getActivity());
	        
	     // set calendar
			Calendar c = Calendar.getInstance();
		    mStartYear = c.get(Calendar.YEAR);
		    mStartMonth = c.get(Calendar.MONTH);
		    mStartDay = c.get(Calendar.DAY_OF_MONTH);
		    mEndYear = c.get(Calendar.YEAR);
		    mEndMonth = c.get(Calendar.MONTH);
		    mEndDay = c.get(Calendar.DAY_OF_MONTH);
		    mStartHour = c.get(Calendar.HOUR_OF_DAY);
		    mStartMinute = c.get(Calendar.MINUTE);
		    mEndHour = c.get(Calendar.HOUR_OF_DAY);
		    mEndMinute = c.get(Calendar.MINUTE);
		    
		    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
		    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	        
	        startDateView = (TextView) rootView.findViewById(R.id.dateStartText);
	        startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("-")
	            .append(pad(mStartMonth + 1)).append("-")
	            .append(pad(mStartYear)));
	        
	        endDateView = (TextView) rootView.findViewById(R.id.dateEndText);
	        endDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mEndDay)).append("-")
	            .append(pad(mEndMonth + 1)).append("-")
	            .append(pad(mEndYear)));
		    
		    startTimeView = (TextView) rootView.findViewById(R.id.timeStartText);
	        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));

		    endTimeView = (TextView) rootView.findViewById(R.id.timeEndText);
	        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
	        
	        numberOfDaysText = (TextView) rootView.findViewById(R.id.numberOfDaysText);
	        numberOfDaysText.setText("1");

	        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);
	        
	        startDateView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showStartDatePicker();
				} }); 
	        
	        endDateView.setOnClickListener(new OnClickListener() {		
				@Override
				public void onClick(View v) {
					showEndDatePicker();
				} }); 
	        
	        startTimeView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showStartTimePicker();
				} });
			
	        endTimeView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showEndTimePicker();
				} });
	        
	        btnSubmit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					RegisterTaskHoliday holidayTask;
					holidayTask = new RegisterTaskHoliday();
					holidayTask.execute();
				} });
			
	        
	        return rootView;  
	 }
	 
	 private Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
	 
	 
	      /* *** Start Date *** */
	 private void showStartDatePicker() {
		  DatePickerFragment date = new DatePickerFragment();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("year", mStartYear);
		  args.putInt("month", mStartMonth);
		  args.putInt("day", mStartDay);
		  date.setArguments(args);
		  /**
		   * Set Call back to capture selected date
		   */
		  date.setCallBack(onStartDate);
		  date.show(getFragmentManager(), "Start Date Picker");
		 }
	 
	 		/* *** End Date *** */
	 private void showEndDatePicker() {
		  DatePickerFragment date = new DatePickerFragment();
		  /**
		   * Set Up Current Date Into dialog
		   */
		  Bundle args = new Bundle();
		  args.putInt("year", mEndYear);
		  args.putInt("month", mEndMonth);
		  args.putInt("day", mEndDay);
		  date.setArguments(args);
		  /**
		   * Set Call back to capture selected date
		   */
		  date.setCallBack(onEndDate);
		  date.show(getFragmentManager(), "End Date Picker");
		 }

	/* *** Start Time *** */
	private void showStartTimePicker() {
		TimePickerFragment time = new TimePickerFragment();
		//CustomHourPickerDialog time = new CustomHourPickerDialog();
		/**
		 * Set Up Current Date Into dialog
		 */
		Bundle args = new Bundle();
		args.putInt("hours", mStartHour);
		args.putInt("minutes", mStartMinute);
		time.setArguments(args);
		/**
		 * Set Call back to capture selected date
		 */
		time.setCallBack(onStartTime);
		time.show(getFragmentManager(), "Start Time Picker");
	}

	/* *** End Time *** */
	private void showEndTimePicker() {
		TimePickerFragment time = new TimePickerFragment();
		//CustomHourPickerDialog time = new CustomHourPickerDialog();
		/**
		 * Set Up Current Date Into dialog
		 */
		Bundle args = new Bundle();
		args.putInt("hours", mEndHour);
		args.putInt("minutes", mEndMinute);
		time.setArguments(args);
		/**
		 * Set Call back to capture selected date
		 */
		time.setCallBack(onEndTime);
		time.show(getFragmentManager(), "End Time Picker");
	}
	 
	 		/* *** Start Date *** */
	 OnDateSetListener onStartDate = new OnDateSetListener() {
		  @Override
		  public void onDateSet(DatePicker view, int year, int monthOfYear,
		    int dayOfMonth) {
			  
			  	mStartYear = year;
	            mStartMonth = monthOfYear;
	            mStartDay = dayOfMonth; 
			  
			  String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
			  
			  startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));
			  

			  		/* numberOfDaysText */
			  final Calendar date1 = Calendar.getInstance();
	          final Calendar date2 = Calendar.getInstance();

	          date1.clear();
	          date1.set(mStartYear, mStartMonth, mStartDay);
	          date2.clear();
	          date2.set(mEndYear, mEndMonth, mEndDay);
	          
	          int numberOfDays = 1;
	          while (date1.before(date2)) {
	        	  date1.add(Calendar.DATE,1);
	              if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
	                 &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
	                  numberOfDays++; 
	              }else {
	            	  date1.add(Calendar.DATE,1);
	              }
	          }
	          getNumberOfDays = numberOfDays;
	          numberOfDaysText.setText(String.valueOf(numberOfDays));
		  }
		 };

		  /* *** End Date *** */
	 OnDateSetListener onEndDate = new OnDateSetListener() {
		  @Override
		  public void onDateSet(DatePicker view, int year, int monthOfYear,
		    int dayOfMonth) {
			  
			  	mEndYear = year;
	            mEndMonth = monthOfYear;
	            mEndDay = dayOfMonth; 
			  
			  String dayOfWeekStart = DateFormat.format("EEEE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();
			  
			  endDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mEndDay)).append("-")
		                .append(pad(mEndMonth + 1)).append("-")
		                .append(pad(mEndYear)));
			  

		  		/* numberOfDaysText */
			  final Calendar date1 = Calendar.getInstance();
	          final Calendar date2 = Calendar.getInstance();
	
	          date1.clear();
	          date1.set(mStartYear, mStartMonth, mStartDay);
	          date2.clear();
	          date2.set(mEndYear, mEndMonth, mEndDay);
	          
	          int numberOfDays = 1;
	          while (date1.before(date2)) {
	        	  date1.add(Calendar.DATE,1);
	              if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
	                 &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
	                  numberOfDays++;
	              }else {
	            	  date1.add(Calendar.DATE,1);
	              }
	          }
	        
	         // long diff = date2.getTimeInMillis() - date1.getTimeInMillis();
	
	          //float dayCount = (float) diff / (24 * 60 * 60 * 1000);
	          getNumberOfDays = numberOfDays;
	          numberOfDaysText.setText(String.valueOf(numberOfDays));
		  }
		 };
		 
		 	/* *** Start Time *** */
	OnTimeSetListener onStartTime = new OnTimeSetListener() {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mStartHour = hourOfDay;
			mStartMinute = minute;

			 startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
			
		}
	};
		 
		 	/* *** End Time *** */
	OnTimeSetListener onEndTime = new OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				mEndHour = hourOfDay;
				mEndMinute = minute;
				
				 endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
		                    .append(pad(mEndMinute)));
				
			}
		};

		
		/** AsyncTask register holiday  */
	    private class RegisterTaskHoliday extends AsyncTask<String, String, Boolean>{
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	        	
	        	// extract data
	 
	        	  String myStartDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay)).append(" ")
	        			  .append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
		        
	        	  String myEndDateTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay)).append(" ")
	        			  .append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();
	        	
	        	  int hours = getNumberOfDays * 24;
	        	  String hoursText = String.valueOf(hours);
			 
				 
				  // call web method
				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				  boolean suc = false;

					   int holidayID = MyProvider.create_holiday(myStartDateTime, myEndDateTime, hoursText, "0");
					   if(holidayID != 0){suc = true;}

	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
	        	// updating UI from Background Thread
	        	
	        	getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 myUIHelper.ShowDialog(getActivity(), getString(R.string.text_error), error_message);
			    		}
						else
						{
							Toast.makeText(getActivity(), "Request holiday sent", Toast.LENGTH_SHORT).show();
							
							ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
							viewPager.setCurrentItem(0); 
				            
						}
					}
				});
     
		    }
	    }
	 
}
