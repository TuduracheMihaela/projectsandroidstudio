package com.sharethatdata.digitime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import androidx.fragment.app.DialogFragment;

public class YearPickerFragment extends DialogFragment implements NumberPicker.OnValueChangeListener{
	
	public interface Listener {
        public void onDialogPositiveClick(YearPickerFragment dialog);
        public void onDialogNegativeClick(YearPickerFragment dialog);
    }
	
	private View mDialogView = null;
	private Listener mListener = null;
	private Activity mActivity;
	//private OnDateSetListener ondateSet;
	private NumberPicker mPicker;
	private String mTitle = null;
	private Integer mMinValue = null;
    private Integer mMaxValue = null;
	private int year;

	 public YearPickerFragment() {
	 }

	 /*public void setCallBack(OnDateSetListener ondate) {
	  ondateSet = ondate;
	 }	*/ 

	 @Override
	 public void setArguments(Bundle args) {
	  super.setArguments(args);
	  year = args.getInt("year");

	 }
	 
	 @Override
		public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
			// TODO Auto-generated method stub
			 Log.i("value is",""+newVal);
			 year = newVal;
		}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		//final Dialog d = new Dialog(getActivity());
       // d.setTitle("Select year");
       // d.setContentView(R.layout.dialog);
        //Button b1 = (Button) d.findViewById(R.id.button1);
        //Button b2 = (Button) d.findViewById(R.id.button2);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		
		 // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        
     // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        mDialogView = inflater.inflate(R.layout.dialog, null);
        builder.setView(mDialogView)
         // Add action buttons
               .setTitle(mTitle)
               .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int id) {
                       if (mListener != null) {
                    	   mListener.onDialogPositiveClick(YearPickerFragment.this);
                       }
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            	   @Override
            	   public void onClick(DialogInterface dialog, int id) {
            		   if (mListener != null) {
            			   mListener.onDialogNegativeClick(YearPickerFragment.this);
            		   }
            	   }
               });
		
        mPicker = (NumberPicker) mDialogView.findViewById(R.id.numberPicker1);
        mPicker.setWrapSelectorWheel(false);
        mPicker.setOnValueChangedListener(this);
        mPicker.setMinValue(mMinValue);
        mPicker.setMaxValue(mMaxValue);
        mPicker.setValue(year);
        
        
        
        /*b1.setOnClickListener(new OnClickListener()
        {
         @Override
         public void onClick(View v) {
            // tv.setText(String.valueOf(np.getValue()));
             d.dismiss();
          }    
         });
        b2.setOnClickListener(new OnClickListener()
        {
         @Override
         public void onClick(View v) {
             d.dismiss();
          }    
         });
      d.show();*/
		 
        return builder.create();

	}

	YearPickerFragment setActivity(Activity activity) {
    	mActivity = activity;
    	return this;
    }
    
	YearPickerFragment setListener(Listener listener) {
    	mListener = listener;
    	return this;
    }
	
	YearPickerFragment setValue(int value) {
    	year = value;
    	return this;
    }
	
	int getValue() {
    	return year;
    }
	
	YearPickerFragment setMax(int value) {
    	mMaxValue = value;
    	return this;
    }
    
	YearPickerFragment setMin(int value) {
    	mMinValue = value;
    	return this;
    }
    
	YearPickerFragment setTitle(String value) {
    	mTitle = value;
    	return this;
    }

}
