package com.sharethatdata.digitime;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendDirection;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cTimeStats;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class StatisticsActivity extends AppCompatActivity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider2 = null;
	
	TimeUtils MyTimeUtils = null;
	
	private ProgressDialog pDialog;
	
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndYear,mEndMonth,mEndDay;
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	ListView lv = null;
	ListView lv2 = null;
	ListView lv3 = null;
	ListView lv4 = null;
	
	Spinner userSpinner = null;
	Spinner statsSpinner;
		
	ArrayList<HashMap<String, String>> userList;
	List<String> userkeyArray =  new ArrayList<String>();
		
	ArrayList<HashMap<String, String>> projectList;
	ArrayList<HashMap<String, String>> taskList;
	ArrayList<HashMap<String, String>> activityList;
	ArrayList<HashMap<String, String>> locationList;
	
	TextView startDateView;
	TextView endDateView;
	Button prevButton = null;
	Button nextButton = null;
	Button week, month, year;
	
	private boolean userIsInteracting = false;
    private int userSpinnerIndex = -1;
    
    private int spinnerPositionAfterRotateScreen = 0;
	boolean setFirstTimePositionSpinner = false;
	    
    // vars for remember report
    private String report_user = "";
    private String report_user_id = "";
    
    // var for get id of project for Statistics Project Activity
    private String projectid = "";
    private String taskid = "";
    
    
    // CHART GRAPH PRESENTATION
    // MPAndroid for Pie Chart
 	private PieChart mChart;
    ArrayList<String> listNameProject = new ArrayList<String>();
    ArrayList<String> listHoursProject = new ArrayList<String>();
    ArrayList<String> listNameTask = new ArrayList<String>();
    ArrayList<String> listHoursTask = new ArrayList<String>();
    ArrayList<String> listNameActivities = new ArrayList<String>();
    ArrayList<String> listHoursActivities = new ArrayList<String>();
    ArrayList<String> listNameLocations = new ArrayList<String>();
    ArrayList<String> listHoursLocations = new ArrayList<String>();
   
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		lv = (ListView) findViewById(R.id.listView);
		lv2 = (ListView) findViewById(R.id.listView2);
		lv3 = (ListView) findViewById(R.id.listView3);
		lv4 = (ListView) findViewById(R.id.listView4);
				
		userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		statsSpinner = (Spinner) findViewById(R.id.spinnerStats);
		statsSpinner.setVisibility(Spinner.GONE);
			
		prevButton = (Button) findViewById(R.id.btnPrev);
        nextButton = (Button) findViewById(R.id.btnNext);
        prevButton.setVisibility(Button.GONE);
        nextButton.setVisibility(Button.GONE);

		week = (Button) findViewById(R.id.buttonWeek);
		month = (Button) findViewById(R.id.buttonMonth);
		year = (Button) findViewById(R.id.buttonYear);

        startDateView = (TextView) findViewById(R.id.dateStartText);
        endDateView = (TextView) findViewById(R.id.dateEndText);
		
        Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");		
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
	
		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(StatisticsActivity.this, StatisticsActivity.this);
		
		// check manager
		String isManager = Globals.getValue("manager");
		if (isManager == "yes")
		{	
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(View.VISIBLE);
			RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
			userSpinnerLayout.setVisibility(View.VISIBLE);
			
			LoadUserList InitUserList;
			InitUserList = new LoadUserList();
			InitUserList.execute();
			
		} else {
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.GONE);
			RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
			userSpinnerLayout.setVisibility(View.GONE);
		}
		
		// if screen was rotate
        if (isManager == "yes")
		{
	        if (savedInstanceState != null) 
	        {
	        	LoadUserList InitUserList;
				InitUserList = new LoadUserList();
				InitUserList.execute();

	        	int value = savedInstanceState.getInt("spinnerPosition");
	        	spinnerPositionAfterRotateScreen = value;
	        	
	        	setFirstTimePositionSpinner = true;
	        }
		}
		
		
		// set calendar
		Calendar c1 = Calendar.getInstance();
		mStartYear = c1.get(Calendar.YEAR);
		mStartMonth = c1.get(Calendar.MONTH);
		c1.set(Calendar.DAY_OF_MONTH, 1);
		mStartDay = c1.get(Calendar.DAY_OF_MONTH);


		Calendar c2 = Calendar.getInstance();
		mEndYear = c2.get(Calendar.YEAR);
		mEndMonth = c2.get(Calendar.MONTH);
		mEndDay = c2.get(Calendar.DAY_OF_MONTH);

		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
		String weekdayname1 = weekdays[c1.get(Calendar.DAY_OF_WEEK)];
		String weekdayname2 = weekdays[c2.get(Calendar.DAY_OF_WEEK)];

		startDateView.setText(new StringBuilder().append(weekdayname1).append(" ").append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
		endDateView.setText(new StringBuilder().append(weekdayname2).append(" ").append(pad(mEndDay)).append("/").append(pad(mEndMonth + 1)).append("/").append(pad(mEndYear)));

		TextView titleText = (TextView) findViewById(R.id.title);
		String list_type = "Projects";
		titleText.setText(list_type);
		TextView titleText2 = (TextView) findViewById(R.id.title2);
		titleText2.setText("Tasks");
		TextView titleText3 = (TextView) findViewById(R.id.title3);
		titleText3.setText("Activities");
		TextView titleText4 = (TextView) findViewById(R.id.title4);
		titleText4.setText("Locations");
		
		userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user = userkeyArray.get(myIndex);
		  	   		report_user_id = userkeyArray.get(myIndex);
		  	   
		  	   		userSpinnerIndex = myIndex;
		  	   		
		  	   		spinnerPositionAfterRotateScreen = myIndex;
		  	   		
		  		    //LoadStatsPerMonth();
		  	   		LoadStatsPerDay();
	            	pieChartProjects();
	            	pieChartTasks();
	            	pieChartActivities();
	            	pieChartLocations();
		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				HashMap<String, String> itemsList = (HashMap<String, String>) projectList.get(position);
				 String item = (String) itemsList.get("pid");
				 
				 Intent intent = new Intent(getApplicationContext(), StatisticsProjectActivity.class);
				 intent.putExtra("PID", item);
				 intent.putExtra("CID", report_user_id);
				 startActivity(intent);
				
				
				  Toast.makeText(getApplicationContext(),
					      "Click ListItem Number " + item, Toast.LENGTH_SHORT)
					      .show();
				
			}
		});

		week.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

					Calendar c = Calendar.getInstance();

					c.add(Calendar.DATE, -6);
					mStartYear = c.get(Calendar.YEAR);
					mStartMonth = c.get(Calendar.MONTH);
					mStartDay = c.get(Calendar.DAY_OF_MONTH);

				String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
				startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
						.append(pad(mStartMonth + 1)).append("/")
						.append(pad(mStartYear)));

				LoadStatsPerDay();
			}
		});

		month.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Calendar c = Calendar.getInstance();

				c.add(Calendar.MONTH, -1);
				mStartYear = c.get(Calendar.YEAR);
				mStartMonth = c.get(Calendar.MONTH);
				mStartDay = c.get(Calendar.DAY_OF_MONTH);

				String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
				startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
						.append(pad(mStartMonth + 1)).append("/")
						.append(pad(mStartYear)));

				LoadStatsPerDay();
			}
		});

		year.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Calendar c = Calendar.getInstance();

				c.add(Calendar.YEAR, -1);
				mStartYear = c.get(Calendar.YEAR);
				mStartMonth = c.get(Calendar.MONTH);
				mStartDay = c.get(Calendar.DAY_OF_MONTH);

				String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
				startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
						.append(pad(mStartMonth + 1)).append("/")
						.append(pad(mStartYear)));

				LoadStatsPerDay();
			}
		});
		// PREV BUTTON
		/*prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String yearSelect = String.valueOf(statsSpinner.getSelectedItem());
        		
             // YEAR
        		if(yearSelect.contains("Year")){
        			Calendar c = Calendar.getInstance();
			        
	                mStartYear = mStartYear - 1;
	        	    
	        	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth)).append("-").append(pad(mStartDay))).toString();
	        	  
	        	  	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	        				
	                try {
	                	Date myDate = dateFormat.parse(myDateTime);
	                    
	                	c.setTime(myDate);
	                    mStartYear = c.get(Calendar.YEAR);
	        		    
	        		    TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	                    startDayView.setText(new StringBuilder().append(pad(mStartYear)));
	                   
	                } catch (ParseException e) {
	                    	
	                }
	        	  	  
	        	    LoadStatsPerYear();
	        	    
        		}
        		
        		// MONTH
        		else if(yearSelect.contains("Month")){
        			
        			Calendar c = Calendar.getInstance();
        	        
                    mStartMonth = mStartMonth - 1;
            	    
            	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
            	  
            	  	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            				
                    try {
                    	Date myDate = dateFormat.parse(myDateTime);
                        
                    	c.setTime(myDate);
                        mStartYear = c.get(Calendar.YEAR);
                		mStartMonth = c.get(Calendar.MONTH);
                		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            	 	    SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            		    String month_name = month_date.format(c.getTime());
            		    
            		    TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                        startDayView.setText(month_name);
                       
                    } catch (ParseException e) {
                        	
                    }
                    
                    LoadStatsPerMonth();
        			
        		}
        		
        		// WEEK
        		else if(yearSelect.contains("Week")){
        			
        		}
        		
        		// DAY
        		else if(yearSelect.contains("Day")){

        			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
            		String weekdayname = "";
            		
                    try {
                    	Date myDate = dateFormat.parse(startDayView.getText().toString());
                        
                        Calendar c = Calendar.getInstance();
                        c.setTime(myDate);
                        c.add(Calendar.DATE, -1); 
                	    mStartYear = c.get(Calendar.YEAR);
                		mStartMonth = c.get(Calendar.MONTH);
                		mStartDay = c.get(Calendar.DAY_OF_MONTH);
                		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
                		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
                		    	        	       
                		
                    } catch (ParseException e) {
                        	
                    }
                  
                    startDayView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                   // startDayView.setText(weekdayname);
                    
                    LoadStatsPerDay();
        			
        		}
        	  	  
        	   
        	    
            }

        });*/
		
		// NEXT BUTTON
		/*nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
  
                String yearSelect = String.valueOf(statsSpinner.getSelectedItem());
        		
             // YEAR
        		if(yearSelect.contains("Year")){
        			
        			Calendar c = Calendar.getInstance();
	            	if (mStartMonth == 11) mStartYear = mStartYear + 1; 
	            	mStartYear = mStartYear + 1;
	        	    if (mStartMonth > 11) mStartMonth = 0;
	              
	        	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth)).append("-").append(pad(mStartDay))).toString();
	        	  
	        	  	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	        				
	                try {
	                	Date myDate = dateFormat.parse(myDateTime);
	                    
	                	c.setTime(myDate);
	                    mStartYear = c.get(Calendar.YEAR);
	        		    
	        		    TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	                    startDayView.setText(new StringBuilder().append(pad(mStartYear)));
	                   
	                } catch (ParseException e) {
	                    	
	                }
	        	  	  
	        	    LoadStatsPerYear();
        			
        		}
        		
        		// MONTH
        		else if(yearSelect.contains("Month")){
        			
        			Calendar c = Calendar.getInstance();
                	if (mStartMonth == 11) mStartYear = mStartYear + 1; 
            	    mStartMonth = mStartMonth + 1;
            	    if (mStartMonth > 11) mStartMonth = 0;
                  
            	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
            	  
            	  	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            				
                    try {
                    	Date myDate = dateFormat.parse(myDateTime);
                        
                    	c.setTime(myDate);
                        mStartYear = c.get(Calendar.YEAR);
                		mStartMonth = c.get(Calendar.MONTH);
                		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            	 	    SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            		    String month_name = month_date.format(c.getTime());
            		    
            		    TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                        startDayView.setText(month_name);
                       
                    } catch (ParseException e) {
                        	
                    }
                    
                    LoadStatsPerMonth();
        			
        		}
        		
        		// WEEK
        		else if(yearSelect.contains("Week")){
        			
        		}
        		
        		// DAY
        		else if(yearSelect.contains("Day")){
        			
        			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                    String weekdayname = "";
            				
                    try {
                    	Date myDate = dateFormat.parse(startDayView.getText().toString());
                        
                        Calendar c = Calendar.getInstance();
                        c.setTime(myDate);
                        c.add(Calendar.DATE, 1); 
                	    mStartYear = c.get(Calendar.YEAR);
                		mStartMonth = c.get(Calendar.MONTH);
                		mStartDay = c.get(Calendar.DAY_OF_MONTH);
                		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
                		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
                	
                		
                    } catch (ParseException e) {
                        	
                    }
                  
                    startDayView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                    //startDayView.setText(weekdayname);
                    
                    LoadStatsPerDay();
        			
        		}
                
        	   
            }

        });*/
		
		
		
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();
		super.onDestroy();
	}
	
	 @Override
	 public void onSaveInstanceState(Bundle savedInstanceState) {
	   super.onSaveInstanceState(savedInstanceState);
	   Globals = ((MyGlobals) getApplicationContext());
       myUser = Globals.getValue("user");
       MyProvider = new WSDataProvider(myUser, myPass); 
       
	   String isManager = Globals.getValue("manager");
		 
		if (isManager == "yes")
		{
			userSpinner = (Spinner) findViewById(R.id.spinnerUser);
			int value = userSpinner.getSelectedItemPosition();
			spinnerPositionAfterRotateScreen = value;
			savedInstanceState.putInt("spinnerPosition", userSpinner.getSelectedItemPosition());
		}
	 }
	
	 @Override
	 public void onConfigurationChanged(Configuration newConfig) {
	     super.onConfigurationChanged(newConfig);

	     // Checks the orientation of the screen
	     if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	         
	     } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	        
	     }
	 }
	
	private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
			  
			  /*String yearSelect = String.valueOf(statsSpinner.getSelectedItem());
			  
			  if(yearSelect.contains("Day")){
				  mStartYear = year;
		            mStartMonth = month;
		            mStartDay = day; 
		            		
		          TextView startDayView = (TextView) findViewById(R.id.dateDayText);     
		          startDayView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
			                .append(pad(mStartMonth + 1)).append("-")
			                .append(pad(mStartYear)));
		                
			  
		          LoadStatsPerDay();
			  }*/
			  
			    mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            
			    String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
	            		
	          startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
		                .append(pad(mStartMonth + 1)).append("/")
		                .append(pad(mStartYear)));
	                
		  
	          LoadStatsPerDay();
	          
			  
		  }

	};
	
	private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
			  
			    mEndYear = year;
	            mEndMonth = month;
	            mEndDay = day; 
	            
	            String dayOfWeekEnd = DateFormat.format("EE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();
	            		
	          endDateView.setText(new StringBuilder().append(dayOfWeekEnd).append(" ").append(pad(mEndDay)).append("/")
		                .append(pad(mEndMonth + 1)).append("/")
		                .append(pad(mEndYear)));
	          
	          LoadStatsPerDay();
	          
			  
		  }

	};

	public void onStartDayStatsClick(View v) 
	{
		/*String yearSelect = String.valueOf(statsSpinner.getSelectedItem());
		
		if(yearSelect.contains("Year")){
			
		}
		else if(yearSelect.contains("Month")){
			
		}
		else if(yearSelect.contains("Week")){
			
		}
		else if(yearSelect.contains("Day")){
			 DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
			    dp1.show();
		}*/
		
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
	    dp1.show();
	    
	}
	
	public void onEndDayStatsClick(View v){
		DatePickerDialog dp2 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp2.setTitle(sdf.format(d));
		dp2.show();
	}
	
	 @Override
     public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    
     }
	
	 @Override
	public void onResume() {
	    super.onResume();
	 
	    //LoadStatsPerMonth();
	    LoadStatsPerDay();
    	pieChartProjects();
    	pieChartActivities();
    	pieChartLocations();
	    
	}

	 @Override
	 public void onUserInteraction() {
	    super.onUserInteraction();
	    userIsInteracting = true;
	}
 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.statistics, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_enter_report:
				Intent intent = new Intent(this, RecordTimeActivity.class);
				intent.addFlags ( Intent.FLAG_ACTIVITY_CLEAR_TOP );
				startActivity(intent);
				return true;
			case R.id.action_reports:
				finish();
				return true;
			case R.id.action_progress:
				Intent intent3 = new Intent(this, ProgressActivity.class); startActivity(intent3);
				return true;
			case R.id.action_holiday:
				Intent intent4 = new Intent(this, HolidaysActivity.class); startActivity(intent4);
				return true;
			case R.id.action_sickday:
				Intent intent5 = new Intent(this, SickdaysActivity.class); startActivity(intent5);
				return true;
			case R.id.action_stop_watch:
				Intent intent6 = new Intent(this, StopWatchActivity.class); startActivity(intent6);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}


	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	private void LoadStatsPerDay()
	{

   	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
	 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);
			
     	LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
		
	}
	
	private void LoadStatsPerMonth()
	{

   	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 2)).append("-").append(pad(mEndDay))).toString();
	 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);
			
     	LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
		
	}
	
	private void LoadStatsPerYear()
	{

   	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear + 1)).append("-").append(pad(mEndMonth)).append("-").append(pad(mEndDay))).toString();
	 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);
			
     	LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
		
	}
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
         pDialog = new ProgressDialog(StatisticsActivity.this);
         pDialog.setMessage( getString(R.string.loading_alert_dialog));    
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.setCanceledOnTouchOutside(false);
         pDialog.show();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
    	Globals = ((MyGlobals)getApplication());    	
    	MyProvider2 = new WSDataProvider(myUser, myPass);
	    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider2.getMyUsers();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.name);
          
                // adding HashList to ArrayList
                userList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
           
    	    List<String> userArray =  new ArrayList<String>();
           	for (HashMap<String, String> map : userList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
          	        }
           	
           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(StatisticsActivity.this, android.R.layout.simple_spinner_item, userArray);
      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
      	    	sUsers.setAdapter(user_adapter);
      	    	
      	    	/*if(userSpinnerIndex != -1) 
      	    	  // set the value of the spinner 
      	    		sUsers.setSelection(userSpinnerIndex);*/
      	    	
      	    	if(setFirstTimePositionSpinner == false){
    	        	int spinnerPosition = user_adapter.getPosition(myUserName);
		    		//set the default according to value
		    		sUsers.setSelection(spinnerPosition);
		    		
		    		setFirstTimePositionSpinner = true;
    	        }else{
    	        	// set value spinner if change rotation (portrait/landscape)
		    		int x = spinnerPositionAfterRotateScreen;
		    		userSpinner.setSelection(x);
    	        }
      	
      	    	userIsInteracting = false;
      	    	
      	    	pDialog.dismiss();
        	   
           }
       	});
       
       pDialog.dismiss();
 	      
    	}
	}
	
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

         showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
			
			Globals = ((MyGlobals)getApplication());
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
		
	    	 // load data from provider
	    	 projectList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cTimeStats> timeRecords = MyProvider.getTimeStats(report_date_start, report_date_end, report_user, "project");
	    	 for(cTimeStats entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.hours));
                 
                // adding HashList to ArrayList
                projectList.add(map);
                
                // projectid for Statistics Project Activity
                projectid = entry.id;
                
                // list for graph chart
                listNameProject.add(entry.name);
                listHoursProject.add (Integer.toString(entry.hours));
	    	 }
	    	
	    	 // load data from provider
	    	 taskList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 timeRecords = MyProvider.getTimeStats(report_date_start, report_date_end, report_user, "task");
	    	 for(cTimeStats entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.hours));
                 
                // adding HashList to ArrayList
                taskList.add(map);
                
                // taskid for Statistics task Activity
                taskid = entry.id;
                
                // list for graph chart
                listNameTask.add(entry.name);
                listHoursTask.add (Integer.toString(entry.hours));
	    	 }
	    	
	    	 
	    	 // load data from provider
	    	 activityList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 timeRecords = MyProvider.getTimeStats(report_date_start, report_date_end, report_user, "activity");
	    	 for(cTimeStats entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.hours));
                 
                // adding HashList to ArrayList
                activityList.add(map);
                
                // list for graph chart
                listNameActivities.add(entry.name);
                listHoursActivities.add (Integer.toString(entry.hours));
	    	 }
	    	 
	    	 // load data from provider
	    	 locationList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 timeRecords = MyProvider.getTimeStats(report_date_start, report_date_end, report_user, "location");
	    	 for(cTimeStats entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.hours));
                 
                // adding HashList to ArrayList
                locationList.add(map);
                
                // list for graph chart
                listNameLocations.add(entry.name);
                listHoursLocations.add (Integer.toString(entry.hours));
	    	 }
	    	 
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;
	            	ListAdapter adapter2 = null;
	             	ListAdapter adapter3 = null;
	             	ListAdapter adapter4 = null;
	             		 	           	
	            	// total project
	            	int total = 0;
	            	int myNum = 0;
	            	for (HashMap<String, String> map : projectList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                HashMap<String, String> hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", String.valueOf(total));
	                hmap.put("name", "total");
	           	                
	            	projectList.add(hmap);
	            	
	            	// total activity
	            	total = 0;
	            	myNum = 0;
	            	for (HashMap<String, String> map : activityList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", String.valueOf(total));
	                hmap.put("name", "total");
	           	                
	                activityList.add(hmap);
	                
	                // total task
	            	total = 0;
	            	myNum = 0;
	            	for (HashMap<String, String> map : taskList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", String.valueOf(total));
	                hmap.put("name", "total");
	           	                
	                taskList.add(hmap);
	                
	            	// total location
	            	total = 0;
	            	myNum = 0;
	            	for (HashMap<String, String> map : locationList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", String.valueOf(total));
	                hmap.put("name", "total");
	           	                
	                locationList.add(hmap);
	            	
	            	adapter = new SimpleAdapter(
		                		StatisticsActivity.this
		                		, projectList
		                		, R.layout.sublistview_item, new String[] {"pid", "name", "hours" },
	                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
		        	            
	                
	                // updating listview
	                lv.setAdapter(adapter);
	               	 
	            	adapter2 = new SimpleAdapter(
	                		StatisticsActivity.this
	                		, taskList
	                		, R.layout.sublistview_item, new String[] {"pid", "name", "hours" },
                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
	        	            
                
	            	// updating listview
	            	lv2.setAdapter(adapter2);
	            	
	            	adapter3 = new SimpleAdapter(
	                		StatisticsActivity.this
	                		, activityList
	                		, R.layout.sublistview_item, new String[] {"pid", "name", "hours" },
                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
	        	            
                
	            	// updating listview
	            	lv3.setAdapter(adapter3);
	            	
	            	adapter4 = new SimpleAdapter(
	                		StatisticsActivity.this
	                		, locationList
	                		, R.layout.sublistview_item, new String[] {"pid", "name", "hours" },
                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
	        	            
                
	            	// updating listview
	            	lv4.setAdapter(adapter4);
	            	
	            	
	            	// update chart graph presentation
	            	pieChartProjects();
	            	pieChartTasks();
	            	pieChartActivities();
	            	pieChartLocations();

	            	Utility.setListViewHeightBasedOnChildren(lv);
	            	Utility.setListViewHeightBasedOnChildren(lv2);
	            	Utility.setListViewHeightBasedOnChildren(lv3);
	            	Utility.setListViewHeightBasedOnChildren(lv4);

	            }
	        });
	        
	        hideProgressDialog();
	    }
	}
	
	
	
	// MPAndroid for Pie Chart
	private void pieChartProjects(){

		  mChart = (PieChart) findViewById(R.id.pieChartProject);
	        mChart.setUsePercentValues(false);
	        mChart.setDescription("");
	        mChart.setNoDataText("No data");
	        mChart.setNoDataTextDescription("");
	        
	        mChart.setDragDecelerationFrictionCoef(0.95f);
	        
	        mChart.setDrawSliceText(false);
	        mChart.setUsePercentValues(false);
	        mChart.setCenterTextWordWrapEnabled(true);


	        mChart.setDrawHoleEnabled(true);
	        mChart.setHoleColorTransparent(true);

	        mChart.setTransparentCircleColor(Color.WHITE);
	        mChart.setTransparentCircleAlpha(110);
	        
	        mChart.setHoleRadius(58f);
	        mChart.setTransparentCircleRadius(61f);

	        mChart.setDrawCenterText(false);   

	        mChart.setRotationAngle(0);
	        // enable rotation of the chart by touch
	        mChart.setRotationEnabled(true);

	        // mChart.setUnit(" ");
	        // mChart.setDrawUnitsInChart(true);

	        // add a selection listener
	       // mChart.setOnChartValueSelectedListener(this);

	        mChart.setCenterText("");
	        
	        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
	        // mChart.spin(2000, 0, 360);

	        Legend l = mChart.getLegend();
	        l.setPosition(LegendPosition.BELOW_CHART_LEFT);
	        l.setWordWrapEnabled(true);
	        l.setDirection(LegendDirection.LEFT_TO_RIGHT);
	        l.setXEntrySpace(5f);
	        l.setYEntrySpace(0f);
	        l.setYOffset(0f);
	        l.setXOffset(0f);
	        
	        ArrayList<com.github.mikephil.charting.data.Entry> yVals1 = new ArrayList<com.github.mikephil.charting.data.Entry>();

	        // IMPORTANT: In a PieChart, no values (Entry) should have the same
	        // xIndex (even if from different DataSets), since no values can be
	        // drawn above each other.
	        for (int i = 0; i < listHoursProject.size(); i++) {
	        	
	        	 // get hours
	            
	            String hour = null;
	 	        Float[] mHours = new Float[listHoursProject.size()];
	 	        
	            hour = listHoursProject.get(i);
	            Log.i("..............:",""+hour);

	            mHours[i] = Float.parseFloat(hour);
	            
	            Float hourF =  mHours[i];
	             
	            yVals1.add( new com.github.mikephil.charting.data.Entry ( (float) hourF, i));
	            
	            Log.i("..............:",""+yVals1);

	        }
	        
	        
	        ArrayList<String> xVals = new ArrayList<String>();

	        for (int i = 0; i < listNameProject.size(); i++){
	        	
	        	// get name of project
	        	String name = null;
		 		String[] mProjects = new String[] {};
		 		 
	            name = listNameProject.get(i);
	            Log.i("..............:",""+name);
	            
	            mProjects = listNameProject.toArray(new String[listNameProject.size()]);
	        	
	            xVals.add(mProjects[i]);
	            
	        	//xVals.add(mParties[i % mParties.length]);
	            
	            String namep = null;
	            namep = mProjects[i];
	            Log.i("..............:",""+namep);
	            Log.i("..............:",""+xVals);

	        }
       
        
     // PROJECTS

        
	        PieDataSet dataSet = new PieDataSet(yVals1, " ");
	        dataSet.setSliceSpace(3f);
	        dataSet.setSelectionShift(5f);

	        // add a lot of colors

	        /*ArrayList<Integer> colors = new ArrayList<Integer>();

	        for (int c : ColorTemplate.VORDIPLOM_COLORS)
	            colors.add(c);

	        for (int c : ColorTemplate.JOYFUL_COLORS)
	            colors.add(c);

	        for (int c : ColorTemplate.COLORFUL_COLORS)
	            colors.add(c);

	        for (int c : ColorTemplate.LIBERTY_COLORS)
	            colors.add(c);

	        for (int c : ColorTemplate.PASTEL_COLORS)
	            colors.add(c);

	        colors.add(ColorTemplate.getHoloBlue());*/
	        
	        final int COLOR_GREEN = Color.parseColor("#62c51a");
	        final int COLOR_DARK_GREEN = Color.parseColor("#38610B");
	    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
	    	final int COLOR_BLUE = Color.parseColor("#23bae9");
	    	final int COLOR_DARK_BLUE = Color.parseColor("#4074ED");
	    	final int COLOR_YELLOW = Color.parseColor("#E6E32F");
	    	final int COLOR_CYAN = Color.parseColor("#40EDD9");
	    	final int COLOR_PURPLE = Color.parseColor("#AC58FA");

	        
	     // Color of each Pie Chart Sections
	        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, COLOR_DARK_GREEN, Color.RED, COLOR_CYAN,
	        		COLOR_ORANGE, COLOR_YELLOW, COLOR_DARK_BLUE, COLOR_PURPLE};

	        dataSet.setColors(colors);

	        PieData data = new PieData(xVals, dataSet);
	       // data.setValueFormatter(new PercentFormatter());
	        data.setValueTextSize(0f); // percentage text on pie chart
	        data.setValueTextColor(Color.WHITE); // percentage text color on pie chart
	        mChart.setData(data);

	        // undo all highlights
	        mChart.highlightValues(null);

	        mChart.invalidate();
	        
	        listNameProject.clear();
	        listHoursProject.clear();

	}
	
	
	// AChartEngine for Pie Chart
	private void pieChartProjectsTest(){
		// Instantiating CategorySeries to plot Pie Chart
        CategorySeries projectSeries = new CategorySeries( " From " + mStartDay + " " + new DateFormatSymbols().getMonths()[mStartMonth] + " " + mStartYear + 
        		" - " + " To " + mEndDay + " " + new DateFormatSymbols().getMonths()[mEndMonth] + " " + mEndYear);
        
     // Instantiating a renderer for the Pie Chart
        DefaultRenderer defaultRenderer  = new DefaultRenderer();
        
        defaultRenderer.setChartTitle(" From " + mStartDay + " " + new DateFormatSymbols().getMonths()[mStartMonth] + " " + mStartYear + 
        		" - " + " To " + mEndDay + " " + new DateFormatSymbols().getMonths()[mEndMonth] + " " + mEndYear);
        defaultRenderer.setChartTitleTextSize(30);
        defaultRenderer.setLabelsTextSize(35);  
        defaultRenderer.setLegendTextSize(35); 
        defaultRenderer.setMargins(new int[] { 20, 30, 15, 0 });  
        defaultRenderer.setApplyBackgroundColor(true);
        defaultRenderer.setBackgroundColor(Color.WHITE);
        defaultRenderer.setZoomButtonsVisible(false);
        defaultRenderer.setStartAngle(90); 
        defaultRenderer.setDisplayValues(true);
        
        final int COLOR_GREEN = Color.parseColor("#62c51a");
    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
    	final int COLOR_BLUE = Color.parseColor("#23bae9");
        
     // Color of each Pie Chart Sections
        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, Color.RED,
        		COLOR_ORANGE };
       
        
     // PROJECTS
 		 for(int i = 0; i < listNameProject.size() ; i++)
         {
 			 // get name of project
 			 
 			String name = null;
	 		 String[] mProjects = new String[] {};
	 		 
            name = listNameProject.get(i);
            Log.i("..............:",""+name);
            
            mProjects = listNameProject.toArray(new String[listNameProject.size()]);
            
            
            // get hours
            
            String hour = null;
 	        Double[] mHours = new Double[listHoursProject.size()];
 	        
            hour = listHoursProject.get(i);
             Log.i("..............:",""+hour);

             mHours[i] = Double.parseDouble(hour);
             
          // Adding a slice with its values and name to the Pie Chart
             projectSeries.add(mProjects[i], mHours[i]);
              
	             String namep = null;
	             namep = mProjects[i];
	            Log.i("..............:",""+namep);

 		           
         /* Random randomGenerator = new Random();
         int red = randomGenerator.nextInt(256);
         int green = randomGenerator.nextInt(256);
         int blue = randomGenerator.nextInt(256);

         int randomColour = new Color().rgb(red, green, blue);*/
 		           
          SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(colors[i]);
            seriesRenderer.setDisplayChartValues(true);
            seriesRenderer.setChartValuesTextSize(30);
            seriesRenderer.setShowLegendItem(true);
            
            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer); 
          
         }
        
        //this part is used to display graph on the xml
	    LinearLayout chartContainerProjects = (LinearLayout) findViewById(R.id.barchartProjects);
	    
        
        //drawing bar chart
	    GraphicalView mChartProjects;
	       mChartProjects = ChartFactory.getPieChartView(StatisticsActivity.this, projectSeries, defaultRenderer);
	       
	       Animation animChart = AnimationUtils.loadAnimation(StatisticsActivity.this, R.anim.move_left_in_activity);
	       mChartProjects.startAnimation(animChart);
	      
	       
	       if(mChartProjects != null){
	    	   mChartProjects.repaint();
	       }
	    
     //remove any views before u paint the chart
	    chartContainerProjects.removeAllViews();
      
     
      //adding the view to the linearlayout
      chartContainerProjects.addView(mChartProjects);
      
      listNameProject.clear();
      listHoursProject.clear();
        

	}
	
	
	// AChartEngine for Bar Chart
	private void getBarChartProjects(){
		/* *** GRAPH CHART PRESENTATION *** */
		
		XYMultipleSeriesRenderer multiRenderer= new XYMultipleSeriesRenderer();
		
		XYMultipleSeriesDataset datasetProjects = new XYMultipleSeriesDataset();
	    
	    // projectsSeries = new XYSeries("");

	    
		 int[] x = { 1,2,3,4,5,6,7, 8, 9, 10, 11, 12 };
		 
		// Creating a XYMultipleSeriesRenderer to customize the whole chart
        
		 multiRenderer.setOrientation(XYMultipleSeriesRenderer.Orientation.HORIZONTAL);
       
		 multiRenderer.setXLabels(0);
       
		 multiRenderer.setChartTitle("Projects");
       
		 multiRenderer.setXTitle(" From " + mStartDay + " " + new DateFormatSymbols().getMonths()[mStartMonth] + " " + mStartYear + " - " + " To " + mEndDay + " " + new DateFormatSymbols().getMonths()[mEndMonth] + " " + mEndYear);
       
		 multiRenderer.setYTitle("Hours");

       
        /***
       
        * Customizing graphs
       
        */
       
       //setting text size of the title
		 multiRenderer.setChartTitleTextSize(33);
        
        //setting text size of the axis title
		 multiRenderer.setAxisTitleTextSize(34);
        
        //setting text size of the graph lable
		 multiRenderer.setLabelsTextSize(30);
        
		 multiRenderer.setLegendTextSize(30);
        
		// multiRenderer.setPointSize(50);
        
       
        //setting zoom buttons visiblity
		 multiRenderer.setZoomButtonsVisible(false);
       
        //setting pan enablity which uses graph to move on both axis
		 multiRenderer.setPanEnabled(false, false);
       
        //setting click false on graph 
		 multiRenderer.setClickEnabled(false);
       
        //setting zoom to false on both axis
		 multiRenderer.setZoomEnabled(false, false);
       
        //setting lines to display on y axis
		 multiRenderer.setShowGridY(false);
       
        //setting lines to display on x axis
		 multiRenderer.setShowGridX(false);
       
        //setting legend to fit the screen size
		 multiRenderer.setFitLegend(true);
       
        //setting displaying line on grid 
		 multiRenderer.setShowGrid(false);
       
        //setting zoom to false
		 multiRenderer.setZoomEnabled(false);
       
        //setting external zoom functions to false
		 multiRenderer.setExternalZoomEnabled(false);
       
        //setting displaying lines on graph to be formatted(like using graphics)
		 multiRenderer.setAntialiasing(true);
       
        //setting to in scroll to false
		 multiRenderer.setInScroll(false);
       
        //setting to set legend height of the graph
		 multiRenderer.setLegendHeight(30);
		   
        //setting x axis label align
		 multiRenderer.setXLabelsAlign(Align.CENTER);
       
        //setting y axis label to align
		 multiRenderer.setYLabelsAlign(Align.RIGHT);
       
        //setting text style
		 multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
       
        //setting no of values to display in y axis
		 multiRenderer.setYLabels(10);
       
        // setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 200.
       
        // if you use dynamic values then get the max y value and set here
       
		 multiRenderer.setYAxisMax(200);
        
		 multiRenderer.setYAxisMin(0);

        //setting used to move the graph on xaxiz to .5 to the right
		 multiRenderer.setXAxisMin(-0.5);
       
       //setting max values to be display in x axis
		 multiRenderer.setXAxisMax(11);
       
        //setting bar size or space between two bars
		 multiRenderer.setBarSpacing(0.5);
        
		 multiRenderer.setBarWidth(20);
       
        //Setting background color of the graph to transparent
		 multiRenderer.setBackgroundColor(Color.TRANSPARENT);
       
        //Setting margin color of the graph to transparent
		 multiRenderer.setMarginsColor(getResources().getColor(R.color.black1));
       
		 multiRenderer.setApplyBackgroundColor(true);
       

        //setting the margin size for the graph in the order top, left, bottom, right 
		 multiRenderer.setMargins(new int[]{50, 80, 80, 40});
       
		 // add months to x axis
        /*for(int i=0; i< x.length;i++){
       
        	multiRenderer.addXTextLabel(i, mMonth[i]);
       
        }*/
 
		 // PROJECTS
       
		 String name = null;
		 String[] mProjects = new String[] {};
		 for(int i = 0; i < listNameProject.size() ; i++)
        {
           name = listNameProject.get(i);
           Log.i("..............:",""+name);
           
           mProjects = listNameProject.toArray(new String[listNameProject.size()]);
           
           // Creating an XYSeries    
           XYSeries projectsSeries = new XYSeries(mProjects[i].toString());
           //CategorySeries series = new CategorySeries(mProjects[i]);

		           String hour = null;
		           //Double[] mHours = new Double[listHoursProject.size()];
		           for(int j = 0; j < 1; j++)
		           {
		              hour = listHoursProject.get(i);
		              Log.i("..............:",""+hour);
		              
		              Double hourD = Double.parseDouble(hour); 
		              
		              projectsSeries.add(i, hourD);
		              
		             // mHours[j] = Double.parseDouble(hour); //listHoursProject.toArray(new Double[listHoursProject.size()]);
		              
		             // projectsSeries.add(mHours[j]);
		              
		             // series.add(mHours[j]);
		               
		           }
		           
		           
        // Adding Project Series to the dataset  
           datasetProjects.addSeries(projectsSeries);
           
           Random randomGenerator = new Random();
           int red = randomGenerator.nextInt(256);
           int green = randomGenerator.nextInt(256);
           int blue = randomGenerator.nextInt(256);

           int randomColour = new Color().rgb(red, green, blue);
           
          /* int[] colors = new int[] { Color.LTGRAY, Color.rgb(100,128,171) , Color.RED, Color.BLUE};*/
           
           
        // Creating ProjectSeriesRenderer to customize series
           
           XYSeriesRenderer projectRenderer = new XYSeriesRenderer();
          
           projectRenderer.setShowLegendItem(true);

          // for(int c = 0; c < colors.length; c++){
        	   
        	   projectRenderer.setColor(randomColour); //color of the graph
 
          // }
            
           projectRenderer.setFillPoints(true);
          
           //projectRenderer.setLineWidth(2);
          
           projectRenderer.setDisplayChartValues(true);
           
           projectRenderer.setChartValuesTextSize(30);
           
          // projectRenderer.setChartValuesTextAlign(Align.CENTER);
          
          // projectRenderer.setDisplayChartValuesDistance(0); //setting chart value distance
                      
           multiRenderer.addSeriesRenderer(projectRenderer);
           
           
        }
		
        
        //this part is used to display graph on the xml
	    LinearLayout chartContainerProjects = (LinearLayout) findViewById(R.id.barchartProjects);
		
	    
	    //drawing bar chart
	     View mChartProjects;
	       mChartProjects = ChartFactory.getBarChartView(StatisticsActivity.this, datasetProjects, multiRenderer,Type.STACKED);
	       
	       Animation animChart = AnimationUtils.loadAnimation(StatisticsActivity.this, R.anim.move_left_in_activity);
	       mChartProjects.startAnimation(animChart);
	      
	    
      //remove any views before u paint the chart
	    chartContainerProjects.removeAllViews();
       
      
       //adding the view to the linearlayout
       chartContainerProjects.addView(mChartProjects); 
       
       listNameProject.clear();
       listHoursProject.clear();
       
      
       
	}
	
/*	private void drawLegend(){
		 float entryspace = Utils.calcTextWidth(mLegendLabelPaint,
                 labels[i]) + mLegend.getXEntrySpace();
         if ((entryspace + posX) >= (getWidth() - getOffsetRight())) {

             posX = mLegend.getOffsetLeft();
             posY += Utils.calcTextHeight(mLegendLabelPaint, labels[i])+mLegend.getYEntrySpace();
	}*/
	
	// CHART GRAPH ACTIVITIES
	private void pieChartTasks(){
		/* *** GRAPH CHART PRESENTATION *** */	
	    
		 mChart = (PieChart) findViewById(R.id.pieChartTasks);
	        mChart.setUsePercentValues(true);
	        mChart.setDescription("");
	        mChart.setNoDataText("No data");
	        mChart.setNoDataTextDescription("");
	        
	        mChart.setDragDecelerationFrictionCoef(0.95f);
	        
	        mChart.setDrawSliceText(false);
	        mChart.setUsePercentValues(false);
	        mChart.setCenterTextWordWrapEnabled(true);


	        mChart.setDrawHoleEnabled(true);
	        mChart.setHoleColorTransparent(true);

	        mChart.setTransparentCircleColor(Color.WHITE);
	        mChart.setTransparentCircleAlpha(110);
	        
	        mChart.setHoleRadius(58f);
	        mChart.setTransparentCircleRadius(61f);

	        mChart.setDrawCenterText(true);   

	        mChart.setRotationAngle(0);
	        // enable rotation of the chart by touch
	        mChart.setRotationEnabled(true);

	        // mChart.setUnit(" ");
	        // mChart.setDrawUnitsInChart(true);

	        // add a selection listener
	       // mChart.setOnChartValueSelectedListener(this);

	        mChart.setCenterText("");
	        
	        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
	        // mChart.spin(2000, 0, 360);

	        Legend l = mChart.getLegend();
	        l.setPosition(LegendPosition.BELOW_CHART_LEFT);
	        l.setWordWrapEnabled(true);
	        l.setDirection(LegendDirection.LEFT_TO_RIGHT);
	        l.setXEntrySpace(5f);
	        l.setYEntrySpace(0f);
	        l.setYOffset(0f);
	        l.setXOffset(0f);
	       
	        
	        ArrayList<com.github.mikephil.charting.data.Entry> yVals1 = new ArrayList<com.github.mikephil.charting.data.Entry>();

	        // IMPORTANT: In a PieChart, no values (Entry) should have the same
	        // xIndex (even if from different DataSets), since no values can be
	        // drawn above each other.
	        for (int i = 0; i < listHoursTask.size(); i++) {
	        	
	        	 // get hours
	            
	            String hour = null;
	 	        Float[] mHours = new Float[listHoursTask.size()];
	 	        
	            hour = listHoursTask.get(i);
	            Log.i("..............:",""+hour);

	            mHours[i] = Float.parseFloat(hour);
	            
	            Float hourF =  mHours[i];
	             
	            yVals1.add( new com.github.mikephil.charting.data.Entry ( (float) hourF, i));
	            
	            Log.i("..............:",""+yVals1);

	        }
	        
	        
	        ArrayList<String> xVals = new ArrayList<String>();

	        for (int i = 0; i < listNameTask.size(); i++){
	        	
	        	// get name of project
	        	String name = null;
		 		String[] mActivities = new String[] {};
		 		 
	            name = listNameTask.get(i);
	            Log.i("..............:",""+name);
	            
	            mActivities = listNameTask.toArray(new String[listNameTask.size()]);
	        	
	            xVals.add(mActivities[i]);
	            
	        	//xVals.add(mParties[i % mParties.length]);
	            
	            String namep = null;
	            namep = mActivities[i];
	            Log.i("..............:",""+namep);
	            Log.i("..............:",""+xVals);

	        }
    
     
  // TASKS

     
	        PieDataSet dataSet = new PieDataSet(yVals1, " ");
	        dataSet.setSliceSpace(3f);
	        dataSet.setSelectionShift(5f);

	        // add a lot of colors
	        
	        final int COLOR_GREEN = Color.parseColor("#62c51a");
	        final int COLOR_DARK_GREEN = Color.parseColor("#38610B");
	    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
	    	final int COLOR_BLUE = Color.parseColor("#23bae9");
	    	final int COLOR_DARK_BLUE = Color.parseColor("#4074ED");
	    	final int COLOR_YELLOW = Color.parseColor("#E6E32F");
	    	final int COLOR_CYAN = Color.parseColor("#40EDD9");
	    	final int COLOR_PURPLE = Color.parseColor("#AC58FA");

	        
	     // Color of each Pie Chart Sections
	        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, COLOR_DARK_GREEN, Color.RED, COLOR_CYAN,
	        		COLOR_ORANGE, COLOR_YELLOW, COLOR_DARK_BLUE, COLOR_PURPLE};

	        dataSet.setColors(colors);

	        PieData data = new PieData(xVals, dataSet);
	       // data.setValueFormatter(new PercentFormatter());
	        data.setValueTextSize(0f);
	        data.setValueTextColor(Color.WHITE);
	        mChart.setData(data);

	        // undo all highlights
	        mChart.highlightValues(null);

	        mChart.invalidate();
	        
	        listNameTask.clear();
	        listHoursTask.clear();
	}
	
	// CHART GRAPH ACTIVITIES
	private void pieChartActivities(){
		/* *** GRAPH CHART PRESENTATION *** */	
	    
		 mChart = (PieChart) findViewById(R.id.pieChartActivities);
	        mChart.setUsePercentValues(true);
	        mChart.setDescription("");
	        mChart.setNoDataText("No data");
	        mChart.setNoDataTextDescription("");
	        
	        mChart.setDragDecelerationFrictionCoef(0.95f);
	        
	        mChart.setDrawSliceText(false);
	        mChart.setUsePercentValues(false);
	        mChart.setCenterTextWordWrapEnabled(true);


	        mChart.setDrawHoleEnabled(true);
	        mChart.setHoleColorTransparent(true);

	        mChart.setTransparentCircleColor(Color.WHITE);
	        mChart.setTransparentCircleAlpha(110);
	        
	        mChart.setHoleRadius(58f);
	        mChart.setTransparentCircleRadius(61f);

	        mChart.setDrawCenterText(true);   

	        mChart.setRotationAngle(0);
	        // enable rotation of the chart by touch
	        mChart.setRotationEnabled(true);

	        // mChart.setUnit(" ");
	        // mChart.setDrawUnitsInChart(true);

	        // add a selection listener
	       // mChart.setOnChartValueSelectedListener(this);

	        mChart.setCenterText("");
	        
	        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
	        // mChart.spin(2000, 0, 360);

	        Legend l = mChart.getLegend();
	        l.setPosition(LegendPosition.BELOW_CHART_LEFT);
	        l.setWordWrapEnabled(true);
	        l.setDirection(LegendDirection.LEFT_TO_RIGHT);
	        l.setXEntrySpace(5f);
	        l.setYEntrySpace(0f);
	        l.setYOffset(0f);
	        l.setXOffset(0f);
	       
	        
	        ArrayList<com.github.mikephil.charting.data.Entry> yVals1 = new ArrayList<com.github.mikephil.charting.data.Entry>();

	        // IMPORTANT: In a PieChart, no values (Entry) should have the same
	        // xIndex (even if from different DataSets), since no values can be
	        // drawn above each other.
	        for (int i = 0; i < listHoursActivities.size(); i++) {
	        	
	        	 // get hours
	            
	            String hour = null;
	 	        Float[] mHours = new Float[listHoursActivities.size()];
	 	        
	            hour = listHoursActivities.get(i);
	            Log.i("..............:",""+hour);

	            mHours[i] = Float.parseFloat(hour);
	            
	            Float hourF =  mHours[i];
	             
	            yVals1.add( new com.github.mikephil.charting.data.Entry ( (float) hourF, i));
	            
	            Log.i("..............:",""+yVals1);

	        }
	        
	        
	        ArrayList<String> xVals = new ArrayList<String>();

	        for (int i = 0; i < listNameActivities.size(); i++){
	        	
	        	// get name of project
	        	String name = null;
		 		String[] mActivities = new String[] {};
		 		 
	            name = listNameActivities.get(i);
	            Log.i("..............:",""+name);
	            
	            mActivities = listNameActivities.toArray(new String[listNameActivities.size()]);
	        	
	            xVals.add(mActivities[i]);
	            
	        	//xVals.add(mParties[i % mParties.length]);
	            
	            String namep = null;
	            namep = mActivities[i];
	            Log.i("..............:",""+namep);
	            Log.i("..............:",""+xVals);

	        }
    
     
  // ACTIVITIES

     
	        PieDataSet dataSet = new PieDataSet(yVals1, " ");
	        dataSet.setSliceSpace(3f);
	        dataSet.setSelectionShift(5f);

	        // add a lot of colors
	        
	        final int COLOR_GREEN = Color.parseColor("#62c51a");
	        final int COLOR_DARK_GREEN = Color.parseColor("#38610B");
	    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
	    	final int COLOR_BLUE = Color.parseColor("#23bae9");
	    	final int COLOR_DARK_BLUE = Color.parseColor("#4074ED");
	    	final int COLOR_YELLOW = Color.parseColor("#E6E32F");
	    	final int COLOR_CYAN = Color.parseColor("#40EDD9");
	    	final int COLOR_PURPLE = Color.parseColor("#AC58FA");

	        
	     // Color of each Pie Chart Sections
	        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, COLOR_DARK_GREEN, Color.RED, COLOR_CYAN,
	        		COLOR_ORANGE, COLOR_YELLOW, COLOR_DARK_BLUE, COLOR_PURPLE};

	        dataSet.setColors(colors);

	        PieData data = new PieData(xVals, dataSet);
	       // data.setValueFormatter(new PercentFormatter());
	        data.setValueTextSize(0f);
	        data.setValueTextColor(Color.WHITE);
	        mChart.setData(data);

	        // undo all highlights
	        mChart.highlightValues(null);

	        mChart.invalidate();
	        
	        listNameActivities.clear();
	        listHoursActivities.clear();
	}
	
	
	
	
	
	
	// CHART GRAPH LOCATIONS
	private void pieChartLocations(){
		/* *** GRAPH CHART PRESENTATION *** */	
	    
		 mChart = (PieChart) findViewById(R.id.pieChartLocations);
	        mChart.setUsePercentValues(true);
	        mChart.setDescription("");
	        mChart.setNoDataText("No data");
	        mChart.setNoDataTextDescription("");
	        
	        mChart.setDragDecelerationFrictionCoef(0.95f);
	        mChart.setDragDecelerationEnabled(true);
	        
	        mChart.setDrawSliceText(false);
	        mChart.setUsePercentValues(false);
	        mChart.setCenterTextWordWrapEnabled(true);


	        mChart.setDrawHoleEnabled(true);
	        mChart.setHoleColorTransparent(true);

	        mChart.setTransparentCircleColor(Color.WHITE);
	        mChart.setTransparentCircleAlpha(110);
	        
	        mChart.setHoleRadius(58f);
	        mChart.setTransparentCircleRadius(61f);

	        mChart.setDrawCenterText(true);   

	        mChart.setRotationAngle(0);
	        // enable rotation of the chart by touch
	        mChart.setRotationEnabled(true);
	        
	        

	        // mChart.setUnit(" ");
	        // mChart.setDrawUnitsInChart(true);

	        // add a selection listener
	       // mChart.setOnChartValueSelectedListener(this);

	        mChart.setCenterText("");
	        
	        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
	        // mChart.spin(2000, 0, 360);

	        Legend l = mChart.getLegend();
	        l.setPosition(LegendPosition.BELOW_CHART_LEFT);
	        l.setWordWrapEnabled(true);
	        l.setDirection(LegendDirection.LEFT_TO_RIGHT);
	        l.setXEntrySpace(5f);
	        l.setYEntrySpace(0f);
	        l.setYOffset(0f);
	        l.setXOffset(0f);
	        
	        ArrayList<com.github.mikephil.charting.data.Entry> yVals1 = new ArrayList<com.github.mikephil.charting.data.Entry>();

	        // IMPORTANT: In a PieChart, no values (Entry) should have the same
	        // xIndex (even if from different DataSets), since no values can be
	        // drawn above each other.
	        for (int i = 0; i < listHoursLocations.size(); i++) {
	        	
	        	 // get hours
	            
	            String hour = null;
	 	        Float[] mHours = new Float[listHoursLocations.size()];
	 	        
	            hour = listHoursLocations.get(i);
	            Log.i("..............:",""+hour);

	            mHours[i] = Float.parseFloat(hour);
	            
	            Float hourF =  mHours[i];
	             
	            yVals1.add( new com.github.mikephil.charting.data.Entry ( (float) hourF, i));
	            
	            Log.i("..............:",""+yVals1);

	        }
	        
	        
	        ArrayList<String> xVals = new ArrayList<String>();

	        for (int i = 0; i < listNameLocations.size(); i++){
	        	
	        	// get name of project
	        	String name = null;
		 		String[] mLocations = new String[] {};
		 		 
	            name = listNameLocations.get(i);
	            Log.i("..............:",""+name);
	            
	            mLocations = listNameLocations.toArray(new String[listNameLocations.size()]);
	        	
	            xVals.add(mLocations[i]);
	            
	        	//xVals.add(mParties[i % mParties.length]);
	            
	            String namep = null;
	            namep = mLocations[i];
	            Log.i("..............:",""+namep);
	            Log.i("..............:",""+xVals);

	        }
   
    
 // LOCATIONS

    
	        PieDataSet dataSet = new PieDataSet(yVals1, " ");
	        dataSet.setSliceSpace(3f);
	        dataSet.setSelectionShift(5f);

	        // add a lot of colors
	        
	        final int COLOR_GREEN = Color.parseColor("#62c51a");
	        final int COLOR_DARK_GREEN = Color.parseColor("#38610B");
	    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
	    	final int COLOR_BLUE = Color.parseColor("#23bae9");
	    	final int COLOR_DARK_BLUE = Color.parseColor("#4074ED");
	    	final int COLOR_YELLOW = Color.parseColor("#E6E32F");
	    	final int COLOR_CYAN = Color.parseColor("#40EDD9");
	    	final int COLOR_PURPLE = Color.parseColor("#AC58FA");

	        
	     // Color of each Pie Chart Sections
	        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, COLOR_DARK_GREEN, Color.RED, COLOR_CYAN,
	        		COLOR_ORANGE, COLOR_YELLOW, COLOR_DARK_BLUE, COLOR_PURPLE};

	        dataSet.setColors(colors);

	        PieData data = new PieData(xVals, dataSet);
	       // data.setValueFormatter(new PercentFormatter());
	        data.setValueTextSize(0f);
	        data.setValueTextColor(Color.WHITE);
	        mChart.setData(data);

	        // undo all highlights
	        mChart.highlightValues(null);

	        mChart.invalidate();
	        
	        listNameLocations.clear();
	        listHoursLocations.clear();
	}

	/**
	 * Shows a Progress Dialog 
	 *  
	 * @param msg
	 */
	public void showProgressDialog(String msg) 
	{
		
		// check for existing progressDialog
		if (pDialog == null) {
			// create a progress Dialog
			pDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			pDialog.setIndeterminate(true);
			
			pDialog.setCancelable(false);
			pDialog.setCanceledOnTouchOutside(false);
			
			pDialog.setMessage(msg);

		}

		// now display it.
		pDialog.show();		
	}	
	
	
	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {
		
		if (pDialog != null) {
			pDialog.dismiss();
		}
		
		pDialog = null;
	}
	
}
