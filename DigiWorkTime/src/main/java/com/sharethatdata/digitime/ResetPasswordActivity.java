package com.sharethatdata.digitime;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.webservice.WSDataProvider;


/**
 * Created by miha on 9/13/2017.
 */

public class ResetPasswordActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    private String webservice_url = "";

    String PharmacyId;
    private Button btnSend, btnReset;
    EditText editText_login, editText_email;
    EditText editText_Pass1, editText_Pass2, editText_Code;

    private String login_text;
    private String email_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Globals = ((MyGlobals)getApplication());
        Globals.backgroundProcess();

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Set up the login form.
        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );

        String current_user = sharedPrefs.getString("prefUsername", "");
        String current_password = sharedPrefs.getString("prefUserPassword", "");
        boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
        PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        // get webservice url
        webservice_url = sharedPrefs.getString("prefWebServiceServer", "");

        myUser = current_user;
        myPass = current_password;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(webservice_url);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ResetPasswordActivity.this, ResetPasswordActivity.this);

        editText_login = (EditText) findViewById(R.id.login);
        editText_email = (EditText) findViewById(R.id.email);
        editText_Pass1 = (EditText) findViewById(R.id.password1);
        editText_Pass2 = (EditText) findViewById(R.id.password2);
        editText_Code = (EditText) findViewById(R.id.security_code);

        btnSend = (Button) findViewById(R.id.send_button);
        btnReset = (Button) findViewById(R.id.reset_button);

        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                login_text = editText_login.getText().toString();
                email_text = editText_email.getText().toString();

                boolean failFlag = false;
                if(editText_login.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_login.setError("Username is required!");
                }

                if(editText_email.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_email.setError("Email is required!");
                }

                if(failFlag == false){

                    boolean security_code = MyProvider.getSecurityCode(editText_login.getText().toString(), editText_email.getText().toString());

                    if(security_code){
                        Toast.makeText(ResetPasswordActivity.this, "You will receive email with security code.", Toast.LENGTH_LONG).show();

                        confirmPassword(editText_login.getText().toString(), editText_email.getText().toString());
                    }else{
                        Toast.makeText(ResetPasswordActivity.this, MyProvider.last_error, Toast.LENGTH_LONG).show();

                    }

                }
            }

        });
    }

    private void confirmPassword(String username, String email) {

        final String final_username = username;
        final String final_email = email;

        editText_login.setVisibility(View.GONE);
        editText_email.setVisibility(View.GONE);
        btnSend.setVisibility(View.GONE);

        editText_Pass1.setVisibility(View.VISIBLE);
        editText_Pass2.setVisibility(View.VISIBLE);
        editText_Code.setVisibility(View.VISIBLE);
        btnReset.setVisibility(View.VISIBLE);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean failFlag = false;
                if(editText_Pass1.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_Pass1.setError("Password is required!");
                }

                if(editText_Pass2.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_Pass2.setError("Password is required!");
                }
                if(editText_Code.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_Code.setError("Security Code is required!");
                }

                if(failFlag == false){

                    if(editText_Pass1.getText().toString().equals(editText_Pass2.getText().toString())){
                        boolean security_code = MyProvider.getApproveNewPassword(final_username, final_email, editText_Pass1.getText().toString(), editText_Code.getText().toString());

                        if(security_code){

                            SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
                            String password = sharedPrefs.getString("prefUserPassword", "");

                            SharedPreferences.Editor editor1 = sharedPrefs.edit();
                            editor1.putString("prefUserPassword",editText_Pass1.getText().toString());
                            editor1.commit();

                            Toast.makeText(ResetPasswordActivity.this, "Password successfully reset!", Toast.LENGTH_LONG).show();
                            finish();
                        }else{
                            String error_message = MyProvider.last_error;
                            Toast.makeText(ResetPasswordActivity.this, error_message, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

    }
}
