package com.sharethatdata.digitime.news;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by mihu__000 on 7/30/2015.
 */
public class NewsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList;
	
	// List News Status
	ArrayList<HashMap<String, String>> newsListStatus;
	
	ListView lv = null;
	SearchView searchView;
	
	private String myUser = "";
	private String myPass = "";

	ProgressDialog progressDialog;
	
	public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    
    ImageView ivIcon;
    TextView tvItemName;
    
    private ActionBar actionBar;
    
    String myUserID = "";
    
    String idNewsForStatus = "";
    
    private String error_message = "";
    
    ArrayList<HashMap<String, String>> keyArray = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);
        
        Globals = ((MyGlobals)getApplicationContext());
		Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);

		myUserID = Globals.getValue("idContact");

        
        lv = (ListView)findViewById(R.id.listView);

        //new LoadListNews().execute();
        new LoadListNewsStatus().execute();
        

        /*tvItemName.setText(getArguments().getString(ITEM_NAME));
        ivIcon.setImageDrawable(rootView.getResources().getDrawable(
                    getArguments().getInt(IMAGE_RESOURCE_ID)));*/
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				// id for detail news
				HashMap<String, String> itemsList = (HashMap<String, String>) newsListStatus.get(position);
				 String itemId = (String) itemsList.get("id_news");
				 String itemIdStatus = (String) itemsList.get("id");
				 
				 Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
				 intent.putExtra("ID", itemIdStatus);
				 startActivity(intent);
				
				
				 idNewsForStatus = itemIdStatus;
				 new UpdateTaskNews().execute();
				 
				  Toast.makeText(getApplicationContext(),
					      "Click List Item Number " + itemId, Toast.LENGTH_SHORT)
					      .show();
				
			}
		});

        //return rootView;
    }
    

    
    @Override
    public void onResume(){
    	Log.d("TAG", "onResume News");
    	super.onResume();  	
	    new LoadListNewsStatus().execute();
    }

	static List<cNews> listNewsFiltered;
	static private ArrayList<cNews> arraylistNews;

	public static List<cNews> filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		listNewsFiltered = new ArrayList<cNews>();
		listNewsFiltered.clear();
		if (charText.length() == 0) {
			listNewsFiltered.addAll(arraylistNews);
		} else {
			for (cNews news : arraylistNews) {
				if (news.title.toLowerCase(Locale.getDefault()).contains(charText)
						|| news.created.toLowerCase(Locale.getDefault()).contains(charText)
						|| news.description.toLowerCase(Locale.getDefault()).contains(charText))
				{
					listNewsFiltered.add(news);
				}
			}
		}
		return listNewsFiltered;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// User pressed the search button

		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// User changed the text

		String text = newText.toString().toLowerCase(Locale.getDefault());
		List<cNews> filter_projects = filter(text.toString());
		newsListStatus.clear(); // <--- clear the list before add

		for(cNews entry : filter_projects)
		{

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("id",  String.valueOf(entry.id));
			//map.put("id_news", entry.id_news);
			//map.put("id_user", entry.id_user);
			//map.put("status", entry.status);
			map.put("title", entry.title);
			map.put("description", entry.description);
			map.put("created", entry.created);
			map.put("image", entry.picture);

			newsListStatus.add(map);
		}

		ListViewAdapter customAdapter = new ListViewAdapter(NewsActivity.this, R.layout.activity_news_listview_items, newsListStatus);
		lv.setAdapter(null);
		lv.setAdapter(customAdapter);

		return true;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// check manager
		String isManager = Globals.getValue("publisher");
		if (isManager == "yes")
		{
			getMenuInflater().inflate(R.menu.news, menu);
		}else{
			getMenuInflater().inflate(R.menu.search_news, menu);
		}

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(false);
		searchView.setQueryHint(getString(R.string.search_title_news));

		return true;
	}

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_create_news:
			 Intent intent = new Intent(this, NewsCreateActivity.class); startActivity(intent);
			 //new LoadListNews().execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }
    
    
    
    /**
	 * Background Async Task to Load all News by making HTTP Request
  	 * */
/*	class LoadListNews extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(getActivity());
         pDialog.setTitle("Loading News");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cNews> news = MyProvider.getNewsItems();
	    	 for(cNews entry : news)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.title);
                map.put("description", entry.description);
                map.put("created", entry.created);
                 
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);

            *//**
             * Updating parsed JSON data into ListView
             * *//*

            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), itemList,
                    R.layout.fragment_listview_items, new String[] { "id", "title","created","description"}, new int[] { R.id.textViewID,R.id.textTitle, R.id.textDateTime, R.id.textDescription});

           lv.setAdapter(adapter);
	    	
	    	ListViewAdapter customAdapter = new ListViewAdapter(getActivity(), R.layout.fragment_listview_items, itemList);
	        lv.setAdapter(customAdapter);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();   
	    }
	}*/
	
	
	
	/**
	 * Background Async Task to Load all News for status by making HTTP Request
  	 * */
	class LoadListNewsStatus extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
    		Globals = ((MyGlobals) getApplicationContext());
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
		    newsListStatus = new ArrayList<HashMap<String, String>>();
			arraylistNews = new ArrayList<cNews>();
	    	
	    	 //List<cNewsStatus> status = MyProvider.getNewsStatus(myUserID);
	    	 List<cNews> status = MyProvider.getNewsItems();
	    	 for(cNews entry : status)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  String.valueOf(entry.id));
                //map.put("id_news", entry.id_news);
                //map.put("id_user", entry.id_user);
                //map.put("status", entry.status);
                map.put("title", entry.title);
                map.put("description", entry.description);  
                map.put("created", entry.created);  
                map.put("image", entry.picture);

                // adding HashList to ArrayList
                newsListStatus.add(map);
	    	 }

			arraylistNews.addAll(status);

	    	 return "";
	    }

	    protected void onPostExecute(String result) {

	    	runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	/*for(HashMap<String, String> map1 : itemList){
	            		for (Entry<String, String> entry1 : map1.entrySet())
	           	        {
	            			for(HashMap<String, String> map2 : newsListStatus){
	            				for(Entry<String, String> entry2 : map2.entrySet()){
	            					if(entry1.getValue().equals(entry2.getValue())){
	            						System.out.println("Equal");
	            						keyArray.add(map1);
	            						keyArray.add(map2);

	            						System.out.println("Equal " + keyArray.toString());
	            						//keyArray.add(entry.getValue());
	            						
	            						
	            					}
	            				}
	            			}
	           	        }
	            	}*/
	            	
	            	SharedPreferences.Editor editor = getSharedPreferences("PREF_CHECK", 0).edit();
		   	   	     editor.putString("check", "news");
		   	   	     editor.commit();
	            	
	            	ListViewAdapter customAdapter = new ListViewAdapter(NewsActivity.this, R.layout.activity_news_listview_items, newsListStatus);
	    	        lv.setAdapter(customAdapter);
		            
	            }
	        });

			hideProgressDialog();
	    }
	}
	
	
	/** AsyncTask update status news  */
    private class UpdateTaskNews extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {	
			 
		      String id = idNewsForStatus;
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;

				   suc = MyProvider.updateNewsStatusRead(id);

        	  return suc;
        }

        protected void onPostExecute(final Boolean success) {
        	runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(getApplicationContext(), getString(R.string.text_error), error_message);
		    		}
					else
					{
						//Toast.makeText(getActivity(), "News update id " + idNewsForStatus, Toast.LENGTH_SHORT).show();
					}
				}
			});
 
	    }
    }


	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	private void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	private void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}
	
	
}
