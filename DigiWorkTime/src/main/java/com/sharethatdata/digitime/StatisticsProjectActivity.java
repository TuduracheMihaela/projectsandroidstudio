package com.sharethatdata.digitime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.sharethatdata.digitime.ListActivity.LoadList;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cTimeStats;
import com.sharethatdata.webservice.datamodel.cTimeStatsProject;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class StatisticsProjectActivity extends AppCompatActivity {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	TimeUtils MyTimeUtils = null;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
    // var pid and contact id
    private String pid = "";
    private String cid = "";
    
    ArrayList<HashMap<String, String>> userList;
	ArrayList<HashMap<String, String>> activityList;
	
	// MPAndroid for Pie Chart
	 private PieChart mChart;
	ArrayList<String> listNameActivities = new ArrayList<String>();
	ArrayList<String> listHoursActivities = new ArrayList<String>();
	
	ListView lv = null;
	ListView lv2 = null;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics_project);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		
		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(StatisticsProjectActivity.this, StatisticsProjectActivity.this);
		
		lv = (ListView) findViewById(R.id.listView1);
		lv2 = (ListView) findViewById(R.id.listView2);
		
		TextView titleText = (TextView) findViewById(R.id.title);
		titleText.setText("Users");
		TextView titleText2 = (TextView) findViewById(R.id.title2);
		titleText2.setText("Activities");
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String value_project = extras.getString("PID");
		    String value_contact = extras.getString("CID");
		    
		    pid = value_project;
		    cid = value_contact;
		    
		    if(cid.length() < 1){
		    	cid = myUserID; // default(user logged)
		    }
		    
		    System.out.println("value_contact: " + value_contact);
		    System.out.println("report_user_id: " + myUserID);

		}
		
		LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
   /*      pDialog = new ProgressDialog(ListActivity.this);
         pDialog.setMessage( getString(R.string.loading_alert_dialog) + ".");
         //"Loading " // "Please wait..."
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();*/
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
			
			Globals = ((MyGlobals)getApplication());
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
	    	 // load data from provider
	    	 userList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cTimeStatsProject> timeRecords = MyProvider.getTimeStatsProject( "project", cid, pid);
	    	 for(cTimeStatsProject entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.total_hours));
                 
                // adding HashList to ArrayList
                userList.add(map);

	    	 }
	    	
	    	 // load data from provider
	    	 activityList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 timeRecords = MyProvider.getTimeStatsProject( "", cid, pid);
	    	 for(cTimeStatsProject entry : timeRecords)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("hours", Integer.toString(entry.total_hours));
                 
                // adding HashList to ArrayList
                activityList.add(map);
                
                // list for graph chart
                listNameActivities.add(entry.name);
                listHoursActivities.add (Integer.toString(entry.total_hours));

	    	 }
	    	 
	    	 
	    	 
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;
	            	ListAdapter adapter2 = null;
	 	           	
	            	// total project
	            	int total = 0;
	            	int myNum = 0;
	            	for (HashMap<String, String> map : userList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        }
	            	
	                HashMap<String, String> hmap = new HashMap<String, String>();
	          		
	                hmap.put("hours",String.valueOf(total) );
	                hmap.put("name", "total");
	           	                
	                userList.add(hmap);
	            	
	            	// total activity
	            	total = 0;
	            	myNum = 0;
	            	for (HashMap<String, String> map : activityList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "hours")
	           	        	{
	           	        		
	           	        		try {
	           	        			myNum = Integer.parseInt(entry.getValue().toString());
	           	        		
	           	        			entry.setValue(String.valueOf(myNum));
	           	        			
	           	        		} catch(NumberFormatException nfe) {
	           	        			myNum = 0;
	           	        		}
	           	        		total = total + myNum;
	           	        	}
	           	        	           	        	
	           	        }
	            	
	                hmap = new HashMap<String, String>();
	          		
	            	hmap.put("hours", String.valueOf(total));
	                hmap.put("name", "total");
	           	                
	                activityList.add(hmap);
	                
	                
	            	adapter = new SimpleAdapter(
	                		StatisticsProjectActivity.this
	                		, userList
	                		, R.layout.sublistview_item, new String[] {"id", "name", "hours" },
                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
		        	            
	                
	                // updating listview
	                lv.setAdapter(adapter);
	               	 
	                
	            	adapter2 = new SimpleAdapter(
	                		StatisticsProjectActivity.this
	                		, activityList
	                		, R.layout.sublistview_item, new String[] {"id", "name", "hours" },
                            new int[] { R.id.textViewID, R.id.textViewName, R.id.textViewHours });
	        	            
                
	            	// updating listview
	            	lv2.setAdapter(adapter2);
	            	
	            	pieChartActivities();
	            	
	            	Utility.setListViewHeightBasedOnChildren(lv);
	            	Utility.setListViewHeightBasedOnChildren(lv2);
	            }
	        });
	  	      
	    }
	  

	}
	
	
	
	// CHART GRAPH ACTIVITIES
		private void pieChartActivities(){
			/* *** GRAPH CHART PRESENTATION *** */	
		    
			 mChart = (PieChart) findViewById(R.id.pieChartActivities);
		        mChart.setUsePercentValues(true);
		        mChart.setDescription("");
		        mChart.setNoDataText("No data");
		        mChart.setNoDataTextDescription("");
		        
		        mChart.setDragDecelerationFrictionCoef(0.95f);
		        
		        mChart.setDrawSliceText(false);
		        mChart.setUsePercentValues(false);
		        mChart.setCenterTextWordWrapEnabled(true);


		        mChart.setDrawHoleEnabled(true);
		        mChart.setHoleColorTransparent(true);

		        mChart.setTransparentCircleColor(Color.WHITE);
		        mChart.setTransparentCircleAlpha(110);
		        
		        mChart.setHoleRadius(58f);
		        mChart.setTransparentCircleRadius(61f);

		        mChart.setDrawCenterText(true);   

		        mChart.setRotationAngle(0);
		        // enable rotation of the chart by touch
		        mChart.setRotationEnabled(true);

		        // mChart.setUnit(" ");
		        // mChart.setDrawUnitsInChart(true);

		        // add a selection listener
		       // mChart.setOnChartValueSelectedListener(this);

		        mChart.setCenterText("");
		        
		        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
		        // mChart.spin(2000, 0, 360);

		        Legend l = mChart.getLegend();
		        l.setPosition(LegendPosition.RIGHT_OF_CHART);
		        l.setXEntrySpace(7f);
		        l.setYEntrySpace(0f);
		        l.setYOffset(0f);
		        
		        ArrayList<com.github.mikephil.charting.data.Entry> yVals1 = new ArrayList<com.github.mikephil.charting.data.Entry>();

		        // IMPORTANT: In a PieChart, no values (Entry) should have the same
		        // xIndex (even if from different DataSets), since no values can be
		        // drawn above each other.
		        for (int i = 0; i < listHoursActivities.size(); i++) {
		        	
		        	 // get hours
		            
		            String hour = null;
		 	        Float[] mHours = new Float[listHoursActivities.size()];
		 	        
		            hour = listHoursActivities.get(i);
		            Log.i("..............:",""+hour);

		            mHours[i] = Float.parseFloat(hour);
		            
		            Float hourF =  mHours[i];
		             
		            yVals1.add( new com.github.mikephil.charting.data.Entry ( (float) hourF, i));
		            
		            Log.i("..............:",""+yVals1);

		        }
		        
		        
		        ArrayList<String> xVals = new ArrayList<String>();

		        for (int i = 0; i < listNameActivities.size(); i++){
		        	
		        	// get name of project
		        	String name = null;
			 		String[] mActivities = new String[] {};
			 		 
		            name = listNameActivities.get(i);
		            Log.i("..............:",""+name);
		            
		            mActivities = listNameActivities.toArray(new String[listNameActivities.size()]);
		        	
		            xVals.add(mActivities[i]);

		        }
	    
	     
	  // PROJECTS

	     
		        PieDataSet dataSet = new PieDataSet(yVals1, "Activities Results");
		        dataSet.setSliceSpace(3f);
		        dataSet.setSelectionShift(5f);

		        // add a lot of colors
		        
		        final int COLOR_GREEN = Color.parseColor("#62c51a");
		    	final int COLOR_ORANGE = Color.parseColor("#ff6c0a");
		    	final int COLOR_BLUE = Color.parseColor("#23bae9");
		        
		     // Color of each Pie Chart Sections
		        int[] colors = { COLOR_BLUE, Color.MAGENTA, COLOR_GREEN, Color.DKGRAY, Color.RED,
		        		COLOR_ORANGE };

		        dataSet.setColors(colors);

		        PieData data = new PieData(xVals, dataSet);
		       // data.setValueFormatter(new PercentFormatter());
		        data.setValueTextSize(0f);
		        data.setValueTextColor(Color.WHITE);
		        mChart.setData(data);

		        // undo all highlights
		        mChart.highlightValues(null);

		        mChart.invalidate();
		        
		        listNameActivities.clear();
		        listHoursActivities.clear();
		}

}
