package com.sharethatdata.digitime.users;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

public class UsersListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

	ProgressDialog progressDialog;

	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> userList;
	
	ListView lv = null;
	SearchView searchView;
	
	private String myUser = "";
    private String myPass = "";
    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_users);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    lv = (ListView) findViewById(R.id.listView);
	
	    lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String idUser = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
				
			    // start detail activity
				Intent intent = new Intent(getApplicationContext(), UserProfileDetail.class); 
				intent.putExtra("idUser", idUser);
				startActivityForResult(intent, 1);
				
			}

		});	
	    
	    new LoadList().execute();
    }
    
 // after edit or delete, load data again
 	@Override 
 	protected void onActivityResult(int requestCode, int resultCode, Intent data) {     
 	  super.onActivityResult(requestCode, resultCode, data); 
	   	  if (resultCode == Activity.RESULT_OK) { 
		    	  new LoadList().execute();
	   	  }
 	}

	static List<cContact> listContactFiltered;
	static private ArrayList<cContact> arraylistContacts;

	public static List<cContact> filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		listContactFiltered = new ArrayList<cContact>();
		listContactFiltered.clear();
		if (charText.length() == 0) {
			listContactFiltered.addAll(arraylistContacts);
		} else {
			for (cContact contact : arraylistContacts) {
				if (contact.name.toLowerCase(Locale.getDefault()).contains(charText)
						|| contact.function.toLowerCase(Locale.getDefault()).contains(charText)
						|| contact.description.toLowerCase(Locale.getDefault()).contains(charText))
				{
					listContactFiltered.add(contact);
				}
			}
		}
		return listContactFiltered;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// User pressed the search button

		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// User changed the text

		String text = newText.toString().toLowerCase(Locale.getDefault());
		List<cContact> filter_contacts = filter(text.toString());
		userList.clear(); // <--- clear the list before add

		for(cContact entry : filter_contacts)
		{

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("id", entry.id);
			map.put("name", entry.name);
			map.put("subtitle", entry.function);
			map.put("status", entry.status_id);
			map.put("image_id", entry.image_id);

			userList.add(map);
		}

		UsersAdapter adapter = new UsersAdapter(UsersListActivity.this, R.layout.activity_list_users_items, userList, "users_list");
		lv.setAdapter(null);
		lv.setAdapter(adapter);

		return true;
	}
    
    public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:   
			   finish();
			   		super.onBackPressed();
		        return true;
		   case R.id.action_new: 
	            Intent intent = new Intent(this, UserProfileDetailEditActivity.class); 
	            startActivityForResult(intent, 3);
	            return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// check manager
		String isManager = Globals.getValue("publisher");
		if (isManager == "yes")
		{
			getMenuInflater().inflate(R.menu.contacts, menu);
		}else{
			getMenuInflater().inflate(R.menu.search_contacts, menu);
		}

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(false);
		searchView.setQueryHint(getString(R.string.search_title_contacts));

		return true;
	}
    

	class LoadList extends AsyncTask<String, String, String> {

     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         showProgressDialog(getResources().getString(R.string.loading_alert_dialog));
     }

	    protected String doInBackground(String... args) {
	    	
	    	userList = new ArrayList<HashMap<String, String>>();
			arraylistContacts = new ArrayList<cContact>();

	    		List<cContact> List = MyProvider.getContactsWithThumbImage();
	    	
	    		for(cContact entry : List)
             	{
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("id", entry.id);
	                map.put("name", entry.name);
	                map.put("subtitle", entry.function);
	                map.put("status", entry.status_id);
	                map.put("image_id", entry.image_id);
	                 
	                userList.add(map);
	             }

			arraylistContacts.addAll(List);

	    	return "";
	    }

	    protected void onPostExecute(String file_url) {
	        hideProgressDialog();
	        runOnUiThread(new Runnable() {
	            public void run() {
					lv.setAdapter(new UsersAdapter(UsersListActivity.this, R.layout.activity_list_users_items, userList, "users_list"));
	            }
	        });
	         
	    }

	}


	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	public void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

}
