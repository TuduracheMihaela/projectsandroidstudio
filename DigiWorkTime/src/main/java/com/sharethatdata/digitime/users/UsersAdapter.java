package com.sharethatdata.digitime.users;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.WSDataProvider;

import java.util.ArrayList;
import java.util.HashMap;

public class UsersAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	protected ListView mListView;
 	
	WSDataProvider MyProvider = null;
	private String list_type = "";

 	 public UsersAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter, String list_type)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        this.list_type = list_type;
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        
        if(list_type.equals("chat_users_list")){
        
	        holder.id = (TextView)view.findViewById(R.id.textViewID);
	        holder.titleTextView = (TextView)view.findViewById(R.id.textTitle);
	        holder.functionTextView = (TextView)view.findViewById(R.id.textFunction); 
	        //holder.imageView = (ImageView)view.findViewById(R.id.imgUser);
        }
        else if(list_type.equals("users_list")){
        	holder.id = (TextView)view.findViewById(R.id.textViewID);
	        holder.titleTextView = (TextView)view.findViewById(R.id.textViewName);
	        holder.functionTextView = (TextView)view.findViewById(R.id.textViewFunc); 
	        holder.statusTextView = (TextView)view.findViewById(R.id.textViewStatus); 
	        holder.imageView = (ImageView)view.findViewById(R.id.iv_img); 
        }
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
    if(list_type.equals("chat_users_list")){
    	holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
        holder.titleTextView.setText(Html.fromHtml(hashmap_Current.get("name")));   
        holder.functionTextView.setText(Html.fromHtml(hashmap_Current.get("function"))); 
    }
    else if(list_type.equals("users_list"))
    {
    	holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
        holder.titleTextView.setText(Html.fromHtml(hashmap_Current.get("name")));   
        holder.functionTextView.setText(Html.fromHtml(hashmap_Current.get("subtitle")));
        holder.statusTextView.setText(Html.fromHtml(hashmap_Current.get("status")));
    }
     
     
     Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image_id"));
	  if (d != null)
	  {
		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
		  holder.imageView.setImageBitmap(scaled);
		  				  
	  }else{
		  holder.imageView.setImageResource(R.drawable.person_small);
	  }
    return view; 
}



	final class ViewHolder {
		TextView id;
		TextView titleTextView, functionTextView, statusTextView;
        ImageView imageView;
	}
	
	 private Bitmap ConvertByteArrayToBitmap(String b)
		{
		 Bitmap decodedByte = null;
		 	if(b.isEmpty() || b.contentEquals(""))
		 	{
		 		decodedByte = null;
		 	}else{
				byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
				decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		 	}
			return decodedByte;		
		}

		
}
