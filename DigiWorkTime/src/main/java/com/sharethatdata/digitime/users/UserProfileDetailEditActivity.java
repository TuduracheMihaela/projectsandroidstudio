package com.sharethatdata.digitime.users;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cDepartment;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cWorkLocation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class UserProfileDetailEditActivity extends AppCompatActivity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;
	String myUser = "";
	String myPass = "";
	private String error_message = "";
	
	private static ProgressDialog pDialog;

	ArrayList<HashMap<String, String>> departmentsList;
	ArrayList<HashMap<String, String>> companysList;
	ArrayList<HashMap<String, String>> locationsList;

	List<String> locationkeyArray =  new ArrayList<String>();
	List<String> companykeyArray =  new ArrayList<String>();
	List<String> departmentkeyArray =  new ArrayList<String>();

	private String contactid = "";
	private String profileid = "";
	
	ImageView imageView1;
	Button btnCreateImage;
	Button btnSearchImage;
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;
	String myImageData = "";

	boolean permission_camera = false;
	
	///////////////////////////////
	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;
	// For zoom image /////////////////////////
	// Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_user_edit);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		
		String userId = "";
		if (savedInstanceState == null) {
		    Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	userId= "";
		    } else {
		    	userId= extras.getString("idUser");
		    	profileid=extras.getString("idProfile");
		    }
		} else {
			userId= (String) savedInstanceState.getSerializable("idUser");
			profileid=(String) savedInstanceState.getSerializable("idProfile");
		}
		
		contactid = userId;
		System.out.print(contactid);
		
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);			
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
	    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
		imageView1 = (ImageView) findViewById(R.id.imageView1);
		imageView1.setVisibility(View.GONE);
	    btnCreateImage = (Button) findViewById(R.id.pictureFromPhone);
	    btnSearchImage = (Button) findViewById(R.id.pictureCamera);
	    TextView textView1 = (TextView) findViewById(R.id.textView1);
	    EditText username = (EditText) findViewById(R.id.textUserName);
	    EditText birthdate = (EditText) findViewById(R.id.textBirthdate);
	    EditText startdate = (EditText) findViewById(R.id.textStartdate);
	    Button buttonEditOrCreate = (Button) findViewById(R.id.btnSubmit);
	    
	    if(!userId.equals("")){
	    	// edit mode
	    	username.setEnabled(false);
	    	textView1.setText(R.string.editButtonText);
	    	buttonEditOrCreate.setText(R.string.editButtonText);
	    	setTitle(getResources().getString(R.string.title_activity_detail_profile_edit));
	    }else{
	    	// create mode
	    	textView1.setText(R.string.createButtonText);
	    	buttonEditOrCreate.setText(R.string.createButtonText);
	    	setTitle(getResources().getString(R.string.title_activity_detail_profile_create));
	    }

	    birthdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDateDialogBirthdate();
			}
		});

	    startdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDateDialogStartdate();
			}
		});

		new LoadDataTask(userId).execute();

		buttonEditOrCreate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 new RegisterTask().execute();
			}
		});
		
		if(savedInstanceState != null)
	    {
			  EditText SurNameView = (EditText) findViewById(R.id.textSurName);
			  SurNameView.setText(savedInstanceState.getString("SurNameView"));
			  EditText LastNameView = (EditText) findViewById(R.id.textLastName);
			  LastNameView.setText(savedInstanceState.getString("LastNameView"));
			  EditText UserNameView = (EditText) findViewById(R.id.textUserName);
			  UserNameView.setText(savedInstanceState.getString("UserNameView"));
			  
			  EditText functionView = (EditText) findViewById(R.id.textFunction);
			  functionView.setText(savedInstanceState.getString("functionView"));
			  EditText emailView = (EditText) findViewById(R.id.textEmail);
			  emailView.setText(savedInstanceState.getString("emailView"));
			  EditText phoneView = (EditText) findViewById(R.id.textPhone);
			  phoneView.setText(savedInstanceState.getString("phoneView"));

			  EditText descriptionView = (EditText) findViewById(R.id.textDescription);
			  descriptionView.setText(String.valueOf(savedInstanceState.getString("descriptionView")));

	    }else{
	    	new LoadDataTask(userId).execute();
	    }


		btnCreateImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

				if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
						== PackageManager.PERMISSION_DENIED
						&&
						ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
								== PackageManager.PERMISSION_DENIED  ){
					ActivityCompat.requestPermissions(UserProfileDetailEditActivity.this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_IMAGE_CAPTURE);
				}else{
					permission_camera = true;
				}

				if(permission_camera){
					StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
					StrictMode.setVmPolicy(builder.build());

					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
						// Create the File where the photo should go
						File photoFile = null;
						try {
							photoFile = createImageFile();
						} catch (IOException ex) {
							Log.d("IMAGE onCreate", ex.toString());
						}

						if (photoFile != null) {
							takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(photoFile));
							takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
							startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
						}
						//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
					}
				}
				else {
					Toast.makeText(UserProfileDetailEditActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   
			   finish();
			   
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    
	    EditText SurNameView = (EditText) findViewById(R.id.textSurName);
	    EditText LastNameView = (EditText) findViewById(R.id.textLastName);
	    EditText UserNameView = (EditText) findViewById(R.id.textUserName);
		  
	    EditText functionView = (EditText) findViewById(R.id.textFunction);
	    EditText emailView = (EditText) findViewById(R.id.textEmail);
	    EditText phoneView = (EditText) findViewById(R.id.textPhone);

	    EditText descriptionView = (EditText) findViewById(R.id.textDescription);

	    outState.putString("SurNameView", SurNameView.getText().toString());
	    outState.putString("LastNameView", LastNameView.getText().toString());
	    outState.putString("UserNameView", UserNameView.getText().toString());
	    outState.putString("functionView", functionView.getText().toString());
	    outState.putString("emailView", emailView.getText().toString());
	    outState.putString("phoneView", phoneView.getText().toString());
	    outState.putString("descriptionView", descriptionView.getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
	    super.onRestoreInstanceState(savedInstanceState);
	 // Save the values you need from your text view into "outState"-object
	      EditText SurNameView = (EditText) findViewById(R.id.textSurName);
		  SurNameView.setText(savedInstanceState.getString("SurNameView"));
		  EditText LastNameView = (EditText) findViewById(R.id.textLastName);
		  LastNameView.setText(savedInstanceState.getString("LastNameView"));
		  EditText UserNameView = (EditText) findViewById(R.id.textUserName);
		  UserNameView.setText(savedInstanceState.getString("UserNameView"));
		  
		  EditText functionView = (EditText) findViewById(R.id.textFunction);
		  functionView.setText(savedInstanceState.getString("functionView"));
		  EditText emailView = (EditText) findViewById(R.id.textEmail);
		  emailView.setText(savedInstanceState.getString("emailView"));
		  EditText phoneView = (EditText) findViewById(R.id.textPhone);
		  phoneView.setText(savedInstanceState.getString("phoneView"));

		  EditText descriptionView = (EditText) findViewById(R.id.textDescription);
		  descriptionView.setText(String.valueOf(savedInstanceState.getString("descriptionView")));
	}


	private void showDateDialogBirthdate() {
		LayoutInflater mLayoutInflater = getLayoutInflater();
		View mCustomDatePicker = mLayoutInflater.inflate(R.layout.custom_date_picker, null);

		final DatePicker mDatePicker = (DatePicker) mCustomDatePicker.findViewById(R.id.datePicker);
		mDatePicker.setMaxDate(Calendar.getInstance().getTime().getTime());

		final EditText mBirthdayEdit = (EditText) findViewById(R.id.textBirthdate);

		final Calendar mCalendar = Calendar.getInstance();
		final SimpleDateFormat mFormatDate = new SimpleDateFormat("yyyy-MM-dd");

		AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
		mDialog.setView(mCustomDatePicker);
		mDialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mCalendar.set(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
				mBirthdayEdit.setText(mFormatDate.format(mCalendar.getTime()));

			}
		}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		mDialog.create();
		mDialog.show();
	}


	private void showDateDialogStartdate() {
		LayoutInflater mLayoutInflater = getLayoutInflater();
		View mCustomDatePicker = mLayoutInflater.inflate(R.layout.custom_date_picker, null);

		final DatePicker mDatePicker = (DatePicker) mCustomDatePicker.findViewById(R.id.datePicker);
		mDatePicker.setMaxDate(Calendar.getInstance().getTime().getTime());

		final EditText mStartdateEdit = (EditText) findViewById(R.id.textStartdate);

		final Calendar mCalendar = Calendar.getInstance();
		final SimpleDateFormat mFormatDate = new SimpleDateFormat("yyyy-MM-dd");

		AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
		mDialog.setView(mCustomDatePicker);
		mDialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mCalendar.set(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
				mStartdateEdit.setText(mFormatDate.format(mCalendar.getTime()));

			}
		}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		mDialog.create();
		mDialog.show();
	}

	
	private class LoadDataTask extends AsyncTask<String, String, cContact> {
		  String myID = "";
		  
		  public LoadDataTask(String id) {
		     myID = id;
		  }

		  protected cContact doInBackground(String... urls) {
			  			  
			  cContact myEmployee = MyProvider.getContact(myID);
			  
			  if (myEmployee != null)
			  {
				  if (!Objects.equals(myEmployee.image_id, "0"))
				  {
					  //myImageData = MyRoboProvider.getImage(myEmployee.image_id);
					  myImageData = MyProvider.getImage(myEmployee.image_id);
				  }
			  }

			  departmentsList = new ArrayList<HashMap<String, String>>();
			  List<cDepartment> departList = MyProvider.getDepartments();
			  for(cDepartment entry : departList) {
				  // creating new HashMap
				  HashMap<String, String> map = new HashMap<String, String>();

				  map.put("id", entry.ID);
				  map.put("name", entry.name);

				  // adding HashList to ArrayList
				  departmentsList.add(map);
			  }

			  companysList = new ArrayList<HashMap<String, String>>();
			  List<cCompany> companyList = MyProvider.getCompanys();
			  for(cCompany entry : companyList)
			  {
				  // creating new HashMap
				  HashMap<String, String> map = new HashMap<String, String>();

				  map.put("id",  String.valueOf(entry.id));
				  map.put("name", entry.name);

				  // adding HashList to ArrayList
				  companysList.add(map);
			  }

			  locationsList = new ArrayList<HashMap<String, String>>();
			  List<cWorkLocation> locationList = MyProvider.getWorkLocations();
			  for(cWorkLocation entry : locationList)
			  {
				  // creating new HashMap
				  HashMap<String, String> map = new HashMap<String, String>();

				  map.put("id",  String.valueOf(entry.ID));
				  map.put("name", entry.name);

				  // adding HashList to ArrayList
				  locationsList.add(map);
			  }
			
			  return myEmployee;
		  }

		  protected void onPostExecute(cContact result) {

			  List<String> departmentArray =  new ArrayList<String>();
			  List<String> companyArray =  new ArrayList<String>();
			  List<String> locationArray =  new ArrayList<String>();

			  departmentkeyArray.clear();
			  companykeyArray.clear();
			  locationkeyArray.clear();

			  for (HashMap<String, String> map : departmentsList)
				  for (Map.Entry<String, String> entry : map.entrySet())
				  {
					  if (entry.getKey() == "id") departmentkeyArray.add(entry.getValue());
					  if (entry.getKey() == "name") departmentArray.add(entry.getValue());
				  }

			  for (HashMap<String, String> map : companysList)
				  for (Map.Entry<String, String> entry : map.entrySet())
				  {
					  if (entry.getKey() == "id") companykeyArray.add(entry.getValue());
					  if (entry.getKey() == "name") companyArray.add(entry.getValue());
				  }

			  for (HashMap<String, String> map : locationsList)
				  for (Map.Entry<String, String> entry : map.entrySet())
				  {
					  if (entry.getKey() == "id") locationkeyArray.add(entry.getValue());
					  if (entry.getKey() == "name") locationArray.add(entry.getValue());
				  }


			  final ArrayAdapter<String> departments_adapter = new ArrayAdapter<String>(UserProfileDetailEditActivity.this, android.R.layout.simple_spinner_item, departmentArray);
			  departments_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			  final Spinner sDepartmentPlaces = (Spinner) findViewById(R.id.spinnerDepartmanet);
			  sDepartmentPlaces.setAdapter(departments_adapter);

			  final ArrayAdapter<String> company_adapter = new ArrayAdapter<String>(UserProfileDetailEditActivity.this, android.R.layout.simple_spinner_item, companyArray);
			  company_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			  final Spinner sCompanyPlaces = (Spinner) findViewById(R.id.spinnerCompany);
			  sCompanyPlaces.setAdapter(company_adapter);

			  final ArrayAdapter<String> location_adapter = new ArrayAdapter<String>(UserProfileDetailEditActivity.this, android.R.layout.simple_spinner_item, locationArray);
			  location_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			  final Spinner sLocationPlaces = (Spinner) findViewById(R.id.spinnerLocation);
			  sLocationPlaces.setAdapter(location_adapter);
			  
			  if (result != null)
			  {
				  if(!Objects.equals(result.id, "0")){
					  TextView SurNameView = (TextView) findViewById(R.id.textSurName);
					  SurNameView.setText(result.surname);
					  TextView LastNameView = (TextView) findViewById(R.id.textLastName);
					  LastNameView.setText(result.lastname);
					  TextView UserNameView = (TextView) findViewById(R.id.textUserName);
					  UserNameView.setText(result.username);
					  TextView Password1View = (TextView) findViewById(R.id.textPassword1);
					  Password1View.setText(result.password);
					  TextView Password2View = (TextView) findViewById(R.id.textPassword2);
					  Password2View.setText(result.password);


					  TextView functionView = (TextView) findViewById(R.id.textJobTitle);
					  functionView.setText(result.function);
					  TextView phoneView = (TextView) findViewById(R.id.textPhone);
					  phoneView.setText(result.phone);
					  TextView phoneshortView = (TextView) findViewById(R.id.textPhoneShort);
					  phoneshortView.setText(result.phone_short);
					  TextView emailView = (TextView) findViewById(R.id.textEmail);
					  emailView.setText(result.email);


					  TextView cityView = (TextView) findViewById(R.id.textCity);
					  cityView.setText(result.city);
					  TextView hobbyView = (TextView) findViewById(R.id.textHobbies);
					  hobbyView.setText(result.hobby);
					  TextView languageView = (TextView) findViewById(R.id.textLanguages);
					  languageView.setText(result.languages);
					  TextView educationView = (TextView) findViewById(R.id.textEducation);
					  educationView.setText(result.education_level);
					  TextView diplomaView = (TextView) findViewById(R.id.textDiploma);
					  diplomaView.setText(result.diplomas);
					  TextView skillsView = (TextView) findViewById(R.id.textSkills);
					  skillsView.setText(result.skills);
					  TextView licenseView = (TextView) findViewById(R.id.textLicenses);
					  licenseView.setText(result.driver_license);

					  TextView birthdateView = (TextView) findViewById(R.id.textBirthdate);
					  birthdateView.setText(result.birth_date);
					  TextView startdateView = (TextView) findViewById(R.id.textStartdate);
					  startdateView.setText(result.date_in_service);
					  TextView presentView = (TextView) findViewById(R.id.textPresent);
					  presentView.setText(result.days_present);
					  TextView absentView = (TextView) findViewById(R.id.textAbsent);
					  absentView.setText(result.days_not_present);

					  TextView maintask1View = (TextView) findViewById(R.id.textMaintask1);
					  maintask1View.setText(result.main_task_1);
					  TextView maintask2View = (TextView) findViewById(R.id.textMaintask2);
					  maintask2View.setText(result.main_task_2);
					  TextView maintask3View = (TextView) findViewById(R.id.textMaintask3);
					  maintask3View.setText(result.main_task_3);
					  TextView maintask4View = (TextView) findViewById(R.id.textMaintask4);
					  maintask4View.setText(result.main_task_4);
					  TextView maintask5View = (TextView) findViewById(R.id.textMaintask5);
					  maintask5View.setText(result.main_task_5);


					  CheckBox BHV = (CheckBox) findViewById(R.id.checkboxBHV);
					  if(Objects.equals(result.BHV, "1")) BHV.setChecked(true);

					  CheckBox AED = (CheckBox) findViewById(R.id.checkboxAED);
					  if(Objects.equals(result.AED, "1")) AED.setChecked(true);

					  CheckBox VCA = (CheckBox) findViewById(R.id.checkboxVCA);
					  if(Objects.equals(result.VCA, "1")) VCA.setChecked(true);

					  CheckBox Keyholder = (CheckBox) findViewById(R.id.checkboxKeyholder);
					  if(Objects.equals(result.keyholder, "1")) Keyholder.setChecked(true);

					  CheckBox BasicConfidant = (CheckBox) findViewById(R.id.checkboxBasicConfidant);
					  if(Objects.equals(result.BC, "1")) BasicConfidant.setChecked(true);

					  if (result.department != "") {
						  int spinnerPositionDepartment = departments_adapter.getPosition(result.department);
						  sDepartmentPlaces.setSelection(spinnerPositionDepartment);
					  }
					  if (result.work_location != "") {
						  int spinnerPositionWorklocation = location_adapter.getPosition(result.work_location);
						  sLocationPlaces.setSelection(spinnerPositionWorklocation);
					  }
					  if (result.company != "") {
						  int spinnerPositionCompany = company_adapter.getPosition(result.company);
						  sCompanyPlaces.setSelection(spinnerPositionCompany);
					  }

					  imageView1 = (ImageView)findViewById(R.id.imageView1);
					  imageView1.setVisibility(View.VISIBLE);

					  Bitmap d = ConvertByteArrayToBitmap(myImageData);
					  if (d != null)
					  {
						  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
						  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
						  imageView1.setImageBitmap(scaled);
					  }
				  }
					/*ImageProcessor myImgProcessor = new ImageProcessor(getBaseContext());
					myImgProcessor.SetWebServiceUrl(Globals.getValue("ws_url"));
					
					String imgUrl = myImgProcessor.getImageURL(myID, "contacts");

					new DownloadImageTask(bmImage).execute(imgUrl);*/
			  }
		  }
	}
	
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
    	
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
         // Showing progress dialog
            pDialog = new ProgressDialog(UserProfileDetailEditActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
 
        @Override
        protected Boolean doInBackground(String... args) {
        	
	 		  boolean suc = false;
	 		  boolean suc1 = false;
	 		  int last_contact_created = 0;
        		
	 		  cContact myEmployee = new cContact();
	 		  
	 		  TextView SurNameView = (TextView) findViewById(R.id.textSurName);
			  myEmployee.surname = SurNameView.getText().toString();

			  TextView LastNameView = (TextView) findViewById(R.id.textLastName);
			  myEmployee.lastname = LastNameView.getText().toString();

			  TextView UserNameView = (TextView) findViewById(R.id.textUserName);
			  myEmployee.username = UserNameView.getText().toString();
			  myEmployee.login = UserNameView.getText().toString();

			  TextView descriptionView = (TextView) findViewById(R.id.textDescription);
			  myEmployee.description = descriptionView.getText().toString();

			  TextView emailView = (TextView) findViewById(R.id.textEmail);
			  myEmployee.email = emailView.getText().toString();

			  TextView phoneView = (TextView) findViewById(R.id.textPhone);
			  myEmployee.phone = phoneView.getText().toString();

			  TextView phoneshortView = (TextView) findViewById(R.id.textPhoneShort);
			  myEmployee.phone_short = phoneshortView.getText().toString();


			  TextView functionView = (TextView) findViewById(R.id.textJobTitle);
			  myEmployee.function = functionView.getText().toString();

			  TextView cityView = (TextView) findViewById(R.id.textCity);
			  myEmployee.city = cityView.getText().toString();

			  TextView hobbyView = (TextView) findViewById(R.id.textHobbies);
			  myEmployee.hobby = hobbyView.getText().toString();

			  TextView birthdateView = (TextView) findViewById(R.id.textBirthdate);
			  myEmployee.birth_date = birthdateView.getText().toString();

			  TextView languagesView = (TextView) findViewById(R.id.textLanguages);
			  myEmployee.languages = languagesView.getText().toString();

			  TextView startdateView = (TextView) findViewById(R.id.textStartdate);
			  myEmployee.date_in_service = startdateView.getText().toString();

			  TextView presentView = (TextView) findViewById(R.id.textPresent);
			  myEmployee.days_present = presentView.getText().toString();

			  TextView absentView = (TextView) findViewById(R.id.textAbsent);
			  myEmployee.days_not_present = absentView.getText().toString();

			  TextView educationView = (TextView) findViewById(R.id.textEducation);
			  myEmployee.education_level = educationView.getText().toString();

			  TextView diplomaView = (TextView) findViewById(R.id.textDiploma);
			  myEmployee.diplomas = diplomaView.getText().toString();

			  TextView skillsView = (TextView) findViewById(R.id.textSkills);
			  myEmployee.skills = skillsView.getText().toString();

			  TextView licensesView = (TextView) findViewById(R.id.textLicenses);
			  myEmployee.driver_license = licensesView.getText().toString();



			  TextView maintask1View = (TextView) findViewById(R.id.textMaintask1);
			  myEmployee.main_task_1 = maintask1View.getText().toString();

			  TextView maintask2View = (TextView) findViewById(R.id.textMaintask2);
			  myEmployee.main_task_2 = maintask2View.getText().toString();

			  TextView maintask3View = (TextView) findViewById(R.id.textMaintask3);
			  myEmployee.main_task_3 = maintask3View.getText().toString();

			  TextView maintask4View = (TextView) findViewById(R.id.textMaintask4);
			  myEmployee.main_task_4 = maintask4View.getText().toString();

			  TextView maintask5View = (TextView) findViewById(R.id.textMaintask5);
			  myEmployee.main_task_5 = maintask5View.getText().toString();

			  CheckBox BHV = (CheckBox) findViewById(R.id.checkboxBHV);
			  if (BHV.isChecked()) myEmployee.BHV = "1";
			  if (!BHV.isChecked()) myEmployee.BHV = "0";

			  CheckBox AED = (CheckBox) findViewById(R.id.checkboxAED);
			  if (AED.isChecked()) myEmployee.AED = "1";
			  if (!AED.isChecked()) myEmployee.AED = "0";

			  CheckBox VCA = (CheckBox) findViewById(R.id.checkboxVCA);
			  if (VCA.isChecked()) myEmployee.VCA = "1";
			  if (!VCA.isChecked()) myEmployee.VCA = "0";

			  CheckBox Keyholder = (CheckBox) findViewById(R.id.checkboxKeyholder);
			  if (Keyholder.isChecked()) myEmployee.keyholder = "1";
			  if (!Keyholder.isChecked()) myEmployee.keyholder = "0";

			  CheckBox BasicConfidant = (CheckBox) findViewById(R.id.checkboxBasicConfidant);
			  if (BasicConfidant.isChecked()) myEmployee.BC = "1";
			  if (!BasicConfidant.isChecked()) myEmployee.BC = "0";


			  Spinner departmentSpinner = (Spinner) findViewById(R.id.spinnerDepartmanet);
			  int myIndex = departmentSpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myEmployee.department_id = departmentkeyArray.get(myIndex);
			  }

			  Spinner locationSpinner = (Spinner) findViewById(R.id.spinnerLocation);
			  myIndex = locationSpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myEmployee.work_location_id = locationkeyArray.get(myIndex);
			  }

			  Spinner companySpinner = (Spinner) findViewById(R.id.spinnerCompany);
			  myIndex = companySpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myEmployee.company_id = companykeyArray.get(myIndex);
			  }
			  
		      int BitmapID = 0;
		      int ProjectID = 0;
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_user", bitmapByteArray, bitmapByteArrayThumbnail);
				  myEmployee.image_id = String.valueOf(BitmapID);
				  suc = true;
			  }	
	 		
			  // test password twice entered the same
			  TextView PassView1 = (TextView) findViewById(R.id.textPassword1);
			  TextView PassView2 = (TextView) findViewById(R.id.textPassword2);
			  String pass1 = PassView1.getText().toString();
			  String pass2 = PassView2.getText().toString();
			  
			 /* if (!pass1.equals("") && !pass2.equals(""))
			  {
				  if (pass1.equals(pass2))
				  {
					  if (!pass1.equals("")) myEmployee.password = pass1;
					  
					  if (contactid == "")
					  {
						  suc = MyProvider.createContact(myEmployee);
				  	  } else {
				  		  suc = MyProvider.updateContact(contactid, myEmployee);
				  	  }
					  
					  if (!suc)
					  {
						  error_message = MyProvider.last_error;
					  }
				  } else {
					  error_message = getResources().getString(R.string.password_different);
				  }
			  } else {
				  error_message = getResources().getString(R.string.password_empty);  
			  }*/

			  if(!contactid.equals("")){
			    	// edit mode
				  if (!pass1.equals("") && !pass2.equals(""))
				  {
					  if (pass1.length() > 4) 
					  {
						  if (pass1.equals(pass2))
						  {
							  if (!pass1.equals("")) 
							  {
								  // if user changed password
								  myEmployee.password = pass1;
								  suc = MyProvider.updateContact(contactid, myEmployee);
								  suc1 = MyProvider.updateContactProfile(profileid,contactid, myEmployee);
							  }
						  } else {
							  error_message = getResources().getString(R.string.password_different);
						  }
					  }else{
						  error_message = getResources().getString(R.string.error_invalid_password); 
					  }
				  }else{
					  // if password empty => user didn't change password, is the same
					  myEmployee.password = myPass;
					  suc = MyProvider.updateContact(contactid, myEmployee);
					  suc1 = MyProvider.updateContactProfile(profileid,contactid, myEmployee);
				  }
				  
			  }else{
			    	// create mode
				  if (!pass1.equals("") && !pass2.equals(""))
				  {
					  if (pass1.length() > 4) 
					  {			   
						  if (pass1.equals(pass2))
						  {
							  if (!pass1.equals("")) myEmployee.password = pass1;
	
							  	  last_contact_created = MyProvider.createContact(myEmployee);
							  	  if(last_contact_created != 0){
							  		suc = MyProvider.createContactProfile(myEmployee,last_contact_created);
							  	  }
								  
							  
							  if (!suc)
							  {
								  error_message = MyProvider.last_error;
							  }
						  } else {
							  error_message = getResources().getString(R.string.password_different);
						  }
					  }else{
						  error_message = getResources().getString(R.string.error_invalid_password); 
					  }
				  } else {
					  error_message = getResources().getString(R.string.password_empty);  
				  }
				  
			  }
			  
			  if (suc)
			  {
				    setResult(RESULT_OK);
				  	finish();
			  } 
			  			  
	    	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(UserProfileDetailEditActivity.this, getString(R.string.text_error), error_message);
		    			 
		    			 if (pDialog.isShowing())
				                pDialog.dismiss();
		    		} 
	            	
	            	if (pDialog.isShowing())
		                pDialog.dismiss();
	            	         	   	
	              }
	        });
	         
	    }
    }
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}

	protected boolean shouldAskPermissions() {
		return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
	}

	@TargetApi(23)
	protected void askPermissions() {
		String[] permissions = {
				"android.permission.READ_EXTERNAL_STORAGE",
				"android.permission.WRITE_EXTERNAL_STORAGE"
		};
		int requestCode = 200;
		requestPermissions(permissions, requestCode);
	}

	// button create image
	public void onCreateImage(View v){
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try
			{
				if (shouldAskPermissions()) {
					if (ContextCompat.checkSelfPermission(UserProfileDetailEditActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
							!= PackageManager.PERMISSION_GRANTED
							&&
							ContextCompat.checkSelfPermission(UserProfileDetailEditActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
									!= PackageManager.PERMISSION_GRANTED) {
						// Permission is not granted
						askPermissions();
					}else{
						photoFile = createImageFile();
					}

				}else {
					photoFile = createImageFile();
				}
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}

			if (photoFile != null)
			{
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}

			//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}
	
	// button create image
	/*public void onCreateImage(View v){
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();
		
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
		// Create the File where the photo should go
		File photoFile = null;
		try
		{
			photoFile = createImageFile();
		} 
		catch (IOException ex) 
		{
		
		}
		
		if (photoFile != null) 
		{
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
			Uri.fromFile(photoFile));
			startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
		}
		
		//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}*/

	private File createImageFile() throws IOException {

		if(permission_camera ){
			// Create an image file name
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String imageFileName = "JPEG_" + timeStamp + "_";
			File storageDir = Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_PICTURES);
			File image = File.createTempFile(
					imageFileName,  /* prefix */
					".jpg",         /* suffix */
					storageDir      /* directory */
			);

			// Save a file: path for use with ACTION_VIEW intents
			mCurrentPhotoPath = "file:" + image.getAbsolutePath();
			mCurrentPhotoPathAbsolute = image.getAbsolutePath();
			return image;
		}else {
			return null;
		}
	}
	/*private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
		imageFileName,  *//* prefix *//*
		".jpg",         *//* suffix *//*
		storageDir      *//* directory *//*
		);
		
		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		mCurrentPhotoPathAbsolute = image.getAbsolutePath();
		return image;
	}*/
	
	// button search image
	public void onSearchImage(View v){
		Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
		
		Intent intent = new Intent();
		
		intent.setAction(Intent.ACTION_GET_CONTENT);
		
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		
		intent.setType("image/*");
		
		startActivityForResult(intent, REQUEST__BROWSE_PHOTO);
	}
	
	// Methods - set visible the preview image
	private void AttachBitmapToReport()
	{
		int photoW = myBitmap.getWidth();
		int photoH = myBitmap.getHeight();   	
		int scaleFactor = Math.min(photoW/100, photoH/100);
		int NewW = photoW/scaleFactor;
		int NewH = photoH/scaleFactor;
		
		imageView1.setVisibility(View.VISIBLE);		
		imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
	}
	
	
	@SuppressLint("MissingSuperCall")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		InputStream stream = null;
		
		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
		{
			myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			AttachBitmapToReport();
		}
		
		if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK) 
		{
		
			try 
			{
				stream = getContentResolver().openInputStream(data.getData());
				myBitmap = BitmapFactory.decodeStream(stream);
				AttachBitmapToReport();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			if (stream != null) 
			{
				try
				{
					stream.close();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}            
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public String resizeBase64Image(String base64image){
		int IMG_WIDTH = 800;
		int IMG_HEIGHT = 600;
		byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT); 
		BitmapFactory.Options options=new BitmapFactory.Options();
		options.inPurgeable = true;
		Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
		
		
		if(image.getHeight() <= 400 && image.getWidth() <= 400){
			return base64image;
		}
			image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);
		
		ByteArrayOutputStream baos=new  ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.PNG,100, baos);
		
		byte [] b=baos.toByteArray();
		System.gc();
		return Base64.encodeToString(b, Base64.NO_WRAP);
	
	} 
	
	private void zoomImageFromThumb(final View thumbView, String imageResId) {
			// If there's an animation in progress, cancel it
			// immediately and proceed with this one.
			if (mCurrentAnimator != null) {
				mCurrentAnimator.cancel();
			}
		
		// Load the high-resolution "zoomed-in" image.
		final ImageView expandedImageView = (ImageView) findViewById(R.id.expanded_image);
		
		//
			byte[] decodedString;
			if(imageResId != ""){
				String resizeString =  resizeBase64Image(imageResId);
				decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
			}else{
				decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
			}
		
		
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
				if (decodedByte != null)
				{
				//int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
				//Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
					expandedImageView.setImageBitmap(decodedByte);
				
				}
			
			
			
			// Calculate the starting and ending bounds for the zoomed-in image.
			// This step involves lots of math. Yay, math.
			final Rect startBounds = new Rect();
			final Rect finalBounds = new Rect();
			final Point globalOffset = new Point();
			
			// The start bounds are the global visible rectangle of the thumbnail,
			// and the final bounds are the global visible rectangle of the container
			// view. Also set the container view's offset as the origin for the
			// bounds, since that's the origin for the positioning animation
			// properties (X, Y).
			thumbView.getGlobalVisibleRect(startBounds);
			findViewById(R.id.scrollView)// the container
			.getGlobalVisibleRect(finalBounds, globalOffset);
			startBounds.offset(-globalOffset.x, -globalOffset.y);
			finalBounds.offset(-globalOffset.x, -globalOffset.y);
			
			// Adjust the start bounds to be the same aspect ratio as the final
			// bounds using the "center crop" technique. This prevents undesirable
			// stretching during the animation. Also calculate the start scaling
			// factor (the end scaling factor is always 1.0).
			float startScale;
			if ((float) finalBounds.width() / finalBounds.height()
			> (float) startBounds.width() / startBounds.height()) {
				// Extend start bounds horizontally
				startScale = (float) startBounds.height() / finalBounds.height();
				float startWidth = startScale * finalBounds.width();
				float deltaWidth = (startWidth - startBounds.width()) / 2;
				startBounds.left -= deltaWidth;
				startBounds.right += deltaWidth;
			} else {
				// Extend start bounds vertically
				startScale = (float) startBounds.width() / finalBounds.width();
				float startHeight = startScale * finalBounds.height();
				float deltaHeight = (startHeight - startBounds.height()) / 2;
				startBounds.top -= deltaHeight;
				startBounds.bottom += deltaHeight;
			}
			
			// Hide the thumbnail and show the zoomed-in view. When the animation
			// begins, it will position the zoomed-in view in the place of the
			// thumbnail.
			thumbView.setAlpha(0f);
			expandedImageView.setVisibility(View.VISIBLE);
			
			// Set the pivot point for SCALE_X and SCALE_Y transformations
			// to the top-left corner of the zoomed-in view (the default
			// is the center of the view).
			expandedImageView.setPivotX(0f);
			expandedImageView.setPivotY(0f);
			
			// Construct and run the parallel animation of the four translation and
			// scale properties (X, Y, SCALE_X, and SCALE_Y).
			AnimatorSet set = new AnimatorSet();
			set
			.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
			startBounds.left, finalBounds.left))
			.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
			startBounds.top, finalBounds.top))
			.with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
			startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
			View.SCALE_Y, startScale, 1f));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mCurrentAnimator = null;
				}
			
				@Override
				public void onAnimationCancel(Animator animation) {
					mCurrentAnimator = null;
				}
			});
			set.start();
			mCurrentAnimator = set;
			
			// Upon clicking the zoomed-in image, it should zoom back down
			// to the original bounds and show the thumbnail instead of
			// the expanded image.
			final float startScaleFinal = startScale;
			expandedImageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (mCurrentAnimator != null) {
						mCurrentAnimator.cancel();
					}
				
				// Animate the four positioning/sizing properties in parallel,
				// back to their original values.
				AnimatorSet set = new AnimatorSet();
				set.play(ObjectAnimator
				.ofFloat(expandedImageView, View.X, startBounds.left))
				.with(ObjectAnimator
				.ofFloat(expandedImageView, 
				View.Y,startBounds.top))
				.with(ObjectAnimator
				.ofFloat(expandedImageView, 
				View.SCALE_X, startScaleFinal))
				.with(ObjectAnimator
				.ofFloat(expandedImageView, 
				View.SCALE_Y, startScaleFinal));
				set.setDuration(mShortAnimationDuration);
				set.setInterpolator(new DecelerateInterpolator());
				set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
						thumbView.setAlpha(1f);
						expandedImageView.setVisibility(View.GONE);
						mCurrentAnimator = null;
					}
					
					@Override
					public void onAnimationCancel(Animator animation) {
						thumbView.setAlpha(1f);
						expandedImageView.setVisibility(View.GONE);
						mCurrentAnimator = null;
					}
				});
					set.start();
					mCurrentAnimator = set;
				}
			});
		}

	
}
