package com.sharethatdata.digitime.users;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class UserProfileDetail extends AppCompatActivity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	//WSRoboDataProvider MyRoboProvider = null;
	
	private String myUser = "";
	private String myPass = "";
	
	ArrayList<HashMap<String, String>> userList;
	
	ProgressDialog progressDialog;
	
	String myImageData = "";
	String userId = "";
	String profileId = "";
	//String statusId = "";
	String statusName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_employee);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);
		
		// Initializing
        Globals = ((MyGlobals)getApplicationContext());
		Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        //MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		//MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		//getActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState == null) {
		    Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	userId= null;
		    } else {
		    	userId= extras.getString("idUser");
		    }
		} else {
			userId= (String) savedInstanceState.getSerializable("idUser");
		}
		
		SharedPreferences prefContactID = getSharedPreferences("PREF_ID_CONTACT", 0); 
        String id_user_logged = prefContactID.getString("idContact", ""); // ID of the user logged
        String isMan = Globals.getValue("publisher");
        
        // owner user selected from users list
        if(userId.equals(id_user_logged) || isMan == "yes"){
        	Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.VISIBLE);
        }else{
        	Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.INVISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);
        }
		
		if (isMan == "yes")
		{	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.VISIBLE);	
		} else {	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);	
		}
		
		Button btnDelete = (Button) findViewById(R.id.btnDelete);
  	    
	    // delete button listener
	    btnDelete.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {
		
				  new AlertDialog.Builder(UserProfileDetail.this)
				  .setTitle("Delete employee")
				  .setMessage("Do you really want to delete this employee?")
				  .setIcon(android.R.drawable.ic_dialog_alert)
				  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	
				      public void onClick(DialogInterface dialog, int whichButton) {

				          new DeleteUserTask(userId).execute();
				          
				      }})
				   .setNegativeButton(android.R.string.no, null).show();
			  
		  }
		 
		});
	    
	    Button btnEdit = (Button) findViewById(R.id.btnEdit);
  	    
	    // edit button listener
	    btnEdit.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {

				  Intent intent = new Intent(v.getContext(), UserProfileDetailEditActivity.class); 
				  intent.putExtra("idUser", userId);
				  intent.putExtra("idProfile", profileId);
				  startActivityForResult(intent, 2); 
		  }
		 
		});

		final TextView tv_email = (TextView) findViewById(R.id.emailText);
		tv_email.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{tv_email.getText().toString()});
				i.putExtra(Intent.EXTRA_SUBJECT, ""); // subject of email
				i.putExtra(Intent.EXTRA_TEXT   , ""); // body of email
				try {
					startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(UserProfileDetail.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
			}
		});

		final TextView tv_phone = (TextView) findViewById(R.id.phoneText);
		tv_phone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tv_phone.getText().toString()));
				try {
					startActivity(intent);
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(UserProfileDetail.this, "Error", Toast.LENGTH_SHORT).show();
				}
			}
		});

		new LoadUser().execute();
        
        
	}


	
	// save values if phone rotate
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	  savedInstanceState.putString("idUser", userId);    
	  super.onSaveInstanceState(savedInstanceState);  
	}  
 
	// load values after phone rotate
	@Override  
	public void onRestoreInstanceState(Bundle savedInstanceState) {  
	  super.onRestoreInstanceState(savedInstanceState);  
	  userId = savedInstanceState.getString("idUser");  
	}
	
	// load data again, just if turn back from edit. Is like this,because, onResume don't stop  ProgressDialog, it keeps running
	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {     
	  super.onActivityResult(requestCode, resultCode, data); 
		  if (resultCode == Activity.RESULT_OK) { 
	    	  new LoadUser().execute();
		  }
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
			   
			   finish();
			   
			   		super.onBackPressed();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
		}
	
	// when get and show the image
		private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
	
	/**
	 * Background Async Task to Load User by making HTTP Request
  	 * */
	class LoadUser extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         showProgressDialog(getResources().getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	final cContact contact = MyProvider.getContact(userId);
	    	
	    	if (contact != null)
			  {
				  if (contact.image_id != "0")
				  {
					  //myImageData = MyRoboProvider.getImage(contact.image_id);
					  myImageData = MyProvider.getImage(contact.image_id);
				  }
			  }
	    	
	    	runOnUiThread(new Runnable() {
	               @Override
	               public void run() {
	            	   
					  profileId = contact.id;
					  TextView nameView = (TextView) findViewById(R.id.titleText);
					  nameView.setText(contact.surname + " " + contact.lastname);
					  TextView functionView = (TextView) findViewById(R.id.subTitleText);
					  functionView.setText(contact.function);
					  TextView emailView = (TextView) findViewById(R.id.emailText);
					  emailView.setText(contact.email);
					  TextView phoneView = (TextView) findViewById(R.id.phoneText);
					  phoneView.setText(contact.phone);
					  TextView statusView = (TextView) findViewById(R.id.StatusText);
					  //statusView.setText(String.valueOf(contact.status_name));
					  //statusId = contact.status_id;

					  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
					  descriptionView.setText(String.valueOf(contact.description));

					  TextView departmentView = (TextView) findViewById(R.id.departmentText);
					  departmentView.setText(String.valueOf(contact.department));
					  TextView headdepartmentView = (TextView) findViewById(R.id.headdepartmentText);
					  headdepartmentView.setText(String.valueOf(contact.head_department));

					   TextView bhvView = (TextView) findViewById(R.id.bhvText);
					   bhvView.setText(String.valueOf(contact.BHV));
					   TextView vcaView = (TextView) findViewById(R.id.vcaText);
					   vcaView.setText(String.valueOf(contact.VCA));
					   TextView aedView = (TextView) findViewById(R.id.aedText);
					   aedView.setText(String.valueOf(contact.AED));
					   TextView keyholderView = (TextView) findViewById(R.id.keyholderText);
					   keyholderView.setText(String.valueOf(contact.keyholder));
					   TextView basicconfidantView = (TextView) findViewById(R.id.basicconfidantText);
					   basicconfidantView.setText(String.valueOf(contact.BC));


					   TextView startdateView = (TextView) findViewById(R.id.startdateText);
					   startdateView.setText(String.valueOf(contact.date_in_service));
					   TextView locationView = (TextView) findViewById(R.id.locationText);
					   locationView.setText(String.valueOf(contact.work_location));
					   TextView timepresentView = (TextView) findViewById(R.id.timepresentText);
					   timepresentView.setText(String.valueOf(contact.days_present));
					   TextView timeabsentView = (TextView) findViewById(R.id.timeabsentText);
					   timeabsentView.setText(String.valueOf(contact.days_not_present));
					   //TextView phoneView = (TextView) findViewById(R.id.phoneText);
					   //phoneView.setText(String.valueOf(contact.phone));
					   //TextView emailView = (TextView) findViewById(R.id.emailText);
					   //emailView.setText(String.valueOf(contact.email));
					   TextView focuscompanyView = (TextView) findViewById(R.id.focuscompanyText);
					   focuscompanyView.setText(String.valueOf(contact.company));

					   TextView maintask1View = (TextView) findViewById(R.id.maintask1Text);
					   maintask1View.setText(String.valueOf(contact.main_task_1));
					   TextView maintask2View = (TextView) findViewById(R.id.maintask2Text);
					   maintask2View.setText(String.valueOf(contact.main_task_2));
					   TextView maintask3View = (TextView) findViewById(R.id.maintask3Text);
					   maintask3View.setText(String.valueOf(contact.main_task_3));
					   TextView maintask4View = (TextView) findViewById(R.id.maintask4Text);
					   maintask4View.setText(String.valueOf(contact.main_task_4));
					   TextView maintask5View = (TextView) findViewById(R.id.maintask5Text);
					   maintask5View.setText(String.valueOf(contact.main_task_5));

					   TextView cityView = (TextView) findViewById(R.id.cityText);
					   cityView.setText(String.valueOf(contact.city));
					   TextView phonepersonalView = (TextView) findViewById(R.id.phonepersonalText);
					   phonepersonalView.setText(String.valueOf(contact.phone_short));
					   TextView hobbiesView = (TextView) findViewById(R.id.hobbiesText);
					   hobbiesView.setText(String.valueOf(contact.hobby));
					   TextView birthdateView = (TextView) findViewById(R.id.birthdateText);
					   birthdateView.setText(String.valueOf(contact.birth_date));
					   TextView languagesView = (TextView) findViewById(R.id.languagesText);
					   languagesView.setText(String.valueOf(contact.languages));

					   TextView educationlevelView = (TextView) findViewById(R.id.educationlevelText);
					   educationlevelView.setText(String.valueOf(contact.education_level));
					   TextView diplomasView = (TextView) findViewById(R.id.diplomasText);
					   diplomasView.setText(String.valueOf(contact.diplomas));
					   TextView skillsView = (TextView) findViewById(R.id.skillsText);
					   skillsView.setText(String.valueOf(contact.skills));
					   TextView licensesView = (TextView) findViewById(R.id.licensesText);
					   licensesView.setText(String.valueOf(contact.driver_license));
    
	   		    		
	   		    		ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
	  				  
		  				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
		  				  if (d != null)
		  				  {
		  					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
		  					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
		  					  imageView1.setImageBitmap(scaled);
		  					  				  
		  				  }
	               }
	            });
	    	 return "";
	    }

	    protected void onPostExecute(String result) {

	    	super.onPostExecute(result);
	    	
	    	if (result != null)
			  {
	    		//new LoadUserStatusTask(statusId).execute();
			  }
            hideProgressDialog();
	    }
	  

	}
	
	
/*	private class LoadUserStatusTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public LoadUserStatusTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  
			  ArrayList<HashMap<String, String>> statusList = new ArrayList<HashMap<String, String>>();

		    		// convert objects to hashmap for list
		    		List<cUserStatus> List = MyProvider.getUserStatus();
		    	
		    		for(cUserStatus entry : List)
	             	{
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		
		                map.put("pid", entry.id);
		                map.put("name", entry.name);
		                
		                if(myID.contains(entry.id)){
		                	statusName = entry.name;
		                }
		                 
		                // adding HashList to ArrayList
		                statusList.add(map);
		             }
			
			  return statusName;
		  }

		  protected void onPostExecute(String result) { 
			  if(result != null){
				  TextView statusView = (TextView) findViewById(R.id.StatusText);
		          statusView.setText(String.valueOf(statusName));
			  }
			  
		  }
	}*/
	
	private class DeleteUserTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public DeleteUserTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  			  
			  boolean suc = MyProvider.deleteContact(myID);
			  
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  setResult(RESULT_OK);
			  finish();
			  
		  }
	}


	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	public void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

}
