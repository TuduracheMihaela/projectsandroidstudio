package com.sharethatdata.digitime;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

//public class SickdaysActivity extends FragmentActivity implements ActionBar.TabListener{
public class SickdaysActivity extends AppCompatActivity {

	private ViewPager viewPager;
    private SickdaysTabsPagerAdapter mAdapter;
    //private ActionBar actionBar;
    // Tab titles
    //private String[] tabs = { "All sickdays", "Request sickday" };

    MyGlobals Globals;
    
    private SickdaysTabsPagerAdapter mSectionsPagerAdapter; 
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sickdays);

        Globals = ((MyGlobals)getApplication());
        Globals.backgroundProcess();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        
        /*mSectionsPagerAdapter = new SickdaysTabsPagerAdapter(getSupportFragmentManager());

 		mSectionsPagerAdapter.notifyDataSetChanged();
 		viewPager = (ViewPager) findViewById(R.id.pager);  
 		
 		viewPager.setAdapter(mSectionsPagerAdapter);
 
        // Initilization
        actionBar = getActionBar();
        mAdapter = new SickdaysTabsPagerAdapter(getSupportFragmentManager());
 
        viewPager.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);        
 
        // Adding Tabs
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
 
        *//**
         * on swiping the viewpager make respective tab selected
         * *//*
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
 
            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);
                
                switch (position) {
       	        case 0:
       	        	//Toast.makeText(getActivity(), "Inbox", Toast.LENGTH_SHORT).show();
       	        	SickdaysListFragment fragmentI = (SickdaysListFragment) mSectionsPagerAdapter.instantiateItem(viewPager, 0);
                    if (fragmentI != null) {
                   	 fragmentI.myRefresh();
                    }
       	            break;
       	        case 1: {
       	            break;
       	        	}
       	        }
            }
 
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
 
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });*/
    }

    private void setupViewPager(ViewPager viewPager) {
        HolidaysTabsPagerAdapter adapter = new HolidaysTabsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HolidaysListFragment(), "All sickdays");
        adapter.addFragment(new HolidaysRequestFragment(), "Request sickday");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

	/*@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
        // show respected fragment view
		 viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

	void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }*/


}
