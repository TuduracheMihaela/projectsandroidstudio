package com.sharethatdata.digitime.projects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.ListActivity;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.RecordTimeActivity;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cStatus;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cTask;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class AddProjectActivity extends AppCompatActivity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;

	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	//private static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final int REQUEST_TAKE_PHOTO = 1;
	private static final int REQUEST_BROWSE_PHOTO = 2;
	private static final int PERMISSION_TAKE_PHOTO = 3;
	private Bitmap myBitmap;
	private byte[] bitmapByteArray;

	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	
	ProgressDialog progressDialog;
	
	private int mYear,mMonth,mDay;
	
	List<cContact> employeeList; // getContacts 
	ArrayList<HashMap<String, String>> userListbyRights; // getContacts by Rights
	ArrayList<HashMap<String, String>> privacyList; // get project privacy
	ArrayList<HashMap<String, String>> statusList; // get project status
	ArrayList<HashMap<String, String>> costplaceList; // get project costplace
	
	List<String> userkeyArray =  new ArrayList<String>();
	List<String> privacykeyArray =  new ArrayList<String>();
	List<String> statuskeyArray =  new ArrayList<String>();
	List<String> costplacekeyArray =  new ArrayList<String>();

	List<String> statusArray =  new ArrayList<String>();

	TextInputLayout projectNameInputLayout;
	EditText projectNameEditText;
	
	TextView dateTextView;
	TextView textSelectCoWorker;
	String textSelectCoWorkerString, textSelectCoWorkerStringOriginal;
	
	ImageView imageView1;
	Button btnCreateImage;
	Button btnSearchImage;
	Button btnCreate;

	boolean permission_camera = false;
	
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_add_project);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		// avoid automatically appear android keyboard when activity start
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(AddProjectActivity.this, AddProjectActivity.this);

		projectNameInputLayout = (TextInputLayout) findViewById(R.id.project_text_input_layout);
		projectNameEditText = (EditText) findViewById(R.id.textProjectName);
		
		textSelectCoWorker = (TextView) findViewById(R.id.textProjectMembers);
		dateTextView = (TextView) findViewById(R.id.textProjectDateEnd);
		
		imageView1 = (ImageView) findViewById(R.id.imageView1);
		imageView1.setVisibility(View.GONE);
	    btnSearchImage = (Button) findViewById(R.id.pictureFromPhone);
	    btnCreateImage = (Button) findViewById(R.id.pictureCamera);
	    btnCreate = (Button) findViewById(R.id.buttonAddProject);
	    
	    EditText editDesc = (EditText) findViewById(R.id.textProjectDescription);       
	    editDesc.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.textProjectDescription) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                    }
                }
                return false;
            }
        });
		
		Calendar c = Calendar.getInstance();
	    mYear = c.get(Calendar.YEAR);
	    mMonth = c.get(Calendar.MONTH);
	    mDay = c.get(Calendar.DAY_OF_MONTH);
	    
	    dateTextView.setText(new StringBuilder().append(pad(mDay)).append("-")
	            .append(pad(mMonth + 1)).append("-")
	            .append(pad(mYear)));
		
		new LoadList().execute();
		
		textSelectCoWorker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LoadEmployeeList().execute();
			}
		});
		
		
		//////////////////////////////////////////////////////////////
		/*
		 * EDIT
		 */
		
		String edit = Globals.getValue("edit_project");
        if(edit != ""){
        	setTitle(getResources().getString(R.string.title_activity_edit_projects));
       	    btnCreate.setVisibility(View.VISIBLE);
       	    btnCreate.setText(getResources().getString(R.string.title_activity_edit_projects));
        }else{
        	setTitle(getResources().getString(R.string.title_activity_add_projects));
        	btnCreate.setVisibility(View.VISIBLE);
        	btnCreate.setText(getResources().getString(R.string.title_activity_add_projects));
        }



        btnCreateImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

				if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
						== PackageManager.PERMISSION_DENIED
						&&
						ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
								== PackageManager.PERMISSION_DENIED  ){
					ActivityCompat.requestPermissions(AddProjectActivity.this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
				}else{
					permission_camera = true;
				}

				if(permission_camera){
					StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
					StrictMode.setVmPolicy(builder.build());

					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
						// Create the File where the photo should go
						File photoFile = null;
						try {
							photoFile = createImageFile();
						} catch (IOException ex) {
							Log.d("IMAGE onCreate", ex.toString());
						}

						if (photoFile != null) {
							takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(photoFile));
							takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
							startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
						}
						//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
					}
				}
				else {
					Toast.makeText(AddProjectActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}

	private boolean shouldShowError() {
		int emailLength = projectNameEditText.getText().length();
		return emailLength <= 0 ;
	}

	private void showError() {
		projectNameInputLayout.setErrorEnabled(true);
		projectNameInputLayout.setError(getString(R.string.text_project_name_required));
	}

	private void hideError() {
		projectNameInputLayout.setErrorEnabled(false);
		projectNameInputLayout.setError("");
	}

	private void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
					hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	public void onAddProject(View v){
		String edit = Globals.getValue("edit_project");

		boolean cancel = false;
		View focusView = null;

		boolean textLenght = shouldShowError();
        if(edit != ""){
       	    // edit project

			if (textLenght) {
				showError();
				focusView = projectNameEditText;
				cancel = true;
			}

			if(cancel){
				focusView.requestFocus();
			}else{
				hideError();
			}

			if(!cancel){
				hideKeyboard();

				String id = Globals.getValue("projectid");
				new EditProjectTask(id).execute();
			}
        }else{
			// create project

			if (textLenght) {
				showError();
				focusView = projectNameEditText;
				cancel = true;
			}

			if(cancel){
				focusView.requestFocus();
			}else{
				hideError();
			}

			if(!cancel){
				hideKeyboard();

				new RegisterTask().execute();
			}

        }
		
	}
    
	public void onSelectDate(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    dp1.show();
	}
	
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mYear = year;
	            mMonth = month;
	            mDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.textProjectDateEnd);
	    		        
	            startDateView.setText(new StringBuilder().append(pad(mDay)).append("-")
	                .append(pad(mMonth + 1)).append("-")
	                .append(pad(mYear)));
	    }
	};
	
	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         progressDialog = new ProgressDialog(AddProjectActivity.this);
         progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
      
         progressDialog.setIndeterminate(false);
         progressDialog.setCancelable(false);
         progressDialog.setCanceledOnTouchOutside(false);
         progressDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	userListbyRights = new ArrayList<HashMap<String, String>>();
	    	privacyList = new ArrayList<HashMap<String, String>>();
	    	statusList = new ArrayList<HashMap<String, String>>();
			costplaceList = new ArrayList<HashMap<String, String>>();
	    	
	    	String isAdministrator = Globals.getValue("administrator");
			String isDeveloper = Globals.getValue("developer");
			String isEmployee = Globals.getValue("employee");
			
			List<cContact> contactsList = MyProvider.getContacts();
			List<cStatus> status_list = MyProvider.getStatus();
			List<cCostplace> costplace_list = MyProvider.getCostPlaces(myUserID);

			/*List<cContact> contactsList = null;
			if( isAdministrator == "yes" || isDeveloper == "yes"){
				contactsList = MyProvider.getContacts();
			} else {
				contactsList = MyProvider.getContactsByRights();
			}*/
	    	
	    	// load data from provider
    		for(cContact entry : contactsList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                userListbyRights.add(map);
			}

			for(cStatus entry : status_list)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  entry.id);
				map.put("name", entry.name);

				// adding HashList to ArrayList
				statusList.add(map);
			}

			for(cCostplace entry : costplace_list)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  String.valueOf(entry.id));
				map.put("name", entry.name);

				// adding HashList to ArrayList
				costplaceList.add(map);
			}

	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	        progressDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {

		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	
		            	// store the id of the item in a separate array
		             
		           	    List<String> userArray =  new ArrayList<String>();
		           	    List<String> privacyArray =  new ArrayList<String>();
						List<String> costplaceArray =  new ArrayList<String>();
		           	    privacyArray.add(0, "Private");
		           	    privacyArray.add(1, "Public");
		           	    privacykeyArray.add(0, "Private");
		           	    privacykeyArray.add(1, "Public");
		           	    
		           	   
		           	    // load spinner data from dataprovider
		           	    for (HashMap<String, String> map : userListbyRights)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") userkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
		           	        }

						for (HashMap<String, String> map : statusList)
							for (Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
								if (entry.getKey() == "name") statusArray.add(entry.getValue());
							}

						for (HashMap<String, String> map : costplaceList)
							for (Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "id") costplacekeyArray.add(entry.getValue());
								if (entry.getKey() == "name") costplaceArray.add(entry.getValue());
							}
		           		
	           	    	final ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(AddProjectActivity.this, android.R.layout.simple_spinner_item, userArray);
	           	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	final Spinner sUsers = (Spinner) findViewById(R.id.textProjectOwner);
	          	       	sUsers.setAdapter(user_adapter);
		      	        
		      	        int spinnerPosition = user_adapter.getPosition(myUserName);
		  	    		//set the default according to value
		  	    		sUsers.setSelection(spinnerPosition);
	           	    
	           	    	final ArrayAdapter<String> privacy_adapter = new ArrayAdapter<String>(AddProjectActivity.this, android.R.layout.simple_spinner_item, privacyArray);
	           	    	privacy_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final Spinner sPrivacy = (Spinner) findViewById(R.id.textProjectPrivacy);
	           	       	sPrivacy.setAdapter(privacy_adapter);

						final ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(AddProjectActivity.this, android.R.layout.simple_spinner_item, statusArray);
						status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						final Spinner sStatus = (Spinner) findViewById(R.id.textProjectStatus);
						sStatus.setAdapter(status_adapter);

						final ArrayAdapter<String> costplace_adapter = new ArrayAdapter<String>(AddProjectActivity.this, android.R.layout.simple_spinner_item, costplaceArray);
						costplace_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						final Spinner sCostplace = (Spinner) findViewById(R.id.textProjectCostplace);
						sCostplace.setAdapter(costplace_adapter);
	           	       	
	           	       	final TextView mView = (TextView) findViewById(R.id.textProjectMembers);
	           	       	mView.setText(myUserName);
						textSelectCoWorkerString = mView.getText().toString();
	           	    	
	           	       	//////////////////////////////////////////////////////////////////////////////
	           	       	// set edit parameters for edit project
	           	       	
	        	        String edit = Globals.getValue("edit_project");	           	    	
	           	    	
	           	     // set edit parameters for edit time report
	        	        String id = Globals.getValue("projectid");
	                	String textTitle =  Globals.getValue("textViewName");
	                	String textCode = Globals.getValue("textViewCode");
	                	String textOwner = Globals.getValue("textViewOwner");
	                	String textCreated = Globals.getValue("textViewCreated");
	                	String textDeadline = Globals.getValue("textViewDeadline");
	                	String textStatus = Globals.getValue("textViewStatus");
	                	String textPrivacy = Globals.getValue("textViewPrivacy");
						String textCostplace = Globals.getValue("textViewCostplace");
	                	String textMembers = Globals.getValue("textViewMembers");	
	                	String textDescription =  Globals.getValue("textViewDescription");
	                	String myImageData = Globals.getValue("image_string");
	        			
	        			if(edit != ""){
	        				
	        				 // date
	        		        StringTokenizer tkDate = new StringTokenizer(textDeadline);
	        		        String dateTokenizer = tkDate.nextToken();  // <---  yyyy-mm-dd
	        		        String[] separatedDate = dateTokenizer.split("/");
	        		        String day = separatedDate[0];
	        		        String month = separatedDate[1];
	        		        String year = separatedDate[2];
	        		        	        				
	        				Calendar c = new GregorianCalendar();
	        				c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
	        				c.set(Calendar.MONTH, Integer.valueOf(month));
	        				c.set(Calendar.YEAR, Integer.valueOf(year));
	        				
	        			    mYear = Integer.valueOf(year);
	        			    mMonth = Integer.valueOf(month);
	        			    mDay = Integer.valueOf(day);
	        		        
	        		        TextView startDateView = (TextView) findViewById(R.id.textProjectDateEnd);
	        		        startDateView.setText(new StringBuilder().append(pad(mDay)).append("-")
	        		            .append(pad(mMonth)).append("-")
	        		            .append(pad(mYear)));
	        		        
	        		        mMonth = mMonth - 1;
	        		        
	           		    	int spinnerPosition1 = user_adapter.getPosition(textOwner);
	           		    	//set the default according to value
	           		    	sUsers.setSelection(spinnerPosition1);
	           		    	
	           		    	int spinnerPosition2 = privacy_adapter.getPosition(textPrivacy);
	        	    		//set the default according to value
	           		    	sPrivacy.setSelection(spinnerPosition2);

							int spinnerPosition3 = costplace_adapter.getPosition(textCostplace);
							//set the default according to value
							sCostplace.setSelection(spinnerPosition3);

							int spinnerPosition4 = status_adapter.getPosition(textStatus);
							//set the default according to value
							sStatus.setSelection(spinnerPosition4);
	           		    	
	           		        TextView codeText = (TextView) findViewById(R.id.textProjectCode);
		        		    codeText.setText(textCode);
		        		    
		        		    TextView nameText = (TextView) findViewById(R.id.textProjectName);
		        		    nameText.setText(textTitle);
		        		    
		        		    TextView descriptionText = (TextView) findViewById(R.id.textProjectDescription);
		        		    descriptionText.setText(Html.fromHtml(textDescription)); 
		        		    
		        		    TextView membersText = (TextView) findViewById(R.id.textProjectMembers);
		        		    membersText.setText(textMembers);
		        		    textSelectCoWorkerString = textMembers;
							textSelectCoWorkerStringOriginal = textMembers;

		        		    Bitmap d = ConvertByteArrayToBitmap(myImageData);
			      			  if (d != null)
			      			  {
			      				  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
			      				  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
			      				  imageView1.setVisibility(View.VISIBLE);
			      				  imageView1.setImageBitmap(scaled);
			      				  				  
			      			  }
	           		    	
	           		    }
	        			
	        			
	        			
	            	
	              }
	        });
	              
	    }

	}

	
    /**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadEmployeeList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();  
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {   
	    
	   		employeeList = MyProvider.getContacts();
	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() { 
	            	
	            	
	            	if (employeeList.size() > 0)
	            	{
		            	final List<String> employee_list = new ArrayList<String>();
		            	// create array of string
		            	for(cContact v : employeeList)
		            	{
		            		employee_list.add(v.name);	            	
		            	}
		            	final CharSequence[] items = employee_list.toArray(new String[employee_list.size()]);
		            	final boolean[] checkedItems = new boolean[employeeList.size()];	            	
						final TextView mView = (TextView) findViewById(R.id.textProjectMembers);
						
						String text = textSelectCoWorkerString;
						Log.d("TEXT", text);
						String[] member_list = null;
						
						if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
						  {
							if(textSelectCoWorkerString.contains(";")){
								member_list = textSelectCoWorkerString.split(" ; ");
							}else{
								member_list = textSelectCoWorkerString.split("\n");
							}
						  }
						
						for (int i = 0; i < items.length; i++) {
							if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
							  {
								  for (String item : member_list)
								  {
									  if (items[i].equals(item)) {
										  checkedItems[i] = true;										  
									  }
								  }			  			 
							  } 
						}

		            	 // Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(AddProjectActivity.this);
		
		                // Set a title for alert dialog
		                builder.setTitle(getString(R.string.text_alert_dialog_selectMembers))
		                	// Specify the dialog is not cancelable
		                		.setCancelable(false)

		                		.setMultiChoiceItems(items, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which, boolean isChecked) {
										  
										// Update the current focused item's checked status
										checkedItems[which] = isChecked;

				                        // Get the current focused item
				                        String currentItem = employee_list.get(which);

				                        // Notify the current action
				                        Toast.makeText(getApplicationContext(),
				                                currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
		                		   			
						             } 
										
									
								});
		                		   	
		                //Set the positive/yes button click listener
		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								mView.setText("");
								textSelectCoWorkerString = "";

		                        for (int i = 0; i<checkedItems.length; i++){
		                            boolean checked = checkedItems[i];
		                            if (checked) {
		                            	mView.setText(mView.getText() + employee_list.get(i) + "\n");
		                            	textSelectCoWorkerString = mView.getText().toString();
		                            }
		                        }
							}
						});

		                // Set the neutral/cancel button click listener
		                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
		                    @Override
		                    public void onClick(DialogInterface dialog, int which) {
		                    	
		                    }
		                });
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                
	            	}
	            }
	        });    
	    }
	}
	
	
	  /** AsyncTask register project  */
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
    	
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddProjectActivity.this);
            progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
         
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
 
        @Override
        protected Boolean doInBackground(String... args) {
 
        	  String myDate = (new StringBuilder().append(pad(mYear)).append("-").append(pad(mMonth + 1)).append("-").append(pad(mDay))).toString();
        	  // extract data
			  Spinner userSpinner = (Spinner) findViewById(R.id.textProjectOwner);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myOwner = "";
			  if(myIndex != -1){
				  myOwner = userkeyArray.get(myIndex);		  
			  }
			  			  			  
			  Spinner privacySpinner = (Spinner) findViewById(R.id.textProjectPrivacy);
			  myIndex = privacySpinner.getSelectedItemPosition();
			  String myPrivacy = "";
			  if(myIndex != -1){
				  myPrivacy = privacykeyArray.get(myIndex);
			  }

			  Spinner costplaceSpinner = (Spinner) findViewById(R.id.textProjectCostplace);
			  myIndex = costplaceSpinner.getSelectedItemPosition();
			  String myCostplace = "";
			  if(myIndex != -1){
				  myCostplace = costplacekeyArray.get(myIndex);
			  }

			  Spinner statusSpinner = (Spinner) findViewById(R.id.textProjectStatus);
			  myIndex = statusSpinner.getSelectedItemPosition();
			  String myStatusId = "";
			  String myStatusName = "";
			  if(myIndex != -1){
				  myStatusId = statuskeyArray.get(myIndex);
				  myStatusName = statusArray.get(myIndex);
			  }
			  
			  TextView textProjectCodeView = (TextView) findViewById(R.id.textProjectCode);
		      String textProjectCode = textProjectCodeView.getText().toString();	
			  			  
		      TextView textProjectNameView = (TextView) findViewById(R.id.textProjectName);
		      String textProjectName = textProjectNameView.getText().toString();	
		      
		      TextView DescriptionView = (TextView) findViewById(R.id.textProjectDescription);
		      String description = Html.toHtml((Spanned) DescriptionView.getText());		
		      
		      
		      int BitmapID = 0;
		      int ProjectID = 0;
			  boolean sucImage = false;
			  boolean sucNews = false;
			  boolean suc = false;
			  if (myBitmap != null)
			  {
				  /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();*/
				  
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  bitmapByteArray = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_project", bitmapByteArray, bitmapByteArray);
				  suc = true;
			  }	
			  
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			   
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");
			  
			  	ProjectID = MyProvider.createProject(textProjectCode, myCostplace, textProjectName, description, myOwner, myStatusName, myDate, myPrivacy, String.valueOf(BitmapID));
			  	
			  	if(ProjectID != 0){
			  		if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
					  {
			  			String[] member_list;
						if(textSelectCoWorkerString.contains(";")){
							member_list = textSelectCoWorkerString.split(" ; ");
						}else{
							member_list = textSelectCoWorkerString.split("\n");
						}
						
						for (String item : member_list)
						  {
							  cContact myMember = MyProvider.getUser(this_organisation, item);
							  if (myMember.id != "")
							  suc =  MyProvider.createProjectMember(String.valueOf(ProjectID), myMember.id);

						  }	
						
					  }
			  	}
			  	
        	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
        	progressDialog.dismiss();
        	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 Toast.makeText(AddProjectActivity.this, "Was not created the project", Toast.LENGTH_SHORT).show();
		    		}else{
		    			Intent intent = new Intent(getApplicationContext(), MyProjectsListActivity.class);
		    			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    			startActivity(intent); 
		    		}
         	   	
	              }
	        });  
	    }
    }
    
    
    private class EditProjectTask extends AsyncTask<String, String, Boolean> {
    	
    	
		  String myID = "";
		  
		  public EditProjectTask(String id) {
		     myID = id;
		  }

		  
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            progressDialog = new ProgressDialog(AddProjectActivity.this);
	            progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
	         
	            progressDialog.setIndeterminate(false);
	            progressDialog.setCancelable(false);
	            progressDialog.setCanceledOnTouchOutside(false);
	            progressDialog.show();
	        }
	        
		  protected Boolean doInBackground(String... urls) {
        	
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");
			  
			// extract data
			  
			  TextView textProjectCodeView = (TextView) findViewById(R.id.textProjectCode);
		      String textProjectCode = textProjectCodeView.getText().toString();	
			  			  
		      TextView textProjectNameView = (TextView) findViewById(R.id.textProjectName);
		      String textProjectName = textProjectNameView.getText().toString();	
		      
		      TextView DescriptionView = (TextView) findViewById(R.id.textProjectDescription);
		      String description = Html.toHtml((Spanned) DescriptionView.getText());	
		      
		      String myDate = (new StringBuilder().append(pad(mYear)).append("-").append(pad(mMonth + 1)).append("-").append(pad(mDay))).toString();
        	  // extract data
			  Spinner userSpinner = (Spinner) findViewById(R.id.textProjectOwner);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myOwner = "";
			  if(myIndex != -1){
				  myOwner = userkeyArray.get(myIndex);		  
			  }

			  Spinner statusSpinner = (Spinner) findViewById(R.id.textProjectStatus);
			  int myIndexS = statusSpinner.getSelectedItemPosition();
			  String myStatusId = "";
			  String myStatusName = "";
			  if(myIndexS != -1){
				  myStatusId = statuskeyArray.get(myIndexS);
				  myStatusName = statusArray.get(myIndexS);
			  }
			  			  			  
			  Spinner privacySpinner = (Spinner) findViewById(R.id.textProjectPrivacy);
			  myIndex = privacySpinner.getSelectedItemPosition();
			  String myPrivacy = "";
			  if(myIndex != -1){
				  myPrivacy = privacykeyArray.get(myIndex); 
				  if(myPrivacy.contains("Private")){
					  myPrivacy = "0";
				  }else if(myPrivacy.contains("Public")){
					  myPrivacy = "1";
				  }
			  }

			  Spinner costplaceSpinner = (Spinner) findViewById(R.id.textProjectCostplace);
			  myIndex = costplaceSpinner.getSelectedItemPosition();
			  String myCostplace = "";
			  if(myIndex != -1){
				  myCostplace = costplacekeyArray.get(myIndex);
			  }
		      
		      int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();*/
				  
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  bitmapByteArray = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_project", bitmapByteArray, bitmapByteArray);
			  }		  
			  
			  suc = true;
			  

			  
					  //if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null) {
			  			String[] member_list;
			  			String[] member_list_original = new String[0];
						if(textSelectCoWorkerString.contains(";")){
							member_list = textSelectCoWorkerString.split(" ; ");
						}else{
							member_list = textSelectCoWorkerString.split("\n");
						}

					  	if (textSelectCoWorkerStringOriginal != "" && textSelectCoWorkerStringOriginal != null)
					  	{
							if(textSelectCoWorkerStringOriginal.contains(";")) {
								member_list_original = textSelectCoWorkerStringOriginal.split(" ; ");
							}else{
								member_list_original = textSelectCoWorkerStringOriginal.split("\n");
							}
					  	}

						List<String> sList = new ArrayList(Arrays.asList(member_list));
						List<String> sListOriginal = new ArrayList(Arrays.asList(member_list_original));
						List<String> sListADD = new ArrayList(Arrays.asList(member_list));
						List<String> sListDELETE = new ArrayList();


						for(String item1 : sListOriginal){
							if(sList.contains(item1)){
								sListADD.remove(item1); // here are the new members, add them as member to project
														// who remains in this list, are the new members
														// and who is repeated, remove from this list
							}else{
								sListDELETE.add(item1); // delete members those are unchecked from previous members list
							}
						}


						if(sListADD.size() > 0){
							for (String item3 : sListADD){
								Log.d("member_list_add", item3);

								cContact myMember = MyProvider.getUser(this_organisation, item3);
								if (myMember.id != "")
									suc =  MyProvider.createProjectMember(String.valueOf(myID), myMember.id);
									Log.d("createProjectMember", myMember.id);
							}
						}

						if(sListDELETE.size() > 0){
							for (String item4 : sListDELETE){
								Log.d("member_list_delete", item4);

                                cContact myMember = MyProvider.getUser(this_organisation, item4);
                                if (myMember.id != "")
                                    suc = MyProvider.deleteProjectMember(String.valueOf(myID), myMember.id); // delete member of this project
                                Log.d("deleteProjectMember", myMember.id);

							}
						}

						
					  /*}else{
						  TextView membersText = (TextView) findViewById(R.id.textProjectMembers);	        		    
		        		    String members = membersText.getText().toString();
		        		    String[] member_list;
		        		    if(members.contains(";")){
								member_list = members.split(" ; ");
							}else{
								member_list = members.split("\n");
							}
							  for (String item : member_list)
							  {
								  cContact myMember = MyProvider.getUser(this_organisation, item);
								  if (myMember.id != "")
								  //suc =  MyProvider.createProjectMember(String.valueOf(myID), myMember.id);
                                      Log.d("createProjectMember", myMember.id);
							  }	
					  }*/

			  
			  if (suc)
			  {
				  suc = MyProvider.editProject(String.valueOf(myID), textProjectCode, myCostplace, textProjectName, description, myStatusName, myDate, myPrivacy, "" + BitmapID);
			  }			  
		  
        	  return suc;
  			  
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  progressDialog.dismiss();
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Project updated", Toast.LENGTH_SHORT).show();
				  Globals.setValue("project_update", "project_update");
				  Globals.setValue("edit_project","");
				  finish();
				  
			  }else{
				  Toast.makeText(getApplicationContext(), "Was not updated project", Toast.LENGTH_SHORT).show();
				  Globals.setValue("project_update", "project_update");
				  Globals.setValue("edit_project","");
				  finish();
			  }
			  
			  
		  }
	}
    
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// when get and show the image
		private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
		
		/**
	     * This is very useful to overcome Memory waring issue while selecting image
	     * from Gallery
	     * 
	     * @param selectedImage
	     * @param context
	     * @return Bitmap
	     * @throws FileNotFoundException
	     */
	    public static Bitmap decodeBitmap(Uri selectedImage, Context context)
	            throws FileNotFoundException {
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(context.getContentResolver()
	                .openInputStream(selectedImage), null, o);

	        final int REQUIRED_SIZE = 100;

	        int width_tmp = o.outWidth, height_tmp = o.outHeight;
	        int scale = 1;
	        while (true) {
	            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
	                break;
	            }
	            width_tmp /= 2;
	            height_tmp /= 2;
	            scale *= 2;
	        }

	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        return BitmapFactory.decodeStream(context.getContentResolver()
	                .openInputStream(selectedImage), null, o2);
	    }
		
		// button create image
		/*public void onCreateImage(View v){
			Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

			if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
					== PackageManager.PERMISSION_DENIED
					&&
					ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
							== PackageManager.PERMISSION_DENIED  ){
				ActivityCompat.requestPermissions(AddProjectActivity.this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
			}else{
				permission_camera = true;
			}

			if(permission_camera){
				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					// Create the File where the photo should go
					File photoFile = null;
					try {
						photoFile = createImageFile();
					} catch (IOException ex) {
						Log.d("IMAGE onCreate", ex.toString());
					}

					if (photoFile != null) {
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(photoFile));
						startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
					}
					//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
				}

			}
			else {
				Toast.makeText(AddProjectActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
			}
		}*/

		private File createImageFile() throws IOException {

			if(permission_camera ){
				// Create an image file name
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
				String imageFileName = "JPEG_" + timeStamp + "_";
				File storageDir = Environment.getExternalStoragePublicDirectory(
						Environment.DIRECTORY_PICTURES);
				File image = File.createTempFile(
						imageFileName,  /* prefix */
						".jpg",         /* suffix */
						storageDir      /* directory */
				);

				// Save a file: path for use with ACTION_VIEW intents
				mCurrentPhotoPath = "file:" + image.getAbsolutePath();
				mCurrentPhotoPathAbsolute = image.getAbsolutePath();
				return image;
			}else {
				return null;
			}
		}
		
		// button search image
		public void onSearchImage(View v){
			Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
			
			Intent intent = new Intent();

			intent.setAction(Intent.ACTION_GET_CONTENT);

			intent.addCategory(Intent.CATEGORY_OPENABLE);

			intent.setType("image/*");

			startActivityForResult(intent, REQUEST_BROWSE_PHOTO);
		}
		
		// Methods - set visible the preview image
			private void AttachBitmapToReport()
			{
			    int photoW = myBitmap.getWidth();
			    int photoH = myBitmap.getHeight();   	
			    int scaleFactor = Math.min(photoW/100, photoH/100);
			    int NewW = photoW/scaleFactor;
			    int NewH = photoH/scaleFactor;
			    
				imageView1.setVisibility(View.VISIBLE);		
		    	imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
			}
			
			
			@Override
			protected void onActivityResult(int requestCode, int resultCode, Intent data) 
			{
				InputStream stream = null;

			    if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
			    {
			    	myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			    	AttachBitmapToReport();
			    }
			    	    
		        if (requestCode == REQUEST_BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
		        {
		        	
		            try 
		            {
		            	stream = getContentResolver().openInputStream(data.getData());
		            	myBitmap = BitmapFactory.decodeStream(stream);
		            	AttachBitmapToReport();
		            } 
		            catch (Exception e)
		            {
		            	e.printStackTrace();
		            }

		            if (stream != null) 
		            {
		            	try
		            	{
		            		stream.close();
		            	} 
		            	catch (Exception e) 
		            	{
		            		e.printStackTrace();
		            	}
		            }            
		        }
			}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
