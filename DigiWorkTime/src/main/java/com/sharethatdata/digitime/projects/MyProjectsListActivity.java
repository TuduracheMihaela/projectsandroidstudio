package com.sharethatdata.digitime.projects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.SettingsActivity;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.GridViewAdapter;
import com.sharethatdata.digitime.tasks.MyTasksListActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cProject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
//import android.widget.SearchView;
//import android.widget.SearchView.OnQueryTextListener;

public class MyProjectsListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;

	TimeUtils MyTimeUtils = null;

	ProgressDialog progressDialog;
	
	private String myUser = "";
	private String myPass = "";
	
	ArrayList<HashMap<String, String>> projectList;
	
	GridView gv = null;
	SearchView searchView;
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.grid_view_adapter);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		gv = (GridView) findViewById(R.id.gridView);
		//gv.setTextFilterEnabled(true);
		
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyProjectsListActivity.this, MyProjectsListActivity.this);
		
		new LoadList().execute();
		
		// phone screen
	     if(getResources().getDisplayMetrics().widthPixels>getResources().getDisplayMetrics().heightPixels) {
	    	 // Landscape mode
	    	 gv.setColumnWidth(320);
	     }else{
	    	 // Portrait mode
	    	 gv.setColumnWidth(350);
	     }
	     
	     // tablet screen
	     boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
	     if (tabletSize) {
	         // is tablet
	    	 if(getResources().getDisplayMetrics().widthPixels>getResources().getDisplayMetrics().heightPixels) {
	    		 // Landscape mode
	    		 gv.setColumnWidth(320);
	         }else{
	        	 // Portrait mode
	        	 gv.setColumnWidth(320);
	    	 }
	    	 
	     } else {
	         // is not tablet
	    	 
	     }
		
		gv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					
				HashMap<String, String> itemsList = (HashMap<String, String>) projectList.get(position);
				 String itemId = (String) itemsList.get("id");
				 //String image_base64 = (String) itemsList.get("image_string");
				 
				 Intent intent = new Intent(MyProjectsListActivity.this, MyProjectsDetailActivity.class);
				 intent.putExtra("ID", itemId);
				// intent.putExtra("image_string", image_base64);
				 startActivity(intent);
				
			}
	    });
		
	}
	
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String update = Globals.getValue("project_update");
	    if(update != ""){
	    	Globals.setValue("project_update", "");
			Globals.setValue("load_project_spinner", "true");
	    	new LoadList().execute();
	    }
	    Globals.setValue("edit","");
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();
		super.onDestroy();
	}
	
	static List<cProject> listProjectFiltered;
	static private ArrayList<cProject> arraylistProjects;
	
	public static List<cProject> filter(String charText) {
	    charText = charText.toLowerCase(Locale.getDefault());
	    listProjectFiltered = new ArrayList<cProject>();
	    listProjectFiltered.clear();
	    if (charText.length() == 0) {
	    	listProjectFiltered.addAll(arraylistProjects);
	    } else {
	        for (cProject project : arraylistProjects) {
	            if (project.name.toLowerCase(Locale.getDefault()).contains(charText) 
	            	|| project.code.toLowerCase(Locale.getDefault()).contains(charText)
	            	|| project.created.toLowerCase(Locale.getDefault()).contains(charText) 
	            	|| project.deadline.toLowerCase(Locale.getDefault()).contains(charText)
	            	|| project.description.toLowerCase(Locale.getDefault()).contains(charText)) 
	            {
	            	listProjectFiltered.add(project);
	            }
	        }
	    }
		return listProjectFiltered;
	}
	
	@Override
    public boolean onQueryTextSubmit(String query) {
    	// User pressed the search button
    	
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
    	// User changed the text
    	
    	String text = newText.toString().toLowerCase(Locale.getDefault());
    	List<cProject> filter_projects = filter(text.toString()); 
    	projectList.clear(); // <--- clear the list before add
    	
    	for(cProject entry : filter_projects)
   	 	{

           HashMap<String, String> map = new HashMap<String, String>();

           map.put("id",  Long.toString(entry.id));
           map.put("image_id", entry.image_id);
           map.put("name", entry.name);  
           map.put("code", entry.code);
           map.put("created", entry.created);
           map.put("description", entry.description);

           projectList.add(map);
   	 	}
    	
    	GridViewAdapter adapter = new GridViewAdapter(MyProjectsListActivity.this, R.layout.grid_view_item, projectList);
		gv.setAdapter(null);	
		gv.setAdapter(adapter);
    	
        return true;
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// check manager
				String isManager = Globals.getValue("manager");
				if (isManager == "yes")
				{
					getMenuInflater().inflate(R.menu.project_menu, menu);
				}else{
					getMenuInflater().inflate(R.menu.search_project, menu);
				}
				
				searchView = (SearchView) menu.findItem(R.id.search).getActionView();
				searchView.setIconifiedByDefault(false);
				searchView.setOnQueryTextListener(this);
				searchView.setSubmitButtonEnabled(false);
				searchView.setQueryHint(getString(R.string.search_title_project));
		
		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_add_project:
				Intent intent = new Intent(this, AddProjectActivity.class); startActivity(intent);
				return true;
			case R.id.action_tasks:
				Intent intent1 = new Intent(this, MyTasksListActivity.class); startActivity(intent1);
				return true;
			case R.id.action_settings:
				Intent intent2 = new Intent(this, SettingsActivity.class); startActivity(intent2);
				return true;
			default:
				return super.onOptionsItemSelected(item);
        }
    }

	
	/**
	 * Background Async Task to Load all product by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
		 showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals)getApplication());
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		    projectList = new ArrayList<HashMap<String, String>>();  
		    arraylistProjects = new ArrayList<cProject>();
		    
		    List<cProject> projects = MyProvider.getProjects();
	    	 for(cProject entry : projects)
	    	 {

                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("image_id", entry.image_id);
                map.put("name", entry.name);  
                map.put("code", entry.code);
                map.put("created", entry.created);
                map.put("deadline", entry.deadline);
                map.put("description", entry.description);

                projectList.add(map);

	    	 }
	    	 
	    	 arraylistProjects.addAll(projects);
	    	 
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
			if (MyProjectsListActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			hideProgressDialog();
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	
	            	GridViewAdapter adapter = new GridViewAdapter(MyProjectsListActivity.this, R.layout.grid_view_item, projectList);
        			gv.setAdapter(adapter);	

	            }
	        });     
	    }
	}

	/**
	 * Shows a Progress Dialog
	 *
	 * @param msg
	 */
	private void showProgressDialog(String msg)
	{

		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);

			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);

			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();
	}


	/**
	 * Hides the Progress Dialog
	 */
	private void hideProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

}
