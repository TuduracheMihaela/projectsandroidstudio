package com.sharethatdata.digitime.projects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class EditNoteProject extends AppCompatActivity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;
	
	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	

	private String myUser = "";
	private String myPass = "";
	
	//ProgressDialog progressDialog;

	TextInputLayout noteNameInputLayout;
	EditText noteNameEditText;
	
	ImageView imageViewNote;
	Button btnCreateImage;
	Button btnSearchImage;
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;
	
	Button btnEdit;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_note);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(EditNoteProject.this, EditNoteProject.this);

		noteNameInputLayout = (TextInputLayout) findViewById(R.id.name_text_input_layout);
		noteNameEditText = (EditText) findViewById(R.id.textNoteName);
		
		TextView textNoteNameView = (TextView) findViewById(R.id.textNoteName);
		TextView descriptionView = (TextView) findViewById(R.id.textNoteDescription);
		
		EditText editDesc = (EditText) findViewById(R.id.textNoteDescription);       
	    editDesc.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.textNoteDescription) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                    }
                }
                return false;
            }
        });
		
		String edit = Globals.getValue("note_edit");
		String noteid = Globals.getValue("noteid");
		String note_name = Globals.getValue("textViewName");
		String note_description = Globals.getValue("textViewDescription");
		String image_string = Globals.getValue("image_string");
		
		textNoteNameView.setText(note_name);
		descriptionView.setText(Html.fromHtml(note_description));
		
		imageViewNote = (ImageView) findViewById(R.id.imageViewNote);
		  
	    Bitmap d = ConvertByteArrayToBitmap(image_string);
	    if (d != null)
	    {
		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
		  imageViewNote.setImageBitmap(scaled);
		  				  
	    }
		
		
	}

	private boolean shouldShowError() {
		int emailLength = noteNameEditText.getText().length();
		return emailLength <= 0 ;
	}

	private void showError() {
		noteNameInputLayout.setErrorEnabled(true);
		noteNameInputLayout.setError(getString(R.string.text_project_note_name_required));
	}

	private void hideError() {
		noteNameInputLayout.setErrorEnabled(false);
		noteNameInputLayout.setError("");
	}

	private void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
					hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onEditNote(View v){
		boolean cancel = false;
		View focusView = null;

		boolean textLenght = shouldShowError();

		if (textLenght) {
			showError();
			focusView = noteNameEditText;
			cancel = true;
		}

		if(cancel){
			focusView.requestFocus();
		}else{
			hideError();
		}

		if(!cancel){
			hideKeyboard();

			String noteid = Globals.getValue("noteid");
			new EditNoteTask(noteid).execute();
		}

	}
    
    private class EditNoteTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public EditNoteTask(String id) {
		     myID = id;
		  }

		  protected Boolean doInBackground(String... urls) {
        	
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			// extract data
			  			  
		      TextView textProjectNameView = (TextView) findViewById(R.id.textNoteName);
		      String textProjectName = textProjectNameView.getText().toString();	
		      
		      TextView DescriptionView = (TextView) findViewById(R.id.textNoteDescription);
		      String description = Html.toHtml((Spanned) DescriptionView.getText());		
		      
		      int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_project_item", bitmapByteArray, bitmapByteArrayThumbnail);
			  }		  
			  
			  suc = true;

			  
			  if (suc)
			  {
				  suc = MyProvider.editNote(String.valueOf(myID), textProjectName, description , "" + BitmapID);
			  }			  
		  
        	  return suc;
  			  
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Project updated", Toast.LENGTH_SHORT).show();
				  Globals.setValue("note_update", "note_update");
				  Globals.setValue("edit","");
				  finish();
				  
			  }else{
				  Toast.makeText(getApplicationContext(), "Was not updated project", Toast.LENGTH_SHORT).show();
				  Globals.setValue("note_update", "note_update");
				  Globals.setValue("edit","");
				  finish();
			  }
			  
			  
		  }
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}
	
	// button create image
	public void onCreateImage(View v)
	{
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();
		
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
		// Create the File where the photo should go
		File photoFile = null;
		try
		{
			photoFile = createImageFile();
		} 
		catch (IOException ex) 
		{
		
		}
		
		if (photoFile != null) 
		{
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
			Uri.fromFile(photoFile));
			startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
		}
		
		//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
		imageFileName,  /* prefix */
		".jpg",         /* suffix */
		storageDir      /* directory */
		);
		
		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		mCurrentPhotoPathAbsolute = image.getAbsolutePath();
		return image;
	}
	
	// button search image
	public void onSearchImage(View v){
		Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
		
		Intent intent = new Intent();
		
		intent.setAction(Intent.ACTION_GET_CONTENT);
		
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		
		intent.setType("image/*");
		
		startActivityForResult(intent, REQUEST__BROWSE_PHOTO);
	}
	
	// Methods - set visible the preview image
	private void AttachBitmapToReport()
	{
		int photoW = myBitmap.getWidth();
		int photoH = myBitmap.getHeight();   	
		int scaleFactor = Math.min(photoW/100, photoH/100);
		int NewW = photoW/scaleFactor;
		int NewH = photoH/scaleFactor;
		
		imageViewNote.setVisibility(View.VISIBLE);		
		imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		InputStream stream = null;
		
		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
		{
			myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			AttachBitmapToReport();
		}
		
		if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK) 
		{
			
			try 
			{
				stream = getContentResolver().openInputStream(data.getData());
				myBitmap = BitmapFactory.decodeStream(stream);
				AttachBitmapToReport();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			if (stream != null) 
			{
				try
				{
					stream.close();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}            
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
