package com.sharethatdata.digitime.projects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.json.JSONObject;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.DetailActivity;
import com.sharethatdata.digitime.HolidaysListFragment;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.RecordTimeActivity;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.HolidaysAdapter;
import com.sharethatdata.digitime.adapter.NotesAdapter;
import com.sharethatdata.digitime.tasks.MyTasksDetailActivity;
import com.sharethatdata.digitime.tasks.MyTasksListActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cHolidays;
import com.sharethatdata.webservice.datamodel.cImage;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectItem;
import com.sharethatdata.webservice.datamodel.cProjectMember;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MyProjectsDetailActivity extends AppCompatActivity {

	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;

	TimeUtils MyTimeUtils = null;

	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;

	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;

	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
	private String myUserID = "";

	ArrayList<HashMap<String, String>> notesList;

	private static ProgressDialog pDialog;

	private String id_project = "";
	private String image_string = "";

	TextInputLayout projectNoteNameInputLayout;
	EditText projectNoteNameEditText;

	TextView textViewName;
	TextView textViewCode;
	TextView textViewOwner;
	TextView textViewCreated;
	TextView textViewDeadline;
	TextView textViewStatus;
	TextView textViewPrivacy;
	TextView textViewCostplace;
	TextView textViewMembers;
	TextView textViewDescription;

	Button buttonEditProject;
	Button buttonDeleteProject;

	ListView lv = null;

	ImageView imageView1;
	int width = 400;
	int height = 320;
	int padding = 30;

	// Notes
	ImageView imageViewNote;
	Button btnCreateImage;
	Button btnSearchImage;
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;

	String myImageData = "";

	// For zoom image /////////////////////////
	// Hold a reference to the current animator,
	// so that it can be canceled mid-way.
	private Animator mCurrentAnimator;

	// The system "short" animation time duration, in milliseconds. This
	// duration is ideal for subtle animations or animations that occur
	// very frequently.
	private int mShortAnimationDuration;

	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.grid_view_detail);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged

		MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyProjectsDetailActivity.this, MyProjectsDetailActivity.this);

		projectNoteNameInputLayout = (TextInputLayout) findViewById(R.id.title_text_input_layout);
		projectNoteNameEditText = (EditText) findViewById(R.id.textNoteName);

		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewCode = (TextView) findViewById(R.id.textViewCode);
		textViewOwner = (TextView) findViewById(R.id.textViewOwner);
		textViewCreated = (TextView) findViewById(R.id.textViewCreated);
		textViewDeadline = (TextView) findViewById(R.id.textViewDeadline);
		textViewStatus = (TextView) findViewById(R.id.textViewStatus);
		textViewPrivacy = (TextView) findViewById(R.id.textViewPrivacy);
		textViewCostplace = (TextView) findViewById(R.id.textViewCostplace);
		textViewMembers = (TextView) findViewById(R.id.textViewMembers);
		textViewDescription = (TextView) findViewById(R.id.textViewDescription);
		buttonEditProject = (Button) findViewById(R.id.buttonEditProject);
		buttonDeleteProject = (Button) findViewById(R.id.buttonDeleteProject);
		lv = (ListView) findViewById(R.id.listViewNotes);

		imageViewNote = (ImageView) findViewById(R.id.imageViewNote);
		imageViewNote.setVisibility(View.GONE);
		btnCreateImage = (Button) findViewById(R.id.pictureFromPhone);
		btnSearchImage = (Button) findViewById(R.id.pictureCamera);

		EditText editDesc = (EditText) findViewById(R.id.textNoteDescription);
		editDesc.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View view, MotionEvent event) {
				// TODO Auto-generated method stub
				if (view.getId() ==R.id.textNoteDescription) {
					view.getParent().requestDisallowInterceptTouchEvent(true);
					switch (event.getAction()&MotionEvent.ACTION_MASK){
						case MotionEvent.ACTION_UP:
							view.getParent().requestDisallowInterceptTouchEvent(false);
							break;
					}
				}
				return false;
			}
		});

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String valueID = extras.getString("ID");
			id_project = valueID;
		}

		new LoadProjectItem().execute();

		new LoadListNotes().execute();


		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				HashMap<String, String> itemsList = (HashMap<String, String>) notesList.get(position);
				String itemId = (String) itemsList.get("id");

				Intent intent = new Intent(MyProjectsDetailActivity.this, NoteDetailsActivity.class);
				intent.putExtra("edit_note", "edit_note");

				intent.putExtra("ID", itemId);
				intent.putExtra("ID_project", id_project);
				startActivity(intent);
			}
		});


		imageView1 = (ImageView) findViewById(R.id.imageViewProject);
		imageView1.setOnClickListener(new View.OnClickListener() {

			@RequiresApi(api = Build.VERSION_CODES.M)
			@Override
			public void onClick(View v) {
				Log.d("Proj Detail Image", myImageData);
				String image_string = myImageData;
				//if (TextUtils.isEmpty(image_string) || image_string.contains("null")){
				if (TextUtils.isEmpty(image_string)){
					Log.d("Proj Detail Image", myImageData);
				} else {

					byte[] decodedString;
					if(myImageData != ""){
						String resizeString =  resizeBase64Image(myImageData);
						decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
					}else{
						decodedString = Base64.decode(myImageData, Base64.NO_WRAP);
					}


					Bitmap bitmapDecodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
					if (bitmapDecodedByte != null)
					{
						//expandedImageView.setImageBitmap(decodedByte);

						Bitmap d = ConvertByteArrayToBitmap(myImageData);

						int imageWidth = d.getWidth();
						int imageHeight = d.getHeight();

                        AnimatorSet set = new AnimatorSet();
                        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                        if(tabletSize){
                            // is tablet

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int newHeight = displayMetrics.heightPixels;
                            int width = displayMetrics.widthPixels;

                            //int newWidth = getScreenWidth(); //this method should return the width of device screen.
                            float scaleFactor = (float)newHeight/(float)imageHeight;
                            int newWidth = (int)(imageWidth * scaleFactor);

                            bitmapDecodedByte = Bitmap.createScaledBitmap(bitmapDecodedByte, newWidth, newHeight, true);

                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(MyProjectsDetailActivity.this);
                            View mView = getLayoutInflater().inflate(R.layout.grid_view_detail_zoom_image, null);
                            PhotoView photoView = mView.findViewById(R.id.imageView);
							RelativeLayout relativeLayout = mView.findViewById(R.id.relativeLayout);
							//photoView.setImageResource(R.drawable.message);
                            photoView.setImageBitmap(bitmapDecodedByte);
							mView.setBackgroundColor(getColor(R.color.orange4));
							RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(newWidth, newHeight);
							relativeLayout.setLayoutParams(params);
							mBuilder.setView(mView);
                            AlertDialog mDialog = mBuilder.create();
                            mDialog.show();

                        }else{
                            // is phone

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int height = displayMetrics.heightPixels;
                            int newWidth = displayMetrics.widthPixels;

                            //int newWidth = getScreenWidth(); //this method should return the width of device screen.
                            float scaleFactor = (float)newWidth/(float)imageWidth;
                            int newHeight = (int)(imageHeight * scaleFactor);

                            bitmapDecodedByte = Bitmap.createScaledBitmap(bitmapDecodedByte, newWidth, newHeight, true);

                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(MyProjectsDetailActivity.this);
                            View mView = getLayoutInflater().inflate(R.layout.grid_view_detail_zoom_image, null);
                            PhotoView photoView = mView.findViewById(R.id.imageView);
                            //photoView.setImageResource(R.drawable.message);
                            photoView.setImageBitmap(bitmapDecodedByte);
                            mBuilder.setView(mView);
                            AlertDialog mDialog = mBuilder.create();
                            mDialog.show();
                        }
					}



					//zoom(imageView1, myImageData);

					//zoomImageFromThumb(imageView1, myImageData);


					/*DisplayMetrics metrics = new DisplayMetrics();
					getWindowManager().getDefaultDisplay().getMetrics(metrics);
					float screen_width = metrics.widthPixels;

					float scale =  screen_width / v.getWidth();
					if(v.getScaleX() == 1) {
						v.setScaleY(scale);
						v.setScaleX(scale);
					}else{
						v.setScaleY(1);
						v.setScaleX(1);
					}*/
				}
			}
		});



		// Retrieve and cache the system's default "short" animation time.
		mShortAnimationDuration = getResources().getInteger(
				android.R.integer.config_shortAnimTime);
	}




	// Scale and maintain aspect ratio given a desired width
	// BitmapScaler.scaleToFitWidth(bitmap, 100);
	public static Bitmap scaleToFitWidth(Bitmap b, int width)
	{
		float factor = width / (float) b.getWidth();
		return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
	}


	// Scale and maintain aspect ratio given a desired height
	// BitmapScaler.scaleToFitHeight(bitmap, 100);
	public static Bitmap scaleToFitHeight(Bitmap b, int height)
	{
		float factor = height / (float) b.getHeight();
		return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), height, true);
	}






	@Override
	public void onResume() {
		super.onResume();

		String updateProject = Globals.getValue("project_update");
		if(updateProject != ""){
			Globals.setValue("project_update", "");
			new LoadProjectItem().execute();
		}
		Globals.setValue("edit_project","");

		// Note
		String updateNote = Globals.getValue("note_update");
		if(updateNote != ""){
			Globals.setValue("note_update", "");
			new LoadListNotes().execute();
		}
	}

	private boolean shouldShowError() {
		int emailLength = projectNoteNameEditText.getText().length();
		return emailLength <= 0 ;
	}

	private void showError() {
		projectNoteNameInputLayout.setErrorEnabled(true);
		projectNoteNameInputLayout.setError(getString(R.string.text_project_note_name_required));
	}

	private void hideError() {
		projectNoteNameInputLayout.setErrorEnabled(false);
		projectNoteNameInputLayout.setError("");
	}

	private void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
					hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Globals.setValue("project_update", "project_update");
			finish();
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	public void onClickEditProject(View v){

		Globals.setValue("edit_project", "edit_project");
		Globals.setValue("projectid", id_project.toString());
		Globals.setValue("textViewName", textViewName.getText().toString());
		Globals.setValue("textViewCode", textViewCode.getText().toString());
		Globals.setValue("textViewOwner", textViewOwner.getText().toString());
		Globals.setValue("textViewCreated", textViewCreated.getText().toString());
		Globals.setValue("textViewDeadline", textViewDeadline.getText().toString());
		Globals.setValue("textViewStatus", textViewStatus.getText().toString());
		Globals.setValue("textViewPrivacy", textViewPrivacy.getText().toString());
		Globals.setValue("textViewCostplace", textViewCostplace.getText().toString());
		Globals.setValue("textViewMembers", textViewMembers.getText().toString());
		Globals.setValue("textViewDescription", Html.toHtml((Spanned)textViewDescription.getText()));
		Globals.setValue("image_string", image_string);

		Intent i = new Intent(MyProjectsDetailActivity.this, AddProjectActivity.class);
		startActivity(i);
	}

	public void onClickDeleteProject(View v){
		new AlertDialog.Builder(MyProjectsDetailActivity.this)
				.setTitle("Delete project")
				.setMessage("Do you really want to delete this project?")
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {

						new DeleteProjectTask(id_project).execute();

					}})
				.setNegativeButton(android.R.string.no, null).show();
	}


	public void onAddNote(View v){
		boolean cancel = false;
		View focusView = null;

		boolean textLenght = shouldShowError();

		if (textLenght) {
			showError();
			focusView = projectNoteNameEditText;
			cancel = true;
		}

		if(cancel){
			focusView.requestFocus();
		}else{
			hideError();
		}

		if(!cancel){
			hideKeyboard();

			new RegisterNoteTask().execute();
		}

	}

	/**
	 * Background Async Task to Load Project Item by making HTTP Request
	 * */
	class LoadProjectItem extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MyProjectsDetailActivity.this);
			pDialog.setTitle("Loading Project");
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			final cProject project = MyProvider.getProject(id_project);

			final List<cProjectMember> projectMember = MyProvider.getProjectMemberList(id_project);

/*      	    	if (project != null)
					  {
						  if (project.image_id != "0")
						  {
							  myImageData = MyRoboProvider.getImage(project.image_id);
							  image_string = myImageData;
						  }
					  }*/

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					textViewName.setText(Html.fromHtml(project.name));
					textViewCode.setText(Html.fromHtml(project.code));
					textViewOwner.setText(Html.fromHtml(project.owner));
					textViewCreated.setText(Html.fromHtml(project.created));
					textViewDeadline.setText(Html.fromHtml(project.deadline));
					textViewStatus.setText(Html.fromHtml(project.status));
					textViewCostplace.setText(Html.fromHtml(project.costplace));
					if(project.privacy.contains("1")){
						textViewPrivacy.setText(Html.fromHtml("Public"));
					}else {
						textViewPrivacy.setText(Html.fromHtml("Private"));
					}
					textViewDescription.setText(Html.fromHtml(project.description));

					Log.d("Proj Detail Image", project.full_image);

					if (project.full_image != "")
					{
						myImageData = project.full_image;
						image_string = myImageData;
					}

					StringBuilder builder = new StringBuilder();
					for(cProjectMember member : projectMember){
						builder.append(member.name + "\n" + "; " );
					}
					textViewMembers.setText(Html.fromHtml(builder.toString()));

					ImageView imageView1 = (ImageView) findViewById(R.id.imageViewProject);

					Bitmap d = ConvertByteArrayToBitmap(project.full_image);
					if (d != null)
					{
						int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
						Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
						imageView1.setImageBitmap(scaled);

					}

					String x = project.owner;
					String y = myUserName;
					if(project.owner.equals(myUserName)){
						buttonEditProject.setVisibility(View.VISIBLE);
						buttonDeleteProject.setVisibility(View.VISIBLE);
					}
				}
			});



			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			// dismiss the dialog after getting all products

			super.onPostExecute(result);

			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();


		}
	}


	private class DeleteProjectTask extends AsyncTask<String, String, Boolean> {
		String myID = "";

		public DeleteProjectTask(String id) {
			myID = id;
		}

		protected Boolean doInBackground(String... urls) {

			boolean suc = false;

			suc = MyProvider.deleteProject(myID);

			return suc;
		}

		protected void onPostExecute(Boolean result) {

			if(result == true){
				Toast.makeText(getApplicationContext(), "Project deleted", Toast.LENGTH_SHORT).show();
				Globals.setValue("project_update", "project_update");
				finish();

			}else{
				Toast.makeText(getApplicationContext(), "Was not deleted the project", Toast.LENGTH_SHORT).show();
				Globals.setValue("project_update", "project_update");
				finish();
			}


		}
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Background Async Task to Load notes by making HTTP Request
	 * */
	class LoadListNotes extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			// load data from provider
			notesList = new ArrayList<HashMap<String, String>>();

			List<cProjectItem> note = MyProvider.getProjectNotes(id_project);
			for(cProjectItem entry : note)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				String[] token = entry.created.split("\\s");
				String value = token[0]+token[1];
				String date = token[0];
				String time = token[1];

				map.put("date", date);
				map.put("time" , time );

				map.put("id",  String.valueOf(entry.id));
				map.put("title", entry.title);
				map.put("entry", entry.entry);
				map.put("owner", entry.owner);

				// adding HashList to ArrayList
				notesList.add(map);
			}

			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					/**
					 * Updating parsed JSON data into ListView
					 * */
					NotesAdapter adapter = null;

					adapter = new NotesAdapter(MyProjectsDetailActivity.this, R.layout.grid_view_detail_items_notes, notesList);
					lv.setAdapter(adapter);
					setListViewHeightBasedOnChildren(lv);


				}
			});
		}
	}


	// for listview in scrollview (without this method, the listview won't scroll)
	public static void setListViewHeightBasedOnChildren(ListView listView)
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++)
		{
			view = listAdapter.getView(i, view, listView);

			if (i == 0)
				view.setLayoutParams(new LayoutParams(desiredWidth,
						LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();

		}

		LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

		listView.setLayoutParams(params);
		listView.requestLayout();

	}


	/** AsyncTask register note  */
	private class RegisterNoteTask extends AsyncTask<String, String, Boolean>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MyProjectsDetailActivity.this);
			pDialog.setTitle("Loading");
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... args) {

			EditText textNoteNameView = (EditText) findViewById(R.id.textNoteName);
			String textNoteName = textNoteNameView.getText().toString();

			EditText DescriptionView = (EditText) findViewById(R.id.textNoteDescription);
			String description =  DescriptionView.getText().toString();


			int BitmapID = 0;
			int ProjectID = 0;
			boolean sucImage = false;
			boolean sucNews = false;
			boolean suc = false;
			if (myBitmap != null)
			{
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				bitmapByteArray = stream.toByteArray();

				byte[] bitmapByteArrayThumbnail;
				bitmapByteArrayThumbnail = bitmapByteArray;
				myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				bitmapByteArrayThumbnail = stream.toByteArray();

				BitmapID = MyProvider.createImage("image_project_item", bitmapByteArray, bitmapByteArrayThumbnail);
				suc = true;
			}

			suc = MyProvider.createProjectNotes(id_project, textNoteName, description, String.valueOf(BitmapID), myUser);

			return suc;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(final Boolean success) {

			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {

					if (success == false)
					{
						Toast.makeText(MyProjectsDetailActivity.this, "The project note was not created", Toast.LENGTH_SHORT).show();
					}else{
						//Intent intent = new Intent(getApplicationContext(), MyProjectsListActivity.class); startActivity(intent);
						Toast.makeText(MyProjectsDetailActivity.this, "The project note was created", Toast.LENGTH_SHORT).show();
						TextView textNoteNameView = (TextView) findViewById(R.id.textNoteName);
						textNoteNameView.setText("");
						TextView DescriptionView = (TextView) findViewById(R.id.textNoteDescription);
						DescriptionView.setText("");
						imageViewNote.setImageBitmap(null);

						new LoadListNotes().execute();
					}

				}
			});
		}
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		Bitmap decodedByte = null;
		if(b.isEmpty() || b.contentEquals(""))
		{
			decodedByte = null;
		}else{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		}

		return decodedByte;
	}

	// button create image
	public void onCreateImage(View v){
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try
			{
				photoFile = createImageFile();
			}
			catch (IOException ex)
			{
				System.out.println(ex.toString());
			}

			if (photoFile != null)
			{
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}

			//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
				imageFileName,  /* prefix */
				".jpg",         /* suffix */
				storageDir      /* directory */
		);

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		mCurrentPhotoPathAbsolute = image.getAbsolutePath();
		return image;
	}

	// button search image
	public void onSearchImage(View v){
		Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();

		Intent intent = new Intent();

		intent.setAction(Intent.ACTION_GET_CONTENT);

		intent.addCategory(Intent.CATEGORY_OPENABLE);

		intent.setType("image/*");

		startActivityForResult(intent, REQUEST__BROWSE_PHOTO);
	}

	// Methods - set visible the preview image
	private void AttachBitmapToReport()
	{
		int photoW = myBitmap.getWidth();
		int photoH = myBitmap.getHeight();
		int scaleFactor = Math.min(photoW/100, photoH/100);
		int NewW = photoW/scaleFactor;
		int NewH = photoH/scaleFactor;

		imageViewNote.setVisibility(View.VISIBLE);
		imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		InputStream stream = null;

		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
		{
			myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			AttachBitmapToReport();
		}

		if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
		{

			try
			{
				stream = getContentResolver().openInputStream(data.getData());
				myBitmap = BitmapFactory.decodeStream(stream);
				AttachBitmapToReport();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			if (stream != null)
			{
				try
				{
					stream.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public String resizeBase64Image(String base64image){
		int IMG_WIDTH = 800;
		int IMG_HEIGHT = 600;
		byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
		BitmapFactory.Options options=new BitmapFactory.Options();
		options.inPurgeable = true;
		Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


		if(image.getHeight() <= 400 && image.getWidth() <= 400){
			return base64image;
		}
		image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

		ByteArrayOutputStream baos=new  ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.PNG,100, baos);

		byte [] b=baos.toByteArray();
		System.gc();
		return Base64.encodeToString(b, Base64.NO_WRAP);

	}

	private void zoom(final View thumbView, String imageResId){

		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		int screenHeight = size.y;


		// Load the high-resolution "zoomed-in" image.
		final ImageView expandedImageView = (ImageView) findViewById(R.id.expanded_image_project);

		byte[] decodedString;
		if(imageResId != ""){
			String resizeString =  resizeBase64Image(imageResId);
			decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
		}else{
			decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
		}

		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		if (decodedByte != null)
		{
			expandedImageView.setImageBitmap(decodedByte);
		}

		expandedImageView.setVisibility(View.VISIBLE);

		// Set the pivot point for SCALE_X and SCALE_Y transformations
		// to the top-left corner of the zoomed-in view (the default
		// is the center of the view).
		expandedImageView.setPivotX(0f);
		expandedImageView.setPivotY(0f);

		LayoutParams params = expandedImageView.getLayoutParams();
		params.height = 2000 ;

	}

	private void zoomImageFromThumb(final View thumbView, String imageResId) {
		// If there's an animation in progress, cancel it
		// immediately and proceed with this one.
		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();
		}

		// Load the high-resolution "zoomed-in" image.
		final ImageView expandedImageView = (ImageView) findViewById(
				R.id.expanded_image_project);

		//
		byte[] decodedString;
		if(imageResId != ""){
			String resizeString =  resizeBase64Image(imageResId);
			decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
		}else{
			decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
		}


		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		if (decodedByte != null)
		{
			//int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
			//Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
			expandedImageView.setImageBitmap(decodedByte);

		}


		// Calculate the starting and ending bounds for the zoomed-in image.
		// This step involves lots of math. Yay, math.
		final Rect startBounds = new Rect();
		final Rect finalBounds = new Rect();
		final Point globalOffset = new Point();


		// The start bounds are the global visible rectangle of the thumbnail,
		// and the final bounds are the global visible rectangle of the container
		// view. Also set the container view's offset as the origin for the
		// bounds, since that's the origin for the positioning animation
		// properties (X, Y).
		thumbView.getGlobalVisibleRect(startBounds);
		findViewById(R.id.scrollViewProjects).getGlobalVisibleRect(finalBounds, globalOffset); // the container
		//findViewById(R.id.relativeLayoutProjects).getGlobalVisibleRect(finalBounds, globalOffset); // the container


		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float screen_width = metrics.widthPixels;

		float scale =  screen_width / findViewById(R.id.scrollViewProjects).getWidth();
		if(findViewById(R.id.scrollViewProjects).getScaleX() == 1) {
			findViewById(R.id.scrollViewProjects).setScaleY(scale);
			findViewById(R.id.scrollViewProjects).setScaleX(scale);
		}else{
			findViewById(R.id.scrollViewProjects).setScaleY(1);
			findViewById(R.id.scrollViewProjects).setScaleX(1);
		}*/


		startBounds.offset(-globalOffset.x, -globalOffset.y);
		finalBounds.offset(-globalOffset.x, -globalOffset.y);

		// Adjust the start bounds to be the same aspect ratio as the final
		// bounds using the "center crop" technique. This prevents undesirable
		// stretching during the animation. Also calculate the start scaling
		// factor (the end scaling factor is always 1.0).
		float startScale;
		if ((float) finalBounds.width() / finalBounds.height()
				> (float) startBounds.width() / startBounds.height()) {
			// Extend start bounds horizontally
			startScale = (float) startBounds.height() / finalBounds.height() * 2;
			float startWidth = startScale * finalBounds.width();
			float deltaWidth = (startWidth - startBounds.width()) / 2;
			startBounds.left -= deltaWidth;
			startBounds.right += deltaWidth;
		} else {
			// Extend start bounds vertically
			startScale = (float) startBounds.width() / finalBounds.width();
			float startHeight = startScale * finalBounds.height();
			float deltaHeight = (startHeight - startBounds.height()) / 2;
			startBounds.top -= deltaHeight;
			startBounds.bottom += deltaHeight;
		}

		float width_s = startBounds.width();
		float height_s = startBounds.height();

		float width_f = finalBounds.width();
		float height_f = finalBounds.height();

		// Hide the thumbnail and show the zoomed-in view. When the animation
		// begins, it will position the zoomed-in view in the place of the
		// thumbnail.
		thumbView.setAlpha(0f);
		expandedImageView.setVisibility(View.VISIBLE);

		// Set the pivot point for SCALE_X and SCALE_Y transformations
		// to the top-left corner of the zoomed-in view (the default
		// is the center of the view).
		expandedImageView.setPivotX(0f);
		expandedImageView.setPivotY(0f);



	    /*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float screen_width = metrics.widthPixels;

		float scale =  screen_width / findViewById(R.id.scrollViewProjects).getWidth();
		if(expandedImageView.getScaleX() == 1) {
			expandedImageView.setScaleY(scale + 3f);
			expandedImageView.setScaleX(scale + 5f);
		}else{
			expandedImageView.setScaleY(1);
			expandedImageView.setScaleX(1);
		}*/


		// Construct and run the parallel animation of the four translation and
		// scale properties (X, Y, SCALE_X, and SCALE_Y).
		// resize image SCALE_X and SCALE_Y -> I should check separate resize phone and tablet

		AnimatorSet set = new AnimatorSet();
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		if (tabletSize) {
			// is tablet
			if(startScale <= 0.4){
				// image portrait

				set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
						startBounds.left, finalBounds.left))
						.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
								startBounds.top, finalBounds.top))
						.with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
								startScale, 0.5f)).with(ObjectAnimator.ofFloat(expandedImageView,
						View.SCALE_Y, startScale, 1f));
				set.setDuration(mShortAnimationDuration);
				set.setInterpolator(new DecelerateInterpolator());
				set.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mCurrentAnimator = null;
					}

					@Override
					public void onAnimationCancel(Animator animation) {
						mCurrentAnimator = null;
					}
				});
				set.start();
			}else if(startScale > 0.4){
				// image landscape = 0.4411277

				set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
						startBounds.left, finalBounds.left))
						.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
								startBounds.top, finalBounds.top))
						.with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
								startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
						View.SCALE_Y, startScale, 1f));
				set.setDuration(mShortAnimationDuration);
				set.setInterpolator(new DecelerateInterpolator());
				set.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mCurrentAnimator = null;
					}

					@Override
					public void onAnimationCancel(Animator animation) {
						mCurrentAnimator = null;
					}
				});
				set.start();
			}

		} else {
			// is phone
			set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
					startBounds.left, finalBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top, finalBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
							startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
					View.SCALE_Y, startScale, 2f));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mCurrentAnimator = null;
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					mCurrentAnimator = null;
				}
			});
			set.start();
		}



		mCurrentAnimator = set;

		// Upon clicking the zoomed-in image, it should zoom back down
		// to the original bounds and show the thumbnail instead of
		// the expanded image.
		final float startScaleFinal = startScale ;
		expandedImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mCurrentAnimator != null) {
					mCurrentAnimator.cancel();
				}

				// Animate the four positioning/sizing properties in parallel,
				// back to their original values.
				AnimatorSet set = new AnimatorSet();
				set.play(ObjectAnimator
						.ofFloat(expandedImageView, View.X, startBounds.left))
						.with(ObjectAnimator
								.ofFloat(expandedImageView,
										View.Y,startBounds.top))
						.with(ObjectAnimator
								.ofFloat(expandedImageView,
										View.SCALE_X, startScaleFinal))
						.with(ObjectAnimator
								.ofFloat(expandedImageView,
										View.SCALE_Y, startScaleFinal));
				set.setDuration(mShortAnimationDuration);
				set.setInterpolator(new DecelerateInterpolator());
				set.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						thumbView.setAlpha(1f);
						expandedImageView.setVisibility(View.GONE);
						mCurrentAnimator = null;
					}

					@Override
					public void onAnimationCancel(Animator animation) {
						thumbView.setAlpha(1f);
						expandedImageView.setVisibility(View.GONE);
						mCurrentAnimator = null;
					}
				});
				set.start();
				mCurrentAnimator = set;
			}
		});
	}
}