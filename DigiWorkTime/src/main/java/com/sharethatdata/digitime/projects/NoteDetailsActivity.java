package com.sharethatdata.digitime.projects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.MyImageLoader;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity.LoadListNotes;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity.LoadProjectItem;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cImage;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectItemDetail;
import com.sharethatdata.webservice.datamodel.cProjectMember;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class NoteDetailsActivity extends AppCompatActivity {
	

	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;

	TimeUtils MyTimeUtils = null;
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;
	
	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	
	private static ProgressDialog pDialog;
	
	String id_note, id_project;
	
	TextView textViewName;
    TextView textViewDate;
    TextView textViewOwner;
    TextView textViewDescription;
    Button editNote;
    Button deleteNote;
    
    
    ImageView imageViewNote;
	Button btnCreateImage;
	Button btnSearchImage;
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;
	String myImageData = "";
	
	// For zoom image /////////////////////////
	// Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid_view_detail_items_notes_details);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		MyProvider = new WSDataProvider(myUser, myPass);
		MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(NoteDetailsActivity.this, NoteDetailsActivity.this);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String valueID = extras.getString("ID");    
		    	 id_note = valueID; 
		    String valueImageId = extras.getString("ID_project");
		    	 id_project = valueImageId;
		}
		
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewDate = (TextView) findViewById(R.id.textViewDate);
		textViewOwner = (TextView) findViewById(R.id.textViewOwner);
		textViewDescription = (TextView) findViewById(R.id.textViewDescription);
		editNote = (Button) findViewById(R.id.buttonEditNote);
		deleteNote = (Button) findViewById(R.id.buttonDeleteNote);
		
		new LoadProjectItem().execute();
		
		 final ImageView imageView1 = (ImageView) findViewById(R.id.imageViewNote);
		 
		 imageView1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 zoomImageFromThumb(imageView1, myImageData);
			}
		});
		 
		 // Retrieve and cache the system's default "short" animation time.
	        mShortAnimationDuration = getResources().getInteger(
	                android.R.integer.config_shortAnimTime);
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Globals.setValue("note_update", "note_update");
	    	finish();
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String updateNote = Globals.getValue("note_update");
	    if(updateNote != ""){
	    	Globals.setValue("note_update", "");
	    	new LoadProjectItem().execute();
	    }
	    Globals.setValue("edit","");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickEditNote(View v){
		    	Globals.setValue("note_edit", "note_edit");
		  		Globals.setValue("noteid", id_note.toString());
		  		Globals.setValue("textViewName", textViewName.getText().toString());
		  		Globals.setValue("textViewDescription", Html.toHtml((Spanned)textViewDescription.getText())); 
		  		Globals.setValue("image_string", myImageData);
		  		
		  		Intent i = new Intent(NoteDetailsActivity.this, EditNoteProject.class);
		  		startActivity(i);

	}
	
	public void onClickDeleteNote(View v){
		 new AlertDialog.Builder(NoteDetailsActivity.this)
		  .setTitle("Delete project")
		  .setMessage("Do you really want to delete this note?")
		  .setIcon(android.R.drawable.ic_dialog_alert)
		  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		      public void onClick(DialogInterface dialog, int whichButton) {
		    	  				      
		    	  new DeleteNoteTask(id_note, id_project).execute();
		          
		      }})
		   .setNegativeButton(android.R.string.no, null).show();
	}
		
		 /**
			 * Background Async Task to Load News Item by making HTTP Request
		  	 * */
			class LoadProjectItem extends AsyncTask<String, String, String> {

		     /**
		      * Before starting background thread Show Progress Dialog
		      * */
		     @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		      // Showing progress dialog
		         pDialog = new ProgressDialog(NoteDetailsActivity.this);
		         pDialog.setTitle("Loading Note");
		         pDialog.setMessage("Please wait...");
		         pDialog.setIndeterminate(false);
		         pDialog.setCancelable(true);
		         pDialog.setCanceledOnTouchOutside(false);
		         pDialog.show();
		     }

			    /**
			     * getting items from url
			     * */
			    protected String doInBackground(String... args) {
			    	
			    	Globals = ((MyGlobals) getApplicationContext());	
					
				    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
					
			    	final cProjectItemDetail note = MyProvider.getDetailProjectItem(id_project, id_note);
			    	
			    	if (note != null)
					  {
						  if (note.image_id != "0")
						  {
							  myImageData = MyRoboProvider.getImage(note.image_id);
						  }
					  }
			    	
			    	runOnUiThread(new Runnable() {
			               @Override
			               public void run() {
			            	   textViewName.setText(Html.fromHtml(note.name));
			            	   textViewDate.setText(Html.fromHtml(note.status));
			            	   textViewOwner.setText(Html.fromHtml(note.owner));    
			            	   textViewDescription.setText(Html.fromHtml(note.subtitle));   
			   		    		
			   		    		ImageView imageView1 = (ImageView) findViewById(R.id.imageViewNote);
			  				  
				  				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
				  				  if (d != null)
				  				  {
				  					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
				  					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
				  					  imageView1.setImageBitmap(scaled);
				  					  				  
				  				  }
				  				  
				  				if(note.owner.equals(myUserName)){
				  					  editNote.setVisibility(View.VISIBLE);
				  					  deleteNote.setVisibility(View.VISIBLE);
				  				  }
			               }
			            });
			    	 return "";
			    }
			
			    /**
			     * After completing background task Dismiss the progress dialog
			     * **/
			    protected void onPostExecute(String result) {
			        // dismiss the dialog after getting all products
			
			    	super.onPostExecute(result);

		            // Dismiss the progress dialog
		            if (pDialog.isShowing())
		                pDialog.dismiss();     
			    }
			}
			
			
			private class DeleteNoteTask extends AsyncTask<String, String, Boolean> {
				  String myID = "";
				  String projID = "";
				  
				  public DeleteNoteTask(String id, String pid) {
				     myID = id;
				     projID = pid;
				     
				  }

				  protected Boolean doInBackground(String... urls) {
					  			  
					  boolean suc = false;
					  
						suc = MyProvider.deleteNote(myID, projID);
					  
					  return suc;
				  }

				  protected void onPostExecute(Boolean result) {
					  
					  if(result == true){
						  Toast.makeText(getApplicationContext(), "Note deleted", Toast.LENGTH_SHORT).show();
						  Globals.setValue("note_update", "note_update");
						  finish();
						  
					  }else{
						  Toast.makeText(getApplicationContext(), "Was not deleted the note", Toast.LENGTH_SHORT).show();
						  Globals.setValue("note_update", "note_update");
						  finish();
					  }
					  
					  
				  }
			}
		
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}
	
	// button create image
	public void onCreateImage(View v)
	{
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();
		
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) 
		{
			// Create the File where the photo should go
			File photoFile = null;
			try
			{
				photoFile = createImageFile();
			} 
			catch (IOException ex) 
			{
			
			}
			
			if (photoFile != null) 
			{
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		
		//startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(
		Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
		imageFileName,  /* prefix */
		".jpg",         /* suffix */
		storageDir      /* directory */
		);
		
		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		mCurrentPhotoPathAbsolute = image.getAbsolutePath();
		return image;
	}
	
	// button search image
	public void onSearchImage(View v){
		Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
		
		Intent intent = new Intent();
		
		intent.setAction(Intent.ACTION_GET_CONTENT);
		
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		
		intent.setType("image/*");
		
		startActivityForResult(intent, REQUEST__BROWSE_PHOTO);
	}
	
	// Methods - set visible the preview image
	private void AttachBitmapToReport()
	{
		int photoW = myBitmap.getWidth();
		int photoH = myBitmap.getHeight();   	
		int scaleFactor = Math.min(photoW/100, photoH/100);
		int NewW = photoW/scaleFactor;
		int NewH = photoH/scaleFactor;
		
		imageViewNote.setVisibility(View.VISIBLE);		
		imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
	}
	
	
	@SuppressLint("MissingSuperCall")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		InputStream stream = null;
		
		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
		{
			myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
			AttachBitmapToReport();
		}
		
		if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK) 
		{
		
			try 
			{
				stream = getContentResolver().openInputStream(data.getData());
				myBitmap = BitmapFactory.decodeStream(stream);
				AttachBitmapToReport();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			if (stream != null) 
			{
			try
			{
				stream.close();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			}            
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public String resizeBase64Image(String base64image){
		int IMG_WIDTH = 800;
		int IMG_HEIGHT = 600;
	    byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT); 
	    BitmapFactory.Options options=new BitmapFactory.Options();
	    options.inPurgeable = true;
	    Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


	    if(image.getHeight() <= 400 && image.getWidth() <= 400){
	        return base64image;
	    }
	    image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

	    ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	    image.compress(Bitmap.CompressFormat.PNG,100, baos);

	    byte [] b=baos.toByteArray();
	    System.gc();
	    return Base64.encodeToString(b, Base64.NO_WRAP);

	} 
	
	private void zoomImageFromThumb(final View thumbView, String imageResId) {
	    // If there's an animation in progress, cancel it
	    // immediately and proceed with this one.
	    if (mCurrentAnimator != null) {
	        mCurrentAnimator.cancel();
	    }

	    // Load the high-resolution "zoomed-in" image.
	    final ImageView expandedImageView = (ImageView) findViewById(
	            R.id.expanded_image);

	   //
	    byte[] decodedString;
	    if(imageResId != ""){
	    	String resizeString =  resizeBase64Image(imageResId);
	    	decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
	    }else{
	    	decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
	    }
	  
	  
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		  if (decodedByte != null)
		  {
			  //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
			  //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
			  expandedImageView.setImageBitmap(decodedByte);
			  				  
		  }
		
	    

	    // Calculate the starting and ending bounds for the zoomed-in image.
	    // This step involves lots of math. Yay, math.
	    final Rect startBounds = new Rect();
	    final Rect finalBounds = new Rect();
	    final Point globalOffset = new Point();

	    // The start bounds are the global visible rectangle of the thumbnail,
	    // and the final bounds are the global visible rectangle of the container
	    // view. Also set the container view's offset as the origin for the
	    // bounds, since that's the origin for the positioning animation
	    // properties (X, Y).
	    thumbView.getGlobalVisibleRect(startBounds);
	    findViewById(R.id.relativeLayout)// the container
	            .getGlobalVisibleRect(finalBounds, globalOffset);
	    startBounds.offset(-globalOffset.x, -globalOffset.y);
	    finalBounds.offset(-globalOffset.x, -globalOffset.y);

	    // Adjust the start bounds to be the same aspect ratio as the final
	    // bounds using the "center crop" technique. This prevents undesirable
	    // stretching during the animation. Also calculate the start scaling
	    // factor (the end scaling factor is always 1.0).
	    float startScale;
	    if ((float) finalBounds.width() / finalBounds.height()
	            > (float) startBounds.width() / startBounds.height()) {
	        // Extend start bounds horizontally
	        startScale = (float) startBounds.height() / finalBounds.height();
	        float startWidth = startScale * finalBounds.width();
	        float deltaWidth = (startWidth - startBounds.width()) / 2;
	        startBounds.left -= deltaWidth;
	        startBounds.right += deltaWidth;
	    } else {
	        // Extend start bounds vertically
	        startScale = (float) startBounds.width() / finalBounds.width();
	        float startHeight = startScale * finalBounds.height();
	        float deltaHeight = (startHeight - startBounds.height()) / 2;
	        startBounds.top -= deltaHeight;
	        startBounds.bottom += deltaHeight;
	    }

	    // Hide the thumbnail and show the zoomed-in view. When the animation
	    // begins, it will position the zoomed-in view in the place of the
	    // thumbnail.
	    thumbView.setAlpha(0f);
	    expandedImageView.setVisibility(View.VISIBLE);

	    // Set the pivot point for SCALE_X and SCALE_Y transformations
	    // to the top-left corner of the zoomed-in view (the default
	    // is the center of the view).
	    expandedImageView.setPivotX(0f);
	    expandedImageView.setPivotY(0f);

	    // Construct and run the parallel animation of the four translation and
	    // scale properties (X, Y, SCALE_X, and SCALE_Y).
	    AnimatorSet set = new AnimatorSet();
	    set
	            .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
	                    startBounds.left, finalBounds.left))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
	                    startBounds.top, finalBounds.top))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
	            startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
	                    View.SCALE_Y, startScale, 1f));
	    set.setDuration(mShortAnimationDuration);
	    set.setInterpolator(new DecelerateInterpolator());
	    set.addListener(new AnimatorListenerAdapter() {
	        @Override
	        public void onAnimationEnd(Animator animation) {
	            mCurrentAnimator = null;
	        }

	        @Override
	        public void onAnimationCancel(Animator animation) {
	            mCurrentAnimator = null;
	        }
	    });
	    set.start();
	    mCurrentAnimator = set;

	    // Upon clicking the zoomed-in image, it should zoom back down
	    // to the original bounds and show the thumbnail instead of
	    // the expanded image.
	    final float startScaleFinal = startScale;
	    expandedImageView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	            if (mCurrentAnimator != null) {
	                mCurrentAnimator.cancel();
	            }

	            // Animate the four positioning/sizing properties in parallel,
	            // back to their original values.
	            AnimatorSet set = new AnimatorSet();
	            set.play(ObjectAnimator
	                        .ofFloat(expandedImageView, View.X, startBounds.left))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.Y,startBounds.top))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.SCALE_X, startScaleFinal))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.SCALE_Y, startScaleFinal));
	            set.setDuration(mShortAnimationDuration);
	            set.setInterpolator(new DecelerateInterpolator());
	            set.addListener(new AnimatorListenerAdapter() {
	                @Override
	                public void onAnimationEnd(Animator animation) {
	                    thumbView.setAlpha(1f);
	                    expandedImageView.setVisibility(View.GONE);
	                    mCurrentAnimator = null;
	                }

	                @Override
	                public void onAnimationCancel(Animator animation) {
	                    thumbView.setAlpha(1f);
	                    expandedImageView.setVisibility(View.GONE);
	                    mCurrentAnimator = null;
	                }
	            });
	            set.start();
	            mCurrentAnimator = set;
	        }
	    });
	}

}
