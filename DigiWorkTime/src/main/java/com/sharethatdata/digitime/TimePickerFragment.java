package com.sharethatdata.digitime;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

public class TimePickerFragment extends DialogFragment {
	OnTimeSetListener ontimeset;
	private TimePicker timePicker;
	private final static int TIME_PICKER_INTERVAL = 10;
	
	public TimePickerFragment() {
	 }

	 public void setCallBack(OnTimeSetListener ontime) {
		 ontimeset = ontime;
	 }

	 private int minutes, hours;

	 @Override
	 public void setArguments(Bundle args) {
	  super.setArguments(args);
	  hours = args.getInt("hours");
	  minutes = args.getInt("minutes"); 
	 }

	 @Override
	 public Dialog onCreateDialog(Bundle savedInstanceState) {
		 
	 // Create a new instance of TimePickerDialog and return it
    final TimePickerDialog tpd = new TimePickerDialog(getActivity(), ontimeset, hours, minutes,
            DateFormat.is24HourFormat(getActivity()));
		    
	  return tpd;
	 }
	 
	 
}
