package com.sharethatdata.digitime;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import com.sharethatdata.digitime.adapter.ProgressAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cProjectMy;
import com.sharethatdata.webservice.datamodel.cProjectProgress;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ProgressActivity extends AppCompatActivity {

	// Progress Dialog
    private ProgressDialog pDialog;

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider2 = null;
	WSDataProvider MyProvider3 = null;
	
	TimeUtils MyTimeUtils = null;
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;

	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();
    List<cProjectProgress> projectProgress = new ArrayList<cProjectProgress>();
    List<cProjectProgress> taskProgress = new ArrayList<cProjectProgress>();
    
    Spinner userSpinner = null;
    ListView lv = null;
	ListView task_lv = null;
	
	Button prevButton = null;
	Button nextButton = null;
	Button saveButton = null;
	
	EditText edittext = null;
		   
	ProgressAdapter adapter = null;
	ProgressAdapter adapter2 = null;
	
    private boolean no_network = false;
    private String error_message = "";
    
    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";
    
    private int maxwork_hours = 8;
    
    private boolean userIsInteracting = false;
    private int userSpinnerIndex = -1;
    
    // vars for remember report
    private String report_user = "";
    private String report_user_id = "";
    private String myTodayDateTime = "";
    
    TextView startDateView;
	   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progress_list);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
				
		lv = (ListView) findViewById(R.id.listView);
		task_lv = (ListView) findViewById(R.id.task_listView);
		
        userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		
        prevButton = (Button) findViewById(R.id.btnPrev);
        nextButton = (Button) findViewById(R.id.btnNext);
        saveButton = (Button) findViewById(R.id.buttonAddProgressReport);
            
        edittext = (EditText) findViewById(R.id.textProgress);
        
        Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // id of user logged
	
		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(ProgressActivity.this, ProgressActivity.this);
		
		// check manager
		String isManager = Globals.getValue("manager");
		if (isManager == "yes")
		{	
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(View.VISIBLE);
			RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
			userSpinnerLayout.setVisibility(View.VISIBLE);
			
			LoadUserList InitUserList;
			
			InitUserList = new LoadUserList();
			InitUserList.execute();
			
		} else {
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.GONE);
			RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
			userSpinnerLayout.setVisibility(View.GONE);
		}
		
		// set calendar
		Calendar c = Calendar.getInstance();
		
		// find friday of this week
 		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);  
		int addDays = 6 - dayOfWeek;
 		// make today = friday
		c.add(Calendar.DATE, addDays); 
		
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    
	    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	    	    
	    myTodayDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
       
        startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
        TextView startDayView = (TextView) findViewById(R.id.dateDayText);
        startDayView.setText(weekdayname);
                
		TextView titleText = (TextView) findViewById(R.id.title);
		String list_type = "Project Progress";
		titleText.setText(list_type);
		
		TextView task_titleText = (TextView) findViewById(R.id.task_title);
		String task_list_type = "Task Progress";
		task_titleText.setText(task_list_type);
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    String strText = ((TextView) itemClicked.findViewById(R.id.textViewName)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("pid", pid);
			    Globals.setValue("name", strText);
			    Globals.setValue("action", "project");
				
			    // start detail activity
			//	Intent intent = new Intent(getApplicationContext(), DetailActivity.class); startActivity(intent); 
				
			}

		});	
	
		userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user = userkeyArray.get(myIndex);
		  	   		report_user_id = userkeyArray.get(myIndex);
			  	   		  	   
		  	   		userSpinnerIndex = myIndex;
		  	   		
		  		    LoadProgressReports();
		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                
            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        		startDateView = (TextView) findViewById(R.id.dateStartText);
        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
        		String weekdayname = "";
        		
                try {
                	Date myDate = dateFormat.parse(startDateView.getText().toString());
                    
                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, -7); 
            	    mStartYear = c.get(Calendar.YEAR);
            		mStartMonth = c.get(Calendar.MONTH);
            		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
            		    	        	       
            		
                } catch (ParseException e) {
                    	
                }
              
                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);
                
        	    LoadProgressReports();
        	    
            }

        });
		
		nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            
            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        		startDateView = (TextView) findViewById(R.id.dateStartText);
        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                String weekdayname = "";
        				
                try {
                	Date myDate = dateFormat.parse(startDateView.getText().toString());
                    
                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, 7); 
            	    mStartYear = c.get(Calendar.YEAR);
            		mStartMonth = c.get(Calendar.MONTH);
            		mStartDay = c.get(Calendar.DAY_OF_MONTH);
            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
            	
            		
                } catch (ParseException e) {
                    	
                }
              
                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);
                
        	    LoadProgressReports();
            }

        });
		
		saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	
	            try {
	            	
	            	// get result data from adapter
	            	projectProgress = adapter.projectProgress;
	            	taskProgress = adapter2.projectProgress;
	            	
	            	int x = projectProgress.size();
	            	int y = taskProgress.size();
	            	
	            	// create progress report
	        	    RegisterProgressTask register;
				    register = new RegisterProgressTask();
				    register.execute();
	        		
	            } catch (Exception e) {
	                	
	            }
              
               
            }

        });
		
	}
	
	
	@Override
	public void onUserInteraction() {
	    super.onUserInteraction();
	    userIsInteracting = true;
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    LoadProgressReports();
	    
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	public long Daybetween(String date1,String date2,String pattern)
	{
	    SimpleDateFormat sdf = new SimpleDateFormat(pattern,Locale.ENGLISH);
	    Date Date1 = null,Date2 = null;
	    try{
	        Date1 = sdf.parse(date1);
	        Date2 = sdf.parse(date2);
	    }catch(Exception e)
	    {
	        e.printStackTrace();
	    }
	    return (Date2.getTime() - Date1.getTime())/(24*60*60*1000);
	}
	
	private void LoadProgressReports()
	{
		

        String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);

		LoadList RefreshList;
		RefreshList = new LoadList();
		RefreshList.execute();
		
		
	}
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	        	TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	                
	            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	                .append(pad(mStartMonth + 1)).append("-")
	                .append(pad(mStartYear)));
	                
		  
	            LoadProgressReports();
	    	
	            
		  }
	};

	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    dp1.show();
	    
	   
	}
	
	// set time from mysql datetime
		private String SetTime(String mysqldate)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			StringBuilder setText = null;
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    mStartMinute = c.get(Calendar.MINUTE);
			    
			    setText = new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute));
					
			} catch (ParseException e) {
			    	
			}
			
			return setText.toString();

	    }
		
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
         pDialog = new ProgressDialog(ProgressActivity.this);
         pDialog.setMessage( getString(R.string.loading_alert_dialog));
      
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.setCanceledOnTouchOutside(false);
         pDialog.show();
         
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider2 = new WSDataProvider(myUser, myPass);
		
		Globals = ((MyGlobals)getApplication());
		
	    MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider2.getMyUsers();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.name);
                         
                // adding HashList to ArrayList
                userList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
           
    	    List<String> userArray =  new ArrayList<String>();
           	for (HashMap<String, String> map : userList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
          	        }
           	
           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(ProgressActivity.this, android.R.layout.simple_spinner_item, userArray);
      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
      	    	sUsers.setAdapter(user_adapter);
    	        
    	        int spinnerPosition = user_adapter.getPosition(myUserName);
	    		//set the default according to value
	    		sUsers.setSelection(spinnerPosition);
      	
      	    	userIsInteracting = false;
      	    	
      	    	pDialog.dismiss();
        	   
           }
       	});
       
       pDialog.dismiss();
 	      
    	}
	}

	/**
	 * Background Async Task to Load all progress reports by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

		boolean isDayToday = false;
		
     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	Globals = ((MyGlobals)getApplication());	
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");
			
			if (report_user_id == "") 
			{
				report_user_id = myUserID;
			}
			
			long days = Daybetween(report_date_start, myTodayDateTime, "yyyy-MM-dd");
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	       	     
	    	 if (report_date_start.equals(myTodayDateTime) || days < 13) isDayToday = true;
	    	 
	    	 if (isDayToday)
	    	 {
	    		 List<cProjectProgress> projectProgress = MyProvider.getProjectProgress(report_user_id, report_date_start);
	    		 int numProgressReports = projectProgress.size();
	    		 if (numProgressReports > 0)
	    		 {
	    			 for(cProjectProgress entry : projectProgress)
			    	 { 
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		                
		                // filter out non-project
		                if (entry.id != "0")
		                {                
			                map.put("pid",  entry.id);
			                map.put("name", entry.name);
			                map.put("progress", entry.progress);
			                map.put("hours", entry.hours);
			          	                 
			                // adding HashList to ArrayList
			                itemList.add(map);
		                }
			    	 }
	    		 } else {
			    	 List<cProjectMy> latestProjects = MyProvider.getLatestProjects(report_user_id, report_date_start);
			    	 for(cProjectMy entry : latestProjects)
			    	 { 
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		                
		                // filter out non-project
		                if (entry.id != "0")
		                {                
			                map.put("pid",  entry.id);
			                map.put("name", entry.name);
			                map.put("progress", "");
			                map.put("hours", "");
			          	                 
			                // adding HashList to ArrayList
			                itemList.add(map);
		                }
			    	 }
	    		 }
	    	 } else {
	    		 List<cProjectProgress> projectProgress = MyProvider.getProjectProgress(report_user_id, report_date_start);
		    	 for(cProjectProgress entry : projectProgress)
		    	 { 
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                
	                // filter out non-project
	                if (entry.id != "0")
	                {                
		                map.put("pid",  entry.id);
		                map.put("name", entry.name);
		                map.put("progress", entry.progress);
		                map.put("hours", entry.hours);
		          	                 
		                // adding HashList to ArrayList
		                itemList.add(map);
	                }
		    	 }
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	if (!isDayToday)
	            	{
		            	ListAdapter adapter = null;
		            
		            	adapter = new SimpleAdapter(
			               		ProgressActivity.this
			                		, itemList
			                		, R.layout.listview_item_project, new String[] { "pid", "name", "progress", "hours" },
		                           new int[] { R.id.textViewID, R.id.textViewName, R.id.textProgress, R.id.textHours});
			        	            
		                
		                // updating listview
		            	lv.setAdapter(adapter);
		            	
		            	saveButton.setVisibility(View.GONE);
	            	} else {
	            	
		            	 ListView listView = (ListView) findViewById(R.id.listView);
		                 adapter = new ProgressAdapter(ProgressActivity.this, itemList);
		                 listView.setAdapter(adapter);
		                 
		             	saveButton.setVisibility(View.VISIBLE);
	            	}
	            		            	
	                setListViewHeightBasedOnChildren(lv);
	             
	            }
	        });
	  	      
	        
	        // finished
	        // load task progress
	        LoadTaskList TaskList;
			TaskList = new LoadTaskList();
			TaskList.execute();
	    }
	  

	}
	
	/**
	 * Background Async Task to Load all progress reports for tasks by making HTTP Request
  	 * */
	class LoadTaskList extends AsyncTask<String, String, String> {

		boolean isDayToday = false;
		
     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
         showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	Globals = ((MyGlobals)getApplication());	
	    	MyProvider3 = new WSDataProvider(myUser, myPass);
		    MyProvider3.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");
			
			if (report_user_id == "") 
			{
				report_user_id = myUserID;
			}
		
			long days = Daybetween(report_date_start, myTodayDateTime, "yyyy-MM-dd");
		
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	       	     
	    	 if (report_date_start.equals(myTodayDateTime) || days < 13) isDayToday = true;
	    	 
	    	 if (isDayToday)
	    	 {
	    		 List<cProjectProgress> projectProgress = MyProvider3.getTaskProgress(report_user_id, report_date_start);
	    		 int numProgressReports = projectProgress.size();
	    		 if (numProgressReports > 0)
	    		 {
	    			 for(cProjectProgress entry : projectProgress)
			    	 { 
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		                
		                // filter out non-project
		                if (entry.id != "0")
		                {                
			                map.put("pid",  entry.id);
			                map.put("name", entry.name);
			                map.put("progress", entry.progress);
			                map.put("hours", entry.hours);
			          	                 
			                // adding HashList to ArrayList
			                itemList.add(map);
		                }
			    	 }
	    		 } else {
			    	 List<cProjectMy> latestProjects = MyProvider3.getLatestTasks(report_user_id, report_date_start);
			    	 for(cProjectMy entry : latestProjects)
			    	 { 
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		                
		                // filter out non-project
		                if (entry.id != "0")
		                {                
			                map.put("pid",  entry.id);
			                map.put("name", entry.name);
			                map.put("progress", "");
			                map.put("hours", "");
			          	                 
			                // adding HashList to ArrayList
			                itemList.add(map);
		                }
			    	 }
	    		 }
	    	 } else {
	    		 List<cProjectProgress> projectProgress = MyProvider3.getTaskProgress(report_user_id, report_date_start);
		    	 for(cProjectProgress entry : projectProgress)
		    	 { 
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                
	                // filter out non-project
	                if (entry.id != "0")
	                {                
		                map.put("pid",  entry.id);
		                map.put("name", entry.name);
		                map.put("progress", entry.progress);
		                map.put("hours", entry.hours);
		          	                 
		                // adding HashList to ArrayList
		                itemList.add(map);
	                }
		    	 }
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	if (!isDayToday)
	            	{
		            	ListAdapter adapter = null;
		            
		            	adapter = new SimpleAdapter(
			               		ProgressActivity.this
			                		, itemList
			                		, R.layout.listview_item_project, new String[] { "pid", "name", "progress", "hours" },
		                           new int[] { R.id.textViewID, R.id.textViewName, R.id.textProgress, R.id.textHours});
			        	
		                // updating listview
		            	task_lv.setAdapter(adapter);
		            	
		            	setListViewHeightBasedOnChildren(task_lv);
		            	
		            	saveButton.setVisibility(View.GONE);
	            	} else {
	            	
		            	 ListView listView = (ListView) findViewById(R.id.task_listView);
		                 adapter2 = new ProgressAdapter(ProgressActivity.this, itemList);
		                 listView.setAdapter(adapter2);
		                 
		             	saveButton.setVisibility(View.VISIBLE);
	            	}
	            		            	
	                
	             
	            }
	        });
	        
	        hideProgressDialog();
	  	      
	    }
	  

	}
	
	// for listview in scrollview (without this method, the listview won't scroll)
	   public static void setListViewHeightBasedOnChildren(ListView listView) 
	   {
	       ListAdapter listAdapter = listView.getAdapter();
	       if (listAdapter == null)
	           return;

	       int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	       int totalHeight=0;
	       View view = null;

	       for (int i = 0; i < listAdapter.getCount(); i++) 
	       {
	           view = listAdapter.getView(i, view, listView);

	           if (i == 0)
	               view.setLayoutParams(new LayoutParams(desiredWidth,
	                                         LayoutParams.MATCH_PARENT));

	           view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	           totalHeight += view.getMeasuredHeight();

	       }

	       LayoutParams params = listView.getLayoutParams();
	       params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

	       listView.setLayoutParams(params);
	       listView.requestLayout();

	   }
	   

		  /** AsyncTask register project progress  */
	    private class RegisterProgressTask extends AsyncTask<String, String, Boolean>{
	    	
	    	 @Override
		     protected void onPreExecute() {
	    		 showProgressDialog(getString(R.string.loading_alert_dialog));
	    	 }
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	 	        	 
				  // call web method
				 MyProvider = new WSDataProvider(myUser, myPass);
				 MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				  
				 String report_date_start = Globals.getValue("report_date_start");
				
				 boolean suc = false;
				
				 for(cProjectProgress entry : projectProgress)
				 {
              			// create in ws
					  suc = MyProvider.createProjectProgress(report_date_start, entry.id, entry.progress, entry.hours, entry.comment);
               	 }
				 

				 for(cProjectProgress entry : taskProgress)
				 {
              			// create in ws
					  suc = MyProvider.createTaskProgress(report_date_start, entry.id, entry.progress, entry.hours, entry.comment);
               	 }
				 
				  
	        	  return suc;
	        }
	        
	        protected void onPostExecute(Boolean result) {
	        	if(result == true){
	        		Toast.makeText(ProgressActivity.this, "Saved the Progress Report", Toast.LENGTH_LONG).show();
	        		finish();
	        	}else{
	        		Toast.makeText(ProgressActivity.this, "Not Saved the Progress Report", Toast.LENGTH_LONG).show();
	        	}
	        	hideProgressDialog();
	        }
	    }
	    
	    /**
		 * Shows a Progress Dialog 
		 *  
		 * @param msg
		 */
		public void showProgressDialog(String msg) 
		{
			
			// check for existing progressDialog
			if (pDialog == null) {
				// create a progress Dialog
				pDialog = new ProgressDialog(this);

				// remove the ability to hide it by tapping back button
				pDialog.setIndeterminate(true);
				
				pDialog.setCancelable(false);
				pDialog.setCanceledOnTouchOutside(false);
				
				pDialog.setMessage(msg);

			}

			// now display it.
			pDialog.show();		
		}	
		
		
		/**
		 * Hides the Progress Dialog
		 */
		public void hideProgressDialog() {
			
			if (pDialog != null) {
				pDialog.dismiss();
			}
			
			pDialog = null;
		}
	  
}
