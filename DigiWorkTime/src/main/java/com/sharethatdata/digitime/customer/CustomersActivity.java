package com.sharethatdata.digitime.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cCustomer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by miha on 27.09.2018.
 */

public class CustomersActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";

    ArrayList<HashMap<String, String>> customerList;

    ListView lv = null;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_list_customers);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Globals = ((MyGlobals)getApplication());
        Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CustomersActivity.this, CustomersActivity.this);

        lv = (ListView) findViewById(R.id.listView);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                HashMap<String, String> itemsList = (HashMap<String, String>) customerList.get(position);
                String itemId = (String) itemsList.get("id");

                Intent intent = new Intent(CustomersActivity.this, CustomersDetailActivity.class);

                intent.putExtra("ID", itemId);

                startActivity(intent);

            }

        });


        new LoadList().execute();

    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("customer_update");
        if(update != ""){
            Globals.setValue("customer_update", "");
            new LoadList().execute();
        }
        Globals.setValue("edit_customer","");
    }

    static List<cCustomer> listCustomerFiltered;
    static private ArrayList<cCustomer> arraylistCustomers;

    public static List<cCustomer> filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listCustomerFiltered = new ArrayList<cCustomer>();
        listCustomerFiltered.clear();
        if (charText.length() == 0) {
            listCustomerFiltered.addAll(arraylistCustomers);
        } else {
            for (cCustomer customer : arraylistCustomers) {
                if (customer.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listCustomerFiltered.add(customer);
                }
            }
        }
        return listCustomerFiltered;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text

        String text = newText.toString().toLowerCase(Locale.getDefault());
        List<cCustomer> filter_customers = filter(text.toString());
        customerList.clear(); // <--- clear the list before add

        for(cCustomer entry : filter_customers)
        {

            HashMap<String, String> map = new HashMap<String, String>();

            map.put("id",  Long.toString(entry.id));
            map.put("name", entry.name);

            customerList.add(map);
        }

        ListAdapter adapter = null;
        adapter = new SimpleAdapter(
                CustomersActivity.this
                , customerList
                , R.layout.listview_item_customers, new String[] { "id", "name" },
                new int[] { R.id.textViewID, R.id.textViewName });
        lv.setAdapter(null);
        lv.setAdapter(adapter);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.customer_menu, menu);

        // check manager
        String isManager = Globals.getValue("manager");
        if (isManager == "yes")
        {
            getMenuInflater().inflate(R.menu.customer_menu, menu);
        }else{
            getMenuInflater().inflate(R.menu.search_customer, menu);
        }

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(false);
        searchView.setQueryHint(getString(R.string.search_title_customer));

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_add_customer:
                Intent intent = new Intent(this, CreateCustomerActivity.class); startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Background Async Task to Load all customers by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            customerList = new ArrayList<HashMap<String, String>>();
            arraylistCustomers = new ArrayList<cCustomer>();

            List<cCustomer> customers = MyProvider.getCustomers();
            for(cCustomer entry : customers)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("name", entry.name);

                // adding HashList to ArrayList
                customerList.add(map);

            }

            arraylistCustomers.addAll(customers);

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    ListAdapter adapter = null;

                    adapter = new SimpleAdapter(
                            CustomersActivity.this
                            , customerList
                            , R.layout.listview_item_customers, new String[] { "id", "name" },
                            new int[] { R.id.textViewID, R.id.textViewName });
                    lv.setAdapter(adapter);

        			/*pDialog.dismiss();*/

                }
            });

            hideProgressDialog();

        }
    }



    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

}
