package com.sharethatdata.digitime.customer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by miha on 02.10.2018.
 */

public class CreateCustomerActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";

    private String error_message = "";

    TextInputLayout nameInputLayout;
    TextInputLayout streetInputLayout;
    TextInputLayout zipcodeInputLayout;
    TextInputLayout cityInputLayout;
    TextInputLayout countryInputLayout;
    TextInputLayout phoneInputLayout;
    TextInputLayout faxInputLayout;
    TextInputLayout emailInputLayout;
    TextInputLayout contactInputLayout;
    TextInputLayout servicenrInputLayout;
    TextInputLayout servicecontractInputLayout;

    TextInputEditText editTextName;
    TextInputEditText editTextStreet;
    TextInputEditText editTextZipcode;
    TextInputEditText editTextCity;
    TextInputEditText editTextCountry;
    TextInputEditText editTextPhone;
    TextInputEditText editTextFax;
    TextInputEditText editTextEmail;
    TextInputEditText editTextContact;
    TextInputEditText editTextServiceNr;
    TextInputEditText editTextServiceContract;


    TextView textHintName;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_create_customers);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Globals = ((MyGlobals) getApplication());
        Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateCustomerActivity.this, CreateCustomerActivity.this);

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);

        editTextName = (TextInputEditText) findViewById(R.id.editTextName);
        editTextStreet = (TextInputEditText) findViewById(R.id.editTextStreet);
        editTextZipcode = (TextInputEditText) findViewById(R.id.editTextZipcode);
        editTextCity = (TextInputEditText) findViewById(R.id.editTextCity);
        editTextCountry = (TextInputEditText) findViewById(R.id.editTextCountry);
        editTextPhone = (TextInputEditText) findViewById(R.id.editTextPhone);
        editTextFax = (TextInputEditText) findViewById(R.id.editTextFax);
        editTextEmail = (TextInputEditText) findViewById(R.id.editTextEmail);
        editTextContact = (TextInputEditText) findViewById(R.id.editTextContact);
        editTextServiceNr = (TextInputEditText) findViewById(R.id.editTextServiceNr);
        editTextServiceContract = (TextInputEditText) findViewById(R.id.editTextServiceContract);
        //textHintName = (TextView) findViewById(R.id.text_hint_name);

        nameInputLayout = (TextInputLayout) findViewById(R.id.name_text_input_layout);
        streetInputLayout = (TextInputLayout) findViewById(R.id.street_text_input_layout);
        zipcodeInputLayout = (TextInputLayout) findViewById(R.id.zipcode_text_input_layout);
        cityInputLayout = (TextInputLayout) findViewById(R.id.city_text_input_layout);
        countryInputLayout = (TextInputLayout) findViewById(R.id.country_text_input_layout);
        phoneInputLayout = (TextInputLayout) findViewById(R.id.phone_text_input_layout);
        faxInputLayout = (TextInputLayout) findViewById(R.id.fax_text_input_layout);
        emailInputLayout = (TextInputLayout) findViewById(R.id.email_text_input_layout);
        contactInputLayout = (TextInputLayout) findViewById(R.id.contact_text_input_layout);
        servicenrInputLayout = (TextInputLayout) findViewById(R.id.servicenr_text_input_layout);
        servicecontractInputLayout = (TextInputLayout) findViewById(R.id.servicecontract_text_input_layout);

        //////////////////////////////////////////////////////////////
		/*
		 * EDIT
		 */

        String edit = Globals.getValue("edit_customer");
        if(edit != ""){
            btnSubmit.setVisibility(View.VISIBLE);
            btnSubmit.setText("Edit customer");
            setTitle("Edit customer");

            LoadDetailCustomer();
        }else{
            btnSubmit.setVisibility(View.VISIBLE);
            btnSubmit.setText("Add new customer");
            setTitle("Add customer");
        }


        checkEditText();

        // Name
        /*editTextName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // Show white background behind floating label
                            textHintName.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    // editTextName to show/hide white background behind floating label during focus change
                    if (editTextName.getText().length() > 0)
                        textHintName.setVisibility(View.VISIBLE);
                    else
                        textHintName.setVisibility(View.INVISIBLE);
                }
            }
        });
*/

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean cancelName = false;
                boolean cancelStreet = false;
                boolean cancelZipcode = false;
                boolean cancelCity = false;
                boolean cancelCountry = false;
                boolean cancelPhone = false;
                boolean cancelFax = false;
                boolean cancelEmail = false;
                boolean cancelContact = false;
                boolean cancelServiceNr = false;
                boolean cancelServiceContract = false;
                View focusView = null;
                //View focusViewEmail = null;
                String edit = Globals.getValue("edit_customer");
                if(edit != ""){
                    // edit customer

                    if (!validateEmail(editTextEmail.getText().toString())) {
                        emailInputLayout.setError(getString(R.string.text_email_not_valid));
                        focusView = editTextEmail;
                        cancelEmail = true;
                    }

                    if (!validatePhone(editTextPhone.getText().toString())) {
                        phoneInputLayout.setError(getString(R.string.text_phone_not_valid));
                        focusView = editTextPhone;
                        cancelPhone = true;
                    }

                    boolean text_lenght_email = shouldShowError("email", editTextEmail);
                    if (text_lenght_email) {
                        //editTextName.setError(getString(R.string.error_field_required));
                        //textInputLayout.setError(getString(R.string.text_name_required));
                        showError(getString(R.string.text_email_required), emailInputLayout);
                        focusView = editTextEmail;
                        cancelEmail = true;
                    }

                    boolean text_lenght_name = shouldShowError("name", editTextName);
                    if (text_lenght_name) {
                        //editTextName.setError(getString(R.string.error_field_required));
                        //textInputLayout.setError(getString(R.string.text_name_required));
                        showError(getString(R.string.text_name_required), nameInputLayout);
                        focusView = editTextName;
                        cancelName = true;
                    }

                    boolean text_lenght_street = shouldShowError("street", editTextStreet);
                    if (text_lenght_street) {
                        showError(getString(R.string.text_street_required), streetInputLayout);
                        focusView = editTextStreet;
                        cancelStreet = true;
                    }

                    boolean text_lenght_zipcode = shouldShowError("zipcode", editTextZipcode);
                    if (text_lenght_zipcode) {
                        showError(getString(R.string.text_zipcode_required), zipcodeInputLayout);
                        focusView = editTextZipcode;
                        cancelZipcode = true;
                    }

                    boolean text_lenght_city = shouldShowError("city", editTextCity);
                    if (text_lenght_city) {
                        showError(getString(R.string.text_city_required), cityInputLayout);
                        focusView = editTextCity;
                        cancelCity = true;
                    }

                    boolean text_lenght_country = shouldShowError("country", editTextCountry);
                    if (text_lenght_country) {
                        showError(getString(R.string.text_country_required), countryInputLayout);
                        focusView = editTextCountry;
                        cancelCountry = true;
                    }

                    boolean text_lenght_phone = shouldShowError("phone", editTextPhone);
                    if (text_lenght_phone) {
                        showError(getString(R.string.text_phone_required), phoneInputLayout);
                        focusView = editTextPhone;
                        cancelPhone = true;
                    }

                    boolean text_lenght_fax = shouldShowError("fax", editTextFax);
                    if (text_lenght_fax) {
                        showError(getString(R.string.text_fax_required), faxInputLayout);
                        focusView = editTextFax;
                        cancelFax = true;
                    }

                    boolean text_lenght_contact = shouldShowError("contact", editTextContact);
                    if (text_lenght_contact) {
                        showError(getString(R.string.text_contact_name_required), contactInputLayout);
                        focusView = editTextContact;
                        cancelContact = true;
                    }

                    boolean text_lenght_servicenr = shouldShowError("servicenr", editTextServiceNr);
                    if (text_lenght_servicenr) {
                        showError(getString(R.string.text_service_nr_required), servicenrInputLayout);
                        focusView = editTextServiceNr;
                        cancelServiceNr = true;
                    }

                    boolean text_lenght_servicecontract = shouldShowError("servicecontract", editTextServiceContract);
                    if (text_lenght_servicecontract) {
                        showError(getString(R.string.text_service_contract_required), servicecontractInputLayout);
                        focusView = editTextServiceContract;
                        cancelServiceContract = true;
                    }

                    if(cancelName){focusView.requestFocus();}else{hideError(nameInputLayout);}
                    if(cancelStreet){focusView.requestFocus();}else{hideError(streetInputLayout);}
                    if(cancelZipcode){focusView.requestFocus();}else{hideError(zipcodeInputLayout);}
                    if(cancelCity){focusView.requestFocus();}else{hideError(cityInputLayout);}
                    if(cancelCountry){focusView.requestFocus();}else{hideError(countryInputLayout);}
                    if(cancelPhone){focusView.requestFocus();}else{hideError(phoneInputLayout);}
                    if(cancelFax){focusView.requestFocus();}else{hideError(faxInputLayout);}
                    if(cancelEmail){focusView.requestFocus();}else{hideError(emailInputLayout);}
                    if(cancelContact){focusView.requestFocus();}else{hideError(contactInputLayout);}
                    if(cancelServiceNr){focusView.requestFocus();}else{hideError(servicenrInputLayout);}
                    if(cancelServiceContract){focusView.requestFocus();}else{hideError(servicecontractInputLayout);}

                    if(cancelName || cancelStreet || cancelEmail || cancelZipcode || cancelCity || cancelCountry
                            || cancelPhone || cancelFax || cancelContact || cancelServiceNr || cancelServiceContract){
                        focusView.requestFocus();
                    }else{
                        //hideError();
                        hideKeyboard();

                        String id = Globals.getValue("customerid");
                        new EditCustomer(id).execute();
                    }

                }else{
                    // create customer

                    if (!validateEmail(editTextEmail.getText().toString())) {
                        emailInputLayout.setError(getString(R.string.text_email_not_valid));
                        focusView = editTextEmail;
                        cancelEmail = true;
                    }

                    if (!validatePhone(editTextPhone.getText().toString())) {
                        phoneInputLayout.setError(getString(R.string.text_phone_not_valid));
                        focusView = editTextPhone;
                        cancelPhone = true;
                    }

                    boolean text_lenght_email = shouldShowError("email", editTextEmail);
                    if (text_lenght_email) {
                        //editTextName.setError(getString(R.string.error_field_required));
                        //textInputLayout.setError(getString(R.string.text_name_required));
                        showError(getString(R.string.text_email_required), emailInputLayout);
                        focusView = editTextEmail;
                        cancelEmail = true;
                    }

                    boolean text_lenght_name = shouldShowError("name", editTextName);
                    if (text_lenght_name) {
                        //editTextName.setError(getString(R.string.error_field_required));
                        //textInputLayout.setError(getString(R.string.text_name_required));
                        showError(getString(R.string.text_name_required), nameInputLayout);
                        focusView = editTextName;
                        cancelName = true;
                    }

                    boolean text_lenght_street = shouldShowError("street", editTextStreet);
                    if (text_lenght_street) {
                        showError(getString(R.string.text_street_required), streetInputLayout);
                        focusView = editTextStreet;
                        cancelStreet = true;
                    }

                    boolean text_lenght_zipcode = shouldShowError("zipcode", editTextZipcode);
                    if (text_lenght_zipcode) {
                        showError(getString(R.string.text_zipcode_required), zipcodeInputLayout);
                        focusView = editTextZipcode;
                        cancelZipcode = true;
                    }

                    boolean text_lenght_city = shouldShowError("city", editTextCity);
                    if (text_lenght_city) {
                        showError(getString(R.string.text_city_required), cityInputLayout);
                        focusView = editTextCity;
                        cancelCity = true;
                    }

                    boolean text_lenght_country = shouldShowError("country", editTextCountry);
                    if (text_lenght_country) {
                        showError(getString(R.string.text_country_required), countryInputLayout);
                        focusView = editTextCountry;
                        cancelCountry = true;
                    }

                    boolean text_lenght_phone = shouldShowError("phone", editTextPhone);
                    if (text_lenght_phone) {
                        showError(getString(R.string.text_phone_required), phoneInputLayout);
                        focusView = editTextPhone;
                        cancelPhone = true;
                    }

                    boolean text_lenght_fax = shouldShowError("fax", editTextFax);
                    if (text_lenght_fax) {
                        showError(getString(R.string.text_fax_required), faxInputLayout);
                        focusView = editTextFax;
                        cancelFax = true;
                    }

                    boolean text_lenght_contact = shouldShowError("contact", editTextContact);
                    if (text_lenght_contact) {
                        showError(getString(R.string.text_contact_name_required), contactInputLayout);
                        focusView = editTextContact;
                        cancelContact = true;
                    }

                    boolean text_lenght_servicenr = shouldShowError("servicenr", editTextServiceNr);
                    if (text_lenght_servicenr) {
                        showError(getString(R.string.text_service_nr_required), servicenrInputLayout);
                        focusView = editTextServiceNr;
                        cancelServiceNr = true;
                    }

                    boolean text_lenght_servicecontract = shouldShowError("servicecontract", editTextServiceContract);
                    if (text_lenght_servicecontract) {
                        showError(getString(R.string.text_service_contract_required), servicecontractInputLayout);
                        focusView = editTextServiceContract;
                        cancelServiceContract = true;
                    }

                    if(cancelName){focusView.requestFocus();}else{hideError(nameInputLayout);}
                    if(cancelStreet){focusView.requestFocus();}else{hideError(streetInputLayout);}
                    if(cancelZipcode){focusView.requestFocus();}else{hideError(zipcodeInputLayout);}
                    if(cancelCity){focusView.requestFocus();}else{hideError(cityInputLayout);}
                    if(cancelCountry){focusView.requestFocus();}else{hideError(countryInputLayout);}
                    if(cancelPhone){focusView.requestFocus();}else{hideError(phoneInputLayout);}
                    if(cancelFax){focusView.requestFocus();}else{hideError(faxInputLayout);}
                    if(cancelEmail){focusView.requestFocus();}else{hideError(emailInputLayout);}
                    if(cancelContact){focusView.requestFocus();}else{hideError(contactInputLayout);}
                    if(cancelServiceNr){focusView.requestFocus();}else{hideError(servicenrInputLayout);}
                    if(cancelServiceContract){focusView.requestFocus();}else{hideError(servicecontractInputLayout);}

                    if(cancelName || cancelStreet || cancelEmail || cancelZipcode || cancelCity || cancelCountry
                            || cancelPhone || cancelFax || cancelContact || cancelServiceNr || cancelServiceContract){
                        focusView.requestFocus();
                    }else{
                        //hideError();
                        hideKeyboard();

                        RegisterCustomer register;
                        register = new RegisterCustomer();
                        register.execute();
                    }
                }

            }
        });


    }


    private boolean shouldShowError(String text, TextInputEditText editText) {
        boolean result = false;

        if(text.equals("name")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 4;
            result = !test_lenght;
        }

        if(text.equals("street")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 1;
            result = !test_lenght;
        }

        if(text.equals("zipcode")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 3;
            result = !test_lenght;
        }

        if(text.equals("city")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 1;
            result = !test_lenght;
        }

        if(text.equals("country")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 1;
            result = !test_lenght;
        }

        if(text.equals("phone")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 1;
            result = !test_lenght;
        }

        if(text.equals("fax")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength >= 1;
            result = !test_lenght;
        }

        if(text.equals("email")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength > 1;
            result = !test_lenght;
        }

        if(text.equals("contact")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength >= 1;
            result = !test_lenght;
        }

        if(text.equals("fax")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength >= 1;
            result = !test_lenght;
        }

        if(text.equals("servicenr")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength >= 1;
            result = !test_lenght;
        }

        if(text.equals("servicecontract")){
            int textLength = editText.getText().length();
            boolean test_lenght = textLength >= 1;
            result = !test_lenght;
        }

        return result;
    }

    private void showError(String text, TextInputLayout inputlayout) {
        inputlayout.setErrorEnabled(true);
        inputlayout.setError(text);
    }

    private void hideError(TextInputLayout inputlayout) {
        inputlayout.setErrorEnabled(false);
        inputlayout.setError("");
    }

    public boolean validatePhone(String phone) {
        //return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return (Patterns.PHONE.matcher(phone).matches());
    }

    public boolean validateEmail(String email) {
        //return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return (Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void checkEditText(){
        // Name
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                //if (s.charAt(s.length() - 1) == '\n') {
                Log.d("TEST RESPONSE", "Enter was pressed");

                boolean text_lenght = shouldShowError("name", editTextName);
                if (text_lenght) {
                    showError(getString(R.string.text_name_required), nameInputLayout);
                } else {
                    hideError(nameInputLayout);
                }
                //}
            }
        });

        // Street
        editTextStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("street", editTextStreet);
                if (text_lenght) {
                    showError(getString(R.string.text_street_required), streetInputLayout);
                } else {
                    hideError(streetInputLayout);
                }
            }
        });

        // Zipcode
        editTextZipcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("zipcode", editTextZipcode);
                if (text_lenght) {
                    showError(getString(R.string.text_zipcode_required), zipcodeInputLayout);
                } else {
                    hideError(zipcodeInputLayout);
                }
            }
        });

        // City
        editTextCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("city", editTextCity);
                if (text_lenght) {
                    showError(getString(R.string.text_city_required), cityInputLayout);
                } else {
                    hideError(cityInputLayout);
                }
            }
        });

        // Country
        editTextCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("country", editTextCountry);
                if (text_lenght) {
                    showError(getString(R.string.text_country_required), countryInputLayout);
                } else {
                    hideError(countryInputLayout);
                }
            }
        });

        // Country
        editTextPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("phone", editTextPhone);
                if (text_lenght) {
                    showError(getString(R.string.text_phone_required), phoneInputLayout);
                } else {
                    hideError(phoneInputLayout);
                }
            }
        });

        // Fax
        editTextFax.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("fax", editTextFax);
                if (text_lenght) {
                    showError(getString(R.string.text_fax_required), faxInputLayout);
                } else {
                    hideError(faxInputLayout);
                }
            }
        });

        // Email
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("email", editTextEmail);
                if (text_lenght) {
                    showError(getString(R.string.text_email_required), emailInputLayout);
                } else {
                    hideError(emailInputLayout);
                }
            }
        });

        // Contact
        editTextContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("contact", editTextContact);
                if (text_lenght) {
                    showError(getString(R.string.text_contact_name_required), contactInputLayout);
                } else {
                    hideError(contactInputLayout);
                }
            }
        });

        // Contract number
        editTextServiceNr.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("servicenr", editTextServiceNr);
                if (text_lenght) {
                    showError(getString(R.string.text_service_nr_required), servicenrInputLayout);
                } else {
                    hideError(servicenrInputLayout);
                }
            }
        });

        // Contract service
        editTextServiceContract.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                boolean text_lenght = shouldShowError("servicecontract", editTextServiceContract);
                if (text_lenght) {
                    showError(getString(R.string.text_service_contract_required), servicecontractInputLayout);
                } else {
                    hideError(servicecontractInputLayout);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void LoadDetailCustomer(){

        String id = Globals.getValue("customerid");
        String text_name =  Globals.getValue("Name");
        String text_street = Globals.getValue("Street");
        String text_zipcode = Globals.getValue("Zipcode");
        String text_city = Globals.getValue("City");
        String text_country = Globals.getValue("Country");
        String text_phone = Globals.getValue("Phone");
        String text_fax = Globals.getValue("Fax");
        String text_email = Globals.getValue("Email");
        String text_contact = Globals.getValue("Contact");


        EditText editTextName = (EditText) findViewById(R.id.editTextName);
        editTextName.setText(text_name);

        EditText editTextStreet = (EditText) findViewById(R.id.editTextStreet);
        editTextStreet.setText(text_street);

        EditText editTextZipcode = (EditText) findViewById(R.id.editTextZipcode);
        editTextZipcode.setText(text_zipcode);

        EditText editTextCity = (EditText) findViewById(R.id.editTextCity);
        editTextCity.setText(text_city);

        EditText editTextCountry = (EditText) findViewById(R.id.editTextCountry);
        editTextCountry.setText(text_country);

        EditText editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextPhone.setText(text_phone);

        EditText editTextFax = (EditText) findViewById(R.id.editTextFax);
        editTextFax.setText(text_fax);

        EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextEmail.setText(text_email);

        EditText editTextContact = (EditText) findViewById(R.id.editTextContact);
        editTextContact.setText(text_contact);
    }


    /** AsyncTask register time record  */
    private class RegisterCustomer extends AsyncTask<String, String, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CreateCustomerActivity.this);
            progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");

            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {
            // extract data

            EditText editTextName = (EditText) findViewById(R.id.editTextName);
            String customerName = editTextName.getText().toString();

            EditText editTextStreet = (EditText) findViewById(R.id.editTextStreet);
            String customerStreet = editTextStreet.getText().toString();

            EditText editTextZipcode = (EditText) findViewById(R.id.editTextZipcode);
            String customerZipcode = editTextZipcode.getText().toString();

            EditText editTextCity = (EditText) findViewById(R.id.editTextCity);
            String customerCity = editTextCity.getText().toString();

            EditText editTextCountry = (EditText) findViewById(R.id.editTextCountry);
            String customerCountry = editTextCountry.getText().toString();

            EditText editTextPhone = (EditText) findViewById(R.id.editTextPhone);
            String customerPhone = editTextPhone.getText().toString();

            EditText editTextFax = (EditText) findViewById(R.id.editTextFax);
            String customerFax = editTextFax.getText().toString();

            EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
            String customerEmail = editTextEmail.getText().toString();

            EditText editTextContact = (EditText) findViewById(R.id.editTextContact);
            String customerContact = editTextContact.getText().toString();

            EditText editTextServiceNr = (EditText) findViewById(R.id.editTextServiceNr);
            String customerServiceNr = editTextServiceNr.getText().toString();

            EditText editTextServiceContract = (EditText) findViewById(R.id.editTextServiceContract);
            String customerServiceContract = editTextServiceContract.getText().toString();


            boolean suc = false;

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            suc = MyProvider.createCustomer(customerName, customerStreet, customerZipcode, customerCity, customerCountry, customerPhone,
                    customerFax, customerEmail, customerContact, customerServiceNr, customerServiceContract);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(CreateCustomerActivity.this, "Was not created customer", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateCustomerActivity.this, "Customer was created!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(getApplicationContext(), CustomersActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                }
            });

            progressDialog.dismiss();

        }
    }

    private class EditCustomer extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CreateCustomerActivity.this);
            progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");

            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        String myID = "";

        public EditCustomer(String id) {
            myID = id;
        }

        protected Boolean doInBackground(String... urls) {
            // extract data

            EditText editTextName = (EditText) findViewById(R.id.editTextName);
            String customerName = editTextName.getText().toString();

            EditText editTextStreet = (EditText) findViewById(R.id.editTextStreet);
            String customerStreet = editTextStreet.getText().toString();

            EditText editTextZipcode = (EditText) findViewById(R.id.editTextZipcode);
            String customerZipcode = editTextZipcode.getText().toString();

            EditText editTextCity = (EditText) findViewById(R.id.editTextCity);
            String customerCity = editTextCity.getText().toString();

            EditText editTextCountry = (EditText) findViewById(R.id.editTextCountry);
            String customerCountry = editTextCountry.getText().toString();

            EditText editTextPhone = (EditText) findViewById(R.id.editTextPhone);
            String customerPhone = editTextPhone.getText().toString();

            EditText editTextFax = (EditText) findViewById(R.id.editTextFax);
            String customerFax = editTextFax.getText().toString();

            EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
            String customerEmail = editTextEmail.getText().toString();

            EditText editTextContact = (EditText) findViewById(R.id.editTextContact);
            String customerContact = editTextContact.getText().toString();

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;
                suc = MyProvider.updateCustomer(myID, customerName, customerStreet, customerZipcode, customerCity, customerCountry, customerPhone,
                        customerFax, customerEmail, customerContact);

            return suc;
        }

        protected void onPostExecute(Boolean result) {

            if(result == true){
                Toast.makeText(getApplicationContext(), "Customer updated", Toast.LENGTH_SHORT).show();
                Globals.setValue("customer_update", "customer_update");
                Globals.setValue("edit_customer","");
                Intent intent = new Intent(CreateCustomerActivity.this, CustomersActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }else{
                error_message = MyProvider.last_error;

                UIHelper myUIHelper = new UIHelper();

                //myUIHelper.ShowDialog(CreateCustomerActivity.this, getString(R.string.text_error), error_message);

                Toast.makeText(getApplicationContext(), "Was not updated customer", Toast.LENGTH_SHORT).show();
                Globals.setValue("customer_update", "customer_update");
                Globals.setValue("edit_customer","");
                Intent intent = new Intent(CreateCustomerActivity.this, CustomersActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

            progressDialog.dismiss();

        }
    }

    public void hideProgressDialog() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

}
