package com.sharethatdata.digitime.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cCustomer;

import org.apache.http.protocol.HTTP;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by miha on 27.09.2018.
 */

public class CustomersDetailActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";

    TextView textViewId, textViewName, textViewStreet, textViewZipcode, textViewCity, textViewCountry,
    textViewPhone, textViewFax, textViewEmail, textViewContact;

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_detail_customers);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Globals = ((MyGlobals) getApplication());
        Globals.backgroundProcess();
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CustomersDetailActivity.this, CustomersDetailActivity.this);

        String valueID = "";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valueID = extras.getString("ID");
        }
        final String finalValueID = valueID;

        textViewId = (TextView) findViewById(R.id.textViewId);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewStreet = (TextView) findViewById(R.id.textViewStreet);
        textViewZipcode = (TextView) findViewById(R.id.textViewZipcode);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewCountry = (TextView) findViewById(R.id.textViewCountry);
        textViewPhone = (TextView) findViewById(R.id.textViewPhone);
        textViewFax = (TextView) findViewById(R.id.textViewFax);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        textViewContact = (TextView) findViewById(R.id.textViewContact);

        Button buttonEdit = (Button) findViewById(R.id.buttonEditCustomer);
        Button buttonDelete = (Button) findViewById(R.id.buttonDeleteCustomer);

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Globals.setValue("edit_customer", "edit_customer");
                Globals.setValue("customerid", finalValueID.toString());
                Globals.setValue("Name", textViewName.getText().toString());
                Globals.setValue("Street", textViewStreet.getText().toString());
                Globals.setValue("Zipcode", textViewZipcode.getText().toString());
                Globals.setValue("City", textViewCity.getText().toString());
                Globals.setValue("Country", textViewCountry.getText().toString());
                Globals.setValue("Phone", textViewPhone.getText().toString());
                Globals.setValue("Fax", textViewFax.getText().toString());
                Globals.setValue("Email", textViewEmail.getText().toString());
                Globals.setValue("Contact", textViewContact.getText().toString());

                Intent i = new Intent(CustomersDetailActivity.this, CreateCustomerActivity.class);
                startActivity(i);
            }
        });


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(CustomersDetailActivity.this)
                        .setTitle("Delete customer")
                        .setMessage("Do you really want to delete this customer?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                new DeleteCustomer(finalValueID).execute();

                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        textViewPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneClick(textViewPhone);
            }
        });

        textViewEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailClick(textViewEmail);
            }
        });

            new LoadDetail(valueID).execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void phoneClick(TextView view) {

        String phone_number = view.getText().toString();

        // call phone number
        Uri number = Uri.parse("tel:" + phone_number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);

        startActivity(callIntent);
    }

    public void emailClick(TextView view) {

        String email_addr = view.getText().toString();

        // send email
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // The intent does not have a URI, so declare the "text/plain" MIME type
        emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email_addr}); // recipients
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text");

        startActivity(emailIntent);
    }

    /**
     * Background Async Task to Load customer details by making HTTP Request
     * */
    class LoadDetail extends AsyncTask<String, String, String> {
        String valueID;

        public LoadDetail(String valueID) {
            this.valueID = valueID;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cCustomer customer = MyProvider.getCustomerDetail(valueID);

            runOnUiThread(new Runnable() {
                public void run() {

                    textViewId.setText(String.valueOf(customer.id));
                    textViewName.setText(String.valueOf(customer.name));
                    textViewStreet.setText(String.valueOf(customer.street));
                    textViewZipcode.setText(String.valueOf(customer.zipcode));
                    textViewCity.setText(String.valueOf(customer.city));
                    textViewCountry.setText(String.valueOf(customer.country));
                    textViewPhone.setText(String.valueOf(customer.phone));
                    textViewFax.setText(String.valueOf(customer.fax));
                    textViewEmail.setText(String.valueOf(customer.email));
                    textViewContact.setText(String.valueOf(customer.contact));

                    /*Linkify.addLinks(textViewPhone, Linkify.PHONE_NUMBERS);
                    Linkify.addLinks(textViewEmail, Linkify.EMAIL_ADDRESSES);*/


                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (CustomersDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
        			/*pDialog.dismiss();*/
                }
            });
        }
    }

    private class DeleteCustomer extends AsyncTask<String, String, Boolean> {
        String myID = "";

        public DeleteCustomer(String id) {
            myID = id;
        }

        protected Boolean doInBackground(String... urls) {

            boolean suc = false;

            suc = MyProvider.deleteCustomer(myID);

            return suc;
        }

        protected void onPostExecute(Boolean result) {

            if(result == true){
                Toast.makeText(getApplicationContext(), "Customer deleted", Toast.LENGTH_SHORT).show();
                Globals.setValue("customer_update", "customer_update");
                Globals.setValue("edit_customer","");
                finish();

            }else{
                Toast.makeText(getApplicationContext(), "Was not deleted customer", Toast.LENGTH_SHORT).show();
                Globals.setValue("customer_update", "customer_update");
                Globals.setValue("edit_customer","");
                finish();
            }


        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }


    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        progressDialog = null;
    }


}
