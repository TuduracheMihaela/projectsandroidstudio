package com.sharethatdata.digitime;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

public class CustomHourPickerDialog extends DialogFragment {


	OnTimeSetListener ontimeset;
	
	public CustomHourPickerDialog() {
	 }

	 public void setCallBack(OnTimeSetListener ontime) {
		 ontimeset = ontime;
	 }

	 private int minutes, hours;

	 @Override
	 public void setArguments(Bundle args) {
	  super.setArguments(args);
	  hours = args.getInt("hours");
	  minutes = args.getInt("minutes"); 
	 }

	 @Override
	 public Dialog onCreateDialog(Bundle savedInstanceState) {
		 
	 // Create a new instance of TimePickerDialog and return it
		 final TimePickerDialog tpd = new TimePickerDialog(getActivity(), ontimeset, hours, minutes, DateFormat.is24HourFormat(getActivity()));
		 
		 
		 
		 tpd.setOnShowListener(new DialogInterface.OnShowListener() {
		        @Override
		        public void onShow(DialogInterface dialog) {
		            int tpLayoutId = getResources().getIdentifier("timePickerLayout", "id", "android");

		            ViewGroup tpLayout = (ViewGroup) tpd.findViewById(tpLayoutId);
		            ViewGroup layout = (ViewGroup) tpLayout.getChildAt(0);

		            String[] values = {"00"};
		            // Customize minute NumberPicker
		            try{	 
		            	
		            	NumberPicker hourPicker = (NumberPicker) layout.getChildAt(0);
		            		hourPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		            	
		            	 NumberPicker minutePicker = (NumberPicker) layout.getChildAt(2);
				            minutePicker.setMinValue(0);
				            minutePicker.setMaxValue(values.length - 1);
				            minutePicker.setDisplayedValues(values);
				            minutePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		            }catch (Exception e) {
		                e.printStackTrace();	
		            }
		        }
		    });
   
	  return tpd;
	 }

}
