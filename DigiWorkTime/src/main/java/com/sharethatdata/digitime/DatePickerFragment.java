package com.sharethatdata.digitime;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment {
	 OnDateSetListener ondateSet;

	 public DatePickerFragment() {
	 }

	 public void setCallBack(OnDateSetListener ondate) {
	  ondateSet = ondate;
	 }

	 private int year, month, day;

	 @Override
	 public void setArguments(Bundle args) {
	  super.setArguments(args);
	  year = args.getInt("year");
	  month = args.getInt("month");
	  day = args.getInt("day");
	 }

	 @Override
	 public Dialog onCreateDialog(Bundle savedInstanceState) {
		 
	 // Create a new instance of TimePickerDialog and return it
	    final DatePickerDialog dpd = new DatePickerDialog(getActivity(), ondateSet, year, month, day);
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dpd.setTitle(sdf.format(d));
		 
	  return dpd;
	 }
	} 
