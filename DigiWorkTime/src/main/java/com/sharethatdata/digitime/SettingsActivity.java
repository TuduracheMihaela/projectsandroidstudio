package com.sharethatdata.digitime;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cDeviceSetting;
import com.sharethatdata.logistics_webservice.datamodel.cWebService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String apikey = "";
    private String ws_url_id = "";
    private String ws_url = "";
    private String myDeviceID = "";
    private String myDeviceName = "";

    ArrayList<String> entriesId, entriesName, entriesAccessKey, entriesWebserviceId, entryValuesURL;
	
	SharedPreferences preferences;
	TextView textViewUser;
	TextView textViewPass;
	
	static final String PREFS_NAME = "defaults";

    ProgressDialog progressDialog;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
        addPreferencesFromResource(R.xml.settings);

        // save settings
        /* *** */

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myDeviceID = Globals.getValue("device");
        if(myDeviceID == "") myDeviceID = myUser;
        myDeviceName = Globals.getValue("device_name");
        MyProvider = new WSDataProvider(myUser, myPass);
        String url_ws = Globals.getValue("ws_url");
        MyProvider.SetWebServiceUrl(url_ws);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SettingsActivity.this, SettingsActivity.this);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        apikey = prefs.getString("apikey","");
        ws_url = prefs.getString("ws_url", "");
        ws_url_id = prefs.getString("ws_url_id", "");

        /* *** */
       
        // Display the fragment as the main content.
        //if (savedInstanceState == null)
        //    getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();

        CheckBoxPreference checkBoxNotification = (CheckBoxPreference) findPreference("prefNotifications");
        TimePreferenceForSettings timeNotificationStart = (TimePreferenceForSettings) findPreference("prefNotificationStartDay");
        TimePreferenceForSettings timeNotificationEnd = (TimePreferenceForSettings) findPreference("prefNotificationEndDay");

        EditTextPreference editTextWorkHours = (EditTextPreference) findPreference("prefWorkDayHours");
        TimePreferenceForSettings editTextWorkHoursStart = (TimePreferenceForSettings) findPreference("prefWorkDayStart");
        TimePreferenceForSettings editTextWorkHoursEnd = (TimePreferenceForSettings) findPreference("prefWorkDayEnd");

        EditTextPreference editTextSSIDOffice = (EditTextPreference) findPreference("prefNameOfficeSSID");
        EditTextPreference editTextSSIDHome = (EditTextPreference) findPreference("prefNameHomeSSID");
        EditTextPreference editTextSSIDInterval = (EditTextPreference) findPreference("prefIntervalSSID");

        ListPreference listPreferenceURL = (ListPreference) findPreference("prefWebServiceServer");
        CheckBoxPreference checkBoxSecurity = (CheckBoxPreference) findPreference("prefAdvancedSecurity");



        findPreference("prefNotifications").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.toString().contains("true")){
                            updateSettings("Notifications", "1");
                        }else if(newValue.toString().contains("false")){
                            updateSettings("Notifications", "0");
                        }
                        return true;
                    }
                });

        findPreference("prefNotificationStartDay").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_notification_start_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Notification_start_time", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefNotificationEndDay").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_notification_end_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Notification_end_time", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefWorkDayHours").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_work_hours_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Workday_hours", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefWorkDayStart").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_work_hour_start_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Workday_start_time", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefWorkDayEnd").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_work_hour_end_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Workday_end_time", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefNameOfficeSSID").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_office_ssid_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("SSID_office", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefNameHomeSSID").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_home_ssid_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("SSID_home", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefIntervalSSID").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_interval_ssid_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("SSID_interval", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefWebServiceServer").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                String access_key = "";
                String id_webservice = "";
                for(int i = 0; i < entryValuesURL.size(); i++){
                    if(entryValuesURL.get(i).equals(newValue.toString())){
                        access_key = entriesAccessKey.get(i);
                        id_webservice = entriesWebserviceId.get(i);
                    }
                }

                Globals.setValue("ws_url", newValue.toString());
                Globals.setValue("ws_url_id", id_webservice);
                Globals.setValue("access_key", access_key);

                // get list of web services one time and get,set the default url -> if the device was activated by the administrator
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("ws_url", newValue.toString());
                editor.putString("ws_url_id", id_webservice);
                editor.putString("access_key", access_key);
                editor.commit();

                new LoadListSettings(checkBoxNotification, timeNotificationStart, timeNotificationEnd, editTextWorkHours,
                        editTextWorkHoursStart, editTextWorkHoursEnd,
                        editTextSSIDOffice,editTextSSIDHome, editTextSSIDInterval, checkBoxSecurity, id_webservice).execute();

                return true;
            }
        });

        findPreference("prefAdvancedSecurity").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.toString().contains("true")){
                            updateSettings("Security", "1");
                        }else if(newValue.toString().contains("false")){
                            updateSettings("Security", "0");
                        }
                        return true;
                    }
                });





        new LoadListURL(listPreferenceURL).execute();

        new LoadListSettings(checkBoxNotification, timeNotificationStart, timeNotificationEnd, editTextWorkHours,
                editTextWorkHoursStart, editTextWorkHoursEnd,
                editTextSSIDOffice,editTextSSIDHome, editTextSSIDInterval, checkBoxSecurity, ws_url_id).execute();

	}



    public void updateSettings(String setting, String newValue){
        String id = "";
        String name = "";
        for(int i=0; i<entriesName.size(); i++) {
            if (entriesName.get(i).equals(setting)) {
                id = entriesId.get(i);
                name = entriesName.get(i);
            }
        }
        if(id != "" ) new UpdateDeviceSettings(apikey, myDeviceID, id, name, newValue.toString()).execute();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListURL extends AsyncTask<String, String, String> {

        ArrayList<String> entries;
        ListPreference lp;

        public LoadListURL(ListPreference lp){
            this.lp = lp;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
            showProgressDialog("Loading: get link webservice");
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            // load data from provider
            entries = new ArrayList<>();
            entryValuesURL = new ArrayList<>();
            entriesWebserviceId = new ArrayList<>();
            entriesAccessKey = new ArrayList<>();

            List<cWebService> myService = MyProvider.getAllWebServices(apikey, myDeviceID, "1");
            for(cWebService entry : myService)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("type",  entry.type);
                map.put("url",  entry.url);
                map.put("access_key",  entry.access_key);

                // adding HashList to ArrayList
                entries.add(entry.name); // name = what user see, name of the url
                entryValuesURL.add(entry.url); // values = url to connect to service
                entriesWebserviceId.add(entry.id); // values = url to connect to service
                entriesAccessKey.add(entry.access_key); // access key = for secure login
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListPreference
                     * */
                    lp.setEntries(entries.toArray(new String[entries.size()]));
                    lp.setEntryValues(entryValuesURL.toArray(new String[entryValuesURL.size()]));

                    lp.setValue(ws_url);
                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListSettings extends AsyncTask<String, String, String> {

        //ArrayList<HashMap<String, String>> settingList;
        ArrayList<String>  entryValues;
        CheckBoxPreference checkBoxNotification;
        TimePreferenceForSettings timeNotificationStart;
        TimePreferenceForSettings timeNotificationEnd;
        EditTextPreference editTextWorkHours;
        TimePreferenceForSettings editTextWorkHoursStart;
        TimePreferenceForSettings editTextWorkHoursEnd;
        EditTextPreference editTextSSIDOffice;
        EditTextPreference editTextSSIDHome;
        EditTextPreference editTextSSIDInterval;
        CheckBoxPreference checkBoxSecurity;
        String webservice_url_id;

        public LoadListSettings(CheckBoxPreference checkBoxNotification, TimePreferenceForSettings timeNotificationStart,
                                TimePreferenceForSettings timeNotificationEnd, EditTextPreference editTextWorkHours,
                                TimePreferenceForSettings editTextWorkHoursStart, TimePreferenceForSettings editTextWorkHoursEnd,
                                EditTextPreference editTextSSIDOffice, EditTextPreference editTextSSIDHome,
                                EditTextPreference editTextSSIDInterval, CheckBoxPreference checkBoxSecurity, String webservice_url_id){
            this.checkBoxNotification = checkBoxNotification;
            this.timeNotificationStart = timeNotificationStart;
            this.timeNotificationEnd = timeNotificationEnd;
            this.editTextWorkHours = editTextWorkHours;
            this.editTextWorkHoursStart = editTextWorkHoursStart;
            this.editTextWorkHoursEnd = editTextWorkHoursEnd;
            this.editTextSSIDOffice = editTextSSIDOffice;
            this.editTextSSIDHome = editTextSSIDHome;
            this.editTextSSIDInterval = editTextSSIDInterval;
            this.checkBoxSecurity = checkBoxSecurity;

            this.webservice_url_id = webservice_url_id;

        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
            showProgressDialog("Loading: get settings");
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            //settingList = new ArrayList<HashMap<String, String>>();
            entriesId = new ArrayList<>();
            entriesName = new ArrayList<>();
            entryValues = new ArrayList<>();

            String myDeviceID = Globals.getValue("device");
            String myDeviceName = Globals.getValue("device_name");

            if(myDeviceID == "") myDeviceID = Globals.getValue("user");

            // load data from provider

            List<cDeviceSetting> mySetting = MyProvider.getDeviceSettings(apikey, myDeviceID, "1", webservice_url_id);
            for(cDeviceSetting entry : mySetting)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                // 1 = Dev Intranet newtonrp
                // 2 = Prod Intranet
                //if(entry.webservice.equals("1")){
                    entriesId.add(entry.id); // name = what user see, name of the url
                    entriesName.add(entry.setting); // name = what user see, name of the url
                    entryValues.add(entry.value); // values = url to connect to service
                //}

                /*map.put("id",  entry.id);
                map.put("webservice",  entry.webservice);
                map.put("ws_type",  entry.ws_type);
                map.put("setting",  entry.setting);
                map.put("value",  entry.value);*/

                // adding HashList to ArrayList
                //settingList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListPreference
                     * */
                    /*String myRow = "";
                    for (HashMap<String, String> map : settingList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "webservice")
                            {
                                try {
                                    myRow = entry.getValue().toString();
                                    String row = String.valueOf(myRow);

                                    // 1 = Dev Intranet newtonrp
                                    // 3 = Dev HomeDelivery
                                    if(row.equals("3")){

                                    }
                                } catch(NumberFormatException nfe) {

                                }
                            }
                        }*/


                    for(int i=0; i<entriesName.size(); i++){

                        // User Profile - Notifications
                        if(entriesName.get(i).equals("Notifications")){
                            if(entryValues.get(i).equals("1")){
                                checkBoxNotification.setChecked(true);
                            }else {
                                checkBoxNotification.setChecked(false);
                            }
                        }

                        if(entriesName.get(i).equals("Notification_start_time")){
                            timeNotificationStart.onSetInitialValue(true, entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Notification_end_time")){
                            timeNotificationStart.onSetInitialValue(true, entryValues.get(i));
                        }

                        // Working day

                        if(entriesName.get(i).equals("Workday_hours")){
                            editTextWorkHours.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Workday_start_time")){
                            editTextWorkHoursStart.onSetInitialValue(true, entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Workday_end_time")){
                            editTextWorkHoursEnd.onSetInitialValue(true, entryValues.get(i));
                        }

                        // Location discovery

                        if(entriesName.get(i).equals("SSID_office")){
                            editTextSSIDOffice.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("SSID_home")){
                            editTextSSIDHome.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("SSID_interval")){
                            editTextSSIDInterval.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Security")){
                            if(entryValues.get(i).equals("1")){
                                checkBoxSecurity.setChecked(true);
                            }else {
                                checkBoxSecurity.setChecked(false);
                            }
                        }


                        /*if(entriesName.get(i).equals("Pharmacy_id")){
                            editTextPharmacyId.setText(entryValues.get(i));
                            SharedPreferences.Editor editor = getSharedPreferences("pref_PharmacyId", 0).edit();
                            editor.putString("prefPharmacyId", String.valueOf(entryValues.get(i)));
                            editor.commit();
                        }*/


                    }
                }
            });
            hideProgressDialog();
        }
    }


    /** AsyncTask update status package  */
    private class UpdateDeviceSettings extends AsyncTask<String, String, Boolean>{
        String apikey = "";
        String device = "";
        String setting = "";
        String value = "";
        String id = "";

        public UpdateDeviceSettings(String apikey, String device, String id, String setting, String value){
            this.apikey = apikey;
            this.device = device;
            this.setting = setting;
            this.value = value;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            //showProgressDialog(getString(R.string.loading_alert_dialog));
            showProgressDialog("Loading: update settings");
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            boolean suc = false;

            suc = MyProvider.updateDeviceSettings(apikey, device, id, setting, value);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Settings", "false");
                        //Toast.makeText(DeliverActivity.this, "Failed to set deliver status", Toast.LENGTH_SHORT).show();

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SettingsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SettingsActivity.this, getString(R.string.toast_text_failed_update_settings) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Log.d("Update Settings", "true");
                    }
                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }



    /*public static class PrefFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            
            getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
            addPreferencesFromResource(R.xml.settings);

            findPreference("prefWebServiceServer").setOnPreferenceChangeListener(
                    new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                    	 Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                    	 if(newValue.equals("https://newtonrp-ws.sharethatdata.com")){
                    		 // correct
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Valid Url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }else if(newValue.equals("http://dev-ws.sharethatdata.com")){
                    		 // correct
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Valid Url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }*//*else if(newValue.equals("")){
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Development url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }else{
                    		 // not correct
                    		 Log.e("E","NOT CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Invalid Url");
                             builder.setMessage("Please write correct url");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }*//*

                         //return Pattern.matches("mailPattern", (String) newValue);



                    	 return true;

                    }

                });


        }


    }


    
	public void SaveSettings()
	{
		
	}*/
	
     
}
