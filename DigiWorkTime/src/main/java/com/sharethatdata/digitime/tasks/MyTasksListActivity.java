package com.sharethatdata.digitime.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import com.sharethatdata.digitime.DetailActivity;
import com.sharethatdata.digitime.ListActivity;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.GridViewAdapter;
import com.sharethatdata.digitime.adapter.NotesAdapter;
import com.sharethatdata.digitime.adapter.TasksAdapter;
import com.sharethatdata.digitime.projects.AddProjectActivity;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity;
import com.sharethatdata.digitime.projects.MyProjectsListActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cImage;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectMy;
import com.sharethatdata.webservice.datamodel.cStatus;
import com.sharethatdata.webservice.datamodel.cTaskUser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

public class MyTasksListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	TimeUtils MyTimeUtils = null;
	
	ProgressDialog progressDialog;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	
	ArrayList<HashMap<String, String>> taskList;
	ArrayList<HashMap<String, String>> userList; // getContacts
	ArrayList<HashMap<String, String>> projectList;
	ArrayList<HashMap<String, String>> statusList;
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> projectkeyArray =  new ArrayList<String>();
    List<String> statuskeyArray =  new ArrayList<String>();
	 
	 private String report_user_id = "";
	 private String report_user = "";
	 private String report_project = "";
	 private String report_status = "1";
	
	ListView lv = null;
	SearchView searchView;

	Spinner userSpinner = null;
	Spinner projectSpinner = null;
	Spinner statusSpinner = null;
	
	private boolean userIsInteracting = false;
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_list_tasks);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		lv = (ListView) findViewById(R.id.listViewTasks);
		userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
		statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		
		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyTasksListActivity.this, MyTasksListActivity.this);
		
		// check manager
		String isManager = Globals.getValue("manager");
		if (isManager == "yes")
		{	
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.VISIBLE);
			Spinner sProject = (Spinner) findViewById(R.id.spinnerProject);
			sProject.setVisibility(Spinner.VISIBLE);
			Spinner sStatus = (Spinner) findViewById(R.id.spinnerStatus);
			sStatus.setVisibility(Spinner.VISIBLE);
			
		} else {
			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.GONE);
			Spinner sProject = (Spinner) findViewById(R.id.spinnerProject);
			sProject.setVisibility(Spinner.VISIBLE);
			Spinner sStatus = (Spinner) findViewById(R.id.spinnerStatus);
			sStatus.setVisibility(Spinner.VISIBLE);
		}
		
		
		new LoadUserList().execute();
		
		new LoadList().execute();
		
		userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user = userkeyArray.get(myIndex);
		  	   		
		  	   		// load projects list per user
		  	   		new LoadProjectListByUser().execute();

		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		projectSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
		  	   		int myIndex = projectSpinner.getSelectedItemPosition();
		  	   		report_project = projectkeyArray.get(myIndex);	  	   
		  	   		
		  	   		new LoadList().execute();
		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	if (userIsInteracting) {
			  		
		    		Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
		  	   		int myIndex = statusSpinner.getSelectedItemPosition();
		  	   		report_status = statuskeyArray.get(myIndex);	  	   
		  	   		
		  	   		new LoadList().execute();
		    	}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
				HashMap<String, String> itemsList = (HashMap<String, String>) taskList.get(position);
				 String itemId = (String) itemsList.get("id");
				 String itemName = (String) itemsList.get("name");
				 String itemDeadline = (String) itemsList.get("deadline");
				 String itemStartDate = (String) itemsList.get("startdate");
				 String itemUser = (String) itemsList.get("user");
				 String itemProject = (String) itemsList.get("project");
				 String itemActivity = (String) itemsList.get("activity");
				 String itemImportancy = (String) itemsList.get("importancy");
				 String itemStatus = (String) itemsList.get("status");
				 String itemUrgency = (String) itemsList.get("urgency");
				 String itemEstimated_time = (String) itemsList.get("estimated_time");
				 String itemReported_time = (String) itemsList.get("reported_time");
				 String itemDescription = (String) itemsList.get("description");
				 
				 String itemStatusID = (String) itemsList.get("status_id");
				 String itemImpID = (String) itemsList.get("importancy_id");
				 String itemUrgID = (String) itemsList.get("urgency_id");
				 
				 Intent intent = new Intent(MyTasksListActivity.this, MyTasksDetailActivity.class);
				 intent.putExtra("edit_task", "edit_task");
				 
				 intent.putExtra("ID", itemId);
				 intent.putExtra("Name", itemName);
				 intent.putExtra("Deadline", itemDeadline);
				 intent.putExtra("Startdate", itemStartDate);
				 intent.putExtra("User", itemUser);
				 intent.putExtra("Project", itemProject);
				 intent.putExtra("Activity", itemActivity);
				 intent.putExtra("Importancy", itemImportancy);
				 intent.putExtra("Status", itemStatus);
				 intent.putExtra("Urgency", itemUrgency);
				 intent.putExtra("Estimated_time", itemEstimated_time);
				 intent.putExtra("Reported_time", itemReported_time);
				 intent.putExtra("Description", itemDescription);
				 
				 intent.putExtra("StatusID", itemStatusID);
				 intent.putExtra("ImpID", itemImpID);
				 intent.putExtra("UrgID", itemUrgID);
				 
				 if(report_user != ""){
					 intent.putExtra("report_user", report_user); 
				 }
				 startActivity(intent);
				
			}

		});	
	}

	@Override
	public void onResume() {
	    super.onResume();
	 
	    String update = Globals.getValue("task_update");
	    if(update != ""){
	    	Globals.setValue("task_update", "");
	    	new LoadList().execute();
	    }
		Globals.setValue("edit_task","");
	}

	@Override
	protected void onDestroy() {
		hideProgressDialog();
		super.onDestroy();
	}

	static List<cTaskUser> listFiltered;
	static private ArrayList<cTaskUser> arraylist;

	public static List<cTaskUser> filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		listFiltered = new ArrayList<cTaskUser>();
		listFiltered.clear();
		if (charText.length() == 0) {
			listFiltered.addAll(arraylist);
		} else {
			for (cTaskUser project : arraylist) {
				if (project.name.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.user.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.project.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.activity.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.imp_name.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.sta_name.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.urg_name.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.deadline.toLowerCase(Locale.getDefault()).contains(charText)
						|| project.description.toLowerCase(Locale.getDefault()).contains(charText))
				{
					listFiltered.add(project);
				}
			}
		}
		return listFiltered;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// User pressed the search button

		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// User changed the text

		String text = newText.toString().toLowerCase(Locale.getDefault());
		List<cTaskUser> filter_tasks = filter(text.toString());
		taskList.clear(); // <--- clear the list before add

		for(cTaskUser entry : filter_tasks)
		{

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("id",  entry.id);
			map.put("name", entry.name);
			map.put("deadline", entry.deadline);
			map.put("startdate", entry.date);
			map.put("user", entry.user);
			map.put("project", entry.project);
			map.put("activity", entry.activity);
			map.put("importancy", entry.imp_name);
			map.put("status", entry.sta_name);
			map.put("urgency", entry.urg_name);
			map.put("estimated_time", entry.est_minutes);
			map.put("reported_time", entry.reported_minutes);
			map.put("description", entry.description);

			map.put("status_id", entry.status);
			map.put("importancy_id", entry.importancy);
			map.put("urgency_id",entry.urgency);

			taskList.add(map);
		}

		TasksAdapter adapter = new TasksAdapter(MyTasksListActivity.this, R.layout.listview_item_tasks, taskList);
		lv.setAdapter(null);
		lv.setAdapter(adapter);

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_menu, menu);

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(false);
		searchView.setQueryHint(getString(R.string.search_title_task));

		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_add_task:
				Intent intent = new Intent(this, AddTaskActivity.class); startActivity(intent);
				return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onUserInteraction() {
	    super.onUserInteraction();
	    userIsInteracting = true;
	}
	
	
	
	/**
	 * Background Async Task to Load all tasks by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

         showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals)getApplication());
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				report_user = myUserID;
			}
		
	    	 // load data from provider
		    taskList = new ArrayList<HashMap<String, String>>();
			arraylist = new ArrayList<cTaskUser>();
	    	
	    	 List<cTaskUser> tasks = MyProvider.getTaskUserList(report_user, report_status, report_project);
	    	 for(cTaskUser entry : tasks)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("deadline", entry.deadline);
                map.put("startdate", entry.date);
                map.put("user", entry.user);
                map.put("project", entry.project);
                map.put("activity", entry.activity);
                map.put("importancy", entry.imp_name);
                map.put("status", entry.sta_name);
                map.put("urgency", entry.urg_name);
                map.put("estimated_time", entry.est_minutes);
                map.put("reported_time", entry.reported_minutes);
                map.put("description", entry.description);
                
                map.put("status_id", entry.status);
                map.put("importancy_id", entry.importancy);
                map.put("urgency_id",entry.urgency);

                // adding HashList to ArrayList
      	    	taskList.add(map);

	    	 }

			arraylist.addAll(tasks);

	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */

	            	TasksAdapter adapter = null;

        			adapter = new TasksAdapter(MyTasksListActivity.this, R.layout.listview_item_tasks, taskList);
        			lv.setAdapter(adapter);
        			
        			/*pDialog.dismiss();*/

	            }
	        });
	        
	        hideProgressDialog();
	  	      
	    }
	  

	}
	
	
	 /**
		 * Background Async Task to Load all data by making HTTP Request
	  	 * */
		class LoadUserList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	 
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
		   	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
			
			Globals = ((MyGlobals)getApplication());
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

				userList = new ArrayList<HashMap<String, String>>();
				projectList = new ArrayList<HashMap<String, String>>();
				statusList = new ArrayList<HashMap<String, String>>();
				
				// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getMyUsers();
	    		for(cContact entry : List)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("pid", entry.id);
	                map.put("username", entry.username);
	                map.put("name", entry.name);

	                // adding HashList to ArrayList
	                userList.add(map);
	             }
	    		
	    		List<cProject> projList = MyProvider.getProjectsSpinnerPerUser(myUserID);
	    		for(cProject entry : projList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("id",  Long.toString(entry.id));
	                map.put("name", entry.name);
	              
	                // adding HashList to ArrayList
	                projectList.add(map);
	             }
	    		
	    		List<cStatus> stsList = MyProvider.getStatus();
	    		for(cStatus entry : stsList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("id",  entry.id);
	                map.put("name", entry.name);
	              
	                // adding HashList to ArrayList
	                statusList.add(map);
	             }
	    		
			return "";
	   }

	   /**
	    * After completing background task Dismiss the progress dialog
	    * **/
	   protected void onPostExecute(String file_url) {
	       // dismiss the dialog after getting all products

		   		// updating UI from Background Thread
		       runOnUiThread(new Runnable() {
		           public void run() {
		           
			       		String isManager = Globals.getValue("manager");
			    		if (isManager == "yes")
			    		{	
				    	    List<String> userArray =  new ArrayList<String>();
				    	    
				    	    
				           	for (HashMap<String, String> map : userList)
				          	        for (Entry<String, String> entry : map.entrySet())
				          	        {
				          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
				          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
				          	        }
	
				           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyTasksListActivity.this, android.R.layout.simple_spinner_item, userArray);
				      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				      	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
				      	    	sUsers.setAdapter(user_adapter);
				    	        
				    	        int spinnerPosition = user_adapter.getPosition(myUserName);
					    		//set the default according to value
					    		sUsers.setSelection(spinnerPosition);					    		
			    		}
			    		
			    		List<String> projectArray =  new ArrayList<String>();
			    	    List<String> statusArray =  new ArrayList<String>();
			    	    
		           	    projectArray.add(0, "Select a project");
		           	    projectkeyArray.add(0, "");
		           	    
		           	    statusArray.add(0, "Select status");
		           	    statuskeyArray.add(0, "");
			    	    
			    		for (HashMap<String, String> map : projectList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") projectArray.add(entry.getValue());
		           	        }
			           	
			           	for (HashMap<String, String> map : statusList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") statusArray.add(entry.getValue());
		           	        }
			           	
			           	ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(MyTasksListActivity.this, android.R.layout.simple_spinner_item, projectArray);
		      	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		      	    	Spinner sProject = (Spinner) findViewById(R.id.spinnerProject);
		      	    	sProject.setAdapter(project_adapter);
		      	    	
		      	    	ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(MyTasksListActivity.this, android.R.layout.simple_spinner_item, statusArray);
		      	    	status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		      	    	Spinner sStatus = (Spinner) findViewById(R.id.spinnerStatus);
		      	    	sStatus.setAdapter(status_adapter);
		      	    	
		      	    	
		        	   
		           }
		       	});
	       
	    	}
		}
		
		/**
		 * Background Async Task to Load all users by making HTTP Request
	  	 * */
		class LoadProjectListByUser extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	 
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
		   	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
			
			Globals = ((MyGlobals)getApplication());
			
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

				projectList = new ArrayList<HashMap<String, String>>();
	    		
	    		List<cProject> projList = MyProvider.getProjectsSpinnerPerUser(report_user);
	    		for(cProject entry : projList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("id",  Long.toString(entry.id));
	                map.put("name", entry.name);
	              
	                // adding HashList to ArrayList
	                projectList.add(map);
	             }
	    		
			return "";
	   }

	   /**
	    * After completing background task Dismiss the progress dialog
	    * **/
	   protected void onPostExecute(String file_url) {
	       // dismiss the dialog after getting all products

		   		// updating UI from Background Thread
		       runOnUiThread(new Runnable() {
		           public void run() {
			    		
			    		List<String> projectArray =  new ArrayList<String>();
			    		projectkeyArray = new ArrayList<String>();
			    		
			    		projectArray.add(0, "Select a project");
		           	    projectkeyArray.add(0, "");
			    	    
			    		for (HashMap<String, String> map : projectList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") projectArray.add(entry.getValue());
		           	        }
			           	
			           	ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(MyTasksListActivity.this, android.R.layout.simple_spinner_item, projectArray);
		      	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		      	    	Spinner sProject = (Spinner) findViewById(R.id.spinnerProject);
		      	    	sProject.setAdapter(project_adapter);
		      	    	
		      	    	new LoadList().execute();
 
		           }
		       	});
	       
	    	}
		}
		
		public void showProgressDialog(String msg) 
		{
			
			// check for existing progressDialog
			if (progressDialog == null) {
				// create a progress Dialog
				progressDialog = new ProgressDialog(this);

				// remove the ability to hide it by tapping back button
				progressDialog.setIndeterminate(true);
				
				progressDialog.setCancelable(false);
				progressDialog.setCanceledOnTouchOutside(false);
				
				progressDialog.setMessage(msg);

			}

			// now display it.
			progressDialog.show();		
		}	
		
		
		/**
		 * Hides the Progress Dialog
		 */
		public void hideProgressDialog() {
			
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			
			progressDialog = null;
		}

}
