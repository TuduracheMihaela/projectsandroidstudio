package com.sharethatdata.digitime.tasks;

import java.io.ByteArrayOutputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import com.google.android.material.textfield.TextInputLayout;
import com.sharethatdata.digitime.ListActivity;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.RecordTimeActivity;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.projects.AddProjectActivity;
import com.sharethatdata.digitime.projects.MyProjectsListActivity;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cStatus;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cTask;
import com.sharethatdata.webservice.datamodel.cTaskImportancy;
import com.sharethatdata.webservice.datamodel.cTaskUrgency;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AddTaskActivity extends AppCompatActivity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	
    List<cContact> employeeList; // getContacts 
	ArrayList<HashMap<String, String>> userList;
	ArrayList<HashMap<String, String>> projectList;
	ArrayList<HashMap<String, String>> activityList;
	ArrayList<HashMap<String, String>> importancyList;
	ArrayList<HashMap<String, String>> statusList;
	ArrayList<HashMap<String, String>> urgencyList;
	
	List<String> userkeyArray =  new ArrayList<String>();
	List<String> projectkeyArray =  new ArrayList<String>();
	List<String> activitykeyArray =  new ArrayList<String>();
	List<String> importancykeyArray =  new ArrayList<String>();
    List<String> statuskeyArray =  new ArrayList<String>();
    List<String> urgencykeyArray =  new ArrayList<String>();
    
    private int mStartYear,mStartMonth,mStartDay, mEndYear,mEndMonth,mEndDay;
	
	/*Spinner spinner = null;*/
	ProgressDialog progressDialog;
	
    private boolean no_network = false;
    private String error_message = "";

	TextInputLayout taskNameInputLayout, taskTimeInputLayout;
	EditText taskNameEditText, taskTimeEditText;
	
	TextView dateTextView;
	TextView textSelectCoWorker;
	String textSelectCoWorkerString, textSelectCoWorkerStringOriginal;

	String myDateStartTime;
	String myDateEndTime;
	String myContact = "";
	String myProject = "";
	String myActivity = "";
	String myImportancy = "";
	String myStatus = "";
	String myUrgency = "";
	String estimatedTime;
	String taskName;
	String description;
	
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_add_task);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(AddTaskActivity.this, AddTaskActivity.this);
		
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mEndYear = c.get(Calendar.YEAR);
	    mEndMonth = c.get(Calendar.MONTH);
	    mEndDay = c.get(Calendar.DAY_OF_MONTH);
	    
	    TextView startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
            .append(pad(mStartMonth + 1)).append("-")
            .append(pad(mStartYear)));
        
        TextView endDateView = (TextView) findViewById(R.id.dateEndText);
        endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
            .append(pad(mEndMonth + 1)).append("-")
            .append(pad(mEndYear)));
        
        textSelectCoWorker = (TextView) findViewById(R.id.textTaskMembers);

		taskNameInputLayout = (TextInputLayout) findViewById(R.id.name_text_input_layout);
		taskTimeInputLayout = (TextInputLayout) findViewById(R.id.estimatedtime_text_input_layout);
		taskNameEditText = (EditText) findViewById(R.id.textTask);
		taskTimeEditText = (EditText) findViewById(R.id.textEstimatedTimeHour);

        EditText editDesc = (EditText) findViewById(R.id.taskDescription);       
	    editDesc.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.taskDescription) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                    }
                }
                return false;
            }
        });
        
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
				  //boolean cancel = false;
					//View focusView = null;
				  boolean cancelName = false;
				  boolean cancelTime = false;
				  View focusViewName = null;
				  View focusViewTime = null;

				  boolean nameLenght = shouldShowErrorTaskName();
				  boolean timeLenght = shouldShowErrorTaskTime();

				  String edit = Globals.getValue("edit_task");
			        if(edit != ""){
			       	    // edit task

						if (nameLenght) {
							showErrorTaskName();
							focusViewName = taskNameEditText;
							cancelName = true;
						}

						if (timeLenght) {
							showErrorTaskTime();
							focusViewTime = taskTimeEditText;
							cancelTime = true;
						}

						if(cancelName){
							focusViewName.requestFocus();
						}else{
							hideErrorTaskName();
						}

						if(cancelTime){
							focusViewTime.requestFocus();
						}else{
							hideErrorTaskTime();
						}

						if(!cancelName && !cancelTime){
							hideKeyboard();

							String id = Globals.getValue("taskid");
							new EditTask(id).execute();
						}


			        	/*TextView EstimatedViewHour = (TextView) findViewById(R.id.textEstimatedTimeHour);
			        	String mHourEstimated = EstimatedViewHour.getText().toString();
						TextView TextViewTask = (TextView) findViewById(R.id.textTask);
						String mtask= TextViewTask.getText().toString();

						
						if (TextUtils.isEmpty(mHourEstimated)) {
							EstimatedViewHour.setError(getString(R.string.error_field_required));
							focusView = EstimatedViewHour;
							cancel = true;
						}

						if (TextUtils.isEmpty(mtask)) {
							TextViewTask.setError(getString(R.string.error_field_required));
							focusView = TextViewTask;
							cancel = true;
						}*/

			        	
			        }else{
						// create task

						if (nameLenght) {
							showErrorTaskName();
							focusViewName = taskNameEditText;
							cancelName = true;
						}

						if (timeLenght) {
							showErrorTaskTime();
							focusViewTime = taskTimeEditText;
							cancelTime = true;
						}

						if(cancelName){
							focusViewName.requestFocus();
						}else{
							hideErrorTaskName();
						}

						if(cancelTime){
							focusViewTime.requestFocus();
						}else{
							hideErrorTaskTime();
						}

						if(!cancelName && !cancelTime){
							hideKeyboard();

							RegisterTask register;
							register = new RegisterTask();
							register.execute();
						}


			        	/*TextView EstimatedViewHour = (TextView) findViewById(R.id.textEstimatedTimeHour);
			        	String mHourEstimated = EstimatedViewHour.getText().toString();
						//TextView EstimatedViewMinute = (TextView) findViewById(R.id.textEstimatedTimeMinute);
						//String mMinEstimated = EstimatedViewMinute.getText().toString();
						TextView TextViewTask = (TextView) findViewById(R.id.textTask);
						String mtask= TextViewTask.getText().toString();

						
						if (TextUtils.isEmpty(mHourEstimated)) {
							EstimatedViewHour.setError(getString(R.string.error_field_required));
							focusView = EstimatedViewHour;
							cancel = true;
						}
						*//*if (TextUtils.isEmpty(mMinEstimated)) {
							EstimatedViewMinute.setError(getString(R.string.error_field_required));
							focusView = EstimatedViewMinute;
							cancel = true;
						}*//*
						if (TextUtils.isEmpty(mtask)) {
							TextViewTask.setError(getString(R.string.error_field_required));
							focusView = TextViewTask;
							cancel = true;
						}*/



			        }

			  }
		});
		
		new LoadList().execute();
		
		textSelectCoWorker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LoadEmployeeList().execute();
			}
		});
		
		//////////////////////////////////////////////////////////////
		/*
		 * EDIT
		 */
		
		String edit = Globals.getValue("edit_task");
        if(edit != ""){
        	btnSubmit.setVisibility(View.VISIBLE);
        	btnSubmit.setText("Edit task");
			setTitle(getString(R.string.text_task_modify));
			//TextView textViewTitle = (TextView) findViewById(R.id.textView1);
			//textViewTitle.setText("Modify the current task");
        }else{
        	btnSubmit.setVisibility(View.VISIBLE);
        	btnSubmit.setText("Add new task");
			setTitle(getString(R.string.text_task_create));
			//TextView textViewTitle = (TextView) findViewById(R.id.textView1);
			//textViewTitle.setText("Create a new task");
        }
		
	}

	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }

	private boolean shouldShowErrorTaskName() {
		int textLength = taskNameEditText.getText().length();
		return textLength <= 0 ;
	}

	private boolean shouldShowErrorTaskTime() {
		int textLength = taskTimeEditText.getText().length();
		return textLength <= 0 ;
	}

	private void showErrorTaskName() {
		taskNameInputLayout.setErrorEnabled(true);
		taskNameInputLayout.setError(getString(R.string.text_task_name_required));
	}

	private void showErrorTaskTime() {
		taskTimeInputLayout.setErrorEnabled(true);
		taskTimeInputLayout.setError(getString(R.string.text_task_time_required));
	}

	private void hideErrorTaskName() {
		taskNameInputLayout.setErrorEnabled(false);
		taskNameInputLayout.setError("");
	}

	private void hideErrorTaskTime() {
		taskTimeInputLayout.setErrorEnabled(false);
		taskTimeInputLayout.setError("");
	}

	private void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
					hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	    		        
	            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	                .append(pad(mStartMonth + 1)).append("-")
	                .append(pad(mStartYear)));     
	    }
	};
	
	private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
			    mEndYear = year;
	            mEndMonth = month;
	            mEndDay = day; 
	            		
	            TextView endDateView = (TextView) findViewById(R.id.dateEndText);
	    		        
	            endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
	                .append(pad(mEndMonth + 1)).append("-")
	                .append(pad(mEndYear)));
	    }
	};
	
	public void startDateClick(View v) 
	{
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
	    dp1.show();
	}
	
	public void endDateClick(View v) 
	{
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
	    dp1.show();
	}
	
	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         progressDialog = new ProgressDialog(AddTaskActivity.this);
         progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
      
         progressDialog.setIndeterminate(false);
         progressDialog.setCancelable(false);
         progressDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	userList = new ArrayList<HashMap<String, String>>();
	    	projectList = new ArrayList<HashMap<String, String>>();
	    	activityList = new ArrayList<HashMap<String, String>>();
	    	importancyList = new ArrayList<HashMap<String, String>>();
	    	statusList = new ArrayList<HashMap<String, String>>();
	    	urgencyList = new ArrayList<HashMap<String, String>>();
	    	
	    	// load data from provider
	    	List<cContact> contactsList = MyProvider.getContacts();
	    	//List<cContact> contactsList = MyProvider.getMyUsers();
	    	for(cContact entry : contactsList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                userList.add(map);
             }
    		
    		List<cProject> projList = MyProvider.getProjectsSpinner(myUserID);
    		for(cProject entry : projList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                projectList.add(map);
             }
    		
    		List<cActivity> actList = MyProvider.getUserActivity(myUserID);
    		for(cActivity entry : actList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                activityList.add(map);
             }
    
    		List<cTaskImportancy> impList = MyProvider.getTaskImportancy();
    		for(cTaskImportancy entry : impList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                importancyList.add(map);
             }

    		List<cStatus> stsList = MyProvider.getStatus();
    		for(cStatus entry : stsList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                statusList.add(map);
             }
    		
    		List<cTaskUrgency> urgList = MyProvider.getTaskUrgency();
    		for(cTaskUrgency entry : urgList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                urgencyList.add(map);
             }

	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.error_network);
	         	
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	        progressDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	/*if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"
	    			 	            		   	 
	            	} else {*/
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	
		            	// store the id of the item in a separate array
		             
	            		List<String> userArray =  new ArrayList<String>();
	            		List<String> projectArray =  new ArrayList<String>();
	            		List<String> activityArray =  new ArrayList<String>();
		           	    List<String> importancyArray =  new ArrayList<String>();
		           	    List<String> statusArray =  new ArrayList<String>();
		           	    List<String> urgencyArray =  new ArrayList<String>();
		           	    
		           	   
		           	    // load spinner data from dataprovider
		           	   for (HashMap<String, String> map : userList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") userkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
		           	        }
		           	    for (HashMap<String, String> map : projectList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") projectkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") projectArray.add(entry.getValue());
		           	        }
		           		for (HashMap<String, String> map : activityList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") activitykeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") activityArray.add(entry.getValue());
		           	        }
		           	    for (HashMap<String, String> map : importancyList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") importancykeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") importancyArray.add(entry.getValue());
		           	        }
		           		for (HashMap<String, String> map : statusList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") statusArray.add(entry.getValue());
		           	        }
		           		for (HashMap<String, String> map : urgencyList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") urgencykeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") urgencyArray.add(entry.getValue());
		           	        }
		           		
		           		final ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, userArray);
		           		user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	final Spinner sUser = (Spinner) findViewById(R.id.spinnerUser);
	          	       	sUser.setAdapter(user_adapter);
		      	        
		      	        int spinnerPosition = user_adapter.getPosition(myUserName);
		  	    		//set the default according to value
		  	    		sUser.setSelection(spinnerPosition);
	           	       	
	          	        final ArrayAdapter<String> project_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, projectArray);
	           	    	project_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final Spinner sProjects = (Spinner) findViewById(R.id.spinnerProject);
	           	    	sProjects.setAdapter(project_adapter);
	          	       	
	           	    	final ArrayAdapter<String> activity_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, activityArray);
	           	    	activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sActivities = (Spinner) findViewById(R.id.spinnerActivity);
	           	    	sActivities.setAdapter(activity_adapter);
	          	       	
	           	    	final ArrayAdapter<String> importancy_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, importancyArray);
	           	    	importancy_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	final Spinner sImportancy = (Spinner) findViewById(R.id.spinnerImportancy);
	          	        sImportancy.setAdapter(importancy_adapter);
	           	    
	           	    	final ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, statusArray);
	           	    	status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sStatus = (Spinner) findViewById(R.id.spinnerStatus);
	           	    	sStatus.setAdapter(status_adapter);
	           	       
	           	    	ArrayAdapter<String> urgency_adapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, urgencyArray);
	           	    	urgency_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	final Spinner sUrgency = (Spinner) findViewById(R.id.spinnerUrgency);
	           	    	sUrgency.setAdapter(urgency_adapter);
	           	    	
	           	    	final TextView mView = (TextView) findViewById(R.id.textTaskMembers);
	           	       	mView.setText(myUserName);
						textSelectCoWorkerString = mView.getText().toString();
	           	       
	            	//}
	           	    	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	           	    	String edit = Globals.getValue("edit_task");

	           	    	String id = Globals.getValue("taskid");
	                	String text_task =  Globals.getValue("Name");
	                	String text_deadline = Globals.getValue("Deadline");
	                	String text_startdate = Globals.getValue("StartDate");
	                	String text_user = Globals.getValue("User");
	                	String text_project = Globals.getValue("Project");
	                	String text_activity = Globals.getValue("Activity");
	                	String text_imp = Globals.getValue("Importancy");
	                	String text_sts = Globals.getValue("Status");
	                	String text_urg = Globals.getValue("Urgency");	
	                	String text_estTime =  Globals.getValue("Estimated_time");
	                	String text_repTime =  Globals.getValue("Reported_time");
	                	String textMembers = Globals.getValue("textViewMembers");
	                	String textDescription =  Globals.getValue("Description");
	                	
	                	String text_stsID =  Globals.getValue("StatusID");
	                	String text_impID =  Globals.getValue("ImpID");
	                	String text_urgID =  Globals.getValue("UrgID");
	                	
	                	if(edit != ""){
	                	
	                		 // date start
	                		SimpleDateFormat json_format = new SimpleDateFormat("dd/MM/yyyy");  
	                		SimpleDateFormat new_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                    	try {  
	                    	    Date dateCreated = json_format.parse(text_startdate);  
	                    	    Date dateDeadline = json_format.parse(text_deadline); 
	                    	    Date today = new Date(); 
	                    	    today=new Date(today.getTime()-(24*60*60*1000));
	                    	    if (dateCreated.before(today) )
	                    	    {
	                    	    	new_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                    	    } 
	                    	    text_startdate = new_format.format(dateCreated);
	                    	    text_deadline = new_format.format(dateDeadline);
	                    	    System.out.println("myTask DATE -> " + dateCreated);
	                    	    System.out.println("myTask DATE -> " + dateDeadline);
	                    	      
	                    	} catch (ParseException e) {  
	                    	    // TODO Auto-generated catch block  
	                    	    e.printStackTrace();  
	                    	}
	                    	
	        		        StringTokenizer tkDateStart = new StringTokenizer(text_startdate);
	        		        String dateTokenizerStart = tkDateStart.nextToken();  // <---  yyyy-mm-dd
	        		        String[] separatedDateStart = dateTokenizerStart.split("-");
	        		        String year_start = separatedDateStart[0];
	        		        String month_start = separatedDateStart[1];
	        		        String day_start = separatedDateStart[2];	   
	        		        
	        		        // date end
	        		        StringTokenizer tkDateEnd = new StringTokenizer(text_deadline);
	        		        String dateTokenizerEnd = tkDateEnd.nextToken();  // <---  yyyy-mm-dd
	        		        String[] separatedDateEnd = dateTokenizerEnd.split("-");
	        		        String year_end = separatedDateEnd[0];
	        		        String month_end = separatedDateEnd[1];
	        		        String day_end = separatedDateEnd[2];	    
	        				
	        			    mStartYear = Integer.valueOf(year_start);
	        			    mStartMonth = Integer.valueOf(month_start);
	        			    mStartDay = Integer.valueOf(day_start);
	        			    mEndYear = Integer.valueOf(year_end);
	        			    mEndMonth = Integer.valueOf(month_end);
	        			    mEndDay= Integer.valueOf(day_end);
		        		        
		        			    TextView startDateView = (TextView) findViewById(R.id.dateStartText);
		        		        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
		        		            .append(pad(mStartMonth)).append("-")
		        		            .append(pad(mStartYear)));
		        		        
		        		        TextView endDateView = (TextView) findViewById(R.id.dateEndText);
		        		        endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
		        		            .append(pad(mEndMonth)).append("-")
		        		            .append(pad(mEndYear)));
		        		        
		        		        mStartMonth = mStartMonth - 1;
		        		        mEndMonth = mEndMonth - 1;

		        		        // Spinners		        		        
		           		    	int spinnerPosition1 = user_adapter.getPosition(text_user);
		           		    	//set the default according to value
		           		    	sUser.setSelection(spinnerPosition1);
		           		    	
		           		    	int spinnerPosition2 = project_adapter.getPosition(text_project);
		        	    		//set the default according to value
		           		    	sProjects.setSelection(spinnerPosition2);	    
		           		    	
		           		    	int spinnerPosition3 = activity_adapter.getPosition(text_activity);
		        	    		//set the default according to value
		           		    	sActivities.setSelection(spinnerPosition3);	
		           		    	
		           		    	int spinnerPosition4 = importancy_adapter.getPosition(text_imp);
		        	    		//set the default according to value
		           		    	sImportancy.setSelection(spinnerPosition4);	
		           		    	
		           		    	int spinnerPosition5 = status_adapter.getPosition(text_sts);
		        	    		//set the default according to value
		           		    	sStatus.setSelection(spinnerPosition5);	
		           		    	
		           		    	int spinnerPosition6 = urgency_adapter.getPosition(text_urg);
		        	    		//set the default according to value
		           		    	sUrgency.setSelection(spinnerPosition6);		          			  
		           		    	
		           		        TextView estTimeTextHour = (TextView) findViewById(R.id.textEstimatedTimeHour);
		           		       // TextView estTimeTextMinute = (TextView) findViewById(R.id.textEstimatedTimeMinute);
		           		        String[] estTime = text_estTime.split(":");
		           		        String estHour = estTime[0];
		           		        //String estMinute = estTime[1];
		           		        estTimeTextHour.setText(estHour);
		           		       // estTimeTextMinute.setText(estMinute);
			        		    
			        		    TextView taskText = (TextView) findViewById(R.id.textTask);
			        		    taskText.setText(text_task);

								TextView membersText = (TextView) findViewById(R.id.textTaskMembers);
								membersText.setText(textMembers);
								textSelectCoWorkerString = textMembers;
								textSelectCoWorkerStringOriginal = textMembers;
			        		    
			        		    TextView descriptionText = (TextView) findViewById(R.id.taskDescription);
			        		    descriptionText.setText(textDescription);

	                	}
  	
	              }
	        });
	              
	    }

	}
	
	
    /**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadEmployeeList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();  
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {   
	    
	   		employeeList = MyProvider.getContacts();
	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() { 
	            	
	            	
	            	if (employeeList.size() > 0)
	            	{
		            	final List<String> employee_list = new ArrayList<String>();
		            	// create array of string
		            	for(cContact v : employeeList)
		            	{
		            		employee_list.add(v.name);	            	
		            	}
		            	final CharSequence[] items = employee_list.toArray(new String[employee_list.size()]);
		            	final boolean[] checkedItems = new boolean[employeeList.size()];	            	
						final TextView mView = (TextView) findViewById(R.id.textTaskMembers);
						
						String text = textSelectCoWorkerString;
						Log.d("TEXT", text);
						String[] member_list = null;
						
						if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
						  {
							if(textSelectCoWorkerString.contains(";")){
								member_list = textSelectCoWorkerString.split(" ; ");
							}else{
								member_list = textSelectCoWorkerString.split("\n");
							}
						  }
						
						for (int i = 0; i < items.length; i++) {
							if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
							  {
								  for (String item : member_list)
								  {
									  if (items[i].equals(item)) {
										  checkedItems[i] = true;										  
									  }
								  }			  			 
							  } 
						}

		            	 // Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(AddTaskActivity.this);
		
		                // Set a title for alert dialog
		                builder.setTitle(getString(R.string.text_alert_dialog_selectMembers))
		                	// Specify the dialog is not cancelable
		                		.setCancelable(false)
		                		.setMultiChoiceItems(items, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which, boolean isChecked) {
										  
										// Update the current focused item's checked status
										checkedItems[which] = isChecked;

				                        // Get the current focused item
				                        String currentItem = employee_list.get(which);

				                        // Notify the current action
				                        Toast.makeText(getApplicationContext(),
				                                currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
		                		   			
						             } 
								});
		                		   	
		                //Set the positive/yes button click listener
		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								mView.setText("");
								textSelectCoWorkerString = "";

		                        for (int i = 0; i<checkedItems.length; i++){
		                            boolean checked = checkedItems[i];
		                            if (checked) {
		                            	mView.setText(mView.getText() + employee_list.get(i) + "\n");
		                            	textSelectCoWorkerString = mView.getText().toString();
		                            }
		                        }
							}
						});

		                // Set the neutral/cancel button click listener
		                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
		                    @Override
		                    public void onClick(DialogInterface dialog, int which) {
		                    	
		                    }
		                });
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                
	            	}
	            }
	        });    
	    }
	}

	  /** AsyncTask register task  */
  private class RegisterTask extends AsyncTask<String, String, Boolean>{
	  
	  @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	         progressDialog = new ProgressDialog(AddTaskActivity.this);
	         progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
	      
	         progressDialog.setIndeterminate(false);
	         progressDialog.setCancelable(false);
	         progressDialog.show();
	     }

      @Override
      protected Boolean doInBackground(String... args) {

      	  myDateStartTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
      	  myDateEndTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();

      	  // extract data
		 
	      	  Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myContact = userkeyArray.get(myIndex);		  
			  }
			  
			  Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
			  myIndex = projectSpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myProject = projectkeyArray.get(myIndex);
			  }
			  
			  
			  Spinner activitySpinner = (Spinner) findViewById(R.id.spinnerActivity);
			  myIndex = activitySpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myActivity = activitykeyArray.get(myIndex);
			  }
			  
			  Spinner importancySpinner = (Spinner) findViewById(R.id.spinnerImportancy);
			  myIndex = importancySpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myImportancy = importancykeyArray.get(myIndex);		  
			  }
			  			  			  
			  Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  myIndex = statusSpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myStatus = statuskeyArray.get(myIndex);
			  }
			  
			  Spinner urgencySpinner = (Spinner) findViewById(R.id.spinnerUrgency);
			  myIndex = urgencySpinner.getSelectedItemPosition();
			  if(myIndex != -1){
				  myUrgency = urgencykeyArray.get(myIndex);
			  }
			  
			  TextView EstimatedViewHour = (TextView) findViewById(R.id.textEstimatedTimeHour);
			  //TextView EstimatedViewMinute = (TextView) findViewById(R.id.textEstimatedTimeMinute);
		      String estimatedTimeHour = EstimatedViewHour.getText().toString();	
		      //String estimatedTimeMinute = EstimatedViewMinute.getText().toString();
		      int estTime = Integer.valueOf(estimatedTimeHour) * 60; // + Integer.valueOf(estimatedTimeMinute);
		      estimatedTime = String.valueOf(estTime);
		      
		      TextView taskView = (TextView) findViewById(R.id.textTask);
		      taskName = taskView.getText().toString();
			  
			  TextView DescriptionView = (TextView) findViewById(R.id.taskDescription);
		      description = DescriptionView.getText().toString();
			  
		      int TaskID = 0;
			  boolean suc = false;
			  
			// call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");

			  	TaskID = MyProvider.createTask(myDateStartTime, myDateEndTime,myContact, myProject, myActivity, myImportancy, myStatus, myUrgency, estimatedTime, taskName, description);
			  
			  	if(TaskID != 0){
			  		if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
					  {
			  			String[] member_list;
						if(textSelectCoWorkerString.contains(";")){
							member_list = textSelectCoWorkerString.split(" ; ");
						}else{
							member_list = textSelectCoWorkerString.split("\n");
						}
						
						for (String item : member_list)
						  {
							  cContact myMember = MyProvider.getUser(this_organisation, item);
							  if (myMember.id != "")
							  suc =  MyProvider.createTaskMember(String.valueOf(TaskID), myMember.id);	    
						  }	
						
					  }
			  	}
			  	
      	  return suc;
      }
         
      /**
	     * After completing background task Dismiss the progress dialog
	     * **/
      protected void onPostExecute(final Boolean success) {
      	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 Toast.makeText(AddTaskActivity.this, getString(R.string.toast_add_task_false), Toast.LENGTH_SHORT).show();
		    		}else{
		    			 Toast.makeText(AddTaskActivity.this, getString(R.string.toast_add_task_true), Toast.LENGTH_SHORT).show();
		    			
		    			Intent intent = new Intent(getApplicationContext(), MyTasksListActivity.class);
		    			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    			startActivity(intent); 
		    		}
         	   	
	              }
	        });
	        
	        progressDialog.dismiss();
	         
	    }
  }
  
  private class EditTask extends AsyncTask<String, String, Boolean> {

	  String myID = "";

	  public EditTask(String id) {
		  myID = id;
	  }
	  
	  	 @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	         progressDialog = new ProgressDialog(AddTaskActivity.this);
	         progressDialog.setMessage( getString(R.string.loading_alert_dialog) + " ");
	      
	         progressDialog.setIndeterminate(false);
	         progressDialog.setCancelable(false);
	         progressDialog.show();
	     }

		  protected Boolean doInBackground(String... urls) {
			  
			 // TextView startDateView = (TextView) findViewById(R.id.dateStartText);
			 // String myDateTimeStart =  dateStart;
			 // TextView endDateView = (TextView) findViewById(R.id.dateEndText);
			  //String myDateTimeEnd =   dateEnd;
			  
			String myDateTimeStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
			String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
      	
      	  // extract data
		 
			  Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myContact = userkeyArray.get(myIndex);
			  
			  Spinner projectSpinner = (Spinner) findViewById(R.id.spinnerProject);
			  myIndex = projectSpinner.getSelectedItemPosition();
			  String myProject = projectkeyArray.get(myIndex);
			  			  			  
			  Spinner activitySpinner = (Spinner) findViewById(R.id.spinnerActivity);
			  myIndex = activitySpinner.getSelectedItemPosition();
			  String myActivity = activitykeyArray.get(myIndex);

			  Spinner impSpinner = (Spinner) findViewById(R.id.spinnerImportancy);
			  myIndex = impSpinner.getSelectedItemPosition();
			  String myImp = importancykeyArray.get(myIndex);
			  
			  Spinner stsSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  myIndex = stsSpinner.getSelectedItemPosition();
			  String mySts = statuskeyArray.get(myIndex);
			  
			  Spinner urgSpinner = (Spinner) findViewById(R.id.spinnerUrgency);
			  myIndex = urgSpinner.getSelectedItemPosition();
			  String myUrg = urgencykeyArray.get(myIndex);	
		      
			  TextView EstimatedViewHour = (TextView) findViewById(R.id.textEstimatedTimeHour);
			  //TextView EstimatedViewMinute = (TextView) findViewById(R.id.textEstimatedTimeMinute);
		      String estimatedTimeHour = EstimatedViewHour.getText().toString();	
		     // String estimatedTimeMinute = EstimatedViewMinute.getText().toString();
		      int estTime = Integer.valueOf(estimatedTimeHour) * 60; // + Integer.valueOf(estimatedTimeMinute);
		      String estimatedTime = String.valueOf(estTime);
		      
		      
		      TextView taskView = (TextView) findViewById(R.id.textTask);
		      String taskName = taskView.getText().toString();	
			  
			  TextView DescriptionView = (TextView) findViewById(R.id.taskDescription);
		      String description = DescriptionView.getText().toString();
			  
			// call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");
			  
			  boolean suc = false;

			  
					  //if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null) {
			  			String[] member_list;
			  			String[] member_list_original = new String[0];
						if(textSelectCoWorkerString.contains(";")){
							member_list = textSelectCoWorkerString.split(" ; ");
						}else{
							member_list = textSelectCoWorkerString.split("\n");
						}

			  if (textSelectCoWorkerStringOriginal != "" && textSelectCoWorkerStringOriginal != null)
			  {
				  if(textSelectCoWorkerStringOriginal.contains(";")) {
					  member_list_original = textSelectCoWorkerStringOriginal.split(" ; ");
				  }else{
					  member_list_original = textSelectCoWorkerStringOriginal.split("\n");
				  }
			  }

			  List<String> sList = new ArrayList(Arrays.asList(member_list));
			  List<String> sListOriginal = new ArrayList(Arrays.asList(member_list_original));
			  List<String> sListADD = new ArrayList(Arrays.asList(member_list));
			  List<String> sListDELETE = new ArrayList();


			  for(String item1 : sListOriginal){
				  if(sList.contains(item1)){
					  sListADD.remove(item1); // here are the new members, add them as member to project
					  						  // who remains in this list, are the new members
					  						  // and who is repeated, remove from this list
				  }else{
					  sListDELETE.add(item1); // delete members those are unchecked from previous members list
				  }
			  }


			  if(sListADD.size() > 0){
				  for (String item3 : sListADD){
					  Log.d("member_list_add", item3);

					  cContact myMember = MyProvider.getUser(this_organisation, item3);
					  if (myMember.id != "")
						  suc =  MyProvider.createTaskMember(String.valueOf(myID), myMember.id);
					  Log.d("createTaskMember", myMember.id);
				  }
			  }

			  if(sListDELETE.size() > 0){
				  for (String item4 : sListDELETE){
					  Log.d("member_list_delete", item4);

					  cContact myMember = MyProvider.getUser(this_organisation, item4);
					  if (myMember.id != "")
						  suc = MyProvider.deleteTaskMember(String.valueOf(myID), myMember.id); // delete member of this project
					  Log.d("deleteTaskMember", myMember.id);

				  }
			  }
						
						/*for (String item : member_list)
						  {
							  cContact myMember = MyProvider.getUser(this_organisation, item);
							  if (myMember.id != "")
							  suc =  MyProvider.createTaskMember(String.valueOf(myID), myMember.id);	    
						  }*/
						
					  /*}else{
						  TextView membersText = (TextView) findViewById(R.id.textTaskMembers);	        		    
		        		    String members = membersText.getText().toString();
		        		    String[] member_list;
		        		    if(members.contains(";")){
								member_list = members.split(" ; ");
							}else{
								member_list = members.split("\n");
							}
							  for (String item : member_list)
							  {
								  cContact myMember = MyProvider.getUser(this_organisation, item);
								  if (myMember.id != "")
								  suc =  MyProvider.createTaskMember(String.valueOf(myID), myMember.id);	    
							  }	
					  }*/
			if (suc)
			{
			  suc = MyProvider.updateTask(myID, taskName, myProject, myDateTimeStart, myActivity, description, myDateTimeEnd, estimatedTime, myImp, mySts, myUrg, myContact);
			}
			
			  return suc;
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Task updated", Toast.LENGTH_SHORT).show();
				  Globals.setValue("task_update", "task_update");
				  Globals.setValue("edit_task","");
				  Intent intent = new Intent(AddTaskActivity.this, MyTasksListActivity.class);
				  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  startActivity(intent);
			  }else{
				  error_message = MyProvider.last_error;
        		  
	          	  UIHelper myUIHelper = new UIHelper();
		    			    
		    	  myUIHelper.ShowDialog(AddTaskActivity.this, getString(R.string.text_error), error_message);
		    			 
				  Toast.makeText(getApplicationContext(), "Was not updated task", Toast.LENGTH_SHORT).show();
				  Globals.setValue("task_update", "task_update");
				  Globals.setValue("edit_task","");
				  Intent intent = new Intent(AddTaskActivity.this, MyTasksListActivity.class);
				  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  startActivity(intent);
			  }	
			  
			  progressDialog.dismiss();
			  
		  }
	}
  
  
	

}
