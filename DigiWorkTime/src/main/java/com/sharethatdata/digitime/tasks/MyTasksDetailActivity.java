package com.sharethatdata.digitime.tasks;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.StatisticsActivity;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.digitime.adapter.NotesAdapter;
import com.sharethatdata.digitime.projects.AddProjectActivity;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectItem;
import com.sharethatdata.webservice.datamodel.cProjectMember;
import com.sharethatdata.webservice.datamodel.cTaskMember;
import com.sharethatdata.webservice.datamodel.cTimeRecord;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MyTasksDetailActivity extends AppCompatActivity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	private int mYear,mMonth,mDay;
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndYear,mEndMonth,mEndDay;
	private int mStartHour,mStartMinute;
	
	private String report_user = "";
	
	private static ProgressDialog pDialog;
	
	ArrayList<HashMap<String, String>> reportsList;
	
	TextView startDateView;
	TextView endDateView;
	
    TextView textViewTask;
    TextView textViewDeadline;
    TextView textViewUser;
    TextView textViewProject;
    TextView textViewActivity;
    TextView textViewImportancy;
    TextView textViewStatus;
    TextView textViewUrgency;
    TextView textViewEstimatedTime;
    TextView textViewReportedTime;
    TextView textViewRemainingTime;
    TextView textViewMembers;
    TextView textViewDescription;
    
    Button buttonEditTask;
    Button buttonDeleteTask;
    
    ListView lv = null;
	
	private String id_task = "";
	private String task = "";
	private String deadline = "";
	private String start_date = "";
	private String user = "";
	private String project = "";
	private String activity = "";
	private String importancy = "";
	private String status = "";
	private String urgency = "";
	private String estimated_time = "";
	private String reported_time = "";
	private String remaining_time = "";
	private String description = "";
	
	private String stsID = "";
	private String impID = "";
	private String urgID = "";
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_tasks_detail);

		Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
		setSupportActionBar(myToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		Globals = ((MyGlobals)getApplication());
		Globals.backgroundProcess();
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myUserID = Globals.getValue("idContact"); // id of user logged
		myUserName = Globals.getValue("nameContact"); // name of user logged

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyTasksDetailActivity.this, MyTasksDetailActivity.this);
		
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = 1;
	    
	    mEndYear = c.get(Calendar.YEAR);
	    mEndMonth = c.get(Calendar.MONTH);
	    mEndDay = c.get(Calendar.DAY_OF_MONTH);
	    
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    
	    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	    String weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];

	    startDateView = (TextView) findViewById(R.id.dateStartText);
        endDateView = (TextView) findViewById(R.id.dateEndText);
        startDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
        endDateView.setText(new StringBuilder().append(weekdayname).append(" ").append(pad(mEndDay)).append("/").append(pad(mEndMonth + 1)).append("/").append(pad(mEndYear)));
		
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String valueID = extras.getString("ID");   
		    String valueName = extras.getString("Name");
		    String valueDeadline = extras.getString("Deadline");
		    String valueStartDate = extras.getString("Startdate");
		    String valueUser = extras.getString("User");
		    String valueProject = extras.getString("Project");
		    String valueActivity = extras.getString("Activity");
		    String valueImportancy = extras.getString("Importancy");
		    String valueStatus = extras.getString("Status");
		    String valueUrgency = extras.getString("Urgency");
		    String valueEstimated_time = extras.getString("Estimated_time");
		    String valueReported_time = extras.getString("Reported_time");
		    String valueDescription = extras.getString("Description");
		    
		    String valueStatusID = extras.getString("StatusID");
		    String valueImpID = extras.getString("ImpID");
		    String valueUrgID = extras.getString("UrgID");
		    
		    String get_report_user = extras.getString("report_user");
		    
		    	id_task = valueID; 
		    	task = valueName;
		    	deadline = valueDeadline;
		    	start_date = valueStartDate;
		    	user = valueUser;
		    	project = valueProject;
		    	activity = valueActivity;
		    	importancy = valueImportancy;
		    	status = valueStatus;
		    	urgency = valueUrgency;
		    	estimated_time = valueEstimated_time;
		    	reported_time = valueReported_time;
		    	description = valueDescription;
		    	
		    	stsID = valueStatusID;
		    	impID = valueImpID;
		    	urgID = valueUrgID;
		    	
		    	report_user = get_report_user;

		}
		
		
	    textViewTask = (TextView) findViewById(R.id.textViewTask);
	    textViewDeadline = (TextView) findViewById(R.id.textViewDeadline);
	    textViewUser = (TextView) findViewById(R.id.textViewUser);
	    textViewProject = (TextView) findViewById(R.id.textViewProject);
	    textViewActivity = (TextView) findViewById(R.id.textViewActivity);
	    textViewImportancy = (TextView) findViewById(R.id.textViewImportancy);
	    textViewStatus = (TextView) findViewById(R.id.textViewStatus);
	    textViewUrgency = (TextView) findViewById(R.id.textViewUrgency);
	    textViewEstimatedTime = (TextView) findViewById(R.id.textViewEstimatedTime);
	    textViewReportedTime = (TextView) findViewById(R.id.textViewReportedTime);
	    textViewRemainingTime = (TextView) findViewById(R.id.textViewRemainingTime);
	    textViewMembers = (TextView) findViewById(R.id.textViewMembers);
	    textViewDescription = (TextView) findViewById(R.id.textViewDescription);
	    buttonEditTask = (Button) findViewById(R.id.buttonEditTask);
		buttonDeleteTask = (Button) findViewById(R.id.buttonDeleteTask);
	    
	    lv = (ListView) findViewById(R.id.listViewReports);
	    
	    
		new LoadTaskItem().execute();
		LoadReports();
	}
	
	
	public void onClickEditTask(View v){

		    	Globals.setValue("edit_task", "edit_task");
		  		Globals.setValue("taskid", id_task.toString());
		  		Globals.setValue("Name", task);
		  		Globals.setValue("Deadline", deadline);
		  		Globals.setValue("StartDate", start_date);
		  		Globals.setValue("User", user);
		  		Globals.setValue("Project", project);
		  		Globals.setValue("Activity", activity);
		  		Globals.setValue("Importancy", importancy);
		  		Globals.setValue("Status", status);
		  		Globals.setValue("Urgency", task);
		  		Globals.setValue("textViewMembers", textViewMembers.getText().toString());
		  		
		  		  //Estimated time
		  		  int estimated_time_minutes = Integer.valueOf(estimated_time);
				  // calc hours format
				  float fhourEst = (estimated_time_minutes / 60);
	              float fminuteEst = (((float) estimated_time_minutes / 60) - fhourEst) * 60;
		  		String hoursEst = "";
	              if (estimated_time_minutes > 0)
	              {
	            	  if(fhourEst <= 9 && fminuteEst <= 9){
	            		  hoursEst = "0" + String.format("%.0f", fhourEst) + ":" + "0" + String.format("%.0f", fminuteEst) + "";
	            	  }
	            	  else if(fhourEst <= 9 && fminuteEst >= 9){
	            		  hoursEst = "0" + String.format("%.0f", fhourEst) + ":" + String.format("%.0f", fminuteEst) + "";
	            	  }
	            	  else if(fhourEst >= 9 && fminuteEst <= 9){
	            		  hoursEst = String.format("%.0f", fhourEst) + ":" + "0" + String.format("%.0f", fminuteEst) + "";
	            	  }
	            	  else if(fhourEst >= 9 && fminuteEst >= 9){
	            		  hoursEst = String.format("%.0f", fhourEst) + ":" + String.format("%.0f", fminuteEst) + ""; 
	            	  }
	            	 
	              } else {
	            	  if(fhourEst <= 9){
	            		  hoursEst = "0" + String.format("%.0f", fhourEst) + "";
	            	  }else{
	            		  hoursEst = String.format("%.0f", fhourEst) + "";
	            	  }
	            	 
	              }

		  		Globals.setValue("Estimated_time", hoursEst);
		  		
		  		Globals.setValue("Reported_time", reported_time);
		  		Globals.setValue("Description", description);
		  		
		  		Globals.setValue("StatusID", stsID);
		  		Globals.setValue("ImpID", impID);
		  		Globals.setValue("UrgID", urgID);

		  		
		  		Intent i = new Intent(MyTasksDetailActivity.this, AddTaskActivity.class);
		  		startActivity(i);	    	   	  
	}
	
	public void onClickDeleteTask(View v){
		 new AlertDialog.Builder(MyTasksDetailActivity.this)
		  .setTitle("Delete task")
		  .setMessage("Do you really want to delete this task?")
		  .setIcon(android.R.drawable.ic_dialog_alert)
		  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		      public void onClick(DialogInterface dialog, int whichButton) {
		    	  				      
		    	  new DeleteTask(id_task).execute();
		          
		      }})
		   .setNegativeButton(android.R.string.no, null).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	 /**
		 * Background Async Task to Load Project Item by making HTTP Request
	  	 * */
		class LoadTaskItem extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	      // Showing progress dialog
	         pDialog = new ProgressDialog(MyTasksDetailActivity.this);
	         pDialog.setTitle("Loading Task");
	         pDialog.setMessage("Please wait...");
	         pDialog.setIndeterminate(false);
	         pDialog.setCancelable(true);
	         pDialog.show();
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		    	
		    	Globals = ((MyGlobals) getApplicationContext());	
		    	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			    
			    final List<cTaskMember> taskMember = MyProvider.getTaskMemberList(id_task);
		    	
		    	runOnUiThread(new Runnable() {
		               @Override
		               public void run() {
		            	   textViewTask.setText(Html.fromHtml(task));
		            	   textViewDeadline.setText(Html.fromHtml(deadline));
		            	   textViewUser.setText(Html.fromHtml(user));    
		            	   textViewProject.setText(Html.fromHtml(project));
		            	   textViewStatus.setText(Html.fromHtml(status));
		            	   textViewActivity.setText(Html.fromHtml(activity));
		            	   textViewImportancy.setText(Html.fromHtml(importancy)); 
		            	   textViewUrgency.setText(Html.fromHtml(urgency)); 
		            	   
		            	   //Estimated time
		            	  int estimated_time_minutes = Integer.valueOf(estimated_time);
		 				  // calc hours format
		 				  float fhourEst = (estimated_time_minutes / 60);
		 	              float fminuteEst = (((float) estimated_time_minutes / 60) - fhourEst) * 60;
		 	              String hoursEst = "";
		 	              if (estimated_time_minutes > 0)
		 	              {
		 	            	 hoursEst = String.format("%.0f", fhourEst) + "h" + String.format("%.0f", fminuteEst) + "m";
		 	              } else {
		 	            	 hoursEst = String.format("%.0f", fhourEst) + "h";
		 	              }
		 	              
		 	             textViewEstimatedTime.setText(Html.fromHtml(hoursEst));
		 	              
		 	             // Reported time
		 	              if(reported_time != null && !reported_time.isEmpty() && !reported_time.equals("null")){
			 	             int reported_time_minutes = Integer.valueOf(reported_time);
			 	             
			 	             float fhourRep = (reported_time_minutes / 60);
			 	             float fminuteRep = (((float) reported_time_minutes / 60) - fhourRep) * 60;
			 	             String hoursRep = "";
			 	              if (reported_time_minutes > 0)
			 	              {
			 	            	 hoursRep = String.format("%.0f", fhourRep) + "h" + String.format("%.0f", fminuteRep) + "m";
			 	              } else {
			 	            	 hoursRep = String.format("%.0f", fhourRep) + "h";
			 	              }
			 	              
			 	             textViewReportedTime.setText(Html.fromHtml(hoursRep)); 
		 	              }else{
		 	            	 textViewReportedTime.setText(Html.fromHtml(reported_time)); 
		 	              }
		            	   
		 	              // Remaining time
		 	             if(reported_time != null && !reported_time.isEmpty() && !reported_time.equals("null")){
		 	            	 long estimated = Long.valueOf(estimated_time);
		 	            	 long reported = Long.valueOf(reported_time);
		 	            	 long different = estimated - reported;
		 	            	 
		 	            	 float fhour = (different / 60);
			 	             float fminute = (((float) different / 60) - fhour) * 60;
			 	             String hours = "";
			 	              if (fhour != 0 && fminute != 0)
			 	              {
			 	            	 hours = String.format("%.0f", fhour) + "h" + String.format("%.0f", fminute) + "m";
			 	              } else if(fminute != 0 && fhour == 0){
			 	            	 hours = String.format("%.0f", fminute) + "m";
			 	              }
			 	              else {
			 	            	 hours = String.format("%.0f", fhour) + "h" ;
			 	              }
			 	             textViewRemainingTime.setText(hours);
		 	             }else{
		 	            	textViewRemainingTime.setText(hoursEst);
		 	             }

		 	            StringBuilder builder = new StringBuilder();
		            	   for(cTaskMember member : taskMember){	  
		            		   builder.append(member.name + "\n" + "; " );
		            	   }
		            	   textViewMembers.setText(Html.fromHtml(builder.toString()));
		 	             
		            	 textViewDescription.setText(Html.fromHtml(description)); 
		            	 
		            	  String x = user;
		  				  String y = myUserName;
		  				  if(user.equals(myUserName)){
		  					  buttonEditTask.setVisibility(View.VISIBLE);
		  					  buttonDeleteTask.setVisibility(View.VISIBLE);
		  				  }

		               }
		            });
		    	 
		    	
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String result) {
		        // dismiss the dialog after getting all products
		
		    	super.onPostExecute(result);

	            // Dismiss the progress dialog
	            if (pDialog.isShowing())
	                pDialog.dismiss(); 
		    }
		}
		
		
		private class DeleteTask extends AsyncTask<String, String, Boolean> {
			  String myID = "";
			  
			  public DeleteTask(String id) {
			     myID = id;
			  }

			  protected Boolean doInBackground(String... urls) {
				  			  
				  boolean suc = false;
				  
					suc = MyProvider.deleteTask(myID);
				  
				  return suc;
			  }

			  protected void onPostExecute(Boolean result) {
				  
				  if(result == true){
					  Toast.makeText(getApplicationContext(), "Task deleted", Toast.LENGTH_SHORT).show();
					  Globals.setValue("task_update", "task_update");
					  Globals.setValue("edit_task","");
					  finish();
					  
				  }else{
					  Toast.makeText(getApplicationContext(), "Was not deleted task", Toast.LENGTH_SHORT).show();
					  Globals.setValue("task_update", "task_update");
					  Globals.setValue("edit_task","");
					  finish();
				  }
				  
				  
			  }
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		private Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
		
		private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {
			 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
				  
				    mStartYear = year;
		            mStartMonth = month;
		            mStartDay = day; 
		            
				    String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();
		            		
		          startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
			                .append(pad(mStartMonth + 1)).append("/")
			                .append(pad(mStartYear)));
		                
		          LoadReports();
			  }

		};
		
		private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
			 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
				  
				    mEndYear = year;
		            mEndMonth = month;
		            mEndDay = day; 
		            
		            String dayOfWeekEnd = DateFormat.format("EE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();
		            		
		          endDateView.setText(new StringBuilder().append(dayOfWeekEnd).append(" ").append(pad(mEndDay)).append("/")
			                .append(pad(mEndMonth + 1)).append("/")
			                .append(pad(mEndYear)));
		          
		          LoadReports(); 
			  }
		};
		
		public void onStartDayStatsClick(View v) 
		{		
			DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
			Date d = new Date();
			dp1.setTitle(sdf.format(d));
		    dp1.show();
		    
		}
		
		public void onEndDayStatsClick(View v){
			DatePickerDialog dp2 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
			Date d = new Date();
			dp2.setTitle(sdf.format(d));
			dp2.show();
		}
		
		private void LoadReports()
		{

	   	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
		 	  
			Globals.setValue("report_date_start", myDateTime);
			Globals.setValue("report_date_end", myDateTimeEnd);
				
			LoadListReports RefreshList;
			RefreshList = new LoadListReports();
			RefreshList.execute();
			
		}
		
		// set date from mysql datetime
		private String SetDate(String mysqldate)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			StringBuilder setText = null;
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    mYear = c.get(Calendar.YEAR);
			    mMonth = c.get(Calendar.MONTH);
			    mDay = c.get(Calendar.DAY_OF_MONTH);
			    
			    setText = new StringBuilder().append(pad(mYear)).append("/").append(pad(mMonth+1)).append("/").append(pad(mDay));
					
			} catch (ParseException e) {
			    	
			}
			
			return setText.toString();

	    }
		
		// set time from mysql datetime
		private String SetTime(String mysqldate)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			StringBuilder setText = null;
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    mStartMinute = c.get(Calendar.MINUTE);
			    
			    setText = new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute));
					
			} catch (ParseException e) {
			    	
			}
			
			return setText.toString();

	    }
		
		 /**
		 * Background Async Task to Load notes by making HTTP Request
	  	 * */
		class LoadListReports extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();

	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		   	    	
		    	Globals = ((MyGlobals) getApplicationContext());	
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
				String report_date_start = Globals.getValue("report_date_start");
				String report_date_end = Globals.getValue("report_date_end");
				
				Log.d("LOG",String.valueOf(report_date_start));
				Log.d("LOG",String.valueOf(report_date_end));
				
		    	 // load data from provider
			    reportsList = new ArrayList<HashMap<String, String>>();  
		    	
		    	 List<cTimeRecord> report = MyProvider.getTimeRecords(report_date_start, report_date_end, report_user, false, id_task);
		    	 for(cTimeRecord entry : report)
		    	 {
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("pid",  entry.id);
	                map.put("date", SetDate(entry.starttime));
	                map.put("time" , SetTime(entry.starttime) + " - " + SetTime(entry.endtime));
	                map.put("description", entry.description);

					 if(entry.minutes >= 60 || entry.minutes < -60){
						 Integer hours = entry.minutes / 60;

						 map.put("hours", Integer.toString(hours) + " h");

					 }else if(entry.minutes < 60 || entry.minutes > -60){

						 map.put("hours", Integer.toString(entry.minutes) + " min");

					 }



	                // adding HashList to ArrayList
	                reportsList.add(map);
		    	 }
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String file_url) {
		        // dismiss the dialog after getting all products
		
		    	// updating UI from Background Thread
		    	runOnUiThread(new Runnable() {
		            public void run() {
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	ListAdapter adapter = null;
		            	
		            	Log.d("LOG",String.valueOf(reportsList.size()));
		            	
		            	adapter = new SimpleAdapter(
		                		MyTasksDetailActivity.this
		                		, reportsList
		                		, R.layout.activity_list_tasks_detail_list_reports, new String[] {"pid", "date", "time", "hours", "description" },
	                            new int[] { R.id.textViewID, R.id.textViewDate, R.id.textViewTime, R.id.textViewHours, R.id.textViewDescription });

		        			lv.setAdapter(adapter);
		        			setListViewHeightBasedOnChildren(lv);
		        			
		               	 
		            }
		        });    
		    }  
		}
		
		
		 	// for listview in scrollview (without this method, the listview won't scroll)
		   public static void setListViewHeightBasedOnChildren(ListView listView) 
		   {
		       ListAdapter listAdapter = listView.getAdapter();
		       if (listAdapter == null)
		           return;

		       int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		       int totalHeight=0;
		       View view = null;

		       for (int i = 0; i < listAdapter.getCount(); i++) 
		       {
		           view = listAdapter.getView(i, view, listView);

		           if (i == 0)
		               view.setLayoutParams(new LayoutParams(desiredWidth,
		                                         LayoutParams.MATCH_PARENT));

		           view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
		           totalHeight += view.getMeasuredHeight();

		       }

		       LayoutParams params = listView.getLayoutParams();
		       params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

		       listView.setLayoutParams(params);
		       listView.requestLayout();

		   }

}
