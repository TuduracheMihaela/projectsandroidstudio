package com.sharethatdata.digitime;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.sharethatdata.digitime.adapter.HolidaysAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cHolidays;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import androidx.fragment.app.Fragment;

public class HolidaysListFragment extends Fragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;
	
	private ProgressDialog pDialog;
	
	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userArray =  new ArrayList<String>();
	
	ListView lv = null;
	Spinner userSpinner = null;
	private TextView startYearView; 
	
	private int year,month,day;
	
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	private String report_user_id = "";
	private String report_user = "";
    private String error_message = "";
    private boolean userIsInteracting = false;
    private int userSpinnerIndex = -1;
	private boolean selfApprove = false;
    
    private int spinnerPositionAfterRotateScreen = 0;
    boolean setFirstTimePositionSpinner = false;
    
    private Listener mListener = null;
	
	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
		 View rootView = null;
		 
			Globals = ((MyGlobals)getActivity().getApplicationContext());
		 	Globals.backgroundProcess();
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        myUserID = Globals.getValue("idContact"); // id of user logged
	        myUserName = Globals.getValue("nameContact"); // name of user logged
	        MyProvider = new WSDataProvider(myUser, myPass);

		 	MyTimeUtils = new TimeUtils();
		 	MyTimeUtils.setOrientation(getActivity().getApplicationContext(), getActivity());

		 	report_user_id = myUserID;
	        
	        Calendar c = Calendar.getInstance();
		    year = c.get(Calendar.YEAR);
	        
			 String isManager = Globals.getValue("manager");
			 
	 		if (isManager == "yes")
	 		{
	 			 if (savedInstanceState != null) 
			        {
		 				 rootView = inflater.inflate(R.layout.fragment_holidays_list_manager, container, false);
		 				 
		 				Spinner sUsers = (Spinner) rootView.findViewById(R.id.spinnerUser);
						sUsers.setVisibility(View.VISIBLE);
						RelativeLayout userSpinnerLayout = (RelativeLayout) rootView.findViewById(R.id.userSpinnerLayout);
						userSpinnerLayout.setVisibility(View.VISIBLE);

				        	LoadUserList InitUserList;
							
							InitUserList = new LoadUserList();
							InitUserList.execute();
	
				        	int value = savedInstanceState.getInt("spinnerPosition");
				        	spinnerPositionAfterRotateScreen = value;
				        	
				        	String valueYear = savedInstanceState.getString("valueYear");
				        	year = Integer.valueOf(valueYear);
				        	startYearView = (TextView) rootView.findViewById(R.id.dateStartText);
				        	startYearView.setText(new StringBuilder().append(pad(year))); 
				        	
				        	setFirstTimePositionSpinner = true;
			        	

			        }else{
			        	rootView = inflater.inflate(R.layout.fragment_holidays_list_manager, container, false);

		        	  	Spinner sUsers = (Spinner) rootView.findViewById(R.id.spinnerUser);
						sUsers.setVisibility(View.VISIBLE);
					 	RelativeLayout userSpinnerLayout = (RelativeLayout) rootView.findViewById(R.id.userSpinnerLayout);
					 	userSpinnerLayout.setVisibility(View.VISIBLE);
						
							LoadUserList InitUserList;
							
							InitUserList = new LoadUserList();
							InitUserList.execute();
			        }
	 			
	 			
	 		} else {
	 			 rootView = inflater.inflate(R.layout.fragment_holidays_list_employee, container, false);

	 			Spinner sUsers = (Spinner) rootView.findViewById(R.id.spinnerUser);
				sUsers.setVisibility(View.GONE);
				RelativeLayout userSpinnerLayout = (RelativeLayout) rootView.findViewById(R.id.userSpinnerLayout);
				userSpinnerLayout.setVisibility(View.GONE);
	 		}

		    lv = (ListView) rootView.findViewById(R.id.listView);
	        userSpinner = (Spinner) rootView.findViewById(R.id.spinnerUser);
	        
	        startYearView = (TextView) rootView.findViewById(R.id.dateStartText);
	        startYearView.setText(new StringBuilder().append(pad(year)));
		        
			userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

					//if (userIsInteracting) {

						Spinner userSpinner = (Spinner) getView().findViewById(R.id.spinnerUser);
						int myIndex = userSpinner.getSelectedItemPosition();
						report_user_id = userkeyArray.get(myIndex);
						String textUserName = userSpinner.getSelectedItem().toString();

						if(textUserName.equals(myUserName)){
							selfApprove = true;
						}else{
							selfApprove = false;
						}

						userSpinnerIndex = myIndex;
						spinnerPositionAfterRotateScreen = myIndex;

					LoadListHolidays RefreshList;
					RefreshList = new LoadListHolidays(report_user_id);
					RefreshList.execute();
					//}
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) {

				}

			});
		        
		        startYearView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						showStartDatePicker();
					} });
	        
	        LoadListHolidays RefreshList;
			RefreshList = new LoadListHolidays(myUserID);
			RefreshList.execute();
			

	        return rootView;
	    }

	 
	 @Override
	 public void onSaveInstanceState(Bundle savedInstanceState) {
	   super.onSaveInstanceState(savedInstanceState);
	   Globals = ((MyGlobals)getActivity().getApplicationContext());
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
       
	   String isManager = Globals.getValue("manager");
		 
		if (isManager == "yes")
		{
			userSpinner = (Spinner) getView().findViewById(R.id.spinnerUser);
			int value = userSpinner.getSelectedItemPosition();
			spinnerPositionAfterRotateScreen = value;
			
			 startYearView = (TextView) getView().findViewById(R.id.dateStartText);
			 String valueYear = startYearView.getText().toString();
			 
			savedInstanceState.putString("valueYear", valueYear);
			savedInstanceState.putInt("spinnerPosition", userSpinner.getSelectedItemPosition());
		}
	 }
	 
/*	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
	     setHasOptionsMenu(true);

	     Globals = ((MyGlobals)getActivity().getApplicationContext());
         myUser = Globals.getValue("user");
         MyProvider = new WSDataProvider(myUser, myPass); 
	          
	     String isManager = Globals.getValue("manager");
		 
			if (isManager == "yes")
			{
		        if (savedInstanceState != null) 
		        {
		        	LoadUserList InitUserList;
					
					InitUserList = new LoadUserList();
					InitUserList.execute();

		        	int value = savedInstanceState.getInt("spinnerPosition");
		        	spinnerPositionAfterRotateScreen = value;
		        	
		        	setFirstTimePositionSpinner = true;
		        	

		        }
			}
	 }*/
	 
	 @Override
		public void onResume(){
			super.onResume();
			Log.d("TAG", "onResume holidays");
			setHasOptionsMenu(true);  
			new LoadListHolidays(report_user_id).execute();
		}
	 
	 public void myRefresh(){
	  	   Log.d("TAG", "onResume holidays");
	  	 new LoadListHolidays(report_user_id).execute();
	     }
	 
	    public interface Listener {
	        public void onDialogUpdate(HolidaysListFragment dialog);
	    }
	    
	    abstract private class NumberPickerListener implements YearPickerFragment.Listener {

			abstract public void onDialogPositiveClick(YearPickerFragment dialog);

			abstract public void onDialogNegativeClick(YearPickerFragment dialog);
	    }
	    
	    private void showStartDatePicker() {
			  YearPickerFragment date = new YearPickerFragment()
		        		.setActivity(getActivity())
		        		.setTitle("Select year")//getString(R.string.label_tx_delay)
		        		.setMin(2000)
		        		.setMax(2050)
		        		.setValue(year)
		                .setListener(new NumberPickerListener() {
		                	@Override
		                    public void onDialogPositiveClick(YearPickerFragment dialog) {
		                       year = dialog.getValue();
		                       startYearView.setText(Integer.toString(year));      
		                       
		                       LoadListHolidays RefreshList;
	               			   RefreshList = new LoadListHolidays(report_user_id);
	               			   RefreshList.execute();
	               			   
		                       if (mListener != null) {
		                    	   mListener.onDialogUpdate(HolidaysListFragment.this);
		                       }
		                    }
		                	@Override
		                	public void onDialogNegativeClick(YearPickerFragment dialog) {
		                		// pass
		                	}
		                });
			  date.show(getChildFragmentManager(), "YearPickerFragment");
		        	
			  /**
			   * Set Up Current Date Into dialog
			   */
			  /*Bundle args = new Bundle();
			  args.putInt("year", year);
			  date.setArguments(args);*/
			  /**
			   * Set Call back to capture selected date
			   */
			  //date.setCallBack(onStartDate);

			 }

	 @Override
	 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	     // TODO Add your menu entries here
	     super.onCreateOptionsMenu(menu, inflater);
	     
	     inflater.inflate(R.menu.refresh, menu);
	 }

	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     switch (item.getItemId()) {
	     case R.id.action_refresh:
	         new LoadListHolidays(report_user_id).execute();
	         return true;
	     default:
	         break;
	     }

	     return false;
	 }
	 
	 private Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
	 
	 /**
		 * Background Async Task to Load all holidays by making HTTP Request
	  	 * */
		class LoadListHolidays extends AsyncTask<String, String, String> {

		 String user_id;

		 public LoadListHolidays(String user_id){
			 this.user_id = user_id;
		 }
	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	   
	         showProgressDialog(getString(R.string.loading_alert_dialog));
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		   	    	
		    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
				MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			    
			    Calendar c = Calendar.getInstance();
			    month = 1;
			    day = 1;
			    
			    String myDateStart = (new StringBuilder().append(pad(year)).append("-").append(pad(month)).append("-").append(pad(day))).toString();
		        String myDateEnd = (new StringBuilder().append(pad(year + 1)).append("-").append(pad(month)).append("-").append(pad(day))).toString();
				
				System.out.println("myDateStart: " + myDateStart);
				System.out.println("myDateEnd: " + myDateEnd);
				
				System.out.println("myUser: " + myUser);
				
				if (report_user == "") 
				{
					report_user = myUser;
				}
				
				System.out.println("myUser: " + myUser);

		    	 // load data from provider
		    	 itemList = new ArrayList<HashMap<String, String>>();  

					itemList.clear();
		    	
		    	 List<cHolidays> holidays = MyProvider.getHolidaysList(myDateStart, myDateEnd, report_user_id);
		    	 for(cHolidays entry : holidays)
		    	 {
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                //map.put("date", entry.starttime.substring(0, 16));
	                
	                String[] tokenStart = entry.datetime_start.split("\\s");
	                String valueStart = tokenStart[0]+tokenStart[1];
	                String dateStart = tokenStart[0];
	                String timeStart = tokenStart[1];
	                
	                String[] tokenEnd = entry.datetime_end.split("\\s");
	                String valueEnd = tokenEnd[0]+tokenEnd[1];
	                String dateEnd = tokenEnd[0];
	                String timeEnd = tokenEnd[1];
	                
	                map.put("dateS", dateStart);
	                map.put("timeS" , timeStart );
	                
	                map.put("dateE", dateEnd);
	                map.put("timeE" , timeEnd );
	                
	                map.put("id",  String.valueOf(entry.id));
	                map.put("user", entry.user);
	                map.put("hours", String.valueOf(entry.hours));
	                
	                String approvement = "";
	                if(entry.approved.equals("1")){
	                	approvement = "Approved";
       	        	}else if(entry.approved.equals("0")){
       	        		approvement = "In progress";
       	        	}else if(entry.approved.equals("2")){
       	        		approvement = "Disapproved";
       	        	}
	                
	                map.put("approved", approvement);
	                 
	                // adding HashList to ArrayList
	                itemList.add(map);
		    	 }
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String file_url) {
		        // dismiss the dialog after getting all products
		
		    	// updating UI from Background Thread
		    	getActivity().runOnUiThread(new Runnable() {
		            public void run() {
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	ListAdapter adapterEmployee = null;
		            	HolidaysAdapter adapterManager = null;
		            
		            	// total
		            	int total = 0;
		            	int myNum = 0;
		            	for (HashMap<String, String> map : itemList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "hours")
		           	        	{
		           	        		
		           	        		try {
		           	        			myNum = Integer.parseInt(entry.getValue().toString());
		           	        		
		           	        			int days = (int) myNum/24;
		           	        			entry.setValue(String.valueOf(days));
		           	        			
		           	        		} catch(NumberFormatException nfe) {
		           	        			myNum = 0;
		           	        		}
		           	        		total = total + myNum;
		           	        	}
		           	        	
		           	        }

		            	/*adapter = new SimpleAdapter(getActivity(), itemList, R.layout.fragment_sickdays_list_items_employee, 
		            			new String[] { "dateS", "timeS" ,"dateE", "timeE", "hours", "approved" }
		            	, new int[] { R.id.textViewDateStart, R.id.textViewTimeStart, R.id.textViewDateEnd, R.id.textViewTimeEnd,
                				R.id.textViewHours, R.id.textViewApproved });*/
		            	
		            	String isManager = Globals.getValue("manager");
		        		if (isManager == "yes")
		        		{
							//if(selfApprove){
								//adapterEmployee = new SimpleAdapter(getActivity(), itemList, R.layout.fragment_holidays_list_items_employee,
								//new String[] { "dateS", "timeS" ,"dateE", "timeE", "hours", "approved" }
								//, new int[] { R.id.textViewDateStart, R.id.textViewTimeStart, R.id.textViewDateEnd, R.id.textViewTimeEnd,
								//R.id.textViewHours, R.id.textViewApproved });
								//lv.setAdapter(adapterEmployee);
							//}else{
								adapterManager = new HolidaysAdapter(getActivity(), R.layout.fragment_holidays_list_items_manager, itemList, HolidaysListFragment.this, report_user_id);
								lv.setAdapter(adapterManager);
							//}

		        		} else {
		        			adapterEmployee = new SimpleAdapter(getActivity(), itemList, R.layout.fragment_holidays_list_items_employee, 
							new String[] { "dateS", "timeS" ,"dateE", "timeE", "hours", "approved" }
						    , new int[] { R.id.textViewDateStart, R.id.textViewTimeStart, R.id.textViewDateEnd, R.id.textViewTimeEnd,
							R.id.textViewHours, R.id.textViewApproved });
		        			lv.setAdapter(adapterEmployee);
		        		}

		            	
		                // updating listview

		               	 
		            }
		        }); 
		    	
		    	hideProgressDialog();
		    }  
		    
		    
		}
		
		/**
		 * Background Async Task to Load all users by making HTTP Request
	  	 * */
		class LoadUserList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();

	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
			Globals = ((MyGlobals)getActivity().getApplication());
			MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				userList = new ArrayList<HashMap<String, String>>();
				
				// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getMyUsers();
	    	
	    		for(cContact entry : List)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("pid", entry.id);
	                map.put("name", entry.name);
	          
	                // adding HashList to ArrayList
	                userList.add(map);
	             }
				
				report_user = myUser;
			}
	    	
			return "";
	   }

	   /**
	    * After completing background task Dismiss the progress dialog
	    * **/
	   protected void onPostExecute(String file_url) {
	       // dismiss the dialog after getting all products

	   		// updating UI from Background Thread
		   getActivity().runOnUiThread(new Runnable() {
	           public void run() {
	           
	    	    
	           	for (HashMap<String, String> map : userList)
	          	        for (Entry<String, String> entry : map.entrySet())
	          	        {
	          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
	          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
	          	        }
	           	
	           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, userArray);
	      	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      	    	Spinner sUsers = (Spinner) getView().findViewById(R.id.spinnerUser);
	      	    	sUsers.setAdapter(user_adapter);
	    	        
	    	        if(setFirstTimePositionSpinner == false){
	    	        	int spinnerPosition = user_adapter.getPosition(myUserName);
			    		//set the default according to value
			    		sUsers.setSelection(spinnerPosition);

			    		
			    		setFirstTimePositionSpinner = true;
	    	        }else{
	    	        	// set value spinner if change rotation (portrait/landscape)
			    		int x = spinnerPositionAfterRotateScreen;
			    		userSpinner.setSelection(x);
	    	        }
	      	
	      	    	userIsInteracting = false;
	      	    	
	        	   
	           }
	       	});
		   
	 	      
	    	}
		}

		/**
		 * Shows a Progress Dialog 
		 *  
		 * @param msg
		 */
		public void showProgressDialog(String msg) 
		{
			
			// check for existing progressDialog
			if (pDialog == null) {
				// create a progress Dialog
				pDialog = new ProgressDialog(getActivity());

				// remove the ability to hide it by tapping back button
				pDialog.setIndeterminate(true);
				
				pDialog.setCancelable(false);
				pDialog.setCanceledOnTouchOutside(false);
				
				pDialog.setMessage(msg);

			}

			// now display it.
			pDialog.show();		
		}	
		
		
		/**
		 * Hides the Progress Dialog
		 */
		public void hideProgressDialog() {
			
			if (pDialog != null) {
				pDialog.dismiss();
			}
			
			pDialog = null;
		}

}
