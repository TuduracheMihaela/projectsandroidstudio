package com.sharethatdata.digitime.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.sharethatdata.digitime.R;

import android.app.Activity;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TasksAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;

 	 public TasksAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.name = (TextView)view.findViewById(R.id.textViewName);
        holder.status = (TextView)view.findViewById(R.id.textViewStatus);        
        holder.project = (TextView)view.findViewById(R.id.textViewProject);
        holder.deadline = (TextView)view.findViewById(R.id.textViewDeadline);
        holder.remaining = (TextView)view.findViewById(R.id.textViewRemaining);
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

     holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));      
     holder.status.setText(Html.fromHtml(hashmap_Current.get("status")));       
     holder.project.setText(Html.fromHtml(hashmap_Current.get("project")));       
     holder.deadline.setText(Html.fromHtml(hashmap_Current.get("deadline")));
     
     String estimated_time = hashmap_Current.get("estimated_time");
     String reported_time = hashmap_Current.get("reported_time");
     
     if(estimated_time == "null"){
    	 estimated_time = "0";
     }
 
     if(reported_time == "null"){
    	 reported_time = "0";
     }
         
    	 Integer remaining_time = (Integer.valueOf(estimated_time) - Integer.valueOf(reported_time));
         System.out.println("Remaining " + remaining_time);
         
	     if(remaining_time > 60 || remaining_time < -60){
		     	remaining_time = remaining_time / 60;
		     	
		     	holder.remaining.setText(String.valueOf(remaining_time) + " h"); 
			     	
			     	if(remaining_time < 0){
			     		holder.remaining.setTextColor(Color.RED);
			     	}
		     	
		   }else if(remaining_time < 60 || remaining_time > -60){
		     	
		     	holder.remaining.setText(String.valueOf(remaining_time) + " min"); 
			     	
			     	if(remaining_time < 0){
			     		holder.remaining.setTextColor(Color.RED);
			     	}
		   }


	     
     
     
       

    return view; 
}

	final class ViewHolder {
	    public TextView name, status, project, deadline, remaining;  
	}


}
