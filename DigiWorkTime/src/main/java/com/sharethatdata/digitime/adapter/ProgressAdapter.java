package com.sharethatdata.digitime.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digitime.ProgressActivity;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.RecordTimeActivity;
import com.sharethatdata.webservice.datamodel.cProjectProgress;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class ProgressAdapter extends ArrayAdapter<HashMap<String, String>>  {
    private final Context context;
   
    private final ArrayList<HashMap<String, String>> values;
    public final List<cProjectProgress> projectProgress = new ArrayList<cProjectProgress>();
    

    public ProgressAdapter(Context context, ArrayList<HashMap<String, String>> values) {
        super(context, R.layout.listview_item_project_edit, values);
        
        this.context = context;
        this.values = values;
    }

    public void ShowError(String description)
    {
    	new AlertDialog.Builder(this.context)
        .setTitle("Error")
        .setMessage(description)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // do nothing
            	
            }
         })
        .setIcon(android.R.drawable.ic_dialog_alert)
         .show();
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.listview_item_project_edit, parent, false);

        final TextView textID = (TextView) rowView.findViewById(R.id.textViewID);
        final TextView textName = (TextView) rowView.findViewById(R.id.textViewName);
        final EditText editTextProgress = (EditText) rowView.findViewById(R.id.textProgress);
        final EditText editTextHours = (EditText) rowView.findViewById(R.id.textHours);
        
        HashMap<String, String> map = values.get(position);
        for (Entry<String, String> entry : map.entrySet())
	    {
	    	if (entry.getKey() == "pid") textID.setText(entry.getValue());
	    	if (entry.getKey() == "name") textName.setText(entry.getValue());
	    	if (entry.getKey() == "progress") editTextProgress.setText(entry.getValue());
	     	if (entry.getKey() == "hours") editTextHours.setText(entry.getValue());
	    }

        editTextProgress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String progress_value = editTextProgress.getText().toString();
                
                int value = 0;
                try
                {
                    value = Integer.parseInt(progress_value);
                } catch (Exception ex){
                	ShowError("Please enter a numeric value.");
                }
                if (value < 0 || value > 100)
                	ShowError("Please enter a value between 0 and 100.");
                
                String hours_value = editTextHours.getText().toString();
                String id_value = textID.getText().toString();
                String name_value = textName.getText().toString();
                
                // find if exists
                cProjectProgress found = null;
                for(cProjectProgress myProgress : projectProgress) {
                	if (myProgress.id.equals(id_value))
                	{
                		found = myProgress;
                		break;
                	}
                	
                }
                if (found != null)
                {
                	found.progress = progress_value;
                } else {
                	// create
                	found = new cProjectProgress();
                	found.id = id_value;
                	found.name = name_value;
                	found.progress = progress_value;
                	found.hours = hours_value;
                	
                	projectProgress.add(found);
                }
                
                
            }
        });
        
        editTextHours.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String progress_value = editTextProgress.getText().toString();
                String hours_value = editTextHours.getText().toString();
                String id_value = textID.getText().toString();
                String name_value = textName.getText().toString();
                
                // find if exists
                cProjectProgress found = null;
                for(cProjectProgress myProgress : projectProgress) {
                	if (myProgress.id.equals(id_value))
                	{
                		found = myProgress;
                		break;
                	}
                	
                }
                if (found != null)
                {
                	found.hours = hours_value;
                } else {
                	// create
                	found = new cProjectProgress();
                	found.id = id_value;
                	found.name = name_value;
                	found.progress = progress_value;
                	found.hours = hours_value;
                	
                	projectProgress.add(found);
                }
                
                
            }
        });

        return rowView;
    }
}
