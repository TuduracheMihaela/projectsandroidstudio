package com.sharethatdata.digitime.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.projects.MyProjectsDetailActivity;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;

 	 public NotesAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.date = (TextView)view.findViewById(R.id.textViewDate);
        holder.time = (TextView)view.findViewById(R.id.textViewTime);        
        holder.title = (TextView)view.findViewById(R.id.textViewTitle);
        holder.owner = (TextView)view.findViewById(R.id.textViewOwner);
        holder.entry = (TextView)view.findViewById(R.id.textViewEntry);
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

     holder.date.setText(Html.fromHtml(hashmap_Current.get("date")));      
     holder.time.setText(Html.fromHtml(hashmap_Current.get("time")));       
     holder.title.setText(Html.fromHtml(hashmap_Current.get("title")));       
     holder.owner.setText(Html.fromHtml(hashmap_Current.get("owner")));
     holder.entry.setText(Html.fromHtml(hashmap_Current.get("entry")));

    return view; 
}

	final class ViewHolder {
	    public TextView id, date, time, title, owner, entry;  
	}


}
