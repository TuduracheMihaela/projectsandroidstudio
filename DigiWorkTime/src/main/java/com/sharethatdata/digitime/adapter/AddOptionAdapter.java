package com.sharethatdata.digitime.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.WSDataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by miha on 10/13/2017.
 */

public class AddOptionAdapter extends ArrayAdapter<HashMap<String, String>> {

    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    boolean[] checkBoxState;
    int position;

    final ArrayList<String> mSelectedItems = new ArrayList<String>();
    final ArrayList<String> mUnSelectedItems = new ArrayList<String>();

    public AddOptionAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
        checkBoxState=new boolean[adapter.size()];
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder viewHolder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.id = (TextView)view.findViewById(R.id.textViewId);
            viewHolder.name = (TextView)view.findViewById(R.id.textViewName);
            viewHolder.option_check = (CheckBox) view.findViewById(R.id.checkbox);

            //link the cached views to the view
            view.setTag(viewHolder);
        }else{
            viewHolder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);
        this.position = position;

        Log.e("Zdit", hashmap_Current.toString());

        viewHolder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
        viewHolder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
        viewHolder.option_check.setChecked(checkBoxState[position]); //VITAL PART!!! Set the state of the CheckBox using the boolean array

        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "option")
            {
                try {
                    String myRow = entry.getValue().toString();

                    if(myRow.contains("1")){
                       viewHolder.option_check.setChecked(checkBoxState[position] = true);
                    }else{
                        viewHolder.option_check.setChecked(checkBoxState[position] = false);
                    }

                } catch(NumberFormatException nfe) {

                }
            }
        }

        //for managing the state of the boolean
        //array according to the state of the
        //CheckBox
        viewHolder.option_check.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkBoxState[position]=true;
                    mSelectedItems.add(viewHolder.id.getText().toString());
                    mUnSelectedItems.remove(viewHolder.id.getText().toString());
                }else{
                    checkBoxState[position]=false;
                    mSelectedItems.remove(viewHolder.id.getText().toString());
                    mUnSelectedItems.add(viewHolder.id.getText().toString());
                }
            }
        });

        return view;
    }

    final class ViewHolder {
        TextView id;
        TextView name;
        CheckBox option_check;
    }

    public long getItemId(int position){
        HashMap<String, String> hashmap_Current = adapter.get(position);
        String id =  hashmap_Current.get("id");
        return Long.valueOf(id);
    }

    public String getItemName(int position){
        HashMap<String, String> hashmap_Current = adapter.get(position);
        String name =  hashmap_Current.get("name");
        return name;
    }

    public String getItemOption(int position){
        HashMap<String, String> hashmap_Current = adapter.get(position);
        String option =  hashmap_Current.get("option");
        return option;
    }

    public boolean insert(int position, String deletedItem){
        HashMap<String, String> hashmap_Current = new HashMap<String, String>();
        hashmap_Current.put(String.valueOf(position), deletedItem);
        return adapter.add(hashmap_Current);
    }

    public boolean remove(String position){
        return adapter.remove(position);
    }

    public ArrayList getSelectedItems(){
        return mSelectedItems;
    }

    public ArrayList getUnSelectedItems(){
        return mUnSelectedItems;
    }

}
