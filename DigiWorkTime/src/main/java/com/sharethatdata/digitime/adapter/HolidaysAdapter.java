package com.sharethatdata.digitime.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import com.sharethatdata.digitime.HolidaysListFragment;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.ProgressActivity;
import com.sharethatdata.digitime.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HolidaysAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	private HolidaysListFragment fragment;
 	protected ListView mListView;
	ProgressDialog progressDialog;
 	
 	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
    private String report_user = "";
	private String error_message = "";
	private String holidayID = "";
	String holidayNrDays = "";
	String holidayStart = "";
	String holidayEnd = "";

 	 public HolidaysAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter, HolidaysListFragment fragment, String report_user)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        this.fragment=fragment;
			this.report_user = report_user;

	        Globals = ((MyGlobals)context.getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        myUserID = Globals.getValue("idContact"); // id of user logged
			myUserName = Globals.getValue("nameContact"); // name of user logged
	        MyProvider = new WSDataProvider(myUser, myPass);
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textViewID);
        holder.dateStart = (TextView)view.findViewById(R.id.textViewDateStart);
        holder.timeStart = (TextView)view.findViewById(R.id.textViewTimeStart);        
        holder.dateEnd = (TextView)view.findViewById(R.id.textViewDateEnd);
        holder.timeEnd = (TextView)view.findViewById(R.id.textViewTimeEnd);
        holder.hours = (TextView)view.findViewById(R.id.textViewHours);
        holder.approved = (TextView)view.findViewById(R.id.textViewApproved);
        holder.imageApprove = (ImageView)view.findViewById(R.id.buttonApprove);
        holder.imageDisapprove = (ImageView)view.findViewById(R.id.buttonDisapprove);
        
        // Approve = 1
        holder.imageApprove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			holidayID = itemIdStatus;
				holidayNrDays = (String) itemsList.get("hours");
				holidayStart = (String) itemsList.get("dateS");
				holidayEnd = (String) itemsList.get("dateE");

				holder.imageApprove.setVisibility(View.INVISIBLE);
            	
            	holder.isClicked = true;
            	
            	UpdateTaskHoliday updateList;
            	updateList = new UpdateTaskHoliday("1");
            	updateList.execute();
            }
        });
        
        //Disapprove = 2
        holder.imageDisapprove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			holidayID = itemIdStatus;
            	
            	holder.imageDisapprove.setVisibility(View.INVISIBLE);
            	
            	holder.isClicked = true;
            	
            	UpdateTaskHoliday updateList;
            	updateList = new UpdateTaskHoliday("2");
            	updateList.execute();
            }
        });
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.dateStart.setText(Html.fromHtml(hashmap_Current.get("dateS")));      
     holder.timeStart.setText(Html.fromHtml(hashmap_Current.get("timeS")));       
     holder.dateEnd.setText(Html.fromHtml(hashmap_Current.get("dateE")));       
     holder.timeEnd.setText(Html.fromHtml(hashmap_Current.get("timeE")));
     holder.hours.setText(Html.fromHtml(hashmap_Current.get("hours")));
     holder.approved.setText(Html.fromHtml(hashmap_Current.get("approved")));

     holder.imageApprove.setVisibility(View.VISIBLE);
     holder.imageDisapprove.setVisibility(View.VISIBLE);

     for (Entry<String, String> entry : hashmap_Current.entrySet())
        {
        	if (entry.getKey() == "approved")
        	{
        		
        		try {
        			String myRow = entry.getValue().toString();
        			
        			if(myRow.contains("In progress")){
        				// in progress approvement
     	        	System.out.println("in if 0: " + myRow);
     	        	holder.imageApprove.setVisibility(View.VISIBLE);
     	        	holder.imageDisapprove.setVisibility(View.VISIBLE);
        			}
	     	        if(myRow.contains("Approved")){
	     	        		// approved
	     	        	holder.imageApprove.setVisibility(View.INVISIBLE);
	     	        	holder.imageDisapprove.setVisibility(View.VISIBLE);
	     	       	}
	     	        if(myRow.equals("Disapproved")){
	     	        	// disapproved
	     	    	   holder.imageDisapprove.setVisibility(View.INVISIBLE);
	     	    	   holder.imageApprove.setVisibility(View.VISIBLE);
	     	       }
        			
        		} catch(NumberFormatException nfe) {

        		}

        	}
        }

    return view; 
}

	final class ViewHolder {
	    public ImageView imageApprove, imageDisapprove;
	    public TextView id, dateStart, timeStart, dateEnd, timeEnd, hours, approved;
	    protected boolean isClicked;
	    public LinearLayout content;
	}
	
	
	/** AsyncTask update status sick day  */
	private class UpdateTaskHoliday extends AsyncTask<String, String, Boolean>{
		String approvement = "";
		
		public UpdateTaskHoliday(String approvement){
			this.approvement = approvement;
		}
	
	    @Override
	    protected Boolean doInBackground(String... args) {	
			 
		      String id = holidayID;
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;
	
				   suc = MyProvider.updateHoliday(id, approvement, myUserID); // myUserID who approved
	
	    	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	
	    	// updating UI from Background Thread
	    	
	    	context.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(getContext(), getString(R.string.text_error), error_message);	
	            		 fragment.myRefresh();
		    		}
					else
					{
						//Toast.makeText(getActivity(), "Meesage update", Toast.LENGTH_SHORT).show();
						fragment.myRefresh();

						// if approve then create time report with holiday for these days
						if(approvement.equals("1")){
							new RegisterTimeRecord().execute();
						}



					}
 
				}
			});
	
	    }
	}

	private class RegisterTimeRecord extends AsyncTask<String, String, Boolean>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(getContext());
			progressDialog.setMessage( getContext().getString(R.string.loading_alert_dialog));

			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {

			String x = holidayNrDays;
			String y = holidayStart;
			String z = holidayEnd;

			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

			String[] dateS = holidayStart.split("-");
			String yearS = dateS[0];
			String monthS = dateS[1];
			String dayS = dateS[2];
			String[] dateE = holidayEnd.split("-");
			String yearE = dateE[0];
			String monthE = dateE[1];
			String dayE = dateE[2];

			final Calendar calendarS = Calendar.getInstance();
			final Calendar calendarE = Calendar.getInstance();

			calendarS.clear();
			calendarS.set(Integer.valueOf(yearS), Integer.valueOf(monthS), Integer.valueOf(dayS));
			calendarE.clear();
			calendarE.set(Integer.valueOf(yearE), Integer.valueOf(monthE), Integer.valueOf(dayE));

			long nrDays=0;
			try {
				Date date1 = myFormat.parse(holidayStart);
				Date date2 = myFormat.parse(holidayEnd);
				long diff = date2.getTime() - date1.getTime();
				nrDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
			} catch (ParseException e) {
				e.printStackTrace();
			}

			// set calendar
			Calendar c = Calendar.getInstance();
			c.set(Calendar.YEAR, Integer.valueOf(yearS));
			c.set(Calendar.MONTH, Integer.valueOf(monthS) - 1);
			c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dayS));

			boolean suc = true;
			for(int i=1; i<=nrDays; i++){

				int mStartYear = c.get(Calendar.YEAR);
				int mStartMonth = c.get(Calendar.MONTH);
				int mStartDay = c.get(Calendar.DAY_OF_MONTH);

				// check if it is a Saturday or Sunday
				if ((Calendar.SATURDAY != c.get(Calendar.DAY_OF_WEEK))
						&&(Calendar.SUNDAY != c.get(Calendar.DAY_OF_WEEK))) {
					String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
					suc = MyProvider.createTimeRecord(myDateTime, "09:00", "17:00", "", "", "", "", "", "holiday", "holiday", report_user, "");
				}

				if(suc){
					c.add(Calendar.DATE,1);
				}else{
					break;
				}
			}

			return suc;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if(result){
				Toast.makeText(getContext(), getContext().getString(R.string.toast_time_report_approved), Toast.LENGTH_SHORT).show();
			}
			progressDialog.dismiss();
		}
	}

	private Object pad(int mMinute2) {
		if (mMinute2 >= 10)
			return String.valueOf(mMinute2);
		else
			return "0" + String.valueOf(mMinute2);
	}

}
