package com.sharethatdata.digitime.adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.projects.MyProjectsListActivity;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GridViewAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
	ArrayList<HashMap<String, String>> arraylist;
 	private final Activity context;
 	private final int layoutResourceId;
 	protected ListView mListView;
 	//public MyImageLoader loadMyImg;
 	
 	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	private String myUser = "";
	private String myPass = "";
	private String error_message = "";
	private String projectID = "";

 	 public GridViewAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        
	        arraylist = new ArrayList<HashMap<String,String>>();
            arraylist.addAll(adapter);
	        
	        Globals = ((MyGlobals)context.getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        
	       // loadMyImg = new MyImageLoader(context.getApplicationContext());
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.grid_item_id);
        holder.titleTextView = (TextView)view.findViewById(R.id.grid_item_title);
        holder.imageView = (ImageView)view.findViewById(R.id.grid_item_image);   
        holder.layout = (LinearLayout) view.findViewById(R.id.linearLayout);
        
        holder.titleTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			projectID = itemIdStatus;

            }
        });
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.titleTextView.setText(Html.fromHtml(hashmap_Current.get("name")));  
     
     
     //loadMyImg.DisplayImage(hashmap_Current.get("image_string"), holder.imageView); // image from database
     
     Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image_id"));
     
     // for phone screen
     if(context.getResources().getDisplayMetrics().widthPixels>context.getResources().getDisplayMetrics().heightPixels) {
    	 // Landscape mode
    	 
    	 if (d != null)
   	  {
   		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
   		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
   		  holder.imageView.setImageBitmap(scaled);
   		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(270, 270);
   		  lp.setMargins(40, 20, 15, 30);
   		  holder.imageView.setLayoutParams(lp);
   		  				  
   	  }else{
   		  holder.imageView.setImageResource(R.drawable.ic_launcher); 
   		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
   		  lp.setMargins(25, 10, 10, 10);
   		  holder.imageView.setLayoutParams(lp);
   	  }
    	 
     }else{
    	 // Portrait mode
    	 
    	 if (d != null)
   	  {
   		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
   		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
   		  holder.imageView.setImageBitmap(scaled);
   		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(270, 270);
   		  lp.setMargins(25, 20, 15, 30);
   		  holder.imageView.setLayoutParams(lp);
   		  				  
   	  }else{
   		  holder.imageView.setImageResource(R.drawable.ic_launcher); 
   		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
   		  lp.setMargins(10, 10, 10, 10);
   		  holder.imageView.setLayoutParams(lp);
   	  }
     }
     
     
     // tablet screen
     boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
     if (tabletSize) {
         // is tablet
    	 if(context.getResources().getDisplayMetrics().widthPixels>context.getResources().getDisplayMetrics().heightPixels) {
    		 // Landscape mode
        	 if (d != null)
       	  {
        		 // image from database
       		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
       		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
       		  holder.imageView.setImageBitmap(scaled);
       		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(270, 270);
       		  lp.setMargins(40, 20, 15, 30);
       		  holder.imageView.setLayoutParams(lp);
       		  				  
       	  }else{
       		  // image from local
       		  holder.imageView.setImageResource(R.drawable.ic_launcher); 
       		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
       		  lp.setMargins(25, 10, 10, 10);
       		  holder.imageView.setLayoutParams(lp);
       	  }
        	 
         }else{
        	 // Portrait mode
        	 
        	 if (d != null)
       	  {
        		// image from database
       		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
       		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
       		  holder.imageView.setImageBitmap(scaled);
       		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(270, 270);
       		  lp.setMargins(55, 25, 15, 30);
       		  holder.imageView.setLayoutParams(lp);
       		  				  
       	  }else{
       		  // image from local
       		  holder.imageView.setImageResource(R.drawable.ic_launcher); 
       		  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
       		  lp.setMargins(40, 10, 10, 10);
       		  holder.imageView.setLayoutParams(lp);
       	  }
    	 }
    	 
     } else {
         // is not tablet
    	 
     }
	  


    return view; 
}

	final class ViewHolder {
		TextView id;
		TextView titleTextView;
        ImageView imageView;
        LinearLayout layout;
	}
	
	 private Bitmap ConvertByteArrayToBitmap(String b)
		{
		 Bitmap decodedByte = null;
		 	if(b.isEmpty() || b.contentEquals(""))
		 	{
		 		decodedByte = null;
		 	}else{
				byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
				decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		 	}
			
			return decodedByte;		
		}

		
}
