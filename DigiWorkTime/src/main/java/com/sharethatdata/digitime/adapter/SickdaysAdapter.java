package com.sharethatdata.digitime.adapter;

import android.app.Activity;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.SickdaysListFragment;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class SickdaysAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	private SickdaysListFragment fragment;
 	protected ListView mListView;
 	
 	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	private String myUser = "";
	private String myPass = "";
	private String myUserName = "";
    private String myUserID = "";
	private String error_message = "";
	private String sickdayID = "";

 	 public SickdaysAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter, SickdaysListFragment fragment)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        this.fragment = fragment;
	        
	        Globals = ((MyGlobals)context.getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        myUserID = Globals.getValue("idContact"); // id of user logged
			myUserName = Globals.getValue("nameContact"); // name of user logged
	        MyProvider = new WSDataProvider(myUser, myPass);
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textViewID);
        holder.dateStart = (TextView)view.findViewById(R.id.textViewDateStart);
        holder.timeStart = (TextView)view.findViewById(R.id.textViewTimeStart);        
        holder.dateEnd = (TextView)view.findViewById(R.id.textViewDateEnd);
        holder.timeEnd = (TextView)view.findViewById(R.id.textViewTimeEnd);
        holder.hours = (TextView)view.findViewById(R.id.textViewHours);
        holder.approved = (TextView)view.findViewById(R.id.textViewApproved);
        holder.description = (TextView)view.findViewById(R.id.textViewDescription);
        holder.imageApprove = (ImageView)view.findViewById(R.id.buttonApprove);
        holder.imageDisapprove = (ImageView)view.findViewById(R.id.buttonDisapprove);
        
     // Approve = 1
        holder.imageApprove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			sickdayID = itemIdStatus;
            	
            	holder.imageApprove.setVisibility(View.INVISIBLE);
            	
            	holder.isClicked = true;
            	
            	UpdateTaskSickday updateList;
            	updateList = new UpdateTaskSickday("1");
            	updateList.execute();
            }
        });
        
      //Disapprove = 2
        holder.imageDisapprove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			sickdayID = itemIdStatus;
            	
            	holder.imageDisapprove.setVisibility(View.INVISIBLE);
            	
            	holder.isClicked = true;
            	
            	UpdateTaskSickday updateList;
            	updateList = new UpdateTaskSickday("2");
            	updateList.execute();
            }
        });
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.dateStart.setText(Html.fromHtml(hashmap_Current.get("dateS")));      
     holder.timeStart.setText(Html.fromHtml(hashmap_Current.get("timeS")));       
     holder.dateEnd.setText(Html.fromHtml(hashmap_Current.get("dateE")));       
     holder.timeEnd.setText(Html.fromHtml(hashmap_Current.get("timeE")));
     holder.hours.setText(Html.fromHtml(hashmap_Current.get("hours")));
     holder.approved.setText(Html.fromHtml(hashmap_Current.get("approved")));
     holder.description.setText(Html.fromHtml(hashmap_Current.get("description")));
     
     holder.imageApprove.setVisibility(View.VISIBLE);
     holder.imageDisapprove.setVisibility(View.VISIBLE);

     for (Entry<String, String> entry : hashmap_Current.entrySet())
     {
     	if (entry.getKey() == "approved")
     	{
     		
     		try {
     			String myRow = entry.getValue().toString();
     			
     			if(myRow.contains("In progress")){
     				// in progress approvement
  	        	System.out.println("in if 0: " + myRow);
  	        	holder.imageApprove.setVisibility(View.VISIBLE);
  	        	holder.imageDisapprove.setVisibility(View.VISIBLE);
     			}
	     	        if(myRow.contains("Approved")){
	     	        		// approved
	     	        	holder.imageApprove.setVisibility(View.INVISIBLE);
	     	        	holder.imageDisapprove.setVisibility(View.VISIBLE);
	     	       	}
	     	        if(myRow.equals("Disapproved")){
	     	        	// disapproved
	     	    	   holder.imageDisapprove.setVisibility(View.INVISIBLE);
	     	    	   holder.imageApprove.setVisibility(View.VISIBLE);
	     	       }
     			
     		} catch(NumberFormatException nfe) {

     		}

     	}
     }

    return view; 
}

	final class ViewHolder {
	    public ImageView imageApprove, imageDisapprove;
	    public TextView id, dateStart, timeStart, dateEnd, timeEnd, hours, approved, description;
	    protected boolean isClicked;
	    public LinearLayout content;
	}
	
	
	/** AsyncTask update status sick day  */
	private class UpdateTaskSickday extends AsyncTask<String, String, Boolean>{
		String approvement = "";
		
		public UpdateTaskSickday(String approvement){
			this.approvement = approvement;
		}
	
	    @Override
	    protected Boolean doInBackground(String... args) {	
			 
		      String id = sickdayID;
		      
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  boolean suc = false;
	
				   suc = MyProvider.updateSickday(id, approvement, myUserID);
	
	    	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	
	    	// updating UI from Background Thread
	    	
	    	context.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(getContext(), getString(R.string.text_error), error_message);	 
	            		 fragment.myRefresh();
	            		 
		    		}
					else
					{
						//Toast.makeText(getActivity(), "Meesage update", Toast.LENGTH_SHORT).show();
						fragment.myRefresh();  
					}
 
				}
			});
	
	    }
	}

}
