package com.sharethatdata.digitime;

import com.sharethatdata.digitime.HolidaysListFragment;
import com.sharethatdata.digitime.HolidaysRequestFragment;


import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SickdaysTabsPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
 
    public SickdaysTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
 
    /*@Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Holidays List fragment activity
            return new SickdaysListFragment();
        case 1:
            // Holiday Request fragment activity
            return new SickdaysRequestFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }*/
 
}
