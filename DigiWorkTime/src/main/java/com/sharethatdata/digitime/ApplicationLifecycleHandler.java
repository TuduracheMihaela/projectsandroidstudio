package com.sharethatdata.digitime;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by tudur on 12-Jul-20.
 */

public class ApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private static final String TAG = ApplicationLifecycleHandler.class.getSimpleName();
    private static boolean isInBackground = false;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss aa");
    Date date1, date2;
    long time;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {

        if(isInBackground){
            Log.d(TAG, "app went to foreground");
            isInBackground = false;

            Date systemDate = Calendar.getInstance().getTime();
            String secondDate = simpleDateFormat.format(systemDate);
            try {
                date2 = simpleDateFormat.parse(secondDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millis = date2.getTime() - date1.getTime();
            int Hours = (int) (millis/(1000 * 60 * 60));
            int Mins = (int) (millis/(1000*60)) % 60;
            long Secs = (int) (millis / 1000) % 60;

            String diff = Hours + ":" + Mins + ":" + Secs; // updated value every1 second
            Log.d("diff", diff);

            if(Mins > 10 ) // 0:11:57
            {
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
            }
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override
    public void onLowMemory() {
    }

    @Override
    public void onTrimMemory(int i) {
        if(i == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
            Log.d(TAG, "app went to background");
            isInBackground = true;

            Date systemDate = Calendar.getInstance().getTime();
            String firstDate = simpleDateFormat.format(systemDate);
            try {
                date1 = simpleDateFormat.parse(firstDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
