package com.sharethatdata.digitime.notify.timerecord;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.sharethatdata.digitime.LoginActivity;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cTimeRecord;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

/**
 * Created by tudur on 29-Aug-19.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobServiceYesterday extends JobService {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private PendingIntent pendingIntent;

    private String user = "";
    private String pass = "";
    private String myUser = "";
    private String myPass = "";

    // Check Time Record Hours
    ArrayList<HashMap<String, String>> itemList;

    private int mStartHour,mStartMinute;
    private int mEndHour,mEndMinute;
    private int mStartYear,mStartMonth,mStartDay;

    private String report_user = "";

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    private NotificationManager mManager;

    String CHANNEL_ID_31 = "my_channel_031";// The id of the channel.
    String CHANNEL_ID_32 = "my_channel_032";// The id of the channel.

    boolean monday = false;

    @Override
    public boolean onStartJob(JobParameters params) {
        //Intent service = new Intent(getApplicationContext(), MyServiceYesterday.class);
        //getApplicationContext().startService(service);
        //Util.scheduleJob(getApplicationContext()); // reschedule the job
        //jobFinished(params, false);

        // Initializing
        Globals = ((MyGlobals)getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyTimeUtils = new TimeUtils();

        // set calendar
        Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);
        mEndHour = c.get(Calendar.HOUR_OF_DAY);
        mEndMinute = c.get(Calendar.MINUTE);

        if(myUser == ""){
            SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
            user = prefUser.getString("USER", "");
            pass = prefUser.getString("PASS", "");

            myUser = user;
            myPass = pass;

            Log.d("TAG", "user == null: " + user);

            MyProvider = new WSDataProvider(user, pass);
        }else{
            SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
            editor.putString("USER", String.valueOf(myUser));
            editor.putString("PASS", String.valueOf(myPass));
            editor.commit();

            Log.d("TAG", "myUser != null: " + myUser);

            MyProvider = new WSDataProvider(myUser, myPass);
        }


        final Calendar date1 = Calendar.getInstance();

        date1.clear();
        date1.set(mStartYear, mStartMonth, mStartDay);


        if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
                &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))
                &&(Calendar.MONDAY != date1.get(Calendar.DAY_OF_WEEK))) {
            // here is week work
            LoadHour HourList;
            HourList = new LoadHour();
            HourList.execute();
        }else if((Calendar.MONDAY == date1.get(Calendar.DAY_OF_WEEK))){
            //monday = true;
            LoadHourForMonday HourList;
            HourList = new LoadHourForMonday();
            HourList.execute();
        }else {
            // here is weekend
            try {
                // AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                //     manager.cancel(pendingIntent);
                System.out.println("Alarm canceled ");
            } catch (Exception e) {
                System.out.println("Error when cancelling: "+e.toString());
            }

        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // Reminder

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadHour extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        @RequiresApi(api = Build.VERSION_CODES.N)
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);

            Globals = ((MyGlobals)getApplication());

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();

            String report_date_end = myDateTimeEnd;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = report_date_end;
            Date date1 = null;
            try {
                date1 = sdf.parse(strDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            calendar.add(Calendar.DATE, -1);
            Date yesterday = calendar.getTime();

            String dateYesterday = sdf.format(yesterday);


            if (report_user == "")
            {
                report_user = myUser;
            }

            // load data from provider
            itemList = new ArrayList<HashMap<String, String>>();

            List<cTimeRecord> timeRecords = MyProvider.getTimeRecords(dateYesterday, report_date_end, report_user, false, "0");
            for(cTimeRecord entry : timeRecords)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                //map.put("date", entry.starttime.substring(0, 16));

                String[] tokens = entry.starttime.split("\\s");
                String values = tokens[0]+tokens[1];
                String date = tokens[0];
                String time = tokens[1];

                System.out.println("Values: " + values);

                map.put("date", date);
                map.put("time" , time );

                map.put("hours", Integer.toString(entry.minutes));

                // adding HashList to ArrayList
                itemList.add(map);
            }

            System.out.println("Date: " + dateYesterday + " " + report_date_end);

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // total
            int total = 0;
            int myNum = 0;
            for (HashMap<String, String> map : itemList)
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (entry.getKey() == "hours") {

                        try {
                            myNum = Integer.parseInt(entry.getValue().toString());

                            entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum));

                        } catch (NumberFormatException nfe) {
                            myNum = 0;
                        }
                        total = total + myNum;
                    }
                }

            HashMap<String, String> hmap = new HashMap<String, String>();

            hmap.put("hours", MyTimeUtils.getFormattedHourMinutes(total));
            hmap.put("date", "");
            hmap.put("time", "");
            hmap.put("name", "total");

            itemList.add(hmap);

            float totalFormatted = (float) total / 60;

            // time in morning
            SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
            String startDay = sharedPrefs.getString("prefWorkDayStart", "");
            String endDay = sharedPrefs.getString("prefWorkDayEnd", "");
            boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);

            System.out.println("start and end time from settings: " + startDay + " " + endDay);

            int currentHour, currentMin = 0;

            Calendar currentTime = Calendar.getInstance();
            currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            currentMin = currentTime.get(Calendar.MINUTE);
            currentTime.get(Calendar.SECOND);
            currentTime.get(Calendar.AM_PM);

            StringBuilder stringCurrentTime = new StringBuilder().append(pad(currentHour)).append(":").append(pad(currentMin));

            System.out.println("Curent time: " + stringCurrentTime);

            System.out.println("Curent time in milliseconds: " + System.currentTimeMillis());

            convertSecondsToHMmSs(System.currentTimeMillis());

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm +a");

            Date resultdate = new Date(System.currentTimeMillis());
            System.out.println(sdf.format(resultdate));

            System.out.println("------------------------------------------");



            // First Notification in morning
            //if(stringCurrentTime.equals(startDay)){

            // if notification check box checked = true => set reminder
            if (blNotifications){
                Log.d("notify", "Notification on");

                if(isNetworkAvailable()){
                    Log.d("internet", "internet on");

                    // if 8 hours was not recorded yesterday
                    if(totalFormatted < 8)
                    {
                        //Toast.makeText(getApplicationContext(), "Hours registered yesterday: " + String.valueOf(totalFormatted), Toast.LENGTH_SHORT).show();

                        // incorrect, setting the time of alarm at current, so will fire right after.

                        String hourS = null;
                        String minS = null;

                        if(!startDay.isEmpty()){
                            String[] tokens = startDay.split(":");
                            hourS = tokens[0];
                            minS = tokens[1];
                        }else{
                            hourS = "09";
                            minS = "00";
                        }

                        Calendar calendar = Calendar.getInstance();

                        calendar.setTimeInMillis(System.currentTimeMillis());

                        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourS));
                        calendar.set(Calendar.MINUTE, Integer.parseInt(minS));
                        //calendar.set(Calendar.HOUR_OF_DAY, 11);
                        //calendar.set(Calendar.MINUTE, 58);
                        calendar.set(Calendar.SECOND, 00);
                        //calendar.set(Calendar.AM_PM, Calendar.PM);

                        System.out.println("Time morning set: " + hourS + ":" + minS);

                        //Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
                        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
                        Intent myIntent = new Intent(getApplicationContext(), MyReceiverYesterday.class);
                        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, PendingIntent.FLAG_ONE_SHOT);

                        PendingIntent pendingIntent = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                            pendingIntent = PendingIntent.getActivity
                                    (getApplicationContext(), 0, myIntent, PendingIntent.FLAG_MUTABLE);
                        }
                        else
                        {
                            pendingIntent = PendingIntent.getActivity
                                    (getApplicationContext(), 0, myIntent, PendingIntent.FLAG_ONE_SHOT);
                        }

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            CharSequence name = "my_channel_31";
                            String description_channel = "This is my channel 31";
                            int importance = NotificationManager.IMPORTANCE_HIGH;
                            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_31, name, importance);
                            mChannel.setDescription(description_channel);
                            mChannel.enableLights(true);
                            mChannel.setLightColor(Color.RED);
                            mChannel.enableVibration(true);
                            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                            mChannel.setShowBadge(false);
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.createNotificationChannel(mChannel);
                        }

                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(MyJobServiceYesterday.this, CHANNEL_ID_31)
                                        .setSmallIcon(R.drawable.ic_launcher)
                                        .setContentTitle("DigiWorkTime")
                                        .setContentText("Please enter your time report for yesterday.")
                                        .setTicker("Notification!")
                                        .setWhen(System.currentTimeMillis())
                                        .setContentIntent(pendingIntent)
                                        .setAutoCancel(true)
                                        .setVibrate(new long[] {0,100,0,100})
                                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);



                        // Sets an ID for the notification
                        int mNotificationId = 2;
                        // Gets an instance of the NotificationManager service
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.notify(mNotificationId, mBuilder.build());

                                                  try{Util.cancelYesterday(MyJobServiceYesterday.this);}catch (Exception e){e.printStackTrace();}

                    }

                }else {
                    Log.d("internet", "internet off");
                }

                // if notification check box checked = true => cancel reminder
            }else{
                Log.d("notify", "Notification off");
                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                try{manager.cancel(pendingIntent); Util.cancelYesterday(MyJobServiceYesterday.this);}catch (Exception e){e.printStackTrace();}

            }
        }
    }



    /////////////////////////////////////////////////////////////////////////

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadHourForMonday extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        @RequiresApi(api = Build.VERSION_CODES.N)
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);

            Globals = ((MyGlobals)getApplication());

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String myDateTimeEndSunday = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay-1))).toString();
            String myDateTimeEndMonday = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();

            String report_date_end_sunday = myDateTimeEndSunday;
            String report_date_end_monday = myDateTimeEndMonday;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = report_date_end_monday;
            Date date1 = null;
            try {
                date1 = sdf.parse(strDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            calendar.add(Calendar.DATE, -3);
            Date friday = calendar.getTime();

            String dateFriday = sdf.format(friday);


            if (report_user == "")
            {
                report_user = myUser;
            }

            // load data from provider
            itemList = new ArrayList<HashMap<String, String>>();

            List<cTimeRecord> timeRecords = MyProvider.getTimeRecords(dateFriday, report_date_end_sunday, report_user, false, "0");
            for(cTimeRecord entry : timeRecords)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                //map.put("date", entry.starttime.substring(0, 16));

                String[] tokens = entry.starttime.split("\\s");
                String values = tokens[0]+tokens[1];
                String date = tokens[0];
                String time = tokens[1];

                System.out.println("Values: " + values);

                map.put("date", date);
                map.put("time" , time );

                map.put("hours", Integer.toString(entry.minutes));

                // adding HashList to ArrayList
                itemList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // total
            int total = 0;
            int myNum = 0;
            for (HashMap<String, String> map : itemList)
                for (Map.Entry<String, String> entry : map.entrySet())
                {
                    if (entry.getKey() == "hours")
                    {

                        try {
                            myNum = Integer.parseInt(entry.getValue().toString());

                            entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum));

                        } catch(NumberFormatException nfe) {
                            myNum = 0;
                        }
                        total = total + myNum;
                    }
                }

            HashMap<String, String> hmap = new HashMap<String, String>();

            hmap.put("hours", MyTimeUtils.getFormattedHourMinutes(total) );
            hmap.put("date", "");
            hmap.put("time", "");
            hmap.put("name", "total");

            itemList.add(hmap);

            float totalFormatted = (float)total / 60;

            // time in morning
            SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
            String startDay = sharedPrefs.getString("prefWorkDayStart", "");
            String endDay = sharedPrefs.getString("prefWorkDayEnd", "");
            boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);

            System.out.println("start and end time from settings: " + startDay + " " + endDay);

            int currentHour, currentMin = 0;

            Calendar currentTime = Calendar.getInstance();
            currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            currentMin = currentTime.get(Calendar.MINUTE);
            currentTime.get(Calendar.SECOND);
            currentTime.get(Calendar.AM_PM);

            StringBuilder stringCurrentTime = new StringBuilder().append(pad(currentHour)).append(":").append(pad(currentMin));

            System.out.println("Curent time: " + stringCurrentTime);

            System.out.println("Curent time in milliseconds: " + System.currentTimeMillis() );

            convertSecondsToHMmSs(System.currentTimeMillis());

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm +a");

            Date resultdate = new Date(System.currentTimeMillis());
            System.out.println(sdf.format(resultdate));

            System.out.println("------------------------------------------");



            // First Notification in morning
            //if(stringCurrentTime.equals(startDay)){

            // if notification check box checked = true => set reminder
            if (blNotifications){
                Log.d("notify", "Notification on");

                // if 8 hours was not recorded yesterday
                if(totalFormatted < 8)
                {
                    //Toast.makeText(getApplicationContext(), "Hours registered Friday: " + String.valueOf(totalFormatted), Toast.LENGTH_SHORT).show();

                    // incorrect, setting the time of alarm at current, so will fire right after.

                    String hourS = null;
                    String minS = null;

                    if(!startDay.isEmpty()){
                        String[] tokens = startDay.split(":");
                        hourS = tokens[0];
                        minS = tokens[1];
                    }else{
                        hourS = "09";
                        minS = "00";
                    }

                    Calendar calendar = Calendar.getInstance();

                    calendar.setTimeInMillis(System.currentTimeMillis());

                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourS));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(minS));
                    //calendar.set(Calendar.HOUR_OF_DAY, 11);
                    //calendar.set(Calendar.MINUTE, 58);
                    calendar.set(Calendar.SECOND, 00);
                    //calendar.set(Calendar.AM_PM, Calendar.PM);

                    System.out.println("Time morning set: " + hourS + ":" + minS);

                    //Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
                    Intent myIntent = new Intent(getApplicationContext(), MyReceiverYesterday.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, PendingIntent.FLAG_ONE_SHOT);

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        CharSequence name = "my_channel_32";
                        String description_channel = "This is my channel 32";
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_32, name, importance);
                        mChannel.setDescription(description_channel);
                        mChannel.enableLights(true);
                        mChannel.setLightColor(Color.RED);
                        mChannel.enableVibration(true);
                        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                        mChannel.setShowBadge(false);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.createNotificationChannel(mChannel);
                    }

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(MyJobServiceYesterday.this, CHANNEL_ID_32)
                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setContentTitle("DigiWorkTime")
                                    .setContentText("Please enter your time report for Friday.")
                                    .setTicker("Notification!")
                                    .setWhen(System.currentTimeMillis())
                                    .setContentIntent(pendingIntent)
                                    .setAutoCancel(true)
                                    .setVibrate(new long[] {0,100,0,100})
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);



                    // Sets an ID for the notification
                    int mNotificationId = 002;
                    // Gets an instance of the NotificationManager service
                    NotificationManager mNotifyMgr =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    // Builds the notification and issues it.
                    mNotifyMgr.notify(mNotificationId, mBuilder.build());

                    try{Util.cancelYesterday(MyJobServiceYesterday.this);}catch (Exception e){e.printStackTrace();}

                }

                // if notification check box checked = true => cancel reminder
            }else{
                Log.d("notify", "Notification off");
                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                try{manager.cancel(pendingIntent); Util.cancelYesterday(MyJobServiceYesterday.this);}catch (Exception e){e.printStackTrace();}
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String convertSecondsToHMmSs(long millis) {

        String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS +a";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);

        //String formatTime = String.format("%d:%02d:%02d", h,m,s);
        System.out.println("............. " + formatter.format(calendar.getTime()));

        return formatter.format(calendar.getTime());
    }
}
