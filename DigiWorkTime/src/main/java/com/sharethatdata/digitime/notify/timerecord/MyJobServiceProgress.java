package com.sharethatdata.digitime.notify.timerecord;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.sharethatdata.digitime.LoginActivity;
import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.TimeUtils;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cProjectProgress;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

/**
 * Created by tudur on 27-Aug-19.
 */

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobServiceProgress extends JobService {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private PendingIntent pendingIntent;

    private String user = "";
    private String pass = "";
    private String myUserID = "";
    private String myUser = "";
    private String myPass = "";

    // Check Progress Hours
    ArrayList<HashMap<String, String>> progressList;

    private int mStartHour,mStartMinute;
    private int mEndHour,mEndMinute;
    private int mStartYear,mStartMonth,mStartDay;

    private String report_user = "";

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    private NotificationManager mManager;

    String CHANNEL_ID = "my_channel_04";// The id of the channel.

    @Override
    public boolean onStartJob(JobParameters params) {
        //Intent service = new Intent(getApplicationContext(), MyServiceProgress.class);
        //getApplicationContext().startService(service);
        //Util.scheduleJob(getApplicationContext()); // reschedule the job
        //jobFinished(params, false);

        // Initializing
        Globals = ((MyGlobals)getApplicationContext());
        myUserID = Globals.getValue("idContact");
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyTimeUtils = new TimeUtils();

        // set calendar
        Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);
        mEndHour = c.get(Calendar.HOUR_OF_DAY);
        mEndMinute = c.get(Calendar.MINUTE);

        if(myUser == ""){
            SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
            user = prefUser.getString("USER", "");
            pass = prefUser.getString("PASS", "");

            myUser = user;
            myPass = pass;

            Log.d("TAG", "user == null: " + user);
            Log.d("TAG", "pass == null: " + pass);

            MyProvider = new WSDataProvider(user, pass);

            SharedPreferences prefUserID = getSharedPreferences("PREF_CONTACT", 0);
            myUserID = prefUser.getString("idContact", "");
        }else{
            SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
            editor.putString("USER", String.valueOf(myUser));
            editor.putString("PASS", String.valueOf(myPass));
            editor.commit();

            Log.d("TAG", "myUser != null: " + myUser);

            MyProvider = new WSDataProvider(myUser, myPass);
        }


        final Calendar date1 = Calendar.getInstance();

        date1.clear();
        date1.set(mStartYear, mStartMonth, mStartDay);

             /* Notification for progress report */

        if ((Calendar.FRIDAY == date1.get(Calendar.DAY_OF_WEEK)))
        {
            LoadProgress HoursProgress;
            HoursProgress = new LoadProgress();
            HoursProgress.execute();
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    ///////////////////////////////////////////////////////////////

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadProgress extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);

            Globals = ((MyGlobals)getApplication());

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();

            String report_date_end = myDateTimeEnd;

            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
            String strDate = report_date_end;
            Date date1 = null;
            try {
                date1 = sdf.parse(strDate);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            calendar.add(Calendar.DATE, -1);
            Date yesterday = calendar.getTime();

            String dateYesterday = sdf.format(yesterday);

            // load data from provider
            progressList = new ArrayList<HashMap<String, String>>();

            List<cProjectProgress> progress = MyProvider.getProjectProgress(myUserID, report_date_end);
            for(cProjectProgress entry : progress)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hours", entry.hours);
                progressList.add(map);
            }

            System.out.println("progressList size: " + progressList);
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // time in morning
            SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
            boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);

            // if notification check box checked = true => set reminder
            if (blNotifications){
                Log.d("notify", "Notification on progress");

                // if progressList empty was not recorded progress
                if(progressList.size() == 0 || progressList.isEmpty() || progressList == null)
                {
                    //Toast.makeText(getApplicationContext(), "progress report: empty", Toast.LENGTH_SHORT).show();


                    //Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    Intent myIntent = new Intent(getApplicationContext(), MyReceiverProgress.class);
                    myIntent.putExtra("project_progress", "project_progress");
                    //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
                    //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, PendingIntent.FLAG_ONE_SHOT);

                    PendingIntent pendingIntent = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                        pendingIntent = PendingIntent.getActivity
                                (getApplicationContext(), 0, myIntent, PendingIntent.FLAG_MUTABLE);
                    }
                    else
                    {
                        pendingIntent = PendingIntent.getActivity
                                (getApplicationContext(), 0, myIntent, PendingIntent.FLAG_ONE_SHOT);
                    }

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        CharSequence name = "my_channel_4";
                        String description_channel = "This is my channel 4";
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                        mChannel.setDescription(description_channel);
                        mChannel.enableLights(true);
                        mChannel.setLightColor(Color.RED);
                        mChannel.enableVibration(true);
                        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                        mChannel.setShowBadge(false);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.createNotificationChannel(mChannel);
                    }

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(MyJobServiceProgress.this, CHANNEL_ID)
                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setContentTitle("DigiWorkTime")
                                    .setContentText("Please enter progress report.")
                                    .setTicker("Notification!")
                                    .setWhen(System.currentTimeMillis())
                                    .setContentIntent(pendingIntent)
                                    .setAutoCancel(true)
                                    .setVibrate(new long[] {0,100,0,100})
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);


                    // Sets an ID for the notification
                    int mNotificationId = 3;
                    // Gets an instance of the NotificationManager service
                    NotificationManager mNotifyMgr =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    // Builds the notification and issues it.
                    mNotifyMgr.notify(mNotificationId, mBuilder.build());

                    try{Util.cancelProgress(MyJobServiceProgress.this);}catch (Exception e){e.printStackTrace();}

                }

                // if notification check box checked = true => cancel reminder
            }else{
                Log.d("notify", "Notification off");
                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                try{manager.cancel(pendingIntent); Util.cancelProgress(MyJobServiceProgress.this);}catch (Exception e){e.printStackTrace();}
            }
        }
    }


	 /* ****************************************************************************************************************/


    private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }


    public static String convertSecondsToHMmSs(long millis) {

        String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS +a";
        // Create a DateFormatter object for displaying date in specified format.
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);

        //String formatTime = String.format("%d:%02d:%02d", h,m,s);
        System.out.println("............. " + formatter.format(calendar.getTime()));

        return formatter.format(calendar.getTime());
    }

}
