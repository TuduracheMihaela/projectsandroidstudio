package com.sharethatdata.digitime.notify.timerecord;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digitime.MyGlobals;
import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.RecordTimeActivity;
import com.sharethatdata.digitime.R.drawable;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cTimeRecord;
import com.sharethatdata.webservice.notification.notification.android.oreo.MyForeGroundServiceYesterday;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

public class MyReceiverYesterday extends BroadcastReceiver{

	
	 @Override
	 public void onReceive(Context context, Intent intent) {
		  
		 Log.d("ME2", "Notification started for yesterday");
		 
	//	 Toast.makeText(context, "Service yesterday", Toast.LENGTH_SHORT).show();

		 // Comment this because I try to implement service for android 8 and up
		 /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		 {
			 Intent service1 = new Intent(context, MyForeGroundServiceYesterday.class);
			 context.startForegroundService(service1);
		 }else {
			 Intent service1 = new Intent(context, MyServiceYesterday.class);
			 context.startService(service1);
		 }*/

		 // Schedule a service for android 8 and up
		 Util.scheduleJobYesterday(context);


	        /*NotificationCompat.Builder mBuilder =
	            new NotificationCompat.Builder(context)
	            .setSmallIcon(R.drawable.ic_launcher)
	            .setContentTitle("DigiWorkTime")
		        .setContentText("Please enter your time report for yesterday.");
	       // mBuilder.setDefaults(Notification.DEFAULT_SOUND);
	        mBuilder.setAutoCancel(true);

	        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	        mNotificationManager.notify(2, mBuilder.build());*/

	 }
}
