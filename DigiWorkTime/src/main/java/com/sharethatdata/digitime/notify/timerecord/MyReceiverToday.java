package com.sharethatdata.digitime.notify.timerecord;

import java.util.Calendar;

import com.sharethatdata.digitime.R;
import com.sharethatdata.digitime.R.drawable;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class MyReceiverToday extends BroadcastReceiver
{

	 @Override
	 public void onReceive(Context context, Intent intent) {

		 Log.d("ME1", "Notification started for today");
		 
	//	 Toast.makeText(context, "Service Today", Toast.LENGTH_SHORT).show();

		 // Comment this because I try to implement service for android 8 and up
		 //Intent service1 = new Intent(context, MyServiceToday.class);
	       //context.startService(service1);

		 // Schedule a service for android 8 and up
		 Util.scheduleJobToday(context);

	       /* NotificationCompat.Builder mBuilder =
	            new NotificationCompat.Builder(context)
	            .setSmallIcon(R.drawable.ic_launcher)
	            .setContentTitle("DigiWorkTime")
	            .setContentText("Please enter your time report for today.");
	             mBuilder.setAutoCancel(true);

	        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	        mNotificationManager.notify(1, mBuilder.build());*/
	 }
	

}
