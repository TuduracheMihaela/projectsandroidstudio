package com.sharethatdata.digitime.notify.timerecord;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * Created by tudur on 27-Aug-19.
 */

public class Util {

    // schedule the start of the service every 10 - 30 seconds
    public static void scheduleJobYesterday(Context context) {

        SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
        String startDay = sharedPrefs.getString("prefWorkDayStart", "");

        Calendar c = Calendar.getInstance();
        int mStartYear = c.get(Calendar.YEAR);
        int mStartMonth = c.get(Calendar.MONTH);
        int mStartDay = c.get(Calendar.DAY_OF_MONTH);
        int mStartHour = c.get(Calendar.HOUR_OF_DAY);
        int mStartMinute = c.get(Calendar.MINUTE);
        int mEndHour = c.get(Calendar.HOUR_OF_DAY);
        int mEndMinute = c.get(Calendar.MINUTE);

        final Calendar date = Calendar.getInstance();

        date.clear();
        date.set(mStartYear, mStartMonth, mStartDay);

        String hourS = null;
        String minS = null;

        if(!startDay.isEmpty()){
            String[] tokens = startDay.split(":");
            hourS = tokens[0];
            minS = tokens[1];
        }else{
            hourS = "09";
            minS = "00";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.clear();

        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourS));
        calendar.set(Calendar.MINUTE, Integer.parseInt(minS));

        //calendar.set(Calendar.HOUR_OF_DAY, 12);
        //calendar.set(Calendar.MINUTE, 28);
        calendar.set(Calendar.SECOND, 00);
        //calendar.set(Calendar.AM_PM, Calendar.PM);

        System.out.println("Time morning set: " + hourS + ":" + minS);

        long when = calendar.getTimeInMillis();

        ComponentName serviceComponent = new ComponentName(context, MyJobServiceYesterday.class);
        JobInfo.Builder builder = new JobInfo.Builder(1, serviceComponent);
        //builder.setMinimumLatency(1 * 1000); // wait at least
        //builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // needs network connection
        //builder.setPersisted(true); // scheduled even after device reboots
        //builder.setPeriodic(when);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }


    public static void scheduleJobToday(Context context) {

        SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
        String endDay = sharedPrefs.getString("prefWorkDayEnd", "");

        Calendar c = Calendar.getInstance();
        int mStartYear = c.get(Calendar.YEAR);
        int mStartMonth = c.get(Calendar.MONTH);
        int mStartDay = c.get(Calendar.DAY_OF_MONTH);
        int mStartHour = c.get(Calendar.HOUR_OF_DAY);
        int mStartMinute = c.get(Calendar.MINUTE);
        int mEndHour = c.get(Calendar.HOUR_OF_DAY);
        int mEndMinute = c.get(Calendar.MINUTE);

        final Calendar date = Calendar.getInstance();

        date.clear();
        date.set(mStartYear, mStartMonth, mStartDay);

        String hourE = null;
        String minE = null;

        if(!endDay.isEmpty()){
            String[] tokens = endDay.split(":");
            hourE = tokens[0];
            minE = tokens[1];
        }else{
            hourE = "18";
            minE = "00";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.clear();

        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourE));
        calendar.set(Calendar.MINUTE, Integer.parseInt(minE));

        //calendar.set(Calendar.HOUR_OF_DAY, 12);
        //calendar.set(Calendar.MINUTE, 28);
        calendar.set(Calendar.SECOND, 00);
        //calendar.set(Calendar.AM_PM, Calendar.PM);

        System.out.println("Time morning set: " + hourE + ":" + minE);

        long when = calendar.getTimeInMillis();

        ComponentName serviceComponent = new ComponentName(context, MyJobServiceToday.class);
        JobInfo.Builder builder = new JobInfo.Builder(2, serviceComponent);
        //builder.setMinimumLatency(1 * 1000); // wait at least
        //builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // needs network connection
        //builder.setPersisted(true); //scheduled even after device reboots
        //builder.setPeriodic(when);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }



    public static void scheduleJobProgress(Context context) {

        // set calendar
        Calendar c = Calendar.getInstance();
        int mStartYear = c.get(Calendar.YEAR);
        int mStartMonth = c.get(Calendar.MONTH);
        int mStartDay = c.get(Calendar.DAY_OF_MONTH);
        int mStartHour = c.get(Calendar.HOUR_OF_DAY);
        int mStartMinute = c.get(Calendar.MINUTE);
        int mEndHour = c.get(Calendar.HOUR_OF_DAY);
        int mEndMinute = c.get(Calendar.MINUTE);

        final Calendar date = Calendar.getInstance();

        date.clear();
        date.set(mStartYear, mStartMonth, mStartDay);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();

        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        //calendarToday.set(Calendar.AM_PM, Calendar.PM);


        long when = calendar.getTimeInMillis();

        ComponentName serviceComponent = new ComponentName(context, MyJobServiceProgress.class);
        JobInfo.Builder builder = new JobInfo.Builder(3, serviceComponent);
        //builder.setMinimumLatency(1 * 1000); // wait at least
        //builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // needs network connection
        //builder.setPersisted(true); // scheduled even after device reboots
        //builder.setPeriodic(when);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }

    public static void cancelYesterday(Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(1);
    }

    public static void cancelToday(Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(2);
    }

    public static void cancelProgress(Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(3);
    }

}
