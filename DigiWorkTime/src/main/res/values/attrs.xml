<?xml version="1.0" encoding="utf-8"?>

<resources>

    <!-- A tonal variation of the primary color. -->
    <attr name="colorPrimaryVariant" format="color"/>
    <!-- The secondary branding color for the app, usually a bright complement to the primary
         branding color. -->
    <attr name="colorSecondary" format="color"/>
    <!-- A tonal variation of the secondary color. -->
    <attr name="colorSecondaryVariant" format="color"/>
    <!-- The color of surfaces such as cards, sheets, menus. -->
    <attr name="colorSurface" format="color"/>
    <!-- A color that passes accessibility guidelines for text/iconography when drawn on top of
         primary. -->
    <attr name="colorOnPrimary" format="color"/>
    <!-- A color that passes accessibility guidelines for text/iconography when drawn on top of
         secondary. -->
    <attr name="colorOnSecondary" format="color"/>
    <!-- A color that passes accessibility guidelines for text/iconography when drawn on top of
         background. -->
    <attr name="colorOnBackground"/>
    <!-- A color that passes accessibility guidelines for text/iconography when drawn on top of
         error. -->
    <attr name="colorOnError" format="color"/>
    <!-- A color that passes accessibility guidelines for text/iconography when drawn on top of
         surface. -->
    <attr name="colorOnSurface" format="color"/>

    <!-- The scrim background that appears below modals and expanded navigation menus.
         The background can either be a color or a bitmap drawable with tileMode set to repeat. -->
    <attr name="scrimBackground" format="color|reference"/>

    <!-- Internal flag used to denote that a theme is a Theme.MaterialComponents theme or a
         Theme.MaterialComponents.Bridge theme. -->
    <attr name="isMaterialTheme" format="boolean"/>

    <!-- When set to true, the material selection controls will tint themselves according to
         Material Theme colors. When set to false, Material Theme colors will
         be ignored. This value should be set to false when using custom drawables
         that should not be tinted. This value is ignored if a buttonTint is set.
         Set this attribute on your styles for each selection control.-->
    <attr name="useMaterialThemeColors" format="boolean"/>

    <attr name="materialThemeOverlay" format="reference" />

    <!-- Style to use for TextInputLayout in the theme. -->
    <attr name="textInputStyle" format="reference"/>

    <declare-styleable name="TextInputLayout">
        <!-- The text color for input text. -->
        <attr name="android:textColorHint"/>

        <!-- The hint to display in the floating label. -->
        <attr name="android:hint"/>
        <!-- Whether the layout's floating label functionality is enabled. -->
        <attr name="hintEnabled" format="boolean"/>
        <!-- Whether to animate hint state changes. -->
        <attr name="hintAnimationEnabled" format="boolean"/>
        <!-- TextAppearance of the hint in the floating label. -->
        <attr name="hintTextAppearance" format="reference"/>

        <!-- The text to display as helper text underneath the text input area. -->
        <attr name="helperText" format="string"/>
        <!-- Whether the layout's helper text functionality is enabled. -->
        <attr name="helperTextEnabled" format="boolean"/>
        <!-- TextAppearance of the helper text displayed underneath the text input area. -->
        <attr name="helperTextTextAppearance" format="reference"/>

        <!-- Whether the layout is laid out as if an error will be displayed. -->
        <attr name="errorEnabled" format="boolean"/>
        <!-- TextAppearance of any error message displayed. -->
        <attr name="errorTextAppearance" format="reference"/>

        <!-- Whether the layout is laid out as if the character counter will be displayed. -->
        <attr name="counterEnabled" format="boolean"/>
        <!-- The max length to display in the character counter. -->
        <attr name="counterMaxLength" format="integer"/>
        <!-- TextAppearance of the character counter. -->
        <attr name="counterTextAppearance" format="reference"/>
        <!-- TextAppearance of the character counter when the text is longer than the max. -->
        <attr name="counterOverflowTextAppearance" format="reference"/>

        <!-- Whether the view will display a toggle when the EditText has a password. -->
        <attr name="passwordToggleEnabled" format="boolean"/>
        <!-- Drawable to use as the password input visibility toggle icon. -->
        <attr name="passwordToggleDrawable" format="reference"/>
        <!-- Text to set as the content description for the password input visibility toggle. -->
        <attr name="passwordToggleContentDescription" format="string"/>
        <!-- Icon to use for the password input visibility toggle -->
        <attr name="passwordToggleTint" format="color"/>
        <!-- Blending mode used to apply the background tint. -->
        <attr name="passwordToggleTintMode">
            <!-- The tint is drawn on top of the drawable.
                 [Sa + (1 - Sa)*Da, Rc = Sc + (1 - Sa)*Dc] -->
            <enum name="src_over" value="3"/>
            <!-- The tint is masked by the alpha channel of the drawable. The drawable’s
                 color channels are thrown out. [Sa * Da, Sc * Da] -->
            <enum name="src_in" value="5"/>
            <!-- The tint is drawn above the drawable, but with the drawable’s alpha
                 channel masking the result. [Da, Sc * Da + (1 - Sa) * Dc] -->
            <enum name="src_atop" value="9"/>
            <!-- Multiplies the color and alpha channels of the drawable with those of
                 the tint. [Sa * Da, Sc * Dc] -->
            <enum name="multiply" value="14"/>
            <!-- [Sa + Da - Sa * Da, Sc + Dc - Sc * Dc] -->
            <enum name="screen" value="15"/>
        </attr>

        <!-- Whether the text input area should be drawn as a filled box, an outline box, or not as a box.-->
        <attr name="boxBackgroundMode">
            <!-- Specifies that there should be no box set on the text input area. -->
            <enum name="none" value="0"/>
            <!-- Filled box mode for the text input box. -->
            <enum name="filled" value="1"/>
            <!-- Outline box mode for the text input box. -->
            <enum name="outline" value="2"/>
        </attr>
        <!-- Value to use for the EditText's collapsed top padding in box mode. -->
        <attr name="boxCollapsedPaddingTop" format="dimension"/>
        <!-- The value to use for the box's top start corner radius when in box mode. -->
        <attr name="boxCornerRadiusTopStart" format="dimension"/>
        <!-- The value to use for the box's top end corner radius when in box mode. -->
        <attr name="boxCornerRadiusTopEnd" format="dimension"/>
        <!-- The value to use for the box's bottom start corner radius when in box mode. -->
        <attr name="boxCornerRadiusBottomStart" format="dimension"/>
        <!-- The value to use for the box's bottom end corner radius when in box mode. -->
        <attr name="boxCornerRadiusBottomEnd" format="dimension"/>
        <!-- The color to use for the box's stroke when in outline box mode. -->
        <attr name="boxStrokeColor" format="color"/>
        <!-- The color to use for the box's background color when in filled box mode. -->
        <attr name="boxBackgroundColor" format="color"/>
        <!-- The value to use for the box's stroke when in outline box mode. -->
        <attr name="boxStrokeWidth" format="dimension"/>

    </declare-styleable>

    <declare-styleable name="ThemeEnforcement">
        <!-- Internal flag used to denote that a style uses new attributes defined by
             Theme.MaterialComponents, and that the component should check via ThemeEnforcement that the
             client's app theme inherits from Theme.MaterialComponents.

             Not all usages of new attributes are problematic in the context of a legacy app theme. You
             should only use this flag if a particular usage is known to cause a visual glitch or crash.
             For example, tinting a vector drawable with a non-existent theme attribute is known to
             crash on pre-21 devices. -->
        <attr name="enforceMaterialTheme" format="boolean"/>
        <!-- Internal flag used to denote that a style requires that the textAppearance attribute is
             specified and evaluates to a valid text appearance. -->
        <attr name="enforceTextAppearance" format="boolean"/>
        <!-- Attribute used to check that a component has a TextAppearance specified on it. -->
        <attr name="android:textAppearance"/>
    </declare-styleable>

    <declare-styleable name="FlowLayout">
        <!-- Horizontal spacing between two items being laid out. -->
        <attr name="itemSpacing" format="dimension"/>
        <!-- Vertical Spacing between two lines of items being laid out. -->
        <attr name="lineSpacing" format="dimension"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->
    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->


    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="Snackbar">
        <!-- Style to use for Snackbars in this theme. -->
        <attr name="snackbarStyle" format="reference"/>
        <!-- Style to use for action button within a Snackbar in this theme. -->
        <attr name="snackbarButtonStyle" format="reference"/>
    </declare-styleable>

    <declare-styleable name="SnackbarLayout">
        <attr name="android:maxWidth"/>
        <attr name="elevation"/>
        <attr name="maxActionInlineWidth" format="dimension"/>
        <!-- Sets the enter and exit animations for a Snackbar. -->
        <attr name="animationMode" format="enum">
            <!-- Mode that corresponds to the slide in and out animations. -->
            <enum name="slide" value="0"/>
            <!-- Mode that corresponds to the fade in and out animations. -->
            <enum name="fade" value="1"/>
        </attr>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="MaterialRadioButton">
        <!-- When set to true, MaterialRadioButton will tint itself according to
          Material Theme colors. When set to false, Material Theme colors will
          be ignored. This value should be set to false when using custom drawables
          that should not be tinted. This value is ignored if a buttonTint is set. -->
        <attr name="useMaterialThemeColors"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <attr name="navigationViewStyle" format="reference"/>

    <declare-styleable name="NavigationView">
        <attr name="android:background"/>
        <attr name="android:fitsSystemWindows"/>
        <attr name="android:maxWidth"/>
        <attr name="elevation"/>
        <!-- The menu resource to inflate and populate items from. -->
        <attr name="menu" format="reference"/>
        <attr name="itemIconTint" format="color"/>
        <attr name="itemTextColor" format="color"/>
        <attr name="itemBackground" format="reference"/>
        <attr name="itemTextAppearance" format="reference"/>
        <!-- Layout resource to inflate as the header -->
        <attr name="headerLayout" format="reference"/>
        <!-- Horizontal padding (left and right) of navigation items, around the icon & text -->
        <attr name="itemHorizontalPadding" format="dimension"/>
        <!-- Padding between the icon and the text for navigation items that display an icon  -->
        <attr name="itemIconPadding" format="dimension"/>
        <!-- The size of the icon navigation items that display an icon  -->
        <attr name="itemIconSize" format="dimension"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="MaterialCheckBox">
        <!-- When set to true, MaterialCheckBox will tint itself according to
          Material Theme colors. When set to false, Material Theme colors will
          be ignored. This value should be set to false when using custom drawables
          that should not be tinted. This value is ignored if a buttonTint is set. -->
        <attr name="useMaterialThemeColors"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="BottomAppBar">
        <!-- Background for the BottomAppBar. -->
        <attr name="backgroundTint"/>
        <!-- The alignment of the fab relative to the BottomAppBar. -->
        <attr name="fabAlignmentMode">
            <!-- Mode that aligns the fab to the center. -->
            <enum name="center" value="0"/>
            <!-- Mode that aligns the fab to the end. -->
            <enum name="end" value="1"/>
        </attr>
        <!-- The animation mode that should be used when the fab animates between alignment modes. -->
        <attr name="fabAnimationMode">
            <!-- Mode that scales the fab down to a point, moves it, then scales the fab back to its normal size. -->
            <enum name="scale" value="0"/>
            <!-- Mode that slides the fab from one alignment mode to the next. -->
            <enum name="slide" value="1"/>
        </attr>
        <!-- The margin between the semi-circular cradle for the fab and the fab. -->
        <attr name="fabCradleMargin" format="dimension"/>
        <!-- The radius of the rounded corners on each side of the cradle. -->
        <attr name="fabCradleRoundedCornerRadius" format="dimension"/>
        <!-- The vertical offset between the fab from the cradle. -->
        <attr name="fabCradleVerticalOffset" format="dimension"/>
        <!-- Whether the BottomAppBar should hide when a NestedScrollView is scrolled. -->
        <attr name="hideOnScroll" format="boolean"/>
    </declare-styleable>

    <!-- Style to use for BottomAppBar in this theme. -->
    <attr name="bottomAppBarStyle" format="reference"/>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="AppBarLayout">
        <!-- Deprecated. Elevation is now controlled via a state list animator. -->
        <attr name="elevation"/>
        <attr name="android:background"/>
        <!-- The initial expanded state for the AppBarLayout. This only takes effect when this
             view is a direct child of a CoordinatorLayout. -->
        <attr name="expanded" format="boolean"/>
        <attr name="android:keyboardNavigationCluster"/>
        <attr name="android:touchscreenBlocksFocus"/>
        <!-- Whether the {@link AppBarLayout} should lift on scroll. If set to
             true, the {@link AppBarLayout} will animate to the lifted, or
             elevated, state when content is scrolled beneath it. Requires
             `app:layout_behavior="@string/appbar_scrolling_view_behavior` to be
             set on the scrolling sibling (e.g., `NestedScrollView`,
             `RecyclerView`, etc.). Default is false. -->
        <attr name="liftOnScroll" format="boolean"/>
        <!-- The id of the view that the {@link AppBarLayout} should use to determine whether
             it should be lifted (i.e., only if {@link R.attr#liftOnScroll} is set to true).
             If this id is not set, the {@link AppBarLayout} will use the target view provided
             by nested scrolling to determine whether it should be lifted. -->
        <attr name="liftOnScrollTargetViewId" format="reference"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="SwitchMaterial">
        <!-- When set to true, SwitchMaterial will tint itself according to
          Material Theme colors. When set to false, Material Theme colors will
          be ignored. This value should be set to false when using custom drawables
          that should not be tinted. This value is ignored if a buttonTint is set. -->
        <attr name="useMaterialThemeColors"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <!-- Style to use for MaterialButtons in this theme. -->
    <attr name="materialButtonStyle" format="reference"/>

    <declare-styleable name="MaterialButton">
        <attr name="android:insetLeft"/>
        <attr name="android:insetRight"/>
        <attr name="android:insetTop"/>
        <attr name="android:insetBottom"/>
        <!-- Background for the MaterialButton -->
        <attr name="backgroundTint"/>
        <attr name="backgroundTintMode"/>
        <!-- Icon drawable to display at the start of this view. -->
        <attr name="icon" format="reference"/>
        <!-- Specifies the width and height to use for the icon drawable. -->
        <attr name="iconSize" format="dimension"/>
        <!-- Padding between icon and button text. -->
        <attr name="iconPadding" format="dimension"/>
        <!-- Specifies how the icon should be positioned on the X axis. -->
        <attr name="iconGravity">
            <!-- Push icon to the start of the button. -->
            <flag name="start" value="0x1" />
            <!-- Push the icon to the start of the text keeping a distance equal to
                 {@link R.attr#iconPadding} from the text. -->
            <flag name="textStart" value="0x2" />
        </attr>
        <!-- Tint for icon drawable to display. -->
        <attr name="iconTint" format="color"/>
        <!-- Tint mode for icon drawable to display. -->
        <attr name="iconTintMode"/>
        <!-- Specifies the color used to draw the path outline of the button. Attribute type definition
             is in resources package. -->
        <attr name="strokeColor"/>
        <!-- Width of the stroke path of the button. Default is 0. Attribute type definition is in
             resources package. -->
        <attr name="strokeWidth"/>
        <!--
            Specifies the radius for the corners of the button. Default is 0, for non-rounded corners.
        -->
        <attr name="cornerRadius" format="dimension"/>
        <!-- Ripple color for the button. This may be a color state list, if the desired ripple color
             should be stateful. Attribute type definition is in resources package. -->
        <attr name="rippleColor"/>
    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <!-- Style to use for ChipGroups in this theme. -->
    <attr name="chipGroupStyle" format="reference"/>
    <!-- Style to use for Chips in this theme, usually to be used as a view. -->
    <attr name="chipStyle" format="reference"/>
    <!-- Style to use for standalone Chips in this theme, usually to be used in an EditText. -->
    <attr name="chipStandaloneStyle" format="reference"/>

    <declare-styleable name="Chip">
        <!-- Surface layer color to apply to the chip. -->
        <attr name="chipSurfaceColor" format="color"/>
        <!-- Background color to apply to the chip. -->
        <attr name="chipBackgroundColor" format="color"/>
        <!-- Min height to apply to the chip. Total height includes stroke width. -->
        <attr name="chipMinHeight" format="dimension"/>
        <!-- Corner radius to apply to the chip's shape. -->
        <attr name="chipCornerRadius" format="dimension"/>
        <!-- Stroke color to apply to the chip's outline. -->
        <attr name="chipStrokeColor" format="color"/>
        <!-- Stroke width to apply to the chip's outline. -->
        <attr name="chipStrokeWidth" format="dimension"/>
        <!-- Ripple color to apply to the chip. -->
        <attr name="rippleColor"/>
        <!-- Minimum size of chip's touch target, by default, Android recommended 48dp. -->
        <attr name="chipMinTouchTargetSize" format="dimension"/>
        <!-- Reference to touch target delegate used to ensure that chip meets the minimum accessible
         touch target size (48dp by default). Ignored if chip's hitRect already meets the minimum. -->
        <attr name="chipTouchTargetDelegate" format="reference"/>

        <!-- Text to display on the chip. -->
        <attr name="android:text"/>
        <!-- Text color. -->
        <attr name="android:textColor"/>
        <!-- Default appearance of text: color, typeface, size, and style. -->
        <attr name="android:textAppearance"/>
        <!-- If set, causes words that are longer than the view is wide to be ellipsized instead of
             truncated at the end. -->
        <attr name="android:ellipsize"/>
        <!-- Make the Chip to be at most this many pixels wide. -->
        <attr name="android:maxWidth"/>

        <!-- Whether to show the chip icon. -->
        <attr name="chipIconVisible" format="boolean"/>
        <!-- Deprecated. Use chipIconVisible instead. -->
        <attr name="chipIconEnabled" format="boolean"/>
        <!-- Icon drawable to display at the start of the chip. -->
        <attr name="chipIcon" format="reference"/>
        <!-- Tint to apply to the chip icon. -->
        <attr name="chipIconTint" format="color"/>
        <!-- Size of the chip's icon and checked icon. -->
        <attr name="chipIconSize" format="dimension"/>

        <!-- Whether to show the close icon. -->
        <attr name="closeIconVisible" format="boolean"/>
        <!-- Deprecated. Use closeIconVisible instead. -->
        <attr name="closeIconEnabled" format="boolean"/>
        <!-- Close icon drawable to display at the end of the chip. -->
        <attr name="closeIcon" format="reference"/>
        <!-- Tint to apply to the chip's close icon. -->
        <attr name="closeIconTint" format="color"/>
        <!-- Size of the chip's close icon. -->
        <attr name="closeIconSize" format="dimension"/>

        <!-- Whether the chip can be checked. If false, the chip will act as a button. -->
        <attr name="android:checkable"/>
        <!-- Whether to show the checked icon. -->
        <attr name="checkedIconVisible" format="boolean"/>
        <!-- Deprecated. Use checkedIconVisible instead -->
        <attr name="checkedIconEnabled" format="boolean"/>
        <!-- Check icon drawable to overlay the chip's icon. -->
        <attr name="checkedIcon" format="reference"/>

        <!-- Motion spec for show animation. This should be a MotionSpec resource. -->
        <attr name="showMotionSpec"/>
        <!-- Motion spec for hide animation. This should be a MotionSpec resource. -->
        <attr name="hideMotionSpec"/>

        <!-- The following attributes are adjustable padding on the chip, listed from start to end. -->

        <!-- Chip starts here. -->

        <!-- Padding at the start of the chip, before the icon. -->
        <attr name="chipStartPadding" format="dimension"/>
        <!-- Padding at the start of the icon, after the start of the chip. If icon exists. -->
        <attr name="iconStartPadding" format="dimension"/>

        <!-- Icon is here. -->

        <!-- Padding at the end of the icon, before the text. If icon exists. -->
        <attr name="iconEndPadding" format="dimension"/>
        <!-- Padding at the start of the text, after the icon. -->
        <attr name="textStartPadding" format="dimension"/>

        <!-- Text is here. -->

        <!-- Padding at the end of the text, before the close icon. -->
        <attr name="textEndPadding" format="dimension"/>
        <!-- Padding at the start of the close icon, after the text. If close icon exists. -->
        <attr name="closeIconStartPadding" format="dimension"/>

        <!-- Close icon is here. -->

        <!-- Padding at the end of the close icon, before the end of the chip. If close icon exists. -->
        <attr name="closeIconEndPadding" format="dimension"/>
        <!-- Padding at the end of the chip, after the close icon. -->
        <attr name="chipEndPadding" format="dimension"/>

        <!-- Chip ends here. -->
    </declare-styleable>

    <declare-styleable name="ChipGroup">

        <!-- Horizontal and vertical spacing between chips in this group. -->
        <attr name="chipSpacing" format="dimension"/>
        <!-- Horizontal spacing between chips in this group. -->
        <attr name="chipSpacingHorizontal" format="dimension"/>
        <!-- Vertical spacing between chips in this group. -->
        <attr name="chipSpacingVertical" format="dimension"/>

        <!-- Constrains the chips in this group to a single horizontal line. By default, this is false
             and the chips in this group will reflow to multiple lines.

             If you set this to true, you'll usually want to wrap this ChipGroup in a
             HorizontalScrollView. -->
        <attr name="singleLine" format="boolean"/>

        <!-- Whether only a single chip in this group is allowed to be checked at any time. By default,
             this is false and multiple chips in this group are allowed to be checked at once. -->
        <attr name="singleSelection" format="boolean"/>
        <!-- The id of the child chip that should be checked by default within this chip group. -->
        <attr name="checkedChip" format="reference"/>

    </declare-styleable>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

    <declare-styleable name="MaterialShape">
        <!-- Shape appearance style reference to be used to construct a ShapeAppearanceModel. -->
        <attr name="shapeAppearance" format="reference"/>
        <!-- Shape appearance overlay style reference to be used to modify the shapeAppearance. -->
        <attr name="shapeAppearanceOverlay" format="reference"/>
    </declare-styleable>

    <declare-styleable name="ShapeAppearance">
        <!-- Corner size to be used in the ShapeAppearance. All corners default to this value -->
        <attr name="cornerSize" format="dimension"/>
        <!-- Top left corner size to be used in the ShapeAppearance. -->
        <attr name="cornerSizeTopLeft" format="dimension"/>
        <!-- Top right corner size to be used in the ShapeAppearance. -->
        <attr name="cornerSizeTopRight" format="dimension"/>
        <!-- Bottom right corner size to be used in the ShapeAppearance. -->
        <attr name="cornerSizeBottomRight" format="dimension"/>
        <!-- Bottom left corner size to be used in the ShapeAppearance. -->
        <attr name="cornerSizeBottomLeft" format="dimension"/>

        <!-- Corner family to be used in the ShapeAppearance. All corners default to this value -->
        <attr name="cornerFamily" format="enum">
            <enum name="rounded" value="0"/>
            <enum name="cut" value="1"/>
        </attr>
        <!-- Top left corner family to be used in the ShapeAppearance. -->
        <attr name="cornerFamilyTopLeft" format="enum">
            <enum name="rounded" value="0"/>
            <enum name="cut" value="1"/>
        </attr>
        <!-- Top right corner family to be used in the ShapeAppearance. -->
        <attr name="cornerFamilyTopRight" format="enum">
            <enum name="rounded" value="0"/>
            <enum name="cut" value="1"/>
        </attr>
        <!-- Bottom right corner family to be used in the ShapeAppearance. -->
        <attr name="cornerFamilyBottomRight" format="enum">
            <enum name="rounded" value="0"/>
            <enum name="cut" value="1"/>
        </attr>
        <!-- Bottom left corner family to be used in the ShapeAppearance. -->
        <attr name="cornerFamilyBottomLeft" format="enum">
            <enum name="rounded" value="0"/>
            <enum name="cut" value="1"/>
        </attr>
    </declare-styleable>

    <!-- Shape appearance style reference for small components. -->
    <attr name="shapeAppearanceSmallComponent" format="reference"/>
    <!-- Shape appearance style reference for medium components. -->
    <attr name="shapeAppearanceMediumComponent" format="reference"/>
    <!-- Shape appearance style reference for large components. -->
    <attr name="shapeAppearanceLargeComponent" format="reference"/>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->



    <!--<declare-styleable name="MaterialComponentsTheme">

        &lt;!&ndash; The accent color that's applied to many components and controls.
             This is set to colorSecondary by default, but can also be set to colorPrimary.  &ndash;&gt;
        <attr name="colorAccent"/>

        &lt;!&ndash; The primary branding color for the app. &ndash;&gt;
        <attr name="colorPrimary"/>
        &lt;!&ndash; The dark variant of the primary color. &ndash;&gt;
        <attr name="colorPrimaryDark"/>
        &lt;!&ndash; A tonal variation of primary color. &ndash;&gt;
        <attr name="colorPrimaryVariant"/>

        &lt;!&ndash; The secondary branding color for the app, usually a bright complement to the primary
             branding color. &ndash;&gt;
        <attr name="colorSecondary"/>
        &lt;!&ndash; A tonal variation of secondary color. &ndash;&gt;
        &lt;!&ndash;<attr name="colorSecondaryVariant"/>&ndash;&gt;

        &lt;!&ndash; The underlying color of an app's content. &ndash;&gt;
        <attr name="android:colorBackground"/>
        &lt;!&ndash; The color used to indicate error status. &ndash;&gt;
        <attr name="colorError"/>
        &lt;!&ndash; The color of surfaces such as cards, sheets, menus. &ndash;&gt;
        &lt;!&ndash;<attr name="colorSurface"/>&ndash;&gt;

        &lt;!&ndash; A color that passes accessibility guidelines for text/iconography when drawn on top of
             primary. &ndash;&gt;
        &lt;!&ndash;<attr name="colorOnPrimary"/>&ndash;&gt;
        &lt;!&ndash; A color that passes accessibility guidelines for text/iconography when drawn on top of
             secondary. &ndash;&gt;
        &lt;!&ndash;<attr name="colorOnSecondary"/>&ndash;&gt;
        &lt;!&ndash; A color that passes accessibility guidelines for text/iconography when drawn on top of
             background. &ndash;&gt;
        &lt;!&ndash;<attr name="colorOnBackground"/>&ndash;&gt;
        &lt;!&ndash; A color that passes accessibility guidelines for text/iconography when drawn on top of
             error. &ndash;&gt;
        &lt;!&ndash;<attr name="colorOnError"/>&ndash;&gt;
        &lt;!&ndash; A color that passes accessibility guidelines for text/iconography when drawn on top of
             surface. &ndash;&gt;
        &lt;!&ndash;<attr name="colorOnSurface"/>&ndash;&gt;

        &lt;!&ndash; The scrim background that appears below modals and expanded navigation menus.
             The background can either be a color or a bitmap drawable with tileMode set to repeat. &ndash;&gt;
        <attr name="scrimBackground"/>

        &lt;!&ndash; Default color of background imagery for floating components, ex. dialogs, popups, and cards. &ndash;&gt;
        <attr name="colorBackgroundFloating"/>

        &lt;!&ndash; Theme to use for modal bottom sheet dialogs spawned from this theme. &ndash;&gt;
        <attr name="bottomSheetDialogTheme"/>
        &lt;!&ndash; Style to use for modal bottom sheets in this theme. &ndash;&gt;
        <attr name="bottomSheetStyle"/>
        &lt;!&ndash; Style to use for MaterialButtons in this theme. &ndash;&gt;
        <attr name="materialButtonStyle"/>
        &lt;!&ndash; Style to use for ChipGroups in this theme. &ndash;&gt;
        <attr name="chipGroupStyle"/>
        &lt;!&ndash; Style to use for Chips in this theme, usually to be used as a view. &ndash;&gt;
        <attr name="chipStyle"/>
        &lt;!&ndash; Style to use for standalone Chips in this theme, usually to be used in an EditText. &ndash;&gt;
        <attr name="chipStandaloneStyle"/>
        &lt;!&ndash; Style to use for TextInputEditTexts in this theme. &ndash;&gt;
        <attr name="editTextStyle"/>
        &lt;!&ndash; Style to use for FloatingActionButtons in this theme. &ndash;&gt;
        <attr name="floatingActionButtonStyle"/>
        &lt;!&ndash; Style to use for MaterialCardView in this theme. &ndash;&gt;
        <attr name="materialCardViewStyle"/>
        &lt;!&ndash; Style to use for NavigationView in this theme. &ndash;&gt;
        <attr name="navigationViewStyle"/>
        &lt;!&ndash; Style to use for TabLayouts in this theme. &ndash;&gt;
        <attr name="tabStyle"/>
        &lt;!&ndash; Style to use for TextInputLayouts in this theme. &ndash;&gt;
        <attr name="textInputStyle"/>

        &lt;!&ndash; Style to use for action button within a Snackbar in this theme. &ndash;&gt;
        <attr name="snackbarButtonStyle"/>

        &lt;!&ndash; Text appearance for the Headline 1 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline1"/>
        &lt;!&ndash; Text appearance for the Headline 2 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline2"/>
        &lt;!&ndash; Text appearance for the Headline 3 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline3"/>
        &lt;!&ndash; Text appearance for the Headline 4 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline4"/>
        &lt;!&ndash; Text appearance for the Headline 5 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline5"/>
        &lt;!&ndash; Text appearance for the Headline 6 style in this theme. &ndash;&gt;
        <attr name="textAppearanceHeadline6"/>
        &lt;!&ndash; Text appearance for the Subtitle 1 style in this theme. &ndash;&gt;
        <attr name="textAppearanceSubtitle1"/>
        &lt;!&ndash; Text appearance for the Subtitle 2 style in this theme. &ndash;&gt;
        <attr name="textAppearanceSubtitle2"/>
        &lt;!&ndash; Text appearance for the Body 1 style in this theme. &ndash;&gt;
        <attr name="textAppearanceBody1"/>
        &lt;!&ndash; Text appearance for the Body 2 style in this theme. &ndash;&gt;
        <attr name="textAppearanceBody2"/>
        &lt;!&ndash; Text appearance for the Caption style in this theme. &ndash;&gt;
        <attr name="textAppearanceCaption"/>
        &lt;!&ndash; Text appearance for the Button style in this theme. &ndash;&gt;
        <attr name="textAppearanceButton"/>
        &lt;!&ndash; Text appearance for the Overline style in this theme. &ndash;&gt;
        <attr name="textAppearanceOverline"/>

        &lt;!&ndash; Shape appearance style reference for small components in this theme. &ndash;&gt;
        &lt;!&ndash;<attr name="shapeAppearanceSmallComponent"/>&ndash;&gt;
        &lt;!&ndash; Shape appearance style reference for medium components in this theme. &ndash;&gt;
        &lt;!&ndash;<attr name="shapeAppearanceMediumComponent"/>&ndash;&gt;
        &lt;!&ndash; Shape appearance style reference for large components in this theme. &ndash;&gt;
        &lt;!&ndash;<attr name="shapeAppearanceLargeComponent"/>&ndash;&gt;
    </declare-styleable>-->




</resources>