package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol.custom.SwitchCompatCustom;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotControl;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cSensorsRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

/**
 * Created by tudur on 18-Feb-19.
 */

public class SensorsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> sensorList;

    //private String status_interval;
    private String sensor_interval;
    //private boolean blRobotStatus;

    ProgressDialog progressDialog;

    ListView listView;

    SwitchCompat aSwitch;

    String id_robot = "";
    String name_robot = "";

    //Declare the timer
    Timer t;

    boolean check_once = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        //Initialize the timer
        t = new Timer();

        getSupportActionBar().setTitle("Sensors -> " + name_robot);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String rid = ((TextView) itemClicked.findViewById(R.id.textViewId)).getText().toString();

                MyGlobals Globals = ((MyGlobals)getApplication());
                //Globals.setValue("sid", rid);

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), RobotsDetailActivity.class);
                //startActivity(intent);

            }

        });

        aSwitch = (SwitchCompat) findViewById(R.id.switch_start_stop);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //switch on
                    Toast.makeText(SensorsActivity.this, "ON", Toast.LENGTH_SHORT).show();

                    // reset, start, stop, monitor
                    new UpdateRobotControl("","","","1").execute(); // monitor

                } else {
                    //switch off
                    Toast.makeText(SensorsActivity.this, "OFF", Toast.LENGTH_SHORT).show();

                    // reset, start, stop, monitor
                    new UpdateRobotControl("","","","0").execute(); // monitor

                    //t.cancel();
                }
            }
        });

        initializeTimer();
        refresh();

        new LoadSensors().execute();
    }


    public void initializeTimer(){

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        //status_interval = sharedPrefs.getString("prefMonitorStatus", "");
        sensor_interval = sharedPrefs.getString("prefMonitorSensor", "");
        //blIntervalCheck = sharedPrefs.getBoolean("prefIntervalCheck", false);

        long interval = 1;
        try{
            interval = TimeUnit.SECONDS.toMillis(Long.parseLong(sensor_interval));
        }catch (Exception e){
            e.printStackTrace();
        }
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {
                      @Override
                      public void run() {
                          if(isNetworkAvailable() == true){
                              //Called each time when 1000 milliseconds (1 second) (the period parameter)
                              new LoadRobotControl(id_robot).execute();
                              new LoadSensors().execute();

                              check_once = true;
                          }else {
                              if(check_once){
                                  runOnUiThread(new Runnable(){
                                      @Override
                                      public void run(){
                                          Toast.makeText(getApplicationContext(), "No internet connnection available", Toast.LENGTH_SHORT).show();
                                      }
                                  });
                                  check_once = false;
                              }
                          }
                      }
        },
                //Set how long before to start calling the TimerTask (in milliseconds)
                0,
                //Set the amount of time between each execution (in milliseconds)
                interval);
    }

    public void refresh(){
        new LoadRobotControl(id_robot).execute();
    }

    /** AsyncTask update robot control  */
    private class UpdateRobotControl extends AsyncTask<String, String, Boolean> {
        String reset, start, stop, monitor;

        public UpdateRobotControl(String reset, String start, String stop, String monitor){
            this.reset = reset;
            this.start = start;
            this.stop = stop;
            this.monitor = monitor;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateRobotControl(id_robot, reset, start , stop, monitor);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SensorsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SensorsActivity.this, "Error" + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(SensorsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    }

                    refresh();
                }
            });
            hideProgressDialog();
        }
    }


    class LoadRobotControl extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotControl(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobotControl robot = MyProvider.getControlRobot(id);

            if (robot != null)
            {
                if (robot.id != "")
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //String start = robot.start;
                            //String stop = robot.stop;
                            //String reset = robot.reset;
                            String monitor = robot.monitor;

                            if(monitor.equals("1")){
                                aSwitch.setChecked(true);
                            }else {
                                aSwitch.setChecked(false);
                            }
                        }
                    });
                }
            }
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            hideProgressDialog();
        }
    }

    class LoadSensors extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            sensorList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cSensorsRobot> mySensor = MyProvider.getSensorsRobots(id_robot);
            for(cSensorsRobot entry : mySensor)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("value",  entry.value);
                map.put("datetime",  entry.datetime);
                map.put("description",  entry.description);

                // adding HashList to ArrayList
                sensorList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (SensorsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(sensorList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(SensorsActivity.this, sensorList,
                                R.layout.activity_sensors_items_listview,
                                new String[]{"id", "name", "value", "datetime", "description"},
                                new int[]{R.id.textViewId, R.id.textViewName, R.id.textViewValue, R.id.textViewDatetime, R.id.textViewDescription});
                        listView.setAdapter(adapter);
                    }
                }
            });

            hideProgressDialog();
        }
    }


    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    protected void onDestroy() {
        Log.d("onDestroy","onDestroy");
        t.cancel();
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(final String msg)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    //if(!((Activity) getApplicationContext()).isFinishing())
                    if(!SensorsActivity.this.isFinishing())
                    {
                        //show dialog

                        // check for existing progressDialog
                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(SensorsActivity.this); // create a progress Dialog
                            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setMessage(msg);
                        }
                        progressDialog.show(); // now display it.
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
