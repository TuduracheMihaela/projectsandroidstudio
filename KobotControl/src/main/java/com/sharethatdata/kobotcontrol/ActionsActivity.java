package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol.adapter.CustomListAdapterDialog;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cActionsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cParametersRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cQueuesRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by tudur on 18-Feb-19.
 */

public class ActionsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> actionList;
    ArrayList<HashMap<String, String>> queueList;
    ArrayList<HashMap<String, String>> parameterList;

    private String sensor_interval;

    ProgressDialog progressDialog;

    ListView listViewActions;
    ListView listViewQueues;

    String id_robot = "";
    String name_robot = "";

    //Declare the timer
    Timer t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actions);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        //Initialize the timer
        t = new Timer();

        getSupportActionBar().setTitle("Actions -> " + name_robot);

        listViewActions = (ListView) findViewById(R.id.listViewActions);
        listViewActions.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String rid = ((TextView) itemClicked.findViewById(R.id.textViewId)).getText().toString();
                new LoadParameters(rid).execute();

            }

        });

        // For Lanscape
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
            if (tabletSize == true) {
                // landscape tablet
                listViewQueues = (ListView) findViewById(R.id.listViewQueues);
                new LoadQueues().execute();

                final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
                pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new LoadQueues().execute();
                        pullToRefresh.setRefreshing(false);
                    }
                });
            }
        }

        new LoadActions().execute();

    }

    public void initializeTimer(){

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        //status_interval = sharedPrefs.getString("prefMonitorStatus", "");
        sensor_interval = sharedPrefs.getString("prefMonitorActionQueue", "");
        //blIntervalCheck = sharedPrefs.getBoolean("prefIntervalCheck", false);

        long interval = 1;
        try{
            interval = TimeUnit.SECONDS.toMillis(Long.parseLong(sensor_interval));
        }catch (Exception e){
            e.printStackTrace();
        }
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {
                                  @Override
                                  public void run() {
                                      //Called each time when 1000 milliseconds (1 second) (the period parameter)

                                  }},
                //Set how long before to start calling the TimerTask (in milliseconds)
                0,
                //Set the amount of time between each execution (in milliseconds)
                interval);
    }

    class LoadActions extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            actionList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cActionsRobot> myAction = MyProvider.getActionsRobots(id_robot);
            for(cActionsRobot entry : myAction)
            {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);
                map.put("parameters",  entry.parameters);

                actionList.add(map);
            }
            return "";
        }

        protected void onPostExecute(String file_url) {
            if (ActionsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(actionList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(ActionsActivity.this, actionList,
                                R.layout.activity_actions_items_listview,
                                new String[]{"id", "name", "description", "parameters"},
                                new int[]{R.id.textViewId, R.id.textViewName, R.id.textViewDescription, R.id.textViewParameters});
                        listViewActions.setAdapter(adapter);
                    }
                }
            });
            hideProgressDialog();
        }
    }

    class LoadQueues extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            queueList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cQueuesRobot> myQueue = MyProvider.getQueuesRobots(id_robot);
            for(cQueuesRobot entry : myQueue)
            {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);
                map.put("result",  entry.result);

                queueList.add(map);
            }
            return "";
        }

        protected void onPostExecute(String file_url) {
            if (ActionsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(queueList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(ActionsActivity.this, queueList,
                                R.layout.activity_queues_items_listview,
                                new String[]{"id", "name", "description", "result"},
                                new int[]{R.id.textViewId, R.id.textViewName, R.id.textViewDescription, R.id.textViewResult});
                        listViewQueues.setAdapter(adapter);
                    }
                }
            });
            hideProgressDialog();
        }
    }

    class LoadParameters extends AsyncTask<String, String, String> {
        String id_action;

        public LoadParameters(String id_action){
            this.id_action = id_action;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            parameterList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cParametersRobot> myParameter = MyProvider.getActionParameters(id_action);
            for(cParametersRobot entry : myParameter)
            {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);
                map.put("required",  entry.required);

                parameterList.add(map);
            }
            return "";
        }

        protected void onPostExecute(String file_url) {
            if (ActionsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(parameterList.size() > 0){
                        final Dialog dialog = new Dialog(ActionsActivity.this);

                        View view = getLayoutInflater().inflate(R.layout.dialog_action_parameter, null);

                        ListView lv = (ListView) view.findViewById(R.id.listViewParameters);
                        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
                        Button btnAddToQueue = (Button) view.findViewById(R.id.btnAddToQueue);

                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        btnAddToQueue.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //new AddActionToQueue(id_robot, id_action, "").execute();
                            }
                        });

                        // Change MyActivity.this and myListOfItems to your own values
                        CustomListAdapterDialog clad = new CustomListAdapterDialog(ActionsActivity.this,
                                R.layout.dialog_action_parameter_items_listview, parameterList);

                        lv.setAdapter(clad);

                        dialog.setContentView(view);
                        dialog.show();
                    }else{
                        // no required parameter
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ActionsActivity.this);
                        builder1.setMessage("Add action to queue?");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        new AddActionToQueue(id_robot, id_action, "").execute();
                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    private class AddActionToQueue extends AsyncTask<String, String, Boolean>{

        public String idRobot, idAction, value;

        public AddActionToQueue(String idRobot, String idAction, String value){
            this.idRobot = idRobot;
            this.idAction = idAction;
            this.value = value;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.insertActionQueue(idRobot, idAction, value);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(ActionsActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ActionsActivity.this, "Added", Toast.LENGTH_SHORT).show();
                        // if queue added has parameters values
                        // ->
                        // then add parameter values to robot_action_queue_param

                    }

                }
            });
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ActionsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
