package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotSetting;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by tudur on 08-Aug-19.
 */

public class RobotDetailSettingsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    String id_setting = "";
    String id_robot = "";
    String name_robot = "";

    private TextView mIdView;
    private TextView mRobotView;
    private TextView mNameView;
    private TextView mValueView;
    private TextView mDescriptionView;

    Button btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_settings);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        //id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        getSupportActionBar().setTitle("Settings -> " + name_robot);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_setting = extras.getString("idsetting");
            id_robot = extras.getString("idrobot");
        }

        mIdView = (TextView) findViewById(R.id.txtId);
        mRobotView = (TextView) findViewById(R.id.txtRobot);
        mNameView = (TextView) findViewById(R.id.txtName);
        mValueView = (TextView) findViewById(R.id.txtValue);
        mDescriptionView = (TextView) findViewById(R.id.txtDescription);

        btnEdit = (Button) findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateRobotSettingActivity.class);
                intent.putExtra("idsetting", mIdView.getText().toString());
                intent.putExtra("idrobot", mRobotView.getText().toString());
                intent.putExtra("action", "update");
                startActivity(intent);
            }
        });

        new LoadRobotItem(id_setting).execute();

    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadRobotItem(id_setting).execute();
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            final cRobotSetting robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotSetting(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mIdView.setText(robot.id);
                            mRobotView.setText(robot.robot);
                            mNameView.setText(robot.name);
                            mValueView.setText(robot.value);
                            mDescriptionView.setText(robot.description);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotDetailSettingsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
