package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotResults;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tudur on 03-Aug-19.
 */

public class RobotResultsActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> robotList;

    ProgressDialog progressDialog;

    ListView listView;

    String id_action = "";
    String id_robot = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_results);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_robot = extras.getString("idrobot");
            //id_action = extras.getString("idaction");
        }

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                final String rid = ((TextView) itemClicked.findViewById(R.id.textviewId)).getText().toString();
            }
        });

        new LoadRobots().execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadRobots().execute();
        }
    }

    class LoadRobots extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            robotList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cRobotResults> myRobot = MyProvider.getRobotResults("");
            for(cRobotResults entry : myRobot)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("action",  entry.action);
                map.put("datetime",  entry.datetime);
                map.put("result",  entry.result);
                map.put("probability",  entry.probability);

                // adding HashList to ArrayList
                robotList.add(map);
            }
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (RobotResultsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(robotList.size() > 0){
                        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                        if(tabletSize){
                            // tablet

                            //RobotsListViewAdapter adapter = null;
                            //adapter = new RobotsListViewAdapter(RobotSettingsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);

                            ListAdapter adapter = null;
                            adapter = new SimpleAdapter(RobotResultsActivity.this, robotList,
                                    R.layout.activity_robot_results_items_listview,
                                    new String[]{"id", "datetime", "probability"},
                                    new int[]{R.id.textviewId, R.id.textviewDatetime, R.id.textviewProbability});
                            listView.setAdapter(adapter);

                        }else {
                            // phone

                            //RobotsListViewAdapter adapter = null;
                            //adapter = new RobotsListViewAdapter(RobotSettingsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);

                            ListAdapter adapter = null;
                            adapter = new SimpleAdapter(RobotResultsActivity.this, robotList,
                                    R.layout.activity_robot_results_items_listview,
                                    new String[]{"id", "datetime", "probability"},
                                    new int[]{R.id.textviewId, R.id.textviewDatetime, R.id.textviewProbability});
                            listView.setAdapter(adapter);
                        }
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotResultsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
