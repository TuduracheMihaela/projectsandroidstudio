package com.sharethatdata.kobotcontrol;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol.adapter.RobotsListViewAdapter;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cIpRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cStatusRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by tudur on 05-Feb-19.
 */

public class RobotsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> robotList;

    ProgressDialog progressDialog;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robots);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new LoadRobots().execute();
                pullToRefresh.setRefreshing(false);
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String rid = ((TextView) itemClicked.findViewById(R.id.textviewId)).getText().toString();

                MyGlobals Globals = ((MyGlobals)getApplication());
                Globals.setValue("rid", rid);

                new LoadRobotIp(rid).execute();

                // start detail activity
                //Intent intent = new Intent(getApplicationContext(), RobotsDetailActivity.class);
                //startActivity(intent);

            }

        });

        new LoadRobots().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    class LoadRobotIp extends AsyncTask<Object, Object, Boolean> {

        String id;
        String local_ip;

        public LoadRobotIp(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected Boolean doInBackground(Object... args) {

            final cIpRobot robot = MyProvider.getIpRobot(id);
            local_ip = robot.local_ip;

            //WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
            //String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

            boolean connect_to_ip = false;

            try{
                // test functions
                String x = Utils.getMACAddress("wlan0");
                String y = Utils.getMACAddress("eth0");
                String z = Utils.getIPAddress(true); // IPv4
                String b = Utils.getIPAddress(false); // IPv6

                String[] splittedArrayLocalIp = z.split("\\.");
                String firstThreeSegmentsLocalIp = splittedArrayLocalIp [0] + "." + splittedArrayLocalIp [1] + "." + splittedArrayLocalIp [2] + ".";

                String[] splittedArrayDatabaseIp = robot.local_ip.split("\\.");
                String firstThreeSegmentsDatabaseIp = splittedArrayDatabaseIp [0] + "." + splittedArrayDatabaseIp [1] + "." + splittedArrayDatabaseIp [2] + ".";

                if(firstThreeSegmentsLocalIp.equals(firstThreeSegmentsDatabaseIp)){
                    connect_to_ip = true;
                }
            }catch (Exception e){
                e.printStackTrace();
            }



            return connect_to_ip;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result){
                // true - connect to ip

                MyGlobals Globals = ((MyGlobals)getApplication());
                Globals.setValue("local_ip", "http://" + local_ip );

                Intent intent = new Intent(getApplicationContext(), RobotsDetailActivity.class);
                startActivity(intent);
            }else {
                MyGlobals Globals = ((MyGlobals)getApplication());
                Globals.setValue("local_ip", "" );

                Intent intent = new Intent(getApplicationContext(), RobotsDetailActivity.class);
                startActivity(intent);
            }


            hideProgressDialog();
        }
    }

    class LoadRobots extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            robotList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            //List<cRobots> myRobot = MyProvider.getRobots();
            List<cStatusRobot> myRobot = MyProvider.getStatusRobots();
            for(cStatusRobot entry : myRobot)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);
                map.put("datetime",  entry.datetime);
                map.put("status",  entry.status);
                map.put("status_text",  entry.status_text);
                map.put("orders",  entry.orders);
                map.put("errors",  entry.errors);
                map.put("last_error",  entry.last_error);
                map.put("online",  entry.online);

                //if(entry.status.equals("0")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_gray));
                //if(entry.status.equals("1")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_green));
                //if(entry.status.equals("2")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_yellow));
                //if(entry.status.equals("3")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_red));

                // adding HashList to ArrayList
                robotList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (RobotsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(robotList.size() > 0){


                        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                        if(tabletSize){
                            // tablet
                            RobotsListViewAdapter adapter = null;

                            adapter = new RobotsListViewAdapter(RobotsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);
                            listView.setAdapter(adapter);

                        }else {
                            // phone

                            RobotsListViewAdapter adapter = null;

                            adapter = new RobotsListViewAdapter(RobotsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);
                            listView.setAdapter(adapter);

                            /*ListAdapter adapter = null;
                            adapter = new SimpleAdapter(RobotsActivity.this, robotList,
                                    R.layout.activity_robots_items_listview,
                                    new String[]{"id", "name", "description"},
                                    new int[]{R.id.textviewId, R.id.textviewName, R.id.textviewDescription});
                            listView.setAdapter(adapter);*/
                        }
                    }
                }
            });

            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
