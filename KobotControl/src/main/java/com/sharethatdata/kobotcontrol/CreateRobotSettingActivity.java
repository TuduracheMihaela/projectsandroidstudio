package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotSetting;

/**
 * Created by tudur on 31-Jul-19.
 */

public class CreateRobotSettingActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    private EditText mNameView;
    private EditText mValueView;
    private EditText mDescriptionView;

    String id_robot = "";
    String id_setting = "";
    String action = "";

    /*String value = "";
    String categorie = "";
    String name = "";
    String description = "";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_settings_create);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_robot = extras.getString("idrobot");
            id_setting = extras.getString("idsetting");
            action = extras.getString("action");

            /*value = extras.getString("value");
            categorie = extras.getString("categorie");
            name = extras.getString("name");
            description = extras.getString("description");*/
        }

        mNameView = (EditText) findViewById(R.id.txtname);
        mValueView = (EditText) findViewById(R.id.txtvalue);
        mDescriptionView = (EditText) findViewById(R.id.txtdescription);

        Button create_button = (Button) findViewById(R.id.create_robot_setting_button);

        if(action.equals("create")){
            // create
            create_button.setText(getString(R.string.create_button));
        }else if(action.equals("update")){
            // update
            create_button.setText(getString(R.string.update_button));
            /*mNameView.setText(name);
            mDescriptionView.setText(description);
            mValueView.setText(value);*/
            new LoadRobotItem(id_setting).execute();
        }


        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(action.equals("create")){
                    // create
                    new CreateRobotSetting(id_robot, mNameView.getText().toString(),
                            mValueView.getText().toString(), mDescriptionView.getText().toString()).execute();
                }else if(action.equals("update")){
                    // update
                    new UpdateRobotSetting(id_robot, id_setting, mNameView.getText().toString(),
                            mValueView.getText().toString(), mDescriptionView.getText().toString()).execute();
                }
            }
        });

    }


    private class CreateRobotSetting extends AsyncTask<String, String, Boolean> {

        public String idRobot, name, value, description;

        public CreateRobotSetting(String idRobot, String name, String value, String description){
            this.idRobot = idRobot;
            this.name = name;
            this.value = value;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.createRobotSetting(idRobot, name, value, description);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotSettingActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotSettingActivity.this, "Added", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    private class UpdateRobotSetting extends AsyncTask<String, String, Boolean> {

        public String idRobot, idSetting, name, value, description;

        public UpdateRobotSetting(String idRobot, String idSetting, String name, String value, String description){
            this.idRobot = idRobot;
            this.idSetting = idSetting;
            this.name = name;
            this.value = value;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.updateRobotSetting(idSetting, idRobot, name, value, description);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotSettingActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotSettingActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobotSetting robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotSetting(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mNameView.setText(robot.name);
                            mDescriptionView.setText(robot.description);
                            mValueView.setText(robot.value);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateRobotSettingActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
