package com.sharethatdata.kobotcontrol;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cOutputsRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by tudur on 07-Aug-19.
 */

public class RobotOutputsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> outputList;

    ProgressDialog progressDialog;

    ListView listView;

    String id_robot = "";
    String name_robot = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outputs);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        //Initialize the timer

        getSupportActionBar().setTitle("Outputs -> " + name_robot);

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateRobotOutputActivity.class);
                intent.putExtra("idrobot",id_robot);
                intent.putExtra("idoutput","");
                intent.putExtra("action","create");
                startActivity(intent);
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String rid = ((TextView) itemClicked.findViewById(R.id.textViewId)).getText().toString();

                Intent intent = new Intent(getApplicationContext(), RobotDetailOutputsActivity.class);
                intent.putExtra("idoutput",rid);
                startActivity(intent);

            }

        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                final String rid = ((TextView) itemClicked.findViewById(R.id.textViewId)).getText().toString();

                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(RobotOutputsActivity.this);
                builder.setTitle("Action delete");
                builder.setMessage("You are going to delete the item selected");

                // add the buttons
                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // setup the alert builder
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(RobotOutputsActivity.this);
                        builder1.setTitle("Notice");
                        builder1.setMessage("Are you sure?");
                        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(RobotOutputsActivity.this, "Delete", Toast.LENGTH_SHORT).show();
                                new DeleteRobot(rid).execute();
                            }
                        });
                        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog1 = builder1.create();
                        dialog1.show();
                    }
                });
                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });

        new LoadOutputs().execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadOutputs().execute();
        }
    }


    class LoadOutputs extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            outputList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cOutputsRobot> mySensor = MyProvider.getOutputsRobot();
            for(cOutputsRobot entry : mySensor)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("value",  entry.value);
                map.put("datetime",  entry.datetime);
                map.put("description",  entry.description);

                // adding HashList to ArrayList
                outputList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (RobotOutputsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(outputList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(RobotOutputsActivity.this, outputList,
                                R.layout.activity_outputs_items_listview,
                                new String[]{"id", "name", "value", "datetime", "description"},
                                new int[]{R.id.textViewId, R.id.textViewName, R.id.textViewValue, R.id.textViewDatetime, R.id.textViewDescription});
                        listView.setAdapter(adapter);
                    }
                }
            });

            hideProgressDialog();
        }
    }

    private class DeleteRobot extends AsyncTask<String, String, Boolean> {

        public String id;

        public DeleteRobot(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.deleteRobotOutput(id);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(RobotOutputsActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(RobotOutputsActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                        //Globals.setValue("refresh","refresh");
                        //finish();
                        new LoadOutputs().execute();
                    }
                }
            });
        }
    }

    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    protected void onDestroy() {
        Log.d("onDestroy","onDestroy");
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(final String msg)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    //if(!((Activity) getApplicationContext()).isFinishing())
                    if(!RobotOutputsActivity.this.isFinishing())
                    {
                        //show dialog

                        // check for existing progressDialog
                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(RobotOutputsActivity.this); // create a progress Dialog
                            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setMessage(msg);
                        }
                        progressDialog.show(); // now display it.
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }


}
