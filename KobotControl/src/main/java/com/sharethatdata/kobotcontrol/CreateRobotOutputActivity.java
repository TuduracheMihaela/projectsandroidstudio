package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotOutput;

/**
 * Created by tudur on 07-Aug-19.
 */

public class CreateRobotOutputActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    private EditText mNameView;
    private EditText mValueView;
    private EditText mDescriptionView;

    String id_robot = "";
    String id_output = "";
    String action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_outputs_create);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_robot = extras.getString("idrobot");
            id_output = extras.getString("idoutput");
            action = extras.getString("action");
        }

        mNameView = (EditText) findViewById(R.id.txtname);
        mValueView = (EditText) findViewById(R.id.txtvalue);
        mDescriptionView = (EditText) findViewById(R.id.txtdescription);

        Button create_button = (Button) findViewById(R.id.create_robot_output_button);

        if(action.equals("create")){
            // create
            create_button.setText(getString(R.string.create_button));
        }else if(action.equals("update")){
            // update
            create_button.setText(getString(R.string.update_button));
            new LoadRobotItem(id_output).execute();
        }


        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(action.equals("create")){
                    // create
                    new CreateRobotOutput(id_robot, mNameView.getText().toString(),
                            mValueView.getText().toString(), mDescriptionView.getText().toString()).execute();
                }else if(action.equals("update")){
                    // update
                    new UpdateRobotOutput(id_robot, id_output, mNameView.getText().toString(),
                            mValueView.getText().toString(), mDescriptionView.getText().toString()).execute();
                }
            }
        });

    }


    private class CreateRobotOutput extends AsyncTask<String, String, Boolean> {

        public String idRobot, name, value, description;

        public CreateRobotOutput(String idRobot, String name, String value, String description){
            this.idRobot = idRobot;
            this.name = name;
            this.value = value;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.createRobotOutput(idRobot, name, value, description);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotOutputActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotOutputActivity.this, "Added", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    private class UpdateRobotOutput extends AsyncTask<String, String, Boolean> {

        public String idRobot, idOutput, name, value, description;

        public UpdateRobotOutput(String idRobot, String idOutput, String name, String value, String description){
            this.idRobot = idRobot;
            this.idOutput = idOutput;
            this.name = name;
            this.value = value;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.updateRobotOutput(idOutput, name, value, description);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotOutputActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotOutputActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobotOutput robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotOutput(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mNameView.setText(robot.name);
                            mDescriptionView.setText(robot.description);
                            mValueView.setText(robot.value);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateRobotOutputActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
