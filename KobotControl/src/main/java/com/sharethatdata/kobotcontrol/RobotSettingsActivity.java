package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AlertDialog;

/**
 * Created by tudur on 31-Jul-19.
 */

public class RobotSettingsActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ArrayList<HashMap<String, String>> robotList;

    ProgressDialog progressDialog;

    ListView listView;

    String id_robot = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_settings);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_robot = extras.getString("idrobot");
        }

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateRobotSettingActivity.class);
                intent.putExtra("idrobot",id_robot);
                intent.putExtra("idsetting","");
                intent.putExtra("action","create");
                startActivity(intent);
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String rid = ((TextView) itemClicked.findViewById(R.id.textviewId)).getText().toString();

                Intent intent = new Intent(getApplicationContext(), RobotDetailSettingsActivity.class);
                intent.putExtra("idsetting",rid);
                startActivity(intent);

            }

        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                final String rid = ((TextView) itemClicked.findViewById(R.id.textviewId)).getText().toString();

                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(RobotSettingsActivity.this);
                builder.setTitle("Action delete");
                builder.setMessage("You are going to delete the item selected");

                // add the buttons
                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // setup the alert builder
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(RobotSettingsActivity.this);
                        builder1.setTitle("Notice");
                        builder1.setMessage("Are you sure?");
                        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(RobotSettingsActivity.this, "Delete", Toast.LENGTH_SHORT).show();
                                new DeleteRobot(rid).execute();
                            }
                        });
                        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog1 = builder1.create();
                        dialog1.show();
                    }
                });
                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                final String rid = ((TextView) itemClicked.findViewById(R.id.textviewId)).getText().toString();
                final String vid = ((TextView) itemClicked.findViewById(R.id.textviewValue)).getText().toString();
                final String cid = ((TextView) itemClicked.findViewById(R.id.textviewCategorie)).getText().toString();
                final String nid = ((TextView) itemClicked.findViewById(R.id.textviewName)).getText().toString();
                final String did = ((TextView) itemClicked.findViewById(R.id.textviewDescription)).getText().toString();

                //MyGlobals Globals = ((MyGlobals)getApplication());
                //Globals.setValue("rid", rid);

                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(RobotSettingsActivity.this);
                builder.setTitle("Action");
                builder.setMessage("Select an action:");

                // add the buttons
                builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), CreateRobotSettingActivity.class);
                        intent.putExtra("idrobot",id_robot);
                        intent.putExtra("idsetting",rid);
                        intent.putExtra("value",vid);
                        intent.putExtra("categorie",cid);
                        intent.putExtra("name",nid);
                        intent.putExtra("description",did);
                        intent.putExtra("action","update");
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // setup the alert builder
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(RobotSettingsActivity.this);
                        builder1.setTitle("Notice");
                        builder1.setMessage("Are you sure?");
                        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(RobotSettingsActivity.this, "Delete", Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog1 = builder1.create();
                        dialog1.show();
                    }
                });
                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }

        });*/

        new LoadRobots(id_robot).execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadRobots(id_robot).execute();
        }
    }

    class LoadRobots extends AsyncTask<String, String, String> {
        String id_robot;

        public LoadRobots(String id_robot) {
            this.id_robot = id_robot;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            robotList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            List<cRobotSettings> myRobot = MyProvider.getRobotSettings(id_robot);
            for(cRobotSettings entry : myRobot)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);
                map.put("categorie",  entry.categorie);
                map.put("value",  entry.value);

                // adding HashList to ArrayList
                robotList.add(map);
            }
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (RobotSettingsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(robotList.size() > 0){
                        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                        if(tabletSize){
                            // tablet

                            //RobotsListViewAdapter adapter = null;
                            //adapter = new RobotsListViewAdapter(RobotSettingsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);

                            ListAdapter adapter = null;
                            adapter = new SimpleAdapter(RobotSettingsActivity.this, robotList,
                                    R.layout.activity_robot_settings_items_listview,
                                    new String[]{"id", "name", "description", "value", "categorie"},
                                    new int[]{R.id.textviewId, R.id.textviewName, R.id.textviewDescription,
                                    R.id.textviewValue, R.id.textviewCategorie});
                            listView.setAdapter(adapter);

                        }else {
                            // phone

                            //RobotsListViewAdapter adapter = null;
                            //adapter = new RobotsListViewAdapter(RobotSettingsActivity.this, R.layout.activity_robots_items_listview, robotList, tabletSize);

                            ListAdapter adapter = null;
                            adapter = new SimpleAdapter(RobotSettingsActivity.this, robotList,
                                    R.layout.activity_robot_settings_items_listview,
                                    new String[]{"id", "name", "description", "value", "categorie"},
                                    new int[]{R.id.textviewId, R.id.textviewName, R.id.textviewDescription,
                                            R.id.textviewValue, R.id.textviewCategorie});
                            listView.setAdapter(adapter);
                        }
                    }
                }
            });
            hideProgressDialog();
        }
    }

    private class DeleteRobot extends AsyncTask<String, String, Boolean> {

        public String id;

        public DeleteRobot(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.deleteRobotSetting(id);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(RobotSettingsActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(RobotSettingsActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                        //Globals.setValue("refresh","refresh");
                        //finish();
                        new LoadRobots(id_robot).execute();
                    }
                }
            });
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotSettingsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
