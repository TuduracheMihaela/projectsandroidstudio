package com.sharethatdata.kobotcontrol;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotPosition;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by tudur on 08-Aug-19.
 */

public class RobotDetailPositionsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    String id_position = "";
    String id_robot = "";
    String name_robot = "";

    private TextView mIdView;
    private TextView mNameView;
    private TextView mDescriptionView;
    private TextView mXView;
    private TextView mYView;
    private TextView mZView;
    private TextView mRXView;
    private TextView mRYView;
    private TextView mRZView;
    private TextView mRView;

    Button btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_positions);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        //id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        getSupportActionBar().setTitle("Positions -> " + name_robot);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_position = extras.getString("idposition");
            id_robot = extras.getString("idrobot");
        }

        mIdView = (TextView) findViewById(R.id.txtId);
        mNameView = (TextView) findViewById(R.id.txtName);
        mDescriptionView = (TextView) findViewById(R.id.txtDescription);
        mXView = (TextView) findViewById(R.id.txtX);
        mYView = (TextView) findViewById(R.id.txtY);
        mZView = (TextView) findViewById(R.id.txtZ);
        mRXView = (TextView) findViewById(R.id.txtRX);
        mRYView = (TextView) findViewById(R.id.txtRY);
        mRZView = (TextView) findViewById(R.id.txtRZ);
        mRView = (TextView) findViewById(R.id.txtR);


        btnEdit = (Button) findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateRobotPositionActivity.class);
                intent.putExtra("idposition", mIdView.getText().toString());
                intent.putExtra("idrobot", id_robot);
                intent.putExtra("action", "update");
                startActivity(intent);
            }
        });

        new LoadRobotItem(id_position).execute();

    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadRobotItem(id_position).execute();
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobotPosition robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotPosition(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mIdView.setText(robot.id);
                            mNameView.setText(robot.name);
                            mDescriptionView.setText(robot.description);
                            mXView.setText(robot.x);
                            mYView.setText(robot.y);
                            mZView.setText(robot.z);
                            mRXView.setText(robot.rx);
                            mRYView.setText(robot.ry);
                            mRZView.setText(robot.rz);
                            mRView.setText(robot.r);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotDetailPositionsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
