package com.sharethatdata.kobotcontrol;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotOutput;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by tudur on 07-Aug-19.
 */

public class RobotDetailOutputsActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    String id_output = "";
    String name_robot = "";

    private TextView mIdView;
    private TextView mNameView;
    private TextView mValueView;
    private TextView mDatetimeView;
    private TextView mDescriptionView;

    Button btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_outputs);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        //id_robot = Globals.getValue("rid");
        name_robot = Globals.getValue("robot_name");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        getSupportActionBar().setTitle("Outputs -> " + name_robot);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_output = extras.getString("idoutput");
        }

        mIdView = (TextView) findViewById(R.id.txtId);
        mNameView = (TextView) findViewById(R.id.txtName);
        mValueView = (TextView) findViewById(R.id.txtValue);
        mDatetimeView = (TextView) findViewById(R.id.txtDatetime);
        mDescriptionView = (TextView) findViewById(R.id.txtDescription);

        btnEdit = (Button) findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateRobotOutputActivity.class);
                intent.putExtra("idoutput", mIdView.getText().toString());
                intent.putExtra("action", "update");
                startActivity(intent);
            }
        });

        new LoadRobotItem(id_output).execute();

    }

    @Override
    public void onResume() {
        super.onResume();

        String update = Globals.getValue("refresh");
        if(update != ""){
            Globals.setValue("refresh", "");
            new LoadRobotItem(id_output).execute();
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            final cRobotOutput robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotOutput(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mIdView.setText(robot.id);
                            mNameView.setText(robot.name);
                            mValueView.setText(robot.value);
                            mDescriptionView.setText(robot.description);
                            mDatetimeView.setText(robot.datetime);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RobotDetailOutputsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
