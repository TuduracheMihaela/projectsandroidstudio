package com.sharethatdata.kobotcontrol.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import androidx.appcompat.widget.SwitchCompat;

/**
 * Created by tudur on 07-Mar-19.
 */

public class SwitchCompatCustom extends SwitchCompat {
    private boolean mIgnoreCheckedChange = false;

    public SwitchCompatCustom(Context context) {
        super(context);
    }

    public SwitchCompatCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwitchCompatCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setOnCheckedChangeListener(final OnCheckedChangeListener listener) {
        super.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mIgnoreCheckedChange) {
                    return;
                }
                listener.onCheckedChanged(buttonView, isChecked);
            }
        });
    }

    public void setChecked(boolean checked, boolean notify) {
        mIgnoreCheckedChange = !notify;
        setChecked(checked);
        mIgnoreCheckedChange = false;
    }

}
