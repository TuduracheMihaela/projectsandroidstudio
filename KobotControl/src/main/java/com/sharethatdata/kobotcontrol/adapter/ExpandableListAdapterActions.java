package com.sharethatdata.kobotcontrol.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol.ActionsActivity;
import com.sharethatdata.kobotcontrol.R;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cActionsRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

/**
 * Created by tudur on 11-Feb-19.
 */

public class ExpandableListAdapterActions extends BaseExpandableListAdapter {
    /*private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;

    public ExpandableListAdapterActions(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        //super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }*/

    private Context _context;
    private ArrayList<String> header; // header titles
    // Child data in format of header title, child title
    private HashMap<String, List<cActionsRobot>> child;
    private Timer t;

    public ExpandableListAdapterActions(Context context, ArrayList<String> listDataHeader,
                                        HashMap<String, List<cActionsRobot>> listChildData, Timer t) {
        this._context = context;
        this.header = listDataHeader;
        this.child = listChildData;
        this.t = t;
    }

    final class ViewHolder {
        public TextView id, name, description;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        // This will return the child
        return this.child.get(this.header.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        // Getting child text
        final cActionsRobot childText = (cActionsRobot) getChild(groupPosition, childPosition);

        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.activity_robot_actions_items_listview, parent, false);
        }

        TextView child_id = (TextView) convertView.findViewById(R.id.textViewIdAction);
        child_id.setText(childText.id);
        TextView child_name = (TextView) convertView.findViewById(R.id.textViewNameAction);
        child_name.setText(childText.name);
        TextView child_description = (TextView) convertView.findViewById(R.id.textViewDescriptionAction);
        child_description.setText(childText.description);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        // return children count
        return this.child.get(this.header.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        // Get header position
        return this.header.get(groupPosition);
    }

    @Override
    public int getGroupCount() {

        // Get header size
        return this.header.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // Getting header title
        String headerTitle = (String) getGroup(groupPosition);

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header, parent, false);
        }

        final TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(headerTitle);

        final Button btn = (Button) convertView.findViewById(R.id.btn);
        btn.setText(headerTitle);

        if (isExpanded) {
            // If group is expanded then change the text into bold and change the
            // icon
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_up, 0, 0, 0);

        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon
            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_down, 0, 0, 0);
        }

        //Button button = (Button) convertView.findViewById(R.id.btn);
        //button.setFocusable(false);   /// THIS IS THE SOLUTION

        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //MyGlobals Globals = ((MyGlobals)getApplication());
                //Globals.setValue("rid", rid);

                t.cancel();

                // start actions activity
                Intent intent = new Intent(_context, ActionsActivity.class);
                _context.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
