package com.sharethatdata.kobotcontrol.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol.R;
import com.sharethatdata.kobotcontrol.SensorsActivity;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cSensorsRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

/**
 * Created by tudur on 11-Feb-19.
 */

public class ExpandableListAdapterSensors extends BaseExpandableListAdapter {
    private Context _context;
    private ArrayList<String> header; // header titles
    // Child data in format of header title, child title
    private HashMap<String, List<cSensorsRobot>> child;
    private Timer t;

    public ExpandableListAdapterSensors(Context context, ArrayList<String> listDataHeader,
                                        HashMap<String, List<cSensorsRobot>> listChildData, Timer t) {
        this._context = context;
        this.header = listDataHeader;
        this.child = listChildData;
        this.t = t;
    }

    final class ViewHolder {
        public TextView id, name, value, description;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        // This will return the child
        return this.child.get(this.header.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        // Getting child text
        final cSensorsRobot childText = (cSensorsRobot) getChild(groupPosition, childPosition);

        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.activity_robot_sensors_items_listview, parent, false);
        }

        TextView child_id = (TextView) convertView.findViewById(R.id.textViewIdSensor);
        child_id.setText(childText.id);
        TextView child_name = (TextView) convertView.findViewById(R.id.textViewNameSensor);
        child_name.setText(childText.name);
        TextView child_value = (TextView) convertView.findViewById(R.id.textViewValueSensor);
        child_value.setText(childText.value);
        TextView child_description = (TextView) convertView.findViewById(R.id.textViewDescriptionSensor);
        child_description.setText(childText.description);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        // return children count
        return this.child.get(this.header.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        // Get header position
        return this.header.get(groupPosition);
    }

    @Override
    public int getGroupCount() {

        // Get header size
        return this.header.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // Getting header title
        String headerTitle = (String) getGroup(groupPosition);

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header, parent, false);
        }

        TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(headerTitle);

        final Button btn = (Button) convertView.findViewById(R.id.btn);
        btn.setText(headerTitle);

        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_up, 0, 0, 0);

        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon
            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_down, 0, 0, 0);
        }

        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //MyGlobals Globals = ((MyGlobals)getApplication());
                //Globals.setValue("rid", rid);

                t.cancel();

                // start detail activity
                Intent intent = new Intent(_context, SensorsActivity.class);
                _context.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
