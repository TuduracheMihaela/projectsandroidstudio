package com.sharethatdata.kobotcontrol.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tudur on 21-Mar-19.
 */

public class CustomListAdapterDialog extends ArrayAdapter<HashMap<String, String>> {

    private ArrayList<HashMap<String, String>> adapter = new ArrayList<HashMap<String, String>>();
    private final int layoutResourceId;
    private final Activity context;

    public CustomListAdapterDialog(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter) {
        super(context, layoutResourceId, adapter);
        this.adapter = adapter;
        this.layoutResourceId = layoutResourceId;
        this.context = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String> hashmap_Current;
        View view = null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            view = layoutInflater.inflate(layoutResourceId, null);
            holder = new ViewHolder();

            holder.id = (TextView) view.findViewById(R.id.textViewId);
            holder.name = (TextView) view.findViewById(R.id.textViewName);
            holder.description = (TextView) view.findViewById(R.id.textViewDescription);
            //holder.value = (TextView) view.findViewById(R.id.textViewValue);
            holder.value = (EditText) view.findViewById(R.id.etValue);


            view.setTag(holder);
        } else {

            holder = (ViewHolder) view.getTag();
        }

        hashmap_Current = new HashMap<String, String>();
        hashmap_Current = adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
        holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
        holder.description.setText(Html.fromHtml(hashmap_Current.get("description")));
        holder.value.setText(Html.fromHtml(hashmap_Current.get("required")));


        return view;
    }

    final class ViewHolder {
        public TextView id, name, description;
        public EditText value;
    }

}
