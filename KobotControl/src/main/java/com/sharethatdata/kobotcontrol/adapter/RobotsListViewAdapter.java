package com.sharethatdata.kobotcontrol.adapter;

import android.app.Activity;
import android.content.res.Configuration;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sharethatdata.kobotcontrol.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tudur on 20-Mar-19.
 */

public class RobotsListViewAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter = new ArrayList<HashMap<String, String>>();
    private final Activity context;
    private final int layoutResourceId;
    boolean isTablet;

    public RobotsListViewAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter, boolean isTablet) {
        super(context, layoutResourceId, adapter);
        this.adapter = adapter;
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.isTablet = isTablet;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String> hashmap_Current;
        View view = null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            view = layoutInflater.inflate(layoutResourceId, null);
            holder = new ViewHolder();

            holder.id = (TextView) view.findViewById(R.id.textviewId);
            holder.name = (TextView) view.findViewById(R.id.textviewName);
            holder.description = (TextView) view.findViewById(R.id.textviewDescription);
            holder.squareView = (View) view.findViewById(R.id.mySquareView);
            holder.linearLayoutSquareView = (LinearLayout) view.findViewById(R.id.linearLayoutSquareView);
            holder.linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

            if(isTablet){
                // tablet
                holder.orders = (TextView) view.findViewById(R.id.textviewOrder);
                holder.errors = (TextView) view.findViewById(R.id.textviewError);
                holder.last_error = (TextView) view.findViewById(R.id.textviewLastError);
            }

            view.setTag(holder);
        } else {

            holder = (ViewHolder) view.getTag();
        }

        hashmap_Current = new HashMap<String, String>();
        hashmap_Current = adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
        holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
        holder.description.setText(Html.fromHtml(hashmap_Current.get("description")));

        if(isTablet){
            // tablet
            holder.orders.setText(Html.fromHtml(hashmap_Current.get("orders")));
            holder.errors.setText(Html.fromHtml(hashmap_Current.get("errors")));
            holder.last_error.setText(Html.fromHtml(hashmap_Current.get("last_error")));
        }

        //if(entry.status.equals("0")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_gray));
        //if(entry.status.equals("1")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_green));
        //if(entry.status.equals("2")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_yellow));
        //if(entry.status.equals("3")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_red));

        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "status")
            {
                try {
                    String myRow = entry.getValue().toString();

                    if(myRow.contains("0")){
                        // 0 = gray
                        holder.squareView.setBackground(context.getResources().getDrawable(R.drawable.square_gray));
                    }
                    if(myRow.contains("1")){
                        // 1 = green
                        holder.squareView.setBackground(context.getResources().getDrawable(R.drawable.square_green));
                    }
                    if(myRow.equals("2")){
                        // 2 = yellow
                        holder.squareView.setBackground(context.getResources().getDrawable(R.drawable.square_yellow));
                    }
                    if(myRow.equals("3")){
                        // 3 = red
                        holder.squareView.setBackground(context.getResources().getDrawable(R.drawable.square_red));
                    }
                } catch(NumberFormatException nfe) {

                }
            }
        }

        return view;
    }

    final class ViewHolder {
        public TextView id, name, description, datetime, status, status_text, orders, errors, last_error, online;
        public View squareView;
        public LinearLayout linearLayoutSquareView, linearLayout;
    }

}