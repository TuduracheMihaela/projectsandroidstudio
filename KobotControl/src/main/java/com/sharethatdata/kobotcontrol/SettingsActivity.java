package com.sharethatdata.kobotcontrol;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	
	static final String PREFS_NAME = "defaults";    
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        // Display the fragment as the main content.
        /*if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();*/

        getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
        addPreferencesFromResource(R.xml.settings);

        findPreference("prefMonitorStatus").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_monitor_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            return true;
                        }
                    }

                });

        findPreference("prefMonitorSensor").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_monitor_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            return true;
                        }
                    }
                });


    }

    /*public static class PrefFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            
            getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
            addPreferencesFromResource(R.xml.settings);
        }
    }*/
}

