package com.sharethatdata.kobotcontrol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotPosition;

/**
 * Created by tudur on 03-Aug-19.
 */

public class CreateRobotPositionActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    ProgressDialog progressDialog;

    private EditText mNameView;
    private EditText mDescriptionView;
    private EditText mXView;
    private EditText mYView;
    private EditText mZView;
    private EditText mRXView;
    private EditText mRYView;
    private EditText mRZView;
    private EditText mRView;

    String id_robot = "";
    String id_position = "";
    String action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_positions_create);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_robot = extras.getString("idrobot");
            id_position = extras.getString("idposition");
            action = extras.getString("action");
        }

        mNameView = (EditText) findViewById(R.id.txtname);
        mDescriptionView = (EditText) findViewById(R.id.txtdescription);
        mXView = (EditText) findViewById(R.id.txtx);
        mYView = (EditText) findViewById(R.id.txty);
        mZView = (EditText) findViewById(R.id.txtz);
        mRXView = (EditText) findViewById(R.id.txtrx);
        mRYView = (EditText) findViewById(R.id.txtry);
        mRZView = (EditText) findViewById(R.id.txtrz);
        mRView = (EditText) findViewById(R.id.txtr);

        Button create_button = (Button) findViewById(R.id.create_robot_setting_button);

        if(action.equals("create")){
            // create
            create_button.setText(getString(R.string.create_button));
        }else if(action.equals("update")){
            // update
            create_button.setText(getString(R.string.update_button));
            new LoadRobotItem(id_position).execute();
        }


        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(action.equals("create")){
                    // create
                    new CreateRobot(id_robot, mNameView.getText().toString(), mDescriptionView.getText().toString(),
                            mXView.getText().toString(), mYView.getText().toString(),mZView.getText().toString(),
                            mRXView.getText().toString(), mRYView.getText().toString(), mRZView.getText().toString(),
                            mRView.getText().toString()).execute();
                }else if(action.equals("update")){
                    // update
                    new UpdateRobot(id_robot, id_position, mNameView.getText().toString(), mDescriptionView.getText().toString(),
                            mXView.getText().toString(), mYView.getText().toString(),mZView.getText().toString(),
                            mRXView.getText().toString(), mRYView.getText().toString(), mRZView.getText().toString(),
                            mRView.getText().toString()).execute();
                }
            }
        });

    }


    private class CreateRobot extends AsyncTask<String, String, Boolean> {

        public String idRobot, name, description, x, y, z, rx, ry, rz, r;

        public CreateRobot(String idRobot, String name, String description, String x, String y, String z,
                           String rx, String ry, String rz, String r){
            this.idRobot = idRobot;
            this.name = name;
            this.description = description;
            this.x = x;
            this.y = y;
            this.z = z;
            this.rx = rx;
            this.ry = ry;
            this.rz = rz;
            this.r = r;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.createRobotPosition(idRobot, name, description, x, y, z, rx, ry, rz, r);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotPositionActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotPositionActivity.this, "Added", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    private class UpdateRobot extends AsyncTask<String, String, Boolean> {

        public String idRobot, idSetting, name, description, x, y, z, rx, ry, rz, r;

        public UpdateRobot(String idRobot, String idSetting, String name, String description,
                           String x, String y, String z, String rx, String ry, String rz, String r){
            this.idRobot = idRobot;
            this.idSetting = idSetting;
            this.name = name;
            this.description = description;
            this.x = x;
            this.y = y;
            this.z = z;
            this.rx = rx;
            this.ry = ry;
            this.rz = rz;
            this.r = r;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);

            boolean suc = MyProvider.updateRobotPosition(idSetting, idRobot, name, description, x, y, z, rx, ry, rz, r);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {
                    hideProgressDialog();

                    if (success == false)
                    {
                        Toast.makeText(CreateRobotPositionActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateRobotPositionActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                        Globals.setValue("refresh","refresh");
                        finish();
                    }
                }
            });
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobotPosition robot;

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            robot = MyProvider.getRobotPosition(id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (robot != null)
                    {
                        if (robot.id != "")
                        {
                            mNameView.setText(robot.name);
                            mDescriptionView.setText(robot.description);
                            mXView.setText(robot.x);
                            mYView.setText(robot.y);
                            mZView.setText(robot.z);
                            mRXView.setText(robot.rx);
                            mRYView.setText(robot.ry);
                            mRZView.setText(robot.rz);
                            mRView.setText(robot.r);
                        }
                    }
                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateRobotPositionActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
