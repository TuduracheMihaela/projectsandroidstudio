package com.sharethatdata.kobotcontrol;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.kobotcontrol.adapter.ExpandableListAdapterActions;
import com.sharethatdata.kobotcontrol.adapter.ExpandableListAdapterOutputs;
import com.sharethatdata.kobotcontrol.adapter.ExpandableListAdapterSensors;
import com.sharethatdata.kobotcontrol_webservice.WSDataProvider;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cActionsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cOutputsRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobot;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobotControl;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cRobots;
import com.sharethatdata.kobotcontrol_webservice.datamodel.cSensorsRobot;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

/**
 * Created by tudur on 06-Feb-19.
 */

public class RobotsDetailActivity extends AppCompatActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";

    private String status_interval;
    //private String sensor_interval;
    private boolean blRobotStatus;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> actionsList;
    ArrayList<HashMap<String, String>> sensorsList;
    ArrayList<HashMap<String, String>> outputsList;

    List<cActionsRobot> childActions = new ArrayList<cActionsRobot>();
    List<cSensorsRobot> childSensors = new ArrayList<cSensorsRobot>();
    List<cOutputsRobot> childOutputs = new ArrayList<cOutputsRobot>();

    TextView textViewId;
    TextView textViewStatus;
    TextView textViewStatusLastError;
    TextView textViewDate;
    View mySquareView;

    Button btn_actions;
    Button btn_sensors;
    Button btn_outputs;

    //Switch aSwitch;
    SwitchCompat aSwitch;
    Button btn_reset;

    ListView listViewActions;
    ListView listViewSensors;
    ListView listViewOutputs;
    private static ExpandableListView expandableListViewActions;
    private static ExpandableListView expandableListViewSensors;
    private static ExpandableListView expandableListViewOutputs;
    private static ExpandableListAdapterActions adapterActions;
    private static ExpandableListAdapterSensors adapterSensors;
    private static ExpandableListAdapterOutputs adapterOutputs;

    String id_robot = "";
    String ip_robot = "";

    String myImageData = "";

    // For zoom image /////////////////////////
    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    //Declare the timer
    Timer t;

    boolean sensorListExpanded = false;
    boolean actionListExpanded = false;
    boolean outputListExpanded = false;

    boolean check_once = true;
    boolean actions_once = true;

    String start = "";
    String stop = "";
    String reset = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_robots);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        id_robot = Globals.getValue("rid");
        ip_robot = Globals.getValue("local_ip");

        MyProvider = new WSDataProvider(getApplicationContext());
        MyProvider = new WSDataProvider(myUser, myPass);

        textViewId = (TextView) findViewById(R.id.textViewId);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus);
        textViewStatusLastError = (TextView) findViewById(R.id.textViewStatusLastError);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        mySquareView = (View) findViewById(R.id.mySquareView);
        aSwitch = (SwitchCompat) findViewById(R.id.switch_start_stop);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        if(!ip_robot.equals("")) {
            Toast.makeText(getApplicationContext(), "Local robot connection", Toast.LENGTH_SHORT).show();
        }

        /*btn_actions.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // start actions activity
                Intent intent = new Intent(getApplicationContext(), ActionsActivity.class);
                startActivity(intent);
            }
        });*/

        /*btn_sensors.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // start actions activity
                Intent intent = new Intent(getApplicationContext(), SensorsActivity.class);
                startActivity(intent);
            }
        });*/

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    //SharedPreferences.Editor editor = getSharedPreferences("defaults", MODE_PRIVATE).edit();
                    //editor.putBoolean("prefIntervalCheck", true);
                    //editor.commit();

                    if(stop.equals("0") && reset.equals("1")){
                        // reset, start, stop, monitor
                        //switch on
                        Toast.makeText(RobotsDetailActivity.this, "ON", Toast.LENGTH_SHORT).show();
                        new UpdateRobotControl("0", "1", "0", "").execute(); //start
                    }else{
                        Toast.makeText(RobotsDetailActivity.this, "You need to reset, in order to start.", Toast.LENGTH_SHORT).show();
                        aSwitch.setChecked(false);
                    }

                } else {

                    //SharedPreferences.Editor editor = getSharedPreferences("defaults", MODE_PRIVATE).edit();
                    //editor.putBoolean("prefIntervalCheck", false);
                    //editor.commit();

                    if(reset.equals("0") && start.equals("1")){
                        // reset, start, stop, monitor
                        //switch off
                        Toast.makeText(RobotsDetailActivity.this, "OFF", Toast.LENGTH_SHORT).show();
                        new UpdateRobotControl("0", "0", "1", "").execute();//stop
                    }else{
                        Toast.makeText(RobotsDetailActivity.this, "You need to start, in order to reset.", Toast.LENGTH_SHORT).show();
                        aSwitch.setChecked(false);
                    }


                    //t.cancel();
                }
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start.equals("0") && stop.equals("1")){
                    // reset, start, stop, monitor
                    Toast.makeText(RobotsDetailActivity.this, "RESET", Toast.LENGTH_SHORT).show();
                    new UpdateRobotControl("1", "0", "0", "").execute(); //reset
                }else{
                    Toast.makeText(RobotsDetailActivity.this, "You need to stop, in order to reset.", Toast.LENGTH_SHORT).show();
                }

                //t.cancel();
            }
        });


        final ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Proj Detail Image", myImageData);
                String image_string = myImageData;
                if (TextUtils.isEmpty(image_string) || image_string.contains("null")){
                    Log.d("Proj Detail Image", myImageData);
                } else {
                    zoomImageFromThumb(imageView1, myImageData);
                }
            }
        });

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);



        new LoadActionsAndSensorsRobot(id_robot).execute();
    }

    public void initializeTimer(){

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        status_interval = sharedPrefs.getString("prefMonitorStatus", "");
        if(!ip_robot.equals("")){
            status_interval = sharedPrefs.getString("prefMonitorLocal", "");
        }
        //sensor_interval = sharedPrefs.getString("prefMonitorSensor", "");
        //blIntervalCheck = sharedPrefs.getBoolean("prefIntervalCheck", false);

        long interval = 1;
        try{
            interval = TimeUnit.SECONDS.toMillis(Long.parseLong(status_interval));
        }catch (Exception e){
            e.printStackTrace();
        }
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {
                  @Override
                  public void run() {
                      if(isNetworkAvailable() == true){
                          //Called each time when 1000 milliseconds (1 second) (the period parameter)
                          runOnUiThread(new Runnable(){
                              @Override
                              public void run(){
                                  Toast.makeText(RobotsDetailActivity.this, getString(R.string.loading_alert_dialog), Toast.LENGTH_SHORT).show();
                              }
                          });
                          new LoadRobotItem(id_robot).execute();
                          new LoadRobotControl(id_robot).execute();
                          new LoadActionsAndSensorsRobot(id_robot).execute();

                          check_once = true;
                      }else {
                          if(check_once) {
                              runOnUiThread(new Runnable(){
                                  @Override
                                  public void run(){
                                      Toast.makeText(getApplicationContext(), "No internet connnection available", Toast.LENGTH_SHORT).show();
                                  }
                              });
                                check_once = false;
                          }
                      }
                  }
        },
                //Set how long before to start calling the TimerTask (in milliseconds)
                0,
                //Set the amount of time between each execution (in milliseconds)
                interval);
    }

    public void refresh(){
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                Toast.makeText(RobotsDetailActivity.this, getString(R.string.loading_alert_dialog), Toast.LENGTH_SHORT).show();
            }
        });
        new LoadRobotControl(id_robot).execute();
        new LoadRobotItem(id_robot).execute();
    }

    public void BtnActions(View v){
        t.cancel();
        t.purge();
        Intent intent = new Intent(getApplicationContext(), ActionsActivity.class);
        startActivity(intent);
    }

    public void BtnSensors(View v){
        t.cancel();
        t.purge();
        Intent intent = new Intent(getApplicationContext(), SensorsActivity.class);
        startActivity(intent);
    }

    public void BtnOutputs(View v){
        t.cancel();
        t.purge();
        Intent intent = new Intent(getApplicationContext(), RobotOutputsActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Outputs", Toast.LENGTH_SHORT).show();
    }


    class LoadRobotControl extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotControl(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {
            final cRobotControl robot;

            if(!ip_robot.equals("")){
                //MyProvider.SetWebServiceUrl(ip_robot);
                //robot = MyProvider.getControlRobotIP();
                //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                //robot = MyProvider.getControlRobot(id);
                robot = null;
            }else {
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                robot = MyProvider.getControlRobot(id);
            }



            if (robot != null)
            {
                if (robot.id != "")
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            start = robot.start;
                            stop = robot.stop;
                            reset = robot.reset;
                            //String monitor = robot.monitor;

                                if(start.equals("1")){
                                    aSwitch.setChecked(true);
                                }else if(stop.equals("1")){
                                    aSwitch.setChecked(false);
                                }else if(reset.equals("1")){
                                    aSwitch.setChecked(false);
                                }
                        }
                    });
                }
            }
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            hideProgressDialog();
        }
    }

    /** AsyncTask update robot control  */
    private class UpdateRobotControl extends AsyncTask<String, String, Boolean> {
        String reset, start, stop, monitor;

        public UpdateRobotControl(String reset, String start, String stop, String monitor){
            this.reset = reset;
            this.start = start;
            this.stop = stop;
            this.monitor = monitor;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {


            boolean suc = false;

            if(!ip_robot.equals("")){
                MyProvider.SetWebServiceUrl(ip_robot);
                if(!reset.equals("")) suc = MyProvider.updateRobotControlIP("reset");
                if(!start.equals("")) suc = MyProvider.updateRobotControlIP("start");
                if(!stop.equals("")) suc = MyProvider.updateRobotControlIP("stop");

            }else {
                // call web method
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                suc = MyProvider.updateRobotControl(id_robot, reset, start , stop, monitor);
            }


            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(RobotsDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(RobotsDetailActivity.this, "Error" + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(RobotsDetailActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    }

                    refresh();
                }
            });
            hideProgressDialog();
        }
    }

    class LoadRobotItem extends AsyncTask<String, String, String> {

        String id;

        public LoadRobotItem(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            final cRobots robot;
            final cRobot statusRobot;

            if(!ip_robot.equals("")){
                MyProvider.SetWebServiceUrl(ip_robot);
                robot = MyProvider.getDetailRobotsIP();
                statusRobot = MyProvider.getStatusRobotIP();

                //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                //tatusRobot = MyProvider.getStatusRobot(id);
            }else {
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                robot = MyProvider.getDetailRobots(id);
                statusRobot = MyProvider.getStatusRobot(id);
            }



            if (robot != null)
            {
                if (robot.id != "")
                {
                    if(!ip_robot.equals("")){
                        MyProvider.SetWebServiceUrl(ip_robot);
                        myImageData = MyProvider.getImageIP();
                    }else{
                        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                        myImageData = MyProvider.getImage(robot.id);
                    }
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String title = robot.name;
                    if (!ip_robot.equals("")) {
                        title = title + " - " + ip_robot;
                    } else {
                        title = title + " - Cloud";
                    }
                    getSupportActionBar().setTitle(title);
                    Globals.setValue("robot_name",robot.name);
                    if(statusRobot.status.equals("0")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_gray));
                    if(statusRobot.status.equals("1")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_green));
                    if(statusRobot.status.equals("2")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_yellow));
                    if(statusRobot.status.equals("3")) mySquareView.setBackground(getResources().getDrawable(R.drawable.square_red));
                    textViewStatus.setText(Html.fromHtml(statusRobot.status_text));
                    textViewStatusLastError.setText(Html.fromHtml(statusRobot.last_error));
                    textViewDate.setText(Html.fromHtml(statusRobot.datetime));

                    ImageView imageView1 = (ImageView) findViewById(R.id.imageView);

                    Bitmap d = ConvertByteArrayToBitmap(myImageData);
                    if (d != null)
                    {
                        int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                        Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                        imageView1.setImageBitmap(scaled);
                    }

                }
            });
            return "";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            hideProgressDialog();
        }
    }

    class LoadActionsAndSensorsRobot extends AsyncTask<String, String, String> {

        String id;

        public LoadActionsAndSensorsRobot(String id){
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
            /*runOnUiThread(new Runnable(){
                @Override
                public void run(){
                    Toast.makeText(RobotsDetailActivity.this, getString(R.string.loading_alert_dialog), Toast.LENGTH_SHORT).show();
                }
            });*/
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            actionsList = new ArrayList<HashMap<String, String>>();
            sensorsList = new ArrayList<HashMap<String, String>>();
            outputsList = new ArrayList<HashMap<String, String>>();

            childActions = new ArrayList<cActionsRobot>();
            childSensors = new ArrayList<cSensorsRobot>();
            childOutputs = new ArrayList<cOutputsRobot>();

            List<cActionsRobot> myAction = new ArrayList<cActionsRobot>();
            List<cSensorsRobot> mySensor;
            List<cOutputsRobot> myOutput;

            if(!ip_robot.equals("")){
                MyProvider.SetWebServiceUrl(ip_robot);
                //myAction = MyProvider.getActionsRobotsIP();
                mySensor = MyProvider.getSensorsRobotsIP();
                myOutput = MyProvider.getOutputsRobot();

                if(actions_once) {
                    actions_once = false;
                    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                    myAction = MyProvider.getActionsRobots(id);
                }

            }else {
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
                if(actions_once) {
                    actions_once = false;

                    myAction = MyProvider.getActionsRobots(id);
                }
                mySensor = MyProvider.getSensorsRobots(id);
                myOutput = MyProvider.getOutputsRobot();
            }


            for(cActionsRobot entry : myAction)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("description",  entry.description);

                // Adding child data
                childActions.add(new cActionsRobot(entry.id, entry.name, entry.description));

                // adding HashList to ArrayList
                actionsList.add(map);
            }


            for(cSensorsRobot entry : mySensor)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("value",  entry.value);
                map.put("description",  entry.description);

                // Adding child data
                childSensors.add(new cSensorsRobot(entry.id, entry.name, entry.value, entry.description));

                // adding HashList to ArrayList
                sensorsList.add(map);
            }


            for(cOutputsRobot entry : myOutput)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("value",  entry.value);
                map.put("description",  entry.description);

                // Adding child data
                childOutputs.add(new cOutputsRobot(entry.id, entry.name, entry.value, entry.description));

                // adding HashList to ArrayList
                outputsList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (RobotsDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    if(actionsList.size() > 0){

                        // For Portrait
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                            setItems(actionsList, "Actions");
                            //setListener();
                        }

                        // For Landscape
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                            if(tabletSize == false){
                                // landscape phone
                                setItems(actionsList, "Actions");
                            }else {
                                // landscape tablet
                                ListAdapter adapter = null;
                                adapter = new SimpleAdapter(RobotsDetailActivity.this, actionsList,
                                        R.layout.activity_robot_actions_items_listview,
                                        new String[]{"id", "name", "description"},
                                        new int[]{R.id.textViewIdAction, R.id.textViewNameAction, R.id.textViewDescriptionAction});

                                listViewActions.setAdapter(adapter);
                            }


                        }
                    }

                    if(sensorsList.size() > 0){

                        // For Portrait
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                            setItems(sensorsList, "Sensors");
                            //setListener();
                        }

                        // For Landscape
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                            if(tabletSize == false){
                                // landscape phone
                                setItems(sensorsList, "Sensors");
                            }else {
                                // landscape tablet
                                ListAdapter adapter = null;
                                adapter = new SimpleAdapter(RobotsDetailActivity.this, sensorsList,
                                        R.layout.activity_robot_sensors_items_listview,
                                        new String[]{"id", "name", "value", "description"},
                                        new int[]{R.id.textViewIdSensor, R.id.textViewNameSensor, R.id.textViewValueSensor, R.id.textViewDescriptionSensor});

                                listViewSensors.setAdapter(adapter);
                            }

                        }
                    }

                    if(outputsList.size() > 0){

                        // For Portrait
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                            setItems(outputsList, "Outputs");
                            //setListener();
                        }

                        // For Landscape
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                            if(tabletSize == false){
                                // landscape phone
                                setItems(outputsList, "Outputs");
                            }else {
                                // landscape tablet
                                ListAdapter adapter = null;
                                adapter = new SimpleAdapter(RobotsDetailActivity.this, outputsList,
                                        R.layout.activity_robot_outputs_items_listview,
                                        new String[]{"id", "name", "value", "description"},
                                        new int[]{R.id.textViewIdOutput, R.id.textViewNameOutput, R.id.textViewValueOutput, R.id.textViewDescriptionOutput});

                                listViewOutputs.setAdapter(adapter);
                            }

                        }
                    }
                }
            });

            hideProgressDialog();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.robot_settings, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, RobotSettingsActivity.class);
                intent.putExtra("idrobot",id_robot);
                startActivity(intent);
                return true;
            case R.id.action_positions:
                Intent intent1 = new Intent(this, RobotPositionsActivity.class);
                intent1.putExtra("idrobot",id_robot);
                startActivity(intent1);
                return true;
            case R.id.action_results:
                Intent intent2 = new Intent(this, RobotResultsActivity.class);
                intent2.putExtra("idrobot",id_robot);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // Setting headers and childs to expandable listview
    void setItems(ArrayList<HashMap<String, String>> list, String title) {
        // Array list for header
        ArrayList<String> header = new ArrayList<String>();
        // Adding headers to list
        header.add(title);


        if(title.equals("Actions")){
            // Hash map for both header and child
            HashMap<String, List<cActionsRobot>> hashMap = new HashMap<>();
            // Array list for child items

            // Adding child data
            //childActions.add(new cActionsRobot("0", "name","description"));

            // Adding header and childs to hash map
            hashMap.put(header.get(0), childActions);

            adapterActions = new ExpandableListAdapterActions(RobotsDetailActivity.this, header, hashMap, t);

            // Setting adpater over expandablelistview
            expandableListViewActions.setAdapter(adapterActions);
            if(actionListExpanded){
                expandableListViewActions.expandGroup(0);
            }

        }else if(title.equals("Sensors")){
            // Hash map for both header and child
            HashMap<String, List<cSensorsRobot>> hashMap = new HashMap<>();
            // Array list for child items

            // Adding child data
            //childSensors.add(new cSensorsRobot("0", "name","value","description"));
            // Adding header and childs to hash map
            hashMap.put(header.get(0), childSensors);

            adapterSensors = new ExpandableListAdapterSensors(RobotsDetailActivity.this, header, hashMap, t);

            // Setting adpater over expandablelistview
            expandableListViewSensors.setAdapter(adapterSensors);
            if(sensorListExpanded){
                expandableListViewSensors.expandGroup(0);
            }

        }else if(title.equals("Outputs")){
            // Hash map for both header and child
            HashMap<String, List<cOutputsRobot>> hashMap = new HashMap<>();
            // Array list for child items

            // Adding child data
            //childSensors.add(new cSensorsRobot("0", "name","value","description"));
            // Adding header and childs to hash map
            hashMap.put(header.get(0), childOutputs);

            adapterOutputs = new ExpandableListAdapterOutputs(RobotsDetailActivity.this, header, hashMap, t);

            // Setting adpater over expandablelistview
            expandableListViewOutputs.setAdapter(adapterOutputs);
            if(outputListExpanded){
                expandableListViewOutputs.expandGroup(0);
            }

        }


    }


    // Setting different listeners to expandablelistview
    void setListener() {

        // This listener will show toast on group click
        expandableListViewActions.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView listview, View view,
                                        int group_pos, long id) {
                Toast.makeText(RobotsDetailActivity.this,
                        "You clicked : " + adapterActions.getGroup(group_pos),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // This listener will expand one group at one time
        // You can remove this listener for expanding all groups
        expandableListViewActions
                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    // Default position
                    int previousGroup = -1;
                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (groupPosition != previousGroup)
                            // Collapse the expanded group
                            expandableListViewActions.collapseGroup(previousGroup);
                        previousGroup = groupPosition;
                    }
                });

        // This listener will show toast on child click
        expandableListViewActions.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView listview, View view,
                                        int groupPos, int childPos, long id) {
                Toast.makeText(
                        RobotsDetailActivity.this,
                        "You clicked : " + adapterActions.getChild(groupPos, childPos),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }


    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        Bitmap decodedByte = null;
        try{
            byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        }catch (Exception e){
            e.printStackTrace();
        }


        return decodedByte;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String resizeBase64Image(String base64image){
        int IMG_WIDTH = 800;
        int IMG_HEIGHT = 600;
        byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


        if(image.getHeight() <= 400 && image.getWidth() <= 400){
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100, baos);

        byte [] b=baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void zoomImageFromThumb(final View thumbView, String imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image_project);

        //
        byte[] decodedString;
        if(imageResId != ""){
            String resizeString =  resizeBase64Image(imageResId);
            decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
        }else{
            decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
        }


        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null)
        {
            //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
            //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
            expandedImageView.setImageBitmap(decodedByte);

        }


        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();


        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.linearLayout)// the container
                .getGlobalVisibleRect(finalBounds, globalOffset);


		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float screen_width = metrics.widthPixels;

		float scale =  screen_width / findViewById(R.id.scrollViewProjects).getWidth();
		if(findViewById(R.id.scrollViewProjects).getScaleX() == 1) {
			findViewById(R.id.scrollViewProjects).setScaleY(scale);
			findViewById(R.id.scrollViewProjects).setScaleX(scale);
		}else{
			findViewById(R.id.scrollViewProjects).setScaleY(1);
			findViewById(R.id.scrollViewProjects).setScaleX(1);
		}*/


        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height() * 2;
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        float width_s = startBounds.width();
        float height_s = startBounds.height();

        float width_f = finalBounds.width();
        float height_f = finalBounds.height();

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);



	    /*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float screen_width = metrics.widthPixels;

		float scale =  screen_width / findViewById(R.id.scrollViewProjects).getWidth();
		if(expandedImageView.getScaleX() == 1) {
			expandedImageView.setScaleY(scale + 3f);
			expandedImageView.setScaleX(scale + 5f);
		}else{
			expandedImageView.setScaleY(1);
			expandedImageView.setScaleX(1);
		}*/


        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        // resize image SCALE_X and SCALE_Y -> I should check separate resize phone and tablet

        AnimatorSet set = new AnimatorSet();
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // is tablet
            if(startScale <= 0.4){
                // image portrait

                    set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                            startBounds.left, finalBounds.left))
                            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                    startBounds.top, finalBounds.top))
                            .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                    startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                            View.SCALE_Y, startScale, 1f));
                    set.setDuration(mShortAnimationDuration);
                    set.setInterpolator(new DecelerateInterpolator());
                    set.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mCurrentAnimator = null;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            mCurrentAnimator = null;
                        }
                    });
                    set.start();
            //}else if(startScale > 0.4){
            }else if(startScale > 0.4){
                // image landscape = 0.4411277

                set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                startBounds.top, finalBounds.top))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mCurrentAnimator = null;
                    }
                });
                set.start();
            }

        } else {
            // is phone
            set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                    startBounds.left, finalBounds.left))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                            startBounds.top, finalBounds.top))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                            startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                    View.SCALE_Y, startScale, 2f));
            set.setDuration(mShortAnimationDuration);
            set.setInterpolator(new DecelerateInterpolator());
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mCurrentAnimator = null;
                }
            });
            set.start();
        }



        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale ;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }

    // for listview in scrollview (without this method, the listview won't scroll)
    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    /*public static void setExpandedGroup(ExpandableListView expListView) {
        if (expListView != null
                && expListView.getExpandableListAdapter() != null) {
            final int groupCount = expListView.getExpandableListAdapter().getGroupCount();
            for (int i = 0; i < groupCount; i++) {
                if (expListView.isGroupExpanded(i)) {
                    idsExpand.add(i);
                }
            }
        }

    }

    public static void getExpandedIds(ExpandableListView expListView) {
        if (idsExpand != null) {
            final int groupCount = idsExpand.size();
            for (int i = 0; i < groupCount; i++) {
                if (expListView != null) {
                    try {
                        expListView.expandGroup(idsExpand.get(i));
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }*/

    @Override
    public void onResume(){
        super.onResume();

        // For Portrait
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            expandableListViewActions = (ExpandableListView) findViewById(R.id.listViewActions);
            expandableListViewSensors = (ExpandableListView) findViewById(R.id.listViewSensors);
            expandableListViewOutputs = (ExpandableListView) findViewById(R.id.listViewOutputs);

            // Setting group indicator null for custom indicator
            expandableListViewActions.setGroupIndicator(null);
            expandableListViewSensors.setGroupIndicator(null);
            expandableListViewOutputs.setGroupIndicator(null);
        }

        // For Lanscape
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
            if(tabletSize == false){
                // landscape phone
                expandableListViewActions = (ExpandableListView) findViewById(R.id.listViewActions);
                expandableListViewSensors = (ExpandableListView) findViewById(R.id.listViewSensors);
                expandableListViewOutputs = (ExpandableListView) findViewById(R.id.listViewOutputs);

                // Setting group indicator null for custom indicator
                expandableListViewActions.setGroupIndicator(null);
                expandableListViewSensors.setGroupIndicator(null);
                expandableListViewOutputs.setGroupIndicator(null);
            }else {
                // landscape tablet
                listViewActions = (ListView) findViewById(R.id.listViewActions);
                listViewSensors = (ListView) findViewById(R.id.listViewSensors);
                listViewOutputs = (ListView) findViewById(R.id.listViewOutputs);
                btn_actions = (Button) findViewById(R.id.btn_actions);
                btn_sensors = (Button) findViewById(R.id.btn_sensors);
                btn_outputs = (Button) findViewById(R.id.btn_outputs);
            }
        }

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(tabletSize == false){
            // scrollview just for phone screen
            expandableListViewActions.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);

                    if(parent.isGroupExpanded(groupPosition))
                    {
                        // Collapsed
                        actionListExpanded = false;
                    }
                    else{
                        // Expanded
                        actionListExpanded = true;
                    }

                    return false;
                }
            });

            expandableListViewSensors.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);

                    if(parent.isGroupExpanded(groupPosition))
                    {
                        // Collapsed
                        sensorListExpanded = false;
                    }
                    else{
                        // Expanded
                        sensorListExpanded = true;
                    }
                    return false;
                }
            });

            expandableListViewOutputs.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);

                    if(parent.isGroupExpanded(groupPosition))
                    {
                        // Collapsed
                        outputListExpanded = false;
                    }
                    else{
                        // Expanded
                        outputListExpanded = true;
                    }
                    return false;
                }
            });
        }

        //Initialize the timer
        t = new Timer();

        initializeTimer();
        refresh();
    }

    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onDestroy() {
        Log.d("onDestroy","onDestroy");
        t.cancel();
        t.purge();
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(final String msg)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    //if(!((Activity) getApplicationContext()).isFinishing())
                    if(!RobotsDetailActivity.this.isFinishing())
                    {
                        //show dialog

                        // check for existing progressDialog
                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(RobotsDetailActivity.this); // create a progress Dialog
                            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setMessage(msg);
                        }
                        progressDialog.show(); // now display it.
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
