package com.sharethatdata.digivisitor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.http.protocol.HTTP;
import com.sharethatdata.digivisitor.ListActivity.LoadList;
import com.sharethatdata.webservice.ImageProcessor;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.*;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;

public class DetailActivity extends Activity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider1 = null;
	WSRoboDataProvider MyRoboProvider = null;
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndHour,mEndMinute;
	private int mEndYear,mEndMonth,mEndDay;
	
	ArrayList<HashMap<String, String>> itemList;
	List<cEmployee> employeeList;
	List<cResource> resourceList;
	List<cVisitEntries> visitList;
	 
	ListView lv = null;
	
	String myUser = "";
	String myPass = "";
	String myAction = "";
	String pid = "";
	String statusId = "";
	String statusName = "";
	
	String myImageData = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Globals = ((MyGlobals)getApplication());
		pid = Globals.getValue("pid");
		String action = Globals.getValue("action");
		myAction = action;
		
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider1 = new WSDataProvider(myUser, myPass);
		MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
		MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));				
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyProvider1.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
		setTitle(action);
		
		if (action == "contacts")
			setContentView(R.layout.activity_detail_contact);
		if (action == "projects")
			setContentView(R.layout.activity_detail_project);
		if (action == "news")
			setContentView(R.layout.activity_detail_news);
		if (action == "resources")
			setContentView(R.layout.activity_detail_resource);
		if (action == "messages")
			setContentView(R.layout.activity_detail_message);
		if (action == "visits")
			setContentView(R.layout.activity_detail_visits);
				
		TextView titleText = (TextView) findViewById(R.id.titleText);
		//TextView statusText = (TextView) findViewById(R.id.StatusText);
		
		//titleText.setText("Contacts");
		titleText.setText(Globals.getValue("name"));
		//statusText.setText(Globals.getValue("status"));
		
		    
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    
	    mEndYear = c.get(Calendar.YEAR);
	    mEndMonth = c.get(Calendar.MONTH);
	    mEndDay = c.get(Calendar.DAY_OF_MONTH );
		mEndDay = mEndDay + 1;
	    mEndHour = c.get(Calendar.HOUR_OF_DAY);
	    mEndMinute = c.get(Calendar.MINUTE);
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    LoadData();
	    
	}
	
	public void LoadData()
	{
		if (myAction != "visits" && myAction != "messages")
		{
		
			/*ImageView bmImage = (ImageView)findViewById(R.id.imageView1);
					
			ImageProcessor myImgProcessor = new ImageProcessor(getBaseContext());
			myImgProcessor.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			String imgUrl = myImgProcessor.getImageURL(pid, myAction);

			new DownloadImageTask(bmImage).execute(imgUrl);*/
		
		}
		
		// load data
		if (myAction == "contacts") new LoadDataTask(pid).execute();
		if (myAction == "projects") {
			new LoadProjectTask(pid).execute();
				
		}
		if (myAction == "news") new LoadNewsTask(pid).execute();
		if (myAction == "resources") new LoadResourceTask(pid).execute();
		if (myAction == "messages") new LoadMessageTask(pid).execute();
		if (myAction == "visits") {
			new LoadVisitsTask(pid).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			//new LoadVisitsTask(pid).execute();
		}
		
		String isMan = Globals.getValue("manager");
		if (isMan == "yes")
		{	
			Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.VISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.VISIBLE);	
		} else {
			Button btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setVisibility(Button.INVISIBLE);	
			Button btnDelete = (Button) findViewById(R.id.btnDelete);
			btnDelete.setVisibility(Button.INVISIBLE);	
		}
	    
	    Button btnDelete = (Button) findViewById(R.id.btnDelete);
		  	    
	    // delete button listener
	    btnDelete.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {
		
			  if (myAction == "contacts")
			  {
				  new AlertDialog.Builder(DetailActivity.this)
				  .setTitle("Delete employee")
				  .setMessage("Do you really want to delete this employee?")
				  .setIcon(android.R.drawable.ic_dialog_alert)
				  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	
				      public void onClick(DialogInterface dialog, int whichButton) {
				    	  				      
				    	  String pid = Globals.getValue("pid");
				          new DeleteUserTask(pid).execute();
				          
				      }})
				   .setNegativeButton(android.R.string.no, null).show();
			  }  
		  }
		 
		});
		
	    Button btnEdit = (Button) findViewById(R.id.btnEdit);
  	    
	    // edit button listener
	    btnEdit.setOnClickListener(new OnClickListener() {
			 
		  @Override
		  public void onClick(View v) {
			 
	    	//  String pid = Globals.getValue("pid");
			  if (myAction == "contacts")
			  {
				  Intent intent = new Intent(v.getContext(), EditDetailActivity.class); 
				  startActivity(intent); 
			  }
		  }
		 
		});
		

	}
	
	
	public void imageClick(View view)
	{		
	 	Intent intent = new Intent(this, PictureDetailActivity.class); startActivity(intent); 
	}
	
	public void phoneClick(View view) {  
	   	
		TextView phoneView = (TextView) findViewById(R.id.phoneText);
		String phone_number = phoneView.getText().toString();
		 
		// call phone number
		Uri number = Uri.parse("tel:" + phone_number);
		Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
		
		startActivity(callIntent);
    }  
	
	public void skypeClick(View view) {  
	   	
		// start skype 
		
    }  
	
	public void emailClick(View view) {  
	
		TextView emailView = (TextView) findViewById(R.id.emailText);
		String email_addr = emailView.getText().toString();
		
		// send email
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		// The intent does not have a URI, so declare the "text/plain" MIME type
		emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email_addr}); // recipients
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
		emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text");
		
		startActivity(emailIntent);
	}
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	    		        
	            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	        }
	    };
	    
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
	 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
	    		        
	            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	                    .append(pad(mStartMonth + 1)).append("-")
	                    .append(pad(mStartYear)));
	        }
	    };

	private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mEndHour = hourOfDay;
	            mEndMinute = minute;
	            
	            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	    		        
	            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
	                    .append(pad(mEndMinute)));
	        }
	    };
	    
	private DatePickerDialog.OnDateSetListener mEndDateSetListener = new DatePickerDialog.OnDateSetListener() {
	        public void onDateSet(DatePicker view, int year, int month, int day) {
	            mEndYear = year;
	            mEndMonth = month;
	            mEndDay = day; 
	            		
	            TextView endDateView = (TextView) findViewById(R.id.dateEndText);
	    		        
	            endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
	                    .append(pad(mEndMonth + 1)).append("-")
	                    .append(pad(mEndYear)));
	        }
	    };
	     
	public void startTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
	    tp1.show();
	}
	
	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    dp1.show();
	}
	
	public void endTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
	    tp1.show();
	}
	
	public void endDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mEndDateSetListener, mEndYear, mEndMonth, mEndDay);
	    dp1.show();
	}
		
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		  ImageView bmImage;

		  public DownloadImageTask(ImageView bmImage) {
		      this.bmImage = bmImage;
		  }

		  protected Bitmap doInBackground(String... urls) {
		      String urldisplay = urls[0];
		      Bitmap mIcon11 = null;
		      try {
		        InputStream in = new java.net.URL(urldisplay).openStream();
		        mIcon11 = BitmapFactory.decodeStream(in);
		        mIcon11 = Bitmap.createScaledBitmap(mIcon11, 300, 300, true);
		        
		      } catch (Exception e) {
		    	// Log.e("Error", e.getMessage());
		          System.out.println("Read this error:" + e.toString());
		          e.printStackTrace();
		        
		          
		          // failed get default image
		          try {
				       
		        	 if (myAction == "contacts") mIcon11 = BitmapFactory.decodeResource(getResources(), R.drawable.person);
		        	 if (myAction == "resources") mIcon11 = BitmapFactory.decodeResource(getResources(), R.drawable.person);
		        	 if (myAction == "visits") mIcon11 = BitmapFactory.decodeResource(getResources(), R.drawable.person);
		        	  
		          
		      //    int imageResource = R.drawable.person_small;
		     //     String path = String.valueOf(imageResource);
		         	//  	InputStream in = new java.net.URL(path).openStream();
			       	//	mIcon11 = BitmapFactory.decodeStream(in);
			       	//	mIcon11 = Bitmap.createScaledBitmap(mIcon11, 300, 300, true);
			        	 
		          } catch (Exception e2) {
		        	  	System.out.println("Read this error:" + e2.toString());
				        e2.printStackTrace();
		          }
		      }
		      return mIcon11;
		  }

		  protected void onPostExecute(Bitmap result) {
		      bmImage.setImageBitmap(result);
		  }
		}
	
	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}

//	private class LoadDataTask extends AsyncTask<String, String, cContact> {
//		  String myID = "";
//		  
//		  public LoadDataTask(String id) {
//		     myID = id;
//		  }
//
//		  protected cContact doInBackground(String... urls) {
//			  			  
//			  cContact myContact = MyProvider.getContact(myID);
//							  			  
//		      return myContact;
//		  }
//
//		  protected void onPostExecute(cContact result) {
//			  
//			  if (result != null)
//			  {
//				  TextView functionView = (TextView) findViewById(R.id.subTitleText);
//				  functionView.setText(result.function);
//				  TextView emailView = (TextView) findViewById(R.id.emailText);
//				  emailView.setText(result.email);
//				  TextView phoneView = (TextView) findViewById(R.id.phoneText);
//				  phoneView.setText(result.phone);
//				  TextView statusView = (TextView) findViewById(R.id.StatusText);
//				  statusView.setText(String.valueOf(result.status));
//				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
//				  descriptionView.setText(String.valueOf(result.description));
//				
//			  }
//		  }
//		}
	
	private class DeleteUserTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public DeleteUserTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  			  
			  boolean suc = MyProvider.deleteContact(myID);
			  
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  finish();
			  
		  }
	}
	
	private class LoadDataTask extends AsyncTask<String, String, cContact> {
		  String myID = "";
		  
		  public LoadDataTask(String id) {
		     myID = id;
		  }

		  protected cContact doInBackground(String... urls) {
			  			  
			  cContact myEmployee = MyProvider.getContact(myID);
			  
			  
			  if (myEmployee != null)
			  {	
				  if (myEmployee.image_id != "0")
				  {
					  myImageData = MyRoboProvider.getImage(myEmployee.image_id);
				  }
			  }
			
			  return myEmployee;
		  }

		  protected void onPostExecute(cContact result) {
			  
			  if (result != null)
			  {		
	 			  TextView functionView = (TextView) findViewById(R.id.subTitleText);
				  functionView.setText(result.function);
				  TextView emailView = (TextView) findViewById(R.id.emailText);
				  emailView.setText(result.email);
				  TextView phoneView = (TextView) findViewById(R.id.phoneText);
				  phoneView.setText(result.phone);
				  //TextView statusView = (TextView) findViewById(R.id.StatusText);
			      //statusView.setText(String.valueOf(result.status));
				  statusId = result.status_id;
				  
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(String.valueOf(result.description));

				  ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
  				  
  				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
  				  if (d != null)
  				  {
  					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
  					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
  					  imageView1.setImageBitmap(scaled);
  					  				  
  				  }
						  
				  
				  new LoadUserStatusTask(statusId).execute();
			  }
			  
		  }
	}
	
	
	private class LoadUserStatusTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  public LoadUserStatusTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  
			  ArrayList<HashMap<String, String>> statusList = new ArrayList<HashMap<String, String>>();

		    		// convert objects to hashmap for list
		    		List<cUserStatus> List = MyProvider.getUserStatus();
		    	
		    		for(cUserStatus entry : List)
	             	{
		    			// creating new HashMap
		                HashMap<String, String> map = new HashMap<String, String>();
		
		                map.put("pid", entry.id);
		                map.put("name", entry.name);
		                
		                if(myID.contains(entry.id)){
		                	statusName = entry.name;
		                }
		                 
		                // adding HashList to ArrayList
		                statusList.add(map);
		             }
			
			  return statusName;
		  }

		  protected void onPostExecute(String result) { 
			  if(result != null){
				  TextView statusView = (TextView) findViewById(R.id.StatusText);
		          statusView.setText(String.valueOf(statusName));
			  }
			  
		  }
	}
	
	private class LoadProjectTask extends AsyncTask<String, String, cProject> {
		  String myID = "";
		  
		  public LoadProjectTask(String id) {
		     myID = id;
		  }

		  protected cProject doInBackground(String... urls) {
			  			  
			  cProject myProject = MyProvider.getProject(myID);
							  			  
		      return myProject;
		  }

		  protected void onPostExecute(cProject result) {
			  
			  if (result != null)
			  {
				  TextView functionView = (TextView) findViewById(R.id.subTitleText);
				  functionView.setText(result.code);
				  TextView statusView = (TextView) findViewById(R.id.statusText);
				  statusView.setText(String.valueOf(result.status));
				  TextView ownerView = (TextView) findViewById(R.id.ownerText);
				  ownerView.setText(String.valueOf(result.owner));
				  TextView ETCView = (TextView) findViewById(R.id.ETCText);
				  ETCView.setText("ETC = " + String.valueOf(result.ETC) + " days");
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(String.valueOf(result.description));
				
				  lv = (ListView) findViewById(R.id.listView);
					
				  new LoadProjectEntriesTask(result.code).execute();
				  
			  }
		  }
		}
	
	private class LoadNewsTask extends AsyncTask<String, String, cNews> {
		  String myID = "";
		  
		  public LoadNewsTask(String id) {
		     myID = id;
		  }

		  protected cNews doInBackground(String... urls) {
			  			  
			  cNews myNews = MyProvider.getNewsItem(myID);
							  			  
		      return myNews;
		  }

		  protected void onPostExecute(cNews result) {
			  
			  if (result != null)
			  {
				  TextView statusView = (TextView) findViewById(R.id.StatusText);
				  statusView.setText(String.valueOf(result.created));
				  TextView ownerView = (TextView) findViewById(R.id.ownerText);
				  ownerView.setText(String.valueOf(result.owner));
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(String.valueOf(result.description));
				  
			  }
		  }
		}
	
	private class LoadResourceTask extends AsyncTask<String, String, cResource> {
		  String myID = "";
		  
		  public LoadResourceTask(String id) {
		     myID = id;
		  }

		  protected cResource doInBackground(String... urls) {
			  			  
			 // resourceList 
			  cResource myResource = MyProvider.getResource(myID);
							  			  
		      return myResource;
		  }

		  protected void onPostExecute(cResource result) {
			  
			  if (result != null)
			  {
		
				  TextView functionView = (TextView) findViewById(R.id.codeText);
				  functionView.setText(result.name);
				  TextView statusView = (TextView) findViewById(R.id.statusText);
				  statusView.setText(String.valueOf(result.status));
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(String.valueOf(result.description));

			  }
		  }
		}
	
	private class LoadMessageTask extends AsyncTask<String, String, cMessage> {
		  String myID = "";
		  
		  public LoadMessageTask(String id) {
		     myID = id;
		  }

		  protected cMessage doInBackground(String... urls) {
			  			  
			  cMessage myMessage = MyProvider.getMessage(myID);
							  			  
		      return myMessage;
		  }

		  protected void onPostExecute(cMessage result) {
			  
			  if (result != null)
			  {
				  TextView statusView = (TextView) findViewById(R.id.StatusText);
				  statusView.setText(String.valueOf(result.datetime));
				  TextView ownerView = (TextView) findViewById(R.id.ownerText);
				  ownerView.setText(String.valueOf(result.user_from));
				  TextView descriptionView = (TextView) findViewById(R.id.descriptionText);
				  descriptionView.setText(String.valueOf(result.message));
				  
			  }
		  }
		}
	
	private class LoadProjectEntriesTask extends AsyncTask<String, String, String> {
		String myID = "";
		  
		public LoadProjectEntriesTask(String id) {
		     myID = id;
		}
		  
		protected String doInBackground(String... args) {
		    
	//		itemList = MyProvider.getProjectItems(myID);
	    			    	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	     
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	                ListAdapter adapter = new SimpleAdapter(
	                		DetailActivity.this
	                		, itemList
	                		, R.layout.sublistview_item, new String[] { "status", "pid", "name", "subtitle" },
                            new int[] { R.id.textViewDateTime, R.id.textViewID, R.id.textViewName, R.id.textViewFunc });
	                			                
	                // updating listview
	                lv.setAdapter(adapter);
	               
	            }
	        });
	         
	    }
	    
	
	}
	
    ////////////////////////////////////////////////////////////////////////////////////
	    // List with visits
	    ////////////////////////////////////////////////////////////////////////////////////
	    
		private class LoadVisitsTask extends AsyncTask<String, String, cVisitEntries> {
			  String myID = "";
			  
			  public LoadVisitsTask(String id) {
			     myID = id;
			  }

			  protected cVisitEntries doInBackground(String... urls) {
				  			  
				  cVisitEntries visit = MyProvider.getVisit(myID);
								  			  
			      return visit;
			  }

			  protected void onPostExecute(cVisitEntries result) {
				
				  if (result != null){
						  
						UIHelper myUIHelper = new UIHelper();
					  
						TextView sView = (TextView) findViewById(R.id.titleText);
						sView.setText("Visit");
						sView = (TextView) findViewById(R.id.dateStartText);
						String form_date = myUIHelper.SetFormattedDate(result.startdatetime);
						sView.setText(form_date);
						sView = (TextView) findViewById(R.id.startTimeText);
						myUIHelper.SetTime(sView, result.startdatetime);
						sView = (TextView) findViewById(R.id.endTimeText);
						myUIHelper.SetTime(sView, result.enddatetime);
						sView = (TextView) findViewById(R.id.organisationText);
						sView.setText(result.organisation);
						sView = (TextView) findViewById(R.id.functionText);
						sView.setText(result.function);
						sView = (TextView) findViewById(R.id.nameText);
						sView.setText(result.surname + " " + result.lastname);
						sView = (TextView) findViewById(R.id.emailText);
						sView.setText(result.email);
						sView = (TextView) findViewById(R.id.phoneText);
						sView.setText(result.phone);
						sView = (TextView) findViewById(R.id.employeeText);
						
						String employees = result.employee_name;
					   	if (result.coWorkers.size() > 0)
					   	{
					   		employees = "";
							for(String member : result.coWorkers)
			            	{
						   		employees = employees + System.getProperty("line.separator") + member;
			            	}
					   	}
					   	sView.setText(employees);
						
				  }
			  
			  }

		}
		
		
}


