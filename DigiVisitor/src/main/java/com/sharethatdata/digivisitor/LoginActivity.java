package com.sharethatdata.digivisitor;

import java.util.ArrayList;

import org.json.JSONArray;

import com.sharethatdata.registration.form.RegistrationActivity;
import com.sharethatdata.webservice.WSDataProvider;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

    MyGlobals Globals;
	
    WSDataProvider MyProvider = null;
    WSDataProvider MyProvider1 = null;

    JSONArray order = null;
    
	private String myUser = "";
	private String myPass = "";
	String role;
	ArrayList<String> roles = null;
	
    private boolean no_network = false;
    private String error_message = "";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	private UserRolesTask mRolesTask = null;
	
	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	private String webservice_url = "";
	
	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_new);
		
		// Set up the login form.
		Globals = ((MyGlobals)getApplication());

		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		
		String current_user = sharedPrefs.getString("prefUsername", "");
		String current_password = sharedPrefs.getString("prefUserPassword", "");
		boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
	
		boolean blNotifications = sharedPrefs.getBoolean("prefNotification", false);
		String interval = sharedPrefs.getString("prefUpdateFrequency", "");
	
		// get webservice url
		webservice_url = sharedPrefs.getString("prefWebServiceServer", "");
		
		MyProvider = new WSDataProvider(current_user, current_password);
		MyProvider1 = new WSDataProvider(current_user, current_password);
		
		if (webservice_url != "")
		{
			// store in globals
			Globals.setValue("ws_url", webservice_url);
			
			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider1.SetWebServiceUrl(webservice_url);
		}
		
		// Set up the login form.
		mEmail = current_user;
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPassword = current_password;
		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setText(mPassword);

		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						if(isNetworkAvailable() == true){
                            attemptLogin();
                        }else {
                            Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_SHORT).show();
                        }
					}
				});
		
		if (blAutoLogin)
		{
            if(isNetworkAvailable() == true){
                attemptLogin();
            }else {
                Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_SHORT).show();
            }
		}
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        case R.id.action_create_user:
        	Intent intent_registration = new Intent(this, RegistrationActivity.class); startActivity(intent_registration); 
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		} 

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
			
			//mGetRoleUser = new LoadUserRole();
			//mGetRoleUser.execute();
			
		}
	}
	
	protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			
			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl(webservice_url);
			boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "DigiVisitor"); 
			if (suc)
			{				
				// store current user in globals
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("user", mEmail);
			    myUser = mEmail;
			    myPass = mPassword;
			    
			    MyProvider = new WSDataProvider(mEmail, mPassword);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			    MyProvider.LogEvent(getString(R.string.text6_log_event));
			    
			    /*SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
		   	     editor.putString("USER", String.valueOf(myUser));
		   	     editor.putString("PASS", String.valueOf(myPass));
		   	     editor.putString("URL", String.valueOf(webservice_url));
		   	     editor.commit();*/
			  
			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);
			
			if (success) {
			   
				// register roles
				mRolesTask = new UserRolesTask();
				mRolesTask.execute((Void) null);
				
				// call service to check new message and new news
				//MyProvider = new WSDataProvider(LoginActivity.this);
				//MyProvider.startServicefromNonActivity();
			    				
				finish();
				
				Intent intent = new Intent(LoginActivity.this, IntroActivity.class); 
			 	startActivity(intent); 
			} else {
				mPasswordView.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
				
				//Toast.makeText(LoginActivity.this, mEmailView.getText().toString(), Toast.LENGTH_SHORT).show();
				//Toast.makeText(LoginActivity.this, mPasswordView.getText().toString(), Toast.LENGTH_SHORT).show();
			}
			
			
		
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	/**
	 * Represents an asynchronous task used to check the roles of
	 * the user.
	 */
	public class UserRolesTask extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(Void... params) {
			  
			MyProvider1 = new WSDataProvider(mEmail, mPassword);
		    MyProvider1.SetWebServiceUrl(Globals.getValue("ws_url"));
			roles = MyProvider1.getUserRoles(mEmail);
	
			if (roles != null)
			{
				MyGlobals Globals = ((MyGlobals)getApplication());
				
				if (roles.contains("Administrator"))
				{				
				    Globals.setValue("administrator", "yes");
				} else {
				    Globals.setValue("administrator", "no");
				}
				if (roles.contains("Developer"))
				{				
				    Globals.setValue("developer", "yes");
				} else {
				    Globals.setValue("developer", "no");
				}
				if (roles.contains("Manager"))
				{				
				    Globals.setValue("manager", "yes");
				} else {
				    Globals.setValue("manager", "no");
				}
				if (roles.contains("Employee"))
				{				
				    Globals.setValue("employee", "yes");
				} else {
				    Globals.setValue("employee", "no");
				}
			}
		
			
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			
			runOnUiThread(new Runnable() {
			      @Override
			          public void run() {
			    	  

			    	  	if (roles.size() == 0)
						{
							String data_error = MyProvider1.last_error;
							Toast.makeText(getApplication(), data_error, Toast.LENGTH_LONG).show();
						} 
		
			    	  	showProgress(false);

			      }
			});
					
			      
			if (success) {
			   
			} else {
				
			}
			
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
}
