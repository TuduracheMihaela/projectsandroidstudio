package com.sharethatdata.digivisitor.DataModel;

public class cVisitEntries {
	public long id;
	public String startdatetime;
	public String enddatetime;
	public String allday;
    public String organisation;
    public String surname;
    public String lastname;
    public String function;
    public String phone;
    public String email;
    public String visit_kind;
	public String employee;
}
