package com.sharethatdata.digivisitor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digivisitor.R.id;

import android.app.Activity;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapter extends ArrayAdapter<HashMap<String, String>>{
	    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
	 	private final Activity context;
	 	private final int layoutResourceId;

	    public ListViewAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter, String list)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	    }
	  
	    @Override
	    public View getView(int position, View convertView, ViewGroup parrent)
	    {
	        HashMap<String, String>hashmap_Current;
	        View view=null;
	        convertView = null;
	        ViewHolder holder;
	        
	        if (convertView == null)
	        {
	            LayoutInflater layoutInflater=context.getLayoutInflater();
	            view=layoutInflater.inflate(layoutResourceId, null);
		        holder=new ViewHolder();
		        holder.lbl_ID = (TextView)view.findViewById(id.textViewID);
		        holder.lbl_Name = (TextView)view.findViewById(id.textViewName);        
		        holder.lbl_Subtitle = (TextView)view.findViewById(id.textViewFunc);
		        holder.lbl_Location = (TextView)view.findViewById(id.textViewStatus);
		        holder.imageContact = (ImageView)view.findViewById(R.id.iv_img);
		        view.setTag(holder);
		        
	    }else{

	        holder=(ViewHolder)view.getTag();
	    }
	        hashmap_Current=new HashMap<String, String>();
	        hashmap_Current=adapter.get(position);

	        Log.e("Zdit", hashmap_Current.toString());
	        
	        holder.lbl_ID.setText(hashmap_Current.get("pid").toString());       
 	        holder.lbl_Name.setText(Html.fromHtml(hashmap_Current.get("name")));       
 	        holder.lbl_Subtitle.setText(Html.fromHtml(hashmap_Current.get("subtitle")));       
 	        holder.lbl_Location.setText(Html.fromHtml(hashmap_Current.get("location")));
 	        	
 	        	Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image"));
				  if (d != null)
				  {
					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
					  holder.imageContact.setImageBitmap(scaled);
					  				  
				  }else{
					  holder.imageContact.setImageResource(R.drawable.person); 
				  }
 	        
	        
	        int myRow = 0;

	        return view;
	    }

	    static class ViewHolder {

	        TextView lbl_ID, lbl_Name,lbl_Subtitle,lbl_Location;
	        ImageView imageContact;

	    }
	    
	    private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}
	    
}
