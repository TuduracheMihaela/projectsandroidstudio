package com.sharethatdata.digivisitor;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends Activity{
	
	WebView webView;
	
	protected void onCreate(Bundle bundle){
		super.onCreate(bundle);
		
		setContentView(R.layout.activity_web_view);
		
		 webView = (WebView) findViewById(R.id.webView1);

		         webView.loadUrl("http://sharethatdata.com/");

	}
}
