package com.sharethatdata.digivisitor;

import com.sharethatdata.digivisitor.SignInActivity.LoadList;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class PrintBatchActivity extends Activity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	 	
	private boolean no_network = false;
	private String error_message = "";
	
	private String myUser = "";
	private String myPass = "";
	    
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_print_batch);
		
	     Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
			 
	     SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		 String user = sharedPrefs.getString("prefUsername", "");

	     Globals = ((MyGlobals)getApplication());
	     myUser = Globals.getValue("user");
	     myPass = Globals.getValue("pass");
	     
	     MyProvider = new WSDataProvider(myUser, myPass);
		
	     //print label
	     new CreatePrintJob().execute();
		  
		 btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				  			  
				  Intent intent = new Intent(v.getContext(), SalesActivity.class); 
				  startActivity(intent);
				//  Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent); 
			  }
		 
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");
		
		if( isAdministrator == "yes" ){
			getMenuInflater().inflate(R.menu.print_batch, menu);
		} else if(isDeveloper == "yes"){
			getMenuInflater().inflate(R.menu.print_batch, menu);
		}else if(isEmployee == "yes"){
			
			}

		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	/**
	 * Background Async Task for making HTTP Request
  	 * */
	class CreatePrintJob extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	    	MyGlobals Globals = ((MyGlobals)getApplication());
	    	String action =  Globals.getValue("action");
	    	
	    	String user = myUser;
			String organisation = Globals.getValue("organisation");
			String organisation_employee = Globals.getValue("visitor_employee");
			String visitor = Globals.getValue("visitor_surname") + " " + Globals.getValue("visitor_lastname");
			String visitor_organisation = Globals.getValue("visitor_organisation");
			String visit_id = Globals.getValue("visitID");
			String barcode = "";

			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			if (action == "signin")
			//aanmelden
			{
			//	MyProvider.SetLabelServiceUrl(PrintBatchActivity.this);
			//	MyProvider.CreatPrintJob(user, organisation, visitor, visitor_organisation);
				MyProvider.CreateOnlinePrintJob(user, organisation, organisation_employee, visitor, visitor_organisation, barcode, visit_id);

			}
			
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	
	    	MyGlobals Globals = ((MyGlobals)getApplication());
	    	String action =  Globals.getValue("action");
	    	
	    	if (action == "signin")
			{
			
	    		if (MyProvider.printer_connected == false)
	    		{
	    			no_network = true;
	    			error_message = getString(R.string.text_error_print);
	    			// "LabelPrinterService not available! \n Please check WIFI and/or Internet connection. \n Make sure that Service is running."
	    		}	
			}    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(PrintBatchActivity.this, getString(R.string.text_error), error_message);
	    			    // Error
	    			 	            		   	 
	            	} 
	            	         	   	
	              }
	        });
	         
	    }

	}
	

	
}
