package com.sharethatdata.digivisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

public class SelectOrganisationActivity extends Activity {

	private static final String TAG = "";
    private Button btnSubmit1;
    private Button btnSubmit2;
    
    MyGlobals Globals;
    
    public static void setLanguage(Context context, String languageToLoad) {
        Log.d(TAG, Integer.toString(R.string.text_set_language));
        // "setting language"
        //
        Locale locale = new Locale(languageToLoad); //e.g "sv"
        Locale systemLocale = Locale.getDefault();
        if (systemLocale != null && systemLocale.equals(locale)) {
           Log.d(TAG,  Integer.toString(R.string.text1_set_language));
           // "Already correct language set"
           return;
        }
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Log.d(TAG, Integer.toString(R.string.text2_set_language) );
        // "Language set"
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_organisation);
		
		Globals = ((MyGlobals)getApplication());

		addListenerOnButton();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");
		
		if( isAdministrator == "yes" ){
			getMenuInflater().inflate(R.menu.select_organisation, menu);
		} else if(isDeveloper == "yes"){
			getMenuInflater().inflate(R.menu.select_organisation, menu);
		}else {
			
			}
		
		
			

		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	 
	  // get the selected dropdown list value
	  public void addListenerOnButton() {
	 
		btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
	 
		btnSubmit1.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			 Globals = ((MyGlobals) getApplicationContext());
		     Globals.setValue("organisation","newton");
		     
		     
							  
			  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
		
		btnSubmit2 = (Button) findViewById(R.id.btnSubmit2);
		 
		btnSubmit2.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  Globals.setValue("organisation","robopharma");
							  
			  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
	  }
	  
	
	  
	  public class LanguageOnItemSelectedListener implements OnItemSelectedListener {
		  
		  public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
		
			  // langauge selected
			  setLanguage(getBaseContext(), parent.getItemAtPosition(pos).toString());
		  }

		  @Override
		  public void onNothingSelected(AdapterView<?> arg0) {
			  // TODO Auto-generated method stub
			
		  }
		 
		
	  }
}


