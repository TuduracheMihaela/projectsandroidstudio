package com.sharethatdata.digivisitor;


import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Build;

public class SelectCountryActivity extends Activity {

	private Button btnSubmit1;
	private Button btnSubmit2;
	private Button btnSubmit3;
	    
	MyGlobals Globals;
	
    String email;
    String password;
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_country);

		addListenerOnButton();
	}
	
	  // get the selected dropdown list value
	  public void addListenerOnButton() {
	 
		btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
	 
		btnSubmit1.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			// Globals = ((MyGlobals) getApplicationContext());
		     //Globals.setValue("language","nl");
				
		     Locale mLocale = new Locale("nl");
		     Locale.setDefault(mLocale);
		     Configuration config = getBaseContext().getResources().getConfiguration();
		     if(!config.locale.equals(mLocale))
		     {
		    	 config.locale = mLocale;
		    	 getBaseContext().getResources().updateConfiguration(config, null);
		     }
		     
			  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
			  boolean blOrganisation = sharedPrefs.getBoolean("prefOrganisation", true);
					  if (blOrganisation)
					  {					
						  Intent intent = new Intent(v.getContext(), SelectOrganisationActivity.class); 
						  startActivity(intent);
					  } else {
						  //Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent);
						  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
						  startActivity(intent); 
					  }  			   			  
				  
		  }
	 
		});
		
		btnSubmit2 = (Button) findViewById(R.id.btnSubmit2);
		 
		btnSubmit2.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			 // MyGlobals Globals = ((MyGlobals)getApplication());
			  //Globals.setValue("language","uk");
			  
			     Locale mLocale = new Locale("en");
			     Locale.setDefault(mLocale);
			     Configuration config = getBaseContext().getResources().getConfiguration();
			     if(!config.locale.equals(mLocale))
			     {
			    	 config.locale = mLocale;
			    	 getBaseContext().getResources().updateConfiguration(config, null);
			     }
							  
				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
				  boolean blOrganisation = sharedPrefs.getBoolean("prefOrganisation", true);
						  if (blOrganisation)
						  {					
							  Intent intent = new Intent(v.getContext(), SelectOrganisationActivity.class); 
							  startActivity(intent);
						  } else {
							  //Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent);
							  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
							  startActivity(intent); 
						  }  
		  }
	 
		});
		
		btnSubmit3 = (Button) findViewById(R.id.btnSubmit3);
		 
		btnSubmit3.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			 // MyGlobals Globals = ((MyGlobals)getApplication());
			  //Globals.setValue("language","uk");
			  
			     Locale mLocale = new Locale("de");
			     Locale.setDefault(mLocale);
			     Configuration config = getBaseContext().getResources().getConfiguration();
			     if(!config.locale.equals(mLocale))
			     {
			    	 config.locale = mLocale;
			    	 getBaseContext().getResources().updateConfiguration(config, null);
			     }
							  
				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
				  boolean blOrganisation = sharedPrefs.getBoolean("prefOrganisation", true);
						  if (blOrganisation)
						  {					
							  Intent intent = new Intent(v.getContext(), SelectOrganisationActivity.class); 
							  startActivity(intent);
						  } else {
							  //Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent);
							  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
							  startActivity(intent); 
						  }  
		  }
	 
		});
		
	  }
}
