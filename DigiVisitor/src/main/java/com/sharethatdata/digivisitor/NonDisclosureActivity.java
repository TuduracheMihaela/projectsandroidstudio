package com.sharethatdata.digivisitor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NonDisclosureActivity extends Activity {
	
	Button btnSubmit;
	TextView textNonDisclosureView;
	
	String textBuffer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_non_disclosure);
		
		
		 btnSubmit = (Button) findViewById(R.id.btnSubmit);
		 textNonDisclosureView = (TextView) findViewById(R.id.textDescription);
		 
			SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
			String textFromSettings = sharedPrefs.getString("prefNonDisclosureText", "");
			
			// Set up the text.
			textBuffer = textFromSettings;
			textNonDisclosureView.setText(textBuffer);
		 
		 btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				
				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
				  boolean blPrintLabel = sharedPrefs.getBoolean("prefPrintLabel", true);
				  if (blPrintLabel)
				  {					
					  Intent intent = new Intent(v.getContext(), PrintBatchActivity.class); 
					  startActivity(intent);
				  } else {
					 // Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent); 
					  Intent intent = new Intent(v.getContext(), SalesActivity.class); 
					  startActivity(intent);
				  }  			   			  
			  
			  }
		 
		});
	}



}
