package com.sharethatdata.digivisitor.notification;

import com.sharethatdata.digivisitor.R;

//import android.app.NotificationManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

public class MyReceiverReservation extends BroadcastReceiver
{

	 @Override
	 public void onReceive(Context context, Intent intent) {
/*		 
		 Intent service1 = new Intent(context, MyAlarmServiceToday.class);
	       context.startService(service1);*/
		 
		 Log.d("ME1", "Notification started for reservation");

	        NotificationCompat.Builder mBuilder =
	            new NotificationCompat.Builder(context)
	            .setSmallIcon(R.drawable.ic_launcher)
	            .setContentTitle("DigiVisitor")
	            .setContentText("You have a meeting");
	             mBuilder.setAutoCancel(true);

	        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	        mNotificationManager.notify(1, mBuilder.build());
	 }
}
