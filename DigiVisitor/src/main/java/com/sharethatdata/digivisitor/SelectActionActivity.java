package com.sharethatdata.digivisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

public class SelectActionActivity extends Activity {

	private static final String TAG = "";
    private Button btnSubmit1;
    private Button btnSubmit2;
    private Button btnSubmit3;
    
	MyGlobals Globals;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_action);
		
		Globals = ((MyGlobals)getApplication());
		
		String isManager = Globals.getValue("manager");
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");

		addListenerOnButton();
		
		// set visible buttons
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		boolean blPreSignIn = sharedPrefs.getBoolean("prefPreSignIn", true);
		boolean blSignIn = sharedPrefs.getBoolean("prefSignIn", true);
		boolean blSignOut = sharedPrefs.getBoolean("prefSignOut", true);
		
		if( isManager == "yes" )
		{
			if (blPreSignIn) btnSubmit3.setVisibility(View.VISIBLE); else btnSubmit3.setVisibility(View.INVISIBLE); 
		} else if( isAdministrator == "yes")
		{
			if (blPreSignIn) btnSubmit3.setVisibility(View.VISIBLE); else btnSubmit3.setVisibility(View.INVISIBLE); 
		} else if( isDeveloper == "yes" ) 
		{
			if (blPreSignIn) btnSubmit3.setVisibility(View.VISIBLE); else btnSubmit3.setVisibility(View.INVISIBLE); 
		} else if( isEmployee == "yes" )
		{
			if (blPreSignIn) btnSubmit3.setVisibility(View.VISIBLE); else btnSubmit3.setVisibility(View.INVISIBLE); 
		} else 
		{
			if (blPreSignIn) btnSubmit3.setVisibility(View.INVISIBLE); else btnSubmit3.setVisibility(View.INVISIBLE); 
		}
		
		
		if (blSignIn) btnSubmit1.setVisibility(View.VISIBLE); else btnSubmit1.setVisibility(View.INVISIBLE); 
		
		if (blSignOut) btnSubmit2.setVisibility(View.VISIBLE); else btnSubmit2.setVisibility(View.INVISIBLE); 
	}
	 
	  // get the selected dropdown list value
	  public void addListenerOnButton() {
	 
		btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
	 
		btnSubmit1.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  Globals = ((MyGlobals) getApplicationContext());
			  Globals.setValue("action", "signin");

			  Intent intent = new Intent(v.getContext(), SignInActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
		
		btnSubmit2 = (Button) findViewById(R.id.btnSubmit2);
		 
		btnSubmit2.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  Globals = ((MyGlobals) getApplicationContext());
			  Globals.setValue("action","signout");
			  
			  Intent intent = new Intent(v.getContext(), SignInActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
		
		btnSubmit3 = (Button) findViewById(R.id.btnSubmit3);
		 
		btnSubmit3.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  Globals = ((MyGlobals) getApplicationContext());
			  Globals.setValue("action","schedule");
			 
			  Intent intent = new Intent(v.getContext(), SignInActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
	  }
	  
	  
}


