package com.sharethatdata.digivisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digivisitor.DataModel.cVisitor;
import com.sharethatdata.digivisitor.DataModel.cGetUserRoles;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Global;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;

public class IntroActivity extends Activity {

	MyGlobals Globals;
	
    String email;
    String password;
    
    SearchView searchView;
    SearchManager searchManager;
    
    WSDataProvider MyProvider;
    
	List<cGetUserRoles> rolesList;
	ArrayList<HashMap<String, String>> userList;
	
	private String myUser = "";
	private String myPass = "";
	String role;
	
    private boolean no_network = false;
    private String error_message = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_intro);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
			
		Globals = ((MyGlobals)getApplication());
	
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
	    Button btnSubmit = (Button) findViewById(R.id.buttonStart);
	     		    
		 btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				  
				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
				  boolean blLanguage = sharedPrefs.getBoolean("prefLanguage", true);
				  boolean blOrganisation = sharedPrefs.getBoolean("prefOrganisation", true);
				  if (blLanguage)
					  {
						  Intent intent = new Intent(v.getContext(), SelectCountryActivity.class); 
						  startActivity(intent);
						  
					  } else {
						  if (blOrganisation)
						  {					
							  Intent intent = new Intent(v.getContext(), SelectOrganisationActivity.class); 
							  startActivity(intent);
							  
						  } else {
							  //Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent);
							  Intent intent = new Intent(v.getContext(), SelectActionActivity.class); 
							  startActivity(intent); 
						  }  			   			  
					  } 
				  
			  }
		 
			});
			 
		 if (getIntent().getBooleanExtra("EXIT", false)) {
		    finish();
		 }
		 
		 new LoadUserList().execute();
		 
		 invalidateOptionsMenu();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		String isManager = Globals.getValue("manager");
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");
		
		System.out.println("isManager: " + isManager );
		System.out.println("isAdministrator: " + isAdministrator );
		System.out.println("isDeveloper: " + isDeveloper );
		System.out.println("isEmployee: " + isEmployee );
		
		// default
		//if(isEmployee == "yes")
		//{
			if(isAdministrator == "yes" || isDeveloper == "yes")
			{
				getMenuInflater().inflate(R.menu.intro1, menu);
				System.out.println("intro1 " );
				
			} else if(isManager == "yes"){ 
			
				getMenuInflater().inflate(R.menu.intro_manager, menu);
				System.out.println("intro_manager " );
			
			}else {
				getMenuInflater().inflate(R.menu.intro, menu);
				System.out.println("intro " );
			}	
		//}
		//else 
		//{
			//getMenuInflater().inflate(R.menu.intro, menu);
			System.out.println("intro " );
		//}

		return true;
	}
	
	  
	public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle item selection
	        switch (item.getItemId()) {
	        case R.id.action_settings:
	            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
	            return true;
	        /*case R.id.action_users:
	        	MyGlobals Globals = ((MyGlobals)getApplication());
	    	    Globals.setValue("list", "contacts");
	            intent = new Intent(this, ListActivity.class); startActivity(intent); 
	            return true;*/
	        case R.id.action_resources:
	        	Globals = ((MyGlobals)getApplication());
	    	    Globals.setValue("list", "resources");
	            intent = new Intent(this, ListActivity.class); startActivity(intent); 
	            return true;
	        case R.id.action_visits:
	        	Globals = ((MyGlobals)getApplication());
	    	    Globals.setValue("list", "visits");
	            intent = new Intent(this, ListActivity.class); startActivity(intent); 
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	Globals = ((MyGlobals) getApplicationContext());		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider.getContacts();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.username);

                String usernameContact = entry.username;
                
                if(usernameContact.contains(myUser)){
                	String idContact = entry.id;
                	System.out.println(" " + idContact);
                	
                	SharedPreferences.Editor pref = getSharedPreferences("PREF_ID_CONTACT", MODE_PRIVATE).edit();
                	pref.putString("idContact", idContact);
                	pref.commit();
                	
                }               
                // adding HashList to ArrayList
                userList.add(map);
             }

    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) { 
	         invalidateOptionsMenu();
    	}
	}
		    
}
