package com.sharethatdata.digivisitor;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sharethatdata.digivisitor.DataModel.cContact;
import com.sharethatdata.digivisitor.DataModel.cVisitor;
import com.sharethatdata.digivisitor.SignInActivity.LoadList;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class CreateReservationActivity extends Activity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;
	
	//List<String> itemList;
	ArrayList<HashMap<String, String>> itemList;
	List<cVisitor> visitorlist;
	List<String> memberlist;
	
	Spinner spinner = null;
	
    private boolean no_network = false;
    private boolean no_register = false;
    private boolean user_selected = false;
    private boolean input_correct = false;
    private String error_message = "";
    private cVisitor rememberedVisit = null;
    
    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    
    private String action = "";
    
    private EditText mEmailView;
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_reservation);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
	
		Globals = ((MyGlobals) getApplicationContext());
		action =  Globals.getValue("action");
		myUser =  Globals.getValue("user");
		myPass =  Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
							
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
		// set listener on reservation room
		final EditText et = (EditText)findViewById(R.id.textReservationRoom);
		et.setFocusable(true);
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
		    @Override
		    public void onFocusChange(View v, boolean hasFocus)
		    {
		        if (!hasFocus) {
		        	
		        	// do search
		        	//new LoadVisitorList().execute();
		        }
		    }
		});
		
		// set listener on employee
		final EditText t = (EditText)findViewById(R.id.textNewMember);
		t.setFocusable(true);
		t.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
		    @Override
		    public void onFocusChange(View v, boolean hasFocus)
		    {
		        if (!hasFocus) {
		        	
		        	String n = t.getText().toString();
		        
	        		// do search
	        		new LoadList().execute();
	        
		        }
		    }
		});
		
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    
	    TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
        
        TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
        endTimeView.setText(new StringBuilder().append(pad(mStartHour + 1)).append(":").append(pad(mStartMinute)));
                
        TextView startDateView = (TextView) findViewById(R.id.dateStartReservationText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
            .append(pad(mStartMonth + 1)).append("-")
            .append(pad(mStartYear)));
	    
        // create member list
        memberlist = new ArrayList<String>();
        
	    Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
			 
		btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				
				  // create reservation
				  
				  
				  // close
				  //Intent intent = new Intent(CreateReservationActivity.this, ThankYouActivity.class); startActivity(intent);
				  Intent intent = new Intent(CreateReservationActivity.this, SalesActivity.class); startActivity(intent);
			  }
		 
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_in, menu);
		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
		private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	    		        
	            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	        }
	    };
    
	    private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	    		        
	            endTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	        }
	    };
	    
		private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
		            mStartYear = year;
		            mStartMonth = month;
		            mStartDay = day; 
		            		
		            TextView startDateView = (TextView) findViewById(R.id.dateStartReservationText);
		    		        
		            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));
		    }
		};

		
	public void startTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
	    tp1.show();
	}
	
	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    dp1.show();
	}
		
	public void endTimeClick(View v) 
	{
		TimePickerDialog tp1 = new TimePickerDialog(this, mEndTimeSetListener, mStartHour, mStartMinute, true);
	    tp1.show();
	}
	
	
		
	private boolean IsInputCorrect()
	{
		// fields filled
		boolean fields_filled = true;
			
		EditText mView = (EditText) findViewById(R.id.textOrganisation);
	    String organisation = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textName);
	    String surname = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textLastName);
	    String lastname = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textFunction);
	    String function = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textEmail);
	    String email = mView.getText().toString();
   		mView = (EditText) findViewById(R.id.textPhone);
   		String phone = mView.getText().toString();
   		
   		if(organisation.equalsIgnoreCase("")
		   || surname.equalsIgnoreCase("")
		   || lastname.equalsIgnoreCase("")
		   || function.equalsIgnoreCase("")
		   || email.equalsIgnoreCase("")
		   || phone.equalsIgnoreCase("")
		   )
		{
   			fields_filled = false;
		    Toast.makeText(CreateReservationActivity.this,getString(R.string.text1_toast_create_reservation), 
		         Toast.LENGTH_SHORT).show();
		    //  "Alle velden moeten worden ingevuld."
		}
		if (!checkemail(email))
		{
			fields_filled = false;
		    Toast.makeText(CreateReservationActivity.this,getString(R.string.text2_toast_create_reservation) , 
			         Toast.LENGTH_SHORT).show();
		    // "Ongeldig emailadres."
		}
		if (!user_selected)
		{
		    Toast.makeText(CreateReservationActivity.this, getString(R.string.text3_toast_create_reservation), 
			         Toast.LENGTH_SHORT).show();	
		    // "Onbekende medewerker."
		}
		
				
		// user selected
		return user_selected && fields_filled;
		
	}
	

	public boolean checkemail(String email)
	{
	    Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
	    Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	}
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	         
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	   // 	itemList = MyProvider.getContacts();
	   
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	         	//"Network connection not available! \n Please check WIFI and/or Internet connection."
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(CreateReservationActivity.this, getString(R.string.text_error), error_message);
	    			    // Error
	    			 	            		   	 
	            	} 
	            
	            	// search through list and get the names of matches
	            	EditText mView = (EditText) findViewById(R.id.textNewMember);
	    	   		String[] EmployeeTexts = mView.getText().toString().toLowerCase().split(" ");
	    	   			
	    	   		if (itemList.size() > 0)
	            	{
	    	   			final List<String> user_list = new ArrayList<String>();
		          
		    	   		for (HashMap<String, String> map : itemList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "name")
		           	        	{
		           	        		String name = entry.getValue();
		           	        		boolean found = false;
				            		for(String etext : EmployeeTexts)
				            		{
				            			if (name.toLowerCase().contains(etext) && (!etext.equals("van") && !etext.equals("der") && !etext.equals("de")))
				            			{
				            				found = true;        			
				            			}
				            		}
				            		if (found) user_list.add(name);        
		           	        		
		           	        	}
		           	        }
	    	   	  
//	            	if (itemList.size() > 0)
//	            	{
//		            	final List<String> user_list = new ArrayList<String>();
//		            	// create array of string
//		            	for(String v : itemList)
//		            	{
//		            		boolean found = false;
//		            		for(String etext : EmployeeTexts)
//		            		{
//		            			if (v.toLowerCase().contains(etext) && (!etext.equals("van") && !etext.equals("der") && !etext.equals("de")))
//		            			{
//		            				found = true;        			
//		            			}
//		            		}
//		            		if (found) user_list.add(v);        			
//		            		
//		            	}
		            	
		            	if (user_list.size() > 0)
		            	{
			            	CharSequence[] items = user_list.toArray(new String[user_list.size()]);
			           
			            	 // 1. Instantiate an AlertDialog.Builder with its constructor
			                AlertDialog.Builder builder = new AlertDialog.Builder(CreateReservationActivity.this);
			
			                // 2. Chain together various setter methods to set the dialog characteristics
			                builder.setTitle(getString(R.string.text_title_alert_dialog_create_rezervation))
			                // "Bevestig uitnodigen medewerker"
			                		.setItems(items, new DialogInterface.OnClickListener() {
			                		   	public void onClick(DialogInterface dialog, int which) {
					                    // The 'which' argument contains the index position
					                    // of the selected item
			                		   	
			                		   		// find object
			                		   		int i = 0;
			                		   		String myEmployee = "";
			                		   		for(String v : user_list)
			                		   		{
			                		   			if (i == which)
			                		   			{
			                		   				myEmployee = v;
			                		   			}
			                		   			i++;
			                		   		}
			                		   		
			                		   		memberlist.add(myEmployee);
			                		   		
			                		   		user_selected = true;
			                		   		
			                		   	}
			                	   });
			                
			
			                // 3. Get the AlertDialog from create()
			                AlertDialog dialog = builder.create();
			                dialog.show();
		            	} else {
		            				            		
		            	}
		            	
	            	}
	            	
	              }
	        });
	         
	    }

	}

}
