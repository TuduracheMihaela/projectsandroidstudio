package com.sharethatdata.digivisitor;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SimpleAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sharethatdata.webservice.ImageProcessor;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cEmployee;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cVisitEntries;

public class ListActivity extends Activity implements OnQueryTextListener {

	// Progress Dialog
    private ProgressDialog pDialog;

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider1 = null;
	
	ArrayList<HashMap<String, String>> itemList;
	ArrayList<HashMap<String, String>> userList;
	List<cContact> employeeList;
	List<cVisitEntries> visitList;
	
	List<String> userkeyArray =  new ArrayList<String>();
	ArrayList<String> list = new ArrayList<String>();
	
	private String report_user = "";
	private boolean createHasRun = false;
	
	ListView lv = null;
	//ListAdapter adapter = null;
	SimpleAdapter adapter = null;
	   
    private boolean no_network = false;
    private String error_message = "";
    
    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    private String list_title = "";
    
    SearchView searchView;
    SearchManager searchManager;
    
    // digivisitor
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndYear,mEndMonth,mEndDay;
	private String startDate;
	private String endDate;
    
    Spinner userSpinner = null;
    private boolean userIsInteracting = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
	
		String list_type = Globals.getValue("list");
		list_name = list_type;
			
		if (list_name == "visits")
		{
			setContentView(R.layout.activity_list_visits);
		} else {
			setContentView(R.layout.activity_list);
		}
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider1 = new WSDataProvider(myUser, myPass);
					
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyProvider1.SetWebServiceUrl(Globals.getValue("ws_url"));
		
	
		
		lv = (ListView) findViewById(R.id.listView);
		userSpinner = (Spinner) findViewById(R.id.spinnerUser);
			    
		TextView titleText = (TextView) findViewById(R.id.title);
	
		if (list_name == "resources") list_title = getString(R.string.title_activity_list_resources);
		if (list_name == "contacts") list_title = getString(R.string.title_activity_list_contacts);
		if (list_name == "visits") list_title = getString(R.string.title_activity_list_visits);
	
		setTitle(list_title);
		titleText.setText(list_title);
	
		if (list_name == "visits")
		{
			Calendar c = Calendar.getInstance();
		    c.add(Calendar.DATE, -7); 
            mStartYear = c.get(Calendar.YEAR);
      		mStartMonth = c.get(Calendar.MONTH);
      		mStartDay = c.get(Calendar.DAY_OF_MONTH);
      		startDate = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
			
	        c.add(Calendar.DATE, +7); 
    	    mEndYear = c.get(Calendar.YEAR);
    		mEndMonth = c.get(Calendar.MONTH);
    		mEndDay = c.get(Calendar.DAY_OF_MONTH);
			endDate = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
						
			
		}
	//	new LoadList().execute();
	
		String isManager = Globals.getValue("manager");
		
		if(list_name == "visits"){
						
			if (isManager == "yes")
			{	
				Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
				sUsers.setVisibility(Spinner.VISIBLE);
				
				new LoadEmployeeList().execute();
				
			} else {
				Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
				sUsers.setVisibility(Spinner.INVISIBLE);
				
				report_user = "0";
			}
	
			
			
		}
		

		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    String strText = ((TextView) itemClicked.findViewById(R.id.textViewName)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("pid", pid);
			    Globals.setValue("name", strText);
			    Globals.setValue("action", list_name);
				
			    // start detail activity
				Intent intent = new Intent(getApplicationContext(), DetailActivity.class); startActivity(intent); 
				
			}

		});	
		
		if (list_name == "visits")
		{
	
		
			userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			 @Override
			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			    	
			    	if (userIsInteracting) {
				  		
			       		int myIndex = userSpinner.getSelectedItemPosition();
			  	   		report_user = userkeyArray.get(myIndex);
			  	  	
			  	        new LoadList().execute();
			    	}
			    }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
				}
			});
	
		    Button prevButton = (Button) findViewById(R.id.btnPrev);
		    Button nextButton = (Button) findViewById(R.id.btnNext);
			    
		    TextView startDateView = (TextView) findViewById(R.id.dateDayText);
		    startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)).append(" - ").append(pad(mEndDay)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndYear)));
		   		    
			prevButton.setOnClickListener(new View.OnClickListener() {
	
	            @Override
	            public void onClick(View v) {
	                
	            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	            	TextView startDateView = (TextView) findViewById(R.id.dateDayText);
	        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	        		String weekdayname = "";
	        		
	                try {
	                	Date myDate = dateFormat.parse(startDateView.getText().toString());
	                    
	                    Calendar c = Calendar.getInstance();
	                    c.setTime(myDate);
	                    // set end date = old start date
	                    mEndYear = c.get(Calendar.YEAR);
	            		mEndMonth = c.get(Calendar.MONTH);
	            		mEndDay = c.get(Calendar.DAY_OF_MONTH);
	            		// set new start date
	                    c.add(Calendar.DATE, -7); 
	            	    mStartYear = c.get(Calendar.YEAR);
	            		mStartMonth = c.get(Calendar.MONTH);
	            		mStartDay = c.get(Calendar.DAY_OF_MONTH);
	            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	            		    	        	       
	            		
	                } catch (ParseException e) {
	                    	
	                }
	              
	          //      startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
	           //     startDayView.setText(weekdayname);
	                
	                SetNewDateRange(-7);
	                new LoadList().execute();
	        	    
	            }
	
	        });
			
			nextButton.setOnClickListener(new View.OnClickListener() {
	
	            @Override
	            public void onClick(View v) {
	            
	            	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	            	TextView startDateView = (TextView) findViewById(R.id.dateDayText);
	        		TextView startDayView = (TextView) findViewById(R.id.dateDayText);
	                String weekdayname = "";
	        				
	                try {
	                	Date myDate = dateFormat.parse(startDateView.getText().toString());
	                    
	                    Calendar c = Calendar.getInstance();
	                    c.setTime(myDate);
	                    mStartYear = mEndYear;
	            		mStartMonth = mEndMonth;
	            		mStartDay = mEndDay;
	                    c.add(Calendar.DATE, 14); 
	            	    mEndYear = c.get(Calendar.YEAR);
	            		mEndMonth = c.get(Calendar.MONTH);
	            		mEndDay = c.get(Calendar.DAY_OF_MONTH);
	            		String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
	            		weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
	            	
	            		
	                } catch (ParseException e) {
	                    	
	                }
	              
	               //  startDayView.setText(weekdayname);
	                
	                SetNewDateRange(7);
	                new LoadList().execute();
	            }
	
	        });
		}
		createHasRun = true;
		
	}
	
	public void SetNewDateRange(int diff)
	{
	//	if (diff < 0)
	//	{
			//endDate = startDate;
			startDate = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
//		} else {	
		//	startDate = endDate;
			endDate = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
			
//		}
		TextView startDateView = (TextView) findViewById(R.id.dateDayText);
	    startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)).append(" - ").append(pad(mEndDay)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndYear)));
		     
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		if (list_name == "visits"){
			
			getMenuInflater().inflate(R.menu.search_visits, menu);
			
			MenuItem searchItem = menu.findItem(R.id.search);
			searchView = (SearchView) searchItem.getActionView();
			setupSearchView(searchItem);
			searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
			
			ComponentName cn = new ComponentName(this, ListActivity.class);
			searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
			
			//searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			searchView.setIconifiedByDefault(false);
		
		}
		if (list_name == "contacts"){
			
			getMenuInflater().inflate(R.menu.contacts, menu);
		
		}
		
		return true;
	}
	

	@Override
	public void onResume() {
	    super.onResume();
	    
	    if (createHasRun) {
	    	new LoadList().execute();
	    }
	    
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_new:
        	
            Globals.setValue("pid", "");
            
            Intent intent = new Intent(this, EditDetailActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(ListActivity.this);
         pDialog.setMessage( getString(R.string.text1_alert_dialog) + " " + list_title + " " + getString(R.string.text2_alert_dialog));
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   
	    	error_message = "";
	    	
	    	itemList = new ArrayList<HashMap<String, String>>();
	    	
	    	// load data from provider
	    	if (list_name == "contacts")
	    	{
	    		// convert objects to hashmap for list
	    		List<cContact> List = MyProvider.getContacts();
	    	
	    		for(cContact entry : List)
             	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("pid", entry.id);
	                map.put("name", entry.name);
	                map.put("subtitle", entry.function);
	                map.put("status", entry.status_id);
	                
	                
	              //  test 
	                 
	                // adding HashList to ArrayList
	                itemList.add(map);
	             }
	    	}
	    	if (list_name == "resources")
	    	{
	    		List<cResource> List = MyProvider.getResources("0", ""); // i need a list here
	    		for(cResource entry : List)
             	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("pid",  entry.id);
	                map.put("name", entry.name);
	                map.put("subtitle", entry.description);
	                map.put("status", entry.status);
	                 
	                // adding HashList to ArrayList
	                itemList.add(map);
	             }
	    	
	    	}
	    	if (list_name == "visits")
	    	{
	    	
				UIHelper myUIHelper = new UIHelper();
				
				// if report user = 0 , my own user
				if (report_user == "0")
				{
					String this_organisation = Globals.getValue("organisation");
					cContact myContact = MyProvider.getUserID(myUser);
					report_user = myContact.id;
				}
			    // no start end will result in current date +7 days and 7 days back
	    		List<cVisitEntries> List = MyProvider.getVisits(startDate, endDate, report_user);
	    		for(cVisitEntries entry : List)
             	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("pid",  entry.id);
	                map.put("name", entry.organisation);
	                map.put("subtitle", entry.surname + " " + entry.lastname);
	                map.put("status", entry.employee);
	                
	                String formatted_datetime = myUIHelper.SetFormattedDate(entry.startdatetime);
	                
	                map.put("datetime", formatted_datetime);
	                
	                 
	                // adding HashList to ArrayList
	                itemList.add(map);
	             }
	    		
	    	}
	    	
	    	//if (itemList.isEmpty())	error_message = MyProvider.last_error;
	 
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	        pDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	if (error_message != "")
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(ListActivity.this,getString(R.string.text_error), error_message);
	    			} 
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	 adapter = null;
	            	if (list_name == "messages" || list_name == "news")
	            	{	            		
	            		 adapter = new SimpleAdapter(
		                		ListActivity.this
		                		, itemList
		                		, R.layout.sublistview_item, new String[] { "status", "pid", "name", "subtitle" },
	                            new int[] { R.id.textViewDateTime, R.id.textViewID, R.id.textViewName, R.id.textViewFunc });
		                
	            	} else if(list_name == "visits" ){
	            		 adapter = new SimpleAdapter(
			                		ListActivity.this
			                		, itemList
			                		, R.layout.listview_item, new String[] { "img", "pid" , "name" , "subtitle" , "datetime", "status"},
		                            new int[] { R.id.iv_img, R.id.textViewID, R.id.textViewName , R.id.textViewFunc , R.id.textViewFunc1 , R.id.textViewStatus });
	            	}
	            	else {
	            		
	            		 adapter = new SimpleAdapter(
		                		ListActivity.this
		                		, itemList
		                		, R.layout.listview_item, new String[] { "img", "pid", "name", "subtitle", "status" },
	                            new int[] { R.id.iv_img, R.id.textViewID, R.id.textViewName, R.id.textViewFunc, R.id.textViewStatus });
		                
	            	}
	                		
	                
	                // updating listview
	                lv.setAdapter(adapter);
	                
	                // display images in row
	            	if (list_name != "messages" && list_name != "news")
	            	{	   
		                for(int i=0;i<adapter.getCount();i++){
		                    
	                        HashMap<String, Object> hm = (HashMap<String, Object>) adapter.getItem(i);
	                        String imgUrl = (String) hm.get("pid");
	                        ImageLoaderTask imageLoaderTask = new ImageLoaderTask();
	         
	                     //   HashMap<String, Object> hmDownload = new HashMap<String, Object>();
	                        hm.put("img_path", list_name + "/" + imgUrl);
	                        hm.put("position", i);
	         
	                        // Starting ImageLoaderTask to download and populate image in the listview
	                        imageLoaderTask.execute(hm);
	                    }
		            }
	            }
	        });
	         
	    }

	}
	
	  /** AsyncTask to download and load an image in ListView */
    private class ImageLoaderTask extends AsyncTask<HashMap<String, Object>, Void, HashMap<String, Object>>{
 
    	
        @Override
        protected HashMap<String, Object> doInBackground(HashMap<String, Object>... hm) {
 
        	ImageProcessor myImgProcessor = new ImageProcessor(getBaseContext());
			myImgProcessor.SetWebServiceUrl(Globals.getValue("ws_url"));

        	return myImgProcessor.LoadImage(hm);
        	 
        }
 
        @Override
        protected void onPostExecute(HashMap<String, Object> result) {
            
        	if (result != null)
        	{
	        	// Getting the path to the downloaded image
	            String path = (String) result.get("img");
	 
	            // Getting the position of the downloaded image
	            int position = (Integer) result.get("position");
	 
	            // Getting adapter of the listview
	            SimpleAdapter adapter = (SimpleAdapter ) lv.getAdapter();
	 
	            // Getting the hashmap object at the specified position of the listview
	            HashMap<String, Object> hm = (HashMap<String, Object>) adapter.getItem(position);
	 
	            // Overwriting the existing path in the adapter
	            hm.put("img",path);
	 
	            // Noticing listview about the dataset changes
	            adapter.notifyDataSetChanged();
	        } else {
	        	
	       // 	ImageView image = (ImageView) findViewById(R.id.iv_img);
	       // 	image.setImageDrawable(image.getContext().getResources().getDrawable(R.drawable.person_small));
	        	
	       // 	if (list_name == "resources") image.setImageDrawable(image.getContext().getResources().getDrawable(R.drawable.meeting_room_small));
	       // 	if (list_name != "resources") image.setImageDrawable(image.getContext().getResources().getDrawable(R.drawable.person_small));

	        }
        }
        
    }
    
    /**
  	 * Background Async Task to Load all users by making HTTP Request
    	 * */
  	class LoadEmployeeList extends AsyncTask<String, String, String> {

       /**
        * Before starting background thread Show Progress Dialog
        * */
       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           

       }

  	    /**
  	     * getting items from url
  	     * */
  	    protected String doInBackground(String... args) {
  	    
  	    	employeeList = MyProvider1.getContacts();
  	   
  	    	return "";
  	    }
  	
  	    /**
  	     * After completing background task Dismiss the progress dialog
  	     * **/
  	    protected void onPostExecute(String file_url) {
  	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	                    List<String> userArray =  new ArrayList<String>();
		                for(cContact entry : employeeList)
		             	{
		           	        userkeyArray.add(entry.id);
		            		userArray.add(entry.name);
		            	}
	            	     	
		            	ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(ListActivity.this, android.R.layout.simple_spinner_item, userArray);
	           	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
	           	    	sUsers.setAdapter(user_adapter);
	           	    	
//	           	    	if(userSpinnerIndex != -1) 
//	           	    	  // set the value of the spinner 
//	           	    		sUsers.setSelection(userSpinnerIndex);
//	           	
	           	    	userIsInteracting = false;
		     //   	}
		           	 
	            }
	        });
  	         
  	    }

  	}
   
	@Override
	public void onUserInteraction() {
	    super.onUserInteraction();
	    userIsInteracting = true;
	}
	
    
    private void setupSearchView(MenuItem searchItem) {

        if (isAlwaysExpanded()) {
            searchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        if (searchManager != null) {
//            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();
//
//            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
//            for (SearchableInfo inf : searchables) {
//                if (inf.getSuggestAuthority() != null
//                        && inf.getSuggestAuthority().startsWith("applications")) {
//                    info = inf;
//                }
//            }
//            searchView.setSearchableInfo(info);
//        }

        searchView.setOnQueryTextListener(this);
    }

    public boolean onQueryTextChange(String newText) {
    	//lv.setAdapter(adapter);
//    	lv.setTextFilterEnabled(true);
//    	lv.setFilterText(newText.toString().trim());
    	ListActivity.this.adapter.getFilter().filter(newText.trim());
    	//Toast.makeText(this, "Query = " + newText, Toast.LENGTH_SHORT).show();
       // mStatusView.setText("Query = " + newText);
        return false;
    }

    public boolean onQueryTextSubmit(String query) {
//    	lv.setTextFilterEnabled(true);
//    	lv.setFilterText(query.toString().trim());
    	ListActivity.this.adapter.getFilter().filter(query.trim());
    	//Toast.makeText(this, "Query = " + query + " : submitted", Toast.LENGTH_SHORT).show();
       // mStatusView.setText("Query = " + query + " : submitted");
        return false;
    }

    public boolean onClose() {
    	//Toast.makeText(this, "Closed!", Toast.LENGTH_SHORT).show();

       // mStatusView.setText("Closed!");
        return false;
    }

    protected boolean isAlwaysExpanded() {
        return false;
    }

	
}
