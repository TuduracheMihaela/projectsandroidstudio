package com.sharethatdata.digivisitor;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sharethatdata.digivisitor.CreateScheduleAppointmentActivity.CreateReservationRoom;
import com.sharethatdata.digivisitor.CreateScheduleAppointmentActivity.LoadRoomList;
import com.sharethatdata.digivisitor.SignInActivity.LoadEmployeeList;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cEmployee;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cTask;
import com.sharethatdata.webservice.datamodel.cVisitEntries;
import com.sharethatdata.webservice.datamodel.cVisitKind;
import com.sharethatdata.webservice.datamodel.cVisitor;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class SignInActivity extends Activity {

	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	private int mStartHour,mStartMinute, mEndHour, mEndMinute;
	private int mStartYear,mStartMonth,mStartDay, mEndYear, mEndMonth, mEndDay;
	
	//List<String> itemList;
	List<String> visitKindKeyArray =  new ArrayList<String>();
	ArrayList<HashMap<String, String>> visitKindList;
	
	ArrayList<HashMap<String, String>> itemList;
	List<cVisitEntries> visitorlist;
	List<cContact> employeeList;
	List<cVisitEntries> visitsList;
	//ArrayList<HashMap<String, String>> itemsVisitsList;
	
	ArrayList<String> membersList;
	ArrayList<String> visitsSelectedList;
	   
	Spinner spinner = null;
	ProgressDialog progressDialog;

	private boolean myBool;
	
    private boolean no_network = false;
    private boolean registered = false;
    private boolean user_selected = false;
    private boolean input_correct = false;
    private boolean sign_all_out = false;
    private String error_message = "";
    private cVisitEntries rememberedVisit = null;
    int visitID;
    
    private String list_name = "";
    private String myUserID = "";
    private String myUser = "";
    private String myPass = "";
    private String action = "";
  
    TextView labelSelectCoWorker;
	TextView textSelectCoWorker;
	String textSelectCoWorkerString;
	String textSelectVisitsString;
	String report_organisation;
    
	private String myEmployeeID;
	
    private EditText mEmailView;
    
    TextView startTimeView, endTimeView;
    TextView startDateView, endDateView;
    
    //////////////////////////////////////
    // Meeting Room
    
    List<cResource> roomList;
    String rememberedRoom = "";
    String allday;
    String textStartDateString;
    String textTimeStartString;
	String textTimeEndString ;
	String resourceStatus="0";
    boolean isSet = false;
    boolean take_resource = false;
    boolean return_resource = false;
    boolean duplicate = false;
    
    TextView meetingRoomSelect;
    Button btnSubmit;
    LinearLayout layoutMeetingRoom;
    
    //////////////////////////////////
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		
		labelSelectCoWorker = (TextView) findViewById(R.id.textView3);
		textSelectCoWorker = (TextView) findViewById(R.id.selectCoworkerSchedule_textView);
		//textSelectCoWorkerString = textSelectCoWorker.getText().toString();
	    
	    // retrieve boolean value from ScheduleAppointmentActivity
	    SharedPreferences settings = getSharedPreferences("suc", 0);
	    myBool = settings.getBoolean("silentMode", false);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		
		myUser = sharedPrefs.getString("prefUsername", "");
	
		Globals = ((MyGlobals) getApplicationContext());
		
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
				
		action =  Globals.getValue("action");
				
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	 // getMyUserId for notification
	    LoadMyUserIdList InitMyUserIdList;
		InitMyUserIdList = new LoadMyUserIdList();
		InitMyUserIdList.execute();
		
		// set listener on lastname
		final EditText et = (EditText)findViewById(R.id.textLastName);
		et.setFocusable(true);
		et.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
		    @Override
		    public void onFocusChange(View v, boolean hasFocus)
		    {
		        if (!hasFocus) {
		        	
		        		// do search
			        	new LoadVisitorList().execute();
		        		
		        }
		    }
		});
		
		// set listener on employee
		final EditText t = (EditText)findViewById(R.id.textEmployee);
		t.setOnTouchListener(new OnTouchListener() 
		{
			@Override
			public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
				if (event.getAction() == MotionEvent.ACTION_UP) {

						// do search
			    		new LoadList().execute();
		    		
				}

				return false;
			}
		});
			
		final EditText tt = (EditText)findViewById(R.id.textOrganisation);
		tt.setFocusable(true);
		tt.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
		    @Override
		    public void onFocusChange(View v, boolean hasFocus)
		    {
		    	if (!hasFocus) {
					if (action == "signout")
				    {
			    		// do search
			    		new LoadListVisits().execute();
				    }
				}
		    }
		});
	    
				
	    mEmailView = (EditText) findViewById(R.id.textEmail);

	   	
    	// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mEndYear = c.get(Calendar.YEAR);
	    mEndMonth = c.get(Calendar.MONTH);
	    mEndDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    mEndHour = c.get(Calendar.HOUR_OF_DAY);
	    mEndMinute = c.get(Calendar.MINUTE);
	    
	    TextView dateEndLabelText = (TextView) findViewById(R.id.dateEndLabelText);
	    TextView timeEndLabelText = (TextView) findViewById(R.id.timeEndLabelText);
	    
	    startTimeView = (TextView) findViewById(R.id.timeStartText);
        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));  
        endTimeView = (TextView) findViewById(R.id.timeEndText);
        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));

        startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
            .append(pad(mStartMonth + 1)).append("-")
            .append(pad(mStartYear)));
        endDateView = (TextView) findViewById(R.id.dateEndText);
        endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
                .append(pad(mEndMonth + 1)).append("-")
                .append(pad(mEndYear)));
	    
	    CheckBox chkComp = (CheckBox) findViewById(R.id.chkComp);
	    CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
	    
	    // Meeting Room
	    textStartDateString = startDateView.getText().toString();
	    textTimeStartString = startTimeView.getText().toString();
		textTimeEndString = endTimeView.getText().toString();
	    
	    layoutMeetingRoom = (LinearLayout) findViewById(R.id.layoutMeetingRoom);
	    final TextView meetingRoomHeaderText = (TextView) findViewById(R.id.textView4);
	    final TextView meetingRoomLabelText = (TextView) findViewById(R.id.textView5);
	    meetingRoomSelect = (TextView) findViewById(R.id.selectRoomSchedule_textView);
	    final EditText editTextDescription = (EditText) findViewById(R.id.editTextDescription);
	    
	    new LoadVisitKindList().execute();
	    
	    if (action == "schedule")
        {
        	chkComp.setVisibility(View.VISIBLE);
        	chkReservation.setVisibility(View.VISIBLE);
    	    	
        	labelSelectCoWorker.setVisibility(View.VISIBLE);
    		textSelectCoWorker.setVisibility(View.VISIBLE);
    		    		
    		setTitle(getString(R.string.title_activity_schedule));
    		
    		// Meeting Room
    	    layoutMeetingRoom.setVisibility(View.GONE);
    		meetingRoomHeaderText.setVisibility(View.GONE);
    		meetingRoomLabelText.setVisibility(View.GONE);
    		meetingRoomSelect.setVisibility(View.GONE);
    		editTextDescription.setVisibility(View.GONE);
        	        	
        } else {
        	
        	chkComp.setVisibility(View.GONE);
        	chkReservation.setVisibility(View.GONE);
	    	labelSelectCoWorker.setVisibility(View.GONE);
        	textSelectCoWorker.setVisibility(View.GONE);
        	endTimeView.setVisibility(View.GONE);
        	endDateView.setVisibility(View.GONE);
        	timeEndLabelText.setVisibility(View.GONE);
        	dateEndLabelText.setVisibility(View.GONE);
        	
        	// Meeting Room
        	layoutMeetingRoom.setVisibility(View.GONE);
    		meetingRoomHeaderText.setVisibility(View.GONE);
    		meetingRoomLabelText.setVisibility(View.GONE);
    		meetingRoomSelect.setVisibility(View.GONE);
    		editTextDescription.setVisibility(View.GONE);
        }
	    ////////////////////////////////////////////////////////////
	    if (action == "signin")
	    {	   
		    Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		    btnSubmit.setText(R.string.select_action_text1);

		    setTitle(getString(R.string.title_activity_sign_in));
		    
	    }
	    if (action == "signout")
	    {	   
		    Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		    btnSubmit.setText(R.string.select_action_text2);
		    
		    setTitle(getString(R.string.title_activity_sign_out));
	    }
	    
	    btnSubmit = (Button) findViewById(R.id.btnSubmit);
	    if (action == "schedule")
	    {	   
			btnSubmit.setText(R.string.select_action_text3);
	    }	 
	    
		btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				
				  // sign out
				  if (action == "signout")
				  {
					  if (rememberedVisit != null)
					  {
						  // schedule meeting
						  new RegisterTask().execute();
						  
					  } else {
						  // visitor unknown
						  Toast.makeText(SignInActivity.this, v.getResources().getString(R.string.text1_toast_signin), 
							         Toast.LENGTH_SHORT).show();
					  }
				  } else {
				  	
					  input_correct = IsInputCorrect();
				  
					  if (user_selected)
					  {
						  if (input_correct)
						  {  
							  CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
							  if (chkReservation.isChecked())
							  {
								  if(!isSet){
									   meetingRoomSelect.setError(getString(R.string.error_field_required));
									   isSet = false;
				  				   	}else {
				  				   		if(duplicate || return_resource || take_resource){
				  				   			// register visit and continue without meeting room(no meeting room available)
				  				   			new RegisterTask().execute();
				  				   			
				  				   			
				  				   		 Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class); 
										  startActivity(intent);
				  				   		}else{
				  				   			
				  				   			// register
											  new RegisterTask().execute();
				  				   			
				  				   			// reserve meeting room
				  				   			new CreateReservationRoom().execute();
				  				   		}
				  				   	}
							  }else{
								  
								// register
								  new RegisterTask().execute();
							  }
						  }
					  } else {
						 
						  // load user
						  new LoadList().execute();
					  
					  }
				  }
			  }
		 
			});
		
		 	textSelectCoWorker.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LoadEmployeeList().execute();
			}
		});
		 	
		 	chkReservation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						// reserve a meeting room
					  	layoutMeetingRoom.setVisibility(View.VISIBLE);
			    		meetingRoomHeaderText.setVisibility(View.VISIBLE);
			    		meetingRoomLabelText.setVisibility(View.VISIBLE);
			    		meetingRoomSelect.setVisibility(View.VISIBLE);
			    		editTextDescription.setVisibility(View.VISIBLE);
					}else if(!isChecked){
					  	layoutMeetingRoom.setVisibility(View.GONE);
			    		meetingRoomHeaderText.setVisibility(View.GONE);
			    		meetingRoomLabelText.setVisibility(View.GONE);
			    		meetingRoomSelect.setVisibility(View.GONE);
			    		meetingRoomSelect.setText(getResources().getString(R.string.selectRoomSchedule_text));
			    		editTextDescription.setVisibility(View.GONE);
			    		editTextDescription.setText("");
			    		btnSubmit.setText(action);
					}
					
				}
			});
		 	
		 	
		 	meetingRoomSelect.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					new LoadRoomList().execute();	
				}
			});
		
	}

////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Shows a Progress Dialog 
	 *  
	 * @param msg
	 */
	public void showProgressDialog(String msg) 
	{
		
		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);
			
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			
			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();		
	}	
	
	
	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {
		
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();     
		    }
		}
		
		progressDialog = null;
	}	
	
////////////////////////////////////////////////////////////////////////////
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");
		
		if( isAdministrator == "yes" ){
			getMenuInflater().inflate(R.menu.sign_in, menu);
		} else if(isDeveloper == "yes"){
			getMenuInflater().inflate(R.menu.sign_in, menu);
		}else {
			
			}

		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
		private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mStartHour = hourOfDay;
	            mStartMinute = minute;
	            
	            TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
	    		        
	            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
	                    .append(pad(mStartMinute)));
	            
	            CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
				  if (chkReservation.isChecked())
				  {
					  new verifyReserveAvailability().execute();
				  }
	        }
	    };
	    
	    private TimePickerDialog.OnTimeSetListener mTimeEndSetListener = new TimePickerDialog.OnTimeSetListener() {
	        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	            mEndHour = hourOfDay;
	            mEndMinute = minute;
	            
	            TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
	    		        
	            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
	                    .append(pad(mEndMinute)));
	            
	            CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
				  if (chkReservation.isChecked())
				  {
					  new verifyReserveAvailability().execute();
				  }
	        }
	    };
    
		private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
		            mStartYear = year;
		            mStartMonth = month;
		            mStartDay = day; 
		            		
		            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
		    		        
		            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
		                .append(pad(mStartMonth + 1)).append("-")
		                .append(pad(mStartYear)));
		            
		            CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
					  if (chkReservation.isChecked())
					  {
						  new verifyReserveAvailability().execute();
					  }
		    }
		};
		
		private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
			 
			  public void onDateSet(DatePicker view, int year, int month, int day) {
		            mEndYear = year;
		            mEndMonth = month;
		            mEndDay = day; 
		            		
		            TextView startDateView = (TextView) findViewById(R.id.dateEndText);
		    		        
		            startDateView.setText(new StringBuilder().append(pad(mEndDay)).append("-")
		                .append(pad(mEndMonth + 1)).append("-")
		                .append(pad(mEndYear)));
		            
		            CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
					  if (chkReservation.isChecked())
					  {
						  new verifyReserveAvailability().execute();
					  }
		    }
		};

		
	public void startTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeSetListener, mStartHour, mStartMinute, true);
	    tp1.show();
	}
	
	public void startDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
	    dp1.show();
	}
	
	public void endTimeClick(View v) 
	{
	    TimePickerDialog tp1 = new TimePickerDialog(this, mTimeEndSetListener, mEndHour, mEndMinute, true);
	    tp1.show();
	}
	
	public void endDateClick(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateEndSetListener, mEndYear , mEndMonth, mEndDay);
	    dp1.show();
	}
	
		
	private boolean IsInputCorrect()
	{
		// fields filled
		boolean fields_filled = true;
			
		EditText mView = (EditText) findViewById(R.id.textOrganisation);
	    String organisation = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textName);
	    String surname = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textLastName);
	    String lastname = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textFunction);
	    String function = mView.getText().toString();
	    mView = (EditText) findViewById(R.id.textEmail);
	    String email = mView.getText().toString();
   		mView = (EditText) findViewById(R.id.textPhone);
   		String phone = mView.getText().toString();
   		
   		mView = (EditText) findViewById(R.id.textEmployee);
   		String employee_appointment = mView.getText().toString();
   		
   	  if (action == "signin" || action == "schedule")
   	  {
   		
	   		if(organisation.equalsIgnoreCase("")
			   || surname.equalsIgnoreCase("")
			   || lastname.equalsIgnoreCase("")
			   || function.equalsIgnoreCase("")
			   || email.equalsIgnoreCase("")
			   || phone.equalsIgnoreCase("")
			   || employee_appointment.equalsIgnoreCase("")
			   )
			{
	   			fields_filled = false;
			    Toast.makeText(SignInActivity.this, getString(R.string.text2_toast_signin), 
				         Toast.LENGTH_SHORT).show();
				    // "Alle velden moeten worden ingevuld."
			    
			}
			if (!checkemail(email))
			{
				fields_filled = false;
			    Toast.makeText(SignInActivity.this, getString(R.string.text3_toast_signin), 
				         Toast.LENGTH_SHORT).show();
			    // "Ongeldig emailadres."
			}
	   }
   	  
		if (!user_selected)
		{
		    Toast.makeText(SignInActivity.this, getString(R.string.text4_toast_signin), 
			         Toast.LENGTH_LONG).show();	
		    // "Onbekende medewerker."	
		}
		
				
		// user selected
		return user_selected && fields_filled;
		
	}
	

	public boolean checkemail(String email)
	{
	    Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
	    Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	}

	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadVisitorList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
 		// show the progress bar
 		showProgressDialog(getString(R.string.text_alert_dialog));
 		//Wait ...
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	if(sign_all_out){
	    		
	    		Calendar c = Calendar.getInstance();
	    		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    		String formattedDateNow = df.format(c.getTime());
	    		
				  visitsSelectedList = new ArrayList<String>();
				  if (textSelectVisitsString != "" && textSelectVisitsString != null)
				  {
					  String visits = textSelectVisitsString.replace("[", "");
					  visits = visits.replace("]", "");
					  String[] visits_list = visits.split(",");
					  for (String item : visits_list)
					  {
						  String[] name = item.split("\\s+");
						  //visitorlist = MyProvider.getVisitorList(report_organisation, name[1]);
						  
							//  visitsSelectedList.add(myMember.id);
						  
						  if (visitsList.size() > 0)
			            	{
			            			cVisitEntries myVisit = null;
			            			
			            			for(cVisitEntries v : visitsList)
					            	{
			            				myVisit = v;
			            				
			            				myVisit.id = v.id;
			            				myVisit.organisation = v.organisation;
			            				myVisit.lastname = v.lastname;
			            				myVisit.email = v.email;
			            				myVisit.phone = v.phone;
			            				myVisit.surname = v.surname;
			            				myVisit.function = v.function;
			            				myVisit.visit_kind = v.visit_kind;
			            				myVisit.employee_name = v.employee_name;
			            				myVisit.employee = v.employee;
			            				myVisit.enddatetime = formattedDateNow;
			            				
			            			boolean suc = MyProvider.updateVisitEntry(myVisit.id, myVisit, "signout");
			       					  if (suc)
			       					  {				
			       						  // end visit registered.
			       						  MyProvider.LogEvent(getString(R.string.text2_log_event));
			       										
			       						  registered = true;
		  
			       					  } else {
			       						  
			       						  registered = false;
			       						  error_message = MyProvider.last_error;
			       						  
			       						  // failed to register end visit
			       						  MyProvider.LogEvent(getString(R.string.text1_log_event) + report_organisation + ", " + myVisit.lastname);
			       						  
			       					  }
					            	}
			            			
			            		}
						  						    
					  }
					  
					  Log.d("VISIT","update sign out");
				  }

	    		
    			
    		}else{
    			EditText mView = (EditText) findViewById(R.id.textOrganisation);
    	   		String organisation = mView.getText().toString();
    	   		mView = (EditText) findViewById(R.id.textLastName);
    	   		String lastname = mView.getText().toString();
    	    	
    	   		visitorlist = MyProvider.getVisitorList(organisation, lastname);
    		}

	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this, getString(R.string.text_error), error_message);
	            	} 
	            
	         	   	/**
	                 * Updating parsed JSON data into spinner
	                 * */
	            	
	            	if(sign_all_out)
	            	{
            			
            			sign_all_out = false;
            			
            			Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class); 
 						  startActivity(intent);	
 						  
            		}
	            	else if (visitorlist.size() > 0)
	            	{
	            		
		            	List<String> visit_list = new ArrayList<String>();
		            	// create array of string
		            	for(cVisitEntries v : visitorlist)
		            	{
		            		visit_list.add(v.surname + " " + v.lastname + ", " + v.organisation);
		            	
		            	}
		            	CharSequence[] items = visit_list.toArray(new String[visit_list.size()]);
		            	               
		            	 // 1. Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
		
		                // 2. Chain together various setter methods to set the dialog characteristics
		                builder.setTitle(getString(R.string.text8_toast_signin))
		                		.setItems(items, new DialogInterface.OnClickListener() {
		                		   	public void onClick(DialogInterface dialog, int which) {
				                    // The 'which' argument contains the index position
				                    // of the selected item
		                		   	
		                		   		// find object
		                		   		int i = 0;
		                		   		cVisitEntries myVisit = null;
		                		   		for(cVisitEntries v : visitorlist)
		                		   		{
		                		   			if (i == which)
		                		   			{
		                		   				myVisit = v;
		                		   			}
		                		   			i++;
		                		   		}
		                		   		if (myVisit != null)
		                		   		{
		                		   			// fill fields
		                		   			EditText mView =(EditText) findViewById(R.id.textOrganisation);
		                		   			mView.setText(myVisit.organisation);
		                		   			mView =(EditText) findViewById(R.id.textLastName);
		                		   			mView.setText(myVisit.lastname);
		                		   			mView =(EditText) findViewById(R.id.textEmail);
		                		   			mView.setText(myVisit.email);
		                		   		 	mView =(EditText) findViewById(R.id.textPhone);
		                		   			mView.setText(myVisit.phone);
		                		   			mView =(EditText) findViewById(R.id.textName);
		                		   			mView.setText(myVisit.surname);
		                		   			mView =(EditText) findViewById(R.id.textFunction);
		                		   			mView.setText(myVisit.function);
		                		   		    spinner = (Spinner) findViewById(R.id.spinnerVisit);
		                		   		    spinner.setSelection(Integer.parseInt(myVisit.visit_kind));
		                		   		    mView =(EditText) findViewById(R.id.textEmployee);
		                		   			mView.setText(myVisit.employee_name);
		                		   	   		user_selected = true;
		                		   	   		myEmployeeID = myVisit.employee;
		                		   	   		
		                		   		    rememberedVisit = myVisit;
		                		   		}
		                		   	}
		                	   });
		                
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                // display images in row
	            	
	              }
	            }
	        });
	        
	        hideProgressDialog();
	         
	    }

	}
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	    	error_message = "";
	    	myEmployeeID = "0";
	    	
	    	itemList = new ArrayList<HashMap<String, String>>();
	    
	    	List<cContact> List = MyProvider.getContacts();
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid",  entry.id);
                map.put("name", entry.name);
                  
                // adding HashList to ArrayList
                itemList.add(map);
             }
	    	
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (error_message != "")
	            	{  
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this, getString(R.string.text_error), error_message);
	            	} 
	            
	            	// search through list and get the names of matches
	            	EditText mView = (EditText) findViewById(R.id.textEmployee);
	    	   		String[] EmployeeTexts = mView.getText().toString().toLowerCase().split(" ");
	    	   			    	   
	            	if (itemList.size() > 0)
	            	{
		            	final List<String> user_list = new ArrayList<String>();
		  
		    	   		for (HashMap<String, String> map : itemList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "name")
		           	        	{
		           	        		String name = entry.getValue();
		           	        		boolean found = false;
				            		for(String etext : EmployeeTexts)
				            		{
				            			if (name.toLowerCase().contains(etext) && (!etext.equals("van") && !etext.equals("der") && !etext.equals("de")))
				            			{
				            				found = true;        			
				            			}
				            		}
				            		if (found) user_list.add(name);        
		           	        		
		           	        	}
		           	        }
		            	
		            	if (user_list.size() > 0)
		            	{
			            	CharSequence[] items = user_list.toArray(new String[user_list.size()]);
			           
			            	 // 1. Instantiate an AlertDialog.Builder with its constructor
			                AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
			
			                // 2. Chain together various setter methods to set the dialog characteristics
			                builder.setTitle(getString(R.string.text5_toast_signin))
			                		.setItems(items, new DialogInterface.OnClickListener() {
			                			
			                		   	public void onClick(DialogInterface dialog, int which) {
					                    // The 'which' argument contains the index position
					                    // of the selected item
			                		   	
			                		   		// find object
			                		   		int i = 0;
			                		   		String myEmployee = "";
			                		   		for(String v : user_list)
			                		   		{
			                		   			if (i == which)
			                		   			{
			                		   				myEmployee = v;
			                		   			}
			                		   			i++;
			                		   		}
			                		   		EditText mView =(EditText) findViewById(R.id.textEmployee);
			                		   		mView.setText(myEmployee);
			                		   		user_selected = true;
			                		   		
			                		   	}
			                	   });
			                
			
			                // 3. Get the AlertDialog from create()
			                AlertDialog dialog = builder.create();
			                dialog.show();
		            	} else {
		            				            		
		            	}
		            	
	            	}
	      
	              }
	        });
	         
	    }

	}
	
	public boolean SendEmail(String email, String subject, String emailtext)
	{
		Intent intent = new Intent(Intent.ACTION_SENDTO); 
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, emailtext);
		intent.setData(Uri.parse("mailto:"+email)); 
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
		this.startActivity(intent);	
		return true;
	}
	
	  /** AsyncTask to download and load an image in ListView */
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
    	
    	  @Override
    	     protected void onPreExecute() {
    	         super.onPreExecute();
    	         	showProgressDialog("Wait ...");
    	     }
 
        @Override
        protected Boolean doInBackground(String... args) {
 
        	  String myDateStartTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        
        	  myDateStartTime = myDateStartTime.concat((new StringBuilder().append(" ").append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString());
        	  
        	  String myDateEndTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
  	        
        	  myDateEndTime = myDateEndTime.concat((new StringBuilder().append(" ").append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString());
        	
        	  cVisitEntries newVisit = new cVisitEntries();
        	  
        	  // extract data
			  String myOrganisation = ((EditText) findViewById(R.id.textOrganisation)).getText().toString();
			  String mySurname = ((EditText) findViewById(R.id.textName)).getText().toString();
			  String myLastName = ((EditText) findViewById(R.id.textLastName)).getText().toString();
			  
			  // auto set capitals
			  myOrganisation = myOrganisation.substring(0,1).toUpperCase() + myOrganisation.substring(1);
			  mySurname = mySurname.substring(0,1).toUpperCase() + mySurname.substring(1);
			  myLastName = myLastName.substring(0,1).toUpperCase() + myLastName.substring(1);
			  			  
			  String myPhone = ((EditText) findViewById(R.id.textPhone)).getText().toString();
			  String myEmail = ((EditText) findViewById(R.id.textEmail)).getText().toString();
			  String myFunction = ((EditText) findViewById(R.id.textFunction)).getText().toString();
			  
			  Spinner kindSpinner = (Spinner) findViewById(R.id.spinnerVisit);
			  int myIndex = kindSpinner.getSelectedItemPosition();
			  String myVisitKind = "";
			  if(myIndex != -1){
				  myVisitKind = visitKindKeyArray.get(myIndex);
			  }
			  //mySpinner = (Spinner) findViewById(R.id.spinnerEmployee);
			  //String myEmployee = String.valueOf(mySpinner.getSelectedItemPosition());
			  String myEmployee = ((EditText) findViewById(R.id.textEmployee)).getText().toString();

			  // store changes in object
			  newVisit.startdatetime = myDateStartTime;
			  newVisit.enddatetime = myDateEndTime;
			  newVisit.organisation = myOrganisation;
			  newVisit.surname = mySurname;
			  newVisit.lastname = myLastName;
			  newVisit.phone = myPhone;
			  newVisit.email = myEmail;
			  newVisit.function = myFunction;
			  newVisit.visit_kind = myVisitKind;
			  newVisit.employee = myEmployeeID;
			  			  
			  // save globals 
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");
			 
			  Globals.setValue("visitor_employee", myEmployee);
			  Globals.setValue("visitor_surname", mySurname);
			  Globals.setValue("visitor_lastname", myLastName);
			  Globals.setValue("visitor_organisation",myOrganisation);
			  action =  Globals.getValue("action");
		
			  // schedule or sign in
			  if (action == "signin" || action == "schedule")
			  {		
				  // get employee id
				  cContact myContact = MyProvider.getUser(this_organisation, myEmployee);
				  if (myContact != null)
				  {
					  if (myContact.id != "")
					  {
						  newVisit.employee = myContact.id;

						  // schedule
						  if (action == "schedule")
						  {
							  boolean suc = false;
							  visitID = MyProvider.createVisitEntry(newVisit);
							  if(visitID != 0){suc = true; Log.d("VISIT","createVisitEntry true");}
						  suc = true;
							  if (suc)
							  {
								  /*String visitid = MyProvider.getLatestVisit(myOrganisation, mySurname, myLastName);
								   rememberedVisit.id = String.valueOf(visitID);
								  // create visit members
															*/

								  //MyProvider.createStatusNotification(String.valueOf(visitID), newVisit.employee, "You have a reservation at " + newVisit.startdatetime, String.valueOf(newVisit.startdatetime), "reservation");
								  //MyProvider.createStatusNotification(String.valueOf(visitID), myUserID, "You have a reservation at " + newVisit.startdatetime, String.valueOf(newVisit.startdatetime), "reservation");

								  membersList = new ArrayList<String>();
								  if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null)
								  {
									  String members = textSelectCoWorkerString.replace("[", "");
									  members = members.replace("]", "");
									  String[] member_list = members.split(",");
									  for (String item : member_list)
									  {
										  cContact myMember = MyProvider.getUser(this_organisation, item);
										  if (myMember.id != "")
										  suc =  MyProvider.create_visit_member(String.valueOf(visitID), myMember.id);
											  suc = true;

										  membersList.add(myMember.id);

										  //MyProvider.createStatusNotification(String.valueOf(visitID), myMember.id, "You have a reservation at " + newVisit.startdatetime, String.valueOf(newVisit.startdatetime), "reservation");

									  }

									  Log.d("VISIT","create_visit_member true");
								  }

								  MyProvider.LogEvent(getString(R.string.text5_log_event));

								  registered = true;
							  } else {

								  registered = false;
								  error_message = MyProvider.last_error;
							  }

							  CheckBox chkReservation = (CheckBox) findViewById(R.id.chkReservation);
							  if (chkReservation.isChecked())
							  {
								  // reserve a meeting room
								  /*Intent intent = new Intent(SignInActivity.this, CreateScheduleAppointmentActivity.class);
								  intent.putExtra("startDate",startDateView.getText().toString());
								  intent.putExtra("startTime", startTimeView.getText().toString());
								  intent.putExtra("endDate",endDateView.getText().toString());
								  intent.putExtra("endTime", endTimeView.getText().toString());
								  intent.putExtra("allday", newVisit.allday);
								  System.out.println(membersList);
								  intent.putStringArrayListExtra("members", membersList);
								  startActivity(intent);*/

								  allday = newVisit.allday;


							  } else {
								  // or finish
								  Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class);
								  startActivity(intent);
							  }
						  }

						  // sign in
						  if (action == "signin")
						  {
							  newVisit.startdatetime = myDateStartTime;
							  SharedPreferences.Editor pref = getSharedPreferences("PREF_START_TIME", MODE_PRIVATE).edit();
							  pref.putString("start_time", myDateStartTime);
							  pref.commit();

							  boolean suc = false;

							  boolean bl_create_new = false;

							  if (rememberedVisit != null)
							  {
								  if (rememberedVisit.sign_in_datetime.equals("0000-00-00 00:00:00"))
								  {

									  Globals.setValue("visitID",rememberedVisit.id);

									  suc = MyProvider.updateVisitEntry(rememberedVisit.id, newVisit, "signin");

								  } else {
									  bl_create_new = true;
								  }

							  } else {

								  bl_create_new = true;

							  }

							  if (bl_create_new)
							  {
								  visitID = MyProvider.createVisitEntry(newVisit);
								  if(visitID != 0){
									  rememberedVisit = new cVisitEntries();
									  rememberedVisit.id = String.valueOf(visitID);

									  Globals.setValue("visitID",rememberedVisit.id);

									  suc = true;
								  }

								  rememberedVisit = MyProvider.getLatestVisit(myOrganisation, mySurname, myLastName);

								  if (rememberedVisit != null)
								  {
									  suc = MyProvider.updateVisitEntry(rememberedVisit.id, newVisit, "signin");
								  }
							  }

							  if (suc)
							  {

								  MyProvider.LogEvent(getString(R.string.text4_log_event));

								  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
								  boolean blNotifications = sharedPrefs.getBoolean("prefNotification", true);

								  if (blNotifications)
								  {
									  // send SMS
									  MyProvider.SendNotification(myContact.phone, getString(R.string.text1_send_notification) + myOrganisation + ", " + mySurname + " " + myLastName + " " + getString(R.string.text2_send_notification) + myPhone + ", e-mail = " + myEmail + "\n");

									  // and to co-workers
									  //String visitid = MyProvider.getLatestVisit(myOrganisation, mySurname, myLastName);

									  List<cContact> coWorkers = MyProvider.getVisitMembers(String.valueOf(rememberedVisit.id));
									  for(cContact coWorker : coWorkers)
									  {
										  MyProvider.SendNotification(coWorker.phone, getString(R.string.text1_send_notification) + myOrganisation + ", " + mySurname + " " + myLastName + " " + getString(R.string.text2_send_notification) + myPhone + ", e-mail = " + myEmail + "\n");

									  }

								  }

								  boolean blScheduleAppointment = sharedPrefs.getBoolean("prefScheduleAppointment", true);
								  boolean blTermsConditions = sharedPrefs.getBoolean("prefGeneralTerms", true);
								  boolean blNonDisclosure = sharedPrefs.getBoolean("prefNonDisclosure", true);
								  boolean blPrintLabel = sharedPrefs.getBoolean("prefPrintLabel", true);

								  if (blTermsConditions)
								  {
									  Intent intent = new Intent(SignInActivity.this, TermsConditionsActivity.class);
									  startActivity(intent);
								  } else {
									  if (blNonDisclosure)
									  {
										  Intent intent = new Intent(SignInActivity.this, NonDisclosureActivity.class);
										  startActivity(intent);
									  } else if(blPrintLabel){
										  Intent intent = new Intent(SignInActivity.this, PrintBatchActivity.class);
										  startActivity(intent);
									  } else {
										  Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class);
										  startActivity(intent);
									  }
								  }

								  registered = true;

							  } else {

								  registered = false;
								  error_message = MyProvider.last_error;

								  // log registration failed
								  MyProvider.LogEvent( getString(R.string.text3_log_event) + myOrganisation + ", " + myLastName);
							  }

						   }
					  } else {

						  // message employee unknown
						  registered = false;
						  error_message = getString(R.string.text6_toast_signin);
					  }

				  } else {
					  
					  // message employee unknown
					  registered = false;
					  error_message = getString(R.string.text6_toast_signin);
				  }
				  
			
			  } 
			  
			  // sign out
			  if (action == "signout")
			  {
				  SharedPreferences pref = getSharedPreferences("PREF_START_TIME", 0); 
			      String starttime = pref.getString("start_time", ""); 
				  newVisit.enddatetime = myDateEndTime;
				  newVisit.startdatetime = starttime;
				  if (rememberedVisit != null)
				  {
					  boolean suc = MyProvider.updateVisitEntry(rememberedVisit.id, newVisit, "signout"); 
					  if (suc)
					  {				
						  // end visit registered.
						  MyProvider.LogEvent(getString(R.string.text2_log_event));
										
						  registered = true;
						  
						  Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class); 
						  startActivity(intent);
					  } else {
						  
						  registered = false;
						  error_message = MyProvider.last_error;
						  
						  // failed to register end visit
						  MyProvider.LogEvent(getString(R.string.text1_log_event) + myOrganisation + ", " + myLastName);
						  
					  }
				  }
			  }
			  			  
        	  return registered;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(SignInActivity.this, getString(R.string.text_error), error_message);
		    		}
	            	
	            	hideProgressDialog();
	            	         	   	
	              }
	        });
	         
	    }
    }
        
    /**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadEmployeeList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();  
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	    	TextView mView = (TextView) findViewById(R.id.selectCoworkerSchedule_textView);
	   		String name = mView.getText().toString();
	    	
	   		error_message = "";
	    
	   		employeeList = MyProvider.getContacts();
    		   		
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this, getString(R.string.text_error), error_message);
	             	} 
	            	
	            	if (employeeList.size() > 0)
	            	{
		            	final List<String> employee_list = new ArrayList<String>();
		            	// create array of string
		            	for(cContact v : employeeList)
		            	{
		            		employee_list.add(v.name);
		            	
		            	}
		            	final CharSequence[] items = employee_list.toArray(new String[employee_list.size()]);
		            	final ArrayList<String> mSelectedItems = new ArrayList<String>();
						final TextView mView =(TextView) findViewById(R.id.selectCoworkerSchedule_textView);
						
						final StringBuilder build = new StringBuilder();
     	            	
		            	 // 1. Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
		
		                // 2. Chain together various setter methods to set the dialog characteristics
		                builder.setTitle(getString(R.string.text_alert_dialog_selectCoworkerSchedule))

		                		.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which, boolean isChecked) {
										  
										// If the user checked the item, add it to the selected items
										if (isChecked) {

			                		   			mSelectedItems.add((String) items[which]);

			                		   		}else{
		                                   if(mSelectedItems.contains(items[which]))
		                                		   mSelectedItems.remove(items[which]);
		                           }
		                		   			
						                   } 
										
									
								});
		                		   	
		                
		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								build.append(mSelectedItems.toString());
								mView.setText(build);
								textSelectCoWorkerString = mView.getText().toString();
							}
						});
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                // display images in row
	            	}
	            }
	        });
	        
	         
	    }

	}
	
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadVisitKindList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider = new WSDataProvider(myUser, myPass);
		Globals = ((MyGlobals)getApplication());
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    visitKindList = new ArrayList<HashMap<String,String>>();
			
			// convert objects to hashmap for list
    		List<cVisitKind> List = MyProvider.getVisitKind();
    	
    		for(cVisitKind entry : List)
         	{
    			HashMap<String, String> map = new HashMap<String, String>();
    			
                map.put("id", entry.id);
                map.put("name", entry.name);
                
                visitKindList.add(map);
             }

		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
        	   
        	   List<String> visitKindArray =  new ArrayList<String>();
        	   
        	   // load spinner data from dataprovider
          	    for (HashMap<String, String> map : visitKindList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "id") visitKindKeyArray.add(entry.getValue());
          	        	if (entry.getKey() == "name") visitKindArray.add(entry.getValue());
          	        }
          	    
          	    final ArrayAdapter<String> kind_adapter = new ArrayAdapter<String>(SignInActivity.this, android.R.layout.simple_spinner_item, visitKindArray);
          	    kind_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	       	final Spinner sVisitKind = (Spinner) findViewById(R.id.spinnerVisit);
    	       	sVisitKind.setAdapter(kind_adapter);
           }
       	});	      
      }
	}
	
	
    /**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadMyUserIdList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider = new WSDataProvider(myUser, myPass);
		Globals = ((MyGlobals)getApplication());
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider.getContacts();
    	
    		for(cContact entry : List)
         	{
                String usernameLogIn = myUser;
				String username = entry.username;
				if(usernameLogIn.equals(username)){myUserID = entry.id;}
             }

		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
           }
       	});	      
      }
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Background Async Task to create reservation
  	 * */
	class CreateReservationRoom extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	 	textTimeStartString = startTimeView.getText().toString();
				textTimeEndString = endTimeView.getText().toString();

			//   textSelectRoomString = textSelectRoom.getText().toString();
			   
	        	  String myDateTimeStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	  	        
	        	  myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
	        	  
	        	  String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	  	        
	        	  myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());
	        	  
	        	  TextView DescriptionView = (TextView) findViewById(R.id.editTextDescription);
			      String description = DescriptionView.getText().toString();

	        	  error_message = "";

			   boolean suc = false;
			   int reservationID;
			   reservationID = MyProvider.create_reservation( myDateTimeStart, myDateTimeEnd, rememberedRoom, allday, description);//
			   if(reservationID != 0){
					  suc = true; 
				 }
			   
			   if(suc){
				   
				   System.out.println(textSelectCoWorkerString);
				   for(String member : membersList){
					   
					   suc =  MyProvider.create_reservation_member(String.valueOf(reservationID), member, myDateTimeStart);
				   }
				   
					   Intent intent = new Intent(SignInActivity.this, ThankYouActivity.class); 
						  startActivity(intent);

	        	    
			  } else {
				  error_message = MyProvider.last_error;
			  }

	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (error_message != "")
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this,getString(R.string.text_error) , error_message);
	    			 	            		   	 
	            	} else
	            	{
	            		meetingRoomSelect.setError(null);
	            		Toast.makeText(getApplicationContext(), "Reservation created successful", Toast.LENGTH_SHORT).show();
	            	}
	            }
	        });
 
	    }
	}
	
	/**
	 * Background Async Task to Load Available Resources(Rooms) users by making HTTP Request
  	 * */
	class LoadRoomList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	    	TextView mView = (TextView) findViewById(R.id.selectRoomSchedule_textView);
	   		String name = mView.getText().toString();

		    textTimeStartString = startTimeView.getText().toString();
			textTimeEndString = endTimeView.getText().toString();
		   
	   		String myDateTimeStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	   		myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
	       	  
	   		String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
 	        myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());
       	  		
	   		//roomList = MyProvider.getRoomAvailableList(myDateTimeStart, myDateTimeEnd);
 	        roomList = MyProvider.getResources("2", "");
	   
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	         	// "Network connection not available! \n Please check WIFI and/or Internet connection."
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this,getString(R.string.text_error) , error_message);
	    			    // "Error"
	    			 	            		   	 
	            	} 
	            
	         	   	/**
	                 * Updating parsed JSON data into spinner
	                 * */
	            	
	            	if (roomList.size() > 0)
	            	{
	                	List<String> room_list = new ArrayList<String>();
		            	// create array of string
		            	for(cResource v : roomList)
		            	{
		            		if(v.status.contains("Free")){
		            			room_list.add(v.name);
		            		}
		            	}
		            	CharSequence[] items = room_list.toArray(new String[room_list.size()]);
		            	               
		            	 // 1. Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
		
		                // 2. Chain together various setter methods to set the dialog characteristics
		                builder.setTitle(getString(R.string.text_alert_dialog_selectRoomSchedule))

		                		.setSingleChoiceItems(items, -1 ,new DialogInterface.OnClickListener() {
		                		   	public void onClick(DialogInterface dialog, int which) {
				                    // The 'which' argument contains the index position
				                    // of the selected item
		                		   	
		                		   		// find object
		                		   		int i = 0;
		                		   		cResource myRoom = null;
		                		   		for(cResource v : roomList)
		                		   		{
		                		   			if (i == which)
		                		   			{
		                		   				myRoom = v;
		                		   			}
		                		   			i++;
		                		   		}
		                		   		if (myRoom != null)
		                		   		{
		                		   			// fill fields
		                		   			TextView mView =(TextView) findViewById(R.id.selectRoomSchedule_textView);
		                		   			mView.setText(myRoom.name);
		                		   			
		                		   		    rememberedRoom = myRoom.id;
		                		   		    
		                		   		    isSet = true;
		                		   		 meetingRoomSelect.setError(null);
		                		   		}
		                		   	}
		                	   });
		                
		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								new verifyReserveAvailability().execute();
							}
						});
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                // display images in row
	            	}
	            }
	        });      
	    }
	}
	
	// Load visits
	/**
	 * Background Async Task to Load all product by making HTTP Request
  	 * */
	class LoadListVisits extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         progressDialog = new ProgressDialog(SignInActivity.this);
         progressDialog.setMessage("Please wait ...");
         progressDialog.setIndeterminate(false);
         progressDialog.setCancelable(false);
         progressDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   
	    	error_message = "";
	    	
	    	//itemsVisitsList = new ArrayList<HashMap<String, String>>();

				UIHelper myUIHelper = new UIHelper();
				
	        	  /*String myDateStartTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        	  myDateStartTime = myDateStartTime.concat((new StringBuilder().append(" ").append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString());*/
	        	  
	        	  Calendar c = Calendar.getInstance();
	        	  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        	  String formattedDateToday = df.format(c.getTime());
	        	  String formattedDateTomorrow = df.format(c.getTime());
	        	  c.add(Calendar.DATE, 1);
	        	  formattedDateTomorrow = df.format(c.getTime());
	        	  
	        	 /* String myDateEndTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	        	  myDateEndTime = myDateEndTime.concat((new StringBuilder().append(" ").append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString());*/
	        	  
	        	  report_organisation = ((EditText) findViewById(R.id.textOrganisation)).getText().toString();

	        	  visitsList = MyProvider.getVisitsByOrganisation(formattedDateToday, formattedDateTomorrow, report_organisation);//filter_by_organization
	    		
	    		/*for(cVisitEntries entry : visitsList)
             	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	
	                map.put("pid",  entry.id);
	                map.put("name", entry.organisation);
	                map.put("subtitle", entry.surname + " " + entry.lastname);
	                map.put("status", entry.employee);
	                
	                String formatted_datetime = myUIHelper.SetFormattedDate(entry.startdatetime);
	                
	                map.put("datetime", formatted_datetime);
	                
	                 
	                // adding HashList to ArrayList
	                itemsVisitsList.add(map);
	             }*/

	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	    	progressDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
				public void run() {
	            	
	            	if (error_message != "")
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(SignInActivity.this,getString(R.string.text_error), error_message);
	    			} 
	    	   			    	   
	    	   		if (visitsList.size() > 0)
	            	{
		            	final List<String> visits_list = new ArrayList<String>();
		            	// create array of string
		            	for(cVisitEntries v : visitsList)
		            	{
		            		visits_list.add(v.surname + " " + v.lastname);
		            	}
		            	final CharSequence[] items = visits_list.toArray(new String[visits_list.size()]);
		            	//final ArrayList<String> mSelectedItems = new ArrayList<String>();
		            	final ArrayList mSelectedItems = new ArrayList();
						
						final StringBuilder build = new StringBuilder();
     	            	
		            	 // 1. Instantiate an AlertDialog.Builder with its constructor
		                final AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
		                
		                LayoutInflater inflater = getLayoutInflater();
		                View dialogView = inflater.inflate(R.layout.simple_list_item_multiple_choice, null);
		                builder.setView(dialogView);
		                final AlertDialog dialog = builder.create();
		                
		                TextView textView = (TextView)dialogView.findViewById(R.id.TexviewMultiChoice);
		                
		                Button btnCheckAll = (Button)dialogView.findViewById(R.id.btnCheckAll);
		                Button btnClearAll = (Button)dialogView.findViewById(R.id.btnClearAll);
		                
		                Button btnOk = (Button)dialogView.findViewById(R.id.btnOk);
		                Button btnCancel = (Button)dialogView.findViewById(R.id.btnCancel);
		                
		                final ListView lvCheckBox = (ListView)dialogView.findViewById(R.id.lvCheckBox);
		                
		                textView.setText("These visitors are signed in from " + report_organisation + " , sign all out?");
		                
		                lvCheckBox.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		                lvCheckBox.setAdapter(new ArrayAdapter(SignInActivity.this,
		                        android.R.layout.simple_list_item_multiple_choice, visits_list));
		                
		                lvCheckBox.setOnItemClickListener(new OnItemClickListener() 
		                {
		                    @Override
		                    public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) 
		                    {     	
		                        if(mSelectedItems.contains((String) items[arg2]))
		                        {
		                        	mSelectedItems.remove((String) items[arg2]);
		                        }
		                        else
		                        {
		                        	mSelectedItems.add((String) items[arg2]);
		                        }
		                        
		                        //Collections.sort(mSelectedItems);
		                        String strText = "";
		                        
		                        for(int i=0 ; i<mSelectedItems.size(); i++)                   
		                               strText += mSelectedItems.get(i) + ",";
		                        
		                        textSelectVisitsString = strText;
		                        //Toast.makeText(SignInActivity.this, "Item Clicked: "+ strText, Toast.LENGTH_SHORT).show();   
		                        
		                        String x = mSelectedItems.toString();
		                        Log.d("selected items", x);
		                    }
		                    
		                });
		                
		                btnCheckAll.setOnClickListener(new OnClickListener() 
		                {            
		                    @Override
		                    public void onClick(View arg0) 
		                    {
		                    	mSelectedItems.clear();               
		                        for(int i=0 ; i < lvCheckBox.getAdapter().getCount(); i++)
		                        {
		                            lvCheckBox.setItemChecked(i, true);
		                            mSelectedItems.add((String) items[i]);
		                        }
		                        
		                        String strText = "";
		                       for(int i=0 ; i<mSelectedItems.size(); i++)    
		                             strText += mSelectedItems.get(i) + ",";
		                        //Toast.makeText(SignInActivity.this, "Item Clicked: "+ strText, Toast.LENGTH_SHORT).show();
		                       textSelectVisitsString = strText;
		                    }
		                });
		                
		                btnClearAll.setOnClickListener(new OnClickListener() 
		                {            
		                    @Override
		                    public void onClick(View v) 
		                    {
		                    	mSelectedItems.clear();
		                        for(int i=0 ; i < lvCheckBox.getAdapter().getCount(); i++)
		                        {
		                            lvCheckBox.setItemChecked(i, false);
		                        }
		                        
		                        String strText = "";
		                      for(int i=0 ; i<mSelectedItems.size(); i++)
		                          strText += mSelectedItems.get(i) + ",";
		                        
		                       // Toast.makeText(SignInActivity.this, "Item Clicked: "+ strText, Toast.LENGTH_SHORT).show();
		                      textSelectVisitsString = strText;
		                    }
		                });
		                
		                btnOk.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								//build.append(mSelectedItems.toString());
								//textSelectVisitsString = build.toString();
								Log.d("VISITS", textSelectVisitsString);
								
								sign_all_out = true;
								
								new LoadVisitorList().execute();
								
				                dialog.dismiss();
							}
						});
		                
		                btnCancel.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {

				                dialog.dismiss();
								
							}
						});
		                
		
		                /*// 2. Chain together various setter methods to set the dialog characteristics
		                builder.setTitle(getString(R.string.text_alert_dialog_selectVisitsSchedule)  + " " + report_organization)

		                		.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which, boolean isChecked) {
										  
										// If the user checked the item, add it to the selected items
										if (isChecked) {

			                		   			mSelectedItems.add((String) items[which]);

			                		   		}else{
		                                   if(mSelectedItems.contains(items[which]))
		                                		   mSelectedItems.remove(items[which]);
			                		   		}	
						            } 
								});

		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								build.append(mSelectedItems.toString());
								textSelectVisitsString = build.toString();
							}
						});*/
		                // 3. Get the AlertDialog from create()
		                
		                dialog.show();
	            	}
	            }
	        });         
	    }
	}
	
	// Check availability of rooms
	
	private class verifyReserveAvailability extends AsyncTask<String, String, Boolean>
    {
    	private Boolean enable = true;
    	
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            
    		// show the progress bar
    		showProgressDialog(getString(R.string.text_alert_dialog));
    		//Wait ...
        }
        
		@Override
        protected Boolean doInBackground(String... args) 
        {
			
		    textTimeStartString = startTimeView.getText().toString();
			textTimeEndString = endTimeView.getText().toString();
			
	//   textSelectRoomString = textSelectRoom.getText().toString();
	   
    	  String myDateStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
    	  //myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
    	  
    	  String myDateEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
    	  //myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());

	  	  
		  	// call web method
			Log.d("_logs","RegisterReserveTask start_date: "+myDateStart+" start_hour: "+textTimeStartString+" end_date: "+myDateEnd+" end_hour: "+textTimeEndString);
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			duplicate=false;
			take_resource = false; 
			return_resource = false;
			if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,""))
			{
				//isResourceAvailable - 0 : reserved => should be taken , 1 : taken => should be returned
				Log.d("_logs","resource is not available");
				if (MyProvider.isMyResourceReservation(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom))
				{ 
					Log.d("_logs","resource is reserved by me");
					if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,"0"))
					{
						Log.d("_logs","resource is reserved . should be taken");
						resourceStatus = "1";
						take_resource = true;
						return_resource = false;
						//resource is reserved
					}
					if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,"1"))
					{
						Log.d("_logs","resource is taken . should be return");
						//resource is taken
						resourceStatus="2";
						take_resource = false;
						return_resource = true;
						
					}					
				}
				else
				{
					Log.d("_logs","resource is reserved by somebody else. unavailable");
					duplicate = true;
				}
			}

        return true;
			   
        }
		@Override
		 protected void onPostExecute(final Boolean success) 
		 {
			 Log.d("_logs","post execute duplicate = " + duplicate + "take_resource ="+take_resource+"return_resource="+return_resource );
			 if (duplicate)
			 {				
				 btnSubmit.setEnabled(true);
				 btnSubmit.setText("Continue without reserve a meeting room");
				 Toast.makeText(getApplicationContext(), "Resource unavailable for selected period of time.Please select other period or other resource!", Toast.LENGTH_LONG).show();
				 return;
			 }
			 else
			 {
				// String text_button="Reserve Item";
				 btnSubmit.setText(action);
				 if (take_resource)
				 {
					 //text_button = "Take Item";
					 btnSubmit.setText("Continue without reserve a meeting room");
					 Toast.makeText(getApplicationContext(), "Resource unavailable for selected period of time.Please select other period or other resource!", Toast.LENGTH_LONG).show();
				 }
				 if (return_resource)
				 {
					 //text_button = "Return Item";	
					 btnSubmit.setText("Continue without reserve a meeting room");
					 Toast.makeText(getApplicationContext(), "Resource unavailable,you have to wait to be returned or select other resource",  Toast.LENGTH_LONG).show(); 
				 }
				 //btnSubmit.setText(text_button);	 
				 btnSubmit.setEnabled(true);
			 }
			 
			 hideProgressDialog();
		 }
    }
	
	//////////////////////////////////////////////////////////////////////////////////////////
	
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	 // Saving variables
		textSelectCoWorker = (TextView) findViewById(R.id.selectCoworkerSchedule_textView);
	    outState.putString("textSelectCoWorkerString", textSelectCoWorkerString);
	    
	    outState.putInt("visitID", visitID);
	    
	 // Call at the end
	    super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState){
	    // Call at the start
	    super.onRestoreInstanceState(savedInstanceState);

	    // Retrieve variables
	    textSelectCoWorker = (TextView) findViewById(R.id.selectCoworkerSchedule_textView);
	    textSelectCoWorkerString = savedInstanceState.getString("textSelectCoWorkerString");
	    textSelectCoWorker.setText(textSelectCoWorkerString);
	    
	    visitID = savedInstanceState.getInt("visitID");
	    //new LoadVisitorList().execute();

	}
	
	
}
