package com.sharethatdata.digivisitor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TermsConditionsActivity extends Activity {
	
	Button btnSubmit;
	TextView textDescriptionView;

	String textBuffer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms_conditions);
		
		 btnSubmit = (Button) findViewById(R.id.btnSubmit);
		 textDescriptionView = (TextView) findViewById(R.id.textDescriptionView);
		 
			SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
			String textFromSettings = sharedPrefs.getString("prefGeneralTermsText", "");
			
			// Set up the text.
			textBuffer = textFromSettings;
			textDescriptionView.setText(textBuffer);
		 
		 btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
					
				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
				  boolean blNonDisclosure = sharedPrefs.getBoolean("prefNonDisclosure", true);
				  boolean blPrintLabel = sharedPrefs.getBoolean("prefPrintLabel", true);
				  if (blNonDisclosure)
					  {					
						  Intent intent = new Intent(v.getContext(), NonDisclosureActivity.class); 
						  startActivity(intent);
					  } else {
						  if (blPrintLabel)
						  {					
							  Intent intent = new Intent(v.getContext(), PrintBatchActivity.class); 
							  startActivity(intent);
						  } else {
							  //Intent intent = new Intent(v.getContext(), ThankYouActivity.class); startActivity(intent);
							  Intent intent = new Intent(v.getContext(), SalesActivity.class); 
							  startActivity(intent); 
						  }  			   			  
					  }  			  
				  }
			   
		 
			});
	}



}
