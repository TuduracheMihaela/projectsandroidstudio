package com.sharethatdata.digivisitor;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cVisitEntries;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SalesActivity extends Activity {
	
	MyGlobals Globals;
	
	WSDataProvider MyProvider = null;
	
	ProgressDialog progressDialog;
	
	Button buttonYes;
	Button buttonNo;
	TextView textDigivisitorView;
	
	String textBuffer;
	private String myUser = "";
	private String myPass= "";
	private String error_message = "";

	protected void onCreate (Bundle bundle){
		super.onCreate(bundle);
		setContentView(R.layout.activity_sales);
		
		
		buttonYes = (Button) findViewById(R.id.btnYes);
		buttonNo = (Button) findViewById(R.id.btnNo);
		textDigivisitorView = (TextView) findViewById(R.id.textDigivisitorView);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
		String textFromSettings = sharedPrefs.getString("prefSalesText", "");
		
		Globals = ((MyGlobals) getApplicationContext());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		// Set up the text.
		textBuffer = textFromSettings;
		textDigivisitorView.setText(textBuffer);
		
		buttonYes.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 new RegisterTask().execute();
			}
		});
		
		buttonNo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				// TODO Auto-generated method stub
				//Toast.makeText(SalesActivity.this, v.getResources().getString(R.string.sales_button_no_text), Toast.LENGTH_SHORT).show();
				 Intent intent = new Intent(v.getContext(), ThankYouActivity.class);
				 startActivity(intent); 
			}
		});
	}
	
	  /** AsyncTask to download and load an image in ListView */
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
    	
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
            
    		// show the progress bar
    		showProgressDialog(getString(R.string.text_alert_dialog));
    		//Wait ...
        }
 
        @Override
        protected Boolean doInBackground(String... args) {
 
        	
	 		  boolean suc = false;
        		
			  // get globals 
			  MyGlobals Globals = ((MyGlobals)getApplication());
			  String this_organisation = Globals.getValue("organisation");
			  
			  String myOrganisation = Globals.getValue("visitor_organisation");
			  String mySurname = Globals.getValue("visitor_surname");
			  String myLastName = Globals.getValue("visitor_lastname");
			  
			  cVisitEntries rememberedVisit = MyProvider.getLatestVisit(myOrganisation, mySurname, myLastName);
			  
			  if (rememberedVisit != null)
			  {
				  rememberedVisit.productinfo = "1";
				  suc = MyProvider.updateVisitEntry(rememberedVisit.id, rememberedVisit, "");
			  }
	
			  if (suc)
			  {
				  	Intent intent = new Intent(SalesActivity.this, ThankYouActivity.class); 
					startActivity(intent); 
			  } else 
			  {
				  error_message = MyProvider.last_error;
			  }
			  
			  			  
	    	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 myUIHelper.ShowDialog(SalesActivity.this, getString(R.string.text_error), error_message);
		    		}
	            	         	   	
	              }
	        });
	         
	        hideProgressDialog();
	    }
    }
    
    /**
	 * Shows a Progress Dialog 
	 *  
	 * @param msg
	 */
	public void showProgressDialog(String msg) 
	{
		
		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);
			
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			
			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();		
	}
	
	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {
		
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();     
		    }
		}
		
		progressDialog = null;
	}
}
