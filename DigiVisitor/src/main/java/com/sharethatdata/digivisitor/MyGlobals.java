package com.sharethatdata.digivisitor;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

public class MyGlobals extends com.sharethatdata.crash.report.MyCrashReport {

	  public class myValue {
		  public String name;
		  public String value;
		  
		  public myValue()
		  {
			name = "";
			value = "";
		  }
	  }
		
	  private myValue[] myValues;
	  private int numberOfValues = 0;
	  
	  
	  public MyGlobals()
	  {
		  myValues = new myValue[100];
		  
		
	  }
	  
	  public String getValue(String n){

		  for(int i=0; i < numberOfValues; i++)
		  {
			  if (myValues[i].name == n)
			  {
				  Log.d("MyGlobals getValue", myValues[i].value);
				  return myValues[i].value;
			  }
		  }
		  return "";
	  }
	  
	  public void setValue(String n, String s){
		  // find existing
		  boolean found = false;
		  if (numberOfValues > 0)
		  {
			  for(int i=0; i < numberOfValues; i++)
			  {
				  if (myValues[i].name == n)
				  {
					  myValues[i].value = s;
					  Log.d("MyGlobals setValue", myValues[i].value);
					  found = true;
				  }
			  }
		  }
		  if (found == false)
		  {
			  int values_len = numberOfValues;
			  if (values_len < 0) values_len = 0;
			  myValue value = new myValue();
			  value.name = n;
			  value.value = s;
			  myValues[numberOfValues] = value;
			  numberOfValues++;
			  
			  Log.d("MyGlobals setValue n and s", n + " " + s);
		  }
		
	  }
	
}
