package com.sharethatdata.digivisitor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;



public class ThankYouActivity extends Activity {


	MyGlobals Globals;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thank_you);
		
		Globals = ((MyGlobals)getApplication());
		
	     Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
			 
		 btnSubmit.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				  
			//	  Intent intent = new Intent(v.getContext(), WebViewActivity.class); 
			//	  startActivity(intent);
				  
				  Intent intent = new Intent(v.getContext(), IntroActivity.class); 
				  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  intent.putExtra("EXIT", true);
				  startActivity(intent); 
			  }
		 
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");
		
		if( isAdministrator == "yes" ){
			getMenuInflater().inflate(R.menu.thank_you, menu);
		} else if(isDeveloper == "yes"){
			getMenuInflater().inflate(R.menu.thank_you, menu);
		}else if(isEmployee == "yes"){
			
			}

		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	

}
