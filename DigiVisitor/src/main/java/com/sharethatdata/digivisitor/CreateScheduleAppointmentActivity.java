package com.sharethatdata.digivisitor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;

import com.sharethatdata.digivisitor.SignInActivity.LoadList;
import com.sharethatdata.digivisitor.notification.MyReceiverReservation;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cEmployee;
import com.sharethatdata.webservice.datamodel.cReservations;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cRoomAvailable;

public class CreateScheduleAppointmentActivity extends Activity{
	
	MyGlobals Globals;
	
	AlertDialog levelDialog;
	ProgressDialog progressDialog;
	
	WSDataProvider MyProvider = null;
	
	List<cResource> roomList;
	
	private boolean no_network = false;
    private String error_message = "";
    private String rememberedRoom = "";
    private cEmployee rememberedEmployee = null;

	
	private String myUser = "";
	private String myPass = "";
	private String action = "";
	private String action1 = "";
	private String action2 = "";
	
	private int mStartHour,mStartMinute;
	private int mStartYear,mStartMonth,mStartDay;
	
	TextView textDateEnd;
	TextView textTimeStartPicker;
	TextView textTimeEndPicker;
	TextView textSelectRoom;
	
	Button btnSubmit;
	
	String textStartDateString;
	String textEndDateString;
	String textTimeStartString;
	String textTimeEndString ;
	String allday;
	ArrayList<String> textSelectCoWorkerString;
	//String textSelectRoomString ;
	String textDateTimeNotification;
	  
	List<String> roomkeyArray =  new ArrayList<String>();
	  
	  boolean isSet = false;
	  View focusView = null;
	  
	  private String resourceStatus="0";
	  private String lastReservationID="";
	  private Boolean take_resource = false;
	  private Boolean return_resource = false;
  	  private Boolean duplicate = false;
	
	protected void onCreate(Bundle bundle){
		super.onCreate(bundle);
		setContentView(R.layout.activity_create_schedule_appointment);
		
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
	
		Globals = ((MyGlobals) getApplicationContext());
		action =  Globals.getValue("action");
	
		myUser =  Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
		// set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = c.get(Calendar.DAY_OF_MONTH);
	    mStartHour = c.get(Calendar.HOUR_OF_DAY);
	    mStartMinute = c.get(Calendar.MINUTE);
	    
	    textDateEnd = (TextView) findViewById(R.id.textCalendar);
	    textTimeStartPicker = (TextView) findViewById(R.id.textTimeStartPicker);
	    textTimeEndPicker = (TextView) findViewById(R.id.textTimeEndPicker);
	    textSelectRoom = (TextView) findViewById(R.id.selectRoomSchedule_textView);
	    
	    textDateEnd.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	            .append(pad(mStartMonth + 1)).append("-")
	            .append(pad(mStartYear)));
	    
	            
	    Intent i = getIntent();
	    if(i.hasExtra("startDate") && i.hasExtra("startTime"))
	    {
	    	textStartDateString = i.getStringExtra("startDate");
	    	textTimeStartString = i.getStringExtra("startTime");
	    	textEndDateString = i.getStringExtra("endDate");
	    	textTimeEndString = i.getStringExtra("endTime");
	    	allday = i.getStringExtra("allday");
	    	textSelectCoWorkerString = i.getStringArrayListExtra("members");
	    }

	    
	    textDateEnd.setText(textStartDateString);
	    textTimeStartPicker.setText(textTimeStartString);
	    textTimeEndPicker.setText(textTimeEndString);
	    
		 btnSubmit = (Button) findViewById(R.id.buttonSubmit);
		 btnSubmit.setText(action);
		 
		 btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			//	action = "Reserve";
			//	action1 = "Zeitplan";
			//	action2 = "Rooster";
				// if(action.contains(getString(R.string.buttonSchedule_text))){
					 if(!isSet){
					   textSelectRoom.setError(getString(R.string.error_field_required));
					   focusView = textSelectRoom;
					   isSet = false;
  				   	}else {
  				   		if(duplicate || return_resource || take_resource){
  				   		 Intent intent = new Intent(CreateScheduleAppointmentActivity.this, ThankYouActivity.class); 
						  startActivity(intent);
  				   		}else{
  				   			new CreateReservationRoom().execute();
  				   		}
  				   	}
				
			/*	 }else if(action1.contains(getString(R.string.buttonSchedule_text))){
					 if(!isSet){
						   textSelectRoom.setError(getString(R.string.error_field_required));
						   focusView = textSelectRoom;
						   isSet = false;
	  				   		}else 
	  				   			{
	  				   				new Load().execute();
	  				   			}
					 
				 }else if(action2.contains(getString(R.string.buttonSchedule_text))){
					 if(!isSet){
						   textSelectRoom.setError(getString(R.string.error_field_required));
						   focusView = textSelectRoom;
						   isSet = false;
	  				   		}else 
	  				   			{
	  				   				new Load().execute();
	  				   			}
					 
				 }else {
					 
				 }
*/			}
		});
//		 
//		 btnSubmit.setOnClickListener(new OnClickListener() {
//		 
//			  @Override
//			  public void onClick(View v) {
//					
//				  SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
//				  boolean blTermsConditions = sharedPrefs.getBoolean("prefGeneralTerms", true);
//				  boolean blNonDisclosure = sharedPrefs.getBoolean("prefNonDisclosure", true);
//				  boolean blPrintLabel = sharedPrefs.getBoolean("prefPrintLabel", true);
////				  if (blTermsConditions)
////					  {
////					  		 Intent intent = new Intent(v.getContext(), TermsConditionsActivity.class); startActivity(intent);
////		
////					  } else {
////						  if (blNonDisclosure)
////						  {					
////							  Intent intent = new Intent(v.getContext(), NonDisclosureActivity.class); startActivity(intent);
////
////						  } else if(blPrintLabel){
////							  Intent intent = new Intent(v.getContext(), PrintBatchActivity.class); startActivity(intent);
////							  
////						  } else {
////							  Intent intent = new Intent(v.getContext(), SalesActivity.class); startActivity(intent); 
////						  } 			   			  
////					  } 
//				  
//				   textCalendarString = textCalendar.getText().toString();
//				   textTimeStartString = textTimeStartPicker.getText().toString();
//				   textTimeEndString = textTimeEndPicker.getText().toString();
//				   textSelectRoomString = textSelectRoom.getText().toString();
//				   
//		        	  String myDateTimeStart = (new StringBuilder().append(textCalendarString)).toString();
//		  	        
//		        	  myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
//		        	  
//		        	  String myDateTimeEnd = (new StringBuilder().append(textCalendarString)).toString();
//		  	        
//		        	  myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());
//				   
//		        	  
//		        	  
//		        	 // boolean suc = MyProvider.create_reservation_member(myDateTimeStart, myDateTimeEnd, textSelectRoomString);
//		        	  
//				   if(!isSet){
//					   textSelectRoom.setError(getString(R.string.error_field_required));
//					   focusView = textSelectRoom;
//					   isSet = false;
//				   }else 
//				   {
//					   boolean suc = MyProvider.create_reservation_member( myDateTimeStart, myDateTimeEnd, textSelectRoomString);//
//					   
//					   
//					   if(suc){
//						   Toast.makeText(v.getContext(), "Rezervation created successful", Toast.LENGTH_SHORT).show();
//
//							  Intent intent = new Intent(v.getContext(), ThankYouActivity.class); 
//							  intent.putExtra("email",email);
//							  intent.putExtra("pass", password);
//							  startActivity(intent);
//							  
//							  textSelectRoom.setError(null);
//					   }
//					   
//						  
//
//				   }
//
//				  
//				  //suc = MyProvider.create_reservation_member(textCalendarString, textTimeStartString, textTimeEndString, textSelectRoomString);
//				  
//				  
//				 // MyProvider.create_reservation_member(textCalendarString, textTimeStartString, textTimeEndString, textSelectRoomString);
//				  
//				 // if(suc)
//				//  {
//					   
//				//  }
//				  
//				  
//				  }
//			   
//		 
//			});
		 
//		 final TextView TextViewAlertDialog1 = (TextView) findViewById(R.id.selectRoomSchedule_textView);
//		 
//		 TextViewAlertDialog1.setOnTouchListener(new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				 if (event.getAction() == MotionEvent.ACTION_UP) {
//
//			    		// do search
//					//if(t.getText().toString().trim().length() == 2){
//					new LoadRoomAvailableList().execute();
//				//}
//		        }
//
//				return false;
//			}
//		});
		 
//		 final EditText editTextAlertDialog2 = (EditText) findViewById(R.id.editText2);
//		 
//		 editTextAlertDialog2.setOnTouchListener(new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				 if (event.getAction() == MotionEvent.ACTION_UP) {
//
//			    		// do search
//					//if(t.getText().toString().trim().length() == 2){
//					new LoadEmployeeList().execute();
//				//}
//		        }
//
//				return false;
//			}
//		});
		 

		 
//		 editTextAlertDialog.addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				final CharSequence[] items = {" Easy "," Medium "," Hard "," Very Hard "};
//		         
//		         // Creating and Building the Dialog 
//		         AlertDialog.Builder builder = new AlertDialog.Builder(CreateScheduleAppointmentActivity.this);
//		         builder.setTitle("Select The Difficulty Level");
//		         builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
//		         public void onClick(DialogInterface dialog, int item) {
//		            
//		             
//		             switch(item)
//		             {
//		                 case 0:
//		                         editTextAlertDialog.setText(items[item]);
//		                          break;
//		                 case 1:
//		                	 editTextAlertDialog.setText(items[item]);
//		                         
//		                         break;
//		                 case 2:
//		                	 editTextAlertDialog.setText(items[item]);
//		                         break;
//		                 case 3:
//		                	 editTextAlertDialog.setText(items[item]);          
//		                         break;
//		                 
//		             }
//		             levelDialog.dismiss();    
//		             }
//		         });
//		         
//		         builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						// TODO Auto-generated method stub
//						
//					}
//				});
//		         
//		         levelDialog = builder.create();
//		         levelDialog.show();
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//				
//		 		
//			}
//			
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				
//			}
//		});	 
			
		 ///////////////////////////////////////////////////////////////////////////////
		 // Date and Time	 
		 textDateEnd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    
			    DatePickerDialog dpd = new DatePickerDialog(CreateScheduleAppointmentActivity.this, new DatePickerDialog.OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						mStartYear = year;
						mStartMonth = monthOfYear;
						mStartDay = dayOfMonth;
						
						//textCalendar.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
						//textCalendar.setText(new StringBuilder().append(pad(year)).append("-").append(pad(monthOfYear + 1)).append("-").append(pad(dayOfMonth)));

						 TextView dateView = (TextView) findViewById(R.id.textCalendar);
		    		        
				            dateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
				                .append(pad(mStartMonth + 1)).append("-")
				                .append(pad(mStartYear)));
				            new verifyReserveAvailability().execute();
					}
				}, mStartYear, mStartMonth, mStartDay);
			    dpd.show();
			    
			}
		});
		 
		 textTimeStartPicker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    
			    TimePickerDialog tpd = new TimePickerDialog(CreateScheduleAppointmentActivity.this, new TimePickerDialog.OnTimeSetListener() {
					
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						// TODO Auto-generated method stub
						
						textTimeStartPicker.setText(new StringBuilder().append(pad(hourOfDay)).append(":").append(pad(minute)));
						new verifyReserveAvailability().execute();
					}
				}, mStartHour, mStartMinute, false);
			    tpd.show();
			}
		});
		 
		 textTimeEndPicker.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
			    
			    TimePickerDialog tpd = new TimePickerDialog(CreateScheduleAppointmentActivity.this, new TimePickerDialog.OnTimeSetListener() {
					
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						// TODO Auto-generated method stub
						
						textTimeEndPicker.setText(new StringBuilder().append(pad(hourOfDay)).append(":").append(pad(minute)));
						new verifyReserveAvailability().execute();
					}
				}, mStartHour, mStartMinute, false);
			    tpd.show();
			}
		});
		 
		 
		 
		 textSelectRoom.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LoadRoomList().execute();	
			}
		});
		 
		 

		 
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadRoomList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         

     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    
	    	TextView mView = (TextView) findViewById(R.id.selectRoomSchedule_textView);
	   		String name = mView.getText().toString();
	    	
	   		textTimeStartString = textTimeStartPicker.getText().toString();
	   		textTimeEndString = textTimeEndPicker.getText().toString();
		   
	   		String myDateTimeStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	   		myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
	       	  
	   		String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
 	        myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());
       	  		
	   		//roomList = MyProvider.getRoomAvailableList(myDateTimeStart, myDateTimeEnd);
 	        roomList = MyProvider.getResources("2", "");
	   
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.text7_toast_signin);
	         	// "Network connection not available! \n Please check WIFI and/or Internet connection."
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(CreateScheduleAppointmentActivity.this,getString(R.string.text_error) , error_message);
	    			    // "Error"
	    			 	            		   	 
	            	} 
	            
	         	   	/**
	                 * Updating parsed JSON data into spinner
	                 * */
	            	
	            	if (roomList.size() > 0)
	            	{
	                	List<String> room_list = new ArrayList<String>();
		            	// create array of string
		            	for(cResource v : roomList)
		            	{
		            		if(v.status.contains("Free")){
		            			room_list.add(v.name);
		            		}
		            	}
		            	CharSequence[] items = room_list.toArray(new String[room_list.size()]);
		            	               
		            	 // 1. Instantiate an AlertDialog.Builder with its constructor
		                AlertDialog.Builder builder = new AlertDialog.Builder(CreateScheduleAppointmentActivity.this);
		
		                // 2. Chain together various setter methods to set the dialog characteristics
		                builder.setTitle(getString(R.string.text_alert_dialog_selectRoomSchedule))

		                		.setSingleChoiceItems(items, -1 ,new DialogInterface.OnClickListener() {
		                		   	public void onClick(DialogInterface dialog, int which) {
				                    // The 'which' argument contains the index position
				                    // of the selected item
		                		   	
		                		   		// find object
		                		   		int i = 0;
		                		   		cResource myRoom = null;
		                		   		for(cResource v : roomList)
		                		   		{
		                		   			if (i == which)
		                		   			{
		                		   				myRoom = v;
		                		   			}
		                		   			i++;
		                		   		}
		                		   		if (myRoom != null)
		                		   		{
		                		   			// fill fields
		                		   			TextView mView =(TextView) findViewById(R.id.selectRoomSchedule_textView);
		                		   			mView.setText(myRoom.name);
		                		   			
		                		   		    rememberedRoom = myRoom.id;
		                		   		    
		                		   		    isSet = true;
		                		   		    textSelectRoom.setError(null);
		                		   		}
		                		   	}
		                	   });
		                
		                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								new verifyReserveAvailability().execute();
							}
						});
		
		                // 3. Get the AlertDialog from create()
		                AlertDialog dialog = builder.create();
		                dialog.show();
		                // display images in row
	            	}
	            }
	        });
	        
	         
	    }

	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private class verifyReserveAvailability extends AsyncTask<String, String, Boolean>
    {
    	private Boolean enable = true;
    	
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            
    		// show the progress bar
    		showProgressDialog(getString(R.string.text_alert_dialog));
    		//Wait ...
        }
        
		@Override
        protected Boolean doInBackground(String... args) 
        {
		 textTimeStartString = textTimeStartPicker.getText().toString();
	     textTimeEndString = textTimeEndPicker.getText().toString();
	//   textSelectRoomString = textSelectRoom.getText().toString();
	   
    	  String myDateStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
    	  //myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
    	  
    	  String myDateEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
    	  //myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());

	  	  
		  	// call web method
			Log.d("_logs","RegisterReserveTask start_date: "+myDateStart+" start_hour: "+textTimeStartString+" end_date: "+myDateEnd+" end_date: "+textTimeEndString);
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			duplicate=false;
			take_resource = false; 
			return_resource = false;
			if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,""))
			{
				//isResourceAvailable - 0 : reserved => should be taken , 1 : taken => should be returned
				Log.d("_logs","resource is not available");
				if (MyProvider.isMyResourceReservation(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom))
				{ 
					Log.d("_logs","resource is reserved by me");
					if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,"0"))
					{
						Log.d("_logs","resource is reserved . should be taken");
						resourceStatus = "1";
						take_resource = true;
						return_resource = false;
						//resource is reserved
					}
					if (!MyProvider.isResourceAvailable(myDateStart,textTimeStartString,myDateEnd,textTimeEndString,rememberedRoom,"1"))
					{
						Log.d("_logs","resource is taken . should be return");
						//resource is taken
						resourceStatus="2";
						take_resource = false;
						return_resource = true;
						
					}					
				}
				else
				{
					Log.d("_logs","resource is reserved by somebody else. unavailable");
					duplicate = true;
				}
			}

        return true;
			   
        }
		@Override
		 protected void onPostExecute(final Boolean success) 
		 {
			 Log.d("_logs","post execute duplicate = " + duplicate + "take_resource ="+take_resource+"return_resource="+return_resource );
			 if (duplicate)
			 {				
				 btnSubmit.setEnabled(true);
				 btnSubmit.setText("Continue without reserve a meeting room");
				 Toast.makeText(getApplicationContext(), "Resource unavailable for selected period of time.Please select other period or other resource!", Toast.LENGTH_LONG).show();
				 return;
			 }
			 else
			 {
				// String text_button="Reserve Item";
				 btnSubmit.setText(action);
				 if (take_resource)
				 {
					 //text_button = "Take Item";
					 btnSubmit.setText("Continue without reserve a meeting room");
					 Toast.makeText(getApplicationContext(), "Resource unavailable for selected period of time.Please select other period or other resource!", Toast.LENGTH_LONG).show();
				 }
				 if (return_resource)
				 {
					 //text_button = "Return Item";	
					 btnSubmit.setText("Continue without reserve a meeting room");
					 Toast.makeText(getApplicationContext(), "Resource unavailable,you have to wait to be returned or select other resource",  Toast.LENGTH_LONG).show(); 
				 }
				 //btnSubmit.setText(text_button);	 
				 btnSubmit.setEnabled(true);
			 }
			 
			 hideProgressDialog();
		 }
    }
	
	/**
	 * Background Async Task to create reservation
  	 * */
	class CreateReservationRoom extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
			   textTimeStartString = textTimeStartPicker.getText().toString();
			   textTimeEndString = textTimeEndPicker.getText().toString();
			//   textSelectRoomString = textSelectRoom.getText().toString();
			   
	        	  String myDateTimeStart = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	  	        
	        	  myDateTimeStart = myDateTimeStart.concat((new StringBuilder().append(" ").append(textTimeStartString)).toString());
	        	  
	        	  String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
	  	        
	        	  myDateTimeEnd = myDateTimeEnd.concat((new StringBuilder().append(" ").append(textTimeEndString)).toString());
	        	  
	        	  TextView DescriptionView = (TextView) findViewById(R.id.editTextDescription);
			      String description = DescriptionView.getText().toString();
					
	  //      		action = "Reserve";
	  //      		action1 = "Zeitplan";
	//				action2 = "Rooster";
	//				 if(action.contains(getString(R.string.buttonSchedule_text)))
	  //   		 {
	        	  error_message = "";

			   boolean suc = false;
			   int reservationID;
			   reservationID = MyProvider.create_reservation( myDateTimeStart, myDateTimeEnd, rememberedRoom, allday, description);//
			   if(reservationID != 0){
					  suc = true; 
				 }
			   
			   if(suc){
				   
				   System.out.println(textSelectCoWorkerString);
				   for(String member : textSelectCoWorkerString){
					   
					   suc =  MyProvider.create_reservation_member(String.valueOf(reservationID), member, myDateTimeStart);
				   }
				   
					   Intent intent = new Intent(CreateScheduleAppointmentActivity.this, ThankYouActivity.class); 
						  startActivity(intent);


					  
					  
//					  SharedPreferences settings = getSharedPreferences("suc", 0);
//				      SharedPreferences.Editor editor = settings.edit();
//					  editor.putBoolean("silentMode", suc);
//	    							 editor.commit();

					 
  
	    			  

	    	/*		   
	     		 }
					 else if(action1.contains(getString(R.string.buttonSchedule_text)))
					 {
						 boolean suc = MyProvider.create_reservation( myDateTimeStart, myDateTimeEnd, rememberedRoom);//
  					   
  					   if(suc){  				   

  							  Intent intent = new Intent(CreateScheduleAppointmentActivity.this, ThankYouActivity.class); 
  							  
  							  SharedPreferences settings = getSharedPreferences("suc", 0);
  						      SharedPreferences.Editor editor = settings.edit();
  							  editor.putBoolean("silentMode", suc);
  							  editor.commit();

  							  startActivity(intent);

  					   }
					 }
					 
					 else if(action2.contains(getString(R.string.buttonSchedule_text)))
					 {
						 boolean suc = MyProvider.create_reservation( myDateTimeStart, myDateTimeEnd, rememberedRoom);//
  					   
  					   if(suc){  				   

  							  Intent intent = new Intent(CreateScheduleAppointmentActivity.this, ThankYouActivity.class); 
  							  
  							  SharedPreferences settings = getSharedPreferences("suc", 0);
  						      SharedPreferences.Editor editor = settings.edit();
  							  editor.putBoolean("silentMode", suc);
  							  editor.commit();

  							  startActivity(intent);

  					   }*/
					// }
	        	    
			  } else {
				  error_message = MyProvider.last_error;
			  }
	   
//	    	if (MyProvider.json_connected == false)
//	    	{
//	        	no_network = true;
//	         	error_message = getString(R.string.text7_toast_signin);
//	         	// "Network connection not available! \n Please check WIFI and/or Internet connection."
//	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            	
	            	if (error_message != "")
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(CreateScheduleAppointmentActivity.this,getString(R.string.text_error) , error_message);
	    			 	            		   	 
	            	} else
	            	{
	            		textSelectRoom.setError(null);
	            		Toast.makeText(getApplicationContext(), "Reservation created successful", Toast.LENGTH_SHORT).show();
	            	}
	            
	         	   	/**
	                 * Updating parsed JSON data into spinner
	                 * */
	            	
	            
	            }
	        });
	        
	         
	    }

	}
	
////////////////////////////////////////////////////////////////////////////
	
/**
* Shows a Progress Dialog 
*  
* @param msg
*/
public void showProgressDialog(String msg) 
{

	// check for existing progressDialog
	if (progressDialog == null) {
		// create a progress Dialog
		progressDialog = new ProgressDialog(this);
		
		// remove the ability to hide it by tapping back button
		progressDialog.setIndeterminate(true);
		
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		
		progressDialog.setMessage(msg);
	
	}
	
	// now display it.
	progressDialog.show();		
}	


/**
* Hides the Progress Dialog
*/
public void hideProgressDialog() {

	if (progressDialog != null) {
		progressDialog.dismiss();
	}
	
	progressDialog = null;
}	

////////////////////////////////////////////////////////////////////////////
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// comment
	
	
	
	}
