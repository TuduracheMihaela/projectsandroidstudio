package com.sharethatdata.digivisitor;

import java.io.InputStream;

import com.sharethatdata.webservice.ImageProcessor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;

public class PictureDetailActivity extends Activity {

	MyGlobals Globals;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_picture_detail);
		
		Globals = ((MyGlobals)getApplication());
		String pid = Globals.getValue("pid");
		String action = Globals.getValue("action");
		
		ImageView bmImage = (ImageView)findViewById(R.id.imageView1);
		
		ImageProcessor myImgProcessor = new ImageProcessor(getBaseContext());
		myImgProcessor.SetWebServiceUrl(Globals.getValue("ws_url"));

		String imgUrl = myImgProcessor.getFullImageURL(pid, action);
	
		new DownloadImageTask(bmImage).execute(imgUrl);
			
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		  ImageView bmImage;

		  public DownloadImageTask(ImageView bmImage) {
		      this.bmImage = bmImage;
		  }

		  protected Bitmap doInBackground(String... urls) {
		      String urldisplay = urls[0];
		      Bitmap mIcon11 = null;
		      try {
		        InputStream in = new java.net.URL(urldisplay).openStream();
		        mIcon11 = BitmapFactory.decodeStream(in);
		      } catch (Exception e) {
		          Log.e(getString(R.string.text_error), e.getMessage());
		          // Error
		          e.printStackTrace();
		      }
		      return mIcon11;
		  }

		  protected void onPostExecute(Bitmap result) {
		      bmImage.setImageBitmap(result);
		  }
	}
}
