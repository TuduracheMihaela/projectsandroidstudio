package com.sharethatdata.digivisitor;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
 
public class DetailFragment extends Fragment {
 
	 String mURL = "";
	 
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  Log.v("DetailFragment", "onCreate()");
	 }
	 
	 @Override
	 public void onActivityCreated(Bundle savedInstanceState) {
	  super.onActivityCreated(savedInstanceState);
	  Log.v("DetailFragment", "onActivityCreated()");
	  if (savedInstanceState != null) {
	   
	  }
	  if(!mURL.trim().equalsIgnoreCase("")){
	
	  }
	  
	  
	  Button btnSubmit = (Button) getActivity().findViewById(R.id.btnSubmit);
	 
	  btnSubmit.setOnClickListener(new OnClickListener() {
	 
	  @Override
	  public void onClick(View v) {
			  
			  Intent intent = new Intent(v.getContext(), IntroActivity.class); startActivity(intent); 
		  }
	 
		});
	 }
	 
	 @Override
	 public void onSaveInstanceState(Bundle outState) {
	  super.onSaveInstanceState(outState);
	  outState.putString("currentURL", mURL);
	 }
	 
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
	  Log.v("DetailFragment", "onCreateView()");
	  View view = inflater.inflate(R.layout.detail_view, container, false);
	  return view;
	 }
	 
	 public void setURLContent(String URL) {
		 mURL = URL;
	 }
	 
	 public void updateURLContent(String URL) {
		
		 mURL = URL;
	  	TextView myView = (TextView) getView().findViewById(R.id.textView1);
	  	myView.setText(mURL);
	  	
		ImageView bmImage = (ImageView) getActivity().findViewById(R.id.imageView1);
		
		if (mURL == "Nederlands") bmImage.setImageResource(R.drawable.nllarge);
		if (mURL == "Engels") bmImage.setImageResource(R.drawable.uklarge);
		if (mURL == "Duits") bmImage.setImageResource(R.drawable.delarge);
		if (mURL == "Frans") bmImage.setImageResource(R.drawable.frlarge);
		if (mURL == "Chinees") bmImage.setImageResource(R.drawable.cnlarge);
		if (mURL == "Russisch") bmImage.setImageResource(R.drawable.rularge);
	
		bmImage.setVisibility(View.VISIBLE);;
	 }
	 
	
}