package com.sharethatdata.digifilefinder;

import android.content.Context;
import android.text.Spanned;

import java.util.ArrayList;
import java.util.List;

public class MyGlobals extends com.sharethatdata.crash.report.MyCrashReport {

	 private static Context mContext;

		public class myValue {
			public String name;
			public String value;
			public Spanned valueHtml;

			public myValue()
			{
				name = "";
				value = "";
				valueHtml = null;
			}
		}
	
	  public class myValueArray {
		  public String name;
		  public ArrayList<String> value;
		  public Spanned valueHtml;
		  
		  public myValueArray()
		  {
			name = "";
			value = new ArrayList<String>();
			valueHtml = null;
		  }
	  }
		
	  private myValue[] myValues;
	  private myValueArray[] myValuesArray;
	  private int numberOfValues = 0;
	  private int numberOfValuesArray = 0;

	  
	  public MyGlobals()
	  {
		  myValues = new myValue[100];
		  myValuesArray = new myValueArray[100];

		  this.mContext = this;
	  }
	  

	  public static Context getContext(){
	        return mContext;
	  }
	  
	  public String getValue(String n){

		  for(int i=0; i < numberOfValues; i++)
		  {
			  if (myValues[i].name == n)
			  {
				  return myValues[i].value;
			  }
		  }
		  return "";
	  }

	public ArrayList<String> getValueArray(String n){

		ArrayList<String> v = new ArrayList<String>();

		for(int i=0; i < numberOfValuesArray; i++)
		{
			if (myValuesArray[i].name == n)
			{
				for(int j=0; j<myValuesArray[i].value.size(); j++){
					v.add(myValuesArray[i].value.get(j));
					//v.add(myValuesArray[i].value);
					//return myValues[i].value;
				}
			}
		}
		return v;
	}
	  
	  public void setValue(String n, String s){
		  // find existing
		  boolean found = false;
		  if (numberOfValues > 0)
		  {
			  for(int i=0; i < numberOfValues; i++)
			  {
				  if (myValues[i].name == n)
				  {
					  myValues[i].value = s;
					  found = true;
				  }
			  }
		  }
		  if (found == false)
		  {
			  int values_len = numberOfValues;
			  if (values_len < 0) values_len = 0;
			  myValue value = new myValue();
			  value.name = n;
			  value.value = s;
			  myValues[numberOfValues] = value;
			  numberOfValues++;
		  }
	  }

	public void setValueArray(String n, ArrayList<String> s){
		// find existing
		boolean found = false;
		if (numberOfValuesArray > 0)
		{
			for(int i=0; i < numberOfValuesArray; i++)
			{
				if (myValuesArray[i].name == n)
				{
					myValuesArray[i].value.clear();
					for(int j=0; j<s.size(); j++){
						//for(int z=0; z<myValuesArray[i].value.size(); z++){
							//myValuesArray[i].value = s.get(i).toString();
							//myValuesArray[i].value.get(z) = s.get(i).toString();
							// rewrite the values if same name "n" exist already
							//myValuesArray[i].value.get(z).toString().equals(s.get(j).toString());
							myValuesArray[i].value.add(s.get(j).toString());
							found = true;
						//}
					}
					//myValues[i].value = s;
					//found = true;
				}
			}
		}
		if (found == false)
		{
			int values_len = numberOfValuesArray;
			if (values_len < 0) values_len = 0;
			myValueArray value = new myValueArray();
			value.name = n;
			value.value = s;
			//for(int j=0; j<s.size(); j++)
			myValuesArray[numberOfValuesArray] = value;
			numberOfValuesArray++;
		}
	}

	  
}

