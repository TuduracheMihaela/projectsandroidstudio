package com.sharethatdata.digifilefinder;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.github.chrisbanes.photoview.PhotoView;
import com.sharethatdata.digifilefinder.help.AppWebViewClients;
import com.sharethatdata.digifilefinder.photogallery.GalleryWidget.GalleryViewPager;
import com.sharethatdata.digifilefinder.photogallery.GalleryWidget.UrlPagerAdapter;
import com.sharethatdata.digifilefinder.photoview.PhotoView;
import com.sharethatdata.sharedactivity.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cContact;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by gotic_000 on 5/14/2018.
 */

public class FileDetailActivity extends AppCompatActivity implements View.OnClickListener {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;
    //WSRoboDataProvider MyRoboProvider = null;

    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    ProgressDialog progressDialog;

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    //ArrayList<HashMap<String, String>> employeeList; // getUsers
    List<cContact> employeeList; // getEmployees

    ArrayList<String> listImagesFullArray = new ArrayList<String>(0);
    ArrayList<String> listImagesThumbArray = new ArrayList<String>(0);
    ArrayList<String> listImagesIdArray = new ArrayList<String>(0);

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String companyId = "";

    String myImageDataThumb = "";
    String myImageDataFull = "";
    String myResourceID = "";
    String groupName = "";

    String textSelectCoWorkerString;

    TextView textViewName, textViewCode, textViewDescription, textViewKeySearch, textViewGroupId,
            textViewGroup,  textViewOwner, textViewDateTimeCreated,
            textViewInUse, textViewLocation, textViewResourceType, textViewFileId, textViewImageId;

    ImageView imageViewResource;

    boolean suc = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(FileDetailActivity.this, FileDetailActivity.this);

        Utils = new Utils();
        Utils.setOrientation(FileDetailActivity.this, FileDetailActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
        //MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
        //MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewCode = (TextView) findViewById(R.id.textViewCode);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);
        textViewKeySearch = (TextView) findViewById(R.id.textViewKeySearch);
        textViewGroupId = (TextView) findViewById(R.id.textViewGroupId);
        textViewGroup = (TextView) findViewById(R.id.textViewGroup);
        textViewOwner = (TextView) findViewById(R.id.textViewOwner);
        textViewInUse = (TextView) findViewById(R.id.textViewInUse);
        textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        textViewDateTimeCreated = (TextView) findViewById(R.id.textViewDatetimeCreated);
        textViewResourceType = (TextView) findViewById(R.id.textViewResourceType);
        textViewFileId = (TextView) findViewById(R.id.textViewFileId);
        textViewImageId = (TextView) findViewById(R.id.textViewImageId);


        Button buttonEdit = (Button) findViewById(R.id.buttonEditCustomer);
        Button buttonDelete = (Button) findViewById(R.id.buttonDeleteCustomer);
        Button buttonCopy = (Button) findViewById(R.id.buttonSendCustomer);

        buttonEdit.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        buttonCopy.setOnClickListener(this);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            myResourceID = extras.getString("resourceid");
        }


        // Hook up clicks on the thumbnail views.

        final View thumb1View = findViewById(R.id.imageView);
        thumb1View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textViewResourceType.getText().toString().equals("document")){
                    new LoadProjectItemDocument(thumb1View, textViewFileId.getText().toString()).execute();
                }else{
                    if(myImageDataFull.toString().equals("")){
                        new LoadProjectItemImage(thumb1View).execute(); // load full image
                    }else {
                        if(!myImageDataFull.equals("")) {
                            String updateResource = Globals.getValue("file_update");
                            if(updateResource != ""){
                                Globals.setValue("file_update","");
                                new LoadProjectItemImage(thumb1View).execute();
                            }else{
                                zoomImageFromThumb(thumb1View, myImageDataFull);
                            }
                        }
                    }
                }

                /*if(myImageDataFull.toString().equals("")){
                    new LoadProjectItemImage(thumb1View).execute();
                }else {
                    if(textViewResourceType.getText().equals("document")){
                        new LoadProjectItemDocument(thumb1View, textViewFileId.getText().toString()).execute();
                    }else{
                        if(!myImageDataFull.equals("")) zoomImageFromThumb(thumb1View, myImageDataFull);
                    }
                }*/
            }
        });

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);



        new LoadProjectItem().execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        String updateResource = Globals.getValue("file_update");
        if(updateResource != ""){
            Globals.setValue("edit", "");
            new LoadProjectItem().execute();
        }
    }

    //public void onClickEdit(View v){
        /*if(myImageDataFull.toString().equals("")){
            Globals.setValue("edit", "edit");
            if(textViewResourceType.getText().toString().equals("image_resource")){
                new LoadProjectItemImage(null).execute();
            }else if(textViewResourceType.getText().toString().equals("multiple_images")){
                new LoadFullImageGallery(false).execute(); // load full image string, no zooming
            }

        }else{
            Globals.setValue("edit", "edit");
            Globals.setValue("resourceID", myResourceID.toString());

            Globals.setValue("resourceTitle", textViewName.getText().toString());
            Globals.setValue("myResourceGroupID", textViewGroupId.getText().toString());
            Globals.setValue("resourceLocation", textViewLocation.getText().toString());
            Globals.setValue("resourceDescription", textViewDescription.getText().toString());
            Globals.setValue("resourceKeySearch", textViewKeySearch.getText().toString());
            Globals.setValue("imageString", myImageDataFull.toString());
            Globals.setValue("imageid", textViewFileId.toString());
            Globals.setValue("fileid", textViewImageId.toString());



            String name = textViewName.getText().toString();
            String group = textViewGroupId.getText().toString();
            String location = textViewLocation.getText().toString();

            Intent i = new Intent(FileDetailActivity.this, FileCreateActivity.class);

            // in case of multiple images, load multiple
            if(listImagesFullArray.size() > 0){
                //Globals.setValue("imageStringArray", listImagesFullArray);
                i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
            }else{
                listImagesFullArray = new ArrayList<String>();
                listImagesIdArray = new ArrayList<String>();
                //Globals.setValue("imageStringArray", listImagesFullArray);
                i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
            }

            startActivity(i);
        }*/

    //}

    //public void onClickDelete(View v){
        /*new AlertDialog.Builder(FileDetailActivity.this)
                .setTitle("File")
                .setMessage("Do you really want to delete this file?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        new DeleteResource(myResourceID, companyId).execute();

                    }})
                .setNegativeButton(android.R.string.no, null).show();*/
    //}

    //public void onClickSend(View v){
        //new LoadEmployeeList().execute();

        /*Boolean checked[] = new Boolean[arr.size()];
        new AlertDialog.Builder(FileDetailActivity.this)
                .setTitle("Send File")
                .setMessage("Please select one or more users:")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMultiChoiceItems(animals, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // user checked or unchecked a box
                        if (isChecked) {
                            checked[indexSelected]=true;
                            brandNameIDMap.put(brandList.getItem(indexSelected).getProductID(), brandList.getItem(indexSelected).getProductName());
                            //Log.i("ID", "onClick: "+brandList.getItem(indexSelected).getProductID());
                            //seletedItems.add(indexSelected);
                        } else{
                            checked[indexSelected]=false;
                            brandNameIDMap.remove(brandList.getItem(indexSelected).getProductID());
                        }
                    }
                })
                .setPositiveButton(getString(R.string.sendButtonText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {



                    }})
                .setNegativeButton(android.R.string.cancel, null).show();*/
    //}

    @Override
    public void onClick(View v) {
        //new LoadFullImageGallery(true).execute(); // load full image for zoom
        switch(v.getId()){
            case -1:
                new LoadFullImageGallery(true).execute(); // load full image for zoom
                break;
            case R.id.buttonEditCustomer:
                if(myImageDataFull.toString().equals("")){
                    Globals.setValue("edit", "edit");
                    if(textViewResourceType.getText().toString().equals("image_resource")){
                        new LoadProjectItemImage(null).execute();
                    }else if(textViewResourceType.getText().toString().equals("multiple_images")){
                        //new LoadFullImageGallery(false).execute(); // load full image string, no zooming
                        Toast.makeText(FileDetailActivity.this, "Sorry, the functionality is not done.", Toast.LENGTH_SHORT).show();
                    }else if(textViewResourceType.getText().toString().equals("document")){
                        new LoadProjectItemImage(null).execute();
                    }else{
                        new LoadProjectItemImage(null).execute();
                    }

                }else{
                    Globals.setValue("edit", "edit");
                    Globals.setValue("resourceID", myResourceID.toString());

                    Globals.setValue("resourceTitle", textViewName.getText().toString());
                    Globals.setValue("myResourceGroupID", textViewGroupId.getText().toString());
                    Globals.setValue("resourceLocation", textViewLocation.getText().toString());
                    Globals.setValue("resourceDescription", textViewDescription.getText().toString());
                    Globals.setValue("resourceKeySearch", textViewKeySearch.getText().toString());
                    Globals.setValue("resourceType", textViewResourceType.getText().toString());
                    Globals.setValue("imageString", myImageDataFull.toString());
                    Globals.setValue("imageid", textViewFileId.toString());
                    Globals.setValue("fileid", textViewImageId.toString());



                    String name = textViewName.getText().toString();
                    String group = textViewGroupId.getText().toString();
                    String location = textViewLocation.getText().toString();

                    Intent i = new Intent(FileDetailActivity.this, FileCreateActivity.class);

                    // in case of multiple images, load multiple
                    if(listImagesFullArray.size() > 0){
                        try{
                            Globals.setValueArray("imageStringArray", listImagesFullArray);
                            //i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                            //i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        try{
                            listImagesFullArray = new ArrayList<String>(0);
                            listImagesIdArray = new ArrayList<String>(0);
                            Globals.setValueArray("imageStringArray", listImagesFullArray);
                            //i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                            //i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    startActivity(i);
                }
                break;
            case R.id.buttonDeleteCustomer:
                new AlertDialog.Builder(FileDetailActivity.this)
                        .setTitle("File")
                        .setMessage("Do you really want to delete this file?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                new DeleteResource(myResourceID, companyId).execute();

                            }})
                        .setNegativeButton(android.R.string.no, null).show();
                break;
            case R.id.buttonSendCustomer:
                new LoadEmployeeList().execute();
                break;
            default:
                break;
        }
    }



    class LoadProjectItem extends AsyncTask<Object, Object, String> {

        cResource resource;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(Object... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resource = MyProvider.getResourceDetail(myResourceID, companyId);
            if (resource != null)
            {
                if (resource.image_id != "0")
                {
                    myImageDataThumb = resource.image_thumb;
                    myImageDataFull = "";
                }
            }

            List<cResourceGroup> resotList = MyProvider.getResourceGroup(companyId, "");
            for(cResourceGroup entry : resotList)
            {
                if( Long.toString(entry.id).equals(resource.groupID)){
                    groupName = entry.name;
                }
            }


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textViewName.setText(Html.fromHtml(resource.name));
                    textViewCode.setText(Html.fromHtml(resource.code));
                    textViewDescription.setText(Html.fromHtml(resource.description));
                    textViewKeySearch.setText(Html.fromHtml(resource.search));
                    textViewGroupId.setText(Html.fromHtml(resource.groupID));
                    textViewGroup.setText(Html.fromHtml(groupName));
                    textViewOwner.setText(Html.fromHtml(resource.owner));
                    //textViewInUse.setText(Html.fromHtml(resource.inuse));
                    textViewLocation.setText(Html.fromHtml(resource.location));
                    textViewDateTimeCreated.setText(Html.fromHtml(resource.created_date));
                    textViewResourceType.setText(Html.fromHtml(resource.resource_type));
                    textViewFileId.setText(Html.fromHtml(resource.file_id));
                    textViewImageId.setText(Html.fromHtml(resource.image_id));


                    ImageView imageView1 = (ImageView) findViewById(R.id.imageView);

                    if(!myImageDataThumb.equals("image_array")){
                        Bitmap d = ConvertByteArrayToBitmap(myImageDataThumb);
                        if (d != null)
                        {
                            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                            imageView1.setImageBitmap(scaled);
                        }
                    }else if(myImageDataThumb.equals("image_array")){
                        LinearLayout linearLayout = findViewById(R.id.layoutPicture1); //LinearLayOut Setup
                        linearLayout.removeAllViews();

                        imageView1.setVisibility(View.GONE);

                        final List<cResource> multiImagesList = resource.list;

                        for(int i = 0; i < multiImagesList.size(); i++){
                            cResource c = multiImagesList.get(i);
                            String c_array = c.image_array_string;
                            String c_id = c.image_id;

                            listImagesThumbArray = new ArrayList<String>();
                            listImagesIdArray = new ArrayList<String>();

                            listImagesThumbArray.add(c_array);
                            listImagesIdArray.add(c_id);
                            Bitmap d = ConvertByteArrayToBitmap(c_array);
                            if (d != null)
                            {
                                int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                                Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);

                                //ImageView imageView = new ImageView(FileDetailActivity.this); //ImageView Setup
                                imageViewResource = new ImageView(FileDetailActivity.this); //ImageView Setup
                                imageViewResource.setVisibility(View.VISIBLE);
                                imageViewResource.setOnClickListener(FileDetailActivity.this);
                                imageViewResource.setImageBitmap(scaled);

                                imageViewResource.setLayoutParams(new LinearLayout.LayoutParams(300, 300)); //setting image position

                                linearLayout.addView(imageViewResource); //adding view to layout

                                //imageView1.setImageBitmap(scaled);
                            }
                        }
                    }

                }
            });

            return "";
        }

        protected void onPostExecute(String result) {
            if (FileDetailActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            hideProgressDialog();
        }
    }

    class LoadFullImageGallery extends AsyncTask<String, String, String>{
        boolean load_full; // load full image just for zoom,
                            // in case the edit button was pressed,
                             // just get the full string

        public LoadFullImageGallery(boolean load_full){
            this.load_full = load_full;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... params) {
            // load full image once when click on image to zoom it
            if(myImageDataFull.equals("")){
                final cResource r = MyProvider.getResourceDetailImage(myResourceID, companyId);
                if (r != null)
                {
                    if (r.image_id != "0")
                    {
                        myImageDataFull = r.image_full;
                    }
                }

                listImagesFullArray = new ArrayList<String>();
                listImagesIdArray = new ArrayList<String>();

                final List<cResource> multiImagesList = r.list;
                for(int i = 0; i < multiImagesList.size(); i++){
                    cResource c = multiImagesList.get(i);
                    String c_array = c.image_array_string;
                    String c_id = c.image_id;
                    listImagesFullArray.add(c_array);
                    listImagesIdArray.add(c_id);
                }
            }

            return "";
        }

        protected void onPostExecute(String result) {
            if (FileDetailActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            if(load_full)
            {
                // true
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(FileDetailActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_gallery, null);

                UrlPagerAdapter pagerAdapter = new UrlPagerAdapter(FileDetailActivity.this, listImagesFullArray);
                GalleryViewPager mViewPager = mView.findViewById(R.id.viewer);
                mViewPager.setOffscreenPageLimit(0);
                mViewPager.setAdapter(pagerAdapter);

                mBuilder.setView(mView);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog mDialog = mBuilder.create();
                        mDialog.show();
                    }
                });
            }

            String edit = Globals.getValue("edit");
            if(!edit.equals("")){
                Globals.setValue("resourceID", myResourceID.toString());

                Globals.setValue("resourceTitle", textViewName.getText().toString());
                Globals.setValue("myResourceGroupID", textViewGroupId.getText().toString());
                Globals.setValue("resourceLocation", textViewLocation.getText().toString());
                Globals.setValue("resourceDescription", textViewDescription.getText().toString());
                Globals.setValue("resourceKeySearch", textViewKeySearch.getText().toString());
                Globals.setValue("imageString", myImageDataFull.toString());

                Intent i = new Intent(FileDetailActivity.this, FileCreateActivity.class);

                // in case of multiple images, load multiple
                if(listImagesFullArray.size() > 0){
                    try{
                        Globals.setValueArray("imageStringArray", listImagesFullArray);
                        //i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                        //i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{
                    try{
                        listImagesFullArray = new ArrayList<String>();
                        listImagesIdArray = new ArrayList<String>();
                        Globals.setValueArray("imageStringArray", listImagesFullArray);
                        //i.putStringArrayListExtra("imageStringArray", listImagesFullArray);
                        //i.putStringArrayListExtra("imageIntArray", listImagesIdArray);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                startActivity(i);
            }
        }
    }

    class LoadProjectItemImage extends AsyncTask<String, String, String> {
        View view;

        public LoadProjectItemImage(View view){
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cResource resource = MyProvider.getResourceDetailImage(myResourceID, companyId);
            if (resource != null)
            {
                if (resource.image_id != "0")
                {
                    myImageDataFull = resource.image_full;
                }
            }

            return "";
        }

        protected void onPostExecute(String result) {
            if (FileDetailActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();
            if(view != null && !myImageDataFull.equals("")) zoomImageFromThumb(view, myImageDataFull);

            String edit = Globals.getValue("edit");
            if(!edit.equals("")){
                Globals.setValue("resourceID", myResourceID.toString());

                Globals.setValue("resourceTitle", textViewName.getText().toString());
                Globals.setValue("myResourceGroupID", textViewGroupId.getText().toString());
                Globals.setValue("resourceLocation", textViewLocation.getText().toString());
                Globals.setValue("resourceDescription", textViewDescription.getText().toString());
                Globals.setValue("resourceKeySearch", textViewKeySearch.getText().toString());
                Globals.setValue("resourceType", textViewResourceType.getText().toString());
                Globals.setValue("imageString", myImageDataFull.toString());
                Globals.setValueArray("imageStringArray",new ArrayList<String>(0));

                Intent i = new Intent(FileDetailActivity.this, FileCreateActivity.class);
                startActivity(i);
            }
        }
    }

    class LoadProjectItemDocument extends AsyncTask<String, String, String> {
        View view;
        String id;

        public LoadProjectItemDocument(View view, String id){
            this.view = view;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cResource resource = MyProvider.getResourceDetailFile(id, companyId);
            myImageDataFull = resource.filename;

            return myImageDataFull;
        }

        protected void onPostExecute(String result) {
            if (FileDetailActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();
            //if(view != null && !myImageDataFull.equals("")) zoomImageFromThumb(view, myImageDataFull);
            if(!myImageDataFull.equals("")){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                String url = Globals.getValue("ws_url") + "/" + myImageDataFull;
                Uri url_pdf = Uri.parse(Globals.getValue("ws_url") + "/" + myImageDataFull);
                intent.setData(url_pdf);
                //intent.setData(Uri.parse(Globals.getValue("ws_url") + "/" + myImageDataFull));
                //intent.setData(Uri.parse("https://dev-ws.sharethatdata.com/snowcloud/" + myImageDataFull));
                //intent.setData(Uri.parse("https://dev-ws.sharethatdata.com/snowcloud/files/sample.pdf"));
                startActivity(intent);

                //Intent i = new Intent(FileDetailActivity.this, WebViewActivity.class);
                //i.putExtra("url","https://dev-ws.sharethatdata.com/snowcloud/" + myImageDataFull);
                //startActivity(i);


                //openDocument("https://dev-ws.sharethatdata.com/snowcloud/" + myImageDataFull);
            }

            String edit = Globals.getValue("edit");
            if(!edit.equals("")){
                Globals.setValue("resourceID", myResourceID.toString());

                Globals.setValue("resourceTitle", textViewName.getText().toString());
                Globals.setValue("myResourceGroupID", textViewGroupId.getText().toString());
                Globals.setValue("resourceLocation", textViewLocation.getText().toString());
                Globals.setValue("resourceDescription", textViewDescription.getText().toString());
                Globals.setValue("resourceKeySearch", textViewKeySearch.getText().toString());
                Globals.setValue("imageString", myImageDataFull.toString());
                Globals.setValueArray("imageStringArray",new ArrayList<String>(0));

                Intent i = new Intent(FileDetailActivity.this, FileCreateActivity.class);
                startActivity(i);
            }
        }
    }

    public void openDocument(String name) {
        WebView urlWebView = (WebView)findViewById(R.id.containWebView);
        urlWebView.setWebViewClient(new AppWebViewClients());
        urlWebView.getSettings().setJavaScriptEnabled(true);
        urlWebView.getSettings().setUseWideViewPort(true);
        urlWebView.loadUrl(name);
    }

    private class DeleteResource extends AsyncTask<String, String, Boolean> {
        String myID = "";
        String companyID = "";

        public DeleteResource(String id, String companyid) {
            myID = id;
            companyID = companyid;

        }

        protected Boolean doInBackground(String... urls) {

            boolean suc = false;

            suc = MyProvider.deleteResource(myID, companyID);

            return suc;
        }

        protected void onPostExecute(Boolean result) {

            if(result == true){
                Toast.makeText(getApplicationContext(), "File deleted", Toast.LENGTH_SHORT).show();
                Globals.setValue("file_update", "file_update");
                finish();

            }else{
                Toast.makeText(getApplicationContext(), "Was not deleted the file", Toast.LENGTH_SHORT).show();
                Globals.setValue("file_update", "file_update");
                finish();
            }


        }
    }


    /**
     * Background Async Task to Load all users by making HTTP Request
     * */
    class LoadEmployeeList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplicationContext());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            //employeeList = new ArrayList<HashMap<String, String>>();
            employeeList = MyProvider.getUsers("", companyId);

            /*List<cContact> myContact = MyProvider.getUsers("", companyId);
            for(cContact entry : myContact)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                //map.put("username", entry.username);
                map.put("name", entry.name);

                // adding HashList to ArrayList
                employeeList.add(map);
            }*/

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (employeeList.size() > 0)
                    {
                        final List<String> employee_list = new ArrayList<String>();
                        final List<String> employee_list_ids = new ArrayList<String>();
                        // create array of string
                        for(cContact v : employeeList)
                        {
                            employee_list.add(v.name);
                            employee_list_ids.add(v.id);

                        }
                        final CharSequence[] items = employee_list.toArray(new String[employee_list.size()]);
                        final ArrayList<String> mSelectedItems = new ArrayList<String>();
                        //final TextView mView =(TextView) findViewById(R.id.selectCoworkerSchedule_textView);

                        final StringBuilder build = new StringBuilder();

                        // 1. Instantiate an AlertDialog.Builder with its constructor
                        AlertDialog.Builder builder = new AlertDialog.Builder(FileDetailActivity.this);

                        // 2. Chain together various setter methods to set the dialog characteristics
                         builder.setTitle("Send File")
                                //.setMessage("Please select one or more users:")
                                .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        if (isChecked) {
                                            mSelectedItems.add((String) items[which]);
                                        }else{
                                            if(mSelectedItems.contains(items[which]))
                                                mSelectedItems.remove(items[which]);
                                        }
                                    }
                                });

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                build.append(mSelectedItems.toString());
                                //mView.setText(build);
                                textSelectCoWorkerString = build.toString();

                                if (textSelectCoWorkerString != "" && textSelectCoWorkerString != null){

                                    //String members = textSelectCoWorkerString.replace("[", "");
                                    //members = members.replace("]", "");
                                    //String[] member_list = members.split(",");

                                    final ArrayList<String> mIdItems = new ArrayList<String>();

                                    for (int i=0;i<employee_list.size();i++) {
                                        for (int j=0;j<mSelectedItems.size();j++) {
                                            if (employee_list.get(i).contains(mSelectedItems.get(j))) {
                                                Log.d("xxx", "found");
                                                Log.d("xxx", "i index " + i);
                                                //Log.d("xxx", "j index " + j);
                                                mIdItems.add(String.valueOf(i));
                                            }
                                        }
                                    }

                                    if(mIdItems.size() > 0 ){
                                        for (int j=0;j<mIdItems.size();j++) {
                                            String id =  employee_list_ids.get(Integer.valueOf(mIdItems.get(j)));
                                            Log.d("xxx", "j index " + j);

                                            new CreateResource(id).execute();
                                        }


                                        /*for (int i=0;i<employee_list_ids.size();i++) {
                                            for (int j=0;j<mIdItems.size();j++) {
                                                if (employee_list_ids.get(i).contains(mIdItems.get(j))) {
                                                    String id = employee_list_ids.get(i);
                                                    Log.d("xxx", "found");
                                                    Log.d("xxx", "i index " + i);
                                                    //Log.d("xxx", "j index " + j);

                                                }
                                            }
                                        }*/
                                    }

                                }
                            }
                        });

                        // 3. Get the AlertDialog from create()
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        // display images in row
                    }
                }
            });
        }
    }


    // Copy resource to another user
    private class CreateResource extends AsyncTask<String, String, Boolean>{

        public String user_id;

        public CreateResource(String user_id){
            this.user_id = user_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));


            final String resourceTitle_text = textViewName.getText().toString();
            final String resourceLocation_text = textViewLocation.getText().toString();
            final String resourceDescription_text = textViewDescription.getText().toString();
            final String resourceKeyword_text = textViewKeySearch.getText().toString();
            final String myResourceGroupID = textViewGroupId.getText().toString();

            int ResourceID = 0;


            /*if(!myImageDataThumb.toString().equals("")){
                myBitmap = ConvertByteArrayToBitmap(myImageDataThumb);
            }*/

            ////////////////////////////////////////////////////////////

            /*if(!myImageDataFull.toString().equals("")){
                myBitmap = ConvertByteArrayToBitmap(myImageDataFull);
            }else{
                Globals = ((MyGlobals) getApplicationContext());
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                final cResource resource = MyProvider.getResourceDetailImage(myResourceID, companyId);
                if (resource != null)
                {
                    if (resource.image_id != "0")
                    {
                        myImageDataFull = resource.image_full;
                        myBitmap = ConvertByteArrayToBitmap(myImageDataFull);
                    }
                }
            }

                // image was selected
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                byte[] bitmapByteArray = stream.toByteArray();

                byte[] bitmapByteArrayThumbnail;
                bitmapByteArrayThumbnail = bitmapByteArray;
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArrayThumbnail = stream.toByteArray();


                BitmapID = MyProvider.createImage("image_resource", bitmapByteArray, bitmapByteArrayThumbnail, companyId);
                suc = true;


                // Create Resource
                if(BitmapID > 0){
                    suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, user_id, String.valueOf(BitmapID), companyId);
                }else {
                    Toast.makeText(FileDetailActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                }*/


            //runOnUiThread(new Runnable() {
                //@Override
                //public void run() {
                    if(textViewResourceType.getText().toString().equals("document")){
                        // document

                        // Read file to byte array
                        //try {
                        //    bitmapByteArray = loadFile(FilePath1);
                        //} catch (IOException e) {
                        //    e.printStackTrace();
                        //}

                        //ResultID = MyProvider.createPdf("pdf", bitmapByteArray, companyId);
                        //String result[] = ResultID.split(",");
                        //String fileid = result[0];
                        //String imageid = result[1];

                        // Create Resource
                        //suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, user_id, imageid, fileid, "document", companyId);
                        suc = false;
                    }else if(textViewResourceType.getText().toString().equals("multiple_images")){
                        suc = false;
                    }else if(textViewResourceType.getText().toString().equals("image_resource")){
                        // image

                        int BitmapID = 0;
                        Bitmap myBitmap = null;

                        if(!myImageDataFull.toString().equals("")){
                            myBitmap = ConvertByteArrayToBitmap(myImageDataFull);
                        }else{
                            Globals = ((MyGlobals) getApplicationContext());
                            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                            cResource resource = MyProvider.getResourceDetailImage(myResourceID, companyId);
                            if (resource != null)
                            {
                                if (resource.image_id != "0")
                                {
                                    myImageDataFull = resource.image_full;
                                    myBitmap = ConvertByteArrayToBitmap(myImageDataFull);
                                }
                            }
                        }

                        // image was selected
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                        byte[] bitmapByteArray = stream.toByteArray();

                        byte[] bitmapByteArrayThumbnail;
                        bitmapByteArrayThumbnail = bitmapByteArray;
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                        Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                        bitmapByteArrayThumbnail = stream.toByteArray();


                        BitmapID = MyProvider.createImage("image_resource", bitmapByteArray, bitmapByteArrayThumbnail, companyId);
                        suc = true;


                        // Create Resource
                        if(BitmapID > 0){
                            ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, user_id, String.valueOf(BitmapID), "0", "image", companyId);
                        }else {
                            Toast.makeText(FileDetailActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        // none - no image - no pdf
                        ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, user_id, "0", "0", "none", companyId);
                    }
                //}
            //});


            if(ResourceID > 0) suc = true; else suc = false;

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        if(textViewResourceType.getText().toString().equals("document") || textViewResourceType.getText().toString().equals("multiple_images")) {
                            Toast.makeText(FileDetailActivity.this, "Sorry.Not implemented yet!", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(FileDetailActivity.this, "Was not created the file", Toast.LENGTH_SHORT).show();
                        }

                    }else{

                        Toast.makeText(FileDetailActivity.this, "The file was created!", Toast.LENGTH_SHORT).show();

                        finish();
                    }
                }
            });
        }
    }


    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    private void zoomImageFromThumb(final View thumbView, String imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        //final ImageView expandedImageView = (ImageView) findViewById(R.id.expanded_image);
        final PhotoView expandedImageView = (PhotoView) findViewById(R.id.expanded_image);

        byte[] decodedString;
        if(!imageResId.equals("")){
            String resizeString =  resizeBase64Image(imageResId);
            decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
        }else{
            decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
        }


        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null)
        {
            //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
            //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
            expandedImageView.setImageBitmap(decodedByte);
        }


        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        /*AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();*/

        AnimatorSet set = new AnimatorSet();
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // is tablet
            if(startScale <= 0.4){
                // image portrait

                set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                startBounds.top, finalBounds.top))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                //}else if(startScale > 0.4){
            }else if(startScale > 0.4){
                // image landscape = 0.4411277

                set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                startBounds.top, finalBounds.top))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mCurrentAnimator = null;
                    }
                });
                set.start();
            }

        } else {
            // is phone
            set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                    startBounds.left, finalBounds.left))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                            startBounds.top, finalBounds.top))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                            startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                    View.SCALE_Y, startScale, 2f));
            set.setDuration(mShortAnimationDuration);
            set.setInterpolator(new DecelerateInterpolator());
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mCurrentAnimator = null;
                }
            });
            set.start();
        }

        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }

    public String resizeBase64Image(String base64image){
        int IMG_WIDTH = 800;
        int IMG_HEIGHT = 600;
        byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


        if(image.getHeight() <= 400 && image.getWidth() <= 400){
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100, baos);

        byte [] b=baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(FileDetailActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private void openRenderer(Context context) throws IOException {
        // In this sample, we read a PDF from the assets directory.
        File file = new File(context.getCacheDir(), "FILENAME");
        if (!file.exists()) {
            // Since PdfRenderer cannot handle the compressed asset file directly, we copy it into
            // the cache directory.
            InputStream asset = context.getAssets().open("FILENAME");
            FileOutputStream output = new FileOutputStream(file);
            final byte[] buffer = new byte[1024];
            int size;
            while ((size = asset.read(buffer)) != -1) {
                output.write(buffer, 0, size);
            }
            asset.close();
            output.close();
        }
        ParcelFileDescriptor mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        PdfRenderer mPdfRenderer;
        // This is the PdfRenderer we use to render the PDF.
        if (mFileDescriptor != null) {
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        }



    }
}
