package com.sharethatdata.digifilefinder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.sharethatdata.sharedactivity.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by gotic_000 on 4/21/2018.
 */

public class MainActivity extends AppCompatActivity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(MainActivity.this, MainActivity.this);

        Utils = new Utils();
        Utils.setOrientation(MainActivity.this, MainActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));


        Button addResourceButton = (Button) findViewById(R.id.add_resource_button); // New Route
        Button searchResourceButton = (Button) findViewById(R.id.search_resource_button); // My Routes

        addResourceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FileCreateActivity.class);
                startActivity(intent);
            }
        });

        searchResourceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FileSearchActivity.class);
                startActivity(intent);
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_admin, menu);


        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent1 = new Intent(this, SettingsAdminActivity.class); startActivity(intent1);
                return true;
            case R.id.action_create_file:
                Intent intent2 = new Intent(this, FileCreateActivity.class); startActivity(intent2);
                return true;
            case R.id.action_create_group:
                Intent intent3 = new Intent(this, FileCreateGroupActivity.class); startActivity(intent3);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
