package com.sharethatdata.digifilefinder.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digifilefinder.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by gotic_000 on 4/27/2018.
 */

public class SearchAdapter extends ArrayAdapter<HashMap<String, String>> implements Filterable {
    //private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    private ItemFilter mFilter = new ItemFilter();
    private ArrayList<HashMap<String, String>> originalData = null;
    private ArrayList<HashMap<String, String>> filteredData = null;


    public SearchAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        //this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;

        this.filteredData = adapter ;
        this.originalData = adapter ;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.id = (TextView)view.findViewById(R.id.resourceid);
            holder.name = (TextView)view.findViewById(R.id.name);
            holder.location = (TextView)view.findViewById(R.id.location);
            //holder.status = (TextView)view.findViewById(R.id.status);
            holder.group = (TextView)view.findViewById(R.id.group_name);
            holder.image = (ImageView)view.findViewById(R.id.imageView);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=filteredData.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.id.setText(hashmap_Current.get("resourceid"));
        holder.name.setText(Html.fromHtml(hashmap_Current.get("resource_name")));
        holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));
        //holder.status.setText(Html.fromHtml(hashmap_Current.get("status")));
        holder.group.setText(Html.fromHtml(hashmap_Current.get("resource_group_name")));

        Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image"));
        if (d != null)
        {
            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
            holder.image.setImageBitmap(scaled);
        }

        return view;
    }

    final class ViewHolder {
        public TextView id, name, location, status, group;
        public ImageView image;
    }

    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    public int getCount() {
        return filteredData.size();
    }

    public HashMap<String, String> getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<HashMap<String, String>> list = originalData;

            int count = list.size();
            final ArrayList<HashMap<String, String>> nlist = new ArrayList<>(count);

            HashMap<String, String> filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.toString().toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<HashMap<String, String>>) results.values;
            notifyDataSetChanged();
        }
    }



}