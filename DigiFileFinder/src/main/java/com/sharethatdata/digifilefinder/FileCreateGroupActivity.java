package com.sharethatdata.digifilefinder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.sharedactivity.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by gotic_000 on 2/14/2018.
 */

public class FileCreateGroupActivity extends AppCompatActivity {


    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";
    String companyId = "";

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> groupList;

    EditText editTextName, editTextDescription;

    String resourceName_text;
    String resourceDescription_text;
    Button btnCreateResourceGroup;

    TextView textViewNoGroups;
    TextView textViewGroups;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_file_group);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(FileCreateGroupActivity.this, FileCreateGroupActivity.this);

        Utils = new Utils();
        Utils.setOrientation(FileCreateGroupActivity.this, FileCreateGroupActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged
        mySelectedUser = myUser;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        editTextName = (EditText) findViewById(R.id.editText_name) ;
        editTextDescription = (EditText) findViewById(R.id.editText_description) ;
        btnCreateResourceGroup = (Button) findViewById(R.id.createResourceGroup);
        textViewNoGroups = (TextView) findViewById(R.id.textViewNoGroups);
        textViewGroups = (TextView) findViewById(R.id.textViewGroups);
        listView = (ListView) findViewById(R.id.listView);

        btnCreateResourceGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if( editTextName.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextName.setError("Name is required!");
                }

                if( editTextDescription.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextDescription.setError("Description is required!");
                }

                if(failFlag == false){
                    resourceName_text = editTextName.getText().toString();
                    resourceDescription_text = editTextDescription.getText().toString();

                    alertDialog();
                }
            }
        });

        new LoadGroups(companyId, myUserID).execute();
    }

    private void alertDialog(){
        new AlertDialog.Builder(FileCreateGroupActivity.this)
                .setTitle("Group file")
                .setMessage("Do you want to add this group file?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateFileGroup(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    class LoadGroups extends AsyncTask<String, String, String> {

        public String company_id;
        public String userid;

        public LoadGroups(String company_id, String userid){
            this.company_id = company_id;
            this.userid = userid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            groupList = new ArrayList<HashMap<String, String>>();

            List<cResourceGroup> myGroup = MyProvider.getResourceGroup(company_id, userid);
            for(cResourceGroup entry : myGroup)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                groupList.add(map);
            }

            return String.valueOf(myGroup.size());
        }

        protected void onPostExecute(final String return_value) {
            if (FileCreateGroupActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(groupList.size() > 0){

                        ListAdapter adapter = new SimpleAdapter(FileCreateGroupActivity.this, groupList,
                                R.layout.activity_groups_items_listview,
                                new String[]{"id", "name"},
                                new int[]{R.id.textViewId, R.id.textViewName});
                        listView.setAdapter(adapter);
                        textViewGroups.setVisibility(View.VISIBLE);
                        textViewNoGroups.setVisibility(View.VISIBLE);
                    }else{
                        textViewNoGroups.setVisibility(View.VISIBLE);
                        textViewGroups.setVisibility(View.GONE);
                        listView.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    private class CreateFileGroup extends AsyncTask<String, String, Boolean> {

        String company_id;

        public CreateFileGroup(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            int ResourceGroupID;
            ResourceGroupID = MyProvider.createResourceGroup(resourceName_text,resourceDescription_text, company_id);

            boolean suc = false;
            if(ResourceGroupID != 0){
                suc = true;
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(FileCreateGroupActivity.this, "Was not created the group file ", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(FileCreateGroupActivity.this, "The group file was created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(FileCreateGroupActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
