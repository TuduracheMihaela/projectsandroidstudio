package com.sharethatdata.digifilefinder;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.sharethatdata.sharedactivity.TimeUtils;

/**
 * Created by gotic_000 on 1/17/2018.
 */

public class SettingsAdminActivity extends PreferenceActivity {

    static final String PREFS_NAME = "defaults";

    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(SettingsAdminActivity.this, SettingsAdminActivity.this);

        Utils = new Utils();
        Utils.setOrientation(SettingsAdminActivity.this, SettingsAdminActivity.this);

        // Display the fragment as the main content.
        if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();
    }

    public static class PrefFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getPreferenceManager().setSharedPreferencesName(PREFS_NAME);
            addPreferencesFromResource(R.xml.settings_admin);
        }
    }
}
