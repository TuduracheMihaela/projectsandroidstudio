package com.sharethatdata.digifilefinder;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sharethatdata.digifilefinder.help.AppWebViewClients;

/**
 * Created by tudur on 14-Aug-19.
 */

public class WebViewActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        String url_ = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url_ = extras.getString("url");
        }

        WebView mWebView = (WebView)findViewById(R.id.containWebView);
        final String finalUrl_ = url_;
        /*mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(finalUrl_);
                return false;
            }
        });*/

        //String doc="<iframe src=http://docs.google.com/viewer?url=http://www.iasted.org/conferences/formatting/presentations-tips.ppt&embedded=true width=100% height=100% style=border: none;></iframe>";
        String doc="<iframe src=http://docs.google.com/viewer?url=" + finalUrl_ +
                "&embedded=true width=100% height=100% style=border: none;></iframe>";

        Log.d("FILE: ", doc);

        //mWebView.setWebViewClient(new AppWebViewClients());

        // WORD - is showing
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setDomStorageEnabled(true);

        // EXCEL


        mWebView.loadData( doc , "text/html",  "UTF-8");
        //mWebView.loadUrl(doc);

    }


}
