package com.sharethatdata.digifilefinder;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.sharethatdata.digifilefinder.help.FilePathClass;
import com.sharethatdata.sharedactivity.BarcodeReader.Barcode.BarcodeCaptureActivity;
import com.sharethatdata.sharedactivity.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import org.apache.http.entity.mime.content.FileBody;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;


/**
 * Created by miha on 4/25/2018.
 */

public class FileCreateActivity extends AppCompatActivity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    ProgressDialog progressDialog;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    /////////////////////////////////////////////
    // on Zoom image

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    ////////////////////////////////////////////

    private String mCurrentPhotoPathAbsolute;
    private String mCurrentPhotoPath;
    private static final int BARCODE_READER_REQUEST_CODE = 5;
    private static final int REQUEST_PERMISSIONS_CAMERA = 102;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_BROWSE_PHOTO = 2;
    private static final int PERMISSION_TAKE_PHOTO = 3;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    private Bitmap myBitmap;
    private byte[] bitmapByteArray;

    String FilePath2 ;
    String FilePath1 ;
    String FilePath ;
    String FileName ;
    String extension = "" ;

    File file1;
    FileBody fileBody1;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    boolean boolean_permission_camera = false; // for scanning location
    boolean permission_camera = false; // for taking image
    boolean permission_storage = false; // for taking image

    //ArrayList<HashMap<String, String>> resourceKindList;
    //List<String> resourcekindkeyArray =  new ArrayList<String>();
    ArrayList<HashMap<String, String>> resourceTypeList;
    List<String> resourcetypekeyArray =  new ArrayList<String>();

    ArrayList<Bitmap> mArrayBitmap  = new ArrayList<Bitmap>();
    ArrayList<String> listImagesFullStringArray = new ArrayList<String>();
    ArrayList<String> listImagesFullIntArray = new ArrayList<String>();

    TextView tvInvisibleFileName;
    EditText editTextTitle;
    EditText editTextLocation;
    EditText editTextKeyword;
    EditText editTextDescription;
    Spinner spinnerResourceType;
    ImageView imageViewLocation;
    ImageView imageView1;
    Button btnCreateImage, btnSearchImage;
    Button btnCreateResource;

    String resourceDescription_text;
    String resourceKeyword_text;
    String resourceLocation_text;
    String resourceTitle_text;
    String resourceType_text;
    String myResourceGroupID;

    String myResourceID = "";
    String image_string = "";
    String imageid = "";
    String fileid = "";
    String edit = "";

    boolean isKitKat = false;

    //Intent intent_browse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_file);

        //Requesting camera permission
        //permission_camera();

        //Requesting write storage permission
        //verifyStoragePermissions(FileCreateActivity.this);


        //Requesting read storage permission
        //requestReadStoragePermission();
        //Requesting write storage permission
        //requestWriteStoragePermission();
        //Requesting camera permission
        //requestCameraPermission();

        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }



        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(FileCreateActivity.this, FileCreateActivity.this);

        Utils = new Utils();
        Utils.setOrientation(FileCreateActivity.this, FileCreateActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            /*edit = extras.getString("edit");
            myResourceID = extras.getString("resourceID");

            myResourceTypeID = extras.getString("myResourceTypeID");
            resourceTitle_text = extras.getString("resourceTitle");
            resourceLocation_text = extras.getString("resourceLocation");
            image_string = extras.getString("imageString");*/
        }

        final TextView textViewHintDescription = (TextView) findViewById(R.id.textViewHintDescription);
        final TextView textViewHintKeyword = (TextView) findViewById(R.id.textViewHintKeyword);
        tvInvisibleFileName = (TextView) findViewById(R.id.tvInvisibleFileName);
        editTextTitle = (EditText) findViewById(R.id.editText_title);
        editTextLocation = (EditText) findViewById(R.id.editText_location);
        editTextKeyword = (EditText) findViewById(R.id.editText_keyword_search);
        editTextDescription = (EditText) findViewById(R.id.editText_description);
        imageViewLocation = (ImageView) findViewById(R.id.btnScanLocation) ;
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView1.setVisibility(View.GONE);
        //spinnerResourceKind = (Spinner) findViewById(R.id.spinnerResourceKind);
        spinnerResourceType = (Spinner) findViewById(R.id.spinnerResourceType);
        btnSearchImage = (Button) findViewById(R.id.pictureFromPhone);
        btnCreateImage = (Button) findViewById(R.id.pictureCamera);
        btnCreateResource = (Button) findViewById(R.id.createResource);

        editTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    textViewHintDescription.setTextColor(getResources().getColor(R.color.colorAccent));
                }else{
                    textViewHintDescription.setTextColor(getResources().getColor(R.color.grey04box));
                }
            }
        });

        editTextKeyword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    textViewHintKeyword.setTextColor(getResources().getColor(R.color.colorAccent));
                }else{
                    textViewHintKeyword.setTextColor(getResources().getColor(R.color.grey04box));
                }
            }
        });

        edit = Globals.getValue("edit");

        if(!edit.equals("")){
            btnCreateResource.setText("Save file");
            setTitle("Edit File");

            myResourceID = Globals.getValue("resourceID");

            myResourceGroupID = Globals.getValue("myResourceGroupID");
            resourceTitle_text = Globals.getValue("resourceTitle");
            resourceLocation_text = Globals.getValue("resourceLocation");
            resourceDescription_text = Globals.getValue("resourceDescription");
            resourceKeyword_text = Globals.getValue("resourceKeySearch");
            resourceType_text = Globals.getValue("resourceType");
            image_string = Globals.getValue("imageString");
            imageid = Globals.getValue("imageid");
            fileid = Globals.getValue("fileid");

            try{
                listImagesFullStringArray = Globals.getValueArray("imageStringArray");
            }
            catch (Exception e) {
                e.printStackTrace();
            }


            if(listImagesFullStringArray.size() > 0){
                loadImages();
            }
        }else {
            btnCreateResource.setText("Create file");
            setTitle("Create File");
        }

        // if checkbox camera checked from settings = show button scan
        if (blCameraScanner)
        {
            imageViewLocation.setVisibility(View.VISIBLE);
        } else {
            imageViewLocation.setVisibility(View.GONE);
        }

        imageViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                permission_camera();

                if(boolean_permission_camera){

                    Intent intent = new Intent(FileCreateActivity.this, BarcodeCaptureActivity.class);
                    startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);

                }else{
                    Toast.makeText(getApplicationContext(), "Please enable the camera", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnSearchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image*//*");
                startActivityForResult(intent, REQUEST_BROWSE_PHOTO);*/

                try{
                    Intent intent = new Intent();
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    //intent.setType("file/");
                    intent.setType("*/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*", "application/pdf",
                            "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                            "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                            "application/odt", "text/plain"});
                    startActivityForResult(intent, REQUEST_BROWSE_PHOTO);
                    //showFileChooser();

                    String[] mimeTypes =
                            {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                    "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                    "text/plain",
                                    "application/pdf",
                                    "application/zip"};
                }
                catch(ActivityNotFoundException exp){
                    Toast.makeText(getBaseContext(), "No File (Manager / Explorer)etc Found In Your Device", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCreateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED
                        &&
                        ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_DENIED  ){
                    ActivityCompat.requestPermissions(FileCreateActivity.this, new String[] {android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
                }else{
                    permission_camera = true;
                }

                if (ContextCompat.checkSelfPermission(FileCreateActivity.this,
                        android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    if ((ActivityCompat.shouldShowRequestPermissionRationale(FileCreateActivity.this,
                            android.Manifest.permission.CAMERA))) {


                    } else {
                        ActivityCompat.requestPermissions(FileCreateActivity.this,
                                new String[]{android.Manifest.permission.CAMERA
                                }, PERMISSION_TAKE_PHOTO);
                    }
                } else {
                    permission_camera = true;
                }

                verifyStoragePermissions(FileCreateActivity.this);

                if(permission_camera && permission_storage){
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            Log.d("IMAGE onCreate", ex.toString());
                        }

                        if (photoFile != null) {
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
                        }
                        //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                else {
                    Toast.makeText(FileCreateActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
                }
            }
        });


        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edit.equals("")){
                    // edit mode
                    if (!image_string.equals("")) {
                        // image from database
                        zoomImageFromThumb(imageView1, image_string);
                    }else {
                        if(myBitmap != null){
                            // image was changed from phone storage
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                            Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                            bitmapByteArray = stream.toByteArray();
                            zoomImageFromThumb(imageView1, Base64.encodeToString(bitmapByteArray, Base64.NO_WRAP));
                        }else {
                            Toast.makeText(FileCreateActivity.this, "No image", Toast.LENGTH_SHORT).show();
                        }
                    }

                }else {
                    // create mode
                    if(myBitmap != null){
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                        bitmapByteArray = stream.toByteArray();
                        zoomImageFromThumb(imageView1, Base64.encodeToString(bitmapByteArray, Base64.NO_WRAP));
                    }else {
                        Toast.makeText(FileCreateActivity.this, "No image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnCreateResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if(editTextTitle.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextTitle.setError("Title is required!");
                }

                /*if(editTextLocation.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextLocation.setError("Location is required!");
                }*/

                /*String mIndex = spinnerResourceKind.getSelectedItem().toString();
                if(mIndex.contains(getString(R.string.select_resource_kind)))
                {
                    failFlag = true;
                    SetError("Kind resource is required!");
                }*/

                String mIndex = spinnerResourceType.getSelectedItem().toString();
                if(mIndex.contains(getString(R.string.select_resource_type)))
                {
                    failFlag = true;
                    SetError("Type file is required!");
                }

                if(failFlag == false){
                    resourceTitle_text = editTextTitle.getText().toString();
                    resourceLocation_text = editTextLocation.getText().toString();
                    resourceKeyword_text = editTextKeyword.getText().toString();
                    resourceDescription_text = editTextDescription.getText().toString();

                    if(!edit.equals("")){
                        // Edit Resource
                        alertDialogEdit();
                    }else{
                        // Create resource
                        alertDialogCreate();
                    }

                }
            }
        });

        editTextTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(edit.equals("")){
                    editTextDescription.setText(s);
                    editTextKeyword.setText(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        new LoadList(companyId).execute();

    }

    public void loadImages(){
        LinearLayout linearLayout = findViewById(R.id.layoutPicture1); //LinearLayOut Setup
        linearLayout.removeAllViews();

        for(int i=0; i<listImagesFullStringArray.size(); i++){
            String image_array = listImagesFullStringArray.get(i);

            byte[] decodedString;
            if(!image_array.equals("")){
                String resizeString =  resizeBase64Image(image_array);
                decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
            }else{
                decodedString = Base64.decode(image_array, Base64.NO_WRAP);
            }

            myBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            if(myBitmap != null){
                ImageView imageView = new ImageView(this); //ImageView Setup

                int photoW = myBitmap.getWidth();
                int photoH = myBitmap.getHeight();
                int scaleFactor = Math.min(photoW/100, photoH/100);
                int NewW = photoW/scaleFactor;
                int NewH = photoH/scaleFactor;

                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));

                mArrayBitmap.add(myBitmap);

                imageView.setLayoutParams(new LinearLayout.LayoutParams(300, 300)); //setting image position

                linearLayout.addView(imageView); //adding view to layout
            }
        }
    }

    /**
     * Background Async Task to Load spinner resource group data by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        public String company_id;

        public LoadList(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... strings) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            //resourceKindList = new ArrayList<HashMap<String, String>>();
            resourceTypeList = new ArrayList<HashMap<String, String>>();

            /*List<cResourceGroup> resokList = MyProvider.getResourceGroup(company_id);
            for(cResourceGroup entry : resokList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                resourceKindList.add(map);
            }
*/

            List<cResourceGroup> resotList = MyProvider.getResourceGroup(company_id, "");
            for(cResourceGroup entry : resotList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                resourceTypeList.add(map);
            }

            return "";
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (FileCreateActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                public void run() {

                    //List<String> kindArray =  new ArrayList<String>();
                    List<String> typeArray =  new ArrayList<String>();

                    /*if(resourceKindList.size()>0){
                        kindArray.add(0, getString(R.string.select_resource_kind));
                        resourcekindkeyArray.add(0, "");
                    }else{
                        kindArray.add(0, getString(R.string.select_no_resource_kind));
                        resourcekindkeyArray.add(0, "");
                    }*/

                    if(resourceTypeList.size()>0){
                        //typeArray.add(0, getString(R.string.select_resource_type));
                        //resourcetypekeyArray.add(0, "");
                    }else{
                        typeArray.add(0, getString(R.string.select_no_resource_type));
                        resourcetypekeyArray.add(0, "");
                    }


                    /*for (HashMap<String, String> map : resourceKindList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") resourcekindkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") kindArray.add(entry.getValue());
                        }*/

                    for (HashMap<String, String> map : resourceTypeList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") resourcetypekeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") typeArray.add(entry.getValue());
                        }

                    /*final ArrayAdapter<String> kind_adapter = new ArrayAdapter<String>(FileCreateActivity.this, android.R.layout.simple_spinner_item, kindArray);
                    kind_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sResourceKind = (Spinner) findViewById(R.id.spinnerResourceKind);
                    sResourceKind.setAdapter(kind_adapter);
                    */

                    final ArrayAdapter<String> type_adapter = new ArrayAdapter<String>(FileCreateActivity.this, android.R.layout.simple_spinner_item, typeArray);
                    type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sResourceType = (Spinner) findViewById(R.id.spinnerResourceType);
                    sResourceType.setAdapter(type_adapter);

                    if(!edit.equals("")){
                        editTextTitle.setText(resourceTitle_text);
                        //editTextBarcode.setEnabled(false);
                        editTextLocation.setText(resourceLocation_text);
                        //editTextLocation.setEnabled(false);
                        editTextDescription.setText(resourceDescription_text);
                        editTextKeyword.setText(resourceKeyword_text);

                        /*int indexResource = resourcekindkeyArray.indexOf(myResourceKindID);
                        sResourceKind.setSelection(indexResource);*/
                        int indexResource1 = resourcetypekeyArray.indexOf(myResourceGroupID);
                        sResourceType.setSelection(indexResource1);

                        if(listImagesFullStringArray.size() > 0){
                            // if multiple images


                        }else{
                            // if single image

                            if (!image_string.equals("")) {
                                Log.d("image", image_string);
                                Bitmap d = ConvertByteArrayToBitmap(image_string);
                                if (d != null) {
                                    int nh = (int) (d.getHeight() * (512.0 / d.getWidth()));
                                    Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                                    imageView1.setVisibility(View.VISIBLE);
                                    imageView1.setImageBitmap(scaled);
                                }
                            }
                        }

                    }
                }
            });
        }
    }


    private class CreateResource extends AsyncTask<String, String, Boolean>{

        public String company_id;
        public boolean edit_multiple = false;

        public CreateResource(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // extract data
            /*Spinner resourceKindSpinner = (Spinner) findViewById(R.id.spinnerResourceKind);
            int myIndex = resourceKindSpinner.getSelectedItemPosition();
            if(myIndex != -1){
                myResourceKindID = resourcekindkeyArray.get(myIndex);
            }*/

            Spinner resourceTypeSpinner = (Spinner) findViewById(R.id.spinnerResourceType);
            int myIndex = resourceTypeSpinner.getSelectedItemPosition();
            if(myIndex != -1){
                myResourceGroupID = resourcetypekeyArray.get(myIndex);
            }


            int BitmapID = 0;
            int ResourceID = 0;
            String DocumentID = "";
            boolean suc = false;

            ArrayList<Integer> mArrayIds = new ArrayList<Integer>();

            // multi selection
            if(mArrayBitmap.size() > 1){

                // create new resource with multiple images
                if(edit.equals("")){
                    edit_multiple = false;
                    for(Bitmap myBit : mArrayBitmap){
                        if (myBit != null)
                        {
                            // image was selected
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            myBit.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                            Bitmap.createScaledBitmap(myBit, 1, 1, false);
                            bitmapByteArray = stream.toByteArray();

                            byte[] bitmapByteArrayThumbnail;
                            bitmapByteArrayThumbnail = bitmapByteArray;
                            myBit.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                            Bitmap.createScaledBitmap(myBit, 1, 1, false);
                            bitmapByteArrayThumbnail = stream.toByteArray();


                            BitmapID = MyProvider.createImage("multiple_images", bitmapByteArray, bitmapByteArrayThumbnail, company_id);

                            if(BitmapID != 0){
                                mArrayIds.add(BitmapID);
                                suc = true;
                            }
                        }
                    }

                    // if multiple images are created successfully -> create resource
                    if(!edit.equals("")){
                        // Edit Resource

                        if(BitmapID > 0){
                            ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                    resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, String.valueOf(BitmapID), "0", "multiple_images", company_id);
                        }else{
                            Toast.makeText(FileCreateActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        // Create Resource

                        if(BitmapID > 0){
                            ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, String.valueOf(BitmapID), "0", "multiple_images", company_id);
                            //suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, String.valueOf(BitmapID), "0", "image", company_id);
                        }else {
                            Toast.makeText(FileCreateActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    // then save multiple ids images with id resource in intr_image_resource
                    if(ResourceID > 0){
                        if(mArrayIds.size() > 0){
                            for(int imageid : mArrayIds){
                                suc = MyProvider.addMultipleImageToResource(String.valueOf(imageid), String.valueOf(ResourceID), company_id);
                            }
                        }
                    }
                }else{
                    // in edit mode
                    // the implementation in web service is not done
                    edit_multiple = true;
                }

            }else{
            // single selection

                if (myBitmap != null)
                {
                    // image was selected
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                    bitmapByteArray = stream.toByteArray();

                    byte[] bitmapByteArrayThumbnail;
                    bitmapByteArrayThumbnail = bitmapByteArray;
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                    Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                    bitmapByteArrayThumbnail = stream.toByteArray();


                    BitmapID = MyProvider.createImage("image_resource", bitmapByteArray, bitmapByteArrayThumbnail, company_id);
                    suc = true;


                    if(!edit.equals("")){
                        // Edit Resource

                        if(BitmapID > 0){
                            ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                    resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, String.valueOf(BitmapID), "0", "image_resource", company_id);
                        }else{
                            Toast.makeText(FileCreateActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        // Create Resource

                        if(BitmapID > 0){
                            ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, String.valueOf(BitmapID), "0", "image_resource", company_id);
                            //suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, String.valueOf(BitmapID), "0", "image", company_id);
                        }else {
                            Toast.makeText(FileCreateActivity.this, "Couldn't save image, file not created!", Toast.LENGTH_SHORT).show();
                        }

                    }

                }else if(!extension.equals("")){

                    // pdf or doc was selected
                    // FilePath
                    // extension

                    // image null

                    //Path pdfFilePath = Paths.get(FilePath1);

                    // Read file to byte array
                /*try {
                    //bitmapByteArray = Files.readAllBytes(pdfFilePath);
                    bitmapByteArray = loadFile(FilePath1);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                    //ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    //myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    //Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                    //bitmapByteArray = stream.toByteArray();

                    //myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                    //Bitmap.createScaledBitmap(myBitmap, 1, 1, false);

                /*try {
                    byte[] base64EncodedData = org.apache.commons.codec.binary.Base64.encodeBase64(loadFileAsBytesArray(FilePath), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                    //suc = MyProvider.uploadMultipart(FileCreateActivity.this, FilePath1);

                    String file_id = "0";
                    String image_id = "0";
                    //if(!extension.equals("")){
                    if(extension.equals("msword")) extension = "doc";
                    if(extension.equals("vnd.openxmlformats-officedocument.wordprocessingml.document")) extension = "docx";
                    if(extension.equals("vnd.ms-excel")) extension = "xls";
                    if(extension.equals("vnd.openxmlformats-officedocument.spreadsheetml.sheet")) extension = "xlsx";
                    if(extension.equals("vnd.oasis.opendocument.text")) extension = "odt";

                    String[] mimeTypes =
                            {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                    "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                    "text/plain",
                                    "application/pdf",
                                    "application/zip"};


                    DocumentID = MyProvider.createPdf(extension, bitmapByteArray, company_id);
                    String result[] = DocumentID.split(",");
                    file_id = result[0];
                    image_id = result[1];
                    //BitmapID = MyProvider.createPdf("pdf", FilePath1, company_id);
                    //}

                    //suc = true;

                    if (ContextCompat.checkSelfPermission(FileCreateActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        //BitmapID = MyProvider.createPdf("pdf", fileBody1, FilePath1, company_id);
                    } else {
                        // Request permission from the user
                        ActivityCompat.requestPermissions(FileCreateActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    }

                    if(!edit.equals("")){
                        // Edit Resource
                        ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, image_id, file_id, "document", company_id);
                    }else{
                        // Create Resource
                        ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, image_id, file_id, "document", company_id);
                        //suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, image_id, file_id, "document", company_id);
                    }
                }else{


                    if(!edit.equals("")){
                        // Edit Resource
                        if(resourceType_text.toString().equals("image_resource")){
                            ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                    resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, "0", "0", "image_resource", company_id);
                        }else if(resourceType_text.toString().equals("document")){
                            ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                    resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, "0", "0", "document", company_id);
                        }else{
                            // none - no image - no pdf
                            ResourceID = MyProvider.editResourceDetail(myResourceID, resourceTitle_text, myResourceGroupID,
                                    resourceLocation_text, resourceDescription_text, resourceKeyword_text, myUserID, "0", "0", "none", company_id);
                        }
                    }else{
                        // Create Resource -> none - no image - no pdf
                        ResourceID = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, "0", "0", "none", company_id);
                        //suc = MyProvider.createResourceDetail(resourceTitle_text, resourceLocation_text, resourceDescription_text, resourceKeyword_text, myResourceGroupID, myUserID, "0", "0", "none", company_id);
                    }
                }

            }

            if(ResourceID > 0) suc = true; else suc = false;

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if(edit_multiple){
                        Toast.makeText(FileCreateActivity.this, "Sorry, the functionality is not done.", Toast.LENGTH_SHORT).show();
                    }

                    if (success == false)
                    {
                        if(!edit.equals("")){
                            Toast.makeText(FileCreateActivity.this, "Was not saved the file", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(FileCreateActivity.this, "Was not created the file", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        if(!edit.equals("")){
                            Globals.setValue("edit", "");
                            Globals.setValue("file_update","file_update");
                            Globals.setValue("typeid",myResourceGroupID);
                            Globals.setValue("resourceid",myResourceID);
                            Toast.makeText(FileCreateActivity.this, "The file was saved!", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(FileCreateActivity.this, "The file was created!", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }

                }
            });
        }
    }





    /**
     * This method loads a file from file system and returns the byte array of the content.
     *
     * @param fileName
     * @return
     * @throws Exception
     */
    public static byte[] loadFileAsBytesArray(String fileName) throws Exception {

        File file = new File(fileName);
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        return bytes;

    }


    private void alertDialogCreate(){
        new AlertDialog.Builder(FileCreateActivity.this)
                .setTitle("File")
                .setMessage("Do you want to create this file?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateResource(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void alertDialogEdit(){
        new AlertDialog.Builder(FileCreateActivity.this)
                .setTitle("File")
                .setMessage("Do you want to edit this file?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateResource(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }



    public static byte[] readFully(InputStream stream) throws IOException
    {
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int bytesRead;
        while ((bytesRead = stream.read(buffer)) != -1)
        {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }

    public static byte[] loadFile(String sourcePath) throws IOException
    {
        InputStream inputStream = null;
        try
        {
            inputStream = new FileInputStream(sourcePath);
            return readFully(inputStream);
        }
        finally
        {
            if (inputStream != null)
            {
                inputStream.close();
            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String UPCScanned = barcode.displayValue.toString();
                    editTextLocation = (EditText) findViewById(R.id.editText_location);
                    editTextLocation.setText(UPCScanned);

                    String bc = editTextLocation.getText().toString();
                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    //new LoadPackages(bc, pharmacyId).execute();
                } else
                    Toast.makeText(FileCreateActivity.this, "No barcode captured, intent data is null", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(FileCreateActivity.this, "Error: " + CommonStatusCodes.getStatusCodeString(resultCode), Toast.LENGTH_SHORT).show();
        }

        // IMAGE
        InputStream stream = null;

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            if(checkPermissionCAMERA(FileCreateActivity.this)){
                myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
                if(myBitmap != null) AttachBitmapToReport();
            }else{
                Toast.makeText(FileCreateActivity.this, "Cannot access camera", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == REQUEST_BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
        //if (requestCode == REQUEST_BROWSE_PHOTO && resultCode == RESULT_OK && data != null && data.getData() != null)
        {

            try
            {
                imageView1.setVisibility(View.GONE);
                tvInvisibleFileName.setVisibility(View.GONE);

                //FilePath2 = getPath(data.getData());

                if (checkPermissionREAD_EXTERNAL_STORAGE(FileCreateActivity.this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());

                        Uri selectedFileURI = data.getData();
                        File file = new File(selectedFileURI.getPath().toString());
                        String uploadedFileName = file.getName().toString();
                        //StringTokenizer tokens = new StringTokenizer(uploadedFileName, ".");
                        //String first = tokens.nextToken();
                        //String file_1 = tokens.nextToken().trim();
                        file1 = new File(Environment.getExternalStorageDirectory(), uploadedFileName);
                        fileBody1 = new FileBody(file1);

                        Uri filePath = FileProvider.getUriForFile(FileCreateActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider", file1);

                        //FilePath1 = FilePathClass.getPath(FileCreateActivity.this, filePath);

                        InputStream iStream = getContentResolver().openInputStream(selectedFileURI);
                        bitmapByteArray = getBytes(iStream);

                    }else{
                        // android 7 and below

                        // multiple selection
                        if(data.getClipData() != null){

                            boolean check_image = false;

                            LinearLayout linearLayout = findViewById(R.id.layoutPicture1); //LinearLayOut Setup
                            linearLayout.removeAllViews();

                            ClipData mClipData = data.getClipData();
                            //ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            mArrayBitmap = new ArrayList<Bitmap>();

                            for(int i = 0; i < mClipData.getItemCount(); i++){
                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();

                                String mimeType = getContentResolver().getType(uri);
                                int dot = mimeType.lastIndexOf("/");
                                if (dot >= 0) {
                                    extension = mimeType.substring(dot + 1);
                                } else {
                                    // No extension.
                                    extension = "";
                                }

                                FilePath1 = FilePathClass.getPath(FileCreateActivity.this, uri);
                                bitmapByteArray = loadFile(FilePath1);
                                //mArrayUri.add(uri);
                                try {

                                    String[] okFileExtensions =  new String[] {"jpg", "png", "gif","jpeg"};
                                    for (String ex : okFileExtensions){
                                        if(extension.toLowerCase().endsWith(ex)){
                                            myBitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), uri);
                                            if(myBitmap != null) mArrayBitmap.add(myBitmap);
                                            check_image = true;

                                            ImageView imageView = new ImageView(this); //ImageView Setup

                                            int photoW = myBitmap.getWidth();
                                            int photoH = myBitmap.getHeight();
                                            int scaleFactor = Math.min(photoW/100, photoH/100);
                                            int NewW = photoW/scaleFactor;
                                            int NewH = photoH/scaleFactor;

                                            imageView.setVisibility(View.VISIBLE);
                                            imageView.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));

                                            imageView.setLayoutParams(new LinearLayout.LayoutParams(300, 300)); //setting image position

                                            linearLayout.addView(imageView); //adding view to layout
                                        }
                                    }

                                    if(!check_image){

                                        // pdf or doc
                                        //stream = getContentResolver().openInputStream(data.getData());
                                        //myBitmap = BitmapFactory.decodeStream(stream);

                                        //tvInvisibleFileName.setText(FileName + " " + extension);
                                        //tvInvisibleFileName.setVisibility(View.VISIBLE);
                                    }



                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            if(!check_image){
                                // pdf or doc
                                Toast.makeText(FileCreateActivity.this, "Multiple selection is available just for images!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            // single selection

                            // refresh the view - layoutPicture1
                            LinearLayout linearLayout = findViewById(R.id.layoutPicture1);
                            linearLayout.removeAllViews();

                            Uri filePath = data.getData();
                            FilePath1 = FilePathClass.getPath(FileCreateActivity.this, filePath);
                            bitmapByteArray = loadFile(FilePath1);

                            FilePath = data.getData().getPath();
                            FileName = data.getData().getLastPathSegment();
                            int lastPos = FilePath.length() - FileName.length();

                            extension = "";

                            Uri returnUri = data.getData();
                            String mimeType = getContentResolver().getType(returnUri);

                            int dot = mimeType.lastIndexOf("/");
                            if (dot >= 0) {
                                extension = mimeType.substring(dot + 1);
                            } else {
                                // No extension.
                                extension = "";
                            }

                            String[] okFileExtensions =  new String[] {"jpg", "png", "gif","jpeg"};
                            for (String ex : okFileExtensions){
                                if(extension.toLowerCase().endsWith(ex)){
                                //if(FilePath.contains("images")){
                                    stream = getContentResolver().openInputStream(data.getData());
                                    myBitmap = BitmapFactory.decodeStream(stream);
                                    if(myBitmap != null) AttachBitmapToReport();
                                }else{

                                    // pdf or doc
                                    stream = getContentResolver().openInputStream(data.getData());
                                    myBitmap = BitmapFactory.decodeStream(stream);

                                    tvInvisibleFileName.setText(FileName + " " + extension);
                                    tvInvisibleFileName.setVisibility(View.VISIBLE);

                                    //String uploadId = UUID.randomUUID().toString();

                                }
                            }
                        }


                    }

                }else{
                    Toast.makeText(FileCreateActivity.this, "Cannot read storage", Toast.LENGTH_SHORT).show();
                }





                /*FilePath = data.getData().getPath();
                FileName = data.getData().getLastPathSegment();
                int lastPos = FilePath.length() - FileName.length();

                extension = "";

                Uri returnUri = data.getData();
                String mimeType = getContentResolver().getType(returnUri);

                int dot = mimeType.lastIndexOf("/");
                if (dot >= 0) {
                    extension = mimeType.substring(dot + 1);
                } else {
                    // No extension.
                    extension = "";
                }

                String[] okFileExtensions =  new String[] {"jpg", "png", "gif","jpeg"};
                for (String ex : okFileExtensions){
                    if(extension.toLowerCase().endsWith(ex)){
                    //if(FilePath.contains("images")){
                        stream = getContentResolver().openInputStream(data.getData());
                        myBitmap = BitmapFactory.decodeStream(stream);
                        if(myBitmap != null) AttachBitmapToReport();
                    }else{
                        // pdf or doc
                        stream = getContentResolver().openInputStream(data.getData());
                        myBitmap = BitmapFactory.decodeStream(stream);

                        tvInvisibleFileName.setText(FileName + " " + extension);
                        tvInvisibleFileName.setVisibility(View.VISIBLE);

                        //String uploadId = UUID.randomUUID().toString();

                    }
                }*/



                    /*//if(FileName.toLowerCase().endsWith(extension)){
                    if(FilePath.contains("images")){
                        stream = getContentResolver().openInputStream(data.getData());
                        myBitmap = BitmapFactory.decodeStream(stream);
                        AttachBitmapToReport();
                    }else{
                        tvInvisibleFileName.setText(FileName);
                        tvInvisibleFileName.setVisibility(View.VISIBLE);
                    }*/

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    ////////////////////////////////////////////////////////////////////////

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////


    public void showDialogReadStorage(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context, new String[] { permission }, STORAGE_PERMISSION_CODE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    public void showDialogWriteStorage(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context, new String[] { permission }, STORAGE_PERMISSION_CODE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    public void showDialogCamera(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context, new String[] { permission }, REQUEST_PERMISSIONS_CAMERA);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    // check permission external storage
    public boolean checkPermissionREAD_EXTERNAL_STORAGE(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialogReadStorage("External read storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, STORAGE_PERMISSION_CODE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    // check permission external storage
    public boolean checkPermissionWRITE_EXTERNAL_STORAGE(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showDialogWriteStorage("External write storage", context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, STORAGE_PERMISSION_CODE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    // check permission external storage
    public boolean checkPermissionCAMERA(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.CAMERA)) {
                    showDialogCamera("Camera", context, Manifest.permission.CAMERA);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.CAMERA }, REQUEST_PERMISSIONS_CAMERA);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }


    // camera //
    private void permission_camera() {
        if (ContextCompat.checkSelfPermission(FileCreateActivity.this,
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(FileCreateActivity.this,
                    android.Manifest.permission.CAMERA))) {


            } else {
                ActivityCompat.requestPermissions(FileCreateActivity.this,
                        new String[]{android.Manifest.permission.CAMERA
                        }, BARCODE_READER_REQUEST_CODE);
            }
        } else {
            boolean_permission_camera = true;
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }else{
            permission_storage = true;
        }
    }


    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission_camera = true;
                    //showSettingsAlert();

                } else {
                    Toast.makeText(getApplicationContext(), "Please allow the camera permission", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }*/

    //Requesting permission
    private void requestReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    //Requesting permission
    private void requestWriteStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_APN_SETTINGS}, STORAGE_PERMISSION_CODE);
    }

    //Requesting permission
    private void requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSIONS_CAMERA);
    }


    //This method will be called when the user will tap on allow or deny
    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        //if (requestCode == STORAGE_PERMISSION_CODE) {}

        switch (requestCode) {
            case REQUEST_PERMISSIONS_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission_camera = true;
                    //showSettingsAlert();
                    Toast.makeText(this, "Permission granted to camera", Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(getApplicationContext(), "Please allow the camera permission", Toast.LENGTH_LONG).show();
                }
                break;
            case STORAGE_PERMISSION_CODE:
                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Displaying a toast
                    Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
                } else {
                    //Displaying another toast if permission is not granted
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    // Methods - set visible the preview image
    private void AttachBitmapToReport()
    {
        int photoW = myBitmap.getWidth();
        int photoH = myBitmap.getHeight();
        int scaleFactor = Math.min(photoW/100, photoH/100);
        int NewW = photoW/scaleFactor;
        int NewH = photoH/scaleFactor;

        imageView1.setVisibility(View.VISIBLE);
        imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
    }

    // IMAGE
    private File createImageFile() throws IOException {

        if(permission_camera ){
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            mCurrentPhotoPathAbsolute = image.getAbsolutePath();
            return image;
        }else {
            return null;
        }
    }

    ////////////////////////////////////////////////////////////////
    // on Zoom image

    public String resizeBase64Image(String base64image){
        int IMG_WIDTH = 800;
        int IMG_HEIGHT = 600;
        byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


        if(image.getHeight() <= 400 && image.getWidth() <= 400){
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100, baos);

        byte [] b=baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    /**
     * When a <code>errorMessage</code> is specified, pops up an error window with the message
     * text, and creates an error icon in the secondary unit spinner. Error cleared through passing
     * in a null string.
     * @param errorMessage Error message to display, or null to clear.
     */
    public void SetError(String errorMessage)
    {
        View view = spinnerResourceType.getSelectedView();

        // Set TextView in Secondary Unit spinner to be in error so that red (!) icon
        // appears, and then shake control if in error
        TextView tvListItem = (TextView)view;

        // Set fake TextView to be in error so that the error message appears
        TextView tvInvisibleError = (TextView)findViewById(R.id.tvInvisibleError);

        // Shake and set error if in error state, otherwise clear error
        if(errorMessage != null)
        {
            tvListItem.setError(errorMessage);
            tvListItem.requestFocus();

            // Shake the spinner to highlight that current selection
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            spinnerResourceType.startAnimation(shake);

            tvInvisibleError.requestFocus();
            tvInvisibleError.setError(errorMessage);
        }
        else
        {
            tvListItem.setError(null);
            tvInvisibleError.setError(null);
        }
    }


    private void zoomImageFromThumb(final View thumbView, String imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);

        byte[] decodedString;
        if(imageResId != ""){
            String resizeString =  resizeBase64Image(imageResId);
            decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
        }else{
            decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
        }


        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null)
        {
            //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
            //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
            expandedImageView.setImageBitmap(decodedByte);
        }


        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        /*AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();*/

        AnimatorSet set = new AnimatorSet();
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // is tablet
            if(startScale <= 0.4){
                // image portrait

                set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                startBounds.top, finalBounds.top))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                //}else if(startScale > 0.4){
            }else if(startScale > 0.4){
                // image landscape = 0.4411277

                set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                                startBounds.top, finalBounds.top))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mCurrentAnimator = null;
                    }
                });
                set.start();
            }

        } else {
            // is phone
            set.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                    startBounds.left, finalBounds.left))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                            startBounds.top, finalBounds.top))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                            startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                    View.SCALE_Y, startScale, 2f));
            set.setDuration(mShortAnimationDuration);
            set.setInterpolator(new DecelerateInterpolator());
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mCurrentAnimator = null;
                }
            });
            set.start();
        }

        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        Globals.setValue("edit", "");
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(FileCreateActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }



}
