package com.sharethatdata.digifilefinder.photoview;

import android.widget.ImageView;

/**
 * Created by tudur on 24-May-19.
 */

public interface OnOutsidePhotoTapListener {
    /**
     * The outside of the photo has been tapped
     */
    void onOutsidePhotoTap(ImageView imageView);
}
