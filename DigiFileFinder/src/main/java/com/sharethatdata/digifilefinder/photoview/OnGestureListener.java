package com.sharethatdata.digifilefinder.photoview;

/**
 * Created by tudur on 24-May-19.
 */

public interface OnGestureListener {
    void onDrag(float dx, float dy);

    void onFling(float startX, float startY, float velocityX,
                 float velocityY);

    void onScale(float scaleFactor, float focusX, float focusY);
}
