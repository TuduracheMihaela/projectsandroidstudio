package com.sharethatdata.digifilefinder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sharethatdata.digifilefinder.adapter.SearchAdapter;
import com.sharethatdata.sharedactivity.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by gotic_000 on 4/27/2018.
 */

public class FileSearchActivity extends AppCompatActivity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;
    Utils Utils = null;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> resourceList;
    ArrayList<HashMap<String, String>> groupList;
    List<String> groupkeyArray =  new ArrayList<String>();

    SearchAdapter adapter;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";
    String groupSelected = "";

    ListView listView = null;
    TextView textViewNoReservation;
    Spinner spinnerGroup;

    boolean flag_loading = false;
    boolean dont_scroll_first_time = false;
    int page_nr = 2;
    boolean one_time = true;
    boolean can_i_search = false;

    private int preLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activity);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(FileSearchActivity.this, FileSearchActivity.this);

        Utils = new Utils();
        Utils.setOrientation(FileSearchActivity.this, FileSearchActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        final EditText editText = (EditText) findViewById(R.id.editText_search);
        listView = (ListView) findViewById(R.id.listView);
        textViewNoReservation = (TextView) findViewById(R.id.textViewNoResources);
        spinnerGroup = (Spinner) findViewById(R.id.spinnerResourceType);

        /*editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                    String bc = EditText.getText().toString();

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    adapter.getFilter().filter(bc);

                    return true;
                }
                return false;
            }
        });*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String resourceid = ((TextView) itemClicked.findViewById(R.id.resourceid)).getText().toString();

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), FileDetailActivity.class);
                intent.putExtra("resourceid", resourceid);
                startActivity(intent);
            }
        });


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                /*if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(dont_scroll_first_time){
                        if(flag_loading)
                        {
                            can_i_search = false;

                            flag_loading = false;
                            new LoadResources(groupSelected, companyId, "", page_nr++).execute();
                        }
                    }
                    dont_scroll_first_time = true;
                }*/

                // Make your calculation stuff here. You have all your
                // needed info from the parameters of this function.

                // Sample calculation to determine if the last
                // item is fully visible.
                final int lastItem = firstVisibleItem + visibleItemCount;

                if(lastItem == totalItemCount)
                {
                    if(preLast!=lastItem)
                    {
                        if(editText.getText().toString().equals("")){
                            flag_loading = true;
                        }
                        //to avoid multiple calls for last item
                        if(flag_loading)
                        {
                            can_i_search = false;

                            flag_loading = false;
                            new LoadResources(groupSelected, companyId, "", page_nr++).execute();
                        }
                        preLast = lastItem;
                    }
                }
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //adapter.getFilter().filter(s.toString());
                if(s.toString().length() >= 3){
                    flag_loading = false; // don't let scrolling
                    can_i_search = true;
                    new LoadResources(groupSelected, companyId, s.toString(), 0).execute();
                }else if(s.toString().length() == 0){
                    can_i_search = true;
                    new LoadResources(groupSelected, companyId, "", 0).execute();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        new LoadResources(groupSelected, companyId, "", 0).execute();
        new LoadList(companyId).execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        String updateResource = Globals.getValue("file_update");
        if(updateResource != ""){
            new LoadResources(groupSelected, companyId, "", 0).execute();
        }
    }



    class LoadResources extends AsyncTask<String, String, String> {

        public String group_id;
        public String company_id;
        public String keyword_search;
        public int pagenr;

        public LoadResources(String group_id, String company_id, String keyword_search, int pagenr){
            this.group_id = group_id;
            this.company_id = company_id;
            this.keyword_search = keyword_search;
            this.pagenr = pagenr;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            if(pagenr == 0) resourceList = new ArrayList<HashMap<String, String>>();

            List<cResource> myResource = MyProvider.getResources(group_id, company_id, keyword_search, pagenr);
            for(cResource entry : myResource)
            {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("resourceid",  entry.id);
                map.put("resource_name",  entry.name);
                map.put("resource_group_name",  entry.group_name);
                map.put("location",  entry.location);
                map.put("code",  entry.code);
                map.put("status",  entry.status);
                map.put("user",  entry.user);
                map.put("image",  entry.image_thumb);

                resourceList.add(map);
            }

            return String.valueOf(myResource.size());
        }

        protected void onPostExecute(final String return_value) {
            if (FileSearchActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(resourceList.size() > 0){
                        if(Integer.valueOf(return_value) > 0){

                            String updateResource = Globals.getValue("file_update");

                        /*adapter = new SimpleAdapter(FileSearchActivity.this, resourceList,
                                R.layout.activity_search_files_items_listview,
                                new String[]{"resourceid", "resource_name", "code", "status", "resource_group_name"},
                                new int[]{R.id.resourceid, R.id.name, R.id.code, R.id.status, R.id.group_name});
                        listView.setAdapter(adapter);*/



                            if(one_time){
                                // load screen with listview once
                                adapter = null;
                                adapter = new SearchAdapter(FileSearchActivity.this, R.layout.activity_search_files_items_listview, resourceList);
                                listView.setVisibility(View.VISIBLE);
                                listView.setAdapter(adapter);

                                textViewNoReservation.setVisibility(View.GONE);

                                one_time = false;
                                flag_loading = true; // to let you keep scrolling
                            }else{
                                if(can_i_search || !group_id.equals(getString(R.string.select_group))){
                                    // when search, load again the listview
                                    adapter = null;
                                    adapter = new SearchAdapter(FileSearchActivity.this, R.layout.activity_search_files_items_listview, resourceList);
                                    listView.setVisibility(View.VISIBLE);
                                    listView.setAdapter(adapter);

                                    textViewNoReservation.setVisibility(View.GONE);

                                    flag_loading = false; // to let you keep scrolling
                                    can_i_search = false;
                                }else if (resourceList.size() > 39){
                                    // don't load again the listview,
                                    // in this way, when load more items on scroll
                                    // the scroll is keeping the position
                                    adapter.notifyDataSetChanged();// notify adapter

                                    listView.setVisibility(View.VISIBLE);

                                    flag_loading = true; // to let you keep scrolling
                                }else if(updateResource != ""){
                                    Globals.setValue("file_update", "");

                                    adapter = null;
                                    adapter = new SearchAdapter(FileSearchActivity.this, R.layout.activity_search_files_items_listview, resourceList);
                                    listView.setVisibility(View.VISIBLE);
                                    listView.setAdapter(adapter);

                                    textViewNoReservation.setVisibility(View.GONE);
                                }

                            }
                        }

                    }else{
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }
                }
            });
        }
    }


    /**
     * Background Async Task to Load spinner resource group data by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        public String company_id;

        public LoadList(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... strings) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            groupList = new ArrayList<HashMap<String, String>>();

            List<cResourceGroup> resotList = MyProvider.getResourceGroup(company_id, "");
            for(cResourceGroup entry : resotList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                groupList.add(map);
            }

            return "";
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (FileSearchActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> groupArray =  new ArrayList<String>();

                    if(groupList.size()>0){
                        groupArray.add(0, getString(R.string.select_group));
                        groupkeyArray.add(0, "");
                    }else{
                        groupArray.add(0, getString(R.string.select_no_group));
                        groupkeyArray.add(0, "");
                    }

                    for (HashMap<String, String> map : groupList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") groupkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") groupArray.add(entry.getValue());
                        }

                    final ArrayAdapter<String> type_adapter = new ArrayAdapter<String>(FileSearchActivity.this, android.R.layout.simple_spinner_item, groupArray);
                    type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //final Spinner sResourceType = (Spinner) findViewById(R.id.spinnerResourceType);
                    //sResourceType.setAdapter(type_adapter);
                    spinnerGroup.setAdapter(type_adapter);
                }
            });


            spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String mIndex = spinnerGroup.getSelectedItem().toString();

                    if(mIndex.contains(getString(R.string.select_group))){
                        // do nothing

                    }else{

                        int myIndex = spinnerGroup.getSelectedItemPosition();
                        String userid = groupkeyArray.get(myIndex);
                        groupSelected = userid;

                        new LoadResources(groupSelected, companyId, "", 0).execute();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(FileSearchActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
