package com.sharethatdata.digifilefinder.help;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by tudur on 14-Aug-19.
 */

public class AppWebViewClients extends WebViewClient {



    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);

    }
}
