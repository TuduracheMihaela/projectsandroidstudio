package com.sharethatdata.crash.report;


import com.sharethatdata.webservice.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CustomCrashReportDialog extends Activity implements OnClickListener{
	
	 	String VersionName;
	    String PackageName;
	    String FilePath;
	    String PhoneModel;
	    String AndroidVersion;
	    String Board;
	    String Brand;
	    String Device;
	    String Display;
	    String FingerPrint;
	    String Host;
	    String ID;
	    String Manufacturer;
	    String Model;
	    String Product;
	    String Tags;
	    long Time;
	    String Type;
	    String User;
	    
	    Context mContext;
	    String crashLog="TEXT";
	    Button buttonSubmitReport;
	    EditText editTextReport;
	    
	
	    WSDataProvider MyProvider = null;
	    
	    String user = "";
	    String pass = "";
	    String webservice_url = "";
	    String companyid = "";

	    String error_message = "";
	    
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        setContentView(R.layout.custom_dialog);
	        

   	    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
	        user = prefUser.getString("USER", ""); 
	        pass = prefUser.getString("PASS", ""); 
	        webservice_url = prefUser.getString("URL", "");
	        companyid = prefUser.getString("COMPANYID", "");


	        mContext = this;

	        if (getIntent().getStringExtra("CRASH_LOG") != null) {
	            crashLog = getIntent().getStringExtra("CRASH_LOG");
	        }
	        // showDialog(text);
	        crashLog += CreateInformationString();

	        ((TextView) findViewById(R.id.textReport)).setText(crashLog);
	        buttonSubmitReport = (Button) findViewById(R.id.buttonSumbmitReport);
	        buttonSubmitReport.setOnClickListener(this);
	        editTextReport = (EditText) findViewById(R.id.editTextReport);
	        
	        Toast.makeText(CustomCrashReportDialog.this, getString(R.string.crash_dialog_text), Toast.LENGTH_LONG).show();

	    }

	    @Override
	    public void onClick(View v) {
	        int id = v.getId();
			if (id == R.id.buttonSumbmitReport) {
				//sendEmailErrorReport(crashLog);
				
				String description = "--------- Report from user ---------\n" +
		        		editTextReport.getText().toString() 
		        		+ "\n------------------------------------------------------------------------\n"
		        + crashLog;
				
				new RegisterTask(description).execute();
			}
	    }

	    public void sendEmailErrorReport(String bodyContent) {

	        final Intent emailIntent = new Intent(
	                Intent.ACTION_SEND);

	        // open with email supported apps
	        emailIntent.setType("message/rfc822");

	        // support mail from string.xml
	        emailIntent.putExtra(Intent.EXTRA_EMAIL,
	                new String[] { getString(R.string.crash_user_email_label) });

	        // AppName + Subject text from string.xml
	        emailIntent.putExtra(Intent.EXTRA_SUBJECT,
	                getString(R.string.app_name) + " Crash Report ");

	        // Inital body text from string.xml + crash log 
	        emailIntent.putExtra(Intent.EXTRA_TEXT,
	        		"--------- Report from user ---------\n" +
	        		editTextReport.getText().toString() 
	        		+ "\n------------------------------------------------------------------------\n"
	        + bodyContent);

	        finish();
	        
	        startActivity(Intent.createChooser(emailIntent, "Send Report"));

	        // restart app
	        // restartApp();
	    	
	    	
	    	/*String customText = getString(R.string.app_name) + "\n" + "Custom report from user: " + "\n" +
	    	editTextReport.getText().toString() + "\n" + bodyContent;
	    	
	    	new RegisterTask(customText).execute();*/
	    	
	        
	        
	    }

	    public void restartApp() {
	        Intent i = getBaseContext().getPackageManager()
	                .getLaunchIntentForPackage(getBaseContext().getPackageName());
	        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        
	        finish();
	        startActivity(i);
	    }
	    

	    void RecoltInformations(Context context) {
	        try {
	            PackageManager pm = context.getPackageManager();
	            PackageInfo pi;
	            // Version
	            pi = pm.getPackageInfo(context.getPackageName(), 0);
	            VersionName = pi.versionName;
	            // Package name
	            PackageName = pi.packageName;
	            // Device model
	            PhoneModel = android.os.Build.MODEL;
	            // Android version
	            AndroidVersion = android.os.Build.VERSION.RELEASE;

	            Board = android.os.Build.BOARD;
	            Brand = android.os.Build.BRAND;
	            Device = android.os.Build.DEVICE;
	            Display = android.os.Build.DISPLAY;
	            FingerPrint = android.os.Build.FINGERPRINT;
	            Host = android.os.Build.HOST;
	            ID = android.os.Build.ID;
	            Model = android.os.Build.MODEL;
	            Product = android.os.Build.PRODUCT;
	            Tags = android.os.Build.TAGS;
	            Time = android.os.Build.TIME;
	            Type = android.os.Build.TYPE;
	            User = android.os.Build.USER;

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    public String CreateInformationString() {
	        RecoltInformations(mContext);

	        String ReturnVal = "";
	        ReturnVal += "Version : " + VersionName;
	        ReturnVal += "\n";
	        ReturnVal += "Package : " + PackageName;
	        ReturnVal += "\n";
	        ReturnVal += "FilePath : " + FilePath;
	        ReturnVal += "\n";
	        ReturnVal += "Phone Model" + PhoneModel;
	        ReturnVal += "\n";
	        ReturnVal += "Android Version : " + AndroidVersion;
	        ReturnVal += "\n";
	        ReturnVal += "Board : " + Board;
	        ReturnVal += "\n";
	        ReturnVal += "Brand : " + Brand;
	        ReturnVal += "\n";
	        ReturnVal += "Device : " + Device;
	        ReturnVal += "\n";
	        ReturnVal += "Display : " + Display;
	        ReturnVal += "\n";
	        ReturnVal += "Finger Print : " + FingerPrint;
	        ReturnVal += "\n";
	        ReturnVal += "Host : " + Host;
	        ReturnVal += "\n";
	        ReturnVal += "ID : " + ID;
	        ReturnVal += "\n";
	        ReturnVal += "Model : " + Model;
	        ReturnVal += "\n";
	        ReturnVal += "Product : " + Product;
	        ReturnVal += "\n";
	        ReturnVal += "Tags : " + Tags;
	        ReturnVal += "\n";
	        ReturnVal += "Time : " + Time;
	        ReturnVal += "\n";
	        ReturnVal += "Type : " + Type;
	        ReturnVal += "\n";
	        ReturnVal += "User : " + User;
	        ReturnVal += "\n";
	        // ReturnVal += "Total Internal memory : " +
	        // getTotalInternalMemorySize();
	        // ReturnVal += "\n";
	        // ReturnVal += "Available Internal memory : "
	        // + getAvailableInternalMemorySize();
	        ReturnVal += "\n";

	        return ReturnVal;
	    }
	    
	    
		  /** AsyncTask register time record  */
	    private class RegisterTask extends AsyncTask<String, String, Boolean>{
	    	
	    	String description;
	    	
	    	public RegisterTask(String description){
	    		this.description = description;
	    	}
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	        	
	        	  // extract data
	        	 System.out.println(" MyProvider: " + webservice_url + " " + user + " " + pass + " " + companyid);
				  
				  // call web method - Data Provider
	        	 	MyProvider = new WSDataProvider(getApplication());
	        	 	MyProvider = new WSDataProvider(user , pass);
			        MyProvider.SetWebServiceUrl(webservice_url);
			        
				  //MyProvider = new WSDataProvider(getApplication());		       
				  
				  boolean suc = false;

					  suc = MyProvider.create_report("", "", user, getString(R.string.crash_user_email_label), getString(R.string.app_name), description, "0", companyid);
			  
	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
		    	// updating UI from Background Thread
		        runOnUiThread(new Runnable() {
		        	
		            public void run() {
		            
		            	if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		  
		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 //myUIHelper.ShowDialog(CustomCrashReportDialog.this, getString(R.string.text_error), error_message);
		            		 
		            		 System.out.println(" CustomCrashReportDialog error_message: " + error_message);
		            		 finish();
			    		}else{
							 System.out.println(" CustomCrashReportDialog: " + error_message);
							 Toast.makeText(CustomCrashReportDialog.this, getString(R.string.crash_dialog_submit_toast), Toast.LENGTH_LONG).show();
							 finish();
			    		}       	   	
		              }
		        });    
		    }
	    }

}


