package com.sharethatdata.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cEmployee;
import com.sharethatdata.webservice.datamodel.cPrintJob;
import com.sharethatdata.webservice.datamodel.cRoomAvailable;
import com.sharethatdata.webservice.datamodel.cVisitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Log;

//
// DataProvider - class with database/JSON interface
//
public class DataProvider {

	private final Context context;
	private final SharedPreferences sharedPrefs;
	    
	JSONParser jsonParser = new JSONParser();
	
    // url to webservice
    private String website_url = "http://www.sharethatdata.com/digivisitor";
    private String labelservice_url = "http://192.168.1.88:8001";
    
    private String url_get_contacts = "get_contacts.php";
    private String url_get_visit_entries = "get_visit_entries.php";
    private String url_get_visitors = "get_visitor.php";
    
    private String url_login = "login.php";
    private String url_log = "log_event.php";
    private String url_get_company = "get_company.php";
    private String url_get_user = "get_user.php";
    
    private String url_get_printjobs = "GetPrintJobs";
    private String url_create_printjob = "AddPrintJob";
    
    private String url_create_visit_entry = "create_visitor_entry.php";
    private String url_update_visit_entry = "update_visitor_entry.php";
    private String url_create_end_visit_entry = "create_visitor_end_entry.php";
    private String url_latest_visit = "get_latest_visit.php";
 	
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ITEMS = "items";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_FUNC = "subtitle";
    private static final String TAG_STATUS = "status";
    
    // products JSONArray
    JSONArray items = null;
   
    public boolean json_connected = false;
    public boolean printer_connected = false;
    public boolean executing_print_job = false;
    
    String myUser = "";
    
	public DataProvider(Context context, String new_user)
	{
		myUser = new_user;
		
		this.context = context;
	    sharedPrefs = context.getSharedPreferences("defaults", 0);
				
		String strLabelService = sharedPrefs.getString("prefLabelServer", "");
		String strLabelPort = sharedPrefs.getString("prefLabelPort", "");
		if (!strLabelService.equals("") && !strLabelPort.equals(""))
		{
			// set new adres
			labelservice_url = "http://" + strLabelService + ":" + strLabelPort;
		
		}
	}

	public boolean LogEvent(String event) 
	{
		 
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
	    
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
      
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_log, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            // free mem
            json = null;
            
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            
            // free mem
            json = null;
            
            return false;
        }
	}
	
	public boolean CheckLogin(String login, String password)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", password));

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_login, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	return true;
            } else {
            	return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    
    }
	  
	
	// list of data elements, generic function
	public List<String> getDataList(String list_name, String user)
	{
		String list_url = "";
		// select url
		if (list_name == "contacts") list_url = url_get_contacts;
		if (list_name == "visit_entries") list_url = url_get_visit_entries;
		
		// result list
		List<String> returnList = new ArrayList<String>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("token", user));
                
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + list_url, "GET", params);
 	   
        if (json != null)
        {
            // Check your log cat for JSON reponse
            Log.d("All Items: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    items = json.getJSONArray(TAG_ITEMS);
 
                    // looping through All Products
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
 
                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
                        
                        // adding HashList to ArrayList
                        returnList.add(name);
                    }
                    
                    json_connected = true;
                    
                } else {
                    // no products found
                	json_connected = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        	
            // adding HashList to ArrayList
            returnList.add("");
        }
        
        // free mem
        json = null;
       // jsonParser = null;
	     
        return returnList;
	}
	
	public cVisitor getLatestVisit(String myOrganisation, String mySurname, String myLastName)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("organisation", myOrganisation));
        params.add(new BasicNameValuePair("lastname", myLastName));
        params.add(new BasicNameValuePair("lastname", myLastName));
                
        cVisitor myVisit = new cVisitor();
                
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_latest_visit, "GET", params);
 	   
        if (json != null)
        {
        	   try {
                   // Checking for SUCCESS TAG
                   int success = json.getInt(TAG_SUCCESS);
    
                   if (success == 1) {
                       // products found
                       // Getting Array of Products
                		items = json.getJSONArray("item");
		             	
                    	JSONObject c = items.getJSONObject(0);
                   
                         // create object 
                         cVisitor myVisitor = new cVisitor();
                           
                         // Storing each json item in variable
                         myVisitor.id = c.getInt(TAG_PID);
                         myVisitor.created = c.getString("created");
                         myVisitor.organisation = c.getString("organisation");
                         myVisitor.surname = c.getString("surname");
                         myVisitor.lastname = c.getString("lastname");
                         myVisitor.function = c.getString("function");
                         myVisitor.phone = c.getString("phone");
                         myVisitor.email = c.getString("email");
                         myVisitor.visit_kind = c.getString("visit_kind");
                         myVisitor.employee = c.getString("employee");
                      
                         myVisit = myVisitor;
                                             
                         json_connected = true;
                       
                   } else {
                       // no items found
                   	json_connected = true;
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
                   json_connected = false;
               }
           } else {
           	 json_connected = false;
           }
           
           // free mem
           json = null;
           
           return myVisit;
	}
	
	
	// get visitors
	// list of data elements, generic function
	public List<cVisitor> getVisitorList(String user, String organisation, String lastname)
	{
		String list_url = url_get_visitors;
		
		// result list
		List<cVisitor> returnList = new ArrayList<cVisitor>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("token", user));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("lastname", lastname));
        
                
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + list_url, "GET", params);
 	   
        if (json != null)
        {
            // Check your log cat for JSON reponse
            Log.d("All Items: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
 
              if (success == 1) {
                    // products found
                    // Getting Array of Products
                    items = json.getJSONArray(TAG_ITEMS);
 
                    // looping through All Products
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
 
                        // create object 
                        cVisitor myVisitor = new cVisitor();
                        
                        // Storing each json item in variable
                        myVisitor.id = c.getInt(TAG_PID);
                        myVisitor.created = c.getString("created");
                        myVisitor.organisation = c.getString("organisation");
                        myVisitor.surname = c.getString("surname");
                        myVisitor.lastname = c.getString("lastname");
                        myVisitor.function = c.getString("function");
                        myVisitor.phone = c.getString("phone");
                        myVisitor.email = c.getString("email");
                        myVisitor.visit_kind = c.getString("visit_kind");
                        myVisitor.employee = c.getString("employee");
                                                
                        // adding HashList to ArrayList
                        returnList.add(myVisitor);
                    }
                    
                    json_connected = true;
                    
                } else {
                    // no items found
                	json_connected = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                json_connected = false;
            }
        } else {
        	 json_connected = false;
        }
        
        // free mem
        json = null;
       // jsonParser = null;
	     
        return returnList;
	}
	
	// company info
	public cCompany getCompanyInfo(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("id", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_company, "GET", params);

        // check for success tag
        try {
          	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cCompany myCompany = new cCompany();
            	myCompany.id = c.getLong("id");
            	myCompany.name = c.getString("name");
            	myCompany.address = c.getString("address");
            	myCompany.postcode = c.getString("postcode");
            	myCompany.city = c.getString("city");
            	myCompany.address_mail = c.getString("address_mail");
            	myCompany.postcode_mail = c.getString("postcode_mail");
            	myCompany.city_mail = c.getString("city_mail");
            	myCompany.country = c.getString("country");
            	myCompany.phone = c.getString("phone");
            	myCompany.fax = c.getString("fax");
            	myCompany.website = c.getString("website");
            	myCompany.email = c.getString("email");
            	myCompany.picture = c.getString("picture");
            	
            	json_connected = true;
            	
            	 // free mem
                json = null;
            	
            	return myCompany;
            } else {
            	json_connected = false;
            	  
            	 // free mem
                json = null;
                
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            
            // free mem
            json = null;
            
            return null;
        }
	}
	
	// user info
	public cContact getUserInfo(String organisation, String name)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        String surname = "";
        String lastname = "";
        String[] wname = name.split(" ");
    	surname = wname[0];
        if (wname.length > 2)
        {
        	for(int i=1;i< wname.length ;i++)
        	{
        		lastname = lastname + wname[i] + " ";
        	}
        } else {
        	lastname = wname[1];
        }
        
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("surname", surname));
        params.add(new BasicNameValuePair("lastname", lastname));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_user, "GET", params);

        // check for success tag
        try {
          	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cContact myUser = new cContact();
            	myUser.id = c.getString("id");
            	myUser.username = c.getString("username");
            	myUser.surname = c.getString("surname");
            	myUser.lastname = c.getString("lastname");
            	myUser.login = c.getString("login");
            	myUser.auth_level = c.getInt("auth_level");
            	myUser.function = c.getString("function");
            	myUser.phone = c.getString("phone");
            	myUser.email = c.getString("email");
            	myUser.picture = c.getString("picture");
            	myUser.status_id = c.getString("status");
            	myUser.description = c.getString("description");
            	
            	json_connected = true;
            	
            	 // free mem
                json = null;
            	
            	return myUser;
            } else {
            	json_connected = false;
            	  
            	 // free mem
                json = null;
                
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            
            // free mem
            json = null;
            
            return null;
        }
	}
		
	
	// create visit entry item
	public boolean createVisitEntry(String datetime, String organisation, String surname, String lastname, String phone, String email, String function, String visit_kind, String employee)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("datetime", datetime));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("surname", surname));
        params.add(new BasicNameValuePair("lastname", lastname));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("function", function));
        params.add(new BasicNameValuePair("visit_kind", visit_kind));
        params.add(new BasicNameValuePair("employee", employee));
      
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_visit_entry, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            json_connected = true;
            // free mem
            json = null;
            
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            
            json_connected = false;
            // free mem
            json = null;
            
            return false;
        }
		
	}
	
	public boolean updateVisitEntry(String visit_id, String organisation, String surname, String lastname, String phone, String email, String function, String visit_kind, String employee)
	{
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
	    
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("datetime", datetime));
        params.add(new BasicNameValuePair("visit_id", visit_id));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("surname", surname));
        params.add(new BasicNameValuePair("lastname", lastname));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("function", function));
        params.add(new BasicNameValuePair("visit_kind", visit_kind));
        params.add(new BasicNameValuePair("employee", employee));
        
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_update_visit_entry, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            json_connected = true;
            // free mem
            json = null;
            
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            
            json_connected = false;
            // free mem
            json = null;
            
            return false;
        }
		
	}
	
	public boolean createEndVisitEntry(String organisation, String surname, String lastname)
	{
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
	    
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("surname", surname));
        params.add(new BasicNameValuePair("lastname", lastname));
        
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_end_visit_entry, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            json_connected = true;
            // free mem
            json = null;
            
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            
            json_connected = false;
            // free mem
            json = null;
            
            return false;
        }
		
	}
	
	
	
	public boolean SendNotification(String phoneNo, String sms)
	{
		  try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNo, null, sms, null, null);
			return true;
		  } catch (Exception e) {
			e.printStackTrace();
			return false;
		  }
	}
	
	// DYMO proxy server interface methods
	
	public boolean CreatPrintJob(String user, String organisation, String visitor, String visitor_organisation)
    {
		
		executing_print_job = true;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("visitor", visitor));
        params.add(new BasicNameValuePair("visitor_organisation", visitor_organisation));
        

        // getting JSON Object
        // Note that create product url accepts POST method
    //    JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_create_printjob, "GET", params);
        JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_create_printjob, "GET", params);

        printer_connected = false;
        
        if (json != null)
        {
		    // check for success tag
		    try {
		        int success = json.getInt(TAG_SUCCESS);
		
		        if (success == 1)
		        {
		        	printer_connected = true;
		        } 
		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
        }
        
        // free mem
        json = null;
        
        executing_print_job = false;
        
        return printer_connected;
    }
	
	// list of printjobs
	public List<cPrintJob> getPrintJobs()
	{
			List<cPrintJob> myPrintJobs = null;
			cPrintJob myPrintJob = null;
			
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        
	 //     params.add(new BasicNameValuePair("user", ""));
	        
	        // getting JSON string from URL
	        JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_get_printjobs, "GET", params);
	 	   
	        if (json != null)
	        {
	           
	            try {
	            	JSONObject c = new JSONObject(json.toString());
	 
	            	 myPrintJob = new cPrintJob();
	         		
	            	 myPrintJob.id = c.getInt("Id");
	            	 myPrintJob.name = c.getString("Name");
	            	 myPrintJob.created = c.getString("datetime");
	            	 myPrintJob.organisation = c.getString("organisation");
	            	 myPrintJob.visitor = c.getString("visitor");
	            	 myPrintJob.visitor_organisation = c.getString("visitor_organisation");
	            	 
	            	 myPrintJobs.add(myPrintJob);
	            	 
	            } catch (JSONException e) {
	                e.printStackTrace();
	            }
	        } else {
	        
	        }
	        
	        // free mem
	        json = null;
	         
	        return myPrintJobs;
	}
	
	
	
	// get rooms available
			// list of data elements, generic function
			public List<cRoomAvailable> getRoomAvailableList(String name)
			{
				//String list_url = "http://www.sharethatdata.com/digivisitor/get_free_resources.php?user=mverbeek&token=secret&startdate=2015-01-01%2010:00&enddate=2015-01-01%2011:00";
				
				// result list
				List<cRoomAvailable> returnList = new ArrayList<cRoomAvailable>();
				
				// Building Parameters
		        List<NameValuePair> params = new ArrayList<NameValuePair>();

		        params.add(new BasicNameValuePair("name", name));
		        
		                
		        // getting JSON string from URL
		        JSONObject json = jsonParser.makeHttpRequest("http://www.sharethatdata.com/digivisitor/get_free_resources.php?user=mverbeek&token=secret&startdate=2015-01-01%2010:00&enddate=2015-01-01%2011:00", "GET", params);
		 	   
		        if (json != null)
		        {
		            // Check your log cat for JSON reponse
		            Log.d("All Items: ", json.toString());
		 
		            try {
		                // Checking for SUCCESS TAG
		                int success = json.getInt(TAG_SUCCESS);
		 
		              if (success == 1) {
		                    // products found
		                    // Getting Array of Products
		                    items = json.getJSONArray(TAG_ITEMS);
		 
		                    // looping through All Products
		                    for (int i = 0; i < items.length(); i++) {
		                        JSONObject c = items.getJSONObject(i);
		 
		                        // create object 
		                        cRoomAvailable myRoom = new cRoomAvailable();
		                        
		                        // Storing each json item in variable
		                        myRoom.id = c.getInt(TAG_PID);
		                        myRoom.name = c.getString("name");
		                        myRoom.code = c.getString("code");
		                                                
		                        // adding HashList to ArrayList
		                        returnList.add(myRoom);
		                    }
		                    
		                    json_connected = true;
		                    
		                } else {
		                    // no items found
		                	json_connected = true;
		                }
		            } catch (JSONException e) {
		                e.printStackTrace();
		                json_connected = false;
		            }
		        } else {
		        	 json_connected = false;
		        }
		        
		        // free mem
		        json = null;
		       // jsonParser = null;
			     
		        return returnList;      
		      
			}
			
			
			// get employees available
			// list of data elements, generic function
			public List<cEmployee> getEmployeeAvailableList(String name)
			{
				//String list_url = "http://www.sharethatdata.com/digivisitor/get_free_resources.php?user=mverbeek&token=secret&startdate=2015-01-01%2010:00&enddate=2015-01-01%2011:00";
				
				// result list
				List<cEmployee> returnList = new ArrayList<cEmployee>();
				
				// Building Parameters
		        List<NameValuePair> params = new ArrayList<NameValuePair>();

		        params.add(new BasicNameValuePair("name", name));
		        
		                
		        // getting JSON string from URL
		        JSONObject json = jsonParser.makeHttpRequest("http://www.sharethatdata.com/digivisitor/get_contacts.php?user=mverbeek&token=secret", "GET", params);
		 	   
		        if (json != null)
		        {
		            // Check your log cat for JSON reponse
		            Log.d("All Items: ", json.toString());
		 
		            try {
		                // Checking for SUCCESS TAG
		                int success = json.getInt(TAG_SUCCESS);
		 
		              if (success == 1) {
		                    // products found
		                    // Getting Array of Products
		                    items = json.getJSONArray(TAG_ITEMS);
		 
		                    // looping through All Products
		                    for (int i = 0; i < items.length(); i++) {
		                        JSONObject c = items.getJSONObject(i);
		 
		                        // create object 
		                        cEmployee myEmployee = new cEmployee();
		                        
		                        // Storing each json item in variable
		                        myEmployee.id = c.getInt(TAG_PID);
		                        myEmployee.name = c.getString("name");
		                        myEmployee.subtitle = c.getString("subtitle");
		                        myEmployee.status = c.getString("status");
		                        myEmployee.phone = c.getString("phone");
		                        myEmployee.email = c.getString("email");
		                                                
		                        // adding HashList to ArrayList
		                        returnList.add(myEmployee);
		                    }
		                    
		                    json_connected = true;
		                    
		                } else {
		                    // no items found
		                	json_connected = true;
		                }
		            } catch (JSONException e) {
		                e.printStackTrace();
		                json_connected = false;
		            }
		        } else {
		        	 json_connected = false;
		        }
		        
		        // free mem
		        json = null;
		       // jsonParser = null;
			     
		        return returnList;      
		      
			}
			
			
			// add members to meeting room reservation
			public boolean addMembersToMeetingRoom(String dateStartSchedule, String timeStartSchedule, String roomSchedule, String coworker)
			{
				String url_create_room_appointment = "";
				String user = myUser;
				
		        // Building Parameters
		        List<NameValuePair> params = new ArrayList<NameValuePair>();
		        params.add(new BasicNameValuePair("user", user));
		        params.add(new BasicNameValuePair("dateStart", dateStartSchedule));
		        params.add(new BasicNameValuePair("timeStart", timeStartSchedule));
		        params.add(new BasicNameValuePair("room", roomSchedule));
		        params.add(new BasicNameValuePair("coworker", coworker));
		      
		        // getting JSON Object
		        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_room_appointment, "GET", params);

		        // check for success tag
		        try {
		            int success = json.getInt(TAG_SUCCESS);
		         
		            json_connected = true;
		            // free mem
		            json = null;
		            
		            return true;
		        } catch (JSONException e) {
		            e.printStackTrace();
		            
		            json_connected = false;
		            // free mem
		            json = null;
		            
		            return false;
		        }
				
			}
}


