package com.sharethatdata.webservice.datamodel;

public class cCompany {

	public long id;
    public String name;
    public String address;
    public String postcode;
    public String city;
    public String address_mail;
    public String postcode_mail;
    public String city_mail;
    public String country;
    public String phone;
    public String fax;
    public String website;
    public String email;
    public String picture;
	
    public cCompany() {

    	id = 0;
        name = "";
        address = "";
        postcode = "";
        city = "";
        address_mail = "";
        postcode_mail = "";
        city_mail = "";
        country = "";
        phone = "";
        fax = "";
        website = "";
        email = "";
        picture = "";
    }
    	
}
