package com.sharethatdata.webservice.datamodel;

public class cNewsStatusNotification {
	
	public String id;
	public String id_object;
	public String id_user;
	public String description;
	public String status;
	
	 public cNewsStatusNotification() {
	    	
	    	id = "";
	        id_object = "";
	    	id_user = "";
	    	description = "";
	    	status = "";
	        
	    }

}
