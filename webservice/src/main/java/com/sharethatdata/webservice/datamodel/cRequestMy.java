package com.sharethatdata.webservice.datamodel;

public class cRequestMy {
	
	public long id;
	public String description;
	public long fromuser;
	public String fromuser_name_surname;
	public long touser;
	public String touser_name_surname;
	public String datetime;
	public String status_name;
	
	public cRequestMy(){
		
		id = 0;
		description = "";
		fromuser = 0;
		fromuser_name_surname = "";
		touser = 0;
		touser_name_surname = "";
		datetime = "";
		status_name = "";
		
	}

}
