package com.sharethatdata.webservice.datamodel;

/**
 * Created by mverbeek on 7-2-2017.
 */

public class cOrderState {

    public String id;
    public String ordernr;
    public String datum;
    public String user;
    public String status;

    public cOrderState(){
        id = "";
        ordernr = "";
        datum = "";
        user = "";
        status = "0";
    }
}
