package com.sharethatdata.webservice.datamodel;

public class cUserLocation {

	public long id;
	public String code;
    public String name;
    public String type;
    public long stock;
    
    public cUserLocation() {
    	
    	id = 0; 
        name = "";
        code = "";
        type = "0";
        stock = 0;
        
    }
}
