package com.sharethatdata.webservice.datamodel;

public class cProject {
	public long id;
    public String name;
    public String code;
    public String created;
    public String deadline;
    public String owner;
    public String status;
    public String description;
    public String image_id;
    public String full_image;
    public String privacy;
    public String costplace;
    public int ETC;
    
    public cProject() {
    	
    	id = 0;
        name = "";
        code = "";
        created = "";
        deadline = "";
        owner = "";
        status = "";
        description = "";
        image_id = "";
        full_image = "";
        privacy = "";
        costplace = "";
        ETC = 0;
    }
}
