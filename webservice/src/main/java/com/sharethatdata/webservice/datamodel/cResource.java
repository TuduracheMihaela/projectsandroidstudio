package com.sharethatdata.webservice.datamodel;

public class cResource {
	public String id;
    public String name;
    public String code;
    public String barcode;
    public String location;
    public String stock;
    public String status;
    public String description;
    public long bookamount;
    public boolean InUse;
    public String User;
    public String groupID;
    public String groupName;
    public String image_id;
    public String return_date;
        
    public cResource()
    {
    	id= "0";
        name = "";
        code = "";
        barcode = "";
        location = "";
        bookamount = 1;
        stock = "0";
        status = "";
        description = "";
        InUse = false;
        User = "0";
        groupID = "0";
        groupName = "";
        image_id = "0";
        return_date = "";
    }
}
