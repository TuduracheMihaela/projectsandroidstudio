package com.sharethatdata.webservice.datamodel;

public class cPrintJob {
	
	public long id;
    public String created;
	public String name;
    public String organisation;
    public String visitor;
    public String visitor_organisation;
    
    public cPrintJob() {
    	
    	id = 0;
        created = "";
    	name = "";
        organisation = "";
        visitor = "";
        visitor_organisation = "";
    }
    
}
