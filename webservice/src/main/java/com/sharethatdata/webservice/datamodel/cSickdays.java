package com.sharethatdata.webservice.datamodel;

public class cSickdays {

	public String id;
    public String user;
    public String datetime_start;
    public String datetime_end;
    public int hours;
    public String description;
    public String approved;
    
    public cSickdays() {
    	
    	id = "0";
        user = "";
        datetime_start = "";
        datetime_end = "";
        hours = 0 ;
        description = "";
        approved = "";
    }
}
