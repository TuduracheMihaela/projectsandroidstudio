package com.sharethatdata.webservice.datamodel;

public class cNews {
	
	public long id;
    public String title;
    public String description;
    public String created;
    public String owner;
    public String picture;
    
    public cNews() {
    	
    	id = 0;
        title = "";
        description = "";
        created = "";
        owner = "";
        picture = "";
        
    }
}
