package com.sharethatdata.webservice.datamodel;

public class cVisitMember {
	
	public long id;
	public String visit;
	public String username;
    public String surname;
    public String lastname;
    
	public cVisitMember() {
		
		id = 0;
		visit = "";
	    username = "";
	    surname = "";
	    lastname = "";
	  
	}
}
