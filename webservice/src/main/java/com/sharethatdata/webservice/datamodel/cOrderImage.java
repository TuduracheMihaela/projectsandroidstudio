package com.sharethatdata.webservice.datamodel;

/**
 * Created by mverbeek on 9-2-2017.
 */



    public class cOrderImage {

        public String id;
        public String image;
        public String datetime;
        public String user;
        public String status;

        public cOrderImage(){
            id = "";
            image = "";
            datetime = "";
            user = "";
            status = "0";
        }
    }

