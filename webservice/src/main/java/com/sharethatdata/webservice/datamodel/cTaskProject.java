package com.sharethatdata.webservice.datamodel;

/**
 * Created by miha on 10/6/2017.
 */

public class cTaskProject {

    public String id;
    public String name;
    public String userid;
    public String user;
    public String projectid;
    public String project;
    public String date;
    public String deadline;
    public String importancy;
    public String imp_name;
    public String status;
    public String sta_name;
    public String urgency;
    public String urg_name;
    public String est_minutes;
    public String activity;
    public String activityid;
    public String description;
    public String notes;

    public cTaskProject(){
        id = "";
        name = "";
        userid = "";
        user = "";
        projectid = "";
        project = "";
        date = "";
        deadline = "";
        importancy = "";
        imp_name = "";
        status = "";
        sta_name = "";
        urgency = "";
        urg_name = "";
        est_minutes = "";
        activity = "";
        activityid = "";
        description = "";
        notes = "";
    }
}
