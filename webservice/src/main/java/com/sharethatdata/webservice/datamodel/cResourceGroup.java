package com.sharethatdata.webservice.datamodel;

public class cResourceGroup {
	public long id;
    public String name;
    public String description;
    public boolean active;
        
    public cResourceGroup()
    {
    	id= 0;
        name = "";
        description = "";
        active = true;
    }
}
