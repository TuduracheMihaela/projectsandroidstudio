package com.sharethatdata.webservice.datamodel;

public class cStatusNotification {
	
	public String id;
    public String id_object;
    public String id_user;
    public String description;
    public String datetime;
    public String status;
    public String type;
    
    public cStatusNotification() {
    	
    	id = "0";
    	id_object = "";
    	id_user = "";
    	description = "";
    	datetime = "" ;
    	status = "";
    	type = "";
    }

}
