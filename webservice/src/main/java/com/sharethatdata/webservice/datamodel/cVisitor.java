package com.sharethatdata.webservice.datamodel;

public class cVisitor {
	
	public long id;
	public String created;
    public String organisation;
    public String surname;
    public String lastname;
    public String function;
    public String phone;
    public String email;
    public String visit_kind;
	public String employee;

	public cVisitor() {
		
		id = 0;
		created = "";
	    organisation = "";
	    surname = "";
	    lastname = "";
	    function = "";
	    phone = "";
	    email = "";
	    visit_kind = "";
		employee = "";
		
	}
}
