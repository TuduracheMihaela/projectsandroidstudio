package com.sharethatdata.webservice.datamodel;

import java.util.ArrayList;
import java.util.List;

public class cVisitEntries {
	
	public String id;
	public String datetime; // $object_type == 'latest_visit'
	public String startdatetime;
	public String enddatetime;
	public String allday;
	public String organisation;
    public String surname;
    public String lastname;
    public String function;
    public String phone;
    public String email;
    public String visit_kind;
    public String sign_in_datetime;
	public String sign_out_datetime;
	public String employee;
	public String employee_name;
	public String productinfo;
	public List<String> coWorkers;
	
	public cVisitEntries() {
		
		id = "0";
		datetime = ""; // $object_type == 'latest_visit'
		startdatetime = "";
		enddatetime = "";
		allday = "";
	    organisation = "";
	    surname = "";
	    lastname = "";
	    function = "";
	    phone = "";
	    email = "";
	    visit_kind = "";
	    sign_in_datetime = "";
	    sign_out_datetime = "";
		employee = "0";
		employee_name = "";
		coWorkers = new ArrayList<String>();
		productinfo = "0";
	}
}
