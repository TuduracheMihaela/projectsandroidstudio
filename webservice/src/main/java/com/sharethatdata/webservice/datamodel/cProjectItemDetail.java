package com.sharethatdata.webservice.datamodel;

public class cProjectItemDetail {
	
	public String id;
	public String name;
	public String subtitle;
	public String status;
	public String owner;
	public String image_id;
	public String projectid;
	public String projectname;
	
	public cProjectItemDetail(){
		id = "";
		name = "";
		subtitle = "";
		status = "";
		owner = "";
		image_id = "";
		projectid = "";
		projectname = "";
		
	}

}
