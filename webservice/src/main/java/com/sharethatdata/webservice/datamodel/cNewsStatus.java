package com.sharethatdata.webservice.datamodel;

public class cNewsStatus {

	public String id;
	public String id_news;
	public String id_user;
	public String status;
	public String title;
	public String description;
	public String created;
	
	 public cNewsStatus() {
	    	
	    	id = "";
	    	id_news = "";
	    	id_user = "";
	    	status = "";
	    	title = "";
	    	description = "";
	    	created = "";
	        
	    }
}
