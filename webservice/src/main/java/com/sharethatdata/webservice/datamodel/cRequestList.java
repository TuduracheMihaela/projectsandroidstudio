package com.sharethatdata.webservice.datamodel;

public class cRequestList {

	public long id;
	public String description;
	public String fromuser;
	public String touser;
	public String status_name;
	public String datetime;
	public String image_id;
	
	public cRequestList(){
		id = 0;
		description = "";
		fromuser = "";
		touser = "";
		status_name = "";
		datetime = "";
		image_id = "";
	}
}
