package com.sharethatdata.webservice.datamodel;

public class cMutation {

	public String id;
	public String datetime;
    public String user;
    public String location;
    public String resource;
    public String resource_code;
    public String mutation_type;
    public long amount;
    public long stock_after;
        
    public cMutation() {
    	
    	id = "0"; 
        datetime = "";
        user = "";
        location = "";
        resource = "";
        resource_code = "";
        mutation_type = "";
        amount = 0;
        stock_after = 0;
    }
}