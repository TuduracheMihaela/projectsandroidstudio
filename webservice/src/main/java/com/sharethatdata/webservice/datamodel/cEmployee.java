package com.sharethatdata.webservice.datamodel;

public class cEmployee {
	public long id;
    public String name;
    public String subtitle;
    public String status;
    public String phone;
    public String email;

    public cEmployee() {
    	
    	id=0;
    	name = "";
        subtitle = "";
        status = "";
        phone = "";
        email = "";
    }
	
}
