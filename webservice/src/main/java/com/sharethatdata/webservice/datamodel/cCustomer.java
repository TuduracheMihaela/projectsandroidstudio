package com.sharethatdata.webservice.datamodel;

public class cCustomer {

	public long id;
    public String name;

    public String street;
    public String zipcode;
    public String city;
    public String country;
    public String phone;
    public String fax;
    public String email;
    public String contact;

    public cCustomer() {
    	
    	id = 0;
        name = "";

        street = "";
        zipcode = "";
        city = "";
        country = "";
        phone = "";
        fax = "";
        email = "";
        contact = "";
        
    }
}