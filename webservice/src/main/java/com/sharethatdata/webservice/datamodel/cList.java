package com.sharethatdata.webservice.datamodel;

/**
 * Created by tudur on 06-Dec-18.
 */

public class cList {

    public String id;
    public String name;
    public String description;

    public String itemID;
    public String amount;
    public String stock;

    public cList(){
        id = "";
        name = "";
        description = "";

        itemID = "";
        amount = "";
        stock = "";
    }
}
