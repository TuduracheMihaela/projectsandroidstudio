package com.sharethatdata.webservice.datamodel;

public class cLocation {

	public String id;
	public String code;
    public String name;
    public String barcode;
    public String type;
    public long stock;
    public long minstock;
    public long maxstock;
        
    public cLocation() {
    	
    	id = "0"; 
        name = "";
        code = "";
        barcode = "";
        type = "0";
        stock = 0;
        minstock = 0;
        maxstock = 0;
        
    }
}