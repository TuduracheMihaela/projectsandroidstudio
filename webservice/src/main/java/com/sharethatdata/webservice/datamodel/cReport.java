package com.sharethatdata.webservice.datamodel;

public class cReport {
	public String id;
    public String name;
    public String email;
    public String project;
    public String date;
    public String starttime;
    public String description;
    public String	image_id;
	
    public cReport() {
    	
    	id = "0";
    	name = "";
    	email = "";
        project = "";
        date = "";
        starttime = "";
        description = "";
        image_id = "0";
    }
}
