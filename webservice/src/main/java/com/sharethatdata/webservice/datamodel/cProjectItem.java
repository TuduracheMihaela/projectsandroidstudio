package com.sharethatdata.webservice.datamodel;

public class cProjectItem {
	public String id;
    public String title;
    public String entry;
    public String created;
    public String owner;
    
    public cProjectItem() {
    	
    	id = "";
        title = "";
        entry = "";
        created = "";
        owner = "";
    }
}
