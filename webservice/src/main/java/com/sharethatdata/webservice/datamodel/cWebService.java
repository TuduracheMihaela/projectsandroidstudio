package com.sharethatdata.webservice.datamodel;

public class cWebService {

	public String id;
    public String name;
    public String url;
    public String art_url;
    public String description;
    public String image_id;

    public String type;
    public String default_url;
    public String access_key;
	
    public cWebService() {
    	
    	id = "0";
    	name = "";
    	url = "";
        art_url = "";
        description = "";
        image_id = "0";

        type = "";
        default_url = "";
        access_key = "";
    }
	
}
