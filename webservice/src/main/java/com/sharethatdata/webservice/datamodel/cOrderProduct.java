package com.sharethatdata.webservice.datamodel;

public class cOrderProduct {
	
	public String id;
	public String productnr;
	public String product;
	public String amount;
	public String amount_done;
	public String location;
	public String price;
	public String processed;
	public String delivered;
	public String resID;
	public String groupID;
	
	public cOrderProduct(){
		id = "";
		productnr = "";
		product = "";
		amount = "";
		amount_done = "0";
		location = "";
		price = "";
		processed = "";
		delivered = "";
		resID = "";
		groupID = "";
	}

}
