package com.sharethatdata.webservice.datamodel;

/**
 * Created by miha on 10/12/2017.
 */

public class cModuleItems {

    public String id;
    public String name;
    public String description;

    public cModuleItems(){
        id = "";
        name = "";
        description = "";
    }
}
