package com.sharethatdata.webservice.datamodel;

public class cProjectProgress {
	public String id;
	public String created;
	public String name;
	public String progress;
	public String hours;
	public String comment;
	
	public cProjectProgress(){
		id = "0";
		created = "";
		name = "";
		progress = "";
		hours = "";
		comment = "";
		
	}
}
