package com.sharethatdata.webservice.datamodel;

public class cOrder {
	
	public String id;
	public String uname;
	public String ordernr;
	public String address;
	public String barcode;
	public String order_type;
	public String req_location;
	public String del_location;
	public String priority;
	public String datum;
	public String started;
	public String ended;
	public String bedrag;
	public String bezorgkosten;
	public String levering;
	public String betaalwijze;
	public String payed;
	public String processed;
	public String checked;
	public String status;
	public String error;
	public String procestime;
	public String waitingtime;
	public String num_products;
	public String num_delivered;
	public String new_stock;
	public String language;
			
	public cOrder(){
		id = "";
		uname = "";
		ordernr = "";
		address = "";
		barcode = "";
		order_type = "";
		req_location = "";
		del_location = "";
		priority = "";
		datum = "";
		started = "";
		ended = "";
		bedrag = "";
		bezorgkosten = "";
		levering = "";
		betaalwijze = "";
		payed = "";
		processed = "";
		checked = "";
		status = "";
		error = "";
		procestime = "";
		waitingtime = "";
		num_products = "0";
		num_delivered = "0";
		new_stock = "0";
		language = "NL";
	}

}
