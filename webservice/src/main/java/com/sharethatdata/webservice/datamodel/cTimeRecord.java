package com.sharethatdata.webservice.datamodel;

public class cTimeRecord {
	public String id;
    public String userid;
    public String user;
    public String customerid;
    public String customer;
    public String projectid;
    public String project;
    public String date;
    public String starttime;
    public String endtime;
    public int minutes;
    public String locationid;
    public String location;
    public String taskid;
    public String task;
    public String activityid;
    public String activity;
    public String costplaceid;
    public String costplace;
    public String description;
	
    public cTimeRecord() {
    	
    	id = "0";
        userid = "";
        user = "";
        customerid = "";
        customer = "";
        projectid = "";
        project = "";
        date = "";
        starttime = "";
        endtime = "";
        minutes = 0;
        locationid = "";
        location = "";
        taskid = "";
        task = "";
        activityid = "";
        activity = "";
        costplaceid = "";
        costplace = "";
        description = "";
    }
}
