package com.sharethatdata.webservice.datamodel;

public class cMessage {

	public long id;
    public String user_from;
    public String user_to;
    public String user_last_message;
    public String datetime;
    public String message;
    public String status_read;
    
    public cMessage() {

    	id = 0;
    	user_from = "";
    	user_to = "";
    	user_last_message = "";
        datetime = "";
        message = "";
        status_read = "";
    
    }
}
