package com.sharethatdata.webservice.datamodel;

public class cReservations {

	public String id;
	public String user;
    public String description;
    public String start_date;
    public String start_hour;
    public String end_date;
    public String end_hour;
    public String members;
    public String resource_name;
    public String active;
    public String reservation_type;
    public String reservation_status;
    public cReservations() {
    	
    	id = "";
    	user= "";
    	description = "";
    	start_date = "";
    	start_hour = "";
    	end_date = "";
    	end_hour = "";
        members = "";
        resource_name="";
        active = "1";
        reservation_type="";
        reservation_status="0";
    }
}