package com.sharethatdata.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cReport;
import com.sharethatdata.webservice.datamodel.cResource;

import android.util.Base64;
import android.util.Log;

import static com.sharethatdata.webservice.WSDataProvider.MD5_Hash;


//
//WSRoboDataProvider - class with database/JSON interface to robopharma reporter web service
//
public class WSRoboDataProvider {
	

	JSONParser jsonParser = new JSONParser();
	private String webservice_url = "http://dev-ws.sharethatdata.com";
	//private String webservice_url = "https://dev-ws.sharethatdata.com/snowcloud";
	
    private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    private String url_create_item = "create_item.php";
    private String url_update_item = "update_item.php";
    private String url_delete_item = "delete_item.php";
        
    private String url_login = "login.php";
    private String url_log = "log_event.php";
    
    private static final String TAG_SUCCESS = "ws_success";
    private static final String TAG_MESSAGE = "ws_error_message";
    
    private static final String TAG_ITEMS = "items";
	    
    private static final String SESSION = "xrobofs234";
  
    private static final String CONNECTION_ERROR = "Web service not found, please check WIFI/Network connection.";
    
    public String last_error = "";
       
    public boolean json_connected = false;
    public boolean printer_connected = false;
    public boolean executing_print_job = false;

	private String myUser = "";
	private String myPass = "";
	private String myApp = "";
    
    private String ip = "";
    private String android_id = "";
    private String appname = "";
    		
    
	public WSRoboDataProvider(String new_user, String new_pass)
	{
		myUser = new_user;
		myPass = new_pass;

		String currentSESSION = WSSession.getInstance().getString();
		if(!currentSESSION.equals("")){

			myPass = WSSession.getInstance().getPassword();
			myApp = WSSession.getInstance().getAppName();
			android_id = WSSession.getInstance().getDevice();
			ip = WSSession.getInstance().getIP();
			android_id = WSSession.getInstance().getDevice();
		}
	}
	
	public void SetWebServiceUrl(String new_url)
	{
		if (!new_url.equals("")) webservice_url = new_url;
	}
	
	public boolean LogEvent(String event) 
	{
		 
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
		
		// get values from device
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        // getting JSON Object
  //      JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_log, "GET", params);

        // check for success tag
 //       try {
 //           int success = json.getInt(TAG_SUCCESS);
         
            return true;
 //       } catch (JSONException e) {
  //          e.printStackTrace();
  //          return false;
 //       }
	}


	
	public boolean CheckLogin(String login, String password)
    {
		// clean variables
		last_error = "";
		json_connected = true;

		String currentSESSION = WSSession.getInstance().getString();
		if(currentSESSION.equals("")){
			password = MD5_Hash(password);
		}
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", password));

        myUser = login;
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_login, "GET", params);

     	
	   	 if (json != null) 
	   	 {    		 
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	String token = json.getString("ws_token");
	            	WSSession.getInstance().setString(token);
	            	
	            	return true;
	            } else {
	            	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
	            	return false;
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            last_error = CONNECTION_ERROR;
	   		 	json_connected = false;
	            return false;
	        }
	   	 } else {
	   		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
	   		 return false;
	   	 }
    
    }
	  
	
	// General Method for calling new web service
	// gets a list of json items in array
	// when something goes wrong, an empty array is returned
	private JSONArray CallWebService(List<NameValuePair> new_params, String url, boolean doPost)
	{
		// clean variables
		last_error = "";
		json_connected = true;
			
		String currentSESSION = WSSession.getInstance().getString();
		if(currentSESSION.equals("")){

			boolean bool = CheckLogin(myUser, myPass);
			if(bool){
				currentSESSION = WSSession.getInstance().getString();
			}
		}
		
		 // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        
        for (NameValuePair p : new_params) {
        	params.add(p);
        }
       
    	// getting JSON Object
    	// Note that create product url accepts POST method
        String Method = "GET";
        if (doPost == true) Method = "POST";
    	JSONObject json = jsonParser.makeHttpRequest(url, Method, params);
    
     	JSONArray items = new JSONArray(); 
     	
    	 if (json != null) 
    	 {    		 
			 try {
				 
				items = json.optJSONArray("items");
			    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
			
		      	int success = json.getInt(TAG_SUCCESS);
	
		        if (success == 1)
		        {
		         	if (items == null) items = new JSONArray(); 
		        } else {
		        	// record error
		        	items = new JSONArray(); 
		        	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
		        }
		     } catch (JSONException e) {
		         e.printStackTrace();
		        
		     }
    	            
    	 } else {
    		 
    		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
    	 
    	 }
    	 return items;
    
	}

	public List<cProject> getProjects()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
         params.add(new BasicNameValuePair("filtered", "1"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProject> returnList = new ArrayList<cProject>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProject myProject = new cProject();
                    
                    myProject.id = c.getLong("id");
                    myProject.name = c.getString("name");
                    myProject.code = c.getString("code");
                    myProject.status = c.getString("status");
                    myProject.created = c.getString("created");
                    myProject.status = c.getString("description");
    		    	
     		    	returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cResource> getResources()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resources"));
        params.add(new BasicNameValuePair("groupid", "0"));
        params.add(new BasicNameValuePair("filtered", "0"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                    
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.description = c.getString("description");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.InUse = c.getBoolean("InUse");
       		    	returnList.add(myResource);
                  }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	 public boolean createReport(String myDateTime,String myStartTime,String myName, String myEmail, String myProject,String description, String ImageID)
	 {
		// Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	 
	    params.add(new BasicNameValuePair("object", "report"));
	     
	    params.add(new BasicNameValuePair("date", myDateTime));
	    params.add(new BasicNameValuePair("starttime", myStartTime));
	    params.add(new BasicNameValuePair("name", myName));
	    params.add(new BasicNameValuePair("email", myEmail));
	    params.add(new BasicNameValuePair("project", myProject));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("image_id", ImageID)); // ? What is image ID parameter
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    
	    if (last_error != "")
	    {
	    	return false;
	    	
	    }else {
	    
	    	return true;
	    }
	 
	 }
	
	 public int createImage(byte[] myImage)
	 {
		// Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    String currentSESSION = WSSession.getInstance().getString();
	    
	    params.add(new BasicNameValuePair("object", "image"));
	     
	    params.add(new BasicNameValuePair("image_type", "image_report"));
	    //params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
 	   
        
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

	    int imageID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	imageID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
	 	
	 	return imageID;
	 }
	
	 public String getImage(String id)
	 {
		 String result = "";

	     List<NameValuePair> params = new ArrayList<NameValuePair>();
	     params.add(new BasicNameValuePair("object", "image"));
	     params.add(new BasicNameValuePair("imageid", id));
	     params.add(new BasicNameValuePair("fullimage", "true"));
	     
	     JSONArray items = null;
	        
	     items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	     try 
	     {
	        if (items.length() > 0)
	        {
	        	JSONObject c = items.getJSONObject(0);
	        	result = c.getString("full_image");
	        }
	     }
	     catch (JSONException e) 
	     {
	    	 e.printStackTrace();
	     }
	     
	     return result;
	 }
	 
	 public String getImageThumnail(String id)
	 {
		 String result = "";

	     List<NameValuePair> params = new ArrayList<NameValuePair>();
	     params.add(new BasicNameValuePair("object", "image"));
	     params.add(new BasicNameValuePair("imageid", id));

	     JSONArray items = null;
	        
	     items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	     try 
	     {
	        if (items.length() > 0)
	        {
	        	JSONObject c = items.getJSONObject(0);
	        	result = c.getString("image");
	        }
	     }
	     catch (JSONException e) 
	     {
	    	 e.printStackTrace();
	     }
	     
	     return result;
	 }

	// report detail
	public cReport getReport(String id, boolean latest)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if (latest == true) { 
        	params.add(new BasicNameValuePair("object", "latest_report"));
        
        } else {
        	params.add(new BasicNameValuePair("reportid", id));
            params.add(new BasicNameValuePair("object", "report"));
        }
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cReport myReport = new cReport();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		 
		    	myReport.id = c.getString("id");
		    	myReport.name = c.getString("name");
		    	myReport.email = c.getString("email");
		    	myReport.project = c.getString("project");
		    	myReport.date = c.getString("date");
		    	myReport.starttime = c.getString("starttime");
		    	myReport.description = c.getString("description");
		    	myReport.image_id = c.getString("image_id");
        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return myReport;
         
	}
	
	public List<cReport> getReports(String date_start, String date_end, String user, boolean addNew)
	{
		// result list
		List<cReport> returnList = new ArrayList<cReport>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "report"));
        
        Date d = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
        if (date_start != "") datetime = date_start;
                
        params.add(new BasicNameValuePair("startdate", datetime));
        if (date_end != "") datetime = date_end;
        params.add(new BasicNameValuePair("enddate", datetime));
        params.add(new BasicNameValuePair("report_user", user));
        params.add(new BasicNameValuePair("projectid", "0"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReport myReport = new cReport();
                                        
                	myReport.id = c.getString("id");
    		    	myReport.name = c.getString("name");
    		    	myReport.email = c.getString("email");
    		    	myReport.project = c.getString("project");
    		    	myReport.date = c.getString("date");
    		    	myReport.starttime = c.getString("starttime");
    		    	myReport.description = c.getString("description");
    		    //	myReport.image_id = c.getString("image_id");
                	
     		    	returnList.add(myReport);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}
	
	public boolean deleteReport(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "report"));
	    params.add(new BasicNameValuePair("reportid", id));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
        
        if (last_error != "")
        {
        	return false;
        }else {
        	return true;
        }
		
	}

}