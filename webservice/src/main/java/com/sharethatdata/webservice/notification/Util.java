package com.sharethatdata.webservice.notification;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;

/**
 * Created by tudur on 05-Sep-19.
 */

public class Util {

    // schedule the start of the service every 10 - 30 seconds
    public static void scheduleJobMyService(Context context) {

        ComponentName serviceComponent = new ComponentName(context, MyJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(4, serviceComponent);
        //builder.setMinimumLatency(1 * 1000); // wait at least
        //builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // needs network connection
        //builder.setPersisted(true); // scheduled even after device reboots
        //builder.setPeriodic(when);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }

    public static void scheduleJobScheduleAlarmService(Context context, Intent intent) {

        ComponentName serviceComponent = new ComponentName(context, ScheduleAlarmJobService.class);

        String description = intent.getStringExtra("descList");
        String id = intent.getStringExtra("idList");

        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("descList", description);
        bundle.putString("idList", id);

        JobInfo.Builder builder = new JobInfo.Builder(5, serviceComponent);
        //builder.setMinimumLatency(1 * 1000); // wait at least
        //builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // needs network connection
        builder.setExtras(bundle);
        //builder.setPersisted(true); // scheduled even after device reboots
        //builder.setPeriodic(when);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not

        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }

    public static void cancelJob(Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(4);
    }
}
