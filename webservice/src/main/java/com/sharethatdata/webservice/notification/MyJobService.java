package com.sharethatdata.webservice.notification;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.sharethatdata.webservice.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cNewsStatus;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectItemDetail;
import com.sharethatdata.webservice.datamodel.cStatusNotification;
import com.sharethatdata.webservice.datamodel.cUserLocation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

/**
 * Created by tudur on 05-Sep-19.
 */

@SuppressLint("NewApi")
public class MyJobService extends JobService {

    WSDataProvider MyProvider = null;

    NotificationManager notificationManager;
    NotificationCompat.InboxStyle inboxStyle ;
    NotificationManagerCompat notificationManagerCompat ;
    NotificationCompat.Builder builder ;

    String CHANNEL_ID_01 = "my_channel_011";// The id of the channel.
    String CHANNEL_ID_02 = "my_channel_012";// The id of the channel.
    String CHANNEL_ID_03 = "my_channel_013";// The id of the channel.
    String CHANNEL_ID_04 = "my_channel_014";// The id of the channel.
    String CHANNEL_ID_05 = "my_channel_015";// The id of the channel.
    String CHANNEL_ID_06 = "my_channel_016";// The id of the channel.
    String CHANNEL_ID_07 = "my_channel_017";// The id of the channel.
    String CHANNEL_ID_08 = "my_channel_018";// The id of the channel.
    String CHANNEL_ID_09 = "my_channel_019";// The id of the channel.

    private String user = "";
    private String pass = "";
    private String webservice_url = "";

    String statusReadMessage = "";
    int nrUnreadMessage = 0;

    String statusReadNews = "";
    int nrUnreadNews = 0;

    int nrNotifications = 0;

    boolean test = false;
    boolean running = false;

    public static final String inputFormat = "yyyy-MM-dd HH:mm";
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);


    // List Messages
    ArrayList<HashMap<String, String>> messageList;

    // List News
    ArrayList<HashMap<String, String>> newsList;

    // List notification normal(news, project, project_item, request, holiday, sickday, etc)
    ArrayList<HashMap<String, String>> notificationListNormal;

    ArrayList<HashMap<String, String>> notificationListDigiTime;
    ArrayList<HashMap<String, String>> notificationListDigiPortal;
    ArrayList<HashMap<String, String>> notificationListDigiSupport;

    //List notification schedule(room reservation)
    ArrayList<HashMap<String, String>> notificationListScheduleBefore;

    //List notification schedule(resource)
    ArrayList<HashMap<String, String>> notificationListScheduleBeforeAndAfter;

    //List notification schedule
    ArrayList<HashMap<String, String>> notificationListTest;

    ArrayList<HashMap<String, String>> locationsList = new ArrayList<HashMap<String, String>>();

    String idSelectedLocation = "";
    String idLocationOffice = "";
    String idLocationHome = "";
    String idLocationTravelling = "";
    String idLocationOther = "";

    private final static AtomicInteger c = new AtomicInteger(0);


    @Override
    public boolean onStartJob(JobParameters params) {

        // Toast.makeText(MyService.this, "Service started!", Toast.LENGTH_SHORT).show();

        Log.d("TAG", "onStartCommand MyService");

        SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
        user = prefUser.getString("USER", "");
        pass = prefUser.getString("PASS", "");

        webservice_url = prefUser.getString("URL", "");

        Log.d("TAG", "user == null: " + user);
        Log.d("TAG", "pass == null: " + pass);
        Log.d("TAG", "webservice_url == null: " + webservice_url);

        MyProvider = new WSDataProvider(user, pass);
        RegisterServiceToWS();

        if (locationsList.size() == 0) new LoadLocationsList().execute();
        //new LoadLocationsList().execute();

        new RegisterTaskLocation().execute();

        new LoadListMessages().execute();
        new LoadList().execute();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        return true;
    }



    /////////////////////////////////////////////////////////////////

    private void RegisterServiceToWS()
    {
        try
        {
            // read device values
            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            int ipAddress = wifiInf.getIpAddress();
            String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
            String myIP = ip;
            String android_id = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            String myDevice = android_id;
            String appname = "service";
            String myApp = appname;

            MyProvider.SetDeviceIdentification(ip, android_id, appname);

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /*********************************************************************
     * Access Control
     *********************************************************************/

    /**
     * Background Async Task to Load all locations by making HTTP Request
     * */
    class LoadLocationsList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            locationsList = new ArrayList<HashMap<String, String>>();

            // convert objects to hashmap for list
            List<cUserLocation> list = MyProvider.getUserLocation("");

            for(cUserLocation entry : list)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", Long.toString(entry.id));
                map.put("name", entry.name);

                if(entry.name.contains("Travelling")){
                    idLocationTravelling = Long.toString(entry.id);
                }else if(entry.name.contains("In Office")){
                    idLocationOffice = Long.toString(entry.id);
                }else if(entry.name.contains("At Home")){
                    idLocationHome = Long.toString(entry.id);
                }else if(entry.name.contains("Out of Office")){
                    idLocationOther = Long.toString(entry.id);
                }

                // adding HashList to ArrayList
                locationsList.add(map);
            }


            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Background Async Task to detect the Wifi or Mobile network location and register in webservice
     * */

    private class RegisterTaskLocation extends AsyncTask<String, String, Boolean>{

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("defaults", Context.MODE_PRIVATE );
            String ssid_name_office = sharedPrefs.getString("prefNameOfficeSSID", "");
            String ssid_name_home = sharedPrefs.getString("prefNameHomeSSID", "");

            boolean suc = false;

            String ssid = null;

            if (ssid_name_office != "" || ssid_name_home != "")
            {

                ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                // WIFI
                NetworkInfo networkInfoWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (networkInfoWifi.isConnected()) {
                    final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {

                        ssid = connectionInfo.getSSID();
                        String ssidResult = ssid.replace("\"", "");
                        Log.d("LOGIN Wifi : ", "Connected : " + ssidResult);

                        if(ssidResult.equals(ssid_name_office)){
                            Log.d("SSID Name Wifi : ", ssid_name_office);
                            Log.d("LOGIN Wifi : ", "i am in office area wifi");

                            if(!idSelectedLocation.equals(idLocationOffice)) suc = MyProvider.createUserCurrentLocation(user, idLocationOffice);

                            idSelectedLocation = idLocationOffice;

                        }else{
                            if(ssidResult.equals(ssid_name_home)){

                                Log.d("SSID Name Wifi : ", ssid_name_home);
                                Log.d("LOGIN Wifi : ", "i am in home area wifi");

                                if(!idSelectedLocation.equals(idLocationHome)) suc = MyProvider.createUserCurrentLocation(user, idLocationHome);

                                idSelectedLocation = idLocationHome;
                            } else {

                                Log.d("LOGIN Wifi : ", "Connected: i am NOT in area wifi");

                                if(!idSelectedLocation.equals(idLocationTravelling)) suc = MyProvider.createUserCurrentLocation( user, idLocationTravelling);

                                idSelectedLocation = idLocationTravelling;
                            }
                        }

                    }
                }else{

                    Log.d("LOGIN Wifi : ", "Connected: I DON'T have wifi on");

                    if(!idSelectedLocation.equals(idLocationOther)) suc = MyProvider.createUserCurrentLocation( user, idLocationOther);

                    idSelectedLocation = idLocationOther;
                }

            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            // updating UI from Background Thread

        }
    }

    /*********************************************************************
     * DIGI_PORTAL
     *********************************************************************/

    /**
     * Background Async Task to Load all messages by making HTTP Request
     * */
    class LoadListMessages extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        protected Boolean doInBackground(String... args) {

            // MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            messageList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            List<cMessage> message = MyProvider.getMessagesInbox();
            for(cMessage entry : message)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("title", entry.user_from);
                map.put("created", entry.datetime);
                map.put("description", entry.message);
                map.put("status", entry.status_read);

                statusReadMessage = entry.status_read;

                Log.d("STATUS_READ " , statusReadMessage);

                // adding HashList to ArrayList
                messageList.add(map);
            }

            boolean tokenIsStillAvailable = true;

            if(messageList.size() > 0){
                System.out.println("Result !=: " + messageList);
                tokenIsStillAvailable = true;

            }else{
                System.out.println("Result: " + messageList);
                //	 tokenIsStillAvailable = false;
            }

            return tokenIsStillAvailable;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(Boolean result) {
            // here you check the value of getActivity() and break up if needed
	        /*if(getApplicationContext() == null)
	            return;*/
            /**
             * Updating parsed JSON data into ListView
             * */
	    	/*new Runnable() {
	            public void run() {*/

            System.out.println("Result boolean: " + result);

            if(result == true){
                int total = 0;
                int total_unread = 0;
                int total_read = 0;
                int myRow = 0;
                for (HashMap<String, String> map : messageList)
                    for (Map.Entry<String, String> entry : map.entrySet())
                    {
                        if (entry.getKey().equals("status") )
                        {

                            try {
                                myRow = Integer.parseInt(entry.getValue().toString());
                                String row = String.valueOf(myRow);

                                if(row.contains("0")){
                                    System.out.println("hello in if 0: " + myRow);
                                    total_unread++;
                                }
                                if(row.contains("1")){
                                    System.out.println("hello in if 1: " + myRow);
                                    total_read++;
                                }

                            } catch(NumberFormatException nfe) {
                                myRow = 0;
                            }
                            total = total_unread + total_read;
                        }
                    }
                System.out.println("Total read MyService message : " + total_read);
                System.out.println("Total unread MyService message : " + total_unread);
                System.out.println("Total MyService message : " + total);

                nrUnreadMessage = total_unread;

                if(nrUnreadMessage > 0){

                    showMessageNotification(total_unread);
                }

            }
            else{
                new UserLoginTask().execute();
            }






	         /*   }
	        };*/
        }


    }



    /**
     * Background Async Task to Load all news by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            newsList = new ArrayList<HashMap<String, String>>();
            notificationListNormal = new ArrayList<HashMap<String, String>>();
            notificationListDigiTime = new ArrayList<HashMap<String, String>>();
            notificationListDigiPortal = new ArrayList<HashMap<String, String>>();
            notificationListDigiSupport = new ArrayList<HashMap<String, String>>();
            notificationListScheduleBefore = new ArrayList<HashMap<String, String>>();
            //notificationListScheduleBefore = new LinkedHashMap<String, String>();
            notificationListScheduleBeforeAndAfter = new ArrayList<HashMap<String, String>>();

            SharedPreferences prefIdContact = getSharedPreferences("PREF_ID_CONTACT", 0);
            String idContact = prefIdContact.getString("idContact", "");

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            List<cNewsStatus> news = MyProvider.getNewsUnread(idContact);
            for(cNewsStatus entry : news)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("id_news", entry.id_news);
                map.put("id_user", entry.id_user);
                map.put("status", entry.status);

                statusReadNews = entry.status;

                Log.d("STATUS_READ " , statusReadNews);

                // adding HashList to ArrayList
                newsList.add(map);
            }

            // intr_notification
            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            List<cStatusNotification> notification = MyProvider.getStatusNotification(idContact);
            for(cStatusNotification entry : notification)
            {

                HashMap<String, String> map = new HashMap<String, String>();
                //HashBiMap<String, String> map = HashBiMap.create();


                // map.put("status", entry.status);

                // System.out.println("map: " + entry.datetime);

                if(entry.type.contains("reservation") || entry.type.contains("visitor")){
                    map.put("datetime/description/id", entry.datetime+"/"+entry.description+"/"+entry.id);

                    notificationListScheduleBefore.add(map);


                }else if (entry.type.contains("resource")){
                    map.put("datetime/description/id", entry.datetime+"/"+entry.description+"/"+entry.id);

                    notificationListScheduleBeforeAndAfter.add(map);

                }else if (entry.type.contains("holiday") || entry.type.contains("sickday") ||
                        entry.type.contains("project") || entry.type.contains("project_item")){

                    if(entry.type.equals("project_item")){
                        cProjectItemDetail item = MyProvider.getDetailProjectItemForNotification(entry.id_object);

                        String description = entry.description + " to the project " + item.projectname;

                        map.put("datetime", entry.datetime);
                        map.put("description/projectid", description+"/"+item.projectid);
                        map.put("id",  String.valueOf(entry.id));

                    }else if(entry.type.equals("project")){
                        cProject item = MyProvider.getProjectForNotification(entry.id_object);

                        String description = entry.description + " " + item.name;

                        map.put("datetime", entry.datetime);
                        map.put("description/projectid", description+"/"+item.id);
                        map.put("id",  String.valueOf(entry.id));

                    }else{
                        map.put("datetime", entry.datetime);
                        map.put("description", entry.description);
                        map.put("id",  String.valueOf(entry.id));
                    }

                    notificationListDigiTime.add(map);

                }else if (entry.type.contains("news")){

                    map.put("datetime", entry.datetime);
                    map.put("description", entry.description);
                    map.put("id",  String.valueOf(entry.id));

                    notificationListDigiPortal.add(map);

                }else if (entry.type.contains("request")){

                    map.put("datetime", entry.datetime);
                    map.put("description", entry.description);
                    map.put("id",  String.valueOf(entry.id));

                    notificationListDigiSupport.add(map);

                }else{
                    map.put("datetime", entry.datetime);
                    map.put("description", entry.description);
                    map.put("id",  String.valueOf(entry.id));

                    notificationListNormal.add(map);
                }

/*    				  Calendar c = Calendar.getInstance();
					  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			          String dateTimeNow = df.format(c.getTime());

			       // date time now
      		        StringTokenizer tkTimeNow = new StringTokenizer(dateTimeNow);
      		        String dateNowTokenizer = tkTimeNow.nextToken();  // <---  yyyy-mm-dd
      		        String timeNowTokenizer = tkTimeNow.nextToken();  // <---  hh:mm:ss
      		        String[] separatedDateNow = dateNowTokenizer.split("-");
      		        String[] separatedTimeNow = timeNowTokenizer.split(":");
      		        String yearNow = separatedDateNow[0];
      		        String monthNow = separatedDateNow[1];
      		        String dayNow = separatedDateNow[2];
      		        String hourNow = separatedTimeNow[0];
      		        String minuteNow = separatedTimeNow[1];

      		        // date time schedule
      		        StringTokenizer tkTimeSchedule = new StringTokenizer(entry.datetime);
      		        String dateScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  yyyy-mm-dd
      		        String timeScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  hh:mm:ss
      		        String[] separatedDateSchedule = dateScheduleTokenizer.split("-");
      		        String[] separatedTimeSchedule = timeScheduleTokenizer.split(":");
      		        String yearSchedule = separatedDateSchedule[0];
      		        String monthSchedule = separatedDateSchedule[1];
      		        String daySchedule = separatedDateSchedule[2];
      		        String hourSchedule = separatedTimeSchedule[0];
      		        String minuteSchedule = separatedTimeSchedule[1];

			          // Compare date time schedule with date time now
			            Date dateCompareNow = parseDate(String.valueOf(yearNow) + "-" + String.valueOf(monthNow) + "-" + String.valueOf(dayNow) + " " + String.valueOf(hourNow) + ":" + String.valueOf(minuteNow));
			            Date dateCompareSchedule = parseDate(String.valueOf(yearSchedule) + "-" + String.valueOf(monthSchedule) + "-" + String.valueOf(daySchedule) + " " + String.valueOf(hourSchedule) + ":" + String.valueOf(minuteSchedule));

			            if(dateCompareSchedule.before(dateCompareNow)){
			            	// date time schedule < date time now = show notification usually

			                notificationListNormal.add(map);
			                nrNotifications++;

			            }else{
			            	// set schedule alarm - 15 minute

			            	notificationListSchedule.add(map);
			            }*/

            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
		    	/*new Runnable() {
		            public void run() {*/

            int total = 0;
            int total_unread = 0;
            int total_read = 0;
            int myRow = 0;
            for (HashMap<String, String> map : newsList)
                for (Map.Entry<String, String> entry : map.entrySet())
                {
                    if (entry.getKey().equals("status"))
                    {

                        try {
                            myRow = Integer.parseInt(entry.getValue().toString());
                            String row = String.valueOf(myRow);

                            if(row.contains("0")){
                                total_unread++;
                            }
                            if(row.contains("1")){
                                total_read++;
                            }

                        } catch(NumberFormatException nfe) {
                            myRow = 0;
                        }
                        total = total_unread + total_read;
                    }
                }
            System.out.println("Total read MyService News : " + total_read);
            System.out.println("Total unread MyService News : " + total_unread);
            System.out.println("Total MyService News : " + total);

            nrUnreadNews = total_unread;

            showNormalNotification();
            showScheduleBeforeNotification();
            showScheduleBeforeAndAfterNotification();
        }

    }


    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    protected void showSingleNotification(NotificationManagerCompat notificationManagerCompat,
                                          PendingIntent pendingIntent,
                                          String title,
                                          String message,
                                          int notificationId) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "my_channel_001";
            String description = "This is my channel 01";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_01, name, importance);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID_01);
        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.launcher)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setVibrate(new long[] {0,100,0,100})
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        Notification notification = builder.build();
        notificationManagerCompat.notify(notificationId, notification);
    }

    public void showMessageNotification(int total_unread){

        Intent launchIntentDigiPortal = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digiportal");

        // if DigiPortal installed then open application when click on notification
        if(launchIntentDigiPortal != null){

            System.out.println("launchIntentDigiPortal status: " + "exist");

            launchIntentDigiPortal.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, launchIntentDigiPortal, PendingIntent.FLAG_ONE_SHOT);

            PendingIntent pendingIntent = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getActivity
                        (this, 1, launchIntentDigiPortal, PendingIntent.FLAG_MUTABLE);
            }
            else
            {
                pendingIntent = PendingIntent.getActivity(this, 1, launchIntentDigiPortal, PendingIntent.FLAG_ONE_SHOT);

            }

            notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
            builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_02);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                CharSequence name = "my_channel_002";
                String description = "This is my channel 02";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_02, name, importance);
                mChannel.setDescription(description);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.RED);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mChannel.setShowBadge(false);
                notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(mChannel);
            }

            builder.setSmallIcon(R.drawable.launcher)
                    .setContentTitle("New message")
                    .setContentText("You have " + total_unread + " messages!")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("messages"))
                    .setTicker("Notification!")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setVibrate(new long[] {0,100,0,100});

            // Sets an ID for the notification
            int mNotificationId = 1;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, builder.build());

            // else if not installed the application , display toast to install the application
        }else{
            System.out.println("launchIntentDigiPortal status: " + "null");

            Intent intentDigiPortalNull = new Intent(this, ToastClass.class);
            intentDigiPortalNull.putExtra("text", "DigiPortal not installed");
            intentDigiPortalNull.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //PendingIntent pendingIntentYes = PendingIntent.getActivity(this, 1, intentDigiPortalNull, PendingIntent.FLAG_UPDATE_CURRENT);

            PendingIntent pendingIntentYes = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                pendingIntentYes = PendingIntent.getActivity
                        (this, 1, intentDigiPortalNull, PendingIntent.FLAG_MUTABLE);
            }
            else
            {
                pendingIntentYes = PendingIntent.getActivity(this, 1, intentDigiPortalNull, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
            builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_03);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                CharSequence name = "my_channel_003";
                String description = "This is my channel 03";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_03, name, importance);
                mChannel.setDescription(description);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.RED);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mChannel.setShowBadge(false);
                notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(mChannel);
            }

            builder.setSmallIcon(R.drawable.launcher)
                    .setContentTitle("New message")
                    .setContentText("You have " + total_unread + " messages!")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("messages"))
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntentYes)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setVibrate(new long[] {0,100,0,100});

            Notification notification = builder.build();
            notificationManagerCompat.notify(1, notification);

        }
    }


    public void showNormalNotification(){

        /*********************************************************************
         * Other notification
         *********************************************************************/

        if(notificationListNormal.size() != 0){

            //Intent launchIntentDigiPortal = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digiportal");
            Intent intent = new Intent();

            // if DigiPortal installed then open application when click on notification
            if(intent != null){

                System.out.println("launchIntentDigiPortal status: " + "exist");

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntent = PendingIntent.getActivity(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntent = PendingIntent.getActivity
                            (this, 2, intent, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntent = PendingIntent.getActivity(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                }

                // Create an InboxStyle notification
                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_04);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_004";
                    String description = "This is my channel 04";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_04, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100})
                        .setNumber(notificationListNormal.size())
                        .setContentTitle(notificationListNormal.size() + " " + "new news");

                inboxStyle.setSummaryText("News summary text")
                        .setBigContentTitle(notificationListNormal.size() + " " + "new news");

                //int myRowNotification1 = 0;
                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListNormal)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            inboxStyle.addLine(notificationDescription);
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(2, notification);

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListNormal)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }

                // else if not installed the application , display toast to install the application
            }else{
                System.out.println("launchIntentDigiPortal status: " + "null");

                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_05);

                Intent intent1 = new Intent();
                //intentDigiPortal.setAction(AppConstant.DIGIPORTAL_ACTION_NO);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntentYes = PendingIntent.getBroadcast(this, 2, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntentYes = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntentYes = PendingIntent.getActivity
                            (this, 2, intent1, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntentYes = PendingIntent.getBroadcast(this, 2, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

                }

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_005";
                    String description = "This is my channel 05";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_05, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setContentIntent(pendingIntentYes)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100});

                //int myRowNotification1 = 0;
                String notificationDescription = "";
                int nrNotify = 0;
                for (HashMap<String, String> map1 : notificationListNormal)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();
                            nrNotify++;

                            builder.setNumber(nrNotify)
                                    .setContentTitle(nrNotify + " " + "new news");

                            inboxStyle.addLine(notificationDescription)
                                    .setSummaryText("News summary text")
                                    .setBigContentTitle(nrNotify + " " + "new news");
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(2, notification);


                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListNormal)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }
            }
        }

        /////////////////////////////////////////////////////////////////////

        /*********************************************************************
         * DIGI_WORK_TIME
         *********************************************************************/

        if(notificationListDigiTime.size() != 0){

            Intent launchIntentDigiTime = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digitime");

            // if DigiPortal installed then open application when click on notification
            if(launchIntentDigiTime != null){

                System.out.println("launchIntentDigiTime status: " + "exist");

                launchIntentDigiTime.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);


                //		notificationManagerCompact = NotificationManagerCompat.from(MyService.this);
                // Create an InboxStyle notification
                //    	inboxStyle = new NotificationCompat.InboxStyle();



                // builder = new NotificationCompat.Builder(MyService.this);

                // builder.setSmallIcon(R.drawable.launcher)
                //  .setWhen(System.currentTimeMillis())
                //  .setContentIntent(pendingIntent)
                //  .setAutoCancel(true)
                //  .setVibrate(new long[] {0,100,0,100})
                //  .setContentTitle("new news");

                //inboxStyle.setBigContentTitle(notificationListDigiTime.size() + " " + "new news");


                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiTime)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if(entry1.getKey().equals("description/projectid")){

                            String row = String.valueOf(entry1.getValue().toString());
                            String[] parts = row.split("/");
                            if (parts.length > 1) {
                                String description = parts[0];
                                String projectid = parts[1];

                                //if (entry1.getKey().equals("description")){
                                //notificationDescription = entry1.getValue().toString();

                                //Intent launchIntent = new Intent(this, LoginActivity.class);
                                //launchIntent.putExtra("ID", projectid);

                                launchIntentDigiTime.putExtra("ID", projectid);
                                //PendingIntent pendingIntent = PendingIntent.getActivity(this, getID(), launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

                                PendingIntent pendingIntent = null;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                                    pendingIntent = PendingIntent.getActivity
                                            (this, getID(), launchIntentDigiTime, PendingIntent.FLAG_MUTABLE);
                                }
                                else
                                {
                                    pendingIntent = PendingIntent.getActivity(this, getID(), launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

                                }

                                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MyJobService.this);

                                showSingleNotification(notificationManager, pendingIntent, "Something new", description, getID());
                            }
                            //inboxStyle.addLine(notificationDescription);
                            //}
                        }else if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 3, launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

                            PendingIntent pendingIntent = null;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                                pendingIntent = PendingIntent.getActivity
                                        (this, 3, launchIntentDigiTime, PendingIntent.FLAG_MUTABLE);
                            }
                            else
                            {
                                pendingIntent = PendingIntent.getActivity(this, 3, launchIntentDigiTime, PendingIntent.FLAG_UPDATE_CURRENT);

                            }

                            NotificationManagerCompat  notificationManager = NotificationManagerCompat.from(MyJobService.this);;
                            showSingleNotification(notificationManager, pendingIntent, "Something new", notificationDescription, getID());

                            //inboxStyle.addLine(notificationDescription);
                        }

                    }

                // builder.setStyle(inboxStyle);
                // Notification notification = builder.build();
                //notificationManagerCompact.notify(3, notification); // 3

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiTime)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }

                // else if not installed the application , display toast to install the application
            }else{
                System.out.println("launchIntentDigiTime status: " + "null");

                Intent intentDigiTimeNull = new Intent(this, ToastClass.class);
                intentDigiTimeNull.putExtra("text", "DigiWorkTime not installed");
                intentDigiTimeNull.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntent = PendingIntent.getActivity(this, 3, intentDigiTimeNull, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntent = PendingIntent.getActivity
                            (this, 3, intentDigiTimeNull, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntent = PendingIntent.getActivity(this, 3, intentDigiTimeNull, PendingIntent.FLAG_UPDATE_CURRENT);
                }

                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiTime)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            NotificationManagerCompat  notificationManager = NotificationManagerCompat.from(MyJobService.this);;
                            showSingleNotification(notificationManager, pendingIntent, "Something new", notificationDescription, getID());
                        }
                    }

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiTime)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }
            }
        }

        /////////////////////////////////////////////////////////////////////

        /*********************************************************************
         * DIGI_PORTAL
         *********************************************************************/

        if(notificationListDigiPortal.size() != 0){

            Intent launchIntentDigiPortal = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digiportal");

            // if DigiPortal installed then open application when click on notification
            if(launchIntentDigiPortal != null){

                System.out.println("launchIntentDigiPortal status: " + "exist");

                launchIntentDigiPortal.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntent = PendingIntent.getActivity(this, 4, launchIntentDigiPortal, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntent = PendingIntent.getActivity
                            (this, 4, launchIntentDigiPortal, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntent = PendingIntent.getActivity(this, 4, launchIntentDigiPortal, PendingIntent.FLAG_UPDATE_CURRENT);
                }

                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_06);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_006";
                    String description = "This is my channel 06";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_06, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100})
                        .setNumber(notificationListDigiPortal.size())
                        .setContentTitle(notificationListDigiPortal.size() + " " + "new news");

                inboxStyle.setSummaryText("News summary text")
                        .setBigContentTitle(notificationListDigiPortal.size() + " " + "new news");

                //int myRowNotification1 = 0;
                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiPortal)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            inboxStyle.addLine(notificationDescription);
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(4, notification);

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiPortal)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }

                // else if not installed the application , display toast to install the application
            }else{
                System.out.println("launchIntentDigiPortal status: " + "null");

                Intent intentDigiPortalNull = new Intent(this, ToastClass.class);
                intentDigiPortalNull.putExtra("text", "DigiPortal not installed");
                intentDigiPortalNull.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntentYes = PendingIntent.getActivity(this, 4, intentDigiPortalNull, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntentYes = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntentYes = PendingIntent.getActivity
                            (this, 4, intentDigiPortalNull, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntentYes = PendingIntent.getActivity
                            (this, 4, intentDigiPortalNull, PendingIntent.FLAG_UPDATE_CURRENT);
                }

                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_07);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_007";
                    String description = "This is my channel 07";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_07, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setContentIntent(pendingIntentYes)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100})
                        .setNumber(notificationListDigiPortal.size())
                        .setContentTitle(notificationListDigiPortal.size() + " " + "new news");

                inboxStyle.setSummaryText("News summary text")
                        .setBigContentTitle(notificationListDigiPortal.size() + " " + "new news");

                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiPortal)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            inboxStyle.addLine(notificationDescription);
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(4, notification);

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiPortal)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }
            }
        }

        /////////////////////////////////////////////////////////////////////

        /*********************************************************************
         * DIGI_SUPPORT
         *********************************************************************/

        if(notificationListDigiSupport.size() != 0){

            Intent launchIntentDigiSupport = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digisupport");

            // if DigiPortal installed then open application when click on notification
            if(launchIntentDigiSupport != null){

                System.out.println("launchIntentDigiSupport status: " + "exist");

                launchIntentDigiSupport.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //PendingIntent pendingIntent = PendingIntent.getActivity(this, 5, launchIntentDigiSupport, PendingIntent.FLAG_ONE_SHOT);

                PendingIntent pendingIntent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntent = PendingIntent.getActivity
                            (this, 5, launchIntentDigiSupport, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntent = PendingIntent.getActivity
                            (this, 5, launchIntentDigiSupport, PendingIntent.FLAG_ONE_SHOT);
                }

                // Create an InboxStyle notification
                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_08);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_008";
                    String description = "This is my channel 08";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_08, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100})
                        .setNumber(notificationListDigiSupport.size())
                        .setContentTitle(notificationListDigiSupport.size() + " " + "new news");

                inboxStyle.setSummaryText("News summary text")
                        .setBigContentTitle(notificationListDigiSupport.size() + " " + "new news");

                //int myRowNotification1 = 0;
                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiSupport)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            inboxStyle.addLine(notificationDescription);
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(5, notification);

                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiSupport)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }

                // else if not installed the application , display toast to install the application
            }else{
                System.out.println("launchIntentDigiSupport status: " + "null");

                Intent intentDigiSupportNull = new Intent(this, ToastClass.class);
                intentDigiSupportNull.putExtra("text", "DigiSupport not installed");
                intentDigiSupportNull.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //PendingIntent pendingIntentYes = PendingIntent.getActivity(this, 5, intentDigiSupportNull, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntentYes = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    pendingIntentYes = PendingIntent.getActivity
                            (this, 5, intentDigiSupportNull, PendingIntent.FLAG_MUTABLE);
                }
                else
                {
                    pendingIntentYes = PendingIntent.getActivity
                            (this, 5, intentDigiSupportNull, PendingIntent.FLAG_UPDATE_CURRENT);
                }

                inboxStyle = new NotificationCompat.InboxStyle();
                notificationManagerCompat = NotificationManagerCompat.from(MyJobService.this);
                builder = new NotificationCompat.Builder(MyJobService.this, CHANNEL_ID_09);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = "my_channel_009";
                    String description = "This is my channel 09";
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID_09, name, importance);
                    mChannel.setDescription(description);
                    mChannel.enableLights(true);
                    mChannel.setLightColor(Color.RED);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mChannel.setShowBadge(false);
                    notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.createNotificationChannel(mChannel);
                }

                builder.setContentText("News summary text")
                        .setSmallIcon(R.drawable.launcher)
                        .setGroupSummary(true)
                        .setGroup("group")
                        .setContentIntent(pendingIntentYes)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] {0,100,0,100})
                        .setNumber(notificationListDigiSupport.size())
                        .setContentTitle(notificationListDigiSupport.size() + " " + "new news");

                inboxStyle.setSummaryText("News summary text")
                        .setBigContentTitle(notificationListDigiSupport.size() + " " + "new news");

                String notificationDescription = "";
                for (HashMap<String, String> map1 : notificationListDigiSupport)
                    for (Map.Entry<String, String> entry1 : map1.entrySet())
                    {
                        if (entry1.getKey().equals("description")){
                            notificationDescription = entry1.getValue().toString();

                            inboxStyle.addLine(notificationDescription);
                        }
                    }

                builder.setStyle(inboxStyle);

                Notification notification = builder.build();
                notificationManagerCompat.notify(5, notification);


                int myRowNotification2 = 0;
                for (HashMap<String, String> map2 : notificationListDigiSupport)
                    for (Map.Entry<String, String> entry2 : map2.entrySet())
                    {
                        if (entry2.getKey().equals("id")){
                            myRowNotification2 = Integer.parseInt(entry2.getValue().toString());
                            String row = String.valueOf(myRowNotification2);

                            new UpdateStatusNotification(row).execute();
                        }
                    }
            }
        }

    }

    public static int getID() {
        return c.incrementAndGet();
    }


    public void showScheduleBeforeNotification(){

        if(notificationListScheduleBefore.size() != 0){

            Intent intent = new Intent(this, ScheduleAlarmReceiver.class);

            for (HashMap<String, String> map1 : notificationListScheduleBefore)
            {
                for (Map.Entry<String, String> entry1 : map1.entrySet())
                {
                    System.out.printf("Before :%s -> %s%n", entry1.getKey(), entry1.getValue());

                    String row = String.valueOf(entry1.getValue().toString());
                    String[] parts = row.split("/");
                    String datetime = parts[0];
                    String description = parts[1];
                    String id = parts[2];


                    // date time schedule
                    StringTokenizer tkTimeSchedule = new StringTokenizer(datetime);
                    String dateScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  yyyy-mm-dd
                    String timeScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  hh:mm:ss
                    String[] separatedDateSchedule = dateScheduleTokenizer.split("-");
                    String[] separatedTimeSchedule = timeScheduleTokenizer.split(":");
                    String yearSchedule = separatedDateSchedule[0];
                    String monthSchedule = separatedDateSchedule[1];
                    String daySchedule = separatedDateSchedule[2];
                    String hourSchedule = separatedTimeSchedule[0];
                    String minuteSchedule = separatedTimeSchedule[1];

                    int decreasedMonth = Integer.valueOf(monthSchedule) - 1;
                    // int decreasedMinutheScehdule = Integer.valueOf(minuteSchedule) - 15 ;

                    Calendar calendar = Calendar.getInstance();
                    calendar.clear();
                    calendar.setTimeInMillis(System.currentTimeMillis());
                    calendar.set(Calendar.YEAR, Integer.valueOf(yearSchedule));
                    calendar.set(Calendar.MONTH, Integer.valueOf(decreasedMonth));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(daySchedule));
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourSchedule));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(minuteSchedule));
                    calendar.set(Calendar.SECOND, 00);

                    long when = calendar.getTimeInMillis();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String result = sdf.format(calendar.getTime());
                    System.out.println(result);

                    intent.putExtra("descList", description);
                    intent.putExtra("idList", id);
                    //PendingIntent pendingIntent = PendingIntent.getBroadcast(this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    PendingIntent pendingIntent = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_MUTABLE);
                    }
                    else
                    {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, when, pendingIntent);
                }
            }

        }

    }


    public void showScheduleBeforeAndAfterNotification(){

        if(notificationListScheduleBeforeAndAfter.size() != 0){
            Intent intent = new Intent(this, ScheduleAlarmReceiver.class);

            for (HashMap<String, String> map1 : notificationListScheduleBeforeAndAfter){
                //map = new HashMap<String, String>();
                for (Map.Entry<String, String> entry1 : map1.entrySet())
                {
                    System.out.printf("BeforeAfter :%s -> %s%n", entry1.getKey(), entry1.getValue());

                    String row = String.valueOf(entry1.getValue().toString());
                    String[] parts = row.split("/");
                    String datetime = parts[0];
                    String description = parts[1];
                    String id = parts[2];

                    // date time schedule
                    StringTokenizer tkTimeSchedule = new StringTokenizer(datetime);
                    String dateScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  yyyy-mm-dd
                    String timeScheduleTokenizer = tkTimeSchedule.nextToken();  // <---  hh:mm:ss
                    String[] separatedDateSchedule = dateScheduleTokenizer.split("-");
                    String[] separatedTimeSchedule = timeScheduleTokenizer.split(":");
                    String yearSchedule = separatedDateSchedule[0];
                    String monthSchedule = separatedDateSchedule[1];
                    String daySchedule = separatedDateSchedule[2];
                    String hourSchedule = separatedTimeSchedule[0];
                    String minuteSchedule = separatedTimeSchedule[1];

                    int decreasedMonth = Integer.valueOf(monthSchedule) - 1;
                    int decreasedMinutheScehdule = Integer.valueOf(minuteSchedule) - 15 ;
                    int creasedMinutheScehdule = Integer.valueOf(minuteSchedule) + 15 ;

                    Calendar calendar = Calendar.getInstance();
                    calendar.clear();
                    calendar.setTimeInMillis(System.currentTimeMillis());
                    calendar.set(Calendar.YEAR, Integer.valueOf(yearSchedule));
                    calendar.set(Calendar.MONTH, Integer.valueOf(decreasedMonth));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(daySchedule));
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourSchedule));
                    calendar.set(Calendar.MINUTE, decreasedMinutheScehdule);
                    calendar.set(Calendar.SECOND, 00);

                    long when = calendar.getTimeInMillis();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String result = sdf.format(calendar.getTime());
                    System.out.println(result);


                    // map.put("date",result);

                    intent.putExtra("descList", description);
                    intent.putExtra("idList", id);
                    //PendingIntent pendingIntent = PendingIntent.getBroadcast(this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    PendingIntent pendingIntent = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_MUTABLE);
                    }
                    else
                    {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, when, pendingIntent);

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    // if is not returned yet the resource - show notification after 15 min

                    calendar.clear();
                    calendar.setTimeInMillis(System.currentTimeMillis());
                    calendar.set(Calendar.YEAR, Integer.valueOf(yearSchedule));
                    calendar.set(Calendar.MONTH, Integer.valueOf(decreasedMonth));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(daySchedule));
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourSchedule));
                    calendar.set(Calendar.MINUTE, creasedMinutheScehdule);
                    calendar.set(Calendar.SECOND, 00);

                    long whenAfter = calendar.getTimeInMillis();

                    SimpleDateFormat sdfAfter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String resultAfter = sdfAfter.format(calendar.getTime());
                    System.out.println(resultAfter);


                    // map.put("date",result);

                    intent.putExtra("descList", description);
                    intent.putExtra("idList", id);
                    //PendingIntent pendingIntentAfter = PendingIntent.getBroadcast(this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    PendingIntent pendingIntentAfter = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_MUTABLE);
                    }
                    else
                    {
                        pendingIntent = PendingIntent.getActivity
                                (this, getID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    AlarmManager alarmManagerAfter = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManagerAfter.set(AlarmManager.RTC_WAKEUP, whenAfter, pendingIntentAfter);


                }
            }

        }

    }


    /** AsyncTask update notification status */
    private class UpdateStatusNotification extends AsyncTask<String, String, Boolean>{
        String idNotification;

        public UpdateStatusNotification(String row){
            idNotification = row;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... args) {

            boolean suc = false;
            // call web method
            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            suc = MyProvider.updateNewsStatusNotification(idNotification, "1");

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean result) {

        }
    }


    /*********************************************************************
     * DIGI_SUPPORT
     *********************************************************************/

    /*********************************************************************
     * DIGI_VISITOR
     *********************************************************************/

    /*********************************************************************
     * DIGI_WORK_TIME
     *********************************************************************/




    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String mEmail = user;
            String mPassword = pass;

            if(mEmail.length() == 0 || mPassword.length() == 0 ){
			    	/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
			        String prefEmail = pref.getString("Name", "");
			        String prefPass = pref.getString("Pass", "");*/

                SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
                String prefEmail = prefUser.getString("USER", "");
                String prefPass = prefUser.getString("PASS", "");
                webservice_url = prefUser.getString("URL", "");

                mEmail = prefEmail;
                mPassword = prefPass;

                System.out.println("mEmail mPassword from pref: " + mEmail + " " + mPassword);
            }else {
                System.out.println("mEmail mPassword from global: " + mEmail + " " + mPassword);
            }

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "app service");

            return suc;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                System.out.println("Login successfully");
                new LoadListMessages().execute();
            } else {
                System.out.println("Login failed");
            }
        }

        @Override
        protected void onCancelled() {

        }
    }


}
