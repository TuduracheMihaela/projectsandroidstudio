package com.sharethatdata.webservice.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

public class ScheduleAlarmReceiver extends BroadcastReceiver {
   
/*	NotificationCompat.InboxStyle inboxStyle ; 
   	NotificationManagerCompat notificationManager ;  
    NotificationCompat.Builder builder ;*/
    
    private final static AtomicInteger c = new AtomicInteger(0);
	
    @Override
    public void onReceive(Context context, Intent intent) {
	        
    	Log.d("Received : ", "ScheduleAlarmReceiver");

        // Schedule a service for android 8 and up
        Util.scheduleJobScheduleAlarmService(context, intent);

        // Comment this because I try to implement service for android 8 and up
    	 /*Intent service1 = new Intent(context, ScheduleAlarmService.class);
    	
    	 
    	 String description = intent.getStringExtra("descList");
    	 String id = intent.getStringExtra("idList");
    	 
    	 service1.putExtra("descList", description);
    	 service1.putExtra("idList", id);
	      
	     context.startService(service1);*/
    }
    
    public static int getID() {
        return c.incrementAndGet();
    }

    
}
