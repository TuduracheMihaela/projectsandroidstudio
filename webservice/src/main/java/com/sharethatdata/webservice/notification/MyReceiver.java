package com.sharethatdata.webservice.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.sharethatdata.webservice.notification.notification.android.oreo.MyForeGroundService;

public class MyReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		

		Log.d("Received : ", "MyReceiver");

		// Schedule a service for android 8 and up
		Util.scheduleJobMyService(context);


		// Comment this because I try to implement service for android 8 and up
		/*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			Intent service1 = new Intent(context, MyForeGroundService.class);
			context.startForegroundService(service1);
		}else {
			Intent service1 = new Intent(context, MyService.class);
			context.startService(service1);
		}*/


	       
	 }

 } 
