package com.sharethatdata.webservice.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.sharethatdata.webservice.R;
import com.sharethatdata.webservice.WSDataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class ScheduleAlarmService extends Service{
	
	WSDataProvider MyProvider = null;
	
 	NotificationCompat.InboxStyle inboxStyle ; 
   	NotificationManagerCompat notificationManagerCompat ;
    NotificationCompat.Builder builder ;

	String CHANNEL_ID = "my_channel_01";// The id of the channel.
	
	private String user = "";
	private String pass = "";
	private String webservice_url = "";	
	
	 private final static AtomicInteger c = new AtomicInteger(0);
	  
	// ArrayList<HashMap<String, String>> notificationListTest;
	 
	 public static final String inputFormat = "yyyy-MM-dd HH:mm";
	 SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		 Log.d("TAG", "onBind ScheduleAlarmService ");
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		 Log.d("TAG", "onStartCommand ScheduleAlarmService");

	    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
 	        user = prefUser.getString("USER", ""); 
 	        pass = prefUser.getString("PASS", ""); 
 	        webservice_url = prefUser.getString("URL", "");

 	        
 	        String description = intent.getStringExtra("descList");
 	        String id = intent.getStringExtra("idList");
	    	
	    	MyProvider = new WSDataProvider(user, pass);
	    	
	    	inboxStyle = new NotificationCompat.InboxStyle();
			notificationManagerCompat = NotificationManagerCompat.from(ScheduleAlarmService.this);
	        builder = new NotificationCompat.Builder(ScheduleAlarmService.this, CHANNEL_ID);
	        
	 	       
		    	Log.d("TAG", "user == null: " + user);
		    	Log.d("TAG", "pass == null: " + pass);
		    	Log.d("TAG", "webservice_url == null: " + webservice_url);
		    	
		    	MyProvider = new WSDataProvider(user, pass);

		    	if (description != null && !description.isEmpty() && !description.equals("null")) {

					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
						CharSequence name = "my_channel_1";
						String description_channel = "This is my channel 1";
						int importance = NotificationManager.IMPORTANCE_HIGH;
						NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
						mChannel.setDescription(description_channel);
						mChannel.enableLights(true);
						mChannel.setLightColor(Color.RED);
						mChannel.enableVibration(true);
						mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
						mChannel.setShowBadge(false);
						NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.createNotificationChannel(mChannel);
					}
			    	
			    	builder.setSmallIcon(R.drawable.launcher)
				            .setContentTitle("Reservation reminder")
					        .setContentText(description)
					        .setStyle(new NotificationCompat.BigTextStyle().bigText(description))
					        .setGroupSummary(false)
					        .setGroup("group")
					        .setAutoCancel(true)
							.setVibrate(new long[] {0,100,0,100})
				            .setDefaults(Notification.DEFAULT_SOUND)
							.setPriority(NotificationCompat.PRIORITY_DEFAULT);
			    	
			    	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			        mNotificationManager.notify(Integer.valueOf(getID()), builder.build());
		    	}

		    	if (id != null && !id.isEmpty() && !id.equals("null")){

		        new UpdateStatusNotification(id).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "");

	    	}
		    	

	   return Service.START_NOT_STICKY;
		
	}
	
	@Override
    public void onCreate() 
    {
       // TODO Auto-generated method stub  
       super.onCreate();
       Log.d("TAG", "onCreate ScheduleAlarmService");
    }

   @Override
   public void onStart(Intent intent, int startId)
   {
	   
       super.onStart(intent, startId);
       Log.d("TAG", "onStart ScheduleAlarmService");
 
    }
 
    @Override
    public void onDestroy() 
    {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d("TAG", "onDestroy ScheduleAlarmService");

    }
    
    public static int getID() {
        return c.incrementAndGet();
    }
    
	private Date parseDate(String date) {

	    try {
	        return inputParser.parse(date);
	    } catch (java.text.ParseException e) {
	        return new Date(0);
	    }
	}
	
	
	/** AsyncTask update notification status */
    private class UpdateStatusNotification extends AsyncTask<String, String, Boolean>{
    	String idNotification;
    	
    	public UpdateStatusNotification(String row){
    		idNotification = row;
    	}
    	
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
 
        @Override
        protected Boolean doInBackground(String... args) {

        	boolean suc = false;
      	// call web method
			  MyProvider = new WSDataProvider(user, pass);
			  MyProvider.SetWebServiceUrl(webservice_url);

			  suc = MyProvider.updateNewsStatusNotification(idNotification, "1");
		  
			return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean result) {
        	if(result){

        	}

	    }
    }
    

}
//	}

