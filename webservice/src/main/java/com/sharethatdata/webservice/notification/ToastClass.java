package com.sharethatdata.webservice.notification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class ToastClass extends Activity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
	        if(getIntent().getExtras()!=null){
	        	Intent intent = getIntent();
	        	String text = intent.getStringExtra("text");
	        	
	       // 	Toast.makeText(ToastClass.this, text, Toast.LENGTH_LONG).show();
	        	
	        	// Redirect user directly to link in browser to download application, if toast shows with application not installed
	        	Intent httpIntent = new Intent(Intent.ACTION_VIEW);
	        	httpIntent.setData(Uri.parse("http://www.sharethatdata.com/downloads"));
	        	startActivity(httpIntent);
	        	
	        	finish();
	        }
	}	

}
