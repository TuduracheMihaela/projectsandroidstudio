package com.sharethatdata.webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cEmployee;
import com.sharethatdata.webservice.datamodel.cPrintJob;
import com.sharethatdata.webservice.datamodel.cRoomAvailable;
import com.sharethatdata.webservice.datamodel.cVisitor;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cVisitEntries;
import com.sharethatdata.webservice.datamodel.cTimeRecord;

import android.telephony.SmsManager;
import android.util.Log;

//
// DataProvider - class with database/JSON interface
//
public class IntraDataProvider {

	JSONParser jsonParser = new JSONParser();
	
    // url to webservice
    private String website_url = "http://webservice.sharethatdata.com";
    private String digivisitor_url = "http://www.sharethatdata.com/digivisitor";
    private String webservice_url = "http://www.sharethatdata.com/newWS";
	
    private String url_get_contact_new = "get_item.php";
        
    private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    
    private String url_get_contacts = "get_contacts.php";
    private String url_get_projects = "get_projects.php";
    private String url_get_user_locations = "get_user_locations.php";
    private String url_get_activities = "get_project_activities.php";
    private String url_get_user_roles = "get_user_roles.php";
    private String url_get_costplaces = "get_project_costplaces.php";
    
    private String url_get_news = "get_news.php";
    private String url_get_resources = "get_resources.php";
    private String url_get_messages = "get_messages.php";
    
    private String url_get_contact = "get_contact.php";
    private String url_get_project = "get_project.php";
    private String url_get_news_item = "get_news_item.php";
    private String url_get_resource = "get_resource.php";
    private String url_get_message = "get_message.php";
    private String url_get_timerecord = "get_timerecord.php";
    private String url_get_last_timerecord = "get_latest_timerecord.php";
    
    private String url_login = "login.php";
    private String url_log = "log_event.php";
    private String url_get_company = "get_company.php";
    
    private String url_create_message = "create_message.php";
    private String url_create_project_item = "create_project_item.php";
    private String url_create_holiday = "create_holiday.php";
    private String url_create_sickday = "create_sickday.php";
    private String url_create_end_sickday = "create_end_sickday.php";
    private String url_create_time_record = "create_time_record.php";
    private String url_get_project_entries = "get_project_items.php";
    private String url_get_time_records = "get_timerecords.php";
    
    private String url_delete_timerecord = "delete_timerecord.php";
        
	 // urls from digivisitor
	 private String labelservice_url = "http://192.168.1.88:8001";
	 
	 private String url_get_visit_entries = "get_visit_entries.php";
	 private String url_get_visit_entry = "get_visit_members.php";
	 private String url_get_visitors = "get_visitor.php";
	 
	 private String url_get_user = "get_user.php";
	 
	 private String url_get_printjobs = "GetPrintJobs";
	 private String url_create_printjob = "AddPrintJob";
	 
	 private String url_create_visit_entry = "create_visitor_entry.php";
	 private String url_update_visit_entry = "update_visitor_entry.php";
	 private String url_create_end_visit_entry = "create_visitor_end_entry.php";
	 private String url_latest_visit = "get_latest_visit.php";
	 private String url_create_room_appointment = "create_reservation.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    
    private static final String TAG_ITEMS = "items";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_FUNC = "subtitle";
    private static final String TAG_STATUS = "status";
  
    private static final String TAG_DATETIME = "datetime";
    private static final String TAG_DATE = "date";
    private static final String TAG_ORGANISATION = "organisation";
    private static final String TAG_SURNAME = "surname";
    private static final String TAG_LASTNAME = "lastname";
    private static final String TAG_FUNCTION = "function";
    private static final String TAG_PHONE = "phone";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_VISITKIND = "visit_kind";
    private static final String TAG_EMPLOYEE = "employee";
    private static final String TAG_PROJECT = "project";
    private static final String TAG_USER = "user";
    private static final String TAG_STARTTIME = "starttime";
    private static final String TAG_ENDTIME = "endtime";
    private static final String TAG_MINUTES = "hours";
    private static final String TAG_LOCATION = "location";
    private static final String TAG_ACTIVITY = "activity";
    private static final String TAG_DESCRIPTION = "description";
    
    private static final String SESSION = "bl123";
    
    
    public String last_error = "";
    // products JSONArray
 //   JSONArray items = null;
       
    public boolean json_connected = false;
    public boolean printer_connected = false;
    public boolean executing_print_job = false;
    
    String myUser = "";
    
    private String ip = "";
    private String android_id = "";
    private String appname = "";
    		
    
	public IntraDataProvider(String new_user)
	{
		myUser = new_user;
		
	}
	
	public void SetDeviceIdentification(String new_ip, String new_android_id, String new_appname)
	{
		ip = new_ip;
		appname = new_appname;
		android_id = new_android_id;
	
	}
	
	
	public boolean LogEvent(String event) 
	{
		 
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
		
		// get values from device
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_log, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public boolean CheckLogin(String login, String password)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", password));

        myUser = login;
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_login, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	return true;
            } else {
            	return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    
    }
	  
	
	// list of data elements, generic function
	public ArrayList<HashMap<String, String>> getDataList(String list_name, String user, boolean addNew)
	{
		String list_url = "";
		// select url
		if (list_name == "user_locations") list_url = url_get_user_locations;
		if (list_name == "activities") list_url = url_get_activities;
		if (list_name == "contacts") list_url = url_get_contacts;
		if (list_name == "projects") list_url = url_get_projects;
		if (list_name == "costplaces") list_url = url_get_costplaces;
		if (list_name == "news") list_url = url_get_news;
		if (list_name == "resources") list_url = url_get_resources;
		if (list_name == "messages") list_url = url_get_messages;
		if (list_name == "project_items") list_url = url_get_project_entries;
		if (list_name == "visits") list_url = url_get_visit_entries;
		if (list_name == "timerecords") list_url = url_get_time_records;
				
		// result list
		ArrayList<HashMap<String, String>> returnList = new ArrayList<HashMap<String, String>>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
        

        
        if(list_name == "visits")
        {
        	 params.add(new BasicNameValuePair("startdate", "2015-02-20"));
             params.add(new BasicNameValuePair("enddate", "2015-04-28"));
        }
       
        if(list_name == "timerecords")
        {
        	 Date d = new Date();
			 String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
			 
        	 params.add(new BasicNameValuePair("startdate", datetime));
             params.add(new BasicNameValuePair("enddate", datetime));
        }
               
        if (list_name == "project_items")  params.add(new BasicNameValuePair("projectid", user));
        // getting JSON string from URL
        JSONObject json = null;
        if (list_name == "visits")
        	json = jsonParser.makeHttpRequest(digivisitor_url + "/" + list_url, "GET", params);
        else
        	json = jsonParser.makeHttpRequest(website_url + "/" + list_url, "GET", params);
       
        if (json != null)
        {
            // Check your log cat for JSON reponse
            Log.d("All Items: ", json.toString());
 
            try {
            	
            	if (addNew)
            	{
		        	// add create new record
		            HashMap<String, String> fmap = new HashMap<String, String>();
		
		            // adding each child node to HashMap key => value
		            fmap.put(TAG_PID, "0");
		            fmap.put(TAG_NAME, "Add new item");
		            fmap.put(TAG_FUNC, "");
		            fmap.put(TAG_STATUS, "");
		             
		            // adding HashList to ArrayList
		            returnList.add(fmap);
		        }
                
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
         
                if (success == 1) {
                	
                	if (list_name == "timerecords")
                	{
                		JSONArray items = json.getJSONArray(TAG_ITEMS);
     
                        // looping through All Products
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject c = items.getJSONObject(i);
     
                            // Storing each json item in variable
                            String id = c.getString(TAG_PID);
                            String date = c.getString(TAG_DATE);
                            String starttime = c.getString(TAG_STARTTIME);
                            String endtime = c.getString(TAG_ENDTIME);
                            String minutes = c.getString(TAG_MINUTES);
                            String project = c.getString(TAG_PROJECT);
                            String location = c.getString(TAG_LOCATION);
                            String activity = c.getString(TAG_ACTIVITY);
                            String description = c.getString(TAG_DESCRIPTION);
                            String employee = c.getString(TAG_USER);
		                    
		                    // creating new HashMap
		                    HashMap<String, String> map = new HashMap<String, String>();
		
		                    // adding each child node to HashMap key => value
		                    map.put(TAG_PID, id);
		                    map.put(TAG_DATE, starttime.substring(0, 16));
		                    map.put(TAG_NAME, project);
	                        map.put(TAG_FUNC, activity + ": " + description);
	                        map.put(TAG_MINUTES, minutes);
	                        
		                     
				            // adding HashList to ArrayList
				            returnList.add(map);
		            	}
                	
                	}
                	
                	if (list_name == "visits")
                	{
                		// products found
                        // Getting Array of Products
                        JSONArray items = json.getJSONArray(TAG_ITEMS);
     
                        // looping through All Products
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject c = items.getJSONObject(i);
     
                            // Storing each json item in variable
                            String id = c.getString(TAG_PID);
                            String datetime = c.getString(TAG_DATETIME);
                            String organisation = c.getString(TAG_ORGANISATION);
                            String surname = c.getString(TAG_SURNAME);
                            String lastname = c.getString(TAG_LASTNAME);
                            String function = c.getString(TAG_FUNCTION);
                            String phone = c.getString(TAG_PHONE);
                            String email = c.getString(TAG_EMAIL);
                            String visit_kind = c.getString(TAG_VISITKIND);
                            String employee = c.getString(TAG_EMPLOYEE);

                		
	                        // creating new HashMap
	                        HashMap<String, String> map = new HashMap<String, String>();
	
	                        // adding each child node to HashMap key => value
	                        map.put(TAG_PID, id);
	                        map.put(TAG_DATETIME, datetime);
	                        map.put(TAG_ORGANISATION, organisation);
	                        map.put(TAG_SURNAME, surname);
	                        map.put(TAG_LASTNAME, lastname);
	                        map.put(TAG_FUNCTION, function);
	                        map.put(TAG_PHONE, phone);
	                        map.put(TAG_EMAIL, email);
	                        map.put(TAG_VISITKIND, visit_kind);
	                        map.put(TAG_EMPLOYEE, employee);
	    		             
	    		            // adding HashList to ArrayList
	    		            returnList.add(map);
                        }
                
                	}
                 	
                	else if (list_name == "resources" || list_name == "contacts" || list_name == "projects" || list_name == "user_locations" || list_name == "activities" || list_name == "costplaces") {
                    // products found
                    // Getting Array of Products
                    JSONArray items = json.getJSONArray(TAG_ITEMS);
 
                    // looping through All Products
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
 
                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
                     	String subtitle = "";
                    	String status = "";
                   
                        if (list_name == "user_locations" || list_name == "activities" || list_name == "costplaces")
                        {
                        } else {
                        	subtitle = c.getString(TAG_FUNC);
                        	status = c.getString(TAG_STATUS);
                        }
                        
                        // specific formatting
                        if (list_name == "news")
                        {
                        	if (subtitle.length() > 50)
                        	{
                        		subtitle = subtitle.substring(0, 50) + "...";
                        	}
                        }
                        if (list_name == "messages")
                        {
                        	if (name.length() > 30)
                        	{
                        		name = name.substring(0, 30) + "...";
                        	}
                        }
                        
                        if (list_name == "news" || list_name == "messages" || list_name == "project_items")
                        {
                        	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                        	SimpleDateFormat format = new SimpleDateFormat("HH:mm");  
                        	
                        	try {  
                        	    Date date = json_format.parse(status);
                        	    
                        	    Calendar cal = Calendar.getInstance();
                        	    cal.set(Calendar.HOUR_OF_DAY, 0);
                        	    cal.set(Calendar.MINUTE, 0);
                        	    Date today  = cal.getTime();
                        	    
                        	    if (date.before(today))
                        	    {
                        	    	format = new SimpleDateFormat("dd-MM-yyyy");  
                        	    } 
                        		status = format.format(date);
                        	      
                        	} catch (ParseException e) {  
                        	    // TODO Auto-generated catch block  
                        	    e.printStackTrace();  
                        	}
                        	
                        
                        }
                        
                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();
 
                        // adding each child node to HashMap key => value
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_FUNC, subtitle);
                        map.put(TAG_STATUS, status);
                        
                         
                        // adding HashList to ArrayList
                        returnList.add(map);
                    	}
                    }
                    
                    json_connected = true;
                    
                } else {
                    // no products found
                	json_connected = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        	
            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();

            // adding each child node to HashMap key => value
            map.put(TAG_PID, "1");
            map.put(TAG_NAME, "Test item");
            map.put(TAG_FUNC, "");
            map.put(TAG_STATUS, "");

            // adding HashList to ArrayList
            returnList.add(map);
        }
        
        // free mem
        json = null;
        
	     
        return returnList;
	}
	
	public ArrayList<HashMap<String, String>> getTimeRecordList(String date_start, String date_end, String user, boolean addNew)
	{
		String list_url = "";
		list_url = url_get_time_records;
				
		// result list
		ArrayList<HashMap<String, String>> returnList = new ArrayList<HashMap<String, String>>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
        
        Date d = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);

        if (date_start != "")
        {
        	datetime = date_start;
        }
        
        params.add(new BasicNameValuePair("startdate", datetime));
        params.add(new BasicNameValuePair("enddate", datetime));
        params.add(new BasicNameValuePair("report_user", user));
        
         // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + list_url, "GET", params);
       
        if (json != null)
        {
            // Check your log cat for JSON reponse
            Log.d("All Items: ", json.toString());
 
            try {
            	
            	if (addNew)
            	{
		        	// add create new record
		            HashMap<String, String> fmap = new HashMap<String, String>();
		
		            // adding each child node to HashMap key => value
		            fmap.put(TAG_PID, "0");
		            fmap.put(TAG_NAME, "Add new item");
		            fmap.put(TAG_FUNC, "");
		            fmap.put(TAG_STATUS, "");
		             
		            // adding HashList to ArrayList
		            returnList.add(fmap);
		        }
            	
                
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
                
                if (success == 1) {
                		
               		JSONArray items = json.getJSONArray(TAG_ITEMS);
     
                    // looping through All Products
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
 
                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String date = c.getString(TAG_DATE);
                        String starttime = c.getString(TAG_STARTTIME);
                        String endtime = c.getString(TAG_ENDTIME);
                        String minutes = c.getString(TAG_MINUTES);
                        String project = c.getString(TAG_PROJECT);
                        String location = c.getString(TAG_LOCATION);
                        String activity = c.getString(TAG_ACTIVITY);
                        String description = c.getString(TAG_DESCRIPTION);
                        String employee = c.getString(TAG_USER);
	                    
	                    // creating new HashMap
	                    HashMap<String, String> map = new HashMap<String, String>();
	
	                    // adding each child node to HashMap key => value
	                    map.put(TAG_PID, id);
	                    map.put(TAG_DATE, starttime.substring(0, 16));
	                    map.put(TAG_NAME, project);
                        map.put(TAG_FUNC, activity + ": " + description);
                        map.put(TAG_MINUTES, minutes);
                        
	                     
			            // adding HashList to ArrayList
			            returnList.add(map);
	            	}
            	
                    json_connected = true;
                    
                } else {
                    // no products found
                	json_connected = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        	
            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();

            // adding each child node to HashMap key => value
            map.put(TAG_PID, "1");
            map.put(TAG_NAME, "Test item");
            map.put(TAG_FUNC, "");
            map.put(TAG_STATUS, "");

            // adding HashList to ArrayList
            returnList.add(map);
        }
        
        // free mem
        json = null;
        
	     
        return returnList;
	}

	// General Method for calling new web service
	// gets a list of json items in array
	// when something goes wrong, an empty array is returned
	private JSONArray CallWebService(List<NameValuePair> new_params, String url)
	{
		// clean variables
		last_error = "";
		json_connected = true;
			
		 // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
        
        for (NameValuePair p : new_params) {
        	params.add(p);
        }
       
    	// getting JSON Object
    	// Note that create product url accepts POST method
    	JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);
    
     	JSONArray items = new JSONArray(); 
     	
    	 if (json != null) 
    	 {    		 
			 try {
				 
				items = json.optJSONArray("items");
			    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
			
		      	int success = json.getInt(TAG_SUCCESS);
	
		        if (success == 1)
		        {
		         	if (items == null) items = new JSONArray(); 
		        } else {
		        	// record error
		        	items = new JSONArray(); 
		        	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
		        }
		     } catch (JSONException e) {
		         e.printStackTrace();
		        
		     }
    	            
    	 } else {
    		 
    		 last_error = "Web service not found, please check WIFI/Network connection.";
    		 json_connected = false;
    	 
    	 }
    	 return items;
    
	}
	
	// contact detail
	// first implementation of new webservice. 
	// call with contact id
	// always returns an object
	// of object is empty => check last_error
	
	public cContact getContact(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("contactid", id));
        params.add(new BasicNameValuePair("object", "contact"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new);
             
        cContact myContact = new cContact();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		    		                    
		    	myContact.id = c.getString("id");
		    	myContact.username = c.getString("username");
		    	myContact.surname = c.getString("surname");
		    	myContact.lastname = c.getString("lastname");
		    	myContact.login = c.getString("login");
		    	myContact.picture = c.getString("picture");
		    	myContact.auth_level = c.getInt("auth_level");
		    	myContact.function = c.getString("function");
		    	myContact.phone = c.getString("phone");
		    	myContact.email = c.getString("email");
		    	myContact.status_id = c.getString("status");
		    	myContact.description = c.getString("description");
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return myContact;

	}

	public List<cContact> getContacts()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new);
             
    	// result list
		List<cContact> returnList = new ArrayList<cContact>();
	
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("pid");
    		    	myContact.name = c.getString("name");
    		    	myContact.function = c.getString("subtitle");
    		    	myContact.phone = c.getString("phone");
    		    	myContact.email = c.getString("email");
    		    	myContact.status_id = c.getString("status");
    		    	
     		    	returnList.add(myContact);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	
	// time record detail
	public cTimeRecord getTimeRecord(String id, boolean latest)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
        params.add(new BasicNameValuePair("recordid", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
    	JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_timerecord, "GET", params);
        if (latest == true)
        	json = jsonParser.makeHttpRequest(website_url + "/" + url_get_last_timerecord, "GET", params);
        	
        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cTimeRecord myRecord = new cTimeRecord();
            	myRecord.id = c.getString("pid");
            	myRecord.user = c.getString("user");
            	myRecord.project = c.getString("project");
            	myRecord.date = c.getString("date");
            	myRecord.starttime = c.getString("starttime");
            	myRecord.endtime = c.getString("endtime");
            	myRecord.minutes = c.getInt("minutes");
            	myRecord.location = c.getString("location");
            	myRecord.activity = c.getString("activity");
            	myRecord.costplace = c.getString("costplace");
            	myRecord.description = c.getString("description");
            	
            	return myRecord;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
		
	
	// project detail
	public cProject getProject(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
        params.add(new BasicNameValuePair("projectid", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_project, "GET", params);

        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cProject myProject = new cProject();
            	myProject.id = c.getLong("id");
            	myProject.name = c.getString("name");
            	myProject.code = c.getString("code");
            	myProject.created = c.getString("created");
            	myProject.status = c.getString("status");
             	myProject.description = c.getString("description");
             	myProject.status = c.getString("status");
             	myProject.owner = c.getString("owner");
             	myProject.ETC = c.getInt("ETC");
                                	
            	
            	return myProject;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
		
	// news detail
	public cNews getNewsItem(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("newsid", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_news_item, "GET", params);

        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cNews myNews = new cNews();
            	myNews.id = c.getLong("id");
            	myNews.title = c.getString("title");
            	myNews.description = c.getString("description");
            	myNews.created = c.getString("created");
            	myNews.owner = c.getString("owner");
            	myNews.picture = c.getString("picture");
            	
            	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat format = new SimpleDateFormat("HH:mm");  
            	
            	try {  
            	    Date date = json_format.parse(myNews.created);  
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (date.before(today))
            	    {
            	    	format = new SimpleDateFormat("dd-MM-yyyy");  
            	    } 
            	    myNews.created = format.format(date);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
            	
            	return myNews;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
		
	// resource detail
	public cResource getResource(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("resourceid", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_resource, "GET", params);

        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cResource myResource = new cResource();
            	myResource.id = c.getString("id");
            	myResource.name = c.getString("name");
            	myResource.code = c.getString("code");
            	myResource.status = c.getString("status");
            	myResource.description = c.getString("description");
            	myResource.InUse = c.getBoolean("inuse");
            	myResource.User = c.getString("user");
            	
            	return myResource;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
		
	// company info
	public cCompany getCompanyInfo(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("id", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_company, "GET", params);

        // check for success tag
        try {
          	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cCompany myCompany = new cCompany();
            	myCompany.id = c.getLong("id");
            	myCompany.name = c.getString("name");
            	myCompany.address = c.getString("address");
            	myCompany.postcode = c.getString("postcode");
            	myCompany.city = c.getString("city");
            	myCompany.address_mail = c.getString("address_mail");
            	myCompany.postcode_mail = c.getString("postcode_mail");
            	myCompany.city_mail = c.getString("city_mail");
            	myCompany.country = c.getString("country");
            	myCompany.phone = c.getString("phone");
            	myCompany.fax = c.getString("fax");
            	myCompany.website = c.getString("website");
            	myCompany.email = c.getString("email");
            	myCompany.picture = c.getString("picture");
            	
            	return myCompany;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
	
	// get message
	public cMessage getMessage(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("id", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_message, "GET", params);

        // check for success tag
        try {
          	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            	JSONArray items = json.getJSONArray("item");
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	cMessage myMessage = new cMessage();
            	myMessage.id = c.getLong("id");
            	myMessage.user_from = c.getString("messagefrom");
            	myMessage.datetime = c.getString("created");
            	myMessage.message = c.getString("description");
            	
            	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat format = new SimpleDateFormat("HH:mm");  
            	
            	try {  
            	    Date date = json_format.parse(myMessage.datetime);  
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (date.before(today))
            	    {
            	    	format = new SimpleDateFormat("dd-MM-yyyy");  
            	    } 
            	    myMessage.datetime = format.format(date);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
            	
               	return myMessage;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}
	
	// create message
	public boolean createMessage(String user, String message)
	{
		String from = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
	    
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("from", from));
        params.add(new BasicNameValuePair("message", message));
        params.add(new BasicNameValuePair("date", datetime));
      
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_message, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
		
	}
	
	// create project item
	public boolean createProjectItem(String project, String text, String picture)
	{
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
	    
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("project", project));
        params.add(new BasicNameValuePair("entry", text));
        params.add(new BasicNameValuePair("datetime", datetime));
      
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_project_item, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
		
	}
	
	// create a holiday
	public boolean createHoliday(String startdate, String enddate)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("startdate", startdate));
        params.add(new BasicNameValuePair("enddate", enddate));
             
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_holiday, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
		
	}
	
	// create a sick day
	public boolean createSickDay(String date, String text)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("description", text));
             
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_sickday, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	// end sick day
	public boolean endSickDay(String date, String text)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("description", text));
             
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_end_sickday, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	// create time record
	public boolean createTimeRecord(String date, String starttime, String endtime, String project, String location, String activity, String costplace, String description)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("starttime", starttime));
        params.add(new BasicNameValuePair("endtime", endtime));
        params.add(new BasicNameValuePair("project", project));
        params.add(new BasicNameValuePair("location", location));
        params.add(new BasicNameValuePair("activity", activity));
        params.add(new BasicNameValuePair("costplace", costplace));
        params.add(new BasicNameValuePair("description", description));
        
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_create_time_record, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	// delete methods
	// delete time record
	public boolean deleteTimeRecord(String id)
	{
		String user = myUser;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("recordid", id));
             
        // getting JSON Object
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_delete_timerecord, "GET", params);

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);
         
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	// visit detail
	public cVisitEntries getVisit(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("visitid", id));
        params.add(new BasicNameValuePair("startdate", "2015-02-20"));
        params.add(new BasicNameValuePair("enddate", "2015-04-28"));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_get_visit_entries, "GET", params);

        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
            //	JSONArray items = json.getJSONArray("item");
            		             	
            	  // Getting Array of Products
                JSONArray items = json.getJSONArray(TAG_ITEMS);

                cVisitEntries myVisit = new cVisitEntries();
                
                // looping through All Products
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    
            	//JSONObject c = items.getJSONObject(0);
            	    
                    String visitid = c.getString("pid");
                    if (visitid == id)
                    {
		            	
		            	myVisit.id = c.getString("pid");
		            	myVisit.startdatetime = c.getString("startdatetime");
		            	myVisit.enddatetime = c.getString("enddatetime");
		            	myVisit.allday = c.getString("allday");
		            	myVisit.organisation = c.getString("organisation");
		            	myVisit.surname = c.getString("surname");
		            	myVisit.lastname = c.getString("lastname");
		            	myVisit.function = c.getString("function");
		            	myVisit.phone = c.getString("phone");
		            	myVisit.email = c.getString("email");
		            	myVisit.visit_kind = c.getString("visit_kind");
		            	myVisit.employee = c.getString("employee");
                    }
                }
            	
            	return myVisit;
            } else {
            	return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
	}


	// get user roles
	public ArrayList<String> getUserRoles(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        String user = myUser;
		
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", SESSION));
     
        params.add(new BasicNameValuePair("role_user", id));
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_get_user_roles, "GET", params);

        ArrayList<String> roles = new ArrayList<String>();
        
        // check for success tag
        try {
            // Check your log cat for JSON reponse
            Log.d("All items: ", json.toString());
         	
        	int success = json.getInt(TAG_SUCCESS);

            if (success == 1)
            {
                        		             	
                // Getting Array of Products
                JSONArray items = json.getJSONArray(TAG_ITEMS);

                // looping through All Products
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                	                    
		        	String str_name = c.getString("name");
		        	
		        	roles.add(str_name);
		        }
                
                json_connected = true;
                
            	return roles;
            } else {
            	
                json_connected = false;
            	return null;
            }
        } catch (JSONException e) {
            json_connected = false;
            e.printStackTrace();
            return null;
        }
	}
	
	
	// digivistor functions from DataProvider
	// ===============================================================================================================================

				
		public cVisitor getLatestVisit(String myOrganisation, String mySurname, String myLastName)
		{
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     	        
	        params.add(new BasicNameValuePair("organisation", myOrganisation));
	        params.add(new BasicNameValuePair("lastname", myLastName));
	        params.add(new BasicNameValuePair("lastname", myLastName));
	                
	        cVisitor myVisit = new cVisitor();
	                
	        // getting JSON string from URL
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_latest_visit, "GET", params);
	 	   
	        if (json != null)
	        {
	        	   try {
	                   // Checking for SUCCESS TAG
	                   int success = json.getInt(TAG_SUCCESS);
	    
	                   if (success == 1) {
	                       // products found
	                       // Getting Array of Products
	                		JSONArray items = json.getJSONArray("item");
			             	
	                    	JSONObject c = items.getJSONObject(0);
	                   
	                         // create object 
	                         cVisitor myVisitor = new cVisitor();
	                           
	                         // Storing each json item in variable
	                         myVisitor.id = c.getInt(TAG_PID);
	                         myVisitor.created = c.getString("created");
	                         myVisitor.organisation = c.getString("organisation");
	                         myVisitor.surname = c.getString("surname");
	                         myVisitor.lastname = c.getString("lastname");
	                         myVisitor.function = c.getString("function");
	                         myVisitor.phone = c.getString("phone");
	                         myVisitor.email = c.getString("email");
	                         myVisitor.visit_kind = c.getString("visit_kind");
	                         myVisitor.employee = c.getString("employee");
	                      
	                         myVisit = myVisitor;
	                                             
	                         json_connected = true;
	                       
	                   } else {
	                       // no items found
	                   	json_connected = true;
	                   }
	               } catch (JSONException e) {
	                   e.printStackTrace();
	                   json_connected = false;
	               }
	           } else {
	           	 json_connected = false;
	           }
	           
	           // free mem
	           json = null;
	           
	           return myVisit;
		}
		
		
		// get visitors
		// list of data elements, generic function
		public List<cVisitor> getVisitorList(String user, String organisation, String lastname)
		{
			String list_url = url_get_visitors;
			
			// result list
			List<cVisitor> returnList = new ArrayList<cVisitor>();
			
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("lastname", lastname));
	        
	                
	        // getting JSON string from URL
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + list_url, "GET", params);
	 	   
	        if (json != null)
	        {
	            // Check your log cat for JSON reponse
	            Log.d("All Items: ", json.toString());
	 
	            try {
	                // Checking for SUCCESS TAG
	                int success = json.getInt(TAG_SUCCESS);
	 
	              if (success == 1) {
	                    // products found
	                    // Getting Array of Products
	                    JSONArray items = json.getJSONArray(TAG_ITEMS);
	 
	                    // looping through All Products
	                    for (int i = 0; i < items.length(); i++) {
	                        JSONObject c = items.getJSONObject(i);
	 
	                        // create object 
	                        cVisitor myVisitor = new cVisitor();
	                        
	                        // Storing each json item in variable
	                        myVisitor.id = c.getInt(TAG_PID);
	                        myVisitor.created = c.getString("created");
	                        myVisitor.organisation = c.getString("organisation");
	                        myVisitor.surname = c.getString("surname");
	                        myVisitor.lastname = c.getString("lastname");
	                        myVisitor.function = c.getString("function");
	                        myVisitor.phone = c.getString("phone");
	                        myVisitor.email = c.getString("email");
	                        myVisitor.visit_kind = c.getString("visit_kind");
	                        myVisitor.employee = c.getString("employee");
	                                                
	                        // adding HashList to ArrayList
	                        returnList.add(myVisitor);
	                    }
	                    
	                    json_connected = true;
	                    
	                } else {
	                    // no items found
	                	json_connected = true;
	                }
	            } catch (JSONException e) {
	                e.printStackTrace();
	                json_connected = false;
	            }
	        } else {
	        	 json_connected = false;
	        }
	        
	        // free mem
	        json = null;
	       // jsonParser = null;
		     
	        return returnList;
		}
		
		// user info
		public cContact getUserInfo(String organisation, String name)
		{
			 // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        String surname = "";
	        String lastname = "";
	        String[] wname = name.split(" ");
	    	surname = wname[0];
	        if (wname.length > 2)
	        {
	        	for(int i=1;i< wname.length ;i++)
	        	{
	        		lastname = lastname + wname[i] + " ";
	        	}
	        } else {
	        	lastname = wname[1];
	        }
	        
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("surname", surname));
	        params.add(new BasicNameValuePair("lastname", lastname));
	        
	        // getting JSON Object
	        // Note that create product url accepts POST method
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_get_user, "GET", params);

	        // check for success tag
	        try {
	          	
	        	int success = json.getInt(TAG_SUCCESS);

	            if (success == 1)
	            {
	            	JSONArray items = json.getJSONArray("item");
	            		             	
	            	JSONObject c = items.getJSONObject(0);
	            		                    
	            	cContact myUser = new cContact();
	            	myUser.id = c.getString("id");
	            	myUser.username = c.getString("username");
	            	myUser.surname = c.getString("surname");
	            	myUser.lastname = c.getString("lastname");
	            	myUser.login = c.getString("login");
	            	myUser.auth_level = c.getInt("auth_level");
	            	myUser.function = c.getString("function");
	            	myUser.phone = c.getString("phone");
	            	myUser.email = c.getString("email");
	            	myUser.picture = c.getString("picture");
	            	myUser.status_id = c.getString("status");
	            	myUser.description = c.getString("description");
	            	
	            	json_connected = true;
	            	
	            	 // free mem
	                json = null;
	            	
	            	return myUser;
	            } else {
	            	json_connected = false;
	            	  
	            	 // free mem
	                json = null;
	                
	            	return null;
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            // free mem
	            json = null;
	            
	            return null;
	        }
		}
			
		
		// create visit entry item
		public boolean createVisitEntry(String startdatetime,String enddatetime, String allday, String organisation, String surname, String lastname, String phone, String email, String function, String visit_kind, String employee, String co_workers)
		{
			String user = myUser;
			
	        // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("startdatetime", startdatetime));
		    params.add(new BasicNameValuePair("enddatetime", enddatetime));
		    params.add(new BasicNameValuePair("allday", allday));
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("surname", surname));
	        params.add(new BasicNameValuePair("lastname", lastname));
	        params.add(new BasicNameValuePair("phone", phone));
	        params.add(new BasicNameValuePair("email", email));
	        params.add(new BasicNameValuePair("function", function));
	        params.add(new BasicNameValuePair("visit_kind", visit_kind));
	        params.add(new BasicNameValuePair("employee", employee));
	        params.add(new BasicNameValuePair("co-workers", co_workers));
	      
	        // getting JSON Object
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_create_visit_entry, "GET", params);

	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	         
	            json_connected = true;
	            // free mem
	            json = null;
	            
	            return true;
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            json_connected = false;
	            // free mem
	            json = null;
	            
	            return false;
	        }
			
		}
		
		public boolean updateVisitEntry(String visit_id, String organisation, String surname, String lastname, String phone, String email, String function, String visit_kind, String employee, String co_worker)
		{
			String user = myUser;
			String datetime = "";
			
			try
	 	    {
	 	    	 Date d = new Date();
				 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
	 	    } catch (Exception e)
	 	    { 	    
	 	    }
		    
	        // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("datetime", datetime));
	        params.add(new BasicNameValuePair("visit_id", visit_id));
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("surname", surname));
	        params.add(new BasicNameValuePair("lastname", lastname));
	        params.add(new BasicNameValuePair("phone", phone));
	        params.add(new BasicNameValuePair("email", email));
	        params.add(new BasicNameValuePair("function", function));
	        params.add(new BasicNameValuePair("visit_kind", visit_kind));
	        params.add(new BasicNameValuePair("employee", employee));
	        params.add(new BasicNameValuePair("co-worker", co_worker));
	        
	        // getting JSON Object
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_update_visit_entry, "GET", params);

	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	         
	            json_connected = true;
	            // free mem
	            json = null;
	            
	            return true;
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            json_connected = false;
	            // free mem
	            json = null;
	            
	            return false;
	        }
			
		}
			
		public boolean createEndVisitEntry(String organisation, String surname, String lastname)
		{
			String user = myUser;
			String datetime = "";
			
			try
	 	    {
	 	    	 Date d = new Date();
				 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
	 	    } catch (Exception e)
	 	    { 	    
	 	    }
		    
	        // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("surname", surname));
	        params.add(new BasicNameValuePair("lastname", lastname));
	        
	        // getting JSON Object
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_create_end_visit_entry, "GET", params);

	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	         
	            json_connected = true;
	            // free mem
	            json = null;
	            
	            return true;
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            json_connected = false;
	            // free mem
	            json = null;
	            
	            return false;
	        }
			
		}
			
			
			
		public boolean SendNotification(String phoneNo, String sms)
		{
			  try {
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(phoneNo, null, sms, null, null);
				return true;
			  } catch (Exception e) {
				e.printStackTrace();
				return false;
			  }
		}
		
		// DYMO proxy server interface methods
		
		public boolean CreatPrintJob(String user, String organisation, String visitor, String visitor_organisation)
	    {
			
			executing_print_job = true;
			
	        // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("organisation", organisation));
	        params.add(new BasicNameValuePair("visitor", visitor));
	        params.add(new BasicNameValuePair("visitor_organisation", visitor_organisation));
	        

	        // getting JSON Object
	        // Note that create product url accepts POST method
	    //    JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_create_printjob, "GET", params);
	        JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_create_printjob, "GET", params);

	        printer_connected = false;
	        
	        if (json != null)
	        {
			    // check for success tag
			    try {
			        int success = json.getInt(TAG_SUCCESS);
			
			        if (success == 1)
			        {
			        	printer_connected = true;
			        } 
			    } catch (JSONException e) {
			        e.printStackTrace();
			    }
	        }
	        
	        // free mem
	        json = null;
	        
	        executing_print_job = false;
	        
	        return printer_connected;
	    }
		
		// list of printjobs
		public List<cPrintJob> getPrintJobs()
		{
				List<cPrintJob> myPrintJobs = null;
				cPrintJob myPrintJob = null;
				
				// Building Parameters
		        List<NameValuePair> params = new ArrayList<NameValuePair>();
		        params.add(new BasicNameValuePair("user", myUser));
		        params.add(new BasicNameValuePair("token", SESSION));
		        
		        // getting JSON string from URL
		        JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_get_printjobs, "GET", params);
		 	   
		        if (json != null)
		        {
		           
		            try {
		            	JSONObject c = new JSONObject(json.toString());
		 
		            	 myPrintJob = new cPrintJob();
		         		
		            	 myPrintJob.id = c.getInt("Id");
		            	 myPrintJob.name = c.getString("Name");
		            	 myPrintJob.created = c.getString("datetime");
		            	 myPrintJob.organisation = c.getString("organisation");
		            	 myPrintJob.visitor = c.getString("visitor");
		            	 myPrintJob.visitor_organisation = c.getString("visitor_organisation");
		            	 
		            	 myPrintJobs.add(myPrintJob);
		            	 
		            } catch (JSONException e) {
		                e.printStackTrace();
		            }
		        } else {
		        
		        }
		        
		        // free mem
		        json = null;
		         
		        return myPrintJobs;
		}
		
			
			
		// get rooms available
		// list of data elements, generic function
		public List<cRoomAvailable> getRoomAvailableList(String name, String datetime_start, String datetime_end)
		{
			//String list_url = "http://www.sharethatdata.com/digivisitor/get_free_resources.php?user=mverbeek&token=secret&startdate=2015-01-01%2010:00&enddate=2015-01-01%2011:00";
			
			
			// result list
			List<cRoomAvailable> returnList = new ArrayList<cRoomAvailable>();
			
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("name", name));
	        
	        Date d = new Date();
			String datetime_S = new SimpleDateFormat("yyyy-MM-dd").format(d);
			String datetime_E = new SimpleDateFormat("yyyy-MM-dd").format(d);
			
	        if (datetime_start != "")
	        {
	        	datetime_S = datetime_start;
	        }
	        if (datetime_end != "")
	        {
	        	datetime_E = datetime_end;
	        }
	        	        
	        params.add(new BasicNameValuePair("startdate", datetime_S));
	        params.add(new BasicNameValuePair("enddate", datetime_E));
	        
            JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + "get_free_resources.php", "GET", params);
	        
	        if (json != null)
	        {
	            // Check your log cat for JSON reponse
	            Log.d("All Items: ", json.toString());
	 
	            try {
	                // Checking for SUCCESS TAG
	                int success = json.getInt(TAG_SUCCESS);
	 
	              if (success == 1) {
	                    // products found
	                    // Getting Array of Products
	                    JSONArray items = json.getJSONArray(TAG_ITEMS);
	 
	                    // looping through All Products
	                    for (int i = 0; i < items.length(); i++) {
	                        JSONObject c = items.getJSONObject(i);
	 
	                        // create object 
	                        cRoomAvailable myRoom = new cRoomAvailable();
	                        
	                        // Storing each json item in variable
	                        myRoom.id = c.getInt(TAG_PID);
	                        myRoom.name = c.getString("name");
	                        myRoom.code = c.getString("code");
	                                                
	                        // adding HashList to ArrayList
	                        returnList.add(myRoom);
	                    }
	                    
	                    json_connected = true;
	                    
	                } else {
	                    // no items found
	                	json_connected = true;
	                }
	            } catch (JSONException e) {
	                e.printStackTrace();
	                json_connected = false;
	            }
	        } else {
	        	 json_connected = false;
	        }
	        
	        // free mem
	        json = null;
	       // jsonParser = null;
		     
	        return returnList;      
	      
		}
		
		
		// get employees available
		// list of data elements, generic function
		public List<cEmployee> getEmployeeAvailableList(String name)
		{
			// result list
			List<cEmployee> returnList = new ArrayList<cEmployee>();
			
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("name", name));
	        
// MV 22-2-2015
// add current date as parameter		                
	        // getting JSON string from URL
	       JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + "get_contacts.php", "GET", params);

	        if (json != null)
	        {
	            // Check your log cat for JSON reponse
	            Log.d("All Items: ", json.toString());
	 
	            try {
	                // Checking for SUCCESS TAG
	                int success = json.getInt(TAG_SUCCESS);
	 
	              if (success == 1) {
	                    // products found
	                    // Getting Array of Products
	                    JSONArray items = json.getJSONArray(TAG_ITEMS);
	 
	                    // looping through All Products
	                    for (int i = 0; i < items.length(); i++) {
	                        JSONObject c = items.getJSONObject(i);
	 
	                        // create object 
	                        cEmployee myEmployee = new cEmployee();
	                        
	                        // Storing each json item in variable
	                        myEmployee.id = c.getInt(TAG_PID);
	                        myEmployee.name = c.getString("name");
	                        myEmployee.subtitle = c.getString("subtitle");
	                        myEmployee.status = c.getString("status");
	                        myEmployee.phone = c.getString("phone");
	                        myEmployee.email = c.getString("email");
	                                                
	                        // adding HashList to ArrayList
	                        returnList.add(myEmployee);
	                    }
	                    
	                    json_connected = true;
	                    
	                } else {
	                    // no items found
	                	json_connected = true;
	                }
	            } catch (JSONException e) {
	                e.printStackTrace();
	                json_connected = false;
	            }
	        } else {
	        	 json_connected = false;
	        }
	        
	        // free mem
	        json = null;
	        
	        return returnList;      
	      
		}
		
		
		// add members to meeting room reservation
		public boolean create_reservation_member(String dateStartSchedule, String dateEndSchedule, String roomSchedule)//
		{
			//String url_create_room_appointment = "http://www.sharethatdata.com/digivisitor/create_reservation.php?user=mverbeek&startdate=2015-01-01%2010:00&enddate=2015-01-01%2011:00&resource=1";
			String user = myUser;			
			
	        // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", SESSION));
	     
	        params.add(new BasicNameValuePair("startdate", dateStartSchedule));
	        params.add(new BasicNameValuePair("enddate", dateEndSchedule));
	       // params.add(new BasicNameValuePair("startdate", "2015-02-17 15:24:13"));
           // params.add(new BasicNameValuePair("enddate", "2015-02-17 16:24:13"));
//				        params.add(new BasicNameValuePair("startdate", timeStartSchedule));
//				        params.add(new BasicNameValuePair("enddate", timeEndSchedule));
	        params.add(new BasicNameValuePair("resource", roomSchedule));
	        //params.add(new BasicNameValuePair("co-workers", "marko"));
	      
	        // getting JSON Object
	        JSONObject json = jsonParser.makeHttpRequest(digivisitor_url + "/" + url_create_room_appointment, "GET", params);

	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	         
	            json_connected = true;
	            // free mem
	            json = null;
	            
	            return true;
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            json_connected = false;
	            // free mem
	            json = null;
	            
	            return false;
	        }
			
		}

	
}
