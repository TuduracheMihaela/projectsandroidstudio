package com.sharethatdata.webservice;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


public class ImageProcessor {
	
	private String webservice_url = "http://newtonrp-ws.sharethatdata.com";
	private String upLoadServerUri = webservice_url + "/upload_picture.php";
	
    private Context myContext = null;
	private int serverResponseCode = 0;
	   	
	public ImageProcessor(Context new_context)
	{
		myContext = new_context;
 	}
	
	public void SetWebServiceUrl(String new_url)
	{
		if (new_url != "") {
			webservice_url = new_url;
			upLoadServerUri = webservice_url + "/upload_picture.php";
		}
	}
	
	public Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
	    if (maxHeight > 0 && maxWidth > 0) {
	        int width = image.getWidth();
	        int height = image.getHeight();
	        float ratioBitmap = (float) width / (float) height;
	        float ratioMax = (float) maxWidth / (float) maxHeight;

	        int finalWidth = maxWidth;
	        int finalHeight = maxHeight;
	        if (ratioMax > 1) {
	            finalWidth = (int) ((float)maxHeight * ratioBitmap);
	        } else {
	            finalHeight = (int) ((float)maxWidth / ratioBitmap);
	        }
	        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
	        return image;
	    } else {
	        return image;
	    }
	}
	
	public String getImageURL(String id, String section)
	{
		String ret_url = webservice_url + "/art/" + section + "/" + id;
	    ret_url = ret_url + "_small.jpg";
	    if (section == "messages")
	    {
	    	 ret_url = webservice_url + "/art/message_small.jpg";
	    }
		return ret_url;
	}
	
	public String getFullImageURL(String id, String section)
	{
		String ret_url = webservice_url + "/art/" + section + "/" + id;
	    ret_url = ret_url + ".jpg";
	      
		return ret_url;
	}
	
	public String UploadFile(String filename)
	{
	     HttpURLConnection conn = null;
         DataOutputStream dos = null;  
         String lineEnd = "\r\n";
         String twoHyphens = "--";
         String boundary = "*****";
         int bytesRead, bytesAvailable, bufferSize;
         byte[] buffer;
         int maxBufferSize = 2 * 1024 * 1024; 
         File sourceFile = new File(filename); 
         String returnMessage = "";
                  
         if (!sourceFile.isFile()) {

              Log.e("uploadFile", "Source File not exist :" + filename);

              returnMessage = "Source File not exist :" + filename;
              
              return returnMessage;

         }
         else
         {
              try { 
           	   
                  // open a URL connection to the Servlet
                  FileInputStream fileInputStream = new FileInputStream(sourceFile);
                  URL url = new URL(upLoadServerUri);

                  // Open a HTTP  connection to  the URL
                  conn = (HttpURLConnection) url.openConnection(); 
                  conn.setDoInput(true); // Allow Inputs
                  conn.setDoOutput(true); // Allow Outputs
                  conn.setUseCaches(false); // Don't use a Cached Copy
                  conn.setRequestMethod("POST");
                  conn.setRequestProperty("Connection", "Keep-Alive");
                  conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                  conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                  conn.setRequestProperty("uploaded_file", filename); 

                  dos = new DataOutputStream(conn.getOutputStream());

                  dos.writeBytes(twoHyphens + boundary + lineEnd); 
                  dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                            + filename + "\"" + lineEnd);

                  dos.writeBytes(lineEnd);

                  // create a buffer of  maximum size
                  bytesAvailable = fileInputStream.available(); 

                  bufferSize = Math.min(bytesAvailable, maxBufferSize);
                  buffer = new byte[bufferSize];

                  // read file and write it into form...
                  bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

                  while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);   

                   }

                  // send multipart form data necesssary after file data...
                  dos.writeBytes(lineEnd);
                  dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                  // Responses from the server (code and message)
                  serverResponseCode = conn.getResponseCode();
                  String serverResponseMessage = conn.getResponseMessage();

                  Log.i("uploadFile", "HTTP Response is : "
                          + serverResponseMessage + ": " + serverResponseCode);

                  if(serverResponseCode == 200){

                       String msg = "File Upload Completed.\n\n See uploaded file here : \n\n";
         //              Toast.makeText(myContext, "File Upload Complete.", Toast.LENGTH_SHORT).show();
                       returnMessage = "";
                  }    

                  //close the streams //
                  fileInputStream.close();
                  dos.flush();
                  dos.close();

             } catch (MalformedURLException ex) {
                 ex.printStackTrace();

                  returnMessage = "MalformedURLException Exception : check script url.";
           //       Toast.makeText(myContext, "MalformedURLException", Toast.LENGTH_SHORT).show();
                     
                 Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
             } catch (Exception e) {
                 e.printStackTrace();

                 returnMessage = "Got Exception : see logcat ";
          //       Toast.makeText(myContext, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
                 Log.e("Upload file to server Exception", "Exception : "  + e.getMessage(), e);  
             }
   
              return returnMessage;
          } // End else block 
    
	
	}
	
	public String SaveImageToFile(Bitmap img, String filename)
	{
		String ret_path = "";
	    try {
            
            // Getting Caching directory
            File cacheDirectory = myContext.getCacheDir();

            // Temporary file to store the downloaded image
            File tmpFile = new File(cacheDirectory.getPath() + "/" + filename);

            ret_path = cacheDirectory.getPath() + "/" + filename;
            
            // The FileOutputStream to the temporary file
            FileOutputStream fOutStream = new FileOutputStream(tmpFile);

            // Writing the bitmap to the temporary file as png file
            img.compress(Bitmap.CompressFormat.JPEG,100, fOutStream);
	    
            // Flush the FileOutputStream
            fOutStream.flush();

            //Close the FileOutputStream
            fOutStream.close();

            return ret_path;
	    }catch (Exception e) {
            e.printStackTrace();
            return "";
	    }
        
	}
	
	public HashMap<String, Object> LoadImage(HashMap<String, Object>... hm)
	{
         //String imgUrl = (String) hm[0].get("flag_path");
         String imgUrl = webservice_url + "/art/" + (String) hm[0].get("img_path");
         int position = (Integer) hm[0].get("position");

         imgUrl = imgUrl + "_small.jpg";
         
         try {
             
             // Getting Caching directory
             File cacheDirectory = myContext.getCacheDir();

             // Temporary file to store the downloaded image
             File tmpFile = new File(cacheDirectory.getPath() + "/wpta_"+position+".png");

             // The FileOutputStream to the temporary file
             FileOutputStream fOutStream = new FileOutputStream(tmpFile);

        	 Bitmap b = LoadImageFromUrl(imgUrl);

        	 if (b == null)
        	 {
        		 int imageResource = R.drawable.person_small;
        		 String path = String.valueOf(imageResource);
        		// String path = "@drawable/person_small";
 //       		 b = LoadImageFromUrl(path);
        		 
	             // Flush the FileOutputStream
	             fOutStream.flush();
	
	             //Close the FileOutputStream
	             fOutStream.close();
        		 
	             // Create a hashmap object to store image path and its position in the listview
	             HashMap<String, Object> hmBitmap = new HashMap<String, Object>();
	
	             // Storing the path to the temporary image file
	             hmBitmap.put("img",path);
	
	             // Storing the position of the image in the listview
	             hmBitmap.put("position",position);
	
	             // Returning the HashMap object containing the image path and position
	             return hmBitmap;
        		 
        		 //return null;
        		 
        	 } else {
	             // Writing the bitmap to the temporary file as png file
	             b.compress(Bitmap.CompressFormat.PNG,100, fOutStream);
	
	             // Flush the FileOutputStream
	             fOutStream.flush();
	
	             //Close the FileOutputStream
	             fOutStream.close();
	
	             // Create a hashmap object to store image path and its position in the listview
	             HashMap<String, Object> hmBitmap = new HashMap<String, Object>();
	
	             // Storing the path to the temporary image file
	             hmBitmap.put("img",tmpFile.getPath());
	
	             // Storing the position of the image in the listview
	             hmBitmap.put("position",position);
	
	             // Returning the HashMap object containing the image path and position
	             return hmBitmap;
        	 }
             
         }catch (Exception e) {
             e.printStackTrace();
         }
         return null;
	}
	
	public Bitmap LoadImageFromUrl(String imgUrl)
	{
		 try {
			 URL url = new URL(imgUrl);
	
			 Bitmap b = null;
					 
		     InputStream in = url.openStream();
		     if (in != null) b = BitmapFactory.decodeStream(in);
				     
          	return b;
 		 }catch (Exception e) {
             e.printStackTrace();
             return null;
         }
	}


	
}
