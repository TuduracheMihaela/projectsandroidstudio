package com.sharethatdata.webservice;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.jsonlibrary.JSONParserForPassword;
import com.sharethatdata.webservice.datamodel.cDepartment;
import com.sharethatdata.webservice.datamodel.cList;
import com.sharethatdata.webservice.datamodel.cModuleItems;
import com.sharethatdata.webservice.datamodel.cModules;
import com.sharethatdata.webservice.datamodel.cOrderImage;
import com.sharethatdata.webservice.datamodel.cOrderState;
import com.sharethatdata.webservice.datamodel.cStatus;
import com.sharethatdata.webservice.datamodel.cTaskProject;
import com.sharethatdata.webservice.datamodel.cWorkLocation;
import com.sharethatdata.webservice.notification.MyReceiver;
import com.sharethatdata.webservice.datamodel.cFaq;
import com.sharethatdata.webservice.datamodel.cNewsStatusNotification;
import com.sharethatdata.webservice.datamodel.cProjectItemDetail;
import com.sharethatdata.webservice.datamodel.cReportsList;
import com.sharethatdata.webservice.datamodel.cStatusNotification;
import com.sharethatdata.webservice.datamodel.cTaskImportancy;
import com.sharethatdata.webservice.datamodel.cProjectMember;
import com.sharethatdata.webservice.datamodel.cProjectMy;
import com.sharethatdata.webservice.datamodel.cProjectProgress;
import com.sharethatdata.webservice.datamodel.cRequestList;
import com.sharethatdata.webservice.datamodel.cRequestMy;
import com.sharethatdata.webservice.datamodel.cRequestStatus;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cCompany;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cHolidays;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cLocationCurrent;
import com.sharethatdata.webservice.datamodel.cMessage;
import com.sharethatdata.webservice.datamodel.cMutation;
import com.sharethatdata.webservice.datamodel.cNews;
import com.sharethatdata.webservice.datamodel.cNewsLastId;
import com.sharethatdata.webservice.datamodel.cNewsStatus;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderProduct;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cProjectItem;
import com.sharethatdata.webservice.datamodel.cReservations;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cResourceGroup;
import com.sharethatdata.webservice.datamodel.cResourceLocation;
import com.sharethatdata.webservice.datamodel.cSickdays;
import com.sharethatdata.webservice.datamodel.cTask;
import com.sharethatdata.webservice.datamodel.cTaskMember;
import com.sharethatdata.webservice.datamodel.cTaskUrgency;
import com.sharethatdata.webservice.datamodel.cTaskUser;
import com.sharethatdata.webservice.datamodel.cTimeRecord;
import com.sharethatdata.webservice.datamodel.cTimeStats;
import com.sharethatdata.webservice.datamodel.cTimeStatsProject;
import com.sharethatdata.webservice.datamodel.cUserLocation;
import com.sharethatdata.webservice.datamodel.cUserStatus;
import com.sharethatdata.webservice.datamodel.cVisitEntries;
import com.sharethatdata.webservice.datamodel.cVisitKind;
import com.sharethatdata.webservice.datamodel.cCustomer;
import com.sharethatdata.webservice.datamodel.cWebService;

//
//WSDataProvider - class with database/JSON interface to new web service
//
public class WSDataProvider{

	private Context context;
	private SharedPreferences sharedPrefs;

	JSONParser jsonParser = new JSONParser();
	JSONParserForPassword jsonParserForPassword = new JSONParserForPassword();
	WebShopParser webshopParser = new WebShopParser();

	private String webservice_url = "https://newtonrp-ws.sharethatdata.com";
	//private String webservice_url = "https://dev-ws.sharethatdata.com";
	//private String webservice_url = "http://stock-ws.sharethatdata.com";
	//private String webservice_url = "http://www.pneumatiekvoordeel.nl/stock-ws";
    private String labelservice_url = "http://192.168.1.88:8001";
	private String webshopservice_url_add = "http://pneumatiekvoordeel.nl/basket_barcode_api.php?do=insert";
	private String webshopservice_url_check = "http://pneumatiekvoordeel.nl/basket_barcode_api.php?do=check";
	private String webshop = "pneumatiekvoordeel.nl";

    private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    private String url_create_item = "create_item.php";
    private String url_update_item = "update_item.php";
    private String url_delete_item = "delete_item.php";
	private String request_reset_password = "requestReset.php";
	private String process_reset_password = "processResetRequest.php";
        
    private String url_login = "login.php";
    private String url_log = "log_event.php";

	// cerberus - register device for save settings
	private String register_device_cerberus = "register_device.php";
	private String get_webservices_cerberus = "webservices.php";
    
    private String url_create_printjob = "AddPrintJob";
    
    private static final String PRINT_TAG_SUCCESS = "success";
    private static final String TAG_SUCCESS = "ws_success";
	private static final String TAG_ERROR = "ws_error";
    private static final String TAG_MESSAGE = "ws_error_message";
    
    private static final String TAG_ITEMS = "items";
	    
    private static final String SESSION = "bl123";
  
    private static final String CONNECTION_ERROR = "Web service not found, please check WIFI/Network connection.";
    
    public String last_error = "";
       
    public boolean json_connected = false;
    public boolean printer_connected = false;
    public boolean executing_print_job = false;

	private String myUser = "";
	private String myPass = "";
	private String myApp = "";

    private String ip = "";
    private String android_id = "";
    private String appname = "";

    public WSDataProvider(Context context)
	{
    	this.context = context;
	}
    
    public void startServicefromNonActivity(){

   	 /*  Intent intent=new Intent(context, MyService.class);
   	   context.startService(intent);*/
   	
   	AlarmManager alarmManagerMessage = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
       Intent intentMessage = new Intent(context, MyReceiver.class);

		//PendingIntent pendingIntent= PendingIntent.getBroadcast(context, 0, intentMessage, 0);

		PendingIntent pendingIntent = null;
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
			pendingIntent = PendingIntent.getBroadcast
					(context, 0, intentMessage, PendingIntent.FLAG_MUTABLE);
		}
		else
		{
			pendingIntent = PendingIntent.getBroadcast
					(context, 0, intentMessage, PendingIntent.FLAG_ONE_SHOT);
		}
   	
   	 SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
		 boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);
			 if(blNotifications){
				 // true - start service
				 alarmManagerMessage.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),180000,
			        		pendingIntent); // 180000 3 min - 300000 5 min 
			 }else{
				// false - stop service
				 AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                manager.cancel(pendingIntent);
			 }

   	}

	/////////////////////////////////////////////////////////////////////////



	/////////////////////////////////////////////////////////////////////////
    
	public WSDataProvider(String new_user, String new_pass)
	{
		myUser = new_user;	
		myPass = new_pass;

		String currentSESSION = WSSession.getInstance().getString();
		if(!currentSESSION.equals("")){

			myPass = WSSession.getInstance().getPassword();
			myApp = WSSession.getInstance().getAppName();
			android_id = WSSession.getInstance().getDevice();
			ip = WSSession.getInstance().getIP();
			android_id = WSSession.getInstance().getDevice();
		}

	}
	
	public void SetLabelServiceUrl(Context context)
	{
		
		this.context = context;
	    sharedPrefs = context.getSharedPreferences("defaults", 0);
				
		String strLabelService = sharedPrefs.getString("prefLabelServer", "");
		String strLabelPort = sharedPrefs.getString("prefLabelPort", "");
		if (!strLabelService.equals("") && !strLabelPort.equals(""))
		{
			// set new adres
			labelservice_url = "http://" + strLabelService + ":" + strLabelPort;
		
		}
	}
	
	public void SetWebServiceUrl(String new_url)
	{
		if (!new_url.equals("")){
			webservice_url = new_url;

			// extract webshop
			String webshop_url = webservice_url.replace("http://www.", "");
			webshop_url = webshop_url.replace("/stock-ws","");
			webshop = webshop_url;
		}
		
		System.out.println("URL: " + webservice_url);
		
		// save web service url for notifications
		//SharedPreferences.Editor editor = context.getSharedPreferences("PREF_URL", 0).edit();
   	    // editor.putString("URL", String.valueOf(webservice_url));
   	   //  editor.commit();
	}
	
	public void SetDeviceIdentification(String new_ip, String new_android_id, String new_appname)
	{
		ip = new_ip;
		appname = new_appname;
		android_id = new_android_id;
	
	}
	
	public static String MD5_Hash(String input) {
		
	    try {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        byte[] messageDigest = md.digest(input.getBytes());
	        BigInteger number = new BigInteger(1, messageDigest);
	        String md5 = number.toString(16);

	        while (md5.length() < 32)
	            md5 = "0" + md5;

	        return md5;
	    } catch (NoSuchAlgorithmException e) {
	        Log.e("MD5", e.getLocalizedMessage());
	        return null;
	    }
	}
		
	public boolean LogEvent(String event) 
	{
		 // no more logging from client
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
		
		// get values from device
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        // getting JSON Object
     //   JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_log, "GET", params);

        // check for success tag
     //   try {
       //     int success = json.getInt(TAG_SUCCESS);
         
            return true;
   //     } catch (JSONException e) {
   //         e.printStackTrace();
   //         return false;
   //     }
	}

	public boolean CheckLogin(String login, String password, String app)
	{
		return CheckLoginWithReconnect(login, password, app, false);
	}
	
	public boolean CheckLoginWithReconnect(String login, String password, String app, boolean reconnect)
    {
		// clean variables
		last_error = "";
		json_connected = true;
		String myPassword = password;
		String currentSESSION = WSSession.getInstance().getString();
		if(currentSESSION.equals("") || reconnect == false){
			myPassword = MD5_Hash(password);
		}
		// not hashed for digiorder
		if (app.equals("DigiOrder")) myPassword = password;
		//if (app.equals("DigiResource")) myPassword = password;

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
		params.add(new BasicNameValuePair("ip", ip));

		myApp = app;
        myUser = login;
        myPass = myPassword;

		if(context != null){
        // save user and password for notifications 
	      SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
	   	     editor.putString("USER", String.valueOf(myUser));
	   	     editor.putString("PASS", String.valueOf(myPass));
	   	     editor.putString("URL", String.valueOf(webservice_url));
	   	     editor.commit();
        }
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_login, "GET", params);

     	
	   	 if (json != null) 
	   	 {    		 
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	String token = json.getString("ws_token");
	            	WSSession.getInstance().setString(token);
					WSSession.getInstance().setUserPass(myUser, myPass);
					WSSession.getInstance().setAppName(myApp, android_id);

					return true;
	            } else {
	            	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
	            	return false;
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            last_error = CONNECTION_ERROR;
	   		 	json_connected = false;
	            return false;
	        }
	   	 } else {
	   		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
	   		 return false;
	   	 }
    
    }

	// General Method for calling new web service
	// gets a list of json items in array
	// when something goes wrong, an empty array is returned
	private String CallWebShopService(List<NameValuePair> new_params, String url) {

		// clean variables
		last_error = "";
		boolean connected = true;
		String post_str = "GET";
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		url = url.replace(" ", "%20");

		String content = webshopParser.makeHttpRequest(url, post_str, params);

		return content;
	}
	
	// General Method for calling new web service
	// gets a list of json items in array
	// when something goes wrong, an empty array is returned
	private JSONArray CallWebService(List<NameValuePair> new_params, String url, boolean doPost)
	{
		// clean variables
		last_error = "";
		json_connected = true;

		String currentSESSION = WSSession.getInstance().getString();
		if(currentSESSION.equals("")){

			boolean bool = CheckLogin(myUser, myPass, myApp);
			if(bool){
				currentSESSION = WSSession.getInstance().getString();
			}
		}
		
		 // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
          	SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
  	  		  String user = prefs.getString("USER", "");
  	  		  String pass = prefs.getString("PASS", "");

  	          myUser = user;
  	          myPass = pass;
            }
          }
        
        
		params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
        	params.add(p);
        } 
       
    	// getting JSON Object
    	// Note that create product url accepts POST method
        String post_str = "GET"; 
        if (doPost)
        {
        	post_str = "POST";
        } 
        
        JSONObject json = jsonParser.makeHttpRequest(url, post_str, params); 
     	JSONArray items = new JSONArray(); 
    	 if (json != null) 
    	 {    		 
			 try {
				 
				items = json.optJSONArray("items");
			    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
			
				int success = json.getInt(TAG_SUCCESS);
	
		        if (success == 1)
		        {
		         	if (items == null){ 
		         			items = new JSONArray(); 
		         		}else{
		         			System.out.println("items: " + items);
		         		}
		        } else {
		        	// record error
		        	items = new JSONArray(); 
		        	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;

					// watch and learn....
					// token error, login again
					int erronr = json.getInt(TAG_ERROR);
					if (erronr == 10004) {
						boolean bool = CheckLoginWithReconnect(myUser, myPass, myApp, true);
						if (bool) {
							currentSESSION = WSSession.getInstance().getString();


							// call url again to get data now!!!!
							// ..
							// ...
							items = CallWebService(new_params, url, doPost);

						}
					}
		        }
		     } catch (JSONException e) {
		         e.printStackTrace();
		        
		     }
    	            
    	 } else {
    		 
    		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
    	 
    	 }
    	 return items;
    
	}

	private JSONArray CallWebServiceForPassword(List<NameValuePair> new_params, String url, boolean doPost)
	{
		// clean variables
		last_error = "";
		json_connected = true;

		String currentSESSION = WSSession.getInstance().getString();

		// Building header Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		if(context != null){
			if(myUser.contains("")){
				SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
				String user = prefs.getString("USER", "");
				String pass = prefs.getString("PASS", "");

				myUser = user;
				myPass = pass;
			}
		}


		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("filtered", "true"));
		for (NameValuePair p : new_params) {
			params.add(p);
		}

		// getting JSON Object
		// Note that create product url accepts POST method
		String post_str = "GET";
		if (doPost)
		{
			post_str = "POST";
		}

		JSONObject json = jsonParserForPassword.makeHttpRequest(url, post_str, params);
		JSONArray items = new JSONArray();
		if (json != null)
		{
			try {

				items = json.optJSONArray("items");

				// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());

				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					if (items == null){
						items = new JSONArray();

						String error = json.getString(TAG_MESSAGE);
						items = new JSONArray();
						items.put(error);

					}else{
						System.out.println("items: " + items);
					}
				} else {
					// record error
					items = new JSONArray();
					String error = json.getString(TAG_MESSAGE);
					last_error = error;

					if(error.contains("Security token incorrect.")){
						System.out.println("TOKEN EXPIRED");
						System.out.println("TOKEN EXPIRED myUser: " + myUser);
						System.out.println("TOKEN EXPIRED myPass: " + myPass);

						boolean bool = CheckLogin(myUser, myPass, myApp);

						System.out.println("bool: " + bool);

						if(bool){
							JSONArray items_again = CallWebService(new_params, url, false);
							items = items_again;

						}
					}

				}
			} catch (JSONException e) {
				e.printStackTrace();

			}

		} else {

			last_error = CONNECTION_ERROR;
			json_connected = false;

		}
		return items;
	}
	
	// get WebServices
	public List<cWebService> getWebServices()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "ws"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cWebService> returnList = new ArrayList<cWebService>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cWebService myWS = new cWebService();
                    
                    myWS.id = c.getString("id");
                    myWS.name = c.getString("name");
                    myWS.description = c.getString("description");
                    myWS.url = c.getString("url");
					myWS.art_url = c.getString("art_url");

					 //      myWS.image_id = c.getString("image_id");
    		    	
     		    	returnList.add(myWS);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	// get user roles
	public ArrayList<String> getUserRoles(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_role"));
        params.add(new BasicNameValuePair("role_user", id));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
        ArrayList<String> roles = new ArrayList<String>();
        
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    String str_name = c.getString("name");
		        	roles.add(str_name);
        		 }
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
    	return roles;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public ArrayList<String> getDuplicate(String date, String start_time, String end_time)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("starttime", start_time));
        params.add(new BasicNameValuePair("endtime", end_time));
        params.add(new BasicNameValuePair("object", "duplicate_timerecord_entry"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        ArrayList<String> nr = new ArrayList<String>();
        
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    String str_nr = c.getString("nr");
		        	nr.add(str_nr);
        		 }
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
    	return nr;

	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public Boolean isResourceAvailable(String start_date, String start_time,String end_date, String end_time,String resourceID,String type)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "duplicate_reservation_entry"));
        params.add(new BasicNameValuePair("startdate", start_date));
        params.add(new BasicNameValuePair("enddate", end_date));
        params.add(new BasicNameValuePair("starttime", start_time));
        params.add(new BasicNameValuePair("endtime", end_time));
        params.add(new BasicNameValuePair("resourceID", resourceID));
        params.add(new BasicNameValuePair("type", type));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        int number = 0;
        try {
        	if (items.length() > 0)
        	{
        		String str_nr = items.getJSONObject(0).getString("nr");;
        		number = Integer.parseInt(str_nr);
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }

    	return !(number > 0);

	}
	
	public Boolean isMyResourceReservation(String start_date, String start_time,String end_date, String end_time,String resourceID)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "my_reservation_entry"));
        params.add(new BasicNameValuePair("startdate", start_date));
        params.add(new BasicNameValuePair("enddate", end_date));
        params.add(new BasicNameValuePair("starttime", start_time));
        params.add(new BasicNameValuePair("endtime", end_time));
        params.add(new BasicNameValuePair("resourceID", resourceID));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        int number = 0;
        try {
        	if (items.length() > 0)
        	{
        		String str_nr = items.getJSONObject(0).getString("nr");;
        		number = Integer.parseInt(str_nr);
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }

    	return (number > 0);

	}
	
	public String lastReservationId(String start_date, String start_time,String end_date, String end_time,String resourceID)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "last_reservation_entry"));
        params.add(new BasicNameValuePair("startdate", start_date));
        params.add(new BasicNameValuePair("enddate", end_date));
        params.add(new BasicNameValuePair("starttime", start_time));
        params.add(new BasicNameValuePair("endtime", end_time));
        params.add(new BasicNameValuePair("resourceID", resourceID));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        String number = "0";
        try 
        	{
	        	if (items.length() > 0)
	        	{
	        		number = items.getJSONObject(0).getString("id");;
	        	}
        	} 
        	catch (JSONException e) 
        	{
        		e.printStackTrace();
        	}

    	return number;

	}		
	
	public cCompany getCompanyInfo(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyid", id));
        params.add(new BasicNameValuePair("object", "company_info"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        cCompany myCompany = new cCompany();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		    		                    
		    	myCompany.id = c.getInt("id");
		    	myCompany.name = c.getString("name");
		    	myCompany.address = c.getString("address");
		    	myCompany.postcode = c.getString("postcode");
		    	myCompany.city = c.getString("city");
		    	myCompany.address_mail = c.getString("address_mail");
		    	myCompany.postcode_mail = c.getString("postcode_mail");
		    	myCompany.city_mail = c.getString("city_mail");
		    	myCompany.country = c.getString("country");
		    	myCompany.phone = c.getString("phone");
		    	myCompany.fax = c.getString("fax");
		    	myCompany.website = c.getString("website");
		    	myCompany.email = c.getString("email");
		    	myCompany.picture = c.getString("picture");
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return myCompany;

	}
     
	// contact detail
	// first implementation of new webservice. 
	// call with contact id
	// always returns an object
	// of object is empty => check last_error
	public cContact getContact(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("contactid", id));

        // Building Parameters
        List<NameValuePair> params1 = new ArrayList<NameValuePair>();
        params1.add(new BasicNameValuePair("object", "user_profile"));
        params1.add(new BasicNameValuePair("contactid", id));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        JSONArray items1 = CallWebService(params1, webservice_url + "/" + url_get_item_new, false);

        cContact myContact = new cContact();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		    		                    
		    	myContact.id = c.getString("id");
		    	myContact.username = c.getString("username");
		    	myContact.surname = c.getString("surname");
		    	myContact.lastname = c.getString("lastname");
		    	myContact.login = c.getString("login");
		    	myContact.picture = c.getString("picture");
                myContact.auth_level = c.getInt("auth_level");
                myContact.function = c.getString("function");
                myContact.phone = c.getString("phone");
                myContact.email = c.getString("email");
                myContact.status_id = c.getString("status");
                myContact.description = c.getString("description");
		    	myContact.image_id = c.getString("image_id");

		    	//myContact.status_name = c.getString("status_name");
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (items1.length() > 0)
            {
                JSONObject c = items1.getJSONObject(0);

                myContact.id_profile = c.getString("id");
                myContact.user = c.getString("user");
                myContact.name = c.getString("name");
                myContact.surname = c.getString("surname");
                myContact.lastname = c.getString("lastname");
                myContact.function = c.getString("function");
                myContact.company_id = c.getString("company_id");
                myContact.company = c.getString("company");

                myContact.department = c.getString("department");
                myContact.department_id = c.getString("department_id");
                myContact.head_department = c.getString("head_department");

                myContact.phone = c.getString("phone");
                myContact.phone_short = c.getString("phone_short");
                myContact.email = c.getString("email");
                myContact.about = c.getString("about");
                myContact.sentence = c.getString("sentence");
                myContact.work = c.getString("work");
                myContact.work_location = c.getString("work_location");
                myContact.work_location_id = c.getString("work_location_id");
                myContact.experience = c.getString("experience");
                myContact.birth_date = c.getString("birth_date");

                myContact.date_in_service = c.getString("date_in_service");
                myContact.contact_hours = c.getString("contact_hours");
				myContact.days_present = c.getString("days_present");
				myContact.days_not_present = c.getString("days_not_present");

                myContact.image_id = c.getString("image_id");
                myContact.city = c.getString("city");
                myContact.hobby = c.getString("hobby");
                myContact.languages = c.getString("languages");
                myContact.education_level = c.getString("education_level");
                myContact.diplomas = c.getString("diplomas");
                myContact.skills = c.getString("skills");
                myContact.driver_license = c.getString("driver_license");

                myContact.BHV = c.getString("BHV");
                myContact.VCA = c.getString("VCA");
                myContact.AED = c.getString("AED");
                myContact.BC = c.getString("BC");
                myContact.keyholder = c.getString("keyholder");

                myContact.main_task_1 = c.getString("main_task_1");
                myContact.main_task_2 = c.getString("main_task_2");
                myContact.main_task_3 = c.getString("main_task_3");
                myContact.main_task_4 = c.getString("main_task_4");
                myContact.main_task_5 = c.getString("main_task_5");


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

       	return myContact;

	}
	
	// contact detail
	// first implementation of new webservice. 
	// call with contact id
	// always returns an object
	// of object is empty => check last_error
	public cContact getUserID(String username)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("contactid", "0"));
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("username", username));
        
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        cContact myContact = new cContact();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		    		                    
		    	myContact.id = c.getString("id");
		    	myContact.username = c.getString("username");
		    	myContact.surname = c.getString("surname");
		    	myContact.lastname = c.getString("lastname");
		    	myContact.login = c.getString("login");
		    	myContact.picture = c.getString("picture");
		    	myContact.auth_level = c.getInt("auth_level");
		    	myContact.function = c.getString("function");
		    	myContact.phone = c.getString("phone");
		    	myContact.email = c.getString("email");
		    	myContact.status_id = c.getString("status");
		    	myContact.description = c.getString("description");
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return myContact;

	}

	// user by name
	public cContact getUser(String organisation, String name)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("contactid", "0"));
        
        name = name.trim();
        String surname = "";
        String lastname = "";
        String[] wname = name.split(" ");
    	surname = wname[0];
        if (wname.length > 1)
        {
        	for(int i=1;i< wname.length ;i++)
        	{
        		lastname = lastname + wname[i] + " ";
        	}
        } else {
        	//lastname = wname[0];
        }
        
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("surname", surname));
        params.add(new BasicNameValuePair("lastname", lastname));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
        cContact myUser = new cContact();
     
        try {
        	if (items.length() > 0)
        	{
            		             	
            	JSONObject c = items.getJSONObject(0);
            		                    
            	myUser.id = c.getString("id");
            	myUser.username = c.getString("username");
            	myUser.surname = c.getString("surname");
            	myUser.lastname = c.getString("lastname");
            	myUser.login = c.getString("login");
            	myUser.auth_level = c.getInt("auth_level");
            	myUser.function = c.getString("function");
            	myUser.phone = c.getString("phone");
            	myUser.email = c.getString("email");
            	myUser.picture = c.getString("picture");
            	myUser.status_id = c.getString("status");
            	myUser.description = c.getString("description");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
     	return myUser;
	}
	
	public List<cContact> getContacts()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cContact> returnList = new ArrayList<cContact>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("id");
                    myContact.username = c.getString("username");
    		    	myContact.name = c.getString("name");
    		    	myContact.function = c.getString("subtitle");
    		    	myContact.phone = c.getString("phone");
    		    	myContact.email = c.getString("email");
    		    	myContact.status_id = c.getString("status");
    		    	
     		    	returnList.add(myContact);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cContact> getAllUsers()
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user"));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cContact> returnList = new ArrayList<cContact>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cContact myContact = new cContact();

					myContact.id = c.getString("id");
					myContact.name = c.getString("name");
					//myContact.username = c.getString("username");

					returnList.add(myContact);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public List<cContact> getUsers(String report_user, String company)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user"));
		params.add(new BasicNameValuePair("report_user", report_user));
		params.add(new BasicNameValuePair("company", company));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cContact> returnList = new ArrayList<cContact>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cContact myContact = new cContact();

					myContact.id = c.getString("id");
					myContact.name = c.getString("name");
					//myContact.username = c.getString("username");

					returnList.add(myContact);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}
	
	public List<cContact> getMyUsers()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "myusers"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cContact> returnList = new ArrayList<cContact>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("id");
                   	myContact.name = c.getString("name");
    		    	
     		    	returnList.add(myContact);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cContact> getContactsWithThumbImage()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("image", "thumb"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cContact> returnList = new ArrayList<cContact>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("id");
                    myContact.username = c.getString("username");
    		    	myContact.name = c.getString("name");
    		    	myContact.function = c.getString("subtitle");
    		    	myContact.phone = c.getString("phone");
    		    	myContact.email = c.getString("email");
    		    	myContact.status_id = c.getString("status");
    		    	myContact.image_id = c.getString("image_id");
    		    	
     		    	returnList.add(myContact);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cContact> getContactsByRights()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("filter", "true"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cContact> returnList = new ArrayList<cContact>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("id");
                    myContact.username = c.getString("username");
    		    	myContact.name = c.getString("name");
    		    	myContact.function = c.getString("subtitle");
    		    	myContact.phone = c.getString("phone");
    		    	myContact.email = c.getString("email");
    		    	myContact.status_id = c.getString("status");
    		    	
     		    	returnList.add(myContact);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	 public int createContact(cContact newContact)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();
     
	    params.add(new BasicNameValuePair("object", "contact"));
	    
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

	    params.add(new BasicNameValuePair("username", newContact.username));
	    params.add(new BasicNameValuePair("surname", newContact.surname));
	    params.add(new BasicNameValuePair("lastname", newContact.lastname));
	    params.add(new BasicNameValuePair("login", newContact.login));
	    params.add(new BasicNameValuePair("password_hash", newContact.password));

        params.add(new BasicNameValuePair("description", newContact.description));
        params.add(new BasicNameValuePair("email", newContact.email));
        params.add(new BasicNameValuePair("phone", newContact.phone));
	    params.add(new BasicNameValuePair("function", newContact.function));

	    params.add(new BasicNameValuePair("image_id", newContact.image_id));
	    
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	    //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
	    System.out.println(json);
        
	    int contactID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	contactID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }
	 	}
	 	
	 	return contactID;
	 
	 }
	 
	 public boolean createContactProfile(cContact newContact, int userId)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "contact_profile"));

        params.add(new BasicNameValuePair("userfor", String.valueOf(userId)));

	    params.add(new BasicNameValuePair("function", newContact.function)); 
	    params.add(new BasicNameValuePair("about", newContact.about));
	    params.add(new BasicNameValuePair("sentence", newContact.sentence));
	    params.add(new BasicNameValuePair("work", newContact.work));
	    params.add(new BasicNameValuePair("experience", newContact.experience));
	    params.add(new BasicNameValuePair("department", newContact.department_id));
	    params.add(new BasicNameValuePair("birth_date", newContact.birth_date));
	    params.add(new BasicNameValuePair("date_in_service", newContact.date_in_service));

	    params.add(new BasicNameValuePair("contact_hours", newContact.contact_hours));
	    params.add(new BasicNameValuePair("city", newContact.city));
	    params.add(new BasicNameValuePair("hobby", newContact.hobby));
	    params.add(new BasicNameValuePair("languages", newContact.languages));
	    params.add(new BasicNameValuePair("work_location", newContact.work_location_id));
	    params.add(new BasicNameValuePair("days_present", newContact.days_present));
	    params.add(new BasicNameValuePair("days_not_present", newContact.days_not_present));

	    params.add(new BasicNameValuePair("phone_short", newContact.phone_short));
	    params.add(new BasicNameValuePair("education_level", newContact.education_level));
	    params.add(new BasicNameValuePair("diplomas", newContact.diplomas));
	    params.add(new BasicNameValuePair("skills", newContact.skills));
	    params.add(new BasicNameValuePair("driver_license", newContact.driver_license));

	    params.add(new BasicNameValuePair("BHV", newContact.BHV));
	    params.add(new BasicNameValuePair("VCA", newContact.VCA));
	    params.add(new BasicNameValuePair("AED", newContact.AED));
	    params.add(new BasicNameValuePair("BC", newContact.BC));
	    params.add(new BasicNameValuePair("keyholder", newContact.keyholder));

	    params.add(new BasicNameValuePair("main_task_1", newContact.main_task_1));
	    params.add(new BasicNameValuePair("main_task_2", newContact.main_task_2));
	    params.add(new BasicNameValuePair("main_task_3", newContact.main_task_3));
	    params.add(new BasicNameValuePair("main_task_4", newContact.main_task_4));
	    params.add(new BasicNameValuePair("main_task_5", newContact.main_task_5));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        }else {
        	return false;
        }
	 }
	 
	 public boolean createReservation(String resource_id,cReservations reservation)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "reservation"));
	     
	    //params.add(new BasicNameValuePair("user", reservation.user));
	    params.add(new BasicNameValuePair("description", reservation.description));
	    params.add(new BasicNameValuePair("resourceid", resource_id));
	    params.add(new BasicNameValuePair("start_date", reservation.start_date+" "+reservation.start_hour)); 
	    params.add(new BasicNameValuePair("end_date", reservation.end_date+" "+reservation.end_hour));
	    params.add(new BasicNameValuePair("active", reservation.active));
	    params.add(new BasicNameValuePair("reservation_type", reservation.reservation_type));
	    params.add(new BasicNameValuePair("reservation_status", reservation.reservation_status));
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean editReservation(String reservationID,cReservations reservation)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "reservation"));
	    params.add(new BasicNameValuePair("reservationid", reservationID));
	    
	    params.add(new BasicNameValuePair("description", reservation.description));
	    params.add(new BasicNameValuePair("start_date", reservation.start_date+" "+reservation.start_hour)); 
	    params.add(new BasicNameValuePair("end_date", reservation.end_date+" "+reservation.end_hour));
	     
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
		public cReservations getReservation(String reservationID)
		{
			 // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "reservation"));
	        params.add(new BasicNameValuePair("reservationid", reservationID));
	        
	        
	        // getting JSON Object
	        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	             
	        cReservations myReservation = new cReservations();
	        
	        // check for success tag
	        try {
	        	if (items.length() > 0)
	        	{
			    	JSONObject c = items.getJSONObject(0);
			    		                    
			    	myReservation.id = c.getString("id");
			    	myReservation.user = c.getString("user");
			    	myReservation.resource_name = c.getString("resource");
			    	myReservation.start_date = c.getString("startdate");
			    	myReservation.end_date = c.getString("enddate");
			    	myReservation.reservation_status = c.getString("status");		    	
	        	}
	            	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	       	return myReservation;

		}
	 
	public boolean deleteContact(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
	    params.add(new BasicNameValuePair("contactid", id));
	 	  
	    //params.add(new BasicNameValuePair("datetime", newVisit.datetime));
	    params.add(new BasicNameValuePair("active", "0"));
	        	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
		
	}

	public List<cCustomer> getCustomerSpinner(String userfor)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "customer"));
		if(!userfor.equals(""))params.add(new BasicNameValuePair("userfor", userfor));
		params.add(new BasicNameValuePair("image", "none"));
		params.add(new BasicNameValuePair("spinner", "true"));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cCustomer> returnList = new ArrayList<cCustomer>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cCustomer myCustomer = new cCustomer();

					myCustomer.id = c.getLong("id");
					myCustomer.name = c.getString("name");

					returnList.add(myCustomer);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}
	
	public List<cProject> getProjectsSpinner(String userfor)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
		if(!userfor.equals(""))params.add(new BasicNameValuePair("userfor", userfor));
        params.add(new BasicNameValuePair("image", "none"));
        params.add(new BasicNameValuePair("status", "open"));
        params.add(new BasicNameValuePair("spinner", "true"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProject> returnList = new ArrayList<cProject>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProject myProject = new cProject();
                    
                    myProject.id = c.getLong("id");
                    myProject.name = c.getString("name");
					myProject.costplace = c.getString("costplace");
					//String status = c.getString("status");
    		    	
     		    	//if (status.equals("Open")) returnList.add(myProject);
					 returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cProject> getProjectsSpinnerPerUser(String user_id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
        params.add(new BasicNameValuePair("image", "none"));
        params.add(new BasicNameValuePair("userfor", user_id));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProject> returnList = new ArrayList<cProject>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProject myProject = new cProject();
                    
                    myProject.id = c.getLong("id");
                    myProject.name = c.getString("name");
    		    	
     		    	returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cProject> getProjects()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
        params.add(new BasicNameValuePair("image", "thumb"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProject> returnList = new ArrayList<cProject>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProject myProject = new cProject();
                    
                    myProject.id = c.getLong("id");
                    myProject.name = c.getString("name");
                    myProject.code = c.getString("code");
                    myProject.status = c.getString("status");
                    myProject.created = c.getString("created");
                    myProject.deadline = c.getString("ending");
                    myProject.description = c.getString("description");
                    myProject.image_id = c.getString("image_id");
                    
                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy");  
                	
                	try {  
                	    Date dateCreated = json_format.parse(myProject.created);  
                	    Date dateDeadline = json_format.parse(myProject.deadline); 
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (dateCreated.before(today) )
                	    {
                	    	new_format = new SimpleDateFormat("dd/MM/yyyy");  
                	    } 
                	    myProject.created = new_format.format(dateCreated);
                	    myProject.deadline = new_format.format(dateDeadline);
                	    System.out.println("myProject DATE -> " + dateCreated);
                	    System.out.println("myProject DATE -> " + dateDeadline);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
    		    	
     		    	returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cCustomer> getCustomers()
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "customer"));
		//if(!userfor.equals(""))params.add(new BasicNameValuePair("userfor", userfor));
		//params.add(new BasicNameValuePair("image", "none"));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cCustomer> returnList = new ArrayList<cCustomer>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cCustomer myCustomer = new cCustomer();

					myCustomer.id = c.getLong("id");
					myCustomer.name = c.getString("name");

					returnList.add(myCustomer);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}
	
	public List<cProjectItem> getProjectItems(String projectid)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectItem> returnList = new ArrayList<cProjectItem>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProjectItem myProjectItem = new cProjectItem();
                    
                    myProjectItem.id = c.getString("id");
                    myProjectItem.title = c.getString("title");
                    myProjectItem.entry = c.getString("entry");
                    myProjectItem.created = c.getString("created");
                    myProjectItem.owner = c.getString("owner");
    		    	
     		    	returnList.add(myProjectItem);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	
	public List<cProjectMy> getMyProjects()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "myproject"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectMy> returnList = new ArrayList<cProjectMy>();
		
		String result = "";
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProjectMy myProject = new cProjectMy();
                    
                    myProject.id = c.getString("id");
                    myProject.name = c.getString("name");
                    myProject.code = c.getString("code");
                    myProject.status = c.getString("status");
                    myProject.created = c.getString("created");
                    myProject.status = c.getString("description");
                    myProject.image_id = c.getString("image_id");
    		    	
     		    	returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cProjectMy> getLatestProjects(String id, String enddate)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "latest_project"));
        params.add(new BasicNameValuePair("userfor", id));
        params.add(new BasicNameValuePair("enddate", enddate));
                
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectMy> returnList = new ArrayList<cProjectMy>();
		
		String result = "";
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProjectMy myProject = new cProjectMy();
                    
                    myProject.id = c.getString("id");
                    myProject.name = c.getString("name");
                    myProject.code = c.getString("code");
                    myProject.status = c.getString("status");
                    myProject.created = c.getString("created");
                    myProject.status = c.getString("description");
                 	
                    if(!myProject.name.equals("Non-Project")){
                    	returnList.add(myProject);
                    }
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
        	System.out.println(e.toString());
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cProjectProgress> getProjectProgress(String id, String enddate)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_progress"));
        params.add(new BasicNameValuePair("userfor", id));
        params.add(new BasicNameValuePair("enddate", enddate));
                
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectProgress> returnList = new ArrayList<cProjectProgress>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProjectProgress myProjectProgress = new cProjectProgress();
                    
                    myProjectProgress.id = c.getString("id");
                    myProjectProgress.name = c.getString("name");
                    myProjectProgress.progress = c.getString("progress");
                    myProjectProgress.hours = c.getString("hours_remain");
                    myProjectProgress.comment = c.getString("comment");
                   
                    if(!myProjectProgress.name.equals("Non-Project")){
                    	returnList.add(myProjectProgress);
                    }
                    
     		    	
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cProjectMy> getLatestTasks(String id, String enddate)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "latest_task"));
        params.add(new BasicNameValuePair("userfor", id));
        params.add(new BasicNameValuePair("enddate", enddate));
                
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectMy> returnList = new ArrayList<cProjectMy>();
		
		String result = "";
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                  
                    cProjectMy myProject = new cProjectMy();
                    
                    myProject.id = c.getString("id");
                    myProject.name = c.getString("name");
                    myProject.status = c.getString("status");
                    myProject.created = c.getString("created");
                    myProject.status = c.getString("description");
                 	
     		    	returnList.add(myProject);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cProjectProgress> getTaskProgress(String id, String enddate)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "task_progress"));
        params.add(new BasicNameValuePair("userfor", id));
        params.add(new BasicNameValuePair("enddate", enddate));
                
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cProjectProgress> returnList = new ArrayList<cProjectProgress>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cProjectProgress myProjectProgress = new cProjectProgress();
                    
                    myProjectProgress.id = c.getString("id");
                    myProjectProgress.name = c.getString("name");
                    myProjectProgress.progress = c.getString("progress");
                    myProjectProgress.hours = c.getString("hours_remain");
                    myProjectProgress.comment = c.getString("comment");
                   
     		    	returnList.add(myProjectProgress);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////
	
	// get message
		public List<cMessage> getMessageInstant(String id)
		{
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "message"));
	        params.add(new BasicNameValuePair("userfor", id));
	        
	        JSONArray items = null;
	        
	        // getting JSON Object
	        items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	        
	     // result list
			List<cMessage> returnList = new ArrayList<cMessage>();
		
	        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    cMessage myMessage = new cMessage();
	                    
	                    myMessage.id = c.getLong("id");
	                    myMessage.user_from = c.getString("messagefrom");
	                    myMessage.message = c.getString("message");
	                    myMessage.datetime = c.getString("created");
	                    myMessage.status_read = c.getString("status");
	                    
	                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
	                	
	                	try {  
	                	    Date date = json_format.parse(myMessage.datetime);  
	                	    Date today = new Date(); 
	                	    today=new Date(today.getTime()-(24*60*60*1000));
	                	    if (date.before(today))
	                	    {
	                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
	                	    } 
	                	    myMessage.datetime = new_format.format(date);
	                	      
	                	} catch (ParseException e) {  
	                	    // TODO Auto-generated catch block  
	                	    e.printStackTrace();  
	                	}
	                    
	     		    	returnList.add(myMessage);
	        		 }
			    		
	        	}else{
	        		
	        	}
	            	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	       	return returnList;
	        
		}
	
		public List<cMessage> getMessages()
		{
			 // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "message_last"));
	        
	        // getting JSON Object
	        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	             
	    	// result list
			List<cMessage> returnList = new ArrayList<cMessage>();
		
	        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    cMessage myMessage = new cMessage();
	                    
	                    myMessage.id = c.getLong("id");
	                    myMessage.message = c.getString("message");
	                    myMessage.user_from = c.getString("messagefrom");
	                    myMessage.user_last_message = c.getString("last_message_user");
	                    myMessage.user_to = c.getString("messageto");
	                    myMessage.datetime = c.getString("created");
	                    myMessage.status_read = c.getString("status");
	                    
	                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
	                	
	                	try {  
	                	    Date date = json_format.parse(myMessage.datetime);  
	                	    Date today = new Date(); 
	                	    today=new Date(today.getTime()-(24*60*60*1000));
	                	    if (date.before(today))
	                	    {
	                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
	                	    } 
	                	    myMessage.datetime = new_format.format(date);
	                	      
	                	} catch (ParseException e) {  
	                	    // TODO Auto-generated catch block  
	                	    e.printStackTrace();  
	                	}
	                    
	     		    	returnList.add(myMessage);
	        		 }
			    		
	        	}
	            	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	       	return returnList;

		}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<cMessage> getMessagesInbox()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "message_inbox"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cMessage> returnList = new ArrayList<cMessage>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cMessage myMessage = new cMessage();
                    
                    myMessage.id = c.getLong("id");
                    myMessage.message = c.getString("message");
                    myMessage.user_from = c.getString("messagefrom");
                    myMessage.datetime = c.getString("created");
                    myMessage.status_read = c.getString("status");
                    
                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myMessage.datetime);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myMessage.datetime = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
                    
     		    	returnList.add(myMessage);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cMessage> getMessagesSent()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "message_sent"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cMessage> returnList = new ArrayList<cMessage>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cMessage myMessage = new cMessage();
                    
                    myMessage.id = c.getLong("id");
                    myMessage.message = c.getString("message");
                    myMessage.user_from = c.getString("messagefrom");
                    myMessage.datetime = c.getString("created");
                    
                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myMessage.datetime);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myMessage.datetime = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
                    
     		    	returnList.add(myMessage);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cLocation> getLocations(String resource_id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "location"));
        if(!resource_id.equals("0")) params.add(new BasicNameValuePair("resourceid", resource_id));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cLocation> returnList = new ArrayList<cLocation>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cLocation myLocation = new cLocation();
                    
                    myLocation.id = c.getString("id");
                    myLocation.code = c.getString("locationCode");
                    myLocation.name = c.getString("name");
                    myLocation.stock = c.getLong("stock");
                    returnList.add(myLocation);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cMutation> getMutations(String resource_id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "mutation"));
        params.add(new BasicNameValuePair("resourceid", resource_id));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cMutation> returnList = new ArrayList<cMutation>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cMutation myMutation = new cMutation();
                    
                    myMutation.id = c.getString("id");
                    myMutation.datetime = c.getString("datetime");
                    myMutation.location = c.getString("locationCode");
                    myMutation.user = c.getString("user");
                    myMutation.resource = c.getString("resource");
                    myMutation.resource_code = c.getString("resource_code");
                    myMutation.mutation_type = c.getString("mutation_type");
                    myMutation.amount = c.getLong("amount");
                    returnList.add(myMutation);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cResourceGroup> getResourceGroup()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_group"));
        //params.add(new BasicNameValuePair("groupid", "0"));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cResourceGroup> returnList = new ArrayList<cResourceGroup>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResourceGroup myResource = new cResourceGroup();
                    
                    myResource.id = c.getLong("id");
                    myResource.name = c.getString("name");
     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cResource> getResources(String groupSel, String company)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("groupid", groupSel));
        if(!company.equals("")) params.add(new BasicNameValuePair("company", company));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                    
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.groupID = c.getString("resource_group");

					 //cResourceGroup myGroup = getResourceGroupName(myResource.groupID);
					 //myResource.groupName = myGroup.name;

     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public cResourceGroup getResourceGroupName(String group_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_group"));
		params.add(new BasicNameValuePair("groupid", group_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cResourceGroup myGroup = new cResourceGroup();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					myGroup.id = Long.parseLong(c.getString("id"));
					myGroup.name = c.getString("name");
					myGroup.description = c.getString("description");

					//returnList.add(myGroup);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myGroup;

	}
	
	public List<cResource> getAvailableResources(String groupSel)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "available_resource"));
        if (groupSel!="")
        {
        	params.add(new BasicNameValuePair("groupid", groupSel));
        }
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                    
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.groupID = c.getString("resource_group");
     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
	}
	
	public List<cResource> getResourcesbyBarcode(String barcode)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_barcode"));
        params.add(new BasicNameValuePair("barcode", barcode));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                    
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.groupID = c.getString("resource_group");
                    myResource.barcode = c.getString("barcode");
     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cReservations> getReservationsbyBarcode(String barcode)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservations_barcode"));
        params.add(new BasicNameValuePair("barcode", barcode));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cReservations> returnList = new ArrayList<cReservations>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReservations myResource = new cReservations();
                    /*
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.groupID = c.getString("resource_group");
                    myResource.barcode = c.getString("barcode");
     		    	returnList.add(myResource);*/
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cStatusNotification> getStatusNotification(String id_user)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "notifications"));
        //params.add(new BasicNameValuePair("id_user", id_user));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
        System.out.println("notification list: " + items);
             
    	// result list
		List<cStatusNotification> returnList = new ArrayList<cStatusNotification>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cStatusNotification myStatus = new cStatusNotification();
                    
                    myStatus.id = c.getString("id");
                    myStatus.id_object = c.getString("id_object");
                    myStatus.id_user = c.getString("id_user");
                    myStatus.description = c.getString("description");
                    myStatus.datetime = c.getString("datetime");
                    myStatus.status = c.getString("status_read");
                    myStatus.type = c.getString("notification_type");
                    
            		
/*            		SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myStatus.created);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myStatus.created = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}*/
                    
     		    	returnList.add(myStatus);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cList> getLists()
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list"));
		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cList> returnList = new ArrayList<cList>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cList myList = new cList();

					myList.id = c.getString("id");
					myList.name = c.getString("name");
					myList.description = c.getString("description");
					returnList.add(myList);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public List<cList> getListsItem(String id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list_item"));
		params.add(new BasicNameValuePair("listid", id));
		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cList> returnList = new ArrayList<cList>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cList myList = new cList();

					myList.id = c.getString("id");
					myList.name = c.getString("name");
					myList.amount = c.getString("amount");
					myList.stock = c.getString("stock");
					myList.itemID = c.getString("resourceID");
					returnList.add(myList);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}
	
	public List<cResource> getResourcesbyLocationBarcode(String barcode)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_by_location_barcode"));
        params.add(new BasicNameValuePair("location_barcode", barcode));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                    
                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.User = c.getString("user");
                    myResource.groupID = c.getString("resource_group");
     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	 public String getResourceCountbyBarcode(String barcode )
	 {
	 
		// Building Parameters
       List<NameValuePair> params = new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("object", "count_resource_by_barcode"));
	   params.add(new BasicNameValuePair("barcode", barcode));
        
        // getting JSON Object 
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        String count="0";
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		    	count = c.getString("nr");
        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return count;
       	
	 }
	 
	public List<cNews> getNewsItems()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "news"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cNews> returnList = new ArrayList<cNews>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cNews myNews = new cNews();
                    
                    myNews.id = c.getLong("id");
                    myNews.title = c.getString("title");
                    myNews.description = c.getString("description");
                    myNews.created = c.getString("created");
                    myNews.picture = c.getString("image");

                    SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myNews.created);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myNews.created = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
                    
     		    	returnList.add(myNews);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cNewsStatus> getNewsUnread(String id_user)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "news_unread"));
        params.add(new BasicNameValuePair("id_user", id_user));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cNewsStatus> returnList = new ArrayList<cNewsStatus>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cNewsStatus myStatus = new cNewsStatus();
                    
                    myStatus.id = c.getString("id");
                    myStatus.id_news = c.getString("id_news");
                    myStatus.id_user = c.getString("id_user");
                    myStatus.status = c.getString("status");
                    myStatus.title = c.getString("title");
                    myStatus.description = c.getString("description");
                    myStatus.created = c.getString("created");
            		
            		SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myStatus.created);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myStatus.created = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
                    
     		    	returnList.add(myStatus);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cNewsStatus> getNewsStatus(String id_user)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "news_status"));
        params.add(new BasicNameValuePair("id_user", id_user));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cNewsStatus> returnList = new ArrayList<cNewsStatus>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cNewsStatus myStatus = new cNewsStatus();
                    
                    myStatus.id = c.getString("id");
                    myStatus.id_news = c.getString("id_news");
                    myStatus.id_user = c.getString("id_user");
                    myStatus.status = c.getString("status");
                    myStatus.title = c.getString("title");
                    myStatus.description = c.getString("description");
                    myStatus.created = c.getString("created");
            		
            		SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myStatus.created);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myStatus.created = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}
                    
     		    	returnList.add(myStatus);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cNewsStatusNotification> getNewsStatusNotification(String id_user)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "notifications"));
        //params.add(new BasicNameValuePair("id_user", id_user));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
        System.out.println("notification list: " + items);
             
    	// result list
		List<cNewsStatusNotification> returnList = new ArrayList<cNewsStatusNotification>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cNewsStatusNotification myStatus = new cNewsStatusNotification();
                    
                    myStatus.id = c.getString("id");
                    myStatus.id_object = c.getString("id_object");
                    myStatus.id_user = c.getString("id_user");
                    myStatus.description = c.getString("description");
                    myStatus.status = c.getString("status_read");
                    
            		
/*            		SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	
                	try {  
                	    Date date = json_format.parse(myStatus.created);  
                	    Date today = new Date(); 
                	    today=new Date(today.getTime()-(24*60*60*1000));
                	    if (date.before(today))
                	    {
                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
                	    } 
                	    myStatus.created = new_format.format(date);
                	      
                	} catch (ParseException e) {  
                	    // TODO Auto-generated catch block  
                	    e.printStackTrace();  
                	}*/
                    
     		    	returnList.add(myStatus);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cVisitEntries> getVisits(String startdate, String enddate, String report_user)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visit"));

        	Calendar cal = Calendar.getInstance();
    	    cal.set(Calendar.HOUR_OF_DAY, 0);
    	    cal.set(Calendar.MINUTE, 0);
    	    cal.add(Calendar.DAY_OF_MONTH, -7);
    	    Date today  = cal.getTime();
         	String datetime = new SimpleDateFormat("yyyy-MM-dd").format(today);
            if (startdate != "") datetime = startdate;
            params.add(new BasicNameValuePair("startdate", datetime));
            
            cal.add(Calendar.DAY_OF_MONTH, 14);
    	    today  = cal.getTime();
    		datetime = new SimpleDateFormat("yyyy-MM-dd").format(today);
            if (enddate != "") datetime = enddate;
            params.add(new BasicNameValuePair("enddate", datetime));
            params.add(new BasicNameValuePair("report_user", report_user));
        
  
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cVisitEntries> returnList = new ArrayList<cVisitEntries>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cVisitEntries myVisit = new cVisitEntries();
                    
                    myVisit.id = c.getString("id");
                    myVisit.startdatetime = c.getString("startdatetime");
                    myVisit.enddatetime = c.getString("enddatetime");
                    myVisit.allday = c.getString("allday");
                    myVisit.organisation = c.getString("organisation");
                    myVisit.surname = c.getString("surname");
                    myVisit.lastname = c.getString("lastname");
                    myVisit.employee = c.getString("empl_surname") + " " + c.getString("empl_lastname");
                    myVisit.function = c.getString("function");
                    myVisit.phone = c.getString("phone");
                    myVisit.email = c.getString("email");
                    myVisit.visit_kind = c.getString("visit_kind");
                         
     		    	returnList.add(myVisit);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cVisitEntries> getVisitsByOrganisation(String startdate, String enddate, String report_organisation)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visit_organisation"));

    	 params.add(new BasicNameValuePair("report", "report_organisation"));
         params.add(new BasicNameValuePair("report_organisation", report_organisation));
        	

         	Date d = new Date();
         	String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
         	if (startdate != "") datetime = startdate;
            params.add(new BasicNameValuePair("startdate", datetime));
            
            if (enddate != "") datetime = enddate;
            params.add(new BasicNameValuePair("enddate", datetime));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cVisitEntries> returnList = new ArrayList<cVisitEntries>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cVisitEntries myVisit = new cVisitEntries();
                    
                    myVisit.id = c.getString("id");
                    myVisit.startdatetime = c.getString("startdatetime");
                    myVisit.enddatetime = c.getString("enddatetime");
                    myVisit.allday = c.getString("allday");
                    myVisit.organisation = c.getString("organisation");
                    myVisit.surname = c.getString("surname");
                    myVisit.lastname = c.getString("lastname");
                    myVisit.employee = c.getString("empl_surname") + " " + c.getString("empl_lastname");
                    myVisit.function = c.getString("function");
                    myVisit.phone = c.getString("phone");
                    myVisit.email = c.getString("email");
                    myVisit.visit_kind = c.getString("visit_kind");
                         
     		    	returnList.add(myVisit);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	// digitime
	
	public List<cUserLocation> getUserLocation(String userfor)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_location"));
		if(!userfor.equals(""))params.add(new BasicNameValuePair("userfor", userfor));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cUserLocation> returnList = new ArrayList<cUserLocation>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cUserLocation myLocation = new cUserLocation();
                    
                    myLocation.id = c.getLong("id");
                    myLocation.name = c.getString("name");
    	
     		    	returnList.add(myLocation);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}
	
	public String getLocationbyBarcode(String barcode)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "location_by_barcode"));
        params.add(new BasicNameValuePair("barcode", barcode));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
             
    	// result list
		List<cLocation> returnList = new ArrayList<cLocation>();
		String location_id="0";
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) 
        		 {
                    JSONObject c = items.getJSONObject(i);
                    location_id = c.getString("locationID");
    	
        		 }
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return location_id;
		
	}
	
	public List<cContact> getUserIdbyUsername(String username)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_id"));
        params.add(new BasicNameValuePair("username", username));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        List<cContact> returnList = new ArrayList<cContact>();
             
    	// result list
		//List<cUserId> returnList = new ArrayList<cUserId>();
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    cContact myContact = new cContact();
                    
                    myContact.id = c.getString("id");
                    myContact.name = c.getString("name");
                    
                    returnList.add(myContact);
        		 }	
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
	}

	public boolean existUsername(String username)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user_id"));
		params.add(new BasicNameValuePair("username", username));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		//cContact contact = new cContact();
		boolean exist = false;

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);
				cContact myContact = new cContact();

				myContact.id = c.getString("id");
				myContact.name = c.getString("name");

				if(!myContact.id.equals("")){
					exist = true;
				}
			}else{
				exist = false;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return exist;
	}
	
	public List<cUserStatus> getUserStatus()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_status"));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cUserStatus> returnList = new ArrayList<cUserStatus>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cUserStatus myStatus = new cUserStatus();
                    
                    myStatus.id = c.getString("id");
                    myStatus.name = c.getString("name");
    	
     		    	returnList.add(myStatus);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}

	public boolean create_activity(String name)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "activity"));
		params.add(new BasicNameValuePair("name", name));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	// DigiOrder - create list
	public boolean createList(String name, String description)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list"));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("description", description));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	// DigiOrder - delete list
	public boolean deleteList(String listid)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list"));
		params.add(new BasicNameValuePair("id", listid));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	// DigiOrder - delete list item
	public boolean deleteListItem(String listItemId)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list_item"));
		params.add(new BasicNameValuePair("id", listItemId));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	// DigiOrder - add resources to list
	public boolean addToList(String resourceid, String listid, String amount)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_list_item"));
		params.add(new BasicNameValuePair("resourceid", resourceid));
		params.add(new BasicNameValuePair("listid", listid));
		params.add(new BasicNameValuePair("amount", amount));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	public boolean delete_activity(String activity_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "activity"));
		params.add(new BasicNameValuePair("activity_id", activity_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}
	
	public List<cActivity> getUserActivity(String userfor)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_activity"));
		if(!userfor.equals("")){
			params.add(new BasicNameValuePair("userfor", userfor));
		}else{
			params.add(new BasicNameValuePair("filtered", ""));
		}



        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cActivity> returnList = new ArrayList<cActivity>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cActivity myActivity = new cActivity();
                    
                    myActivity.id = c.getString("id");
                    myActivity.name = c.getString("name");
    	
     		    	returnList.add(myActivity);
        		 }
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}

	public boolean create_costplace(String name)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "costplace"));
		params.add(new BasicNameValuePair("name", name));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	public boolean delete_costplace(String costplace_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "costplace"));
		params.add(new BasicNameValuePair("costplace_id", costplace_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}

	public List<cCostplace> getCostPlaces(String userfor)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_costplace"));
		if(!userfor.equals("")){
			params.add(new BasicNameValuePair("userfor", userfor));
		}else{
			params.add(new BasicNameValuePair("filtered", ""));
		}

        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cCostplace> returnList = new ArrayList<cCostplace>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cCostplace myCostplace = new cCostplace();
                    
                    myCostplace.id = c.getLong("id");
                    myCostplace.name = c.getString("name");
    	
     		    	returnList.add(myCostplace);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}
	
	public List<cTask> getProjectTasks(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_task"));
        params.add(new BasicNameValuePair("report_user", id));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cTask> returnList = new ArrayList<cTask>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                     JSONObject c = items.getJSONObject(i);

                     cTask myTask = new cTask();
                    
                     myTask.id = c.getLong("id");
                     myTask.name = c.getString("name");
					 long status = c.getLong("status");

					 if (status == 1) returnList.add(myTask);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}
	
	public List<cTask> getLatestTasks(String id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "latest_task"));
        params.add(new BasicNameValuePair("report_user", id));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cTask> returnList = new ArrayList<cTask>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cTask myTask = new cTask();
                    
                    myTask.id = c.getLong("id");
                    myTask.name = c.getString("name");
    	
     		    	returnList.add(myTask);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}
	
	public List<cTask> getTasksProject(String projectid, String userid)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_task"));
        params.add(new BasicNameValuePair("projectid", projectid));
        params.add(new BasicNameValuePair("report_user", userid));
        params.add(new BasicNameValuePair("status", "1"));
        params.add(new BasicNameValuePair("spinner", "true"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cTask> returnList = new ArrayList<cTask>();
	
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cTask myTask = new cTask();
                    
                    myTask.id = c.getLong("id");
                    myTask.name = c.getString("name");
					//long status = c.getLong("status");

					//if (status == 1) returnList.add(myTask);
					 returnList.add(myTask);

        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
		
	}


    public List<cDepartment> getDepartments()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "department"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cDepartment> returnList = new ArrayList<cDepartment>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cDepartment myDepartment = new cDepartment();

                    myDepartment.ID = c.getString("id");
                    myDepartment.name = c.getString("name");
                    myDepartment.head_department = c.getString("head_department");

                    returnList.add(myDepartment);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cCompany> getCompanys()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "company"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cCompany> returnList = new ArrayList<cCompany>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cCompany myCompany = new cCompany();

                    myCompany.id = c.getLong("id");
                    myCompany.name = c.getString("name");

                    returnList.add(myCompany);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cWorkLocation> getWorkLocations()
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "work_location"));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cWorkLocation> returnList = new ArrayList<cWorkLocation>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cWorkLocation myWorkLocation = new cWorkLocation();

                    myWorkLocation.ID = c.getString("id");
                    myWorkLocation.name = c.getString("name");

                    returnList.add(myWorkLocation);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }
	
	// time record detail
	public cTimeRecord getTimeRecord(String id, boolean latest)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if (latest == true) { 
        	params.add(new BasicNameValuePair("object", "latest_timerecord"));
        
        } else {
            params.add(new BasicNameValuePair("object", "timerecord"));
			params.add(new BasicNameValuePair("recordid", id));
        }
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cTimeRecord myRecord = new cTimeRecord();
        
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		 
		    	myRecord.id = c.getString("id");
            	myRecord.userid = c.getString("userid");
            	myRecord.user = c.getString("user");
            	myRecord.customerid = c.getString("customerid");
            	myRecord.customer = c.getString("customer");
            	myRecord.projectid = c.getString("projectid");
            	myRecord.project = c.getString("project");
            	myRecord.date = c.getString("date");
            	myRecord.starttime = c.getString("starttime");
            	myRecord.endtime = c.getString("endtime");
            	myRecord.minutes = c.getInt("hours");
            	myRecord.locationid = c.getString("locationid");
            	myRecord.location = c.getString("location");
               	myRecord.taskid = c.getString("taskid");
               	myRecord.task = c.getString("task");
            	myRecord.activityid = c.getString("activityid");
            	myRecord.activity = c.getString("activity");
            	myRecord.costplaceid = c.getString("costplaceid");
            	myRecord.costplace = c.getString("costplace");
            	myRecord.description = c.getString("description");


        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return myRecord;
         
	}
	
	public List<cTimeRecord> getTimeRecords(String date_start, String date_end, String user, boolean addNew, String taskid)
	{
		// result list
		List<cTimeRecord> returnList = new ArrayList<cTimeRecord>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "timerecord"));
        
        Date d = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
        if (date_start != "") datetime = date_start;
                
        params.add(new BasicNameValuePair("startdate", datetime));
        if (date_end != "") datetime = date_end;
        params.add(new BasicNameValuePair("enddate", datetime));
        params.add(new BasicNameValuePair("report_user", user));
        //params.add(new BasicNameValuePair("projectid", "289"));
        
        if(!taskid.equals("0")){
        	 params.add(new BasicNameValuePair("taskid", taskid));
        }
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cTimeRecord myRecord = new cTimeRecord();
                                        
                    myRecord.id = c.getString("id");
                	myRecord.user = c.getString("user");
                	myRecord.project = c.getString("project");
                	myRecord.date = c.getString("date");
                	myRecord.starttime = c.getString("starttime");
                	myRecord.endtime = c.getString("endtime");
                	myRecord.minutes = c.getInt("hours");
                	myRecord.location = c.getString("location");
                	myRecord.task = c.getString("task");
                	myRecord.activity = c.getString("activity");
                	myRecord.costplace = c.getString("costplace");
                	myRecord.description = c.getString("description");
                	
     		    	returnList.add(myRecord);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}
	
	public List<cReservations> getReservations( String resourceid,String user,String all)
	{
		// result list
		List<cReservations> returnList = new ArrayList<cReservations>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation_resource"));
        params.add(new BasicNameValuePair("resourceid", resourceid));
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("all", all));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReservations myRecord = new cReservations();
                                        
                    myRecord.id = c.getString("id");
                	myRecord.description = c.getString("description");
                	myRecord.start_date = c.getString("startdate");
                	myRecord.end_date = c.getString("enddate");
                	myRecord.user=c.getString("user");
     		    	returnList.add(myRecord);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}
	
	public List<cReservations> getMyReservations(String userfor,String resid,String date)
	{ 
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "myreservations"));
		if(!userfor.equals("")) params.add(new BasicNameValuePair("userfor", userfor));
		if(!resid.equals("")) params.add(new BasicNameValuePair("resid", resid));
		if(!date.equals("")) params.add(new BasicNameValuePair("date", date));
        
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
             
    	// result list
		List<cReservations> returnList = new ArrayList<cReservations>();
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReservations myReservation = new cReservations();
                    
                    myReservation.id = c.getString("id");
                    myReservation.description = c.getString("description");
                    myReservation.start_date = c.getString("start");
                    myReservation.end_date = c.getString("end");
                    myReservation.resource_name = c.getString("resname");
                    myReservation.user=c.getString("user_name_surname");
                    myReservation.active = c.getString("active");
                    myReservation.reservation_type = c.getString("reservation_type");
                    myReservation.reservation_status = c.getString("reservation_status");
       		    	returnList.add(myReservation);
                  }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
	}

	public List<cTimeStats> getTimeStats(String date_start, String date_end, String user, String stat)
	{
		// result list
		List<cTimeStats> returnList = new ArrayList<cTimeStats>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "timerecord_stats"));
        
        Date d = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
        if (date_start != "") datetime = date_start;
                
        params.add(new BasicNameValuePair("startdate", date_start));
        if (date_end != "") datetime = date_end;
        params.add(new BasicNameValuePair("enddate", date_end));
        params.add(new BasicNameValuePair("report_user", user));
        params.add(new BasicNameValuePair("report_stat", stat));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success tag
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cTimeStats myRecord = new cTimeStats();
                                        
                    myRecord.id = c.getString("id");
                	myRecord.name = c.getString("name");
                	myRecord.hours = c.getInt("hours");
                	
     		    	returnList.add(myRecord);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}
	
	public List<cTimeStatsProject> getTimeStatsProject( String stat, String contact, String project)
	{
		// result list
		List<cTimeStatsProject> returnList = new ArrayList<cTimeStatsProject>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "timerecord_stats_activity"));
        params.add(new BasicNameValuePair("report_stat", stat));
        params.add(new BasicNameValuePair("report_user", contact));
        params.add(new BasicNameValuePair("projectid", project));
        
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success
        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    cTimeStatsProject myRecord = new cTimeStatsProject();
	                                        
	                    //myRecord.id = c.getString("id");
	                	myRecord.name = c.getString("name");
	                	myRecord.total_hours = c.getInt("total_hours");
	                	
	     		    	returnList.add(myRecord);
	        		 }
			    		
	        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}

/*	public List<cTimeStatsProject> getTimeStatsUser(String stat, String project)
	{
		// result list
		List<cTimeStatsProject> returnList = new ArrayList<cTimeStatsProject>();
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "timerecord_stats_users"));
        params.add(new BasicNameValuePair("object", "timerecord_stats_activity"));
        params.add(new BasicNameValuePair("report_stat", stat));
        params.add(new BasicNameValuePair("projectid", project));
        
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
     
        // check for success
        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    cTimeStatsProject myRecord = new cTimeStatsProject();
	                                        
	                  //  myRecord.id = c.getString("id");
	                	myRecord.name = c.getString("name");
	                	myRecord.total_hours = c.getInt("total_hours");
	                	
	     		    	returnList.add(myRecord);
	        		 }
			    		
	        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
       
	}*/
	
	public boolean deleteTimeRecord(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "timerecord"));
	    params.add(new BasicNameValuePair("recordid", id));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
		
	}
	
	// project detail
	public cProject getProject(String id)
	{		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project"));
        params.add(new BasicNameValuePair("projectid", id));
        
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cProject myProject = new cProject();
    
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		
		    	myProject.id = c.getLong("id");
            	myProject.name = c.getString("name");
            	myProject.code = c.getString("code");
				myProject.costplace = c.getString("costplace");
            	myProject.created = c.getString("created");
            	myProject.deadline = c.getString("ending");
            	myProject.status = c.getString("status");
             	myProject.description = c.getString("description");
             	myProject.status = c.getString("status");
             	myProject.owner = c.getString("owner");
             	myProject.full_image = c.getString("full_image");
             	myProject.privacy = c.getString("visibility");
             	//myProject.ETC = c.getInt("ETC");
            	
            	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy");  
            	
            	try {  
            	    Date dateCreated = json_format.parse(myProject.created);  
            	    Date dateDeadline = json_format.parse(myProject.deadline); 
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (dateCreated.before(today) )
            	    {
            	    	new_format = new SimpleDateFormat("dd/MM/yyyy");  
            	    } 
            	    myProject.created = new_format.format(dateCreated);
            	    myProject.deadline = new_format.format(dateDeadline);
            	    System.out.println("myProject DATE -> " + dateCreated);
            	    System.out.println("myProject DATE -> " + dateDeadline);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
            	
             	
                
        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return myProject;
	}
	
	// project detail for notification
		public cProject getProjectForNotification(String id)
		{		
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "project"));
	        params.add(new BasicNameValuePair("projectid", id));
	        
	        JSONArray items = null;
	        
	        // getting JSON Object
	        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	        cProject myProject = new cProject();
	    
	        try {
	        	if (items.length() > 0)
	        	{
			    	JSONObject c = items.getJSONObject(0);
			
			    	myProject.id = c.getLong("id");
	            	myProject.name = c.getString("name");	
	                
	        	}
	        	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } 	
	       	return myProject;
		}
		
	// news detail
	public cNews getNewsItem(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("newsid", id));
        params.add(new BasicNameValuePair("object", "news"));
        
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cNews myNews = new cNews();
             
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		   		                    
            	myNews.id = c.getLong("id");
            	myNews.title = c.getString("title");
            	myNews.description = c.getString("description");
            	myNews.created = c.getString("created");
            	myNews.owner = c.getString("owner");
            	myNews.picture = c.getString("image");
            	
            	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
            	
            	try {  
            	    Date date = json_format.parse(myNews.created);  
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (date.before(today))
            	    {
            	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
            	    } 
            	    myNews.created = new_format.format(date);
            	    System.out.println("DATE -> " + date);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return myNews;
	}
	
	
	// news last id
	public cNewsLastId getNewsLastId()
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "last_news"));
        
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cNewsLastId myNewsLastId = new cNewsLastId();
             
        try {
        	if (items.length() > 0)
        	{
		    	JSONObject c = items.getJSONObject(0);
		   		                    
		    	myNewsLastId.id = c.getString("id"); 
        	}
        	
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
       	return myNewsLastId;
	}
	
		
	// resource detail
	public cResource getResource(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("resourceid", id));

        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cResource myResource = new cResource();
             
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		                    
            	myResource.id = c.getString("id");
            	myResource.name = c.getString("name");
            	myResource.code = c.getString("code");
            	myResource.status = c.getString("status");
            	myResource.description = c.getString("description");
            	myResource.User = c.getString("user");
            	myResource.barcode = c.getString("barcode");
            	myResource.location = c.getString("location");
            	myResource.image_id = c.getString("image_id");
            	myResource.bookamount = c.getLong("book_amount");
            	myResource.groupID = c.getString("resource_group");

        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        return myResource;
	}
	
	// get message
	public cMessage getMessage(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("object", "message"));
        
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
    	cMessage myMessage = new cMessage();
        
    	try {
    		if (items.length() > 0)
    		{

            	JSONObject c = items.getJSONObject(0);
            	
            	myMessage.id = c.getLong("id");
            	myMessage.user_from = c.getString("messagefrom");
            	myMessage.datetime = c.getString("created");
            	myMessage.message = c.getString("description");
            	
            	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
            	
            	try {  
            	    Date date = json_format.parse(myMessage.datetime);  
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (date.before(today))
            	    {
            	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
            	    } 
            	    myMessage.datetime = new_format.format(date);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
    		}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        return myMessage;
        
	}
	
	
	// get image
/*		public cImage getImage(String id)
		{
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("imageid", id));
	        params.add(new BasicNameValuePair("object", "image"));
	        
	        JSONArray items = null;
	        
	        // getting JSON Object
	        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	        cImage myImage = new cImage();
	        
	    	try {
	    		if (items.length() > 0)
	    		{

	            	JSONObject c = items.getJSONObject(0);
	            	
	            	myImage.image = c.getString("image");
	            	//myImage.thumbnail_Image = c.getString("thumbnail_Image");

	    		}
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } 	
	        return myImage;
	        
		}*/
				
	 public String getImage(String id)
	 {
		 String result = "";

	     List<NameValuePair> params = new ArrayList<NameValuePair>();
	     params.add(new BasicNameValuePair("object", "image"));
	     params.add(new BasicNameValuePair("imageid", id));
	     params.add(new BasicNameValuePair("fullimage", "true"));
	     
	     JSONArray items = null;
	        
	     items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	     try 
	     {
	        if (items.length() > 0)
	        {
	        	JSONObject c = items.getJSONObject(0);
	        	result = c.getString("full_image");
	        }
	     }
	     catch (JSONException e) 
	     {
	    	 e.printStackTrace();
	     }
	     
	     return result;
	 }

	public cOrderImage getOrderImage(String id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "order_image"));
		params.add(new BasicNameValuePair("id", id));
		params.add(new BasicNameValuePair("fullimage", "true"));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cOrderImage img = new cOrderImage();

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				img.id = c.getString("id");
				img.datetime = c.getString("datetime");
				img.user = c.getString("user");
				img.image = c.getString("full_image");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return img;
	}

	public List<cOrderImage> getOrderImages(String orderid)
	{
		List<cOrderImage> returnList = new ArrayList<cOrderImage>();

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "order_image"));
		params.add(new BasicNameValuePair("orderid", orderid));
	//	params.add(new BasicNameValuePair("fullimage", "true"));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		try
		{
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {

					cOrderImage img = new cOrderImage();
					JSONObject c = items.getJSONObject(i);

					img.id = c.getString("id");
					img.datetime = c.getString("datetime");
					img.user = c.getString("user");
					img.image = c.getString("image");

					returnList.add(img);
				}
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return returnList;
	}

	 public boolean SendNotification(String phoneNo, String sms)
	 {
		 try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNo, null, sms, null, null);
			return true;
		  } catch (Exception e) {
			e.printStackTrace();
			return false;
		  }
	 }
	 
	 public String getLocationName(String id)
	 {
		 String result = "";

	     List<NameValuePair> params = new ArrayList<NameValuePair>();
	     params.add(new BasicNameValuePair("object", "location_name"));
	     params.add(new BasicNameValuePair("location_id", id));
	     
	     JSONArray items = null;
	        
	     items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	     try 
	     {
	        if (items.length() > 0)
	        {
	        	JSONObject c = items.getJSONObject(0);
	        	result = c.getString("name");
	        }
	     }
	     catch (JSONException e) 
	     {
	    	 e.printStackTrace();
	     }
	     
	     return result;
	 }
	
	 // DIGIVISITOR METHODS
	 
	 public cVisitEntries getVisit(String visitid)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("visitid", visitid));
        params.add(new BasicNameValuePair("object", "visit"));
        
        JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        
        cVisitEntries visit = new cVisitEntries();
		      
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		                    
            	visit.id = c.getString("id");
            	visit.startdatetime = c.getString("startdatetime");
            	visit.enddatetime = c.getString("enddatetime");
            	visit.allday = c.getString("allday");
            	visit.organisation = c.getString("organisation");
            	visit.surname = c.getString("surname");
            	visit.lastname = c.getString("lastname");
            	visit.phone = c.getString("phone");
            	visit.email = c.getString("email");
            	visit.function = c.getString("function");
            	visit.visit_kind = c.getString("visit_kind");
            	visit.employee = c.getString("employee");
            	visit.employee_name = c.getString("user");
            	
            	// get visit members
            	List<cContact> memberList = getVisitMembers(visit.id);
            	for(cContact member : memberList)
            	{
            		visit.coWorkers.add(member.name);
            	}
        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
	   
		 return visit;
	 }
	 
	 public List<cContact> getVisitMembers(String visitid)
	 {
		List<cContact> returnList = new ArrayList<cContact>();
			
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visit_member"));
        params.add(new BasicNameValuePair("visitid", visitid));
        
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
    
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myMember = new cContact();
                    
                    myMember.id = c.getString("id");
                    myMember.name = c.getString("user");
                    myMember.phone = c.getString("phone");
                    myMember.email = c.getString("email");
                    myMember.status_id = c.getString("status");
                                             
     		    	returnList.add(myMember);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
	 }
	 
	 public List<cVisitEntries> getVisitorList(String organisation, String lastname)
	 {
		List<cVisitEntries> returnList = new ArrayList<cVisitEntries>();
			
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visitor"));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("lastname", lastname));
        params.add(new BasicNameValuePair("surname", ""));
        
     
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
    
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cVisitEntries myVisit = new cVisitEntries();
                    
                    myVisit.id = c.getString("id");
                    myVisit.startdatetime = c.getString("startdatetime");
                    myVisit.enddatetime = c.getString("enddatetime");
                    myVisit.allday = c.getString("allday");
                    myVisit.organisation = c.getString("organisation");
                    myVisit.surname = c.getString("surname");
                    myVisit.lastname = c.getString("lastname");
                    myVisit.employee = c.getString("employee");
                    myVisit.employee_name = c.getString("user");
                    myVisit.function = c.getString("function");
                    myVisit.phone = c.getString("phone");
                    myVisit.email = c.getString("email");
                    myVisit.visit_kind = c.getString("visit_kind");
                    myVisit.sign_in_datetime =  c.getString("sign_in_datetime");
                    myVisit.sign_out_datetime =  c.getString("sign_out_datetime");
                    if (myVisit.visit_kind.equals("")) myVisit.visit_kind = "0";    
                    
     		    	returnList.add(myVisit);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;
	 }
	 
	 public boolean createStatusNotification(String objectId, String userId, String description, String datetime, String type )
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "notification"));
	     
	    params.add(new BasicNameValuePair("id_object", objectId));
	    params.add(new BasicNameValuePair("id_user", userId));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("datetime", datetime));
	    params.add(new BasicNameValuePair("notification_type", type));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
	    System.out.println(items);
        
        if (last_error != "")
        {
        	return false;
        	
        }else {
        
        	return true;
        }
	 
	 }
	 
	 public List<cVisitKind> getVisitKind()
	 {
		List<cVisitKind> returnList = new ArrayList<cVisitKind>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("object", "visit_kind"));

		 
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
    
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cVisitKind myVisitKind = new cVisitKind();
                                       
                    myVisitKind.id = c.getString("id");
                    myVisitKind.name = c.getString("name");
					                     
     		    	returnList.add(myVisitKind);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return returnList;		 
	 }

	 public cVisitEntries getLatestVisit(String myOrganisation, String mySurname, String myLastName)
	 {		 
		 cVisitEntries return_visitor = new cVisitEntries();
		 
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
		    
	     params.add(new BasicNameValuePair("object", "latest_visit"));
		 params.add(new BasicNameValuePair("organisation", myOrganisation));
		 params.add(new BasicNameValuePair("lastname", myLastName));
		 params.add(new BasicNameValuePair("surname", mySurname));
		 
	    JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
              
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		  
            	return_visitor.id = c.getString("id");
            	return_visitor.datetime = c.getString("startdatetime");
            	return_visitor.organisation = c.getString("organisation");
            	return_visitor.surname = c.getString("surname");
            	return_visitor.lastname = c.getString("lastname");
            	return_visitor.employee_name = c.getString("user");
            	return_visitor.function = c.getString("function");
            	return_visitor.phone = c.getString("phone");
            	return_visitor.email = c.getString("email");
            	return_visitor.visit_kind = c.getString("visit_kind");
            	return_visitor.employee = c.getString("employee");
            	
            
        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        
		 return return_visitor;
	 }

	 public List<cResource> getRoomAvailableList(String myDateTimeStart, String myDateTimeEnd)
	 {
		List<cResource> returnList = new ArrayList<cResource>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
    
		Date d = new Date();
		String datetime_S = new SimpleDateFormat("yyyy-MM-dd").format(d);
		String datetime_E = new SimpleDateFormat("yyyy-MM-dd").format(d);
		
        if (myDateTimeStart != "")
        {
        	datetime_S = myDateTimeStart;
        }
        if (myDateTimeEnd != "")
        {
        	datetime_E = myDateTimeEnd;
        }
        	        
        params.add(new BasicNameValuePair("object", "free_resource"));
        params.add(new BasicNameValuePair("startdate", datetime_S));
        params.add(new BasicNameValuePair("enddate", datetime_E));
		 
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
    
        try {
        	if (items.length() > 0)
        	{
        		 for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();
                                       
					myResource.id = c.getString("id");
					myResource.name = c.getString("name");
					myResource.code = c.getString("code");
					                     
     		    	returnList.add(myResource);
        		 }
		    		
        	}
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return returnList;		 
	 }
	 
	 public List<cLocation> getLocationList()
	 {
		List<cLocation> returnList = new ArrayList<cLocation>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "location_id"));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cLocation myLocation= new cLocation();
                                      
                   myLocation.id = c.getString("id");
					                     
    		    	returnList.add(myLocation);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 
	 public cLocationCurrent getUserCurrentLocation(String user_id, String datetimeStart)
	 {		 
		 cLocationCurrent myLocation = new cLocationCurrent();
		 
		 List<NameValuePair> params = new ArrayList<NameValuePair>();

         params.add(new BasicNameValuePair("object", "user_current_location"));
         params.add(new BasicNameValuePair("user_id", user_id));

         if(!datetimeStart.equals("")){

			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			// String currentDateandTime = sdf.format(new Date());

			 params.add(new BasicNameValuePair("datetime", datetimeStart));
		 }

	    JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
              
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		  
            	myLocation.id = c.getString("id");
            	myLocation.user_id = c.getString("user_id");
            	myLocation.location_id = c.getString("location_id");
            	myLocation.location_name = c.getString("location_name");
            	myLocation.probabillity = c.getString("probabillity");
            	myLocation.date_time = c.getString("date_time");
            
        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        
		 return myLocation;
	 }
	 
		public cResourceLocation getResourceFromLocation(String resourcelocationid)
		{
			 // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "resource_from_location"));
	        params.add(new BasicNameValuePair("resourcelocationid", resourcelocationid));
	        
	        // getting JSON Object
	        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	             
	    	// result list
	        cResourceLocation resource = new cResourceLocation();
		
	        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    
	                    resource.id = c.getString("id");
	                    resource.name = c.getString("name");
	                    resource.barcode = c.getString("name");

	        		 }
			    		
	        	}
	            	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	       	return resource;
			
		}
		
		public cResourceLocation getResourceLocation(String resourceid)
		{
			 // Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "resource_location"));
	        params.add(new BasicNameValuePair("resourceid", resourceid));
	        
	        // getting JSON Object
	        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	             
	    	// result list
	        cResourceLocation resource = new cResourceLocation();
		
	        try {
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);

	                    
	                    resource.id = c.getString("id");
	                    resource.name = c.getString("name");
	                    resource.barcode = c.getString("name");

	        		 }
			    		
	        	}
	            	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	       	return resource;
			
		}

	public cCustomer getCustomerDetail(String id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "customer"));
		params.add(new BasicNameValuePair("id", id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cCustomer myCustomer = new cCustomer();

		// check for success tag
		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myCustomer.id = c.getInt("id");
				myCustomer.name = c.getString("name");

				myCustomer.street = c.getString("street");
				myCustomer.zipcode = c.getString("zipcode");
				myCustomer.city = c.getString("city");
				myCustomer.country = c.getString("country");
				myCustomer.phone = c.getString("phone");
				myCustomer.fax = c.getString("fax");
				myCustomer.email = c.getString("email");
				myCustomer.contact = c.getString("contact");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myCustomer;

	}

	public boolean createCustomer(String customerName, String street, String zipcode,
			String city, String country, String phone, String fax, String email, String contact,
			String serviceNr, String serviceContract )
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "customer"));

		params.add(new BasicNameValuePair("name", customerName));
		params.add(new BasicNameValuePair("street", street));
		params.add(new BasicNameValuePair("zipcode", zipcode));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("country", country));
		params.add(new BasicNameValuePair("phone", phone));
		params.add(new BasicNameValuePair("fax", fax));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("contact", contact));
		params.add(new BasicNameValuePair("servicenr", serviceNr));
		params.add(new BasicNameValuePair("service_contract", serviceContract));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}

	public boolean updateCustomer(String id, String customerName, String street, String zipcode,
							  String city, String country, String phone, String fax, String email, String contact)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "customer"));
		params.add(new BasicNameValuePair("id", id));

		params.add(new BasicNameValuePair("name", customerName));
		params.add(new BasicNameValuePair("street", street));
		params.add(new BasicNameValuePair("zipcode", zipcode));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("country", country));
		params.add(new BasicNameValuePair("phone", phone));
		params.add(new BasicNameValuePair("fax", fax));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("contact", contact));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean deleteCustomer(String id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "customer"));
		params.add(new BasicNameValuePair("customerid", id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	 public boolean createUserCurrentLocation(String user_id, String location_id)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "user_current_location"));
	     
	    params.add(new BasicNameValuePair("username", user_id));
	    params.add(new BasicNameValuePair("location_id", location_id));
	    
	    if (!location_id.equals(""))
	    {

		    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
	    } else {
		 	return false;
	    }
	 }
	 
	 public boolean createUserCurrentStatus(String username, String status_id)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "user_current_status"));
	     
	    params.add(new BasicNameValuePair("username", username));
	    params.add(new BasicNameValuePair("status_id", status_id));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public int createNews(String owner, String title, String description, String image )
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();
     
	    params.add(new BasicNameValuePair("object", "news"));
	     
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
	    params.add(new BasicNameValuePair("owner", owner));
	    params.add(new BasicNameValuePair("title", title));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("image_id", image));
	    
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	    //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
	    System.out.println(json);
        
	    int newsID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	newsID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }
	 	}
	 	
	 	return newsID;
	 
	 }
	 
	 public boolean createNewsStatusRead(String lastNewsId, String userId, String statusUnread )
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "news_status"));
	     
	    params.add(new BasicNameValuePair("id_news", lastNewsId));
	    params.add(new BasicNameValuePair("id_user", userId));
	    params.add(new BasicNameValuePair("status", statusUnread));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
	    System.out.println(items);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean createNewsStatusNotification(String objectId, String userId, String description )
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "notification"));
	     
	    params.add(new BasicNameValuePair("id_object", objectId));
	    params.add(new BasicNameValuePair("id_user", userId));
	    params.add(new BasicNameValuePair("description", description));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
	    System.out.println(items);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean createProjectProgress(String datetime, String project, String progress, String hours_remain, String comment)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "project_progress"));
	    
	    params.add(new BasicNameValuePair("datetime", datetime));
	    params.add(new BasicNameValuePair("project", project));
	    params.add(new BasicNameValuePair("progress", progress));
	    params.add(new BasicNameValuePair("hours_remain", hours_remain));
	    params.add(new BasicNameValuePair("comment", comment));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean createTaskProgress(String datetime, String task, String progress, String hours_remain, String comment)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "task_progress"));
	     
	    params.add(new BasicNameValuePair("datetime", datetime));
	    params.add(new BasicNameValuePair("task", task));
	    params.add(new BasicNameValuePair("progress", progress));
	    params.add(new BasicNameValuePair("hours_remain", hours_remain));
	    params.add(new BasicNameValuePair("comment", comment));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean createMessage(String user_to, String message)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "message"));
	     
	    params.add(new BasicNameValuePair("FROM", user_to));
	    params.add(new BasicNameValuePair("message", message));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
		
	 
	 public int create_reservation(String myDateTimeStart, String myDateTimeEnd, String SelectRoom, String allday, String description)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation"));
	     
        String currentSESSION = WSSession.getInstance().getString();
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
	    params.add(new BasicNameValuePair("start_date", myDateTimeStart));
	    params.add(new BasicNameValuePair("end_date", myDateTimeEnd));
	    params.add(new BasicNameValuePair("resourceid", SelectRoom));
	    params.add(new BasicNameValuePair("reservation_status", "0"));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("allday", allday));
	    	    
	   // JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
		    
		    int reservationID = 0;
		 	if (json != null) 
		 	{   
		        // check for success tag
		        try {
		            int success = json.getInt(TAG_SUCCESS);
		
		            if (success == 1)
		            {
		            	reservationID = json.getInt("inserted_id");
		            }
		        }
		            catch (JSONException e) 
		            {
		            	e.printStackTrace();
		            }	
		 	}
	        
		 	return reservationID;
	    
        
        /*if (last_error != "")
        {
        	return false;
        	
        }else {
        
        	return true;
        }*/
	 }
	 
	 public boolean create_visit_member(String visitid, String userid)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visit_member"));
	     
	    /*params.add(new BasicNameValuePair("visit", visitid));
	    params.add(new BasicNameValuePair("new_member", userid));*/
        params.add(new BasicNameValuePair("visit_id", visitid));
	    params.add(new BasicNameValuePair("employee", userid));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean create_reservation_member(String reservationid, String userid, String startdatetime)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation_member"));
	     
	    params.add(new BasicNameValuePair("reservationid", reservationid));
	    params.add(new BasicNameValuePair("user_id", userid));
	    params.add(new BasicNameValuePair("startdatetime", startdatetime));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	
	 public boolean createTimeRecord(String myDateTime,String myStartTime,String myEndTime,String myProject,String myLocation, String myTask, String myActivity,String myCostplace,String description, String timeRecorded, String userid, String myCustomer) {

		 /*try {
			 description = java.net.URLEncoder.encode(description, "UTF-8");
		 }catch (UnsupportedEncodingException exception){
			 exception.printStackTrace();
		 }*/

		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "timerecord"));

		if(!userid.equals("")) params.add(new BasicNameValuePair("userid", userid));

	    params.add(new BasicNameValuePair("date", myDateTime));
	    params.add(new BasicNameValuePair("starttime", myDateTime + " " + myStartTime));
	    params.add(new BasicNameValuePair("endtime", myDateTime + " " + myEndTime));
	    params.add(new BasicNameValuePair("location", myLocation));
	    params.add(new BasicNameValuePair("activity", myActivity));
	    params.add(new BasicNameValuePair("task", myTask));
	    params.add(new BasicNameValuePair("projectid", myProject));
		params.add(new BasicNameValuePair("customer", myCustomer));
	    params.add(new BasicNameValuePair("costplace", myCostplace));
		params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("timerecorded", timeRecorded));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, true);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	
	 public int createVisitEntry(cVisitEntries newVisit) 
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();
        
        params.add(new BasicNameValuePair("object", "visit"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
	    params.add(new BasicNameValuePair("startdatetime", newVisit.startdatetime));
	    params.add(new BasicNameValuePair("enddatetime", newVisit.enddatetime));
	    /*params.add(new BasicNameValuePair("sign_out_datetime", newVisit.enddatetime));
     	params.add(new BasicNameValuePair("sign_in_datetime", newVisit.startdatetime));*/
	    // if more than 8 hours allday = 1 else allday = 0
	    SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    String startDate = newVisit.startdatetime;
	    String endDate = newVisit.enddatetime;

	    long diff, hours = 0, minutes;
	    try {
	        Date datestart = dateFormat.parse(startDate);
	        Date dateend = dateFormat.parse(endDate);
	        dateend.compareTo(datestart);
	        diff = dateend.getTime() - datestart.getTime();
	        hours = TimeUnit.MILLISECONDS.toHours(diff);
	        minutes = TimeUnit.MILLISECONDS.toMinutes(diff) - hours*60;
	        System.out.println(hours + " hours and " + minutes + " minutes");
	    } catch (ParseException e) {

	        e.printStackTrace();
	    }

        if(hours > 8){
        	newVisit.allday = "1";
        }else{
        	newVisit.allday = "0";
        }
	    params.add(new BasicNameValuePair("allday", newVisit.allday));
	    System.out.println("allday: " + newVisit.allday);
	    params.add(new BasicNameValuePair("organisation", newVisit.organisation));
	    params.add(new BasicNameValuePair("surname", newVisit.surname));
	    params.add(new BasicNameValuePair("lastname", newVisit.lastname));
	    params.add(new BasicNameValuePair("phone", newVisit.phone));
	    params.add(new BasicNameValuePair("email", newVisit.email));
	    params.add(new BasicNameValuePair("function", newVisit.function));
	    params.add(new BasicNameValuePair("visit_kind", newVisit.visit_kind));
	    params.add(new BasicNameValuePair("employee", newVisit.employee));
	    params.add(new BasicNameValuePair("image", "0"));
	    
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	   // JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    
	    int visitID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	visitID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
        
	 	return visitID;
        /*if (last_error != "")
        {
        	return false;
        	
        }else {
        
        	// also create visit members
        	
        	return true;
        }*/
	 }
	 
	 public boolean updateVisitEntry(String visitid, cVisitEntries newVisit, String filter)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "visit"));
        params.add(new BasicNameValuePair("visitid", visitid));
        
        if(filter.equals("signout")){
         // if (newVisit.enddatetime != "") params.add(new BasicNameValuePair("enddatetime", newVisit.enddatetime));
        	if (newVisit.enddatetime != "") params.add(new BasicNameValuePair("sign_out_datetime", newVisit.enddatetime));
        }
        else {
        	 if(filter.equals("signin")){
        		 if (newVisit.startdatetime != "") params.add(new BasicNameValuePair("sign_in_datetime", newVisit.startdatetime));
        	} else {
        		 if (newVisit.startdatetime != "") params.add(new BasicNameValuePair("startdatetime", newVisit.startdatetime));
        		 if (newVisit.enddatetime != "") params.add(new BasicNameValuePair("enddatetime", newVisit.enddatetime));
        	}
        //	if (newVisit.enddatetime != "") params.add(new BasicNameValuePair("sign_out_datetime", newVisit.enddatetime));
        	
        	// if more than 8 hours allday = 1 else allday = 0
    	    SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	    String startDate = newVisit.startdatetime;
    	    String endDate = newVisit.enddatetime;

    	    long diff, hours = 0, minutes;
    	    try {
    	        Date datestart = dateFormat.parse(startDate);
    	        Date dateend = dateFormat.parse(endDate);
    	        dateend.compareTo(datestart);
    	        diff = dateend.getTime() - datestart.getTime();
    	        hours = TimeUnit.MILLISECONDS.toHours(diff);
    	        minutes = TimeUnit.MILLISECONDS.toMinutes(diff) - hours*60;
    	        System.out.println(hours + " hours and " + minutes + " minutes");
    	        
    	        if(hours > 8){
                	newVisit.allday = "1";
                }else{
                	newVisit.allday = "0";
                }
    	        
    	        System.out.println("allday: " + newVisit.allday);
        	    params.add(new BasicNameValuePair("allday", newVisit.allday));
        	
    	    } catch (ParseException e) {

    	        e.printStackTrace();
    	    }

    	    params.add(new BasicNameValuePair("organisation", newVisit.organisation));
    	    params.add(new BasicNameValuePair("surname", newVisit.surname));
    	    params.add(new BasicNameValuePair("lastname", newVisit.lastname));
    	    params.add(new BasicNameValuePair("phone", newVisit.phone));
    	    params.add(new BasicNameValuePair("email", newVisit.email));
    	    params.add(new BasicNameValuePair("function", newVisit.function));
    	    params.add(new BasicNameValuePair("visit_kind", newVisit.visit_kind));
    	    params.add(new BasicNameValuePair("employee", newVisit.employee));
    	    params.add(new BasicNameValuePair("get_product_info", newVisit.productinfo));
        } 
	    	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateResource(cResource resource)
	 {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "resource"));
	        //params.add(new BasicNameValuePair("resourceid", resource.id));
	        params.add(new BasicNameValuePair("resourceid", resource.code));
	        //params.add(new BasicNameValuePair("id", resource.code));
	        params.add(new BasicNameValuePair("inuse", Boolean.toString(resource.InUse)));
		    params.add(new BasicNameValuePair("code", resource.code));
		    params.add(new BasicNameValuePair("name", resource.name));
		    params.add(new BasicNameValuePair("barcode", resource.barcode));
		    params.add(new BasicNameValuePair("description", resource.description));
		    params.add(new BasicNameValuePair("group_id", resource.groupID));
		    params.add(new BasicNameValuePair("owner", resource.User));
		 if(!resource.image_id.equals("")) params.add(new BasicNameValuePair("image_id", resource.image_id));
		    
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
	 }
	 
	 public boolean updateStock(String resID, String locID, long stock_change, boolean reset_stock)
	 {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "stock"));
	        params.add(new BasicNameValuePair("resourceid", resID));
	        params.add(new BasicNameValuePair("locationid", locID));
	        params.add(new BasicNameValuePair("amount", String.valueOf(stock_change)));
	        params.add(new BasicNameValuePair("reset_stock", String.valueOf(reset_stock)));
	        
	        
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
	 }
	 
	 public boolean updateOrderProduct(String orderlineID, String processed_amount, String myProduct)
	 {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "orderproduct"));
	        params.add(new BasicNameValuePair("id", orderlineID));
	        params.add(new BasicNameValuePair("amount", processed_amount));
		   	if (myProduct.substring(0, 7).equals("LF - FB"))
		    	params.add(new BasicNameValuePair("FB", processed_amount));
			 if (myProduct.substring(0, 7).equals("LF - SP"))
			 params.add(new BasicNameValuePair("SP", processed_amount));


		 JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
	 }
	 
	 public boolean updateOrder(String orderID)
	 {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "order"));
	        params.add(new BasicNameValuePair("id", orderID));
	 	        
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
	 }
	 
	 public boolean createOrderProduct(String orderid, String productname, String productnr, String locationID, String amount)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "orderproduct"));

	    params.add(new BasicNameValuePair("location", locationID));
	    params.add(new BasicNameValuePair("orderid", orderid));
	    params.add(new BasicNameValuePair("productnr", productnr));
	    params.add(new BasicNameValuePair("product", productname));
	    params.add(new BasicNameValuePair("amount", amount));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean createOrder(String user, String orderid, String order_type)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "order"));
	    params.add(new BasicNameValuePair("user", user));
	    params.add(new BasicNameValuePair("orderid", orderid));
	    params.add(new BasicNameValuePair("order_type", order_type));
	   
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	
	 	 
	 public boolean updateContact(String contactid, cContact newContact)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact"));
        params.add(new BasicNameValuePair("contactid", contactid));
  	  
	    //params.add(new BasicNameValuePair("datetime", newVisit.datetime));
	    
	   // if (newContact.username != "") params.add(new BasicNameValuePair("username", newContact.username));
	   // if (newContact.username != "") params.add(new BasicNameValuePair("login", newContact.username));
	    if (newContact.surname != "") params.add(new BasicNameValuePair("surname", newContact.surname));
	    if (newContact.lastname != "") params.add(new BasicNameValuePair("lastname", newContact.lastname));
	    if (newContact.password != "") params.add(new BasicNameValuePair("password", newContact.password));
	    if (newContact.email != "") params.add(new BasicNameValuePair("email", newContact.email));
	    if (newContact.phone != "") params.add(new BasicNameValuePair("phone", newContact.phone)); 
	    if (newContact.description != "") params.add(new BasicNameValuePair("description", newContact.description));
	    if (newContact.image_id != "") params.add(new BasicNameValuePair("image_id", newContact.image_id));
		    	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, true);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateContactProfile(String profileid,String contactid, cContact newContact)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "contact_profile"));
        params.add(new BasicNameValuePair("profileid", profileid));
        params.add(new BasicNameValuePair("userid", contactid));
        params.add(new BasicNameValuePair("department", newContact.department_id));
        params.add(new BasicNameValuePair("company", newContact.company_id));

		 params.add(new BasicNameValuePair("function", newContact.function));
		 params.add(new BasicNameValuePair("about", newContact.about));
		 params.add(new BasicNameValuePair("sentence", newContact.sentence));
		 params.add(new BasicNameValuePair("work", newContact.work));
		 params.add(new BasicNameValuePair("experience", newContact.experience));
		 params.add(new BasicNameValuePair("birth_date", newContact.birth_date));
		 params.add(new BasicNameValuePair("date_in_service", newContact.date_in_service));

		 params.add(new BasicNameValuePair("contact_hours", newContact.contact_hours));
		 params.add(new BasicNameValuePair("city", newContact.city));
		 params.add(new BasicNameValuePair("hobby", newContact.hobby));
		 params.add(new BasicNameValuePair("languages", newContact.languages));
		 params.add(new BasicNameValuePair("work_location", newContact.work_location));
		 params.add(new BasicNameValuePair("days_present", newContact.days_present));
		 params.add(new BasicNameValuePair("days_not_present", newContact.days_not_present));

		 params.add(new BasicNameValuePair("phone_short", newContact.phone_short));
		 params.add(new BasicNameValuePair("education_level", newContact.education_level));
		 params.add(new BasicNameValuePair("diplomas", newContact.diplomas));
		 params.add(new BasicNameValuePair("skills", newContact.skills));
		 params.add(new BasicNameValuePair("driver_license", newContact.driver_license));

		 params.add(new BasicNameValuePair("BHV", newContact.BHV));
		 params.add(new BasicNameValuePair("VCA", newContact.VCA));
		 params.add(new BasicNameValuePair("AED", newContact.AED));
		 params.add(new BasicNameValuePair("BC", newContact.BC));
		 params.add(new BasicNameValuePair("keyholder", newContact.keyholder));

		 params.add(new BasicNameValuePair("main_task_1", newContact.main_task_1));
		 params.add(new BasicNameValuePair("main_task_2", newContact.main_task_2));
		 params.add(new BasicNameValuePair("main_task_3", newContact.main_task_3));
		 params.add(new BasicNameValuePair("main_task_4", newContact.main_task_4));
		 params.add(new BasicNameValuePair("main_task_5", newContact.main_task_5));
		    	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, true);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateReservationStatus(String reservationid, String active, String reservation_status, String locationID)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation_status"));
        params.add(new BasicNameValuePair("reservationid", reservationid));
        params.add(new BasicNameValuePair("active", active));
        params.add(new BasicNameValuePair("reservation_status", reservation_status));
        params.add(new BasicNameValuePair("location", locationID));
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item,false);
	    
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateMessageStatusRead(String messageid)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "message"));
        params.add(new BasicNameValuePair("id", messageid));
   	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item,false);
	    System.out.println("Message update: " + items);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateNewsStatusRead(String newsid)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "news_status"));
        params.add(new BasicNameValuePair("id", newsid));
   	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item,false);
	    System.out.println("News status updated: " + items);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public boolean updateNewsStatusNotification(String notificationid, String status)
	 {
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
			
	        params.add(new BasicNameValuePair("object", "notification"));
	        
	        params.add(new BasicNameValuePair("notificationid", notificationid));
	/*        params.add(new BasicNameValuePair("id_object", objectId));
	        params.add(new BasicNameValuePair("id_user", userId));
	        params.add(new BasicNameValuePair("description", description));*/
	        params.add(new BasicNameValuePair("status_read", status));
	   	    
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		    System.out.println("notification status updated: " + items);
	        
	        if (last_error != "")
	        {
	        	return false;
	        	
	        }else {
	        
	        	return true;
	        }
	 }
	 
	 public boolean updateNews(String newsid, String owner, String title, String description, String image )
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "news"));
	     
	    params.add(new BasicNameValuePair("newsid", newsid));
	    params.add(new BasicNameValuePair("owner", owner));
	    params.add(new BasicNameValuePair("title", title));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("image_id", image));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean updateTimeRecord(String id, String date, String starttime, String endtime, String location, String activity, String projectid, 
			 String task, String costplace, String description, String idUser, String myUser, String customer)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "timerecord"));
        params.add(new BasicNameValuePair("recordid", id));
        //params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("report_user", idUser));
	    params.add(new BasicNameValuePair("date", date));
	    params.add(new BasicNameValuePair("starttime", date + " " + starttime));
	    params.add(new BasicNameValuePair("endtime", date + " " + endtime));
	    params.add(new BasicNameValuePair("location", location));
	    params.add(new BasicNameValuePair("activity", activity));
	    params.add(new BasicNameValuePair("customer", customer));
	    params.add(new BasicNameValuePair("projectid", projectid));
		params.add(new BasicNameValuePair("task", task));
	    params.add(new BasicNameValuePair("costplace", costplace));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("timerecorded", description));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	    
	    System.out.println(items);
	    System.out.println(myUser);
	    System.out.println(idUser);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	public boolean CreatPrintJob(String user, String organisation, String visitor, String visitor_organisation)
    {
		
		executing_print_job = true;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("organisation", organisation));
        params.add(new BasicNameValuePair("visitor", visitor));
        params.add(new BasicNameValuePair("visitor_organisation", visitor_organisation));
        

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(labelservice_url + "/" + url_create_printjob, "GET", params);

        printer_connected = false;
        
        if (json != null)
        {
		    // check for success tag
		    try {
		        int success = json.getInt(PRINT_TAG_SUCCESS);
		
		        if (success == 1)
		        {
		        	printer_connected = true;
		        } 
		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
        }
        
        // free mem
        json = null;
        
        executing_print_job = false;
        
        return printer_connected;
    }

    public boolean CreateOnlinePrintJob(String user, String organisation, String organisation_employee, String visitor, String visitor_organisation, String barcode, String visit_id)
    {

        executing_print_job = true;

        // Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "label"));
		params.add(new BasicNameValuePair("user", user));
		params.add(new BasicNameValuePair("token", currentSESSION));

		params.add(new BasicNameValuePair("barcode", barcode));
        params.add(new BasicNameValuePair("organisation", organisation));
		params.add(new BasicNameValuePair("organisation_employee", organisation_employee));

		params.add(new BasicNameValuePair("visitor", visitor));
        params.add(new BasicNameValuePair("visitor_organisation", visitor_organisation));

		params.add(new BasicNameValuePair("visit_id", visit_id));
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
        printer_connected = false;

        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    printer_connected = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // free mem
        json = null;

        executing_print_job = false;

        return printer_connected;
    }

	public List<cHolidays> getHolidaysList(String myDateTimeStart, String myDateTimeEnd, String user)
	 {
		List<cHolidays> returnList = new ArrayList<cHolidays>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
   
		Date d = new Date();
		String datetime_S = new SimpleDateFormat("yyyy-MM-dd").format(d);
		String datetime_E = new SimpleDateFormat("yyyy-MM-dd").format(d);
		
       if (myDateTimeStart != "")
       {
       	datetime_S = myDateTimeStart;
       }
       if (myDateTimeEnd != "")
       {
       	datetime_E = myDateTimeEnd;
       }
       	        
       params.add(new BasicNameValuePair("object", "holiday"));
       params.add(new BasicNameValuePair("startdate", datetime_S));
       params.add(new BasicNameValuePair("enddate", datetime_E));
       params.add(new BasicNameValuePair("report_user", user));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cHolidays myHoliday = new cHolidays();
                                      
                   myHoliday.id = c.getString("id");
                   myHoliday.user = c.getString("user");
                   myHoliday.datetime_start = c.getString("datetime_start");
                   myHoliday.datetime_end = c.getString("datetime_end");
                   myHoliday.hours = c.getInt("hours");
                   myHoliday.approved = c.getString("approved");
					                     
    		    	returnList.add(myHoliday);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	
	 public int create_holiday(String myDateTimeStart, String myDateTimeEnd, String hours, String approved)
	 {
		// Building Parameters
       List<NameValuePair> params = new ArrayList<NameValuePair>();
       String currentSESSION = WSSession.getInstance().getString();
       params.add(new BasicNameValuePair("object", "holiday"));
       params.add(new BasicNameValuePair("user", myUser));
       params.add(new BasicNameValuePair("token", currentSESSION));
	     
	    params.add(new BasicNameValuePair("startdate", myDateTimeStart));
	    params.add(new BasicNameValuePair("enddate", myDateTimeEnd));
	    params.add(new BasicNameValuePair("hours", hours));
	    params.add(new BasicNameValuePair("approved", approved));
	    	    
	    //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	    
	    int holidayID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	holidayID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
		return holidayID;
     }
	 
	 
	 public boolean updateHoliday(String holidayID, String approved, String approved_by)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "holiday"));
	    params.add(new BasicNameValuePair("holidayid", holidayID));
	    params.add(new BasicNameValuePair("approved", approved));
	    params.add(new BasicNameValuePair("approved_by", approved_by)); 
	     
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public List<cSickdays> getSickdaysList(String myDateTimeStart, String myDateTimeEnd, String user)
	 {
		List<cSickdays> returnList = new ArrayList<cSickdays>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
   
		Date d = new Date();
		String datetime_S = new SimpleDateFormat("yyyy-MM-dd").format(d);
		String datetime_E = new SimpleDateFormat("yyyy-MM-dd").format(d);
		
       if (myDateTimeStart != "")
       {
       	datetime_S = myDateTimeStart;
       }
       if (myDateTimeEnd != "")
       {
       	datetime_E = myDateTimeEnd;
       }
       	        
       params.add(new BasicNameValuePair("object", "sickday"));
       params.add(new BasicNameValuePair("startdate", datetime_S));
       params.add(new BasicNameValuePair("enddate", datetime_E));
       params.add(new BasicNameValuePair("report_user", user));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cSickdays mySickday = new cSickdays();
                                      
                   mySickday.id = c.getString("id");
                   mySickday.user = c.getString("user");
                   mySickday.datetime_start = c.getString("datetime_start");
                   mySickday.datetime_end = c.getString("datetime_end");
                   mySickday.hours = c.getInt("hours");
                   mySickday.description = c.getString("description");
                   mySickday.approved = c.getString("approved");
					                     
    		    	returnList.add(mySickday);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 public int create_sickday(String myDateTimeStart, String myDateTimeEnd, String hours, String description)
	 {
		// Building Parameters
       List<NameValuePair> params = new ArrayList<NameValuePair>();
       String currentSESSION = WSSession.getInstance().getString();
       params.add(new BasicNameValuePair("object", "sickday"));
       params.add(new BasicNameValuePair("user", myUser));
       params.add(new BasicNameValuePair("token", currentSESSION));
	     
	    params.add(new BasicNameValuePair("startdate", myDateTimeStart));
	    params.add(new BasicNameValuePair("enddate", myDateTimeEnd));
	    params.add(new BasicNameValuePair("hours", hours));
	    params.add(new BasicNameValuePair("description", description));
	    	    
	    //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	    
	    int sickdayID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	sickdayID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
        
	 	return sickdayID;
	 }
	 
	 public boolean updateSickday(String sickdayID, String approved, String approved_by)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "sickday"));
	    params.add(new BasicNameValuePair("sickdayid", sickdayID));
	    params.add(new BasicNameValuePair("approved", approved));
	    params.add(new BasicNameValuePair("approved_by", approved_by)); 
	     
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 
	 public List<cRequestStatus> getRequestStatusList()
	 {
		List<cRequestStatus> returnList = new ArrayList<cRequestStatus>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "requests_status"));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cRequestStatus myIssueStatus = new cRequestStatus();
                                      
                   myIssueStatus.id = c.getLong("id");
                   myIssueStatus.name = c.getString("name");
					                     
    		    	returnList.add(myIssueStatus);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 
	 public List<cRequestList> getAllRequestList(String report_user)
	 {
		List<cRequestList> returnList = new ArrayList<cRequestList>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "requests"));
       params.add(new BasicNameValuePair("report_user", report_user));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println("getAllRequestList : " + items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cRequestList myRequest = new cRequestList();
                                      
                   myRequest.id = c.getLong("id");
                   myRequest.fromuser = c.getString("fromuser");
                   myRequest.touser = c.getString("touser");
                   myRequest.description = c.getString("description");
                   myRequest.status_name = c.getString("status_name");
					                     
    		    	returnList.add(myRequest);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 
	 public List<cRequestMy> getMyRequestSentList()
	 {
		List<cRequestMy> returnList = new ArrayList<cRequestMy>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "myrequests"));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cRequestMy myRequest = new cRequestMy();
                                      
                   myRequest.id = c.getLong("id");
                   myRequest.fromuser = c.getLong("fromuser");
                   myRequest.fromuser_name_surname = c.getString("fromuser_name_surname");
                   myRequest.touser = c.getLong("touser");
                   myRequest.touser_name_surname = c.getString("touser_name_surname");
                   myRequest.datetime = c.getString("datetime");
                   myRequest.description = c.getString("description");
                   myRequest.status_name = c.getString("status_name");
					                     
    		    	returnList.add(myRequest);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 
	 public List<cRequestMy> getMyRequestAssignedList()
	 {
		List<cRequestMy> returnList = new ArrayList<cRequestMy>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "myrequests"));
       params.add(new BasicNameValuePair("assigned", "true"));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cRequestMy myRequest = new cRequestMy();
                                      
                   myRequest.id = c.getLong("id");
                   myRequest.fromuser = c.getLong("fromuser");
                   myRequest.fromuser_name_surname = c.getString("fromuser_name_surname");
                   myRequest.touser = c.getLong("touser");
                   myRequest.touser_name_surname = c.getString("touser_name_surname");
                   myRequest.datetime = c.getString("datetime");
                   myRequest.description = c.getString("description");
                   myRequest.status_name = c.getString("status_name");
					                     
    		    	returnList.add(myRequest);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 
	 public cRequestList getDetailRequest(String issue_id)
	 {		 
		 cRequestList myRequest = new cRequestList();
		 
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
		    
	     params.add(new BasicNameValuePair("object", "request"));
		 params.add(new BasicNameValuePair("requestid", issue_id));
		 
	    JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
              
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		  
            	myRequest.id = c.getInt("id");
            	myRequest.fromuser = c.getString("fromuser");
            	myRequest.touser = c.getString("touser");
            	myRequest.datetime = c.getString("datetime");
            	myRequest.status_name = c.getString("status_name");
            	myRequest.description = c.getString("description");
            	myRequest.image_id = c.getString("image_id");
            
        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        
		 return myRequest;
	 }
	 
	 
	 public boolean create_request(String user, String touser, String status, String description, String ImageID)
	 {
		// Building Parameters
       List<NameValuePair> params = new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("object", "request"));
	     
       params.add(new BasicNameValuePair("fromuser", user));
       params.add(new BasicNameValuePair("touser", touser));
       params.add(new BasicNameValuePair("status", status));
       params.add(new BasicNameValuePair("description", description));
       params.add(new BasicNameValuePair("image_id", ImageID));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    
	    System.out.println("Items WS: " + items);
       
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 
	 public boolean editRequest(String requestID, String fromuser, String touser, String status, String description, String ImageID)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "request"));
	    params.add(new BasicNameValuePair("requestid", requestID));
	    
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("fromuser", fromuser));
	    params.add(new BasicNameValuePair("touser", touser));
	    params.add(new BasicNameValuePair("status_id", status));
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String currentDateandTime = sdf.format(new Date());
	    params.add(new BasicNameValuePair("datetime_solved", currentDateandTime));
	    
	    params.add(new BasicNameValuePair("image_id", ImageID));
	     
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	            
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 
	 public boolean solvedRequest(String requestID, String description, String status, String datetime_solved, String ImageID)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "request"));
	    params.add(new BasicNameValuePair("requestid", requestID));
	    
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("status_id", status));
	    params.add(new BasicNameValuePair("datetime_solved", datetime_solved));
	    params.add(new BasicNameValuePair("image_id", ImageID));
	     
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 	public boolean deleteRequest(String id)
		{
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "request"));
		    params.add(new BasicNameValuePair("id", id));
		    
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
			
		}
	 
	 public List<cReportsList> getReportsList(String startdate, String enddate, String projectid, String report_user)
	 {
		List<cReportsList> returnList = new ArrayList<cReportsList>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "report"));
       params.add(new BasicNameValuePair("startdate", startdate));
       params.add(new BasicNameValuePair("enddate", enddate));
       params.add(new BasicNameValuePair("projectid", projectid));
       params.add(new BasicNameValuePair("report_user", report_user));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cReportsList myReport = new cReportsList();
                                      
                   myReport.id = c.getString("id");
                   myReport.userid = c.getString("userid");
                   myReport.user = c.getString("user");
                   myReport.projectid = c.getString("projectid");
                   myReport.project = c.getString("project");
                   myReport.date = c.getString("date");
                   myReport.starttime = c.getString("starttime");
                   myReport.name = c.getString("name");
                   myReport.email = c.getString("email");
                   myReport.description = c.getString("description");
					                     
    		    	returnList.add(myReport);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 public cReportsList getDetailReport(String report_id)
	 {		 
		 cReportsList myReport = new cReportsList();
		 
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
		    
	     params.add(new BasicNameValuePair("object", "report"));
		 params.add(new BasicNameValuePair("reportid", report_id));
		 
	    JSONArray items = null;
        
        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
              
        try {
        	if (items.length() > 0)
        	{
		
            	JSONObject c = items.getJSONObject(0);
            		  
            	myReport.id = c.getString("id");
                myReport.userid = c.getString("userid");
                myReport.user = c.getString("user");
                myReport.projectid = c.getString("projectid");
                myReport.project = c.getString("project");
                myReport.date = c.getString("date");
                myReport.starttime = c.getString("starttime");
                myReport.name = c.getString("name");
                myReport.email = c.getString("email");
                myReport.description = c.getString("description");
                myReport.image_id = c.getString("image_id");
            
        	}
        } catch (JSONException e) {
            e.printStackTrace();
        } 	
        
		 return myReport;
	 }
	 
	 public boolean create_report(String date, String starttime, String name, String email, String project, String description, String image_id, String company)
	 {
		// Building Parameters
       List<NameValuePair> params = new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("object", "report"));
       
       String datetime = "";
		
		try
	    {
	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
	    } catch (Exception e)
	    { 	    
	    }
		
		System.out.println("datetime: " + datetime);
	     
       params.add(new BasicNameValuePair("date", datetime));
       params.add(new BasicNameValuePair("starttime", datetime));
       params.add(new BasicNameValuePair("name", name));
       params.add(new BasicNameValuePair("email", email));
       params.add(new BasicNameValuePair("project", project));
       params.add(new BasicNameValuePair("description", description));
       params.add(new BasicNameValuePair("image_id", image_id));
       params.add(new BasicNameValuePair("company", company));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	    
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 }
	 
	 public List<cFaq> getFaqList()
	 {
		List<cFaq> returnList = new ArrayList<cFaq>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "faq"));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cFaq myFaq = new cFaq();
                                      
                   myFaq.id = c.getString("id");
                   myFaq.question = c.getString("question");
                   myFaq.solution = c.getString("solution");
					                     
    		    	returnList.add(myFaq);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	
	 public int createImage(String image_type, byte[] myImage, byte[] myImageThumbnail)
	 {
		// Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    String currentSESSION = WSSession.getInstance().getString();
	    
	    params.add(new BasicNameValuePair("object", "image"));
	     
	    //params.add(new BasicNameValuePair("image_type", "image_report"));
	    params.add(new BasicNameValuePair("image_type", image_type));
	    //params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
        params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

	    int imageID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	imageID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
	 	
	 	return imageID;
	 }

	 // Available to DigiOrder
	 public int createImageForDigiOrder(String image_type, byte[] myImage, byte[] myImageThumbnail)
	 {
		// Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    String currentSESSION = WSSession.getInstance().getString();

	    params.add(new BasicNameValuePair("object", "image"));

	    //params.add(new BasicNameValuePair("image_type", "image_report"));
	    params.add(new BasicNameValuePair("image_type", image_type));
	    //params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
        params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));
        params.add(new BasicNameValuePair("orderid", ""));

	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

	    int imageID = 0;
	 	if (json != null)
	 	{
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);

	            if (success == 1)
	            {
	            	imageID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e)
	            {
	            	e.printStackTrace();
	            }
	 	}

	 	return imageID;
	 }

	public int createOrderImage(String orderid, byte[] myImage, byte[] myImageThumbnail)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();

		params.add(new BasicNameValuePair("object", "image"));

		//params.add(new BasicNameValuePair("image_type", "image_report"));
		params.add(new BasicNameValuePair("orderid", orderid));
		//params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
		params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

		JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

		int imageID = 0;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					imageID = json.getInt("inserted_id");
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return imageID;
	}

	// Project Methods /////////////////////////////////////////////////////////////////////////////////////////////
	 
	 public int createProject(String projectCode, String costplace, String projectName, String description, String owner, String status, String date, String privacy, String image_id)
	 {
		// Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    String currentSESSION = WSSession.getInstance().getString();   
	    params.add(new BasicNameValuePair("object", "project"));
	    params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
	    
	    params.add(new BasicNameValuePair("projectcode", projectCode));
		params.add(new BasicNameValuePair("costplace", costplace));
        params.add(new BasicNameValuePair("name", projectName));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("status", status));
        params.add(new BasicNameValuePair("enddate", date));
        params.add(new BasicNameValuePair("privacy", privacy));
        params.add(new BasicNameValuePair("image_id", image_id));
 	   
	    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

	    int projectID = 0;
	 	if (json != null) 
	 	{   
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	projectID = json.getInt("inserted_id");
	            }
	        }
	            catch (JSONException e) 
	            {
	            	e.printStackTrace();
	            }	
	 	}
	 	
	 	return projectID;
	 }
	 
	 
	 public boolean createProjectMember(String projectID, String membersID)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "project_member"));
	     
	    params.add(new BasicNameValuePair("projectid", projectID));
	    params.add(new BasicNameValuePair("new_member", membersID));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
	 public boolean createTaskMember(String taskID, String membersID)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "task_member"));
	     
	    params.add(new BasicNameValuePair("taskid", taskID));
	    params.add(new BasicNameValuePair("new_member", membersID));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }

	 // Available for DigiOrder
	 public boolean createResourceBarcode(String resourceID, String barcode)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

	    params.add(new BasicNameValuePair("object", "resource_barcode"));

		params.add(new BasicNameValuePair("resourceid", resourceID));
	    params.add(new BasicNameValuePair("barcode", barcode));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

	    if (last_error.equals(""))
        {
        	return true;

        }else {

        	return false;
        }

	 }

	public boolean createResource(String barcode, String code, String name, String description, String group_id, String image,
								  String owner, String inuse)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//String currentSESSION = WSSession.getInstance().getString();

		params.add(new BasicNameValuePair("object", "resource"));
		//params.add(new BasicNameValuePair("user", myUser));
		//params.add(new BasicNameValuePair("token", currentSESSION));

		params.add(new BasicNameValuePair("barcode", barcode));
		params.add(new BasicNameValuePair("code", code));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("description", description));
		params.add(new BasicNameValuePair("description_short", description));
		params.add(new BasicNameValuePair("group_id", group_id));
		if(!image.equals("")) params.add(new BasicNameValuePair("image", image));
		params.add(new BasicNameValuePair("owner", owner));
		params.add(new BasicNameValuePair("inuse", inuse));

		params.add(new BasicNameValuePair("id", code));
		params.add(new BasicNameValuePair("merk", ""));
		params.add(new BasicNameValuePair("soort", ""));
		params.add(new BasicNameValuePair("specificaties", ""));


		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
		//JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

		/*int resourceID = 0;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					resourceID = json.getInt("inserted_id");
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return resourceID;*/

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}

	public boolean createResourceGroup(String name, String group_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_group"));
		params.add(new BasicNameValuePair("description", name));
		params.add(new BasicNameValuePair("parent", group_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateResourceGroup(String ID, String name, String groupId)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_group"));
		params.add(new BasicNameValuePair("groupid", ID));

		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("parent", groupId));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}

	public boolean deleteResourceGroup(String ID)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource_group"));
		params.add(new BasicNameValuePair("groupid", ID));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}

	 public boolean addBarcodeToResource(String resourceId, String barcode)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
     
	    params.add(new BasicNameValuePair("object", "add_barcode_to_resource"));
	     
	    params.add(new BasicNameValuePair("resourceId", resourceId));
	    params.add(new BasicNameValuePair("barcode", barcode));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }	 
	 
	 public List<cProjectMember> getProjectMemberList(String id)
	 {
		List<cProjectMember> returnList = new ArrayList<cProjectMember>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "project_member"));
       params.add(new BasicNameValuePair("projectid", id));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cProjectMember myMember = new cProjectMember();
                                      
                   myMember.id = c.getString("id");
                   myMember.name = c.getString("name");
					                     
    		    	returnList.add(myMember);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }
	 
	 public List<cTaskMember> getTaskMemberList(String id)
	 {
		List<cTaskMember> returnList = new ArrayList<cTaskMember>();
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
       	        
       params.add(new BasicNameValuePair("object", "task_member"));
       params.add(new BasicNameValuePair("task_id", id));
		 
       // getting JSON Object
       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
       
       System.out.println(items);
   
       try {
       	if (items.length() > 0)
       	{
       		 for (int i = 0; i < items.length(); i++) {
                   JSONObject c = items.getJSONObject(i);

                   cTaskMember myMember = new cTaskMember();
                                      
                   myMember.id = c.getString("id");
                   myMember.name = c.getString("user");
					                     
    		    	returnList.add(myMember);
       		 }
		    		
       	}
           	
       } catch (JSONException e) {
           e.printStackTrace();
       }
       
       return returnList;		 
	 }

	public boolean deleteResource(String resourceId, String company)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "resource"));

		params.add(new BasicNameValuePair("resourceid", resourceId));
		params.add(new BasicNameValuePair("company", company));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}
	 
	public boolean deleteProjectMember(String id, String memberid)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_member"));
	    params.add(new BasicNameValuePair("projectid", id));
	    params.add(new BasicNameValuePair("memberid", memberid));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	}

	public boolean deleteProjectAllMembers(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "project_member_atendee"));
	    params.add(new BasicNameValuePair("projectid", id));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

	    if (last_error.equals(""))
        {
        	return true;

        }else {

        	return false;
        }
	}
	
	public boolean deleteTaskAllMembers(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "task_members"));
	    params.add(new BasicNameValuePair("taskid", id));
	    
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
		
	}

	public boolean deleteTaskMember(String id, String memberid)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "task_member"));
	    params.add(new BasicNameValuePair("taskid", id));
	    params.add(new BasicNameValuePair("memberid", memberid));

	    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

	    if (last_error.equals(""))
        {
        	return true;

        }else {

        	return false;
        }

	}
	 
	 public boolean editProject(String projectID, String code, String costplace, String name, String description, String status, String enddate,String privacy, String ImageID)
	 {
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("object", "project"));
	    params.add(new BasicNameValuePair("projectid", projectID));
	    
	    params.add(new BasicNameValuePair("projectcode", code));
		 params.add(new BasicNameValuePair("costplace", costplace));
	    params.add(new BasicNameValuePair("name", name));
	    params.add(new BasicNameValuePair("description", description));
	    params.add(new BasicNameValuePair("status", status));
	    params.add(new BasicNameValuePair("enddate", enddate));
	    params.add(new BasicNameValuePair("privacy", privacy));
	    params.add(new BasicNameValuePair("image_id", ImageID));
   
	    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	    
        
	    if (last_error.equals(""))
        {
        	return true;
        	
        }else {
        
        	return false;
        }
	 
	 }
	 
		public boolean deleteProject(String id)
		{
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "project"));
		    params.add(new BasicNameValuePair("projectid", id));
		    
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
			
		}
		
		
		 public boolean createProjectNotes(String project_id,String title, String description, String image_id, String userID)
		 {
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	     
		    params.add(new BasicNameValuePair("object", "project_item"));
		    params.add(new BasicNameValuePair("user", userID));
		    
			String datetime = "";
			
			try
	 	    {
	 	    	 Date d = new Date();
				 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
	 	    } catch (Exception e)
	 	    { 	    
	 	    }

		    params.add(new BasicNameValuePair("projectid", project_id));
		    params.add(new BasicNameValuePair("title", title));
		    params.add(new BasicNameValuePair("entry", description)); 
		    params.add(new BasicNameValuePair("datetime", datetime));
		    params.add(new BasicNameValuePair("image_id", image_id));
		     
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
	        
	        if (last_error != "")
	        {
	        	return false;
	        	
	        }else {
	        
	        	return true;
	        }
		 
		 }
		
		
			public List<cProjectItem> getProjectNotes(String id)
			 {
				List<cProjectItem> returnList = new ArrayList<cProjectItem>();
			
				List<NameValuePair> params = new ArrayList<NameValuePair>();
		       	        
		       params.add(new BasicNameValuePair("object", "project_item"));
		       params.add(new BasicNameValuePair("projectid", id));
				 
		       // getting JSON Object
		       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
		       
		       System.out.println(items);
		   
		       try {
		       	if (items.length() > 0)
		       	{
		       		 for (int i = 0; i < items.length(); i++) {
		                   JSONObject c = items.getJSONObject(i);

		                   cProjectItem myMember = new cProjectItem();
		                                      
		                   myMember.id = c.getString("id");
		                   myMember.title = c.getString("title");
		                   myMember.entry = c.getString("entry");
		                   myMember.created = c.getString("created");
		                   
		                  	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		                	SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
		                	
		                	try {  
		                	    Date dateCreated = json_format.parse(myMember.created);  
		                	    Date today = new Date(); 
		                	    today=new Date(today.getTime()-(24*60*60*1000));
		                	    if (dateCreated.before(today) )
		                	    {
		                	    	new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
		                	    } 
		                	    myMember.created = new_format.format(dateCreated);
		                	      
		                	} catch (ParseException e) {  
		                	    // TODO Auto-generated catch block  
		                	    e.printStackTrace();  
		                	}
		                	
		                  
		                   myMember.owner = c.getString("owner");
							                     
		    		    	returnList.add(myMember);
		       		 }
				    		
		       	}
		           	
		       } catch (JSONException e) {
		           e.printStackTrace();
		       }
		       
		       return returnList;		 
			 }

		 public cProjectItemDetail getDetailProjectItem(String projid, String noteid)
		 {		 
			 cProjectItemDetail myNote = new cProjectItemDetail();
			 
			 List<NameValuePair> params = new ArrayList<NameValuePair>();
			    
		     params.add(new BasicNameValuePair("object", "project_item"));
			 params.add(new BasicNameValuePair("projectid", projid));
			 params.add(new BasicNameValuePair("itemid", noteid));
			 
		    JSONArray items = null;
	        
	        // getting JSON Object
	        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	              
	        try {
	        	if (items.length() > 0)
	        	{
			
	            	JSONObject c = items.getJSONObject(0);
	            		  
	            	myNote.id = c.getString("id");
	            	myNote.name = c.getString("name");
	            	myNote.subtitle = c.getString("subtitle");
	            	myNote.status = c.getString("status");
	            	myNote.owner = c.getString("owner");
	            	myNote.image_id = c.getString("image_id");
	            
	        	}
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } 	
	        
			 return myNote;
		 }
		 
		 public cProjectItemDetail getDetailProjectItemForNotification(String noteid)
	{
		cProjectItemDetail myNote = new cProjectItemDetail();

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "project_item"));
		params.add(new BasicNameValuePair("itemid", noteid));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		try {
			if (items.length() > 0)
			{

				JSONObject c = items.getJSONObject(0);

				myNote.id = c.getString("id");
				myNote.name = c.getString("name");
				myNote.subtitle = c.getString("subtitle");
				myNote.status = c.getString("status");
				myNote.owner = c.getString("owner");
				myNote.image_id = c.getString("image_id");
				myNote.projectid = c.getString("projectid");
				myNote.projectname = c.getString("projectname");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return myNote;
	}

	public cTaskProject getTaskProject(String taskid)
	{
		cTaskProject myTask = new cTaskProject();

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "project_task"));
		params.add(new BasicNameValuePair("taskid", taskid));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		try {
			if (items.length() > 0)
			{

				JSONObject c = items.getJSONObject(0);

				myTask.activityid = c.getString("activityid");
				myTask.activity = c.getString("activity");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return myTask;
	}

		 public boolean editNote(String noteID, String name, String description, String ImageID)
		 {
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("object", "project_note"));
		    params.add(new BasicNameValuePair("note_id", noteID));
		    
		    String datetime = "";
			
			try
	 	    {
	 	    	 Date d = new Date();
				 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
	 	    } catch (Exception e)
	 	    { 	    
	 	    }

		    params.add(new BasicNameValuePair("note_title", name));
		    params.add(new BasicNameValuePair("note_description", description)); 
		    params.add(new BasicNameValuePair("create_date", datetime));
		    params.add(new BasicNameValuePair("image_id", ImageID));
	   
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		    
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
		 
		 }
		 
			public boolean deleteNote(String id, String pid)
			{
				// Building Parameters
		        List<NameValuePair> params = new ArrayList<NameValuePair>();
		        params.add(new BasicNameValuePair("object", "project_item"));
			    params.add(new BasicNameValuePair("project_itemid", id));
			    params.add(new BasicNameValuePair("projectid", pid));
			    
			    JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);
		        
			    if (last_error.equals(""))
		        {
		        	return true;
		        	
		        }else {
		        
		        	return false;
		        }
				
			}
	 
	 // Tasks Methods
	
		
		 public int createTask(String dateStart, String dateEnd, String contact, String project, String activity,String importancy,String status,String urgency,String estimatedTime,String taskName,String description)
		 {
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        String currentSESSION = WSSession.getInstance().getString(); 
	        
		    params.add(new BasicNameValuePair("object", "project_task"));
		    params.add(new BasicNameValuePair("user", myUser));
	        params.add(new BasicNameValuePair("token", currentSESSION));

		    params.add(new BasicNameValuePair("date", dateStart));
		    params.add(new BasicNameValuePair("deadline", dateEnd));
		    params.add(new BasicNameValuePair("user_id", contact));
		    params.add(new BasicNameValuePair("project", project));
		    params.add(new BasicNameValuePair("activity", activity));
		    params.add(new BasicNameValuePair("importancy", importancy));
		    params.add(new BasicNameValuePair("status", status));
		    params.add(new BasicNameValuePair("urgency", urgency));
		    params.add(new BasicNameValuePair("est_minutes", estimatedTime));
		    params.add(new BasicNameValuePair("name", taskName));
		    params.add(new BasicNameValuePair("description", description));
		    params.add(new BasicNameValuePair("notes", ""));

		    JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
	        
		    int taskID = 0;
		 	if (json != null) 
		 	{   
		        // check for success tag
		        try {
		            int success = json.getInt(TAG_SUCCESS);
		
		            if (success == 1)
		            {
		            	taskID = json.getInt("inserted_id");
		            }
		        }
		            catch (JSONException e) 
		            {
		            	e.printStackTrace();
		            }	
		 	}
		 	
		 	return taskID;
		 
		 }
		 
		 
		 public List<cTaskUser> getTaskUserList(String user, String status, String project)
		 {
			List<cTaskUser> returnList = new ArrayList<cTaskUser>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "project_task"));
	       
	       params.add(new BasicNameValuePair("report_user", user));
	       params.add(new BasicNameValuePair("projectid", project));
	       params.add(new BasicNameValuePair("status", status));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cTaskUser myTask = new cTaskUser();
	                                      
	                   myTask.id = c.getString("id");
	                   myTask.name = c.getString("name");
	                   myTask.userid = c.getString("userid");
	                   myTask.user = c.getString("user");
	                   myTask.projectid = c.getString("projectid");
	                   myTask.project = c.getString("project");
	                   
	                   myTask.date = c.getString("date");
	                   myTask.deadline = c.getString("deadline");
               	SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            	SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy");  
            	
            	try {  
            	    Date dateCreated = json_format.parse(myTask.date);  
            	    Date dateDeadline = json_format.parse(myTask.deadline); 
            	    Date today = new Date(); 
            	    today=new Date(today.getTime()-(24*60*60*1000));
            	    if (dateCreated.before(today) )
            	    {
            	    	new_format = new SimpleDateFormat("dd/MM/yyyy");  
            	    } 
            	    myTask.date = new_format.format(dateCreated);
            	    myTask.deadline = new_format.format(dateDeadline);
            	    System.out.println("myTask DATE -> " + dateCreated);
            	    System.out.println("myTask DATE -> " + dateDeadline);
            	      
            	} catch (ParseException e) {  
            	    // TODO Auto-generated catch block  
            	    e.printStackTrace();  
            	}
	            	    
	                   myTask.importancy = c.getString("importancy");
	                   myTask.imp_name = c.getString("imp_name");
	                   myTask.status = c.getString("status");
	                   myTask.sta_name = c.getString("sta_name");
	                   myTask.urgency = c.getString("urgency");
	                   myTask.urg_name = c.getString("urg_name");
	                   myTask.est_minutes = c.getString("est_minutes");
	                   myTask.reported_minutes = c.getString("reported_minutes");
	                   myTask.activityid = c.getString("activityid");
	                   myTask.activity = c.getString("activity");
	                   myTask.description = c.getString("description");
	                   myTask.notes = c.getString("notes");
						                     
	    		    	returnList.add(myTask);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
		 
		 public boolean updateTask(String id, String taskName, String projectID, String startDate, String activity, String description, String deadline, 
				 String est_minutes, String importancy, String status, String urgency, String user)
		 {
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("object", "project_task"));
	        params.add(new BasicNameValuePair("project_taskid", id));
	        
		    params.add(new BasicNameValuePair("name", taskName));
		    params.add(new BasicNameValuePair("project_id", projectID));
		    params.add(new BasicNameValuePair("date", startDate));
		    params.add(new BasicNameValuePair("activity", activity));
		    params.add(new BasicNameValuePair("description", description));
		    params.add(new BasicNameValuePair("deadline", deadline));
		    params.add(new BasicNameValuePair("est_minutes", est_minutes));
		    params.add(new BasicNameValuePair("importancy", importancy));
		    params.add(new BasicNameValuePair("status", status));
		    params.add(new BasicNameValuePair("urgency", urgency));
		    params.add(new BasicNameValuePair("notes", ""));
		    params.add(new BasicNameValuePair("user_id", user));
		    
		    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
	        
		    if (last_error.equals(""))
	        {
	        	return true;
	        	
	        }else {
	        
	        	return false;
	        }
		 }
		 
		public boolean deleteTask(String id)
		{
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "project_task"));
			params.add(new BasicNameValuePair("project_taskid", id));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

			if (last_error.equals(""))
			{
				return true;

			}else {

				return false;
			}
		}
		
		public List<cTaskImportancy> getTaskImportancy()
		 {
			List<cTaskImportancy> returnList = new ArrayList<cTaskImportancy>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "project_task_importancy"));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cTaskImportancy myImportancy = new cTaskImportancy();
	                                      
	                   myImportancy.id = c.getString("id");
	                   myImportancy.name = c.getString("name");
						                     
	    		    	returnList.add(myImportancy);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
		
		public List<cStatus> getStatus()
		 {
			List<cStatus> returnList = new ArrayList<cStatus>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "project_task_status"));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cStatus myStatus = new cStatus();
	                                      
	                   myStatus.id = c.getString("id");
	                   myStatus.name = c.getString("name");
						                     
	    		    	returnList.add(myStatus);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
		
		
		public List<cTaskUrgency> getTaskUrgency()
		 {
			List<cTaskUrgency> returnList = new ArrayList<cTaskUrgency>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "project_task_urgency"));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cTaskUrgency myUrgency = new cTaskUrgency();
	                                      
	                   myUrgency.id = c.getString("id");
	                   myUrgency.name = c.getString("name");
						                     
	    		    	returnList.add(myUrgency);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
		
		 public List<cOrder> getOrderList(String user, String startdate, String enddate, String ordertype)
		 {
			List<cOrder> returnList = new ArrayList<cOrder>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
		   params.add(new BasicNameValuePair("object", "order"));
		   params.add(new BasicNameValuePair("startdate", startdate));
		   params.add(new BasicNameValuePair("enddate", enddate));
			 params.add(new BasicNameValuePair("ordertype", ordertype));

			 params.add(new BasicNameValuePair("report_user", user));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cOrder myOrder = new cOrder();
	                                      
	                   myOrder.id = c.getString("id");
	                   myOrder.ordernr = c.getString("ordernr");
	                   myOrder.datum = c.getString("datum");
	                   myOrder.uname = c.getString("uname");
	                   myOrder.bedrag = c.getString("bedrag");
	                   myOrder.processed = c.getString("processed");
	                   myOrder.payed = c.getString("payed");
	                   myOrder.levering = c.getString("levering");
	                   myOrder.betaalwijze = c.getString("betaalwijze");
	                   myOrder.num_products = c.getString("products");
	                   myOrder.num_delivered = c.getString("delivered");
					   myOrder.new_stock = c.getString("new_stock");
					   myOrder.checked = c.getString("checked");
	                   	                   	                   
	                   myOrder.datum = c.getString("datum");
	                   SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                   SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");  
		            	
	                   // post-proces logic
	                   if (myOrder.betaalwijze.equals("rekening") || myOrder.betaalwijze.equals("rembours")) myOrder.payed = "1";
	                   
	                   try {  
		            	    Date dateCreated = json_format.parse(myOrder.datum);  
		            	    myOrder.datum = new_format.format(dateCreated);
	                   } catch (ParseException e) {  
		            	    // TODO Auto-generated catch block  
		            	    e.printStackTrace();  
	                   }
	            	                         
	                   returnList.add(myOrder);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
		 
		
		 
		public List<cOrderProduct> getProducts(String orderid)
		 {
			List<cOrderProduct> returnList = new ArrayList<cOrderProduct>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "orderproduct"));
	       params.add(new BasicNameValuePair("orderid", orderid));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cOrderProduct myProduct = new cOrderProduct();
	                                      
	                   myProduct.id = c.getString("id");
	                   myProduct.productnr = c.getString("productnr");
	                   myProduct.product = c.getString("product");
	                   myProduct.amount = c.getString("aantal");
	                   myProduct.amount_done = c.getString("aantal_done");
	                   myProduct.location = c.getString("location");
	                   myProduct.price = c.getString("prijs");
	                   myProduct.processed = c.getString("processed");
	                   myProduct.delivered = c.getString("verzonden");
	                   myProduct.resID = c.getString("resourceID");
	                   myProduct.groupID = c.getString("groupID");
					   try {
						 String myChild = c.getString("childproduct");
						 int montage = c.getInt("montage");

						 if (!myChild.equals(myProduct.product))
						 {
							myProduct.productnr = c.getString("childproductnr");
							myProduct.product = myProduct.product.substring(0,2) + " - " + myChild;

							if (montage == 1) myProduct.product = myProduct.product + " - montage";
							 // replace processed amount
							if (myChild.substring(0,2).equals("SP")) {
								myProduct.processed = c.getString("sp_processed");
							}
							if (myChild.substring(0,2).equals("FB")) {
							    myProduct.processed = c.getString("fb_processed");
							}


						 } else{

						 }


					   } catch (Exception ex)
					   {

					   }
	                   
	    		       returnList.add(myProduct);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }
			
		public List<cOrderProduct> getCheckProducts(String orderid)
		 {
			List<cOrderProduct> returnList = new ArrayList<cOrderProduct>();
		
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
	       params.add(new BasicNameValuePair("object", "checkproduct"));
	       params.add(new BasicNameValuePair("orderid", orderid));
			 
	       // getting JSON Object
	       JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);
	       
	       System.out.println(items);
	   
	       try {
	       	if (items.length() > 0)
	       	{
	       		 for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);

	                   cOrderProduct myProduct = new cOrderProduct();
	                                      
	                   myProduct.id = c.getString("id");
	                   myProduct.productnr = c.getString("productnr");
	                   myProduct.product = c.getString("product");
	                   myProduct.amount = c.getString("aantal");
	                   myProduct.amount_done = c.getString("aantal_done");
	                   myProduct.location = c.getString("location");
	                
	    		       returnList.add(myProduct);
	       		 }
			    		
	       	}
	           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
	       
	       return returnList;		 
		 }

	public List<cOrderState> getOrderStates(String orderid)
	{
		List<cOrderState> returnList = new ArrayList<cOrderState>();

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "order_status"));
		params.add(new BasicNameValuePair("orderid", orderid));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		System.out.println(items);

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cOrderState myOrderState = new cOrderState();

					myOrderState.id = c.getString("id");
					myOrderState.ordernr = c.getString("ordernr");
					myOrderState.datum = c.getString("datum");
					myOrderState.user = c.getString("name");
					myOrderState.status = c.getString("status");

					SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

					try {
						Date dateCreated = json_format.parse(myOrderState.datum);
						myOrderState.datum = new_format.format(dateCreated);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					returnList.add(myOrderState);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return returnList;
	}

		// order detail
		public cOrder getOrder(String id)
		{		
			// Building Parameters
	        List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("orderid", id));
	        params.add(new BasicNameValuePair("object", "order"));
	        
	        JSONArray items = null;
	        
	        // getting JSON Object
	        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
	        
	        cOrder myOrder = new cOrder();
	    
	        try {
	        	if (items.length() > 0)
	        	{
			    	JSONObject c = items.getJSONObject(0);
			
			    	myOrder.id = c.getString("id");
			    	myOrder.uname = c.getString("uname");
					myOrder.address = c.getString("address");
			    	myOrder.datum = c.getString("datum");
			    	myOrder.ordernr = c.getString("ordernr");
			    	myOrder.bedrag = c.getString("bedrag");
			    	myOrder.processed = c.getString("processed");
			    	myOrder.payed = c.getString("payed");
			    	myOrder.levering = c.getString("levering");
	                myOrder.betaalwijze = c.getString("betaalwijze");
	                myOrder.barcode = c.getString("tray");
					myOrder.checked = c.getString("checked");

					// post-proces logic
					if (myOrder.betaalwijze.equals("rekening") || myOrder.betaalwijze.equals("rembours")) myOrder.payed = "1";

					SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	            	SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");  
	            	
	            	try {  
	            	    Date dateCreated = json_format.parse(myOrder.datum);  
	            	    Date today = new Date(); 
	            	    today=new Date(today.getTime()-(24*60*60*1000));
	            	    if (dateCreated.before(today) )
	            	    {
	            	    	new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");  
	            	    } 
	            	    myOrder.datum = new_format.format(dateCreated);
	            	      
	            	} catch (ParseException e) {  
	            	    // TODO Auto-generated catch block  
	            	    e.printStackTrace();  
	            	}
	            	
	        	}
	        	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } 	
	       	return myOrder;
		}

		public cOrder getOrderByBarcode(String id)
		{
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("barcode", id));
			params.add(new BasicNameValuePair("object", "order"));

			JSONArray items = null;

			// getting JSON Object
			items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

			cOrder myOrder = new cOrder();

			try {
				if (items.length() > 0)
				{
					JSONObject c = items.getJSONObject(0);

					myOrder.id = c.getString("id");
					myOrder.uname = c.getString("uname");
					myOrder.address = c.getString("address");
					myOrder.datum = c.getString("datum");
					myOrder.ordernr = c.getString("ordernr");
					myOrder.bedrag = c.getString("bedrag");
					myOrder.processed = c.getString("processed");
					myOrder.payed = c.getString("payed");
					myOrder.levering = c.getString("levering");
					myOrder.betaalwijze = c.getString("betaalwijze");
					myOrder.barcode = c.getString("tray");
					myOrder.checked = c.getString("checked");

					// post-proces logic
					if (myOrder.betaalwijze.equals("rekening") || myOrder.betaalwijze.equals("rembours")) myOrder.payed = "1";

					SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

					try {
						Date dateCreated = json_format.parse(myOrder.datum);
						Date today = new Date();
						today=new Date(today.getTime()-(24*60*60*1000));
						if (dateCreated.before(today) )
						{
							new_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
						}
						myOrder.datum = new_format.format(dateCreated);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return myOrder;
		}
		
		 public boolean updateOrder(String orderID, int processed)
		 {
				List<NameValuePair> params = new ArrayList<NameValuePair>();
		        params.add(new BasicNameValuePair("object", "order"));
		        params.add(new BasicNameValuePair("orderid", orderID));
		        params.add(new BasicNameValuePair("processed", String.valueOf(processed)));
		        
			    JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		        
			    if (last_error.equals(""))
		        {
		        	return true;
		        	
		        }else {
		        
		        	return false;
		        }
		 }

	public boolean editLocation(String locationId, String locationCode, String description, String company)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "location"));

		params.add(new BasicNameValuePair("locationid", locationId));
		params.add(new BasicNameValuePair("locationCode", locationCode));
		params.add(new BasicNameValuePair("description", description));
		params.add(new BasicNameValuePair("company", company));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}
		 
		 public boolean assignOrder(String orderID, String barcode, String paymethod, String language)
		 {
				List<NameValuePair> params = new ArrayList<NameValuePair>();
		    //    params.add(new BasicNameValuePair("object", "order_barcode"));
		   //     params.add(new BasicNameValuePair("orderid", orderID));
			// 	params.add(new BasicNameValuePair("ordernr", orderID));
		    //    params.add(new BasicNameValuePair("barcode", barcode));
		        
			//    JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
			 	String result = CallWebShopService(params, webshopservice_url_add + "&shop=" + webshop + "&ordernr=" + orderID + "&barcode=" + barcode + "&betaalwijze=" + paymethod + "&language_flag=" + language);

		        if (result.equals("OK"))
		        {
		        	return true;
		        	
		        }else {
		        
		        	return false;
		        }
		 }

		public String getAssignedOrder(String barcode)
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			String result = CallWebShopService(params, webshopservice_url_check + "&barcode=" + barcode);

			if (result.equals("OK"))
			{
				return "";

			}else {

				return result;
			}
		}

		public boolean deleteLocation(String id) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "location"));
			params.add(new BasicNameValuePair("locationid", id));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

			if (last_error.equals("")) {
				return true;

			} else {

				return false;
			}
		}

		public boolean deleteProductLocation(String locationID, String resourceID) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "product_location"));
			params.add(new BasicNameValuePair("locationid", locationID));
			params.add(new BasicNameValuePair("resourceid", resourceID));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

			if (last_error.equals("")) {
				return true;

			} else {

				return false;
			}
		}

		public boolean addLocation(String locationcode, String locationname) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "location"));
			params.add(new BasicNameValuePair("name", locationname));
			params.add(new BasicNameValuePair("locationCode", locationcode));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

			if (last_error.equals("")) {
				return true;

			} else {

				return false;
			}
		}

		public boolean addProductLocation(String locname, String resourceID) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "product_location"));
			params.add(new BasicNameValuePair("locationname", locname));
			params.add(new BasicNameValuePair("resourceid", resourceID));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

			if (last_error.equals("")) {
				return true;

			} else {

				return false;
			}
		}

	public int createLocation (String locationCode, String name, String company)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();

		params.add(new BasicNameValuePair("object", "location"));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));

		params.add(new BasicNameValuePair("code", locationCode));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("company", company));

		JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

		int locationID = 0;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					locationID = json.getInt("inserted_id");
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return locationID;
	}

		// Manage options to user
		public boolean addOptionToUser(String user_id, String module_id, String option_id) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("object", "addoptiontouser"));
			params.add(new BasicNameValuePair("user_id", user_id));
			params.add(new BasicNameValuePair("module_id", module_id));
			params.add(new BasicNameValuePair("option_id", option_id));

			JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

			if (last_error.equals("")) {
				return true;
			} else {
				return false;
			}
		}

	// Manage options to user
	public boolean deleteOptionFromUser(String user_id, String module_id, String option_id) {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "deleteoptionfromuser"));
		params.add(new BasicNameValuePair("user_id", user_id));
		params.add(new BasicNameValuePair("module_id", module_id));
		params.add(new BasicNameValuePair("option_id", option_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals("")) {
			return true;
		} else {
			return false;
		}
	}

		public List<cModules> getModules()
		{
			List<cModules> returnList = new ArrayList<cModules>();

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("object", "module"));

			// getting JSON Object
			JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

			System.out.println(items);

			try {
				if (items.length() > 0)
				{
					for (int i = 0; i < items.length(); i++) {
						JSONObject c = items.getJSONObject(i);

						cModules myModule = new cModules();

						myModule.id = c.getString("id");
						myModule.name = c.getString("name");
						myModule.description = c.getString("description");

						returnList.add(myModule);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return returnList;
		}

		public List<cModuleItems> getModuleItems(String modulemame)
		{
			List<cModuleItems> returnList = new ArrayList<cModuleItems>();

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("object", "moduleitems"));
			params.add(new BasicNameValuePair("modulemame", modulemame));

			// getting JSON Object
			JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

			System.out.println(items);

			try {
				if (items.length() > 0)
				{
					for (int i = 0; i < items.length(); i++) {
						JSONObject c = items.getJSONObject(i);

						cModuleItems myModule = new cModuleItems();

						myModule.id = c.getString("id");
						myModule.name = c.getString("name");
						myModule.description = c.getString("description");

						returnList.add(myModule);
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return returnList;
		}

	public Boolean getSecurityCode(String username, String email)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("email", email));

		JSONArray items = null;

		items = CallWebServiceForPassword(params, webservice_url + "/" + request_reset_password, false);
		System.out.println("items: " + items);

		String items_string = items.toString();

		try
		{
			if (items.length() > 0)
			{
				Matcher m = Pattern.compile("\"([^\"]*)\"").matcher(items_string);
				while(m.find()) {
					String x = m.group(1);
					if(x.equals("reset request successfully created."))result = true;
					System.out.println("getSecurityCode: " + m.group(1));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public Boolean getApproveNewPassword(String username, String email, String password, String token)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("token", token));

		JSONArray items = null;

		items = CallWebServiceForPassword(params, webservice_url + "/" + process_reset_password, false);
		System.out.println("items: " + items);

		String items_string = items.toString();

		try
		{
			if (items.length() > 0)
			{
				Matcher m = Pattern.compile("\"([^\"]*)\"").matcher(items_string);
				while(m.find()) {
					String x = m.group(1);
					if(x.equals("password successfully reset."))result = true;
					System.out.println(m.group(1));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	 /* ***************** */
	/*  *** CERBERUS *** */

	public Boolean registerDeviceCerberus(String apikey, String device_id, String device_name)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", device_id));
		params.add(new BasicNameValuePair("device_name", device_name));


		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + register_device_cerberus, false);
		System.out.println("items= " + items);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public List<cWebService> getAllWebServices(String apikey, String deviceid, String ws_type)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", deviceid));
		if(ws_type != "") params.add(new BasicNameValuePair("ws_type", ws_type));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + get_webservices_cerberus, false);

		// result list
		List<cWebService> returnList = new ArrayList<cWebService>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cWebService myWebService = new cWebService();

					myWebService.id = c.getString("id");
					myWebService.name = c.getString("name");
					myWebService.type = c.getString("type");
					myWebService.url = c.getString("url");
					myWebService.default_url = c.getString("default");
					myWebService.access_key = c.getString("access_key");

					returnList.add(myWebService);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

}
