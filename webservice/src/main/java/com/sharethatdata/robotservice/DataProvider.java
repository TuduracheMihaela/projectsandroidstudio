package com.sharethatdata.robotservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.robotservice.datamodel.robotInfo;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cMutation;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderProduct;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cResourceGroup;

import android.text.Html;
import android.util.Log;

public class DataProvider {

	JSONParser jsonParser = new JSONParser();
	
    // url to webservice
    private String website_url = "http://192.168.0.27:8002";
    private String url_page = "RobotInfo";
	private String url_login = "LoginUser";
 	
    // JSON Node names
    private static final String TAG_SUCCESS = "ws_success";
    private static final String TAG_ITEMS = "items";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_FUNC = "function";

	private static final String TAG_ERROR = "ws_error";
	private static final String TAG_MESSAGE = "ws_error_message";
    public String last_error = "";
    
    public boolean json_connected = false;
    
    private static final String CONNECTION_ERROR = "Robot Engine not found, please check WIFI/Network connection/Firewall.";
    
    // products JSONArray
    JSONArray items = null;
   
	public DataProvider()
	{
		
		// init class
		
	}
	
	public void SetHost(String IP, String port)
	{
		 website_url = "http://" + IP + ":" + port;
	}
	
	public boolean AuthenticateUser(String username, String password)
	{
		// clean variables
		last_error = "";
		json_connected = true;
		String myPassword = password;

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("login", username));
		params.add(new BasicNameValuePair("passw", myPassword));


		// getting JSON Object
		// Note that create product url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_login, "GET", params);

		JSONArray items = new JSONArray();

		if (json != null)
		{
			// check for success tag
			try {
				// Check your log cat for JSON reponse
				Log.d("login result: ", json.toString());

				items = json.optJSONArray("items");

				JSONObject c = json.getJSONObject("items");

				int success = c.getInt("authenticated");

				if (success == 1)
				{
					String token = c.getString("token");

					return true;
				} else {
					String error = c.getString("error");
					last_error = error;
					return false;
				}
			} catch (JSONException e) {
				e.printStackTrace();

				last_error = CONNECTION_ERROR;
				json_connected = false;
				return false;
			}
		} else {
			last_error = CONNECTION_ERROR;
			json_connected = false;
			return false;
		}

	}

	public static String MD5_Hash(String input) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String md5 = number.toString(16);

			while (md5.length() < 32)
				md5 = "0" + md5;

			return md5;
		} catch (NoSuchAlgorithmException e) {
			Log.e("MD5", e.getLocalizedMessage());
			return null;
		}
	}
	
	// list of contacts
	public robotInfo getRobotInfo()
	{
		robotInfo myInfo = null;
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
 //     params.add(new BasicNameValuePair("user", ""));
        
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/" + url_page, "GET", params);

		JSONArray items = new JSONArray();

		if (json != null)
        {
           
            try {
            	 // Check your log cat for JSON reponse
                Log.d("All Items: ", json.toString());

				items = json.optJSONArray("items");

				JSONObject c = json.getJSONObject("items");

                 myInfo = new robotInfo();
         		
                 myInfo.Id = c.getString("Id");
                 myInfo.Name = c.getString("Name");
				 myInfo.Description = c.getString("Description");
                 myInfo.runtime = c.getInt("runtime");
                 myInfo.total_filled = c.getInt("total_filled");
                 myInfo.errors = c.getInt("num_errors");
                 myInfo.istatus = c.getInt("istatus");
                 myInfo.status = c.getString("status");
                 myInfo.order = c.getString("order");
				 myInfo.barcode = c.getString("barcode");
 				 myInfo.orderType = c.getString("order_type");
                 myInfo.product = c.getString("product");
                 myInfo.location = c.getString("location");
                 myInfo.amount = c.getInt("amount");
				 myInfo.amount_done = c.getInt("amount_done");
				 myInfo.error = c.getString("error");


			} catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        
        }
        
        // free mem
        json = null;
         
        return myInfo;
	}
	
	public cOrder getRobotOrder(String user, String orderid)
	{
		cOrder myOrder = null;
		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("object", "order"));
	       
		params.add(new BasicNameValuePair("orderid", orderid));
			         
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/getOrder", "GET", params);
 	   
        JSONArray items = new JSONArray(); 
        
        if (json != null)
        {
           
            try {
            	 // Check your log cat for JSON reponse
                Log.d("All Items: ", json.toString());
                 	
            	items = json.optJSONArray("items");
                
            	JSONObject c = json.getJSONObject("items");
 
            	myOrder = new cOrder();
         		
                myOrder.id = c.getString("orderid");
                myOrder.ordernr = c.getString("orderid");
                myOrder.barcode = c.getString("barcode");
                myOrder.order_type = c.getString("order_type");
                myOrder.req_location = c.getString("req_location");
                myOrder.del_location= c.getString("del_location");
                myOrder.priority = c.getString("priority");
                myOrder.datum = c.getString("datetime");
                myOrder.started = c.getString("started");
                myOrder.ended = c.getString("ended");
                myOrder.uname = c.getString("name");
                myOrder.bedrag = c.getString("bedrag");
                myOrder.processed = c.getString("processed");
                myOrder.payed = c.getString("payed");
                myOrder.status = c.getString("order_status");
                myOrder.error = c.getString("error");
                myOrder.procestime = c.getString("procestime");
                myOrder.waitingtime = c.getString("waitingtime");
             
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        
        }
        
        // free mem
        json = null;
         
        return myOrder;
	}
	
	 public List<cOrder> getRobotOrderList(String user)
	 {
		 List<cOrder> returnList = new ArrayList<cOrder>();
	
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
		 params.add(new BasicNameValuePair("object", "order"));
	       
		 params.add(new BasicNameValuePair("report_user", user));
			 
         // getting JSON Object
		 JSONObject json = jsonParser.makeHttpRequest(website_url + "/getOrders", "GET", params);
       
	  	 JSONArray items = new JSONArray(); 
	     if (json != null) 
    	 {    		 
			 try {
					 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
				
		      	int success = json.getInt(TAG_SUCCESS);
	
		      	if (items.length() > 0)
		      	{
		      		for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);
	
	                   cOrder myOrder = new cOrder();
	                                      
					    myOrder.id = c.getString("orderid");
					    myOrder.ordernr = c.getString("orderid");
					    myOrder.datum = c.getString("datetime");
					    myOrder.uname = c.getString("name");
					    myOrder.bedrag = c.getString("barcode");
					    myOrder.processed = c.getString("processed");
					    myOrder.payed = c.getString("payed");
						myOrder.status = c.getString("order_status");
						myOrder.order_type = c.getString("order_type");
						myOrder.barcode =  c.getString("barcode");
						myOrder.error= c.getString("error");
						myOrder.priority = c.getString("priority");

	                   returnList.add(myOrder);
	       		 }
			    		
	       	}
           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
       }
       
       return returnList;		 
	 }
	
	 
	 public List<cOrderProduct> getRobotOrderProductList(String orderid)
	 {
		 List<cOrderProduct> returnList = new ArrayList<cOrderProduct>();
	
		 List<NameValuePair> params = new ArrayList<NameValuePair>();
	       	        
		 params.add(new BasicNameValuePair("object", "orderproduct"));
	       
		 params.add(new BasicNameValuePair("orderid", orderid));
			 
         // getting JSON Object
		 JSONObject json = jsonParser.makeHttpRequest(website_url + "/getOrderProducts", "GET", params);
       
	  	 JSONArray items = new JSONArray(); 
	     if (json != null) 
    	 {    		 
			 try {
					 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
				
		      	int success = json.getInt(TAG_SUCCESS);
	
		      	if (items.length() > 0)
		      	{
		      		for (int i = 0; i < items.length(); i++) {
	                   JSONObject c = items.getJSONObject(i);
	
	                   cOrderProduct myProduct = new cOrderProduct();
	                                      
	                   myProduct.id = c.getString("id");
	                   myProduct.productnr = c.getString("productid");
	                   myProduct.product = c.getString("productname");
	                   myProduct.location = c.getString("location");
	                   myProduct.amount = c.getString("amount");
	                   myProduct.amount_done = c.getString("amount_done");
	                   myProduct.processed = c.getString("processed");
	                   myProduct.delivered = c.getString("delivered");
					   myProduct.resID = c.getString("productid");
	            	                         
	                   returnList.add(myProduct);
	       		 }
			    		
	       	}
           	
	       } catch (JSONException e) {
	           e.printStackTrace();
	       }
       }
       
       return returnList;		 
	 }
	
	
	public List<cResourceGroup> getResourceGroup()
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_group"));
             
    	// result list
		List<cResourceGroup> returnList = new ArrayList<cResourceGroup>();
	
	    // getting JSON Object
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/getProductGroups", "GET", params);
	       
		JSONArray items = new JSONArray(); 
	
	    try {
		  	 if (json != null) 
	    	 {    		 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
					
		    	if (items.length() > 0)
		    	{
		    		 for (int i = 0; i < items.length(); i++) {
		                JSONObject c = items.getJSONObject(i);
		
		                cResourceGroup myResource = new cResourceGroup();
		                
		                myResource.id = c.getLong("groupid");
		                myResource.name = Html.fromHtml(c.getString("groupname")).toString();
		                myResource.description = c.getString("description");
		 		    	returnList.add(myResource);
		    		 }
			    		
		    		}
	    	 }
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cResource> getResources(String groupSel)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("groupid", groupSel));
    	// result list
		List<cResource> returnList = new ArrayList<cResource>();
	
	    // getting JSON Object
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/getProducts", "GET", params);
	       
		JSONArray items = new JSONArray(); 
	
		try {
		  	 if (json != null) 
	    	 {    		 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
				
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);
	
	                    cResource myResource = new cResource();
	                    
	                    myResource.id = c.getString("productid");
	                    myResource.code = c.getString("productid");
	                    myResource.name = c.getString("productname");
	          //          myResource.status = c.getString("status");
	          //          myResource.User = c.getString("user");
	                    myResource.groupID = "0";
	     		    	returnList.add(myResource);
	        		 }
			    		
	        	}
	    	 }
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}

	public List<cLocation> getLocations(String resource)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("productid", resource));
    	// result list
		List<cLocation> returnList = new ArrayList<cLocation>();
	
	    // getting JSON Object
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/getProductLocations", "GET", params);
	       
		JSONArray items = new JSONArray(); 
	
		try {
		  	 if (json != null) 
	    	 {    		 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
				
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);
	
	                    cLocation myLocation = new cLocation();
	                    
	                    myLocation.id = c.getString("locationid");
	                    myLocation.code = c.getString("code");
	                    myLocation.name = c.getString("name");
	                    myLocation.stock = c.getLong("stock");
	     		    	returnList.add(myLocation);
	        		 }
			    		
	        	}
	    	 }
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	public List<cMutation> getMutations(String resource_id)
	{
		 // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "mutation"));
        params.add(new BasicNameValuePair("productid", resource_id));
		params.add(new BasicNameValuePair("locationid", ""));

		// getting JSON Object
        // getting JSON Object
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/GetMutations", "GET", params);
	       
		JSONArray items = new JSONArray(); 
	             
    	// result list
		List<cMutation> returnList = new ArrayList<cMutation>();
	
		try {
		  	 if (json != null) 
	    	 {    		 
				items = json.optJSONArray("items");
				    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
				
	        	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
	                    JSONObject c = items.getJSONObject(i);
	
	                    cMutation myMutation = new cMutation();
	                    
	                    myMutation.id = c.getString("mutationid");
	                    myMutation.datetime = c.getString("datetime");
	                    myMutation.location = c.getString("location");
	                    myMutation.user = c.getString("user");
	                    myMutation.resource = c.getString("productname");
	                    myMutation.resource_code = c.getString("productid");
	                    myMutation.mutation_type = c.getString("mutation_type");
	                    myMutation.amount = c.getLong("change_stock");
	                    myMutation.stock_after = c.getLong("stock_after");
	                    returnList.add(myMutation);
	        		 }
		    		
	        	}
	    	 }
            	
        } catch (JSONException e) {
            e.printStackTrace();
        }
       	return returnList;

	}
	
	
	
	// resource detail
	public cResource getResource(String id)
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("product", id));

        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/getProduct", "GET", params);
 	   
        cResource myResource = new cResource();
        
        JSONArray items = new JSONArray(); 
        
        if (json != null)
        {
           
            try {
            	 // Check your log cat for JSON reponse
                Log.d("All Items: ", json.toString());
                 	
            	items = json.optJSONArray("items");
                
               	if (items.length() > 0)
	        	{
	        		 for (int i = 0; i < items.length(); i++) {
		                    JSONObject c = items.getJSONObject(i);
		
				    	myResource.id = c.getString("productid");
		            	myResource.name = c.getString("productname");
		            	myResource.code = c.getString("productid");
		       //     	myResource.status = c.getString("status");
		            	myResource.description = c.getString("productname");
		        //    	myResource.User = c.getString("user");
		       //     	myResource.barcode = c.getString("barcode");
		          //  	myResource.location = c.getString("location");
		          //  	myResource.stock = c.getString("stock");
		            	
		            	//myResource.location = c.getString("location");

		            	myResource.image_id = c.getString("productid");
	        		 }
	            	
	        	}
            
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } 	
        }
        return myResource;
	}
	
	 public boolean updateStock(String resID, String locID, long stock_change)
	 {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "stock"));
        params.add(new BasicNameValuePair("productid", resID));
        params.add(new BasicNameValuePair("locationid", locID));
        params.add(new BasicNameValuePair("stock_change", String.valueOf(stock_change)));
        
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(website_url + "/updateStock", "GET", params);
 	    
	   	 if (json != null) 
    	 {    		 
			 try {
				 
		    	int success = json.getInt(TAG_SUCCESS);
		    	
		        if (success == 1)
		        {
		        	return true;
		        } else {
		        	return false;
		        }
		   	} catch (JSONException e) {
			   e.printStackTrace();
			        
		   	}
	    	            
    	 } else {
	    		 
    		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
    	 }
	   	 
	   	 return false;
	 }
	 
	public String getImage(String id)
	 {
		 String result = "";
    
	     try 
	     {
	    	 URL url = new URL(website_url + "/getImage?imageid="+ id);
	         BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

	         StringBuilder sb = new StringBuilder();

	         String inputLine;
	         while ((inputLine = in.readLine()) != null)
	         {
	        	 sb.append(inputLine);
	         }
	         result = sb.toString(); 
	        	 
	         in.close();
	     
	     }
	     catch (Exception e) 
	     {
	    	 e.printStackTrace();
	     }
	     
	     return result;
	 }

	public cResource getResourcebyLocationBarcode(String barcode)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "resource"));
		params.add(new BasicNameValuePair("locationid", barcode));

		// getting JSON string from URL
		JSONObject json = jsonParser.makeHttpRequest(website_url + "/GetProductByLocation", "GET", params);

		cResource myResource = new cResource();

		JSONArray items = new JSONArray();

		if (json != null) {

			try {
				// Check your log cat for JSON reponse
				Log.d("All Items: ", json.toString());

				items = json.optJSONArray("items");

				JSONObject c = json.getJSONObject("items");

				myResource = new cResource();

				myResource.id = c.getString("productid");
				myResource.name = c.getString("productname");
				myResource.code = c.getString("productid");
				myResource.description = c.getString("productname");
				myResource.stock = c.getString("stock");
				myResource.location = c.getString("location");
				myResource.image_id = c.getString("productid");

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			last_error = CONNECTION_ERROR;
			json_connected = false;
		}

		return myResource;

	}

}
