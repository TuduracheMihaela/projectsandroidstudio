package com.sharethatdata.snowcloud_webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.jsonlibrary.JSONParserForPassword;
import com.sharethatdata.snowcloud_webservice.datamodel.cContact;
import com.sharethatdata.snowcloud_webservice.datamodel.cLocation;
import com.sharethatdata.snowcloud_webservice.datamodel.cReservations;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

/**
 * Created by gotic_000 on 2/21/2018.
 */

public class WSDataProvider {

    private Context context;
    private SharedPreferences sharedPrefs;

    JSONParser jsonParser = new JSONParser();
    JSONParserForPassword jsonParserForPassword = new JSONParserForPassword();

    private String webservice_url = "https://dev-ws.sharethatdata.com/snowcloud";
    //private String webservice_url = "http://www.sharethatdata.com/stock-ws";
    private String webshop = "pneumatiekvoordeel.nl";

    private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    private String url_create_item = "create_item.php";
    private String url_update_item = "update_item.php";
    private String url_delete_item = "delete_item.php";
    private String request_reset_password = "requestReset.php";
    private String process_reset_password = "processResetRequest.php";

    private String url_login = "login.php";
    private String url_log = "log_event.php";

    private static final String PRINT_TAG_SUCCESS = "success";
    private static final String TAG_SUCCESS = "ws_success";
    private static final String TAG_ERROR = "ws_error";
    private static final String TAG_MESSAGE = "ws_error_message";
    private static final String TAG_UPDATE = "update";

    private static final String TAG_ITEMS = "items";

    private static final String CONNECTION_ERROR = "Web service not found, please check WIFI/Network connection.";

    public String last_error = "";
    public String update = "";

    public boolean json_connected = false;

    private String myUser = "";
    private String myPass = "";
    private String myApp = "";

    private String ip = "";
    private String android_id = "";
    private String appname = "";

    public WSDataProvider(Context context)
    {
        this.context = context;
    }

    /*public void startServicefromNonActivity(){

   	 *//*  Intent intent=new Intent(context, MyService.class);
   	   context.startService(intent);*//*

        AlarmManager alarmManagerMessage = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intentMessage = new Intent(context, MyReceiver.class);
        PendingIntent pendingIntent= PendingIntent.getBroadcast(context, 0, intentMessage, 0);

        SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
        boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);
        if(blNotifications){
            // true - start service
            alarmManagerMessage.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),180000,
                    pendingIntent); // 180000 3 min - 300000 5 min
        }else{
            // false - stop service
            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            manager.cancel(pendingIntent);
        }

    }*/

    public WSDataProvider(String new_user, String new_pass)
    {
        myUser = new_user;
        myPass = new_pass;

        String currentSESSION = WSSession.getInstance().getString();
        if(!currentSESSION.equals("")){

            myPass = WSSession.getInstance().getPassword();
            myApp = WSSession.getInstance().getAppName();
            android_id = WSSession.getInstance().getDevice();
            ip = WSSession.getInstance().getIP();
            android_id = WSSession.getInstance().getDevice();
        }
    }

    public void SetWebServiceUrl(String new_url)
    {
        if (!new_url.equals("")){
            webservice_url = new_url;

            // extract webshop
            String webshop_url = webservice_url.replace("http://www.", "");
            webshop_url = webshop_url.replace("/stock-ws","");
            webshop = webshop_url;
        }

        System.out.println("URL: " + webservice_url);

        // save web service url for notifications
        //SharedPreferences.Editor editor = context.getSharedPreferences("PREF_URL", 0).edit();
        // editor.putString("URL", String.valueOf(webservice_url));
        //  editor.commit();
    }

    public String GetWebServiceUrl(){
        return webservice_url;
    }

    public void SetDeviceIdentification(String new_ip, String new_android_id, String new_appname)
    {
        ip = new_ip;
        appname = new_appname;
        android_id = new_android_id;
    }

    public static String MD5_Hash(String input) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);

            while (md5.length() < 32)
                md5 = "0" + md5;

            return md5;
        } catch (NoSuchAlgorithmException e) {
            Log.e("MD5", e.getLocalizedMessage());
            return null;
        }
    }

    public boolean LogEvent(String event)
    {
        // no more logging from client
        String user = myUser;
        String datetime = "";

        try
        {
            Date d = new Date();
            datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        } catch (Exception e)
        {
        }

        // get values from device

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        // getting JSON Object
        //   JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_log, "GET", params);

        // check for success tag
        //   try {
        //     int success = json.getInt(TAG_SUCCESS);

        return true;
        //     } catch (JSONException e) {
        //         e.printStackTrace();
        //         return false;
        //     }
    }

    public boolean CheckLogin(String login, String password, String app, String version)
    {
        return CheckLoginWithReconnect(login, password, app, version, false);
    }

    public boolean CheckLoginWithReconnect(String login, String password, String app, String version,  boolean reconnect)
    {
        // clean variables
        last_error = "";
        json_connected = true;
        String myPassword = password;
        String currentSESSION = WSSession.getInstance().getString();
        if(currentSESSION.equals("") || reconnect == false){
            myPassword = MD5_Hash(password);
        }
        // not hashed for digiorder
        if (app.equals("DigiOrder")) myPassword = password;

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("version", version));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        params.add(new BasicNameValuePair("ip", ip));

        myApp = app;
        myUser = login;
        myPass = myPassword;

        if(context != null){
            // save user and password for notifications
            SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
            editor.putString("USER", String.valueOf(myUser));
            editor.putString("PASS", String.valueOf(myPass));
            editor.putString("URL", String.valueOf(webservice_url));
            editor.commit();
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_login, "GET", params);


        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    String token = json.getString("ws_token");
                    WSSession.getInstance().setString(token);
                    WSSession.getInstance().setUserPass(myUser, myPass);
                    WSSession.getInstance().setAppName(myApp, android_id);
                    try{
                        update = json.getString(TAG_UPDATE);
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    return true;
                } else {
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();

                last_error = CONNECTION_ERROR;
                json_connected = false;
                return false;
            }
        } else {
            last_error = CONNECTION_ERROR;
            json_connected = false;
            return false;
        }

    }

    // General Method for calling new web service
    // gets a list of json items in array
    // when something goes wrong, an empty array is returned
    private JSONArray CallWebService(List<NameValuePair> new_params, String url, boolean doPost)
    {
        // clean variables
        last_error = "";
        json_connected = true;

        String currentSESSION = WSSession.getInstance().getString();
        if(currentSESSION.equals("")){

            boolean bool = CheckLogin(myUser, myPass, myApp, "");
            if(bool){
                currentSESSION = WSSession.getInstance().getString();
            }
        }

        // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
                SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
                String user = prefs.getString("USER", "");
                String pass = prefs.getString("PASS", "");

                myUser = user;
                myPass = pass;
            }
        }


        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
            params.add(p);
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        String post_str = "GET";
        if (doPost)
        {
            post_str = "POST";
        }

        JSONObject json = jsonParser.makeHttpRequest(url, post_str, params);
        JSONArray items = new JSONArray();
        if (json != null)
        {
            try {

                items = json.optJSONArray("items");

                // Check your log cat for JSON reponse
                Log.d("All items: ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    if (items == null){
                        items = new JSONArray();
                    }else{
                        System.out.println("items: " + items);
                    }
                } else {
                    // record error
                    items = new JSONArray();
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;

                    // watch and learn....
                    // token error, login again
                    int erronr = json.getInt(TAG_ERROR);
                    if(error.equals("User has no access to this company.")){
                        return items;

                    }else if (erronr == 10004) {
                        boolean bool = CheckLoginWithReconnect(myUser, myPass, myApp, "", true);
                        if (bool) {
                            currentSESSION = WSSession.getInstance().getString();


                            // call url again to get data now!!!!
                            // ..
                            // ...
                            items = CallWebService(new_params, url, doPost);

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        } else {

            last_error = CONNECTION_ERROR;
            json_connected = false;

        }
        return items;
    }

    private JSONArray CallWebServiceForPassword(List<NameValuePair> new_params, String url, boolean doPost)
    {
        // clean variables
        last_error = "";
        json_connected = true;

        String currentSESSION = WSSession.getInstance().getString();

        // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
                SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
                String user = prefs.getString("USER", "");
                String pass = prefs.getString("PASS", "");

                myUser = user;
                myPass = pass;
            }
        }


        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
            params.add(p);
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        String post_str = "GET";
        if (doPost)
        {
            post_str = "POST";
        }

        JSONObject json = jsonParserForPassword.makeHttpRequest(url, post_str, params);
        JSONArray items = new JSONArray();
        if (json != null)
        {
            try {

                items = json.optJSONArray("items");

                // Check your log cat for JSON reponse
                Log.d("All items: ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    if (items == null){
                        items = new JSONArray();

                        String error = json.getString(TAG_MESSAGE);
                        items = new JSONArray();
                        items.put(error);

                    }else{
                        System.out.println("items: " + items);
                    }
                } else {
                    // record error
                    items = new JSONArray();
                    String error = json.getString(TAG_MESSAGE);
                    last_error = error;

                    if(error.contains("Security token incorrect.")){
                        System.out.println("TOKEN EXPIRED");
                        System.out.println("TOKEN EXPIRED myUser: " + myUser);
                        System.out.println("TOKEN EXPIRED myPass: " + myPass);

                        boolean bool = CheckLogin(myUser, myPass, myApp, "");

                        System.out.println("bool: " + bool);

                        if(bool){
                            JSONArray items_again = CallWebService(new_params, url, false);
                            items = items_again;

                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();

            }

        } else {

            last_error = CONNECTION_ERROR;
            json_connected = false;

        }
        return items;
    }

    // get Web Services




    /**
     *  *** CONTACT ***
     */

    // GET USER ROLE LIST
    public ArrayList<String> getUserRoles(String id, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_role"));
        params.add(new BasicNameValuePair("role_user", id));
        params.add(new BasicNameValuePair("company", company));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        ArrayList<String> roles = new ArrayList<String>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    String str_name = c.getString("name");
                    roles.add(str_name);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return roles;
    }

    // GET USER ID BY USER NAME
    public List<cContact> getUserIdbyUsername(String username, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user"));
        params.add(new BasicNameValuePair("name", username));
        params.add(new BasicNameValuePair("company", company));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        List<cContact> returnList = new ArrayList<cContact>();

        // result list
        //List<cUserId> returnList = new ArrayList<cUserId>();
        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);
                    cContact myContact = new cContact();

                    myContact.id = c.getString("id");
                    myContact.name = c.getString("name");

                    returnList.add(myContact);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    /**
     *  *** IMAGE ***
     */

    // GET IMAGE
    public String getImage(String id, String company)
    {
        String result = "";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "image"));
        params.add(new BasicNameValuePair("imageid", id));
        params.add(new BasicNameValuePair("fullimage", "true"));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = null;

        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        try
        {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);
                result = c.getString("full_image");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    // CREATE IMAGE
    public int createImage(String image_type, byte[] myImage, byte[] myImageThumbnail, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "image"));
        params.add(new BasicNameValuePair("company", company));

        //params.add(new BasicNameValuePair("image_type", "image_report"));
        params.add(new BasicNameValuePair("image_type", image_type));
        //params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
        params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

        int imageID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    imageID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return imageID;
    }

    public boolean uploadMultipart(Context context, String FilePath1) {
        
        boolean upload = false;

        if (FilePath1 == null) {
            Log.d("PDF","Please move your .pdf file to internal storage and retry");
            //Toast.makeText(this, "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
            upload = false;
        } else {
            //Uploading code
            try {
                String uploadId = UUID.randomUUID().toString();

                final String UPLOAD_URL = "https://dev-ws.sharethatdata.com/snowcloud/create_item.php";

                //Creating a multi part request
                new MultipartUploadRequest(context, uploadId, UPLOAD_URL)
                        .addFileToUpload(FilePath1, "file") //Adding file
                        .addParameter("object", "resource_file") //Adding text parameter to the request
                        .addParameter("file_type", "pdf") //Adding text parameter to the request
                        .addParameter("file", "pdf") //Adding text parameter to the request
                        .setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(1)
                        .startUpload(); //Starting the upload
                
                upload = true;

            } catch (Exception exc) {
                exc.printStackTrace();
                //Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        return upload;
    }

    // CREATE PDF
    public String createPdf(String file_type, byte[] myFile, String company)
    //public int createPdf(String file_type, FileBody myFile, String filepath, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "resource_file"));
        params.add(new BasicNameValuePair("company", company));

        //params.add(new BasicNameValuePair("image_type", "image_report"));
        params.add(new BasicNameValuePair("file_type", file_type));
        //params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.URL_SAFE)));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("file", Base64.encodeToString(myFile, Base64.NO_WRAP)));
        //params.add(new BasicNameValuePair("file", myFile));
        //params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);
        //JSONObject json = jsonParser.makeHttpRequest1(webservice_url + "/" + url_create_item, "MULTIPOST", params, myFile, filepath);

        int fileID = 0;
        int imageID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    fileID = json.getInt("inserted_id");
                    imageID = json.getInt("image_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return String.valueOf(fileID + "," + imageID);
    }

    /**
     *  *** RESOURCE ***
     */

    /**
     * GET ITEM
     */

    // RESOURCE DETAIL DIGI RESOURCE
    public cResource getResource(String id, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("image", "thumb"));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = null;

        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cResource myResource = new cResource();

        try {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);

                myResource.id = c.getString("id");
                myResource.name = c.getString("name");
                myResource.code = c.getString("code");
                myResource.status = c.getString("status");
                myResource.description = c.getString("description");
                //myResource.User = c.getString("user");
                myResource.barcode = c.getString("barcode");
                myResource.location = c.getString("location");
                myResource.image_id = c.getString("image_id");
                myResource.image_full = c.getString("image");
                //myResource.bookamount = c.getLong("book_amount");
                myResource.groupID = c.getString("resource_group");

                myResource.can_take = c.getString("can_take");
                myResource.can_reserve = c.getString("can_reserve");
                myResource.must_return = c.getString("must_return");
                myResource.hold_stock = c.getString("hold_stock");
                myResource.need_scan = c.getString("need_scan");
                myResource.is_locked = c.getString("is_locked");
                myResource.return_days = c.getString("return_days");

                if(!myResource.barcode.toString().equals("null")){
                    myResource.barcode = c.getString("barcode");
                }else {
                    myResource.barcode = "";
                }

                if(!myResource.location.toString().equals("null")){
                    myResource.location = c.getString("location");
                }else {
                    myResource.location = "";
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return myResource;
    }

    // RESOURCE DETAIL DIGI FILE FINDER
    public cResource getResourceDetail(String id, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("company", company));
        params.add(new BasicNameValuePair("image", "thumb"));

        // Building Parameters
        /*List<NameValuePair> params1 = new ArrayList<NameValuePair>();
        params1.add(new BasicNameValuePair("object", "resource"));
        params1.add(new BasicNameValuePair("id", id));
        params1.add(new BasicNameValuePair("company", company));
        params1.add(new BasicNameValuePair("image", "full"));*/

        JSONArray items = null;

        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);
        //items1 = CallWebService(params1, webservice_url + "/" + url_get_item_new, false);

        cResource myResource = new cResource();

        try {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);

                myResource.id = c.getString("id");
                myResource.code = c.getString("code");
                myResource.name = c.getString("name");
                myResource.description = c.getString("description");
                myResource.search = c.getString("search");
                myResource.groupID = c.getString("resource_group");
                myResource.image_id = c.getString("image_id");
                myResource.image_thumb = c.getString("image");
                myResource.owner = c.getString("user");
                myResource.inuse = c.getString("InUse");
                myResource.barcode = c.getString("barcode");
                myResource.location = c.getString("location");
                myResource.status = c.getString("status");
                myResource.created_date = c.getString("created");
                myResource.return_date = c.getString("return_date");
                myResource.resource_type = c.getString("resource_type");
                myResource.file_id = c.getString("file_id");
                //myResource.book_amount = c.getString("book_amount");

                if(myResource.image_thumb.equals("image_array")){
                    JSONArray image_array = c.getJSONArray("image_array");

                    for (int i = 0; i < image_array.length(); i++) {
                        JSONObject cc = image_array.getJSONObject(i);

                        myResource.image_id = cc.getString("image_id");
                        myResource.image_array_string = cc.getString("image_thumb");

                        cResource myyResource = new cResource(myResource.image_id, myResource.image_array_string);
                        myResource.list.add(myyResource);
                        //list.add(myyResource);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*try {
            if (items1.length() > 0)
            {
                JSONObject c = items1.getJSONObject(0);

                myResource.image_full = c.getString("image");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        return myResource;
    }

    // RESOURCE DETAIL DIGI FILE FINDER - IMAGE
    public cResource getResourceDetailImage(String id, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("company", company));
        params.add(new BasicNameValuePair("image", "full"));

        JSONArray items = null;

        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cResource myResource = new cResource();

        try {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);

                myResource.image_id = c.getString("image_id");
                myResource.image_full = c.getString("image");

                if(myResource.image_full.equals("image_array")){
                    JSONArray image_array = c.getJSONArray("image_array");

                    for (int i = 0; i < image_array.length(); i++) {
                        JSONObject cc = image_array.getJSONObject(i);

                        myResource.image_id = cc.getString("image_id");
                        myResource.image_array_string = cc.getString("image_full");

                        cResource myyResource = new cResource(myResource.image_id, myResource.image_array_string);
                        myResource.list.add(myyResource);
                        //list.add(myyResource);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myResource;
    }

    // RESOURCE DETAIL DIGI FILE FINDER - FILES (PDF/TEXT)
    public cResource getResourceDetailFile(String id, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_file"));
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = null;

        // getting JSON Object
        items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cResource myResource = new cResource();

        try {
            if (items.length() > 0)
            {
                JSONObject c = items.getJSONObject(0);

                myResource.id = c.getString("id");
                myResource.file_type = c.getString("file_type");
                myResource.filename = c.getString("filename");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myResource;
    }

    /**
     * GET ITEMS
     */

    // RESOURCE LIST
    public List<cResource> getResources(String groupSel, String company, String keyword_search, int pagenr)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("company", company));
        params.add(new BasicNameValuePair("image", "thumb"));

        if(!groupSel.equals("")) params.add(new BasicNameValuePair("groupid", groupSel));
        if(!keyword_search.equals("")) params.add(new BasicNameValuePair("search", keyword_search));
        if(pagenr != 0) params.add(new BasicNameValuePair("page", String.valueOf(pagenr)));

        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cResource> returnList = new ArrayList<cResource>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();

                    myResource.id = c.getString("id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.location = c.getString("location");
                    myResource.status = c.getString("status");
                    myResource.user = c.getString("user");
                    myResource.group_name = c.getString("resource_group_name");
                    myResource.image_thumb = c.getString("thumb");
                    returnList.add(myResource);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    // RESOURCE LIST BY BARCODE
    public List<cResource> getResourcesbyBarcode(String barcode, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_barcode"));
        params.add(new BasicNameValuePair("barcode", barcode));
        params.add(new BasicNameValuePair("company", company));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cResource> returnList = new ArrayList<cResource>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();

                    myResource.id = c.getString("resource_id");
                    myResource.code = c.getString("code");
                    myResource.name = c.getString("name");
                    myResource.status = c.getString("status");
                    myResource.user = c.getString("user");
                    myResource.groupID = c.getString("resource_group");
                    myResource.barcode = c.getString("barcode");
                    returnList.add(myResource);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    // RESOURCE GROUP LIST
    public List<cResourceGroup> getResourceGroup(String company, String userid)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_group"));
        params.add(new BasicNameValuePair("company", company));
        if(!userid.equals("")) params.add(new BasicNameValuePair("report_user", userid)); // report_user is for DigiFileFinder, groups created per user id
        //params.add(new BasicNameValuePair("groupid", "0"));
        //params.add(new BasicNameValuePair("filtered", "0"));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cResourceGroup> returnList = new ArrayList<cResourceGroup>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResourceGroup myResource = new cResourceGroup();

                    myResource.id = c.getLong("id");
                    myResource.name = c.getString("name");
                    myResource.description = c.getString("description");
                    returnList.add(myResource);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;

    }

    // RESERVATION LIST
    public List<cReservations> getReservations( String resourceid, String company)
    {
        // result list
        List<cReservations> returnList = new ArrayList<cReservations>();

        Date d = new Date();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(d);

        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //String currentDateandTime = sdf.format(new Date());

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation"));
        params.add(new BasicNameValuePair("resource_id", resourceid));
        params.add(new BasicNameValuePair("date", currentDate));
        //params.add(new BasicNameValuePair("user", user));
        //params.add(new BasicNameValuePair("all", all));
        params.add(new BasicNameValuePair("company", company));
        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // check for success tag
        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReservations myRecord = new cReservations();

                    myRecord.id = c.getString("id");
                    myRecord.description = c.getString("description");
                    myRecord.start_date = c.getString("start");
                    myRecord.end_date = c.getString("end");
                    myRecord.user=c.getString("user");

                    if(!myRecord.start_date.equals("0000-00-00 00:00:00")){
                        SimpleDateFormat json_format_start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        try {
                            Date dateStart = json_format_start.parse(myRecord.start_date);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateStart.before(today) )
                            {
                                new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myRecord.start_date = new_format_start.format(dateStart);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else {
                        myRecord.start_date = "";
                    }


                    if(!myRecord.end_date.equals("0000-00-00 00:00:00")){
                        SimpleDateFormat json_format_end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        try {
                            Date dateEnd = json_format_end.parse(myRecord.end_date);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateEnd.before(today) )
                            {
                                new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myRecord.end_date = new_format_end.format(dateEnd);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else {
                        myRecord.end_date = "";
                    }


                    returnList.add(myRecord);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    // RESOURCES RESERVED (RESERVATION) USER LIST
    public List<cReservations> getMyResourcesReserved(String userfor, String resid, String date, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "reservation"));
        if(!userfor.equals("")) params.add(new BasicNameValuePair("report_user", userfor));
        if(!resid.equals("")) params.add(new BasicNameValuePair("resid", resid));
        if(!date.equals("")) params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cReservations> returnList = new ArrayList<cReservations>();
        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cReservations myReservation = new cReservations();

                    myReservation.id = c.getString("id");
                    myReservation.description = c.getString("description");
                    myReservation.start_date = c.getString("start");
                    myReservation.end_date = c.getString("end");
                    myReservation.resource_id = c.getString("resource_id");
                    myReservation.resource_name = c.getString("resource");
                    myReservation.user=c.getString("user");
                    myReservation.visit_id = c.getString("visit_id");
                    myReservation.reservation_type = c.getString("reservation_type");
                    myReservation.reservation_status = c.getString("reservation_status");

                    if(!myReservation.start_date.equals("0000-00-00 00:00:00")){
                        SimpleDateFormat json_format_start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        try {
                            Date dateStart = json_format_start.parse(myReservation.start_date);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateStart.before(today) )
                            {
                                new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myReservation.start_date = new_format_start.format(dateStart);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else {
                        myReservation.start_date = "";
                    }


                    if(!myReservation.end_date.equals("0000-00-00 00:00:00")){
                        SimpleDateFormat json_format_end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        try {
                            Date dateEnd = json_format_end.parse(myReservation.end_date);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateEnd.before(today) )
                            {
                                new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myReservation.end_date = new_format_end.format(dateEnd);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else {
                        myReservation.end_date = "";
                    }


                    returnList.add(myReservation);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    // RESOURCES TAKEN USER LIST
    public List<cResource> getMyResourcesTaken(String userfor, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "resource_user"));
        if(!userfor.equals("")) params.add(new BasicNameValuePair("report_user", userfor));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cResource> returnList = new ArrayList<cResource>();
        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cResource myResource = new cResource();

                    myResource.id = c.getString("id");
                    myResource.resourceid = c.getString("resourceid");
                    myResource.resource = c.getString("resource");
                    myResource.user = c.getString("user");
                    myResource.userid = c.getString("userid");
                    myResource.datetime = c.getString("datetime");
                    myResource.return_date = c.getString("return_date");
                    myResource.amount = c.getLong("amount");

                    if(!myResource.datetime.equals("0000-00-00 00:00:00")){
                        try {
                            SimpleDateFormat json_format_start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            SimpleDateFormat new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                            Date dateStart = json_format_start.parse(myResource.datetime);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateStart.before(today) )
                            {
                                new_format_start = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myResource.datetime = new_format_start.format(dateStart);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else{
                        myResource.datetime = "";
                    }

                    if(!myResource.return_date.equals("0000-00-00 00:00:00")){
                        SimpleDateFormat json_format_end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        try {
                            Date dateEnd = json_format_end.parse(myResource.return_date);
                            Date today = new Date();
                            today=new Date(today.getTime()-(24*60*60*1000));
                            if (dateEnd.before(today) )
                            {
                                new_format_end = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            }
                            myResource.return_date = new_format_end.format(dateEnd);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }else{
                        myResource.return_date = "";
                    }



                    returnList.add(myResource);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cLocation> getLocations(String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "location"));
        params.add(new BasicNameValuePair("company", company));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cLocation> returnList = new ArrayList<cLocation>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cLocation myLocation = new cLocation();

                    myLocation.id = c.getString("id");
                    myLocation.name = c.getString("name");
                    myLocation.code = c.getString("code");
                    myLocation.barcode = c.getString("barcode");
                    myLocation.resource = c.getString("resource");
                    myLocation.stock = c.getString("stock");

                    returnList.add(myLocation);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public List<cContact> getUsers(String report_user, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user"));
        params.add(new BasicNameValuePair("report_user", report_user));
        params.add(new BasicNameValuePair("company", company));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

        // result list
        List<cContact> returnList = new ArrayList<cContact>();

        try {
            if (items.length() > 0)
            {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    cContact myContact = new cContact();

                    myContact.id = c.getString("id");
                    myContact.name = c.getString("name");
                    //myContact.username = c.getString("username");

                    returnList.add(myContact);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnList;

    }


    /**
     * CREATE
     */

    // RESOURCE DIGI RESOURCE
    public int createResource(String code, String name, String description, String group_id, String location,
                                    boolean can_take, boolean can_reserve, boolean must_return, boolean hold_stock, boolean need_scan,
                                    boolean is_locked, String return_days,
                                    byte[] myImage, String image_type, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("group_id", group_id));
        params.add(new BasicNameValuePair("location", location));

        if(can_take == true)params.add(new BasicNameValuePair("can_take", "1"));
        if(can_reserve == true)params.add(new BasicNameValuePair("can_reserve", "1"));
        if(must_return == true)params.add(new BasicNameValuePair("must_return", "1"));
        if(hold_stock == true)params.add(new BasicNameValuePair("hold_stock", "1"));
        if(need_scan == true)params.add(new BasicNameValuePair("need_scan", "1"));
        if(is_locked == true)params.add(new BasicNameValuePair("is_locked", "1"));
        if(!return_days.equals(""))params.add(new BasicNameValuePair("return_days", return_days));

        params.add(new BasicNameValuePair("company", company));

        JSONObject json;
        if(!image_type.equals("")){
            params.add(new BasicNameValuePair("image_type", image_type));
            params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
            json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "POST", params);
        }else {
            json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);
        }

        //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);


        int resourceID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    resourceID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return resourceID;
    }


    // RESOURCE DIGI FILE FINDER
    public int createResourceDetail(String name, String location, String description, String keywords,
                                        String group_id, String owner, String image_id, String file_id, String resource_type, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

        params.add(new BasicNameValuePair("company", company));

        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("location", location));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("search", keywords));

        params.add(new BasicNameValuePair("group_id", group_id));
        params.add(new BasicNameValuePair("image_id", image_id));
        params.add(new BasicNameValuePair("file_id", file_id));
        params.add(new BasicNameValuePair("owner", owner));
        params.add(new BasicNameValuePair("resource_type", resource_type));

        //params.add(new BasicNameValuePair("code", ""));
        //params.add(new BasicNameValuePair("description", ""));
        //params.add(new BasicNameValuePair("inuse", ""));

        /*JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }*/

        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

        int resourceID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    resourceID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return resourceID;

    }

    // DIGIFILEFINDER - MULTIPLE IMAGE RESOURCE
    public boolean addMultipleImageToResource(String imageId, String resourceId, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "image_resource_multiple"));

        params.add(new BasicNameValuePair("company", company));

        params.add(new BasicNameValuePair("image_id", imageId));
        params.add(new BasicNameValuePair("resource_id", resourceId));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }


    // RESOURCE GROUP
    public int createResourceGroup(String name, String description, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "resource_group"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("company", company));

        //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

        int resourceGroupID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    resourceGroupID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return resourceGroupID;
    }

    // BARCODE
    public boolean addBarcodeToResource(String resourceId, String barcode, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource_barcode"));

        params.add(new BasicNameValuePair("resource_id", resourceId));
        params.add(new BasicNameValuePair("barcode", barcode));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }

    // RESOURCE RESERVATION
    public int createResourceReservation(String reservation_type, String resource, String description, String start, String end, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "reservation"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

        params.add(new BasicNameValuePair("reservation_type", reservation_type));
        params.add(new BasicNameValuePair("resource", resource));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("start", start));
        params.add(new BasicNameValuePair("end", end));
        params.add(new BasicNameValuePair("company", company));

        //JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

        int resourceGroupID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    resourceGroupID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return resourceGroupID;
    }

    // LOCATION
    public int createLocation (String barcode, String name, String code, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String currentSESSION = WSSession.getInstance().getString();

        params.add(new BasicNameValuePair("object", "location"));
        params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));

        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("barcode", barcode));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("company", company));

        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "GET", params);

        int locationID = 0;
        if (json != null)
        {
            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1)
                {
                    locationID = json.getInt("inserted_id");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return locationID;
    }

    /**
     * UPDATE
     */

    // RESOURCE DIGI RESOURCE
    public boolean editResource(String resourceId, String code, String name, String description, String group_id, String location,
                                boolean can_take, boolean can_reserve, boolean must_return, boolean hold_stock, boolean need_scan,
                                boolean is_locked, String return_days,
                                byte[] myImage, String image_type, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource"));

        params.add(new BasicNameValuePair("id", resourceId));
        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("group_id", group_id));
        params.add(new BasicNameValuePair("location", location));
        //params.add(new BasicNameValuePair("image_id", image_id));
        //params.add(new BasicNameValuePair("owner", owner));
        //params.add(new BasicNameValuePair("inuse", inuse));
        //params.add(new BasicNameValuePair("status", status));
        //params.add(new BasicNameValuePair("return_date", return_date));

        if(can_take == true)params.add(new BasicNameValuePair("can_take", "1"));
        if(can_reserve == true)params.add(new BasicNameValuePair("can_reserve", "1"));
        if(must_return == true)params.add(new BasicNameValuePair("must_return", "1"));
        if(hold_stock == true)params.add(new BasicNameValuePair("hold_stock", "1"));
        if(need_scan == true)params.add(new BasicNameValuePair("need_scan", "1"));
        if(is_locked == true)params.add(new BasicNameValuePair("is_locked", "1"));
        if(!return_days.equals(""))params.add(new BasicNameValuePair("return_days", return_days));

        params.add(new BasicNameValuePair("company", company));

        JSONObject json;
        if(!image_type.equals("")){
            params.add(new BasicNameValuePair("image_type", image_type));
            params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
            //json = jsonParser.makeHttpRequest(webservice_url + "/" + url_update_item, "POST", params);
            JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, true); // POST
        }else {
            //json = jsonParser.makeHttpRequest(webservice_url + "/" + url_update_item, "GET", params);
            JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false); // GET
        }


        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }
    // RESOURCE DIGI FILE FINDER
    public int editResourceDetail(String resourceId, String name, String group_id, String location,
                                      String description, String keywords, String owner, String image_id, String file_id, String resource_type, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource"));
        params.add(new BasicNameValuePair("company", company));

        params.add(new BasicNameValuePair("id", resourceId));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("group_id", group_id));
        params.add(new BasicNameValuePair("location", location));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("search", keywords));
        if(Integer.valueOf(image_id) > 0) params.add(new BasicNameValuePair("image_id", image_id));
        if(Integer.valueOf(file_id) > 0) params.add(new BasicNameValuePair("file_id", file_id));
        params.add(new BasicNameValuePair("owner", owner));
        params.add(new BasicNameValuePair("resource_type", resource_type));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false); // GET

        if (last_error.equals(""))
        {
            return 1;
            //return true;
        }else {
            return 0;
            //return false;
        }
    }

    // RESOURCE TAKE
    public boolean takeResource(String resourceId, String take, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource"));

        params.add(new BasicNameValuePair("id", resourceId));
        params.add(new BasicNameValuePair("take", take));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }

    // RESOURCE RETURN
    public boolean returnResource(String resourceId, String return_date, String take, String company) {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource"));

        params.add(new BasicNameValuePair("id", resourceId));
        params.add(new BasicNameValuePair("return_date", return_date));
        params.add(new BasicNameValuePair("take", take));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals("")) {
            return true;

        } else {

            return false;
        }
    }


    // LOCATION
    public boolean editLocation(String locationId, String barcode, String name, String code, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "location"));

        params.add(new BasicNameValuePair("id", locationId));
        params.add(new BasicNameValuePair("barcode", barcode));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }

    /**
     * DELETE
     */

    // RESOURCE
    public boolean deleteResource(String resourceId, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "resource"));

        params.add(new BasicNameValuePair("id", resourceId));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }

    // LOCATION
    public boolean deleteLocation(String locationId, String company)
    {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("object", "location"));

        params.add(new BasicNameValuePair("id", locationId));
        params.add(new BasicNameValuePair("company", company));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

    }


}
