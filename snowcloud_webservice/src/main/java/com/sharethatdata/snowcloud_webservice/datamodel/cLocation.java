package com.sharethatdata.snowcloud_webservice.datamodel;

/**
 * Created by gotic_000 on 3/13/2018.
 */

public class cLocation {

    public String id;
    public String name;
    public String code;
    public String barcode;
    public String resource;
    public String stock;

    public cLocation(){
        id = "";
        name = "";
        code = "";
        barcode = "";
        resource = "";
        stock = "";
    }
}
