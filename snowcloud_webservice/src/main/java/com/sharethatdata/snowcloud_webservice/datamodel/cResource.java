package com.sharethatdata.snowcloud_webservice.datamodel;

import java.util.ArrayList;
import java.util.List;

public class cResource {
	public String id;
    public String resourceid;
    public String resource;
    public String name;
    public String code;
    public String barcode;
    public String location;
    public String stock;
    public String status;
    public String description;
    public String search;
    public long amount;
    public boolean InUse;
    public String inuse;
    public String owner;
    public String userid;
    public String user;
    public String groupID;
    public String group_name;
    public String image_id;
    public String image_thumb;
    public String image_full;
    public String image_array_string;
    public String datetime;
    public String created_date;
    public String return_date;

    public String can_take;
    public String can_reserve;
    public String must_return;
    public String hold_stock;
    public String need_scan;
    public String is_locked;
    public String return_days;
    public String book_amount;

    public String file_type;
    public String filename;
    public String resource_type;
    public String file_id;

    public List<cResource> list;

    public cResource()
    {
    	id= "0";
        name = "";
        resourceid = "";
        resource = "";
        code = "";
        barcode = "";
        location = "";
        amount = 1;
        stock = "0";
        status = "";
        description = "";
        search = "";
        InUse = false;
        inuse = "";
        owner = "";
        userid = "0";
        user = "0";
        groupID = "0";
        group_name = "";
        image_id = "0";
        image_thumb = "0";
        image_full = "0";
        datetime = "";
        created_date = "";
        return_date = "";

        can_take = "";
        can_reserve = "";
        must_return = "";
        hold_stock = "";
        need_scan = "";
        is_locked = "";
        return_days = "";
        book_amount = "";

        file_type = "";
        filename = "";
        resource_type = "";
        file_id = "";

        list = new ArrayList<>();
    }

    public cResource(String id, String name){
        this.image_id = id;
        this.image_array_string = name;
    }
}
