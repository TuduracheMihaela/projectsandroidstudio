package com.sharethatdata.digiorderwebview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by tudur on 10-Dec-20.
 */

public class TestActivity extends AppCompatActivity {

    private WebView webView = null;

    ImageView imageView;


    // photo
    final Activity activity = this;
    public Uri imageUri;
    private static final int FILECHOOSER_RESULTCODE   = 2888;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;

    private static final int MY_CAMERA_REQUEST_CODE = 100;

    //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);

        loadWebPage();

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //loadWebPage();
                webView.reload();
                pullToRefresh.setRefreshing(false);
            }
        });



    } // onCreate();

    public void loadWebPage(){
        webView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);// Add this
        webSettings.setDatabaseEnabled(true);// Add this

        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setDefaultTextEncodingName("utf-8");


        MyWebViewClient webViewClient = new MyWebViewClient(this);
        webView.setWebViewClient(webViewClient);
        webView.setWebChromeClient(new WebChromeClient());

        if (18 < Build.VERSION.SDK_INT ){
            //18 = JellyBean MR2, KITKAT=19
            this.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }

        if (Build.VERSION.SDK_INT >= 19) {
            //this.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        webView.loadUrl("https://testrobopharma.dreamhosters.com/");

        // Add the interface to record javascript events
        webView.addJavascriptInterface(new MyJavaScriptInterface(this), "ButtonCheck");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                loadMyEvent(clickListener());
            }

            private void loadMyEvent(String javascript){
                webView.loadUrl("javascript:"+javascript);
            }

            private String clickListener(){
                String button1 = "for(var i = 0; i < button1.length; i++){ \n " +
                        "\t button1[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethod1('button clicked'); }; \n " +
                        "}";
                String ok = "for(var i = 0; i < buttonok.length; i++){ \n " +
                        "\t buttonok[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethod('button clicked'); }; \n " +
                        "}";
                String no = "for(var i = 0; i < buttonno.length; i++){ \n " +
                        "\t buttonno[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethodd('button clicked'); }; \n " +
                        "}";
                String hello = "for(var i = 0; i < buttonhello.length; i++){ \n " +
                        "\t buttonhello[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethoddd('button clicked'); }; \n " +
                        "}";
                String goodbye = "for(var i = 0; i < buttongoodbye.length; i++){ \n " +
                        "\t buttongoodbye[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethodddd('button clicked'); }; \n " +
                        "}";

                return getMyButtons()+ button1 + ok + no + hello + goodbye;

                //return getMyButtons()+ "for(var i = 0; i < buttons.length; i++){ \n " +
                //        "\t buttons[i].onclick = function(){ console.log('click worked.'); ButtonCheck.boundMethod('button clicked'); }; \n " +
                //        "}";
            }

            private String getMyButtons(){
                String button1 = "var button1 = document.getElementsByClassName('button1'); console.log(button1.length + ' button1'); \n ";
                String ok = "var buttonok = document.getElementsByClassName('okbutton'); console.log(buttonok.length + ' buttonok'); \n ";
                String no = "var buttonno = document.getElementsByClassName('nobutton'); console.log(buttonno.length + ' buttonno'); \n ";
                String hello = "var buttonhello = document.getElementsByClassName('hellobutton'); console.log(buttonhello.length + ' buttonhello'); \n ";
                String goodbye = "var buttongoodbye = document.getElementsByClassName('goodbyebutton'); console.log(buttongoodbye.length + ' buttongoodbye'); \n ";

                return button1 + ok + no + hello + goodbye;
                //return "var buttons = document.getElementsByClassName('okbutton'); console.log(buttons.length + ' buttons'); \n ";
            }
        });

       //
        //takePhoto();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("NewApi")
    private void takePhoto(){
        //Other webview settings
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setSupportZoom(true);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }

        // take image
        webView.setWebChromeClient(new WebChromeClient(){
            // Need to accept permissions to use the camera
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d("onPermissionRequest", "onPermissionRequest");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.getResources());
                }
            }
        });

        // Define Webview manage classes
        //startWebView();

    }

    class MyJavaScriptInterface {

        private Context context;

        MyJavaScriptInterface(Context ctx) {
            context = ctx;
        }

        @JavascriptInterface
        public void boundMethod1(String html) {
            //Check internet connection
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            //startActivity(intent);
            //startActivityForResult(intent, TAKE_PHOTO_REQ);
            selectImage(TestActivity.this);
        }
        @JavascriptInterface
        public void boundMethod(String html) {
            //Check internet connection
            Toast.makeText(TestActivity.this, "TEST", Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void boundMethodd(String html) {
            //Check internet connection
            Toast.makeText(TestActivity.this, "TEST 1", Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void boundMethoddd(String html) {
            //Check internet connection
            Toast.makeText(TestActivity.this, "TEST 2", Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void boundMethodddd(String html) {
            //Check internet connection
            Toast.makeText(TestActivity.this, "TEST 3", Toast.LENGTH_SHORT).show();
        }
    }

    //////////
    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        imageView.setImageBitmap(selectedImage);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

                                //
                                final String imagePath = "file://"+ picturePath;
                                webView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //webView.evaluateJavascript("postImage('" + imagePath + "')", null);

                                        //String barcode_text = "document.getElementById(\"image\").value = " + bc +";";
                                        //webView.loadUrl("javascript: {" + barcode_text + "};");

                                        String action = "javascript:aa('" + imagePath + "')";
                                        webView.loadUrl(action);

                                        //String html = "<img src=\""+ imagePath + "\">";
                                        //webView.loadDataWithBaseURL("", html, "text/html","utf-8", "");
                                    }
                                });

                                if (data == null) {
                                    //Display an error
                                    Log.d("performClick", "Error: data == null");
                                    return;
                                }
                                //

                                cursor.close();
                            }
                        }

                    }
                    break;
            }
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////

    private void startWebView() {



        // Create new webview Client to show progress dialog
        // Called When opening a url or click on link
        // You can create external class extends with WebViewClient
        // Taking WebViewClient as inner class

        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are open in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                // Check if Url contains ExternalLinks string in url
                // then open url in new browser
                // else all webview links will open in webview browser
                if(url.contains("https://testrobopharma.dreamhosters.com/")){

                    // Could be cleverer and use a regex
                    //Open links in new browser
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                    // Here we can open new activity

                    return true;

                } else {

                    // Stay within this webview and load url
                    view.loadUrl(url);
                    return true;
                }

            }



            //Show loader on url load
            public void onLoadResource (WebView view, String url) {

                // if url contains string androidexample
                // Then show progress  Dialog
                if (progressDialog == null && url.contains("https://testrobopharma.dreamhosters.com/")) {

                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(TestActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            // Called when all page resources loaded
            public void onPageFinished(WebView view, String url) {

                try{
                    // Close progressDialog
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });


        // You can create external class extends with WebChromeClient
        // Taking WebViewClient as inner class
        // we will define openFileChooser for select file from camera or sdcard

        webView.setWebChromeClient(new WebChromeClient() {

            // openFileChooser for Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType){

                // Update message
                mUploadMessage = uploadMsg;

                try{

                    // Create AndroidExampleFolder at sdcard

                    File imageStorageDir = new File(
                            Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES)
                            , "AndroidExampleFolder");

                    if (!imageStorageDir.exists()) {
                        // Create AndroidExampleFolder at sdcard
                        imageStorageDir.mkdirs();
                    }

                    // Create camera captured image file path and name
                    File file = new File(
                            imageStorageDir + File.separator + "IMG_"
                                    + String.valueOf(System.currentTimeMillis())
                                    + ".jpg");

                    mCapturedImageURI = Uri.fromFile(file);

                    // Camera capture image intent
                    final Intent captureIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("image/*");

                    // Create file chooser intent
                    Intent chooserIntent = Intent.createChooser(i, "Image Chooser");

                    // Set camera intent to file chooser
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                            , new Parcelable[] { captureIntent });

                    // On select image call onActivityResult method of activity
                    startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

                }
                catch(Exception e){
                    Toast.makeText(getBaseContext(), "Exception:"+e,
                            Toast.LENGTH_LONG).show();
                }

            }

            // openFileChooser for Android < 3.0
            public void openFileChooser(ValueCallback<Uri> uploadMsg){
                openFileChooser(uploadMsg, "");
            }

            //openFileChooser for other Android versions
            public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                        String acceptType,
                                        String capture) {

                openFileChooser(uploadMsg, acceptType);
            }



            // The webPage has 2 filechoosers and will send a
            // console message informing what action to perform,
            // taking a photo or updating the file

            public boolean onConsoleMessage(ConsoleMessage cm) {

                onConsoleMessage(cm.message(), cm.lineNumber(), cm.sourceId());
                return true;
            }

            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                //Log.d("androidruntime", "Show console messages, Used for debugging: " + message);

            }
        });   // End setWebChromeClient

    }



// Return here when file selected from camera or from SDcard
/*    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {

        if(requestCode==FILECHOOSER_RESULTCODE)
        {

            if (null == this.mUploadMessage) {
                return;

            }

            Uri result=null;

            try{
                if (resultCode != RESULT_OK) {

                    result = null;

                } else {

                    // retrieve from the private variable if the intent is null
                    result = intent == null ? mCapturedImageURI : intent.getData();
                }
            }
            catch(Exception e)
            {
                Toast.makeText(getApplicationContext(), "activity :"+e,
                        Toast.LENGTH_SHORT).show();
            }

            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;

        }

    }*/

// Open previous opened link from history on webview when back button pressed

    @Override
// Detect when the back button is pressed
    public void onBackPressed() {

        if(webView.canGoBack()) {

            webView.goBack();

        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }

}
