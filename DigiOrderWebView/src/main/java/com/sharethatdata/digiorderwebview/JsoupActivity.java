package com.sharethatdata.digiorderwebview;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by tudur on 10-Dec-20.
 */

public class JsoupActivity extends AppCompatActivity {

    private WebView webView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadWebPage();

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //loadWebPage();
                webView.reload();
                pullToRefresh.setRefreshing(false);
            }
        });


    } // onCreate();

    public void loadWebPage() {
        webView = (WebView) findViewById(R.id.webview);

        MyWebViewClient webViewClient = new MyWebViewClient(this);
        webView.setWebViewClient(webViewClient);
        webView.setWebChromeClient(new WebChromeClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);// Add this
        webSettings.setDatabaseEnabled(true);// Add this

        if (18 < Build.VERSION.SDK_INT ){
            //18 = JellyBean MR2, KITKAT=19
            this.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }

        if (Build.VERSION.SDK_INT >= 19) {
            //this.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        webView.loadUrl("https://testrobopharma.dreamhosters.com/");

        new MyTask().execute();

    }


    private class MyTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String title ="";
            Document doc;
            try {
                doc = Jsoup.connect("https://testrobopharma.dreamhosters.com/").get();
                title = doc.toString();
                System.out.print(title);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return title;
        }


        @Override
        protected void onPostExecute(final String result) {
            //if you had a ui element, you could display the title
            webView.addJavascriptInterface(new Object() {
                @JavascriptInterface
                public void validateAndSubmit() throws Exception {

                    Toast.makeText(JsoupActivity.this, "Login clicked", Toast.LENGTH_SHORT).show();
                }
            }, "FC");
        }
    }


}
