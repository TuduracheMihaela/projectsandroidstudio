package com.sharethatdata.digiorderwebview;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.sharethatdata.digiorderwebview.BarcodeReader.Barcode.BarcodeCaptureActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MainActivity extends AppCompatActivity {

    private static final int BARCODE_READER_REQUEST_CODE = 9001;

    private WebView webView = null;
    public static String barcode = null;

    Button valid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadWebPage();

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //loadWebPage();
                webView.reload();
                pullToRefresh.setRefreshing(false);
            }
        });


    } // onCreate();

    public void loadWebPage(){
        webView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);// Add this
        webSettings.setDatabaseEnabled(true);// Add this

        webView.addJavascriptInterface(new MyJavaScriptInterface(this), "ButtonRecognizer");
        //webView.addJavascriptInterface(new MyJavaScriptInterface(this), "InputRecognizer");

        //webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());

        MyWebViewClient webViewClient = new MyWebViewClient(this);
        webView.setWebViewClient(webViewClient);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                loadEvent(clickListener());
            }

            private void loadEvent(String javascript){
                webView.loadUrl("javascript:"+javascript);
            }

            private String clickListener(){
                //String buttons = "for(var i = 0; i < buttons.length; i++){\n" +
                //        "\t buttons[i].onclick = function(){ console.log('click worked.'); ButtonRecognizer.boundMethodButton('button clicked'); };\n" +
                //        "}\n";
                //String button_img = "for(var i = 0; i < button_image.length; i++){ \n " +
                //        " \t button_image[i].onclick = function(){ console.log('click worked.'); ButtonRecognizer.boundMethodd('button clicked'); }; \n " +
                //        "}";

                return getButtons()+ "for(var i = 0; i < buttons.length; i++){\n" +
                        "\t buttons[i].onclick = function(){ console.log('click worked.'); ButtonRecognizer.boundMethodButton('button clicked'); };\n" +
                        "}";
                //return getButtons()+button_img;
            }

            private String getButtons(){
                //String buttons = "var buttons = document.getElementsByClassName('openScannerHome'); console.log(buttons.length + ' buttons'); ";
                //String button_image = "var button_image = document.getElementByClassName('openScannerHome'); console.log(button_image.length + ' button_image'); \n ";
                return "var buttons = document.getElementsByClassName('openScannerHome'); console.log(buttons.length + ' buttons');\n";
                //return button_image;
                // openScannerAttach
                // openScannerHomeInput
                // openScannerAttachInput
            }
        });

        webView.loadUrl("https://www.technicomponents.com/backoffice/app/");

        if (18 < Build.VERSION.SDK_INT ){
            //18 = JellyBean MR2, KITKAT=19
            this.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }

        if (Build.VERSION.SDK_INT >= 19) {
            //this.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }


    }



    class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void boundMethodButton(String html) {
            //new AlertDialog.Builder(ctx).setTitle("HTML").setMessage("It worked")
             //       .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();

            Log.d("AAAAAAAAAAAAA", "SCAAAAAAAN");
            //Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            //intent.setPackage("com.google.zxing.client.android");
            //startActivityForResult(intent, 0);

            Intent intent = new Intent(MainActivity.this, BarcodeCaptureActivity.class);
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        }

       /* @JavascriptInterface
        public void boundMethodInput(String html) {
            //new AlertDialog.Builder(ctx).setTitle("HTML").setMessage("It worked")
            //       .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();

            Log.d("BBBBBBBB", "SCAAAAAAAN");
        }*/

        @JavascriptInterface
        public void boundMethodd(String html) {
            //Check internet connection
            Toast.makeText(MainActivity.this, "TEST 1", Toast.LENGTH_SHORT).show();
        }

    } //MyJavaScriptInterface

    /*public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                //here is where you get your result
                barcode = intent.getStringExtra("SCAN_RESULT");
            }
        }
    }*/

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String UPCScanned = barcode.displayValue.toString();
                    Log.d("SCAAAAAAAAAAAN", UPCScanned);

                    String bc = UPCScanned;
                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    Log.d("AAAAAAAAAAAAA", bc);

                    String barcode_text = "document.getElementById(\"openScanner\").value = " + bc +";";
                    webView.loadUrl("javascript: {" + barcode_text + "};");

                } else
                    Toast.makeText(MainActivity.this, getString(R.string.toast_text_no_barcode_captured), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MainActivity.this, getString(R.string.textView_text_error) + ":" + " " + CommonStatusCodes.getStatusCodeString(resultCode), Toast.LENGTH_SHORT).show();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
