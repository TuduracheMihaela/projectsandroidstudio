package com.sharethatdata.jsonlibrary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.KeyStore;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
//import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;


import android.util.Log;

public class JSONParser {
 
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    static ApiCrypter myCrypter = null;
    static boolean ssl = false;
    
    // constructor
    public JSONParser() {
    	myCrypter = new ApiCrypter();
    }
 
    // function get json from url
    // by making HTTP POST or GET mehtod
    public JSONObject makeHttpRequest(String url, String method, List<NameValuePair> params) {
 
    	boolean success = false;
    	jObj = null;
    	
        // Making HTTP request
        try {
        	
            Log.d("Calling webservice= ", url.toString());
 
            for (NameValuePair p : params) {
            	// Removed, Not enough memory for logging a full image as string.
            	Log.d("with param: ", p.getName() + "=" + p.getValue());
            }
            
            // check for request method
            if(method == "MULTIPOST"){
                // request method is POST MultiEntity

                MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
                multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                
                for (NameValuePair p : params) {
                	if (p.getName() == "image")
                	{
                		multipartEntity.addTextBody(p.getName(), p.getValue());
                	} else {
                		multipartEntity.addTextBody(p.getName(), p.getValue());
                	}
                }
                             
             //   DefaultHttpClient httpClient = new DefaultHttpClient();
                //DefaultHttpClient httpClient = getNewHttpClient(); -> is deprecated
                HttpClient httpClient = getNewHttpClient();

             //   httpClient.log.enableDebug(true);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(multipartEntity.build());

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                success = true;
            }
          
            
            if(method == "POST"){
            	HttpParams httpParameters = new BasicHttpParams();
            	int timeoutConnection = 7000;
            	HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            	// Set the default socket timeout (SO_TIMEOUT) 
            	// in milliseconds which is the timeout for waiting for data.
            	int timeoutSocket = 7000;
            	HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            	
                // request method is POST
                // defaultHttpClient
             //   DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

                //DefaultHttpClient httpClient = getNewHttpClient(); -> is deprecated
                HttpClient httpClient = getNewHttpClient();

                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));
                
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
 
                success = true; 
            }else if(method == "GET"){
            	
            	if (ssl)
            	{
            		HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

            		DefaultHttpClient client = new DefaultHttpClient();

            		SchemeRegistry registry = new SchemeRegistry();
            		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
            		socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
            		registry.register(new Scheme("https", socketFactory, 443));
            		SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
            		DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

            		// Set verifier     
            		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

            		 String paramString = URLEncodedUtils.format(params, "utf-8");
                     url += "?" + paramString;
                     HttpGet httpGet = new HttpGet(url);
      
                     HttpResponse httpResponse = httpClient.execute(httpGet);
                     HttpEntity httpEntity = httpResponse.getEntity();
                                  
                     is = httpEntity.getContent();
            		
            	} else {
            	
	            //	HttpParams httpParameters = new BasicHttpParams();
	            //	int timeoutConnection = 10000;
	            //	HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
	            	// Set the default socket timeout (SO_TIMEOUT) 
	            	// in milliseconds which is the timeout for waiting for data.
	            //	int timeoutSocket = 10000;
	            //	HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

                    //DefaultHttpClient httpClient = getNewHttpClient(); -> is deprecated
                    HttpClient httpClient = getNewHttpClient();

	            	// request method is GET
	              //  DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
	                String paramString = URLEncodedUtils.format(params, "utf-8");
	                url += "?" + paramString;
	                HttpGet httpGet = new HttpGet(url);
	 
	                HttpResponse httpResponse = httpClient.execute(httpGet);
	                HttpEntity httpEntity = httpResponse.getEntity();
	                             
	                is = httpEntity.getContent();

            	}
                success = true;
            }           
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        if (success)
        {
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "iso-8859-1"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	                System.out.println(line.toString());
	            }
	            is.close();
	            json = sb.toString();
	                    
	            String res = new String( myCrypter.decrypt( json.toString() ));
	            json = URLDecoder.decode(res,"UTF-8");
	
	        
	        } catch (Exception e) {
	       //     Log.e(Integer.toString(R.string.text1_error), Integer.toString(R.string.text2_error) + e.toString());
	            // "Buffer Error" // "Error converting result " 
	        }
	 
	        // try parse the string to a JSON object
	        try {
	            jObj = new JSONObject(json);
	        } catch (JSONException e) {
	            Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }
	        // return JSON String
	        return jObj;
        } else {
        	
        	return null;
        }
 

 
    }


    public JSONObject makeHttpRequest1(String url, String method, List<NameValuePair> params,
                                       FileBody fileBody, String path) {
        boolean success = false;
        jObj = null;
        // Making HTTP request
        try {
            Log.d("Calling webservice= ", url.toString());
            for (NameValuePair p : params) {
                // Removed, Not enough memory for logging a full image as string.
                Log.d("with param: ", p.getName() + "=" + p.getValue());
            }
            // check for request method
            if(method == "MULTIPOST"){
                // request method is POST MultiEntity

                MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
                multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

                String parameterName = "name";
                String contentType = "pdf";
                String fileName = null;

                if(path != null && !"".equals(path)) {
                    File file = new File(path);
                    if(!file.exists()) {
                        throw new FileNotFoundException("Could not find file at path: " + path);
                    } else {
                        //this.file = file;
                        if(parameterName != null && !"".equals(parameterName)) {
                            //this.paramName = parameterName;
                            if(contentType != null && !contentType.isEmpty()) {
                                //this.contentType = contentType;
                                Log.d("LOG_TAG", "Content Type set for " + file.getAbsolutePath() + " is: " + contentType);
                            } else {
                                //this.contentType = this.autoDetectMimeType();
                                Log.d("LOG_TAG", "Detected MIME type for " + file.getAbsolutePath() + " is: " + contentType);
                            }

                            if(fileName != null && !"".equals(fileName)) {
                                //this.fileName = fileName;
                                Log.d("LOG_TAG", "Using custom file name: " + fileName);
                            } else {
                                //this.fileName = this.file.getName();
                                Log.d("LOG_TAG", "Using original file name: " + fileName);
                            }

                        } else {
                            throw new IllegalArgumentException("Please specify parameterName value for file: " + path);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Please specify a file path! Passed path value is: " + path);
                }

                //multipartEntity.addPart("file", fileBody);
                //fileBody = null;

                FileBody fileBody1 = new FileBody(new File(path));
                multipartEntity.addPart("file", fileBody1);

                for (NameValuePair p : params) {
                    if (fileBody != null)
                    {
                        multipartEntity.addTextBody(p.getName(), p.getValue());
                    } else {
                        //multipartEntity.addTextBody(p.getName(), p.getValue());
                    }
                }

                //   DefaultHttpClient httpClient = new DefaultHttpClient();
                //DefaultHttpClient httpClient = getNewHttpClient(); -> is deprecated
                HttpClient httpClient = getNewHttpClient();

                //   httpClient.log.enableDebug(true);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(multipartEntity.build());

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                success = true;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (success)
        {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                    System.out.println(line.toString());
                }
                is.close();
                json = sb.toString();

                String res = new String( myCrypter.decrypt( json.toString() ));
                json = URLDecoder.decode(res,"UTF-8");

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                jObj = new JSONObject(json); // try parse the string to a JSON object
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return jObj; // return JSON String
        } else {
            return null;
        }
    }


    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);


            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            e.printStackTrace();
            return new DefaultHttpClient();
        }
    }

}


