package com.sharethatdata.jsonlibrary;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

 
public class ApiCrypter {
 
    private String iv              = "fdsfds85435nfdfs";
    private String secretkey       = "89432hjfsd89178729wjd97391727271";
    private IvParameterSpec ivspec;

    private SecretKeySpec keyspec;
    private Cipher cipher;
    
    private static final String SecretKey = "9081726354fabced";
    private static final String Salt = "03xy9z52twq8r4s1uv67";
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String KEY_ALGORITHM = "AES";
    private static final String SECRET_KEY_ALGORITHM = "PBEWithSHA256And256BitAES-CBC-BC";
    private static final String RANDOM_ALGORITHM = "SHA-512";
    private static final int PBE_ITERATION_COUNT = 100;
    private static final int PBE_KEY_LENGTH = 256;
    private static final int IV_LENGTH = 16;
    
    public ApiCrypter()
    {
        ivspec = new IvParameterSpec(iv.getBytes());
        keyspec = new SecretKeySpec(secretkey.getBytes(), "AES");
 
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }
 
    public byte[] encrypt(String text) throws Exception
    {
        if(text == null || text.length() == 0) {
         //   throw new Exception(Integer.toString(R.string.text_exception));
            // "Empty string"
        }
        byte[] encrypted = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            encrypted = cipher.doFinal(text.getBytes("UTF-8"));
        }
        catch (Exception e) {
            throw new Exception("[encrypt] " + e.getMessage());
        }

        return encrypted;
    }


    public byte[] decrypt(String code) throws Exception
    {
        if(code == null || code.length() == 0) {
         //   throw new Exception(Integer.toString(R.string.text_exception));
            // "Empty string"
        }
        
        String iv64 = code.substring(0, IV_LENGTH*2);
        String encrypted64 = code.substring(IV_LENGTH*2);
        byte[] iv = hexToBytes(iv64);
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        byte[] decrypted = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
            decrypted = cipher.doFinal(hexToBytes(encrypted64));
        }
        catch (Exception e) {
            throw new Exception("[decrypt] " + e.getMessage());
        }
        return decrypted;
    }

    private byte[] generateIV() {
        try {
            SecureRandom random = SecureRandom.getInstance(RANDOM_ALGORITHM);
            byte[] iv = new byte[IV_LENGTH];
            random.nextBytes(iv);
            return iv;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String bytesToHex(byte[] data)

    {
        if (data==null) {
            return null;
        }

        int len = data.length;
        String str = "";

        for (int i=0; i<len; i++) {
            if ((data[i]&0xFF)<16) {
                str = str + "0" + Integer.toHexString(data[i]&0xFF);
            }
            else {
                str = str + Integer.toHexString(data[i]&0xFF);
            }
        }

        return str;

    }

 

    public static byte[] hexToBytes(String str) {

        if (str==null) {
            return null;
        }

        else if (str.length() < 2) {
            return null;
        }

        else {

            int len = str.length() / 2;
            byte[] buffer = new byte[len];

            for (int i=0; i<len; i++) {
                buffer[i] = (byte) Integer.parseInt(str.substring(i*2,i*2+2),16);
            }

            return buffer;

        }

    }

}
