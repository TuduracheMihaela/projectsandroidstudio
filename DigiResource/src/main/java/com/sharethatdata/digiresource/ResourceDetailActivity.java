package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiresource.MyResources.ResourcesReserved;
import com.sharethatdata.digiresource.Scanner.IntentIntegrator;
import com.sharethatdata.digiresource.Scanner.IntentResult;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gotic_000 on 1/16/2018.
 */

public class ResourceDetailActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ArrayList<HashMap<String, String>> resourceList;
    ArrayList<HashMap<String, String>> resourceGroupList;
    ArrayList<HashMap<String, String>> resourcebyBarcodeList;

    List<String> resourcekeyArray =  new ArrayList<String>();
    List<String> resourcegroupkeyArray =  new ArrayList<String>();

    List<String> resourcenameArray =  new ArrayList<String>();
    List<String> resourcegroupnameArray =  new ArrayList<String>();

    ProgressDialog progressDialog;
    // DETAILS
    TextView textViewName, textViewDescription, textViewBarcode, textViewCode, textViewLocation, textViewStatus;
    Button buttonTake, buttonReserve, buttonReturn;

    private int mEndYear,mEndMonth,mEndDay;
    private int mEndHour,mEndMinute;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    String resourceID;
    String resourceName = "";

    String action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_detail);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ResourceDetailActivity.this, ResourceDetailActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            resourceID = extras.getString("idResource");
            action = extras.getString("action");
        }

        //DETAILS
        textViewName = (TextView) findViewById(R.id.resourceName);
        textViewDescription = (TextView) findViewById(R.id.resourceDescription);
        textViewBarcode = (TextView) findViewById(R.id.resourceBarcode);
        textViewCode = (TextView) findViewById(R.id.resourceCode);
        textViewLocation = (TextView) findViewById(R.id.resourceLocation);
        textViewStatus = (TextView) findViewById(R.id.resourceStatus);
        buttonTake = (Button) findViewById(R.id.buttonTake);
        buttonReserve = (Button) findViewById(R.id.buttonReserve);
        buttonReturn = (Button) findViewById(R.id.buttonReturn);

        buttonTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SelectResourceActivity.this, "In construction!", Toast.LENGTH_SHORT).show();

                String value_reserved = Globals.getValue("reserved");
                if(value_reserved.equals("reserved")){
                    Globals.setValue("reserved","reserved");
                }

                Intent intent = new Intent(getApplicationContext(), ReserveOrTakeActivity.class);
                intent.putExtra("resourceID", resourceID);
                intent.putExtra("action", "take");
                startActivity(intent);
            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SelectResourceActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), ReserveOrTakeActivity.class);
                intent.putExtra("resourceID", resourceID);
                intent.putExtra("action", "reserve");
                startActivity(intent);
            }
        });

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SelectResourceActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
                new ReturnReservationResource(resourceID, companyId).execute();
            }
        });

        new LoadResource(resourceID, companyId).execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d("TAG", "onResume");
        String refresh = Globals.getValue("refresh");
        if(refresh.equals("refresh")){
            finish();
        }
    }

    /**
     * Background Async Task to Load Project Item by making HTTP Request
     * */
    class LoadResource extends AsyncTask<String, String, String> {

        String resource_id;
        String company_id;
        String image_string;

        public LoadResource(String resource_id, String company_id){
            this.resource_id = resource_id;
            this.company_id = company_id;
            image_string = "";
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cResource resource = MyProvider.getResource(resource_id, company_id);

            if (resource != null)
            {
                resourceID = resource_id;

                /*if (resource.image_id != "0")
                {
                    // myImageData = MyRoboProvider.getImage(project.image_id);
                    String myImageData = MyProvider.getImage(resource.image_id);
                    image_string = myImageData;
                }*/

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewName.setText(Html.fromHtml(resource.name));
                        textViewDescription.setText(Html.fromHtml(resource.description));
                        textViewBarcode.setText(Html.fromHtml(resource.barcode));
                        textViewCode.setText(Html.fromHtml(resource.code));
                        textViewLocation.setText(Html.fromHtml(resource.location));
                        textViewStatus.setText(Html.fromHtml(resource.status));

                    /*if (project.full_image != "")
                    {
                        myImageData = project.full_image;
                        image_string = myImageData;
                    }*/

                        if(!resource.image_id.equals("0")){
                            image_string = resource.image_full;
                            Log.d("image",image_string);
                            ImageView imageView1 = (ImageView) findViewById(R.id.imageViewResource);
                            Bitmap d = ConvertByteArrayToBitmap(image_string);
                            if (d != null)
                            {
                                int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                                Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                                imageView1.setVisibility(View.VISIBLE);
                                imageView1.setImageBitmap(scaled);
                            }
                        }else{
                            ImageView imageView1 = (ImageView) findViewById(R.id.imageViewResource);
                            imageView1.setVisibility(View.GONE);
                        }

                        if(action.equals("reserved")){
                            buttonReserve.setVisibility(View.GONE);
                            buttonTake.setVisibility(View.VISIBLE);
                        }else if(action.equals("taken")){
                            buttonReserve.setVisibility(View.GONE);
                            buttonTake.setVisibility(View.GONE);
                            buttonReturn.setVisibility(View.VISIBLE);

                            Calendar c2 = Calendar.getInstance();
                            mEndYear = c2.get(Calendar.YEAR);
                            mEndMonth = c2.get(Calendar.MONTH);
                            mEndDay = c2.get(Calendar.DAY_OF_MONTH);
                            mEndHour = c2.get(Calendar.HOUR_OF_DAY);
                            mEndMinute = c2.get(Calendar.MINUTE);
                        }else{
                            buttonTake.setVisibility(View.VISIBLE);
                            buttonReserve.setVisibility(View.VISIBLE);
                        }

                    }
                });
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.VISIBLE);
                    }
                });

            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // dismiss the dialog after getting all products
            if (ResourceDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();
        }
    }

        private class ReturnReservationResource extends AsyncTask<String, String, Boolean>{

            String resource_id;
            String company_id;

            public ReturnReservationResource(String resource_id, String company_id){
                this.resource_id = resource_id;
                this.company_id = company_id;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog(getString(R.string.loading_alert_dialog));
            }

            @Override
            protected Boolean doInBackground(String... args) {

                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                String myDateEndTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
                myDateEndTime = myDateEndTime.concat((new StringBuilder().append(" ").append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString());

                // call web method
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                boolean suc = MyProvider.returnResource(resource_id, myDateEndTime, "/'0'/", company_id);

                return suc;
            }

            /**
             * After completing background task Dismiss the progress dialog
             * **/
            protected void onPostExecute(final Boolean success) {

                progressDialog.dismiss();

                // updating UI from Background Thread
                runOnUiThread(new Runnable() {

                    public void run() {

                        if (success == false)
                        {
                            Toast.makeText(ResourceDetailActivity.this, "Not returned", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ResourceDetailActivity.this, "Returned!", Toast.LENGTH_SHORT).show();
                            Globals.setValue("refresh","refresh");
                            finish();
                        }

                    }
                });
            }
        }


        private Object pad(int mMinute2) {
            if (mMinute2 >= 10)
                return String.valueOf(mMinute2);
            else
                return "0" + String.valueOf(mMinute2);
        }

        // when get and show the image
        private Bitmap ConvertByteArrayToBitmap(String b)
        {
            Bitmap decodedByte = null;
            if(b.isEmpty() || b.contentEquals(""))
            {
                decodedByte = null;
            }else{
                //byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
                byte[] decodedString = Base64.decode(b, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            return decodedByte;
        }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ResourceDetailActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}

