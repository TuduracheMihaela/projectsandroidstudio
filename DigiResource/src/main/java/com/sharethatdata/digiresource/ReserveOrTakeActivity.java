package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cReservations;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by gotic_000 on 2/2/2018.
 */

public class ReserveOrTakeActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> resourceReservationList;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    public static final String inputFormat = "HH:mm";
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);

    String idResource;
    //, nameResource;
    private int mStartYear,mStartMonth,mStartDay;
    private int mEndYear,mEndMonth,mEndDay;
    private int mStartHour,mStartMinute, mEndHour,mEndMinute;
    String type_action;

    TextView textViewTitle;
    TextView textViewId;
    TextView startDateView;
    TextView endDateView;
    TextView endTimeView;
    TextView startTimeView;
    //TextView spaceLineText;
    TextView startDateTextLabel;
    TextView endDateTextLabel;
    TextView startTimeTextLabel;
    TextView endTimeTextLabel;
    //, textViewName, textViewDescription, textViewBarcode, textViewLocation, textViewStatus;
    //Button buttonTake, buttonReserve;
    Button buttonAction;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_or_take);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ReserveOrTakeActivity.this, ReserveOrTakeActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String valueID = extras.getString("resourceID");
            String valueAction = extras.getString("action");
            idResource = valueID;
            type_action = valueAction;
        }

        textViewTitle = (TextView) findViewById(R.id.title);
        textViewId = (TextView) findViewById(R.id.resourceId);
        //textViewName = (TextView) findViewById(R.id.resourceName);
        //textViewDescription = (TextView) findViewById(R.id.resourceDescription);
        //textViewBarcode = (TextView) findViewById(R.id.resourceBarcode);
        //textViewLocation = (TextView) findViewById(R.id.resourceLocation);
        //textViewStatus = (TextView) findViewById(R.id.resourceStatus);
        startDateView = (TextView) findViewById(R.id.dateStartText);
        endDateView = (TextView) findViewById(R.id.dateEndText);
        startTimeView = (TextView) findViewById(R.id.timeStartTextView);
        endTimeView = (TextView) findViewById(R.id.timeEndTextView);
        //spaceLineText = (TextView) findViewById(R.id.SpaceLineText);
        startDateTextLabel = (TextView) findViewById(R.id.StartDateLabelText);
        endDateTextLabel = (TextView) findViewById(R.id.EndDateLabelText);
        startTimeTextLabel = (TextView) findViewById(R.id.timeStartLabelText);
        endTimeTextLabel = (TextView) findViewById(R.id.timeEndLabelText);
        //buttonTake = (Button) findViewById(R.id.buttonTake);
        //buttonReserve = (Button) findViewById(R.id.buttonReserve);
        buttonAction = (Button) findViewById(R.id.buttonAction);
        listView = (ListView) findViewById(R.id.listView);

        textViewId.setText(idResource);

        TextView numberOfDaysTextView = (TextView) findViewById(R.id.numberOfDaysText);
        numberOfDaysTextView.setText("0");

        if(type_action.equals("take")){
            setTitle("Take resource");
            textViewTitle.setText("Please select the return date and time");

            buttonAction.setText("Take");
            startDateTextLabel.setVisibility(View.GONE);
            startDateView.setVisibility(View.GONE);
            startTimeTextLabel.setVisibility(View.GONE);
            startTimeView.setVisibility(View.GONE);
            //spaceLineText.setVisibility(View.GONE);
            endDateView.setVisibility(View.VISIBLE);
            endDateTextLabel.setVisibility(View.VISIBLE);

        }else if(type_action.equals("reserve")){
            setTitle("Reserve resource");
            textViewTitle.setText("Please select a start and end date/time");

            buttonAction.setText("Reserve");
            startDateTextLabel.setVisibility(View.VISIBLE);
            startDateView.setVisibility(View.VISIBLE);
            startTimeTextLabel.setVisibility(View.VISIBLE);
            startTimeView.setVisibility(View.VISIBLE);
            //spaceLineText.setVisibility(View.VISIBLE);
            endDateView.setVisibility(View.VISIBLE);
            endDateTextLabel.setVisibility(View.VISIBLE);
        }

        // set calendar
        Calendar c1 = Calendar.getInstance();
        mStartYear = c1.get(Calendar.YEAR);
        mStartMonth = c1.get(Calendar.MONTH);
        mStartDay = c1.get(Calendar.DAY_OF_MONTH);
        if(type_action.equals("take")){
            mStartHour = c1.get(Calendar.HOUR_OF_DAY);
            mStartMinute = c1.get(Calendar.MINUTE);
        }else if(type_action.equals("reserve")){
            mStartHour = Integer.parseInt("09");
            mStartMinute = Integer.parseInt("00");
        }

        Calendar c2 = Calendar.getInstance();
        mEndYear = c2.get(Calendar.YEAR);
        mEndMonth = c2.get(Calendar.MONTH);
        mEndDay = c2.get(Calendar.DAY_OF_MONTH);
        mEndHour = c2.get(Calendar.HOUR_OF_DAY);
        mEndMinute = c2.get(Calendar.MINUTE);

        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
        String weekdayname1 = weekdays[c1.get(Calendar.DAY_OF_WEEK)];
        String weekdayname2 = weekdays[c2.get(Calendar.DAY_OF_WEEK)];
        startDateView.setText(new StringBuilder().append(weekdayname1).append(" ").append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
        endDateView.setText(new StringBuilder().append(weekdayname2).append(" ").append(pad(mEndDay)).append("/").append(pad(mEndMonth + 1)).append("/").append(pad(mEndYear)));

        startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
        endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
        calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);


        /*buttonTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ResourceDetailActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ResourceDetailActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
            }
        });*/

        buttonAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ResourceDetailActivity.this, "In construction!", Toast.LENGTH_SHORT).show();

                new CreateReservationResource(companyId).execute();
            }
        });

        //new LoadResource(idResource).execute();
        new LoadReservations(idResource, companyId).execute();

    }

    private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    private void calculateTotalHours(int startHour, int startMinute, int endHour, int endMinute){
        Date dateCompareStart = parseDate(String.valueOf(startHour) + ":" + String.valueOf(startMinute));;
        Date dateCompareEnd = parseDate(String.valueOf(endHour) + ":" + String.valueOf(endMinute));

        //milliseconds
        long different = dateCompareEnd.getTime() - dateCompareStart.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        TextView numberOfHoursText = (TextView) findViewById(R.id.numberOfHoursText);
        numberOfHoursText.setText(" " + elapsedHours + " hours " + elapsedMinutes + " minutes " );
    }

    private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int day) {

            mStartYear = year;
            mStartMonth = month;
            mStartDay = day;

            String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();

            startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
                    .append(pad(mStartMonth + 1)).append("/")
                    .append(pad(mStartYear)));

            /* numberOfDaysText */
            final Calendar date1 = Calendar.getInstance();
            final Calendar date2 = Calendar.getInstance();

            date1.clear();
            date1.set(mStartYear, mStartMonth, mStartDay);
            date2.clear();
            date2.set(mEndYear, mEndMonth, mEndDay);

            int numberOfDays = 0;
            while (date1.before(date2)) {
                date1.add(Calendar.DATE,1);
                if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
                        &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
                    numberOfDays++;
                }else {
                    date1.add(Calendar.DATE,1);
                }
            }

            TextView numberOfDaysTextView = (TextView) findViewById(R.id.numberOfDaysText);
            numberOfDaysTextView.setText(String.valueOf(numberOfDays));
        }

    };

    private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month, int day) {

            mEndYear = year;
            mEndMonth = month;
            mEndDay = day;

            String dayOfWeekEnd = DateFormat.format("EE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();

            endDateView.setText(new StringBuilder().append(dayOfWeekEnd).append(" ").append(pad(mEndDay)).append("/")
                    .append(pad(mEndMonth + 1)).append("/")
                    .append(pad(mEndYear)));

            /* numberOfDaysText */
            final Calendar date1 = Calendar.getInstance();
            final Calendar date2 = Calendar.getInstance();

            date1.clear();
            date1.set(mStartYear, mStartMonth, mStartDay);
            date2.clear();
            date2.set(mEndYear, mEndMonth, mEndDay);

            int numberOfDays = 0;
            while (date1.before(date2)) {
                date1.add(Calendar.DATE,1);
                if ((Calendar.SATURDAY != date1.get(Calendar.DAY_OF_WEEK))
                        &&(Calendar.SUNDAY != date1.get(Calendar.DAY_OF_WEEK))) {
                    numberOfDays++;
                }else {
                    date1.add(Calendar.DATE,1);
                }
            }

            TextView numberOfDaysTextView = (TextView) findViewById(R.id.numberOfDaysText);
            numberOfDaysTextView.setText(String.valueOf(numberOfDays));
        }

    };

    private TimePickerDialog.OnTimeSetListener mStartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mStartHour = hourOfDay;
            mStartMinute = minute;

            startTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":")
                    .append(pad(mStartMinute)));

            Date dateCompareStart = parseDate(String.valueOf(mStartHour) + ":" + String.valueOf(mStartMinute));
            Date dateCompareEnd = parseDate(String.valueOf(mEndHour) + ":" + String.valueOf(mEndMinute));

            if(dateCompareEnd.before( dateCompareStart )){
                // start time have to be smaller than end time
                //TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
                //endTimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
                calculateTotalHours(mStartHour, mStartMinute, mStartHour, mStartMinute);
            }else{
                //TextView endTimeView = (TextView) findViewById(R.id.timeEndText);
                //String endTime = endTimeView.getText().toString();
                //String[] tokens = endTime.split(":");
                //mEndHour = Integer.valueOf(tokens[0]);
                //mEndMinute = Integer.valueOf(tokens[1]);
                calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
            }
        }
    };

    private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mEndHour = hourOfDay;
            mEndMinute = minute;

            endTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":")
                    .append(pad(mEndMinute)));

            Date dateCompareStart = parseDate(String.valueOf(mStartHour) + ":" + String.valueOf(mStartMinute));
            Date dateCompareEnd = parseDate(String.valueOf(mEndHour) + ":" + String.valueOf(mEndMinute));

            if(dateCompareEnd.before( dateCompareStart )){
                // start time have to be smaller than end time
                //TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
                //startTimeView.setText(new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute)));
                calculateTotalHours(mEndHour, mEndMinute, mEndHour, mEndMinute);
            }else{
                //TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
                //String startTime = startTimeView.getText().toString();
                //String[] tokens = startTime.split(":");
                //mStartHour = Integer.valueOf(tokens[0]);
                //mStartMinute = Integer.valueOf(tokens[1]);
                calculateTotalHours(mStartHour, mStartMinute, mEndHour, mEndMinute);
            }
        }
    };

    public void onStartDayStatsClick(View v)
    {
        DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dp1.setTitle(sdf.format(d));
        dp1.show();

    }

    public void onEndDayStatsClick(View v){
        DatePickerDialog dp2 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dp2.setTitle(sdf.format(d));
        dp2.show();
    }

    public void startTimeClick(View v)
    {
        /*TextView startTimeView = (TextView) findViewById(R.id.timeStartText);
        String startTime = startTimeView.getText().toString();
        String[] tokens = startTime.split(":");
        mStartHour = Integer.valueOf(tokens[0]);
        mStartMinute = Integer.valueOf(tokens[1]);*/

        TimePickerDialog tp1 = new TimePickerDialog(this, mStartTimeSetListener, mStartHour, mStartMinute, true);
        tp1.show();
    }

    public void endTimeClick(View v)
    {
        /*String endTime = endTimeView.getText().toString();
        String[] tokens = endTime.split(":");
        mEndHour = Integer.valueOf(tokens[0]);
        mEndMinute = Integer.valueOf(tokens[1]);*/

        TimePickerDialog tp2 = new TimePickerDialog(this, mEndTimeSetListener, mEndHour, mEndMinute, true);
        tp2.show();
    }

    /**
     * Background Async Task to Load Project Item by making HTTP Request
     * */
    /*class LoadResource extends AsyncTask<String, String, String> {

        String resource_id;
        String image_string;

        public LoadResource(String resource_id){
            this.resource_id = resource_id;
        }

        *//**
     * Before starting background thread Show Progress Dialog
     * *//*
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        *//**
     * getting items from url
     * *//*
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cResource resource = MyProvider.getResource(resource_id);

            if (resource != null)
            {
                if (resource.image_id != "0")
                {
                    // myImageData = MyRoboProvider.getImage(project.image_id);
                    String myImageData = MyProvider.getImage(resource.image_id);
                    image_string = myImageData;
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textViewName.setText(Html.fromHtml(resource.name));
                    textViewDescription.setText(Html.fromHtml(resource.description));
                    textViewBarcode.setText(Html.fromHtml(resource.barcode));
                    textViewLocation.setText(Html.fromHtml(resource.location));
                    textViewStatus.setText(Html.fromHtml(resource.status));

                    Log.d("Resource Detail Image", image_string);

                    *//*if (project.full_image != "")
                    {
                        myImageData = project.full_image;
                        image_string = myImageData;
                    }*//*

                    ImageView imageView1 = (ImageView) findViewById(R.id.imageViewResource);

                    Bitmap d = ConvertByteArrayToBitmap(image_string);
                    if (d != null)
                    {
                        int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                        Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                        imageView1.setImageBitmap(scaled);

                    }

                }
            });


            return "";
        }

        // when get and show the image
        private Bitmap ConvertByteArrayToBitmap(String b)
        {
            Bitmap decodedByte = null;
            if(b.isEmpty() || b.contentEquals(""))
            {
                decodedByte = null;
            }else{
                byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            return decodedByte;
        }

        *//**
     * After completing background task Dismiss the progress dialog
     * **//*
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // dismiss the dialog after getting all products
            if (ResourceDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            new LoadReservations(resource_id).execute();

        }
    }*/

    class LoadReservations extends AsyncTask<String, String, String> {

        public String resourceid;
        public String company_id;

        public LoadReservations(String resourceid, String company_id){
            this.resourceid = resourceid;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceReservationList = new ArrayList<HashMap<String, String>>();

            List<cReservations> myReservation = MyProvider.getReservations(resourceid, company_id);
            for(cReservations entry : myReservation)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("reservationid",  entry.id);
                map.put("description",  entry.description);
                map.put("startdate",  entry.start_date);
                map.put("enddate",  entry.end_date);
                map.put("user",  entry.user);

                // adding HashList to ArrayList
                resourceReservationList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (ReserveOrTakeActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(resourceReservationList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(ReserveOrTakeActivity.this, resourceReservationList,
                                R.layout.activity_reservations_items_listview,
                                new String[]{"reservationid", "startdate", "enddate", "description", "user"},
                                new int[]{R.id.reservationid, R.id.startdate, R.id.enddate, R.id.description, R.id.user});
                        listView.setAdapter(adapter);

                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.GONE);
                    }else{
                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }


                }
            });

            hideProgressDialog();
        }
    }

    private class CreateReservationResource extends AsyncTask<String, String, Boolean>{

        String company_id;

        public CreateReservationResource(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            //String myDateStartTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
            //String myDateEndTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();

            //String myStartTime = (new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString();
            //String myEndTime = (new StringBuilder().append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString();

            String myDateStartTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
            myDateStartTime = myDateStartTime.concat((new StringBuilder().append(" ").append(pad(mStartHour)).append(":").append(pad(mStartMinute))).toString());

            String myDateEndTime = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
            myDateEndTime = myDateEndTime.concat((new StringBuilder().append(" ").append(pad(mEndHour)).append(":").append(pad(mEndMinute))).toString());

            int ResourceID = 0;
            boolean suc = false;


            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            //ResourceID = MyProvider.createResourceReservation(myDateStartTime, myDateEndTime, myUserID);
            if(type_action.equals("take")){
                // take = update take with object "resource"
                suc = MyProvider.takeResource(idResource, "1", company_id);
            }else if(type_action.equals("reserve")){
                // reserve = create a reservation with object "reservation"
                ResourceID = MyProvider.createResourceReservation("1", idResource, "description",  myDateStartTime, myDateEndTime, company_id);
                if(ResourceID != 0){
                    suc =  true;
                }
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        String error_message = MyProvider.last_error;
                        //Toast.makeText(ReserveOrTakeActivity.this, "Was not created the reservation", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ReserveOrTakeActivity.this, error_message, Toast.LENGTH_SHORT).show();
                    }else{

                        String value_reserved = Globals.getValue("reserved");
                        if(value_reserved.equals("reserved")){
                            Globals.setValue("refresh","refresh");
                        }
                        Toast.makeText(ReserveOrTakeActivity.this, "The reservation was created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ReserveOrTakeActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}

