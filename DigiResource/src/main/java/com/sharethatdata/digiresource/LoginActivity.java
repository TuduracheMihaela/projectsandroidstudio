package com.sharethatdata.digiresource;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cContact;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;
    WSDataProvider MyProvider2 = null;

    TimeUtils MyTimeUtils = null;

    JSONArray order = null;

    ArrayList<String> roles = null;

    private String mEmail;
    private String mPassword;

    private String myUser = "";
    private String myPass = "";

    private String webservice_url = "";

    String companyId = "";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private UserRolesTask mRolesTask = null;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private View mLoginStatusView;
    private TextView mLoginStatusMessageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(LoginActivity.this, LoginActivity.this);

        // Set up the login form.
        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );

        String current_user = sharedPrefs.getString("prefUsername", "");
        String current_password = sharedPrefs.getString("prefUserPassword", "");
        boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        // get webservice url
        webservice_url = sharedPrefs.getString("prefWebServiceServer", "");

        myUser = current_user;
        myPass = current_password;

        boolean suc = InitApp();

        if (!suc) Toast.makeText(LoginActivity.this, "Something went wrong with initializing the app!", Toast.LENGTH_SHORT).show();

        // Set up the login form.
        mEmail = current_user;
        mEmailView = (EditText) findViewById(R.id.email);
        mEmailView.setText(mEmail);

        mPassword = current_password;
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setText(mPassword);

        mPasswordView
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int id,
                                                  KeyEvent keyEvent) {
                        if (id == R.id.login || id == EditorInfo.IME_NULL) {
                            attemptLogin();
                            return true;
                        }
                        return false;
                    }
                });

        mLoginFormView = findViewById(R.id.login_form);
        mLoginStatusView = findViewById(R.id.login_status);
        mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

        findViewById(R.id.sign_in_button).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isNetworkAvailable() == true){
                            attemptLogin();
                        }else {
                            Toast.makeText(getApplicationContext(), "No internet connnection available", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        if (blAutoLogin)
        {
            if(isNetworkAvailable() == true){
                attemptLogin();
            }else {
                Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
            }
        }



        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        String address = info.getMacAddress();

        System.out.println("Adresse MAC enabled: " + address);
    }

    private boolean InitApp()
    {
        Globals = ((MyGlobals)getApplication());

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", false);
        String startDay = sharedPrefs.getString("prefNotificationStartDay", "");
        String endDay = sharedPrefs.getString("prefNotificationEndDay", "");

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider2 = new WSDataProvider(myUser, myPass);

        if (webservice_url != "")
        {
            // store in globals
            Globals.setValue("ws_url", webservice_url);

            MyProvider.SetWebServiceUrl(webservice_url);
            MyProvider2.SetWebServiceUrl(webservice_url);
        }

        try
        {
            // read device values
            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            int ipAddress = wifiInf.getIpAddress();
            String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
            Globals.setValue("ip", ip);

            String android_id = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            Globals.setValue("device", android_id);

            String appname = (String) getApplicationInfo().loadLabel(getBaseContext().getPackageManager());
            Globals.setValue("module", appname);

            MyProvider.SetDeviceIdentification(ip, android_id, appname);
            MyProvider2.SetDeviceIdentification(ip, android_id, appname);

            // add here also mac address

            return  true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */


    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mEmailView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }
    }

    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE
                                    : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE
                                    : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl(webservice_url);
            boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "DigiResource", "");
            if (suc)
            {
                // store current user in globals
                MyGlobals Globals = ((MyGlobals)getApplication());
                Globals.setValue("user", mEmail);
                Globals.setValue("pass", mPassword);

                //	    MyProvider = new IntraDataProvider(mEmail);
                MyProvider.LogEvent(getString(R.string.text6_log_event));
                // "user logged in."

                String ws_url = "";
                if (webservice_url != ""){
                    ws_url = webservice_url;
                }else{
                    //ws_url = "http://dev-ws.sharethatdata.com";
                    //ws_url = "https://newtonrp-ws.sharethatdata.com";
                }

                // save user, pass and url for service
                SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
                editor.putString("USER", String.valueOf(mEmail));
                editor.putString("PASS", String.valueOf(mPassword));
                editor.putString("URL", String.valueOf(ws_url));
                editor.commit();

                Log.d("Save for service ", " mEmail: " + mEmail + " ; mPassword: " + mPassword + " ; ws_url: " + ws_url);

            }
            return suc;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {

                // register roles
                mRolesTask = new UserRolesTask();
                mRolesTask.execute((Void) null);

                // call service to check new message and new news, notifications for resources, visit, etc
                //MyProvider = new WSDataProvider(LoginActivity.this);
                //MyProvider.startServicefromNonActivity();

            } else {
                mPasswordView
                        .setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    /**
     * Represents an asynchronous task used to check the roles of
     * the user.
     */
    public class UserRolesTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            MyProvider2 = new WSDataProvider(mEmail, mPassword);
            MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));
            roles = MyProvider2.getUserRoles(mEmail, companyId);

            if (roles != null)
            {
                MyGlobals Globals = ((MyGlobals)getApplication());

                if (roles.contains("Administrator"))
                {
                    Globals.setValue("administrator", "yes");
                } else {
                    Globals.setValue("administrator", "no");
                }
                if (roles.contains("Developer"))
                {
                    Globals.setValue("developer", "yes");
                } else {
                    Globals.setValue("developer", "no");
                }
                if (roles.contains("Manager"))
                {
                    Globals.setValue("manager", "yes");
                } else {
                    Globals.setValue("manager", "no");
                }
                if (roles.contains("Employee"))
                {
                    Globals.setValue("employee", "yes");
                } else {
                    Globals.setValue("employee", "no");
                }
            }

            // get id and name for user logged

            List<cContact> List = MyProvider2.getUserIdbyUsername(mEmail, companyId);

            for(cContact entry : List)
            {

                String id = entry.id;
                String name = entry.name;
                Globals.setValue("idContact", id); // id of user logged
                Globals.setValue("nameContact", name); // name of user logged

                SharedPreferences.Editor pref = getSharedPreferences("PREF_CONTACT", MODE_PRIVATE).edit();
                pref.putString("idContact", id); // id user logged
                pref.putString("nameContact", name); // name of user logged
                pref.commit();

            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    if (roles.size() == 0)
                    {
                        String data_error = MyProvider2.last_error;
                        Toast.makeText(getApplication(), data_error, Toast.LENGTH_LONG).show();
                    }

                    showProgress(false);

                }
            });


            if (success) {


            } else {

            }

            // go to next activity, after you save roles, id and name for user logged

            finish();

            Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(intent);

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

}
