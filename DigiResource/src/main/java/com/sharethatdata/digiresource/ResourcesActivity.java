package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cReservations;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gotic_000 on 3/1/2018.
 */

public class ResourcesActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> resourceList;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ResourcesActivity.this, ResourcesActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        listView = (ListView) findViewById(R.id.listView);

        new LoadResources(companyId).execute();
    }

    class LoadResources extends AsyncTask<String, String, String> {

        public String company_id;

        public LoadResources(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceList = new ArrayList<HashMap<String, String>>();

            List<cResource> myResource = MyProvider.getResources("", company_id, "", 0);
            for(cResource entry : myResource)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("resource_name",  entry.name);
                map.put("resource_group_name",  entry.group_name);
                map.put("code",  entry.code);
                map.put("status",  entry.status);
                map.put("user",  entry.user);

                // adding HashList to ArrayList
                resourceList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (ResourcesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(resourceList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(ResourcesActivity.this, resourceList,
                                R.layout.activity_resources_items_listview,
                                new String[]{"resourceid", "resource_name", "code", "status", "resource_group_name"},
                                new int[]{R.id.resourceid, R.id.name, R.id.code, R.id.status, R.id.group_name});
                        listView.setAdapter(adapter);

                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoResources);
                        textViewNoReservation.setVisibility(View.GONE);
                    }else{
                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoResources);
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }


                }
            });

            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ResourcesActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
