package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.snowcloud_webservice.WSDataProvider;

/**
 * Created by gotic_000 on 2/14/2018.
 */

public class CreateResourceGroupActivity extends Activity {


    TimeUtils MyTimeUtils = null;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";
    String companyId = "";

    ProgressDialog progressDialog;

    EditText editTextName, editTextDescription;

    String resourceName_text;
    String resourceDescription_text;
    Button btnCreateResourceGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_resource_group);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateResourceGroupActivity.this, CreateResourceGroupActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged
        mySelectedUser = myUser;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        editTextName = (EditText) findViewById(R.id.editText_name) ;
        editTextDescription = (EditText) findViewById(R.id.editText_description) ;
        btnCreateResourceGroup = (Button) findViewById(R.id.createResourceGroup);

        btnCreateResourceGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if( editTextName.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextName.setError("Name is required!");
                }

                if( editTextDescription.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextDescription.setError("Description is required!");
                }

                if(failFlag == false){
                    resourceName_text = editTextName.getText().toString();
                    resourceDescription_text = editTextDescription.getText().toString();

                    alertDialog();
                }
            }
        });
    }

    private void alertDialog(){
        new AlertDialog.Builder(CreateResourceGroupActivity.this)
                .setTitle("Group resource ")
                .setMessage("Do you want to add this group resource?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateResourceGroup(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private class CreateResourceGroup extends AsyncTask<String, String, Boolean> {

        String company_id;

        public CreateResourceGroup(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            int ResourceGroupID;
            ResourceGroupID = MyProvider.createResourceGroup(resourceName_text,resourceDescription_text, company_id);

            boolean suc = false;
            if(ResourceGroupID != 0){
                suc = true;
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(CreateResourceGroupActivity.this, "Was not created the group resource ", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CreateResourceGroupActivity.this, "The group resource was created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateResourceGroupActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
