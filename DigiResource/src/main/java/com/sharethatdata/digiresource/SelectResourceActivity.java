package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiresource.MyResources.ResourcesReserved;
import com.sharethatdata.digiresource.Scanner.IntentIntegrator;
import com.sharethatdata.digiresource.Scanner.IntentResult;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cReservations;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by gotic_000 on 1/16/2018.
 */

public class SelectResourceActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ArrayList<HashMap<String, String>> resourceList;
    ArrayList<HashMap<String, String>> resourceGroupList;
    //ArrayList<HashMap<String, String>> resourcebyBarcodeList;
    ArrayList<HashMap<String, String>> resourceReservationList;

    List<String> resourcekeyArray =  new ArrayList<String>();
    List<String> resourcegroupkeyArray =  new ArrayList<String>();

    List<String> resourcenameArray =  new ArrayList<String>();
    List<String> resourcegroupnameArray =  new ArrayList<String>();

    ProgressDialog progressDialog;
    Spinner spinnerResource = null;
    Spinner spinnerResourceGroup = null;
    //EditText editTextBarcode;
    //ImageView imageViewBarcode;
    //ListView modeList;
    //Dialog dialog;
    // DETAILS
    TextView textViewName, textViewDescription, textViewBarcode, textViewCode, textViewLocation, textViewStatus;
    Button buttonTake, buttonReserve;
    ListView listView;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    //boolean processing_scan = false;
    //boolean am_i_scanning = false;

    // save data from menu, after scanning barcode
    String resourceID;
    String resourceID_from_menu = "";
    String resourceGroupID_from_menu = "";
    String resourceGroupID = "";

    // save data in case the user wants to edit
    String resource_name = "";
    String resource_barcode = "";
    String resource_location = "";
    String resource_code = "";
    String resource_description = "";
    String image_string = "";

    String return_days = "";
    String canTake = "";
    String canReserve = "";
    String mustReturn = "";
    String holdStock = "";
    String needScan = "";
    String isLocked = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_resource);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SelectResourceActivity.this, SelectResourceActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            resourceID_from_menu = extras.getString("idResource");
            //resourceName = extras.getString("nameResource");
            resourceGroupID_from_menu = extras.getString("groupidResource");
        }

        spinnerResource = (Spinner) findViewById(R.id.spinnerResource);
        spinnerResourceGroup = (Spinner) findViewById(R.id.spinnerResourceGroup);
        //editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
        //imageViewBarcode = (ImageView) findViewById(R.id.btnSubmit3);
        //DETAILS
        textViewName = (TextView) findViewById(R.id.resourceName);
        textViewDescription = (TextView) findViewById(R.id.resourceDescription);
        textViewBarcode = (TextView) findViewById(R.id.resourceBarcode);
        textViewCode = (TextView) findViewById(R.id.resourceCode);
        textViewLocation = (TextView) findViewById(R.id.resourceLocation);
        textViewStatus = (TextView) findViewById(R.id.resourceStatus);
        buttonTake = (Button) findViewById(R.id.buttonTake);
        buttonReserve = (Button) findViewById(R.id.buttonReserve);
        listView = (ListView) findViewById(R.id.listView);

        //spinnerResource.setEnabled(false);
        //editTextBarcode.setEnabled(false);
        //imageViewBarcode.setEnabled(false);

        // if checkbox camera checked from settings = show button scan
        /*if (blCameraScanner)
        {
            imageViewBarcode.setVisibility(View.VISIBLE);
        } else {
            imageViewBarcode.setVisibility(View.GONE);
        }

        imageViewBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                IntentIntegrator integrator = new IntentIntegrator(SelectResourceActivity.this);
                integrator.initiateScan();
            }
        });*/

        /*editTextBarcode.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                String bc = EditText.getText().toString();

                if (!processing_scan) {

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    processing_scan = true;

                    if(!bc.equals("")){
                        am_i_scanning = true;
                        new LoadResourcesbyBarcode(bc, companyId).execute();
                    }else {
                        processing_scan = false;
                    }

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });*/

        buttonTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SelectResourceActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), ReserveOrTakeActivity.class);
                intent.putExtra("resourceID", resourceID);
                intent.putExtra("action", "take");
                startActivity(intent);
            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SelectResourceActivity.this, "In construction!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), ReserveOrTakeActivity.class);
                intent.putExtra("resourceID", resourceID);
                intent.putExtra("action", "reserve");
                startActivity(intent);
            }
        });

        new LoadResourceGroup(companyId).execute();
    }

    /*protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);
            }
        }
    }*/

    @Override
    public void onResume(){
        super.onResume();
        Log.d("TAG", "onResume");
        String refresh = Globals.getValue("refresh");
        if(!refresh.equals("")){
            resourceGroupID_from_menu = Globals.getValue("groupid");
            resourceID_from_menu = Globals.getValue("resourceid");

            Globals.setValue("refresh","");
            Globals.setValue("groupid","");
            Globals.setValue("resourceid","");

            new LoadResourceGroup(companyId).execute();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // check manager
        String isAdministrator = Globals.getValue("administrator");
        if (isAdministrator == "yes")
        {
            getMenuInflater().inflate(R.menu.detail_resource, menu);
        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_resource:
                /*AlertDialog.Builder dialog1 = new AlertDialog.Builder(SelectResourceActivity.this);
                dialog1.setCancelable(false);
                dialog1.setTitle("Edit resource");
                dialog1.setMessage("Are you sure?" );
                dialog1.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Action for "Cancel".
                            }
                        });

                final AlertDialog alert1 = dialog1.create();
                alert1.show();*/

                Intent intent1 = new Intent(SelectResourceActivity.this, CreateResourceActivity.class);
                intent1.putExtra("edit","edit");
                intent1.putExtra("resourceID",resourceID);
                intent1.putExtra("resourceGroupID",resourceGroupID);

                intent1.putExtra("resource_name",resource_name);
                intent1.putExtra("resource_barcode",resource_barcode);
                intent1.putExtra("resource_location",resource_location);
                intent1.putExtra("resource_code",resource_code);
                intent1.putExtra("resource_description",resource_description);
                intent1.putExtra("image_string",image_string);

                intent1.putExtra("return_days",return_days);
                intent1.putExtra("can_take",canTake);
                intent1.putExtra("can_reserve",canReserve);
                intent1.putExtra("must_return",mustReturn);
                intent1.putExtra("hold_stock",holdStock);
                intent1.putExtra("need_scan",needScan);
                intent1.putExtra("is_locked",isLocked);

                startActivity(intent1);

                return true;
            case R.id.action_delete_resource:
                AlertDialog.Builder dialog = new AlertDialog.Builder(SelectResourceActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("Delete resource");
                dialog.setMessage("Are you sure?" );
                dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        new DeleteResource(resourceID, companyId).execute();
                    }
                })
                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Action for "Cancel".
                            }
                        });

                final AlertDialog alert = dialog.create();
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Background Async Task to Load spinner resource group data by making HTTP Request
     * */
    class LoadResourceGroup extends AsyncTask<String, String, String>{

        String company_id;

        public LoadResourceGroup(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... strings) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceGroupList = new ArrayList<HashMap<String, String>>();

            List<cResourceGroup> resogList = MyProvider.getResourceGroup(company_id, "");
            for(cResourceGroup entry : resogList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                resourceGroupList.add(map);
            }

            return "";
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (SelectResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                public void run() {

                    /*List<String> resogArray =  new ArrayList<String>();
                    resogArray.add(0, "Select resource group");*/

                    resourcegroupnameArray =  new ArrayList<String>();
                    resourcegroupnameArray.add(0, "Select resource group");

                    resourcegroupkeyArray =  new ArrayList<String>();
                    resourcegroupkeyArray.add(0, "");

                    for (HashMap<String, String> map : resourceGroupList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") resourcegroupkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") resourcegroupnameArray.add(entry.getValue());
                        }

                    final ArrayAdapter<String> resog_adapter = new ArrayAdapter<String>(SelectResourceActivity.this, android.R.layout.simple_spinner_item, resourcegroupnameArray);
                    resog_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sResourceGroup = (Spinner) findViewById(R.id.spinnerResourceGroup);
                    sResourceGroup.setAdapter(resog_adapter);

                    if(!resourceGroupID_from_menu.equals("")){
                        int indexResourceGroup = resourcegroupkeyArray.indexOf(resourceGroupID_from_menu);
                        sResourceGroup.setSelection(indexResourceGroup);

                        resourceGroupID_from_menu = "";
                    }

                    sResourceGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Spinner sResourceGroup = (Spinner) findViewById(R.id.spinnerResourceGroup);
                            String mIndex = sResourceGroup.getSelectedItem().toString();

                            if(mIndex.contains(getString(R.string.select_resource_group))){
                                // do nothing

                            }else{

                                int myIndex = sResourceGroup.getSelectedItemPosition();
                                String resource_group_id = resourcegroupkeyArray.get(myIndex);

                                resourceGroupID = resource_group_id;
                                //spinnerResource.setEnabled(true);
                                //editTextBarcode.setEnabled(true);
                                //imageViewBarcode.setEnabled(true);
                                new LoadResources(resource_group_id, company_id).execute();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            });
        }
    }

    class LoadResources extends AsyncTask<String, String, String>{

        String resource_group_id;
        String company_id;

        public LoadResources(String resource_group, String company_id){
            this.resource_group_id = resource_group;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... strings) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceList = new ArrayList<HashMap<String, String>>();

            List<cResource> resoList = MyProvider.getResources(resource_group_id, company_id, "", 0);
            for(cResource entry : resoList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("code", entry.code);
                map.put("status", entry.status);
                map.put("user", entry.user);
                map.put("resource_group", entry.groupID);

                // adding HashList to ArrayList
                resourceList.add(map);
            }

            return "";
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (SelectResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                public void run() {

                    /*List<String> resoArray =  new ArrayList<String>();
                    resoArray.add(0, "Select resource");*/

                    resourcenameArray =  new ArrayList<String>();
                    resourcenameArray.add(0, "Select resource");

                    resourcekeyArray =  new ArrayList<String>();
                    resourcekeyArray.add(0, "");

                    for (HashMap<String, String> map : resourceList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") resourcekeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") resourcenameArray.add(entry.getValue());
                        }

                    final ArrayAdapter<String> reso_adapter = new ArrayAdapter<String>(SelectResourceActivity.this, android.R.layout.simple_spinner_item, resourcenameArray);
                    reso_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sResource = (Spinner) findViewById(R.id.spinnerResource);
                    sResource.setAdapter(reso_adapter);

                    /*if(!resourceName.equals("")){
                        int indexResource = resourcenameArray.indexOf(resourceName);
                        sResource.setSelection(indexResource);
                    }*/

                    if(!resourceID_from_menu.equals("")){
                        int indexResource = resourcekeyArray.indexOf(resourceID_from_menu);
                        sResource.setSelection(indexResource);

                        resourceID_from_menu = "";
                    }

                    sResource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Spinner sResource = (Spinner) findViewById(R.id.spinnerResource);
                            String mIndex = sResource.getSelectedItem().toString();

                            if(mIndex.contains(getString(R.string.select_resource))){
                                // do nothing

                            }else{

                                int myIndex = sResource.getSelectedItemPosition();
                                String idResource = resourcekeyArray.get(myIndex);

                                new LoadResource(idResource, company_id).execute();

                                //reset resource name (is for autoselect resource if use scan barcode)
                                /*if(resourceName.equals("")){
                                    new LoadResource(idResource, company_id).execute();
                                }*/

                                // reset resource name (is for autoselect resource if use scan barcode)
                                //resourceName = "";
                                //am_i_scanning = false;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            });
        }
    }


/*
    class LoadResourcesbyBarcode extends AsyncTask<String, String, String> {

        public String barcode;
        public String company_id;

        public LoadResourcesbyBarcode(String barcode, String company_id){
            this.barcode = barcode;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        *//**
         * getting items from url
         * *//*
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourcebyBarcodeList = new ArrayList<HashMap<String, String>>();

            List<cResource> myResource = MyProvider.getResourcesbyBarcode(barcode, company_id);
            for(cResource entry : myResource)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("resourceid",  entry.id);
                map.put("code",  entry.code);
                map.put("name",  entry.name);
                map.put("user",  entry.user);
                map.put("resource_group",  entry.groupID);
                map.put("barcode",  entry.barcode);

                // adding HashList to ArrayList
                resourcebyBarcodeList.add(map);
            }

            return "";
        }

        *//**
         * After completing background task Dismiss the progress dialog
         * **//*
        protected void onPostExecute(String file_url) {
            if (SelectResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ListAdapter adapter = null;
                    adapter = new SimpleAdapter(SelectResourceActivity.this, resourcebyBarcodeList,
                            R.layout.activity_resources_items_listview_dialog,
                            new String[]{"resourceid", "name"},
                            new int[]{R.id.resource_id, R.id.resource_name});

                    if(resourcebyBarcodeList.size() > 0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SelectResourceActivity.this);
                        builder.setTitle("Choose a resource");

                        modeList = new ListView(SelectResourceActivity.this);
                        modeList.setAdapter(adapter);
                        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
                            {
                                final String idResource = ((TextView) itemClicked.findViewById(R.id.resource_id)).getText().toString();
                                final String nameResource = ((TextView) itemClicked.findViewById(R.id.resource_name)).getText().toString();
                                *//*final String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
                                final String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewAddressID)).getText().toString();
                                final String idRoute = ((TextView) itemClicked.findViewById(R.id.textViewRoute)).getText().toString();
                                final String barcode = ((TextView) itemClicked.findViewById(R.id.packageBarcode)).getText().toString();
                                final String address = ((TextView) itemClicked.findViewById(R.id.packageAddress)).getText().toString();
                                final String status = ((TextView) itemClicked.findViewById(R.id.packageStatus)).getText().toString();
                                final String status_id = ((TextView) itemClicked.findViewById(R.id.textViewStatusId)).getText().toString();*//*

                                // 1 = Loaded for delivery
                                *//*if(status_id.equals("1")){
                                    new AlertDialog.Builder(SelectActionActivity.this)
                                            .setTitle("Package")
                                            .setMessage("Add to current route?")
                                            .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    new AddPackageToRoute(pharmacyId, idDelivery, idRoute, idPackage, idScript, idAddress, barcode, address, status).execute();

                                                }
                                            })
                                            .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // do nothing
                                                    editText = (EditText) findViewById(R.id.editText_barcode);
                                                    editText.setText(null);
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }else{
                                    // start detail activity
                                    Intent intent = new Intent(getApplicationContext(), MyResourcesActivity.class);
                                    intent.putExtra("quick_scan", true);
                                    intent.putExtra("idAddress", idAddress);
                                    intent.putExtra("idPackage", idPackage);
                                    *//**//*intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("packageIdCode", barcode);
                                    intent.putExtra("packageStatus", status);
                                    intent.putExtra("packageStatusID", status_id);
                                    intent.putExtra("packageAddress", address);
                                    //intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("idScript", idScript);
                                    intent.putExtra("idDelivery", idDelivery);*//**//*
                                    startActivity(intent);
                                }*//*

                                // clear the textbox
                                editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
                                editTextBarcode.setText("");

                                *//*Intent intent = new Intent(getApplicationContext(), ResourceDetailActivity.class);
                                intent.putExtra("idResource", idResource);
                                intent.putExtra("nameResource", nameResource);
                                startActivity(intent);*//*
                                new LoadResource(idResource, company_id).execute();

                                dialog.dismiss();

                            }

                        });

                        builder.setView(modeList);

                        dialog = new Dialog(SelectResourceActivity.this);
                        dialog = builder.create();
                        dialog.show();
                    }else{
                        Toast.makeText(SelectResourceActivity.this, "No resource found with barcode " + barcode, Toast.LENGTH_SHORT).show();
                    }

                    // always clear the textbox
                    //editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
                    //editTextBarcode.setText(null);

                }
            });

            processing_scan = false;
            hideProgressDialog();
        }
    }

    */

    /**
     * Background Async Task to Load Project Item by making HTTP Request
     * */
    class LoadResource extends AsyncTask<String, String, String> {

        String resource_id;
        String company_id;


        public LoadResource(String resource_id, String company_id) {
            this.resource_id = resource_id;
            this.company_id = company_id;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cResource resource = MyProvider.getResource(resource_id, company_id);

            if (resource != null) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // if I am using scan barcode in this activity, uncomment this code
                        /*if(am_i_scanning){
                            resourceName = resource.name;

                            int indexGroup = resourcegroupkeyArray.indexOf(resource.groupID);
                            spinnerResourceGroup.setSelection(indexGroup);
                        }*/

                        TextView textViewNoResource = (TextView) findViewById(R.id.textViewNoResource);
                        textViewNoResource.setVisibility(View.GONE);

                        LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.linearLayout3);
                        linearLayout3.setVisibility(View.VISIBLE);
                    }
                });

                resourceID = resource_id;

                /*if (resource.image_id != "0")
                {
                    // myImageData = MyRoboProvider.getImage(project.image_id);
                    String myImageData = MyProvider.getImage(resource.image_id);
                    image_string = myImageData;
                }*/

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewName.setText(Html.fromHtml(resource.name));
                        textViewDescription.setText(Html.fromHtml(resource.description));
                        textViewBarcode.setText(Html.fromHtml(resource.barcode));
                        textViewCode.setText(Html.fromHtml(resource.code));
                        textViewLocation.setText(Html.fromHtml(resource.location));
                        textViewStatus.setText(Html.fromHtml(resource.status));

                    /*if (project.full_image != "")
                    {
                        myImageData = project.full_image;
                        image_string = myImageData;
                    }*/

                        if (!resource.image_id.equals("")) {
                            image_string = resource.image_full;
                            Log.d("image", image_string);
                            ImageView imageView1 = (ImageView) findViewById(R.id.imageViewResource);
                            Bitmap d = ConvertByteArrayToBitmap(image_string);
                            if (d != null) {
                                int nh = (int) (d.getHeight() * (512.0 / d.getWidth()));
                                Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                                imageView1.setVisibility(View.VISIBLE);
                                imageView1.setImageBitmap(scaled);
                            }
                        } else {
                            ImageView imageView1 = (ImageView) findViewById(R.id.imageViewResource);
                            imageView1.setVisibility(View.GONE);
                        }


                        buttonTake.setVisibility(View.VISIBLE);
                        buttonReserve.setVisibility(View.VISIBLE);

                        resource_name = resource.name;
                        resource_barcode = resource.barcode;
                        resource_location = resource.location;
                        resource_code = resource.code;
                        resource_description = resource.description;

                        return_days = resource.return_days;
                        canTake = resource.can_take;
                        canReserve = resource.can_reserve;
                        mustReturn = resource.must_return;
                        holdStock = resource.hold_stock;
                        needScan = resource.need_scan;
                        isLocked = resource.is_locked;

                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView textViewNoResource = (TextView) findViewById(R.id.textViewNoResource);
                        textViewNoResource.setVisibility(View.VISIBLE);
                    }
                });

            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // dismiss the dialog after getting all products
            if (SelectResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            new LoadReservations(resource_id,company_id).execute();

            hideProgressDialog();
        }
    }


    class LoadReservations extends AsyncTask<String, String, String> {

        public String resourceid;
        public String company_id;

        public LoadReservations(String resourceid, String company_id){
            this.resourceid = resourceid;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceReservationList = new ArrayList<HashMap<String, String>>();

            List<cReservations> myReservation = MyProvider.getReservations(resourceid, company_id);
            for(cReservations entry : myReservation)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("reservationid",  entry.id);
                map.put("description",  entry.description);
                map.put("startdate",  entry.start_date);
                map.put("enddate",  entry.end_date);
                map.put("user",  entry.user);

                // adding HashList to ArrayList
                resourceReservationList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (SelectResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(resourceReservationList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(SelectResourceActivity.this, resourceReservationList,
                                R.layout.activity_reservations_items_listview,
                                new String[]{"reservationid", "startdate", "enddate", "description", "user"},
                                new int[]{R.id.reservationid, R.id.startdate, R.id.enddate, R.id.description, R.id.user});
                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);

                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.GONE);
                    }else{
                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }


                }
            });

            hideProgressDialog();
        }
    }

        // when get and show the image
        private Bitmap ConvertByteArrayToBitmap(String b)
        {
            Bitmap decodedByte = null;
            if(b.isEmpty() || b.contentEquals(""))
            {
                decodedByte = null;
            }else{
                //byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
                byte[] decodedString = Base64.decode(b, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }

            return decodedByte;
        }

        class DeleteResource extends AsyncTask<String, String, Boolean>{

            public String company_id;
            public String resource_id;

            public DeleteResource(String resource_id, String company_id){
                this.resource_id = resource_id;
                this.company_id = company_id;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog(getString(R.string.loading_alert_dialog));
            }

            @Override
            protected Boolean doInBackground(String... params) {
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));


                boolean suc = MyProvider.deleteResource(resource_id, company_id);

                return suc;
            }

            protected void onPostExecute(final Boolean success) {

                progressDialog.dismiss();

                // updating UI from Background Thread
                runOnUiThread(new Runnable() {

                    public void run() {

                        if (success == false)
                        {
                            Toast.makeText(SelectResourceActivity.this, "Was not deleted the resource", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SelectResourceActivity.this, "The resource was deleted!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
            }
        }




    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(SelectResourceActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
