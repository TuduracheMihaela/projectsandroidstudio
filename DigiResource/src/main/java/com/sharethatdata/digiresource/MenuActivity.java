package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiresource.Scanner.IntentIntegrator;
import com.sharethatdata.digiresource.Scanner.IntentResult;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by gotic_000 on 1/16/2018.
 */

public class MenuActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ArrayList<HashMap<String, String>> resourcebyBarcodeList;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    ProgressDialog progressDialog;


    //Button takeButton, reserveButton, returnButton;
    EditText editTextBarcode;
    ImageView imageViewBarcode;
    Button returnButton;
    Button actionButton;

    ListView modeList;
    Dialog dialog;

    boolean processing_scan = false;
    boolean am_i_scanning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(MenuActivity.this, MenuActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        //takeButton = (Button) findViewById(R.id.take_button);
        //reserveButton = (Button) findViewById(R.id.reserve_button);
        editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
        imageViewBarcode = (ImageView) findViewById(R.id.btnSubmit3);
        actionButton = (Button) findViewById(R.id.action_button);
        returnButton = (Button) findViewById(R.id.return_button);

        // if checkbox camera checked from settings = show button scan
        if (blCameraScanner)
        {
            imageViewBarcode.setVisibility(View.VISIBLE);
        } else {
            imageViewBarcode.setVisibility(View.GONE);
        }

        imageViewBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                IntentIntegrator integrator = new IntentIntegrator(MenuActivity.this);
                integrator.initiateScan();
            }
        });

        editTextBarcode.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                String bc = EditText.getText().toString();

                if (!processing_scan) {

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    processing_scan = true;

                    if(!bc.equals("")){
                        am_i_scanning = true;
                        new LoadResourcesbyBarcode(bc, companyId).execute();
                    }else {
                        processing_scan = false;
                    }

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, SelectResourceActivity.class);
                startActivity(intent);
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, MyResourcesActivity.class);
                startActivity(intent);
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);
            }
        }
    }

    class LoadResourcesbyBarcode extends AsyncTask<String, String, String> {

        public String barcode;
        public String company_id;

        public LoadResourcesbyBarcode(String barcode, String company_id){
            this.barcode = barcode;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourcebyBarcodeList = new ArrayList<HashMap<String, String>>();

            List<cResource> myResource = MyProvider.getResourcesbyBarcode(barcode, company_id);
            for(cResource entry : myResource)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("resourceid",  entry.id);
                map.put("code",  entry.code);
                map.put("name",  entry.name);
                map.put("user",  entry.user);
                map.put("group_id",  entry.groupID);
                map.put("barcode",  entry.barcode);

                // adding HashList to ArrayList
                resourcebyBarcodeList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (MenuActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ListAdapter adapter = null;
                    adapter = new SimpleAdapter(MenuActivity.this, resourcebyBarcodeList,
                            R.layout.activity_resources_items_listview_dialog,
                            new String[]{"resourceid", "name", "group_id"},
                            new int[]{R.id.resource_id, R.id.resource_name, R.id.resource_group_id});

                    if(resourcebyBarcodeList.size() > 0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                        builder.setTitle("Choose a resource");

                        modeList = new ListView(MenuActivity.this);
                        modeList.setAdapter(adapter);
                        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
                            {
                                final String idResource = ((TextView) itemClicked.findViewById(R.id.resource_id)).getText().toString();
                                final String nameResource = ((TextView) itemClicked.findViewById(R.id.resource_name)).getText().toString();
                                final String groupidResource = ((TextView) itemClicked.findViewById(R.id.resource_group_id)).getText().toString();
                                /*final String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
                                final String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewAddressID)).getText().toString();
                                final String idRoute = ((TextView) itemClicked.findViewById(R.id.textViewRoute)).getText().toString();
                                final String barcode = ((TextView) itemClicked.findViewById(R.id.packageBarcode)).getText().toString();
                                final String address = ((TextView) itemClicked.findViewById(R.id.packageAddress)).getText().toString();
                                final String status = ((TextView) itemClicked.findViewById(R.id.packageStatus)).getText().toString();
                                final String status_id = ((TextView) itemClicked.findViewById(R.id.textViewStatusId)).getText().toString();*/

                                // 1 = Loaded for delivery
                                /*if(status_id.equals("1")){
                                    new AlertDialog.Builder(SelectActionActivity.this)
                                            .setTitle("Package")
                                            .setMessage("Add to current route?")
                                            .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    new AddPackageToRoute(pharmacyId, idDelivery, idRoute, idPackage, idScript, idAddress, barcode, address, status).execute();

                                                }
                                            })
                                            .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // do nothing
                                                    editText = (EditText) findViewById(R.id.editText_barcode);
                                                    editText.setText(null);
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }else{
                                    // start detail activity
                                    Intent intent = new Intent(getApplicationContext(), MyResourcesActivity.class);
                                    intent.putExtra("quick_scan", true);
                                    intent.putExtra("idAddress", idAddress);
                                    intent.putExtra("idPackage", idPackage);
                                    *//*intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("packageIdCode", barcode);
                                    intent.putExtra("packageStatus", status);
                                    intent.putExtra("packageStatusID", status_id);
                                    intent.putExtra("packageAddress", address);
                                    //intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("idScript", idScript);
                                    intent.putExtra("idDelivery", idDelivery);*//*
                                    startActivity(intent);
                                }*/

                                // clear the textbox
                                editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
                                editTextBarcode.setText("");

                                Intent intent = new Intent(getApplicationContext(), SelectResourceActivity.class);
                                intent.putExtra("idResource", idResource);
                                intent.putExtra("nameResource", nameResource);
                                intent.putExtra("groupidResource", groupidResource);
                                startActivity(intent);


                                dialog.dismiss();

                            }

                        });

                        builder.setView(modeList);

                        dialog = new Dialog(MenuActivity.this);
                        dialog = builder.create();
                        dialog.show();
                    }else{
                        Toast.makeText(MenuActivity.this, "No resource found with barcode " + barcode, Toast.LENGTH_SHORT).show();
                    }

                    // always clear the textbox
                    //editTextBarcode = (EditText) findViewById(R.id.editText_barcode);
                    //editTextBarcode.setText(null);

                }
            });

            processing_scan = false;
            hideProgressDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        String isAdministrator = Globals.getValue("administrator");
        if (isAdministrator == "yes")
        {
            getMenuInflater().inflate(R.menu.menu_settings_admin, menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_settings, menu);
        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_create_resource:
                Intent intent1 = new Intent(this, CreateResourceActivity.class); startActivity(intent1);
                return true;
            case R.id.action_create_resource_group:
                Intent intent2 = new Intent(this, CreateResourceGroupActivity.class); startActivity(intent2);
                return true;
            case R.id.action_locations:
                //Intent intent3 = new Intent(this, CreateLocationActivity.class); startActivity(intent3);
                Intent intent3 = new Intent(this, LocationsActivity.class); startActivity(intent3);
                return true;
            case R.id.action_settings:
                String isAdministrator = Globals.getValue("administrator");
                if (isAdministrator == "yes")
                {
                    Intent intent4 = new Intent(this, SettingsAdminActivity.class); startActivity(intent4);
                }else {
                    Intent intent5 = new Intent(this, SettingsActivity.class); startActivity(intent5);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask the user if they want to quit
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Quit DigiResource")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Stop the activity
                            MenuActivity.this.finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(MenuActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
