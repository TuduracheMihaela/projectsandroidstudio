package com.sharethatdata.digiresource;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiresource.Scanner.IntentResult;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cResourceGroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by gotic_000 on 2/1/2018.
 */

public class CreateResourceActivity extends Activity {

    TimeUtils MyTimeUtils = null;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";
    String companyId = "";

    ArrayList<HashMap<String, String>> resourceGroupList;
    List<String> resourcegroupkeyArray =  new ArrayList<String>();

    ProgressDialog progressDialog;

    // For zoom image /////////////////////////
    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    private String mCurrentPhotoPathAbsolute;
    private String mCurrentPhotoPath;
    //private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_BROWSE_PHOTO = 2;
    private static final int PERMISSION_TAKE_PHOTO = 3;
    private static final int REQUEST_SCAN_BARCODE = 4;
    private static final int REQUEST_SCAN_LOCATION = 5;
    private Bitmap myBitmap;
    private byte[] bitmapByteArray;

    EditText editTextBarcode, editTextLocation;
    EditText editTextCode, editTextName, editTextDescription;
    Spinner spinnerResourceGroup;
    ImageView imageViewBarcode, imageViewLocation;
    ImageView imageView1;
    Button btnCreateImage, btnSearchImage;
    Button btnCreateResource;

    CheckBox can_take, can_reserve, must_return, hold_stock, need_scan, is_locked;
    EditText editTextReturnDays;

    boolean permission_camera = false;

    String resourceBarcode_text;
    String resourceLocation_text;
    String resourceCode_text;
    String resourceName_text;
    String resourceDescription_text;
    String myResourceGroupID = "";

    String return_days = "";
    boolean canTake = false;
    boolean canReserve = false;
    boolean mustReturn = false;
    boolean holdStock = false;
    boolean needScan = false;
    boolean isLocked = false;

    String myResourceID = "";
    String image_string = "";
    String edit = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_resource);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateResourceActivity.this, CreateResourceActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged
        mySelectedUser = myUser;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            edit = extras.getString("edit");
            myResourceID = extras.getString("resourceID");
            myResourceGroupID = extras.getString("resourceGroupID");

            resourceName_text = extras.getString("resource_name");
            resourceBarcode_text = extras.getString("resource_barcode");
            resourceLocation_text = extras.getString("resource_location");
            resourceCode_text = extras.getString("resource_code");
            resourceDescription_text = extras.getString("resource_description");
            image_string = extras.getString("image_string");

            return_days = extras.getString("return_days");
            String canTakeString = extras.getString("can_take");
            if(canTakeString.equals("1")) canTake = true;
            String canReserveString = extras.getString("can_reserve");
            if(canReserveString.equals("1")) canReserve = true;
            String mustReturnString = extras.getString("must_return");
            if(mustReturnString.equals("1")) mustReturn = true;
            String holdStockString = extras.getString("hold_stock");
            if(holdStockString.equals("1")) holdStock = true;
            String needScanString = extras.getString("need_scan");
            if(needScanString.equals("1")) needScan = true;
            String isLockedString = extras.getString("is_locked");
            if(isLockedString.equals("1")) isLocked = true;

        }

        editTextBarcode = (EditText) findViewById(R.id.editText_barcode) ;
        editTextLocation = (EditText) findViewById(R.id.editText_location) ;
        editTextCode = (EditText) findViewById(R.id.editText_code) ;
        editTextName = (EditText) findViewById(R.id.editText_name) ;
        editTextDescription = (EditText) findViewById(R.id.editText_description) ;
        spinnerResourceGroup = (Spinner) findViewById(R.id.spinnerResourceGroup);
        imageViewBarcode = (ImageView) findViewById(R.id.btnSubmit3) ;
        imageViewLocation = (ImageView) findViewById(R.id.btnSubmit4) ;
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView1.setVisibility(View.GONE);
        btnSearchImage = (Button) findViewById(R.id.pictureFromPhone);
        btnCreateImage = (Button) findViewById(R.id.pictureCamera);
        btnCreateResource = (Button) findViewById(R.id.createResource);

        editTextReturnDays = (EditText) findViewById(R.id.editText_return_days);
        can_take = (CheckBox) findViewById(R.id.can_take);
        can_reserve = (CheckBox) findViewById(R.id.can_reserve);
        must_return = (CheckBox) findViewById(R.id.must_return);
        hold_stock = (CheckBox) findViewById(R.id.hold_stock);
        need_scan = (CheckBox) findViewById(R.id.need_scan);
        is_locked = (CheckBox) findViewById(R.id.is_locked);

        if(!edit.equals("")){
            btnCreateResource.setText("Save resource");
            setTitle("Edit Resource");
        }else {
            btnCreateResource.setText("Create resource");
            setTitle("Create Resource");
        }

        // if checkbox camera checked from settings = show button scan
        if (blCameraScanner)
        {
            imageViewBarcode.setVisibility(View.VISIBLE);
            imageViewLocation.setVisibility(View.VISIBLE);
        } else {
            imageViewBarcode.setVisibility(View.GONE);
            imageViewLocation.setVisibility(View.GONE);
        }

        imageViewBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                com.sharethatdata.digiresource.Scan.IntentIntegrator intentintegrator = new com.sharethatdata.digiresource.Scan.IntentIntegrator(CreateResourceActivity.this);
                intentintegrator.setRequestCode(REQUEST_SCAN_BARCODE);
                intentintegrator.initiateScan();
                //startActivityForResult(integrator, REQUEST_SCAN_BARCODE);
            }
        });

        imageViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                com.sharethatdata.digiresource.Scan.IntentIntegrator intentintegrator = new com.sharethatdata.digiresource.Scan.IntentIntegrator(CreateResourceActivity.this);
                intentintegrator.setRequestCode(REQUEST_SCAN_LOCATION);
                intentintegrator.initiateScan();
            }
        });

        btnSearchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();

                intent.setAction(Intent.ACTION_GET_CONTENT);

                intent.addCategory(Intent.CATEGORY_OPENABLE);

                intent.setType("image/*");

                startActivityForResult(intent, REQUEST_BROWSE_PHOTO);
            }
        });

        btnCreateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED
                        &&
                        ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_DENIED  ){
                    ActivityCompat.requestPermissions(CreateResourceActivity.this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
                }else{
                    permission_camera = true;
                }

                if(permission_camera){
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            Log.d("IMAGE onCreate", ex.toString());
                        }

                        if (photoFile != null) {
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
                        }
                        //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                else {
                    Toast.makeText(CreateResourceActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCreateResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if(editTextBarcode.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextBarcode.setError("Resource barcode is required!");
                }


                /*if(editTextLocation.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextLocation.setError("Location barcode is required!");
                }*/


                if(editTextCode.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextCode.setError("Code is required!");
                }

                if( editTextName.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextName.setError("Name is required!");
                }

                /*if( editTextDescription.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextDescription.setError("Description is required!");
                }*/

                String mIndex = spinnerResourceGroup.getSelectedItem().toString();
                if(mIndex.contains(getString(R.string.select_resource_group)))
                {
                    failFlag = true;
                    SetError("Group resource is required!");
                }

                if(failFlag == false){
                    resourceBarcode_text = editTextBarcode.getText().toString();
                    resourceLocation_text = editTextLocation.getText().toString();
                    resourceCode_text = editTextCode.getText().toString();
                    resourceName_text = editTextName.getText().toString();
                    resourceDescription_text = editTextDescription.getText().toString();

                    return_days = editTextReturnDays.getText().toString();
                    canTake = can_take.isChecked();
                    canReserve = can_reserve.isChecked();
                    mustReturn = must_return.isChecked();
                    holdStock = hold_stock.isChecked();
                    needScan = need_scan.isChecked();
                    isLocked = is_locked.isChecked();

                    if(!edit.equals("")){
                        // Edit Resource
                        alertDialogEdit();
                    }else{
                        // Create resource
                        alertDialogCreate();
                    }

                }
            }
        });

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomImageFromThumb(imageView1, myBitmap);
            }
        });

        new LoadList(companyId).execute();
    }

    private void alertDialogCreate(){
        new AlertDialog.Builder(CreateResourceActivity.this)
                .setTitle("Resource")
                .setMessage("Do you want to create this resource?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateResource(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void alertDialogEdit(){
        new AlertDialog.Builder(CreateResourceActivity.this)
                .setTitle("Resource")
                .setMessage("Do you want to edit this resource?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateResource(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Background Async Task to Load spinner resource group data by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        public String company_id;

        public LoadList(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected String doInBackground(String... strings) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceGroupList = new ArrayList<HashMap<String, String>>();

            List<cResourceGroup> resogList = MyProvider.getResourceGroup(company_id, "");
            for(cResourceGroup entry : resogList)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
                map.put("description", entry.description);

                // adding HashList to ArrayList
                resourceGroupList.add(map);
            }

            return "";
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (CreateResourceActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            hideProgressDialog();

            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> resogArray =  new ArrayList<String>();

                    if(resourceGroupList.size()>0){
                        resogArray.add(0, getString(R.string.select_resource_group));
                        resourcegroupkeyArray.add(0, "");
                    }else{
                        resogArray.add(0, getString(R.string.select_no_resource_group));
                        resourcegroupkeyArray.add(0, "");
                    }

                    for (HashMap<String, String> map : resourceGroupList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") resourcegroupkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") resogArray.add(entry.getValue());
                        }

                    final ArrayAdapter<String> resog_adapter = new ArrayAdapter<String>(CreateResourceActivity.this, android.R.layout.simple_spinner_item, resogArray);
                    resog_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sResourceGroup = (Spinner) findViewById(R.id.spinnerResourceGroup);
                    sResourceGroup.setAdapter(resog_adapter);

                    if(!edit.equals("")){
                        editTextBarcode.setText(resourceBarcode_text);
                        //editTextBarcode.setEnabled(false);
                        editTextLocation.setText(resourceLocation_text);
                        //editTextLocation.setEnabled(false);
                        editTextCode.setText(resourceCode_text);
                        editTextName.setText(resourceName_text);
                        editTextDescription.setText(resourceDescription_text);

                        int indexResource = resourcegroupkeyArray.indexOf(myResourceGroupID);
                        sResourceGroup.setSelection(indexResource);

                        if (!image_string.equals("")) {
                            Log.d("image", image_string);
                            Bitmap d = ConvertByteArrayToBitmap(image_string);
                            if (d != null) {
                                int nh = (int) (d.getHeight() * (512.0 / d.getWidth()));
                                Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                                imageView1.setVisibility(View.VISIBLE);
                                imageView1.setImageBitmap(scaled);
                            }
                        }

                        editTextReturnDays.setText(return_days);
                        can_take.setChecked(canTake);
                        can_reserve.setChecked(canReserve);
                        must_return.setChecked(mustReturn);
                        hold_stock.setChecked(holdStock);
                        need_scan.setChecked(needScan);
                        is_locked.setChecked(isLocked);

                    }

                }
            });
        }
    }

    private class CreateResource extends AsyncTask<String, String, Boolean>{

        public String company_id;

        public CreateResource(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // extract data
            Spinner resourceGroupSpinner = (Spinner) findViewById(R.id.spinnerResourceGroup);
            int myIndex = resourceGroupSpinner.getSelectedItemPosition();
            if(myIndex != -1){
                myResourceGroupID = resourcegroupkeyArray.get(myIndex);
            }


            //int BitmapID = 0;
            int ResourceID = 0;
            boolean suc = false;
            if (myBitmap != null)
            {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                bitmapByteArray = stream.toByteArray();

                //BitmapID = MyProvider.createImage("image_resource", bitmapByteArray, bitmapByteArray);
                // call web method
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                if(!edit.equals("")){
                    // Edit Resource
                    suc = MyProvider.editResource(myResourceID, resourceCode_text, resourceName_text, resourceDescription_text, myResourceGroupID,
                            resourceLocation_text, canTake, canReserve, mustReturn, holdStock, needScan, isLocked, return_days,
                            bitmapByteArray, "image_resource", company_id);
                }else{
                    // Create Resource
                    ResourceID = MyProvider.createResource(resourceCode_text, resourceName_text,resourceDescription_text, myResourceGroupID,
                            resourceLocation_text, canTake, canReserve, mustReturn, holdStock, needScan, isLocked, return_days,
                            bitmapByteArray, "image_resource", company_id);
                }

            }else{
                // call web method
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                if(!edit.equals("")){
                    // Edit Resource
                    suc = MyProvider.editResource(myResourceID, resourceCode_text, resourceName_text, resourceDescription_text, myResourceGroupID,
                            resourceLocation_text, canTake, canReserve, mustReturn, holdStock, needScan, isLocked, return_days,
                            bitmapByteArray, "", company_id);
                }else{
                    // Create Resource
                    ResourceID = MyProvider.createResource(resourceCode_text, resourceName_text,resourceDescription_text, myResourceGroupID,
                            resourceLocation_text, canTake, canReserve, mustReturn, holdStock, needScan, isLocked, return_days,
                            bitmapByteArray, "", company_id);
                }
            }

            if(ResourceID != 0){
               suc =  MyProvider.addBarcodeToResource(String.valueOf(ResourceID), resourceBarcode_text, company_id);
            }


            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        if(!edit.equals("")){
                            Toast.makeText(CreateResourceActivity.this, "Was not saved the resource", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CreateResourceActivity.this, "Was not created the resource", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        if(!edit.equals("")){
                            Globals.setValue("refresh","refresh");
                            Globals.setValue("groupid",myResourceGroupID);
                            Globals.setValue("resourceid",myResourceID);
                            Toast.makeText(CreateResourceActivity.this, "The resource was saved!", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CreateResourceActivity.this, "The resource was created!", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }

                }
            });
        }
    }

    /**
     * When a <code>errorMessage</code> is specified, pops up an error window with the message
     * text, and creates an error icon in the secondary unit spinner. Error cleared through passing
     * in a null string.
     * @param errorMessage Error message to display, or null to clear.
     */
    public void SetError(String errorMessage)
    {
        View view = spinnerResourceGroup.getSelectedView();

        // Set TextView in Secondary Unit spinner to be in error so that red (!) icon
        // appears, and then shake control if in error
        TextView tvListItem = (TextView)view;

        // Set fake TextView to be in error so that the error message appears
        TextView tvInvisibleError = (TextView)findViewById(R.id.tvInvisibleError);

        // Shake and set error if in error state, otherwise clear error
        if(errorMessage != null)
        {
            tvListItem.setError(errorMessage);
            tvListItem.requestFocus();

            // Shake the spinner to highlight that current selection
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            spinnerResourceGroup.startAnimation(shake);

            tvInvisibleError.requestFocus();
            tvInvisibleError.setError(errorMessage);
        }
        else
        {
            tvListItem.setError(null);
            tvInvisibleError.setError(null);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    /**
     * This is very useful to overcome Memory waring issue while selecting image
     * from Gallery
     *
     * @param selectedImage
     * @param context
     * @return Bitmap
     * @throws FileNotFoundException
     */
    public static Bitmap decodeBitmap(Uri selectedImage, Context context)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(selectedImage), null, o2);
    }

    private File createImageFile() throws IOException {

        if(permission_camera ){
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            mCurrentPhotoPathAbsolute = image.getAbsolutePath();
            return image;
        }else {
            return null;
        }
    }

    // Methods - set visible the preview image
    private void AttachBitmapToReport()
    {
        int photoW = myBitmap.getWidth();
        int photoH = myBitmap.getHeight();
        int scaleFactor = Math.min(photoW/100, photoH/100);
        int NewW = photoW/scaleFactor;
        int NewH = photoH/scaleFactor;

        imageView1.setVisibility(View.VISIBLE);
        imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        InputStream stream = null;

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
            AttachBitmapToReport();
        }

        if (requestCode == REQUEST_BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
        {

            try
            {
                stream = getContentResolver().openInputStream(data.getData());
                myBitmap = BitmapFactory.decodeStream(stream);
                AttachBitmapToReport();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }

        // SCAN
        IntentResult scanResult = com.sharethatdata.digiresource.Scan.IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null && requestCode == REQUEST_SCAN_BARCODE ) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);
            }
        }

        if (scanResult != null && requestCode == REQUEST_SCAN_LOCATION ) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_location);
                EditText.setText(UPCScanned);
            }
        }

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void zoomImageFromThumb(final View thumbView, Bitmap imageBitmap) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);
        expandedImageView.setImageBitmap(imageBitmap);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.scrollView)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateResourceActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
