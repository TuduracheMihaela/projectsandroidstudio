package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cLocation;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gotic_000 on 3/12/2018.
 */

public class LocationsActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> locationList;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    String companyId = "";

    ListView listView;


    String id_text;
    String barcode_text;
    String name_text;
    String code_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(LocationsActivity.this, LocationsActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        listView = (ListView) findViewById(R.id.listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                id_text = ((TextView) itemClicked.findViewById(R.id.locationid)).getText().toString();
                name_text = ((TextView) itemClicked.findViewById(R.id.name)).getText().toString();
                code_text = ((TextView) itemClicked.findViewById(R.id.code)).getText().toString();
                barcode_text = ((TextView) itemClicked.findViewById(R.id.barcode)).getText().toString();

                alertDialog();
            }

        });

        new LoadLocations(companyId).execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d("TAG", "onResume");
        String refresh = Globals.getValue("refresh");
        if(refresh.equals("refresh")){
            new LoadLocations(companyId).execute();
            Globals.setValue("refresh","");
        }
    }

    private void alertDialog(){
        new AlertDialog.Builder(LocationsActivity.this)
                .setTitle("Location")
                .setMessage("Select action:")
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), CreateLocationActivity.class);
                        intent.putExtra("edit","edit");
                        intent.putExtra("id",id_text);
                        intent.putExtra("name",name_text);
                        intent.putExtra("code",code_text);
                        intent.putExtra("barcode",barcode_text);
                        startActivity(intent);

                    }
                })
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new AlertDialog.Builder(LocationsActivity.this)
                                .setTitle("Location")
                                .setMessage("Delete location?")
                                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        new DeleteLocation(id_text, companyId).execute();
                                    }
                                })
                                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_location, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_create_location:
                Intent intent = new Intent(this, CreateLocationActivity.class); startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class LoadLocations extends AsyncTask<String, String, String> {

        public String company_id;

        public LoadLocations(String company_id){
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            locationList = new ArrayList<HashMap<String, String>>();

            List<cLocation> myLocation = MyProvider.getLocations(company_id);
            for(cLocation entry : myLocation)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("code",  entry.code);
                map.put("barcode",  entry.barcode);
                map.put("resource",  entry.resource);
                map.put("stock",  entry.stock);

                // adding HashList to ArrayList
                locationList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (LocationsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(locationList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(LocationsActivity.this, locationList,
                                R.layout.activity_locations_items_listview,
                                new String[]{"id", "name", "code", "barcode", "resource", "stock"},
                                new int[]{R.id.locationid, R.id.name, R.id.code, R.id.barcode, R.id.resource, R.id.stock});
                        listView.setAdapter(adapter);

                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoResources);
                        textViewNoReservation.setVisibility(View.GONE);
                    }else{
                        TextView textViewNoReservation = (TextView) findViewById(R.id.textViewNoResources);
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }


                }
            });

            hideProgressDialog();
        }
    }

    class DeleteLocation extends AsyncTask<String, String, Boolean>{

        public String company_id;
        public String location_id;

        public DeleteLocation(String location_id, String company_id){
            this.location_id = location_id;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... params) {
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));


            boolean suc = MyProvider.deleteLocation(location_id, company_id);

            return suc;
        }

        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(LocationsActivity.this, "Was not deleted the location", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(LocationsActivity.this, "The location was deleted!", Toast.LENGTH_SHORT).show();
                        new LoadLocations(companyId).execute();
                    }
                }
            });
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LocationsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }


}
