package com.sharethatdata.digiresource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.sharethatdata.digiresource.Scanner.IntentResult;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;

import java.io.ByteArrayOutputStream;

/**
 * Created by gotic_000 on 3/12/2018.
 */

public class CreateLocationActivity extends Activity {

    TimeUtils MyTimeUtils = null;

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";
    String companyId = "";

    ProgressDialog progressDialog;

    private static final int REQUEST_SCAN_BARCODE = 4;

    EditText editTextBarcode;
    EditText editTextName;
    EditText editTextCode;
    ImageView imageViewBarcode;
    Button btnCreateLocation;

    String resourceBarcode_text;
    String resourceName_text;
    String resourceCode_text;
    String edit = "";

    String myLocationID = "";
    String myLocationName = "";
    String myLocationCode = "";
    String myLocationBarcode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_location);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateLocationActivity.this, CreateLocationActivity.this);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged
        mySelectedUser = myUser;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            edit = extras.getString("edit");
            myLocationID = extras.getString("id");
            myLocationName = extras.getString("name");
            myLocationCode = extras.getString("code");
            myLocationBarcode = extras.getString("barcode");
        }

        editTextBarcode = (EditText) findViewById(R.id.editText_barcode) ;
        editTextName = (EditText) findViewById(R.id.editText_name) ;
        editTextCode = (EditText) findViewById(R.id.editText_code) ;
        imageViewBarcode = (ImageView) findViewById(R.id.btnSubmit3) ;
        btnCreateLocation = (Button) findViewById(R.id.createLocation);


        if(!edit.equals("")){
            btnCreateLocation.setText("Edit location");
            editTextBarcode.setText(myLocationBarcode);
            editTextCode.setText(myLocationCode);
            editTextName.setText(myLocationName);
        }

        // if checkbox camera checked from settings = show button scan
        if (blCameraScanner)
        {
            imageViewBarcode.setVisibility(View.VISIBLE);
        } else {
            imageViewBarcode.setVisibility(View.GONE);
        }

        imageViewBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // create barcode intent
                com.sharethatdata.digiresource.Scan.IntentIntegrator intentintegrator = new com.sharethatdata.digiresource.Scan.IntentIntegrator(CreateLocationActivity.this);
                intentintegrator.setRequestCode(REQUEST_SCAN_BARCODE);
                intentintegrator.initiateScan();
                //startActivityForResult(integrator, REQUEST_SCAN_BARCODE);
            }
        });

        btnCreateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if(editTextBarcode.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editTextBarcode.setError("Location barcode is required!");
                }

                if( editTextName.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextName.setError("Name is required!");
                }

                if( editTextCode.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editTextCode.setError("Code is required!");
                }


                if(failFlag == false){
                    resourceBarcode_text = editTextBarcode.getText().toString();
                    resourceName_text = editTextName.getText().toString();
                    resourceCode_text = editTextCode.getText().toString();

                    if(!edit.equals("")){
                        // Edit Resource
                        alertDialogEdit();
                    }else{
                        // Create resource
                        alertDialogCreate();
                    }

                }
            }
        });

    }

    private void alertDialogCreate(){
        new AlertDialog.Builder(CreateLocationActivity.this)
                .setTitle("Location")
                .setMessage("Do you want to create this location?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateLocation(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void alertDialogEdit(){
        new AlertDialog.Builder(CreateLocationActivity.this)
                .setTitle("Location")
                .setMessage("Do you want to edit this location?")
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CreateLocation(companyId).execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // SCAN
        IntentResult scanResult = com.sharethatdata.digiresource.Scan.IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null && requestCode == REQUEST_SCAN_BARCODE ) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditTextBarcode = (EditText) findViewById(R.id.editText_barcode);
                EditTextBarcode.setText(UPCScanned);
                EditText EditTextCode = (EditText) findViewById(R.id.editText_code);
                EditTextCode.setText(UPCScanned);
            }
        }

    }

    private class CreateLocation extends AsyncTask<String, String, Boolean> {

        public String company_id;

        public CreateLocation(String company_id){
            this.company_id = company_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            int locationID;
            boolean suc = false;

                // call web method
                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

                if(!edit.equals("")){
                    // Edit Resource
                    suc = MyProvider.editLocation(myLocationID, resourceBarcode_text, resourceName_text, resourceCode_text, company_id);
                }else{
                    // Create Resource
                    locationID = MyProvider.createLocation(resourceBarcode_text, resourceName_text, resourceCode_text, company_id);

                    if(locationID != 0){
                        suc = true;
                    }
                }


            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            progressDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(CreateLocationActivity.this, "Was not created the location", Toast.LENGTH_SHORT).show();
                    }else{

                            Globals.setValue("refresh","refresh");

                        Toast.makeText(CreateLocationActivity.this, "The location was created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            });
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateLocationActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

}
