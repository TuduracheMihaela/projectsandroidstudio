package com.sharethatdata.digiresource;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by gotic_000 on 1/17/2018.
 */

public class SettingsActivity extends PreferenceActivity {

    static final String PREFS_NAME = "defaults";

    TimeUtils MyTimeUtils = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SettingsActivity.this, SettingsActivity.this);

        // Display the fragment as the main content.
        if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();
    }

    public static class PrefFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getPreferenceManager().setSharedPreferencesName(PREFS_NAME);
            addPreferencesFromResource(R.xml.settings);
        }
    }
}
