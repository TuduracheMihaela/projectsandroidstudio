package com.sharethatdata.digiresource.MyResources;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiresource.Adapter.SpinnerAdapter;
import com.sharethatdata.digiresource.MyGlobals;
import com.sharethatdata.digiresource.MyResourcesActivity;
import com.sharethatdata.digiresource.R;
import com.sharethatdata.digiresource.ResourceDetailActivity;
import com.sharethatdata.digiresource.TimeUtils;
import com.sharethatdata.snowcloud_webservice.WSDataProvider;
import com.sharethatdata.snowcloud_webservice.datamodel.cContact;
import com.sharethatdata.snowcloud_webservice.datamodel.cReservations;
import com.sharethatdata.snowcloud_webservice.datamodel.cResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.fragment.app.Fragment;

/**
 * Created by gotic_000 on 3/6/2018.
 */

public class ResourcesTaken extends Fragment {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    Context context;
    View view;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> resourceReservationList;
    ArrayList<HashMap<String, String>> userList; // getContacts
    List<String> userkeyArray =  new ArrayList<String>();
    //List<String> userNameArray =  new ArrayList<String>();

    private String myPass = "";
    private String myUser = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";
    String companyId = "";

    Spinner sUsers;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_resources_taken, container, false);

        context = container.getContext();

        SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
        companyId = sharedPrefs.getString("prefCompanyId", "1");

        MyTimeUtils = new TimeUtils();
        //MyTimeUtils.setOrientation(MyResourcesActivity.this, MyResourcesActivity.this);

        Globals = ((MyGlobals) getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // name of user logged

        mySelectedUser = myUser;

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        listView = (ListView) view.findViewById(R.id.listView);
        sUsers = (Spinner) view.findViewById(R.id.spinnerUser);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemID = resourceReservationList.get(position).get("resource_id");
                String item1 = resourceReservationList.get(position).get("resource_name");

                Intent intent = new Intent(context, ResourceDetailActivity.class);
                intent.putExtra("action","taken");
                intent.putExtra("idResource",itemID);
                startActivity(intent);

                Toast.makeText(context, "Resource: " + itemID + " " + item1, Toast.LENGTH_SHORT).show();
            }
        });

        //new LoadReservations("", myUser, companyId).execute();
        new LoadUserListSpinner(companyId).execute();

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d("TAG", "onResume taken resources");
        String refresh = Globals.getValue("refresh");
        if(refresh.equals("refresh")){
            new LoadReservations(mySelectedUser, companyId).execute();
            Globals.setValue("refresh","");
        }
    }

    public void myRefresh(){
        Log.d("TAG", "onResume taken");
        new LoadReservations(mySelectedUser, companyId).execute();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadUserListSpinner extends AsyncTask<String, String, String> {

        String company_id;

        public LoadUserListSpinner(String company_id){
            this.company_id = company_id;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getActivity().getApplicationContext());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            userList = new ArrayList<HashMap<String, String>>();

            List<cContact> myContact = MyProvider.getUsers(myUserID, company_id);
            for(cContact entry : myContact)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                //map.put("username", entry.username);
                map.put("name", entry.name);

                // adding HashList to ArrayList
                userList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (getActivity().isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    String isManager = Globals.getValue("manager");
                    if (isManager == "yes")
                    {
                        List<String> userArray =  new ArrayList<String>();
                        userArray.add(0, getString(R.string.select_user));
                        userkeyArray.add(0,"");
                        //userNameArray.add(0,"");

                        if(userList.size()>0){
                            //userArray.add(0, getString(R.string.select_user));
                            //userkeyArray.add(0, getString(R.string.select_user));
                        }else{
                            userArray.add(0, getString(R.string.select_no_user));
                            userkeyArray.add(0, getString(R.string.select_no_user));
                        }

                        for (HashMap<String, String> map : userList)
                            for (Map.Entry<String, String> entry : map.entrySet())
                            {
                                if (entry.getKey() == "id") userkeyArray.add(entry.getValue());
                                //if (entry.getKey() == "username") userNameArray.add(entry.getValue());
                                if (entry.getKey() == "name") userArray.add(entry.getValue());
                            }

                        //ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyRoutesActivity.this, android.R.layout.simple_spinner_item, userArray);
                        //user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        RelativeLayout spinnerLayout = (RelativeLayout) view.findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.VISIBLE);

                        SpinnerAdapter user_adapter = new SpinnerAdapter(
                                context,
                                R.layout.spinner_adapter,
                                userArray
                        );
                        sUsers.setAdapter(user_adapter);

                        if(!myUserID.equals("")){
                            int indexUser = userkeyArray.indexOf(myUserID);
                            sUsers.setSelection(indexUser);
                        }

                        sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String mIndex = sUsers.getSelectedItem().toString();

                                if(mIndex.contains(getString(R.string.select_user))){
                                    // do nothing

                                }else{

                                    int myIndex = sUsers.getSelectedItemPosition();
                                    String userid = userkeyArray.get(myIndex);
                                    mySelectedUser = userid;

                                    new LoadReservations(mySelectedUser, companyId).execute();

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // auto select
                        /*int selectUserIndex = userNameArray.indexOf(myUser);
                        sUsers.setSelection(selectUserIndex);
                        mySelectedUser = String.valueOf(selectUserIndex);*/

                    }else{
                        RelativeLayout spinnerLayout = (RelativeLayout) view.findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.GONE);

                        /*int selectUserIndex = userNameArray.indexOf(myUser);
                        mySelectedUser = String.valueOf(selectUserIndex);*/

                        new LoadReservations(mySelectedUser, companyId).execute();
                    }

                }
            });
            hideProgressDialog();
        }
    }

    class LoadReservations extends AsyncTask<String, String, String> {

        public String user_for;
        public String company_id;

        public LoadReservations(String user_for, String company_id){
            this.user_for = user_for;
            this.company_id = company_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getActivity().getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            resourceReservationList = new ArrayList<HashMap<String, String>>();

            List<cResource> myReservation = MyProvider.getMyResourcesTaken(user_for, company_id);
            for(cResource entry : myReservation)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("resource_id",  entry.resourceid);
                map.put("resource_name",  entry.resource);
                map.put("startdate",  entry.datetime);
                map.put("enddate",  entry.return_date);
                map.put("user",  entry.user);
                map.put("amount",  Long.toString(entry.amount));

                // adding HashList to ArrayList
                resourceReservationList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (getActivity().isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(resourceReservationList.size() > 0){
                        ListAdapter adapter = null;
                        adapter = new SimpleAdapter(context, resourceReservationList,
                                R.layout.activity_my_reservations_taken_items_listview,
                                new String[]{"resource_id", "startdate", "enddate", "resource_name", "user", "amount"},
                                new int[]{R.id.reservationid, R.id.startdate, R.id.enddate, R.id.resource_name, R.id.user, R.id.amount});
                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);

                        TextView textViewNoReservation = (TextView) view.findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.GONE);
                    }else{
                        TextView textViewNoReservation = (TextView) view.findViewById(R.id.textViewNoReservation);
                        textViewNoReservation.setVisibility(View.VISIBLE);

                        listView.setVisibility(View.GONE);
                    }

                }
            });

            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }
}
