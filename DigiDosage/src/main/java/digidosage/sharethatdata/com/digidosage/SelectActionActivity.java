package digidosage.sharethatdata.com.digidosage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

/**
 * Created by mihu__000 on 1/24/2017.
 */

public class SelectActionActivity extends Activity {

    TimeUtils MyTimeUtils = null;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_action);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SelectActionActivity.this, SelectActionActivity.this);

        Button btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
        Button btnSubmit2 = (Button) findViewById(R.id.btnSubmit2);

        btnSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectActionActivity.this, SelectCabinetActivity.class);
                startActivity(intent);

            }
        });

        btnSubmit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectActionActivity.this, ScriptListActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.select_action, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent);
                return true;
            case R.id.action_history:
             //   Intent intent1 = new Intent(this, HistoryActivity.class); startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
