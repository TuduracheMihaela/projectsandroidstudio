package digidosage.sharethatdata.com.digidosage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

/**
 * Created by mihu__000 on 1/24/2017.
 */

public class ScriptSelectActivity extends Activity {

    TimeUtils MyTimeUtils = null;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_script_select);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ScriptSelectActivity.this, ScriptSelectActivity.this);





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.select_action, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent);
                return true;
            case R.id.action_history:
             //   Intent intent1 = new Intent(this, HistoryActivity.class); startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
