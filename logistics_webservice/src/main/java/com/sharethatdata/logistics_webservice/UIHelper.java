package com.sharethatdata.logistics_webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.TextView;

public class UIHelper {

	public UIHelper()
	{
		
	
	}
	
	public void ShowDialog(Context myActivity, String title, String message)
	{		  
        // Show The Dialog 
        AlertDialog dialog = new AlertDialog.Builder(myActivity).create();
        dialog.setTitle(title);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setMessage(message);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
            	
        		public void onClick(DialogInterface dialog, int which)
            	{                          
            		dialog.dismiss();
            		return;
            	}   
       	});
        Activity activity = (Activity) myActivity;
        if (!activity.isFinishing()) {
            dialog.show();
        }
	}
	
	// set time from mysql datetime
		public void SetTime(View v, String mysqldate)
		{
			TextView TimeView = (TextView) v;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    int mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    int mStartMinute = c.get(Calendar.MINUTE);
						
			    TimeView.setText(new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute)));
					
			} catch (ParseException e) {
			    	
			}

	    }
	  
		// set time from mysql datetime
		public void SetDate(View v, String mysqldate)
		{
			TextView DateView = (TextView) v;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    int mStartYear = c.get(Calendar.YEAR);
				int mStartMonth = c.get(Calendar.MONTH);
				int mStartDay = c.get(Calendar.DAY_OF_MONTH);
						
				DateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
	                    .append(pad(mStartMonth + 1)).append("-")
	                    .append(pad(mStartYear)));
			   				
			} catch (ParseException e) {
			    	
			}

	    }
		
		public String SetFormattedDate(String mysqldate)
		{
			String outputstr = "";
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				
			try {
				Date myDate = dateFormat.parse(mysqldate);
			    
			    Calendar c = Calendar.getInstance();
			    c.setTime(myDate);
			    int mStartYear = c.get(Calendar.YEAR);
				int mStartMonth = c.get(Calendar.MONTH);
				int mStartDay = c.get(Calendar.DAY_OF_MONTH);
			    int mStartHour = c.get(Calendar.HOUR_OF_DAY);
			    int mStartMinute = c.get(Calendar.MINUTE);

				outputstr = pad(mStartDay) + "-" + pad(mStartMonth + 1) + "-" + pad(mStartYear) + " " + pad(mStartHour) + ":"  + pad(mStartMinute);
			   				
				return outputstr;
			} catch (ParseException e) {
			    	
				return "";
			}

	    }
		
		
		public Object pad(int mMinute2) {
	        if (mMinute2 >= 10)
	            return String.valueOf(mMinute2);
	        else
	            return "0" + String.valueOf(mMinute2);
	    }
		
}
