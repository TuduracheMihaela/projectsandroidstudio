package com.sharethatdata.logistics_webservice;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sharethatdata.jsonlibrary.JSONParser;
import com.sharethatdata.jsonlibrary.JSONParserForPassword;
import com.sharethatdata.logistics_webservice.datamodel.cAddress;
import com.sharethatdata.logistics_webservice.datamodel.cCollections;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.logistics_webservice.datamodel.cDefaultStop;
import com.sharethatdata.logistics_webservice.datamodel.cDeviceSetting;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackage;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPackagesbyDeliveryId;
import com.sharethatdata.logistics_webservice.datamodel.cPharmacyAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRoute;
import com.sharethatdata.logistics_webservice.datamodel.cRouteAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRouteReport;
import com.sharethatdata.logistics_webservice.datamodel.cRoutes;
import com.sharethatdata.logistics_webservice.datamodel.cPackageImage;
import com.sharethatdata.logistics_webservice.datamodel.cMessage;
import com.sharethatdata.logistics_webservice.datamodel.cUserCurrectLocation;
import com.sharethatdata.logistics_webservice.datamodel.cUserImage;
import com.sharethatdata.logistics_webservice.datamodel.cUserStats;
import com.sharethatdata.logistics_webservice.datamodel.cWebService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

public class WSDataProvider {
	
	private Context context;
	private SharedPreferences sharedPrefs;

	JSONParser jsonParser = new JSONParser();
	JSONParserForPassword jsonParserForPassword = new JSONParserForPassword();

	private String url_login = "login.php";
	private String url_secure_login = "secure_login.php";
	private String url_get_item_new = "get_item.php";
    private String url_get_items_new = "get_items.php";
    private String url_create_item = "create_item.php";
    private String url_update_item = "update_item.php";
    private String url_delete_item = "delete_item.php";
	private String create_script_url = "addScript.php";
	private String request_reset_password = "requestReset.php";
	private String process_reset_password = "processResetRequest.php";
	private String register_device = "registerDevice.php";

	private String register_device_cerberus = "register_device.php";
	private String get_webservices_cerberus = "webservices.php";
	private String get_device_setting_cerberus = "get_device_setting.php";
	private String update_device_setting_cerberus = "update_device_setting.php";


	private static final String TAG_SUCCESS = "ws_success";
	private static final String TAG_ERROR = "ws_error";
	private static final String TAG_MESSAGE = "ws_error_message";
	private static final String TAG_UPDATE = "update";

    private static final String CONNECTION_ERROR = "Web service not found, please check WIFI/Network connection.";

	//private String webservice_url_newton = "https://newtonrp-ws.sharethatdata.com";
	private String webservice_url_newton = "https://logistics-ws.sharethatdata.com";
	//private String webservice_url = "https://www.sharethatdata.com/devlogistics-ws";
	//private String webservice_url = "https://devlogistics-ws.sharethatdata.com";
	private String webservice_url = "no web service";
	private String myUser = "";
	private String myPass = "";
	private String myApp = "";
    
    private String ip = "";
    private String android_id = "";
    private String appname = "";

	public String last_error_nr = "0";
    public String last_error = "";
    public String update = "";
    public boolean json_connected = false;

	public WSDataProvider(Context context)
	{
    	this.context = context;
	}
	
	public WSDataProvider(String new_user, String new_pass)
	{
		myUser = new_user;	
		myPass = new_pass;

		String currentSESSION = WSSession.getInstance().getString();
		if(!currentSESSION.equals("")){

			myPass = WSSession.getInstance().getPassword();
			myApp = WSSession.getInstance().getAppName();
			android_id = WSSession.getInstance().getDevice();
			ip = WSSession.getInstance().getIP();
			android_id = WSSession.getInstance().getDevice();
		}
	}
	
	public void SetWebServiceUrl(String new_url)
	{
		if (new_url != ""){
			webservice_url = new_url;
		}
		
		System.out.println("URL= " + webservice_url);
	}

	public String getWebServiceUrl()
	{
		return webservice_url;

	}

	public void SetDeviceIdentification(String new_ip, String new_android_id, String new_appname)
	{
		ip = new_ip;
		appname = new_appname;
		android_id = new_android_id;
	}
	
	public static String MD5_Hash(String input) {
		
	    try {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        byte[] messageDigest = md.digest(input.getBytes());
	        BigInteger number = new BigInteger(1, messageDigest);
	        String md5 = number.toString(16);

	        while (md5.length() < 32)
	            md5 = "0" + md5;

	        return md5;
	    } catch (NoSuchAlgorithmException e) {
	        Log.e("MD5", e.getLocalizedMessage());
	        return null;
	    }
	}
	
	public boolean LogEvent(String event) 
	{
		 // no more logging from client
		String user = myUser;
		String datetime = "";
		
		try
 	    {
 	    	 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
 	    } catch (Exception e)
 	    { 	    
 	    }
		
		// get values from device
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user", user));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("date", datetime));
        params.add(new BasicNameValuePair("ip", ip));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
         
            return true;

	}

	public boolean CheckSecurekLogin(String login, String password, String app, String version, String access_key)
    {
		// clean variables
		last_error = "";
		json_connected = true;
		String myPassword = MD5_Hash(password);
		if (app.equals("DigiOrder")) myPassword = password;
		
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("version", version));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));
        params.add(new BasicNameValuePair("access_key", access_key));

        myApp = app;
        myUser = login;
        myPass = myPassword;
	      
        if(context != null){
        // save user and password for notifications 
	      /*SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
	   	     editor.putString("USER", String.valueOf(myUser));
	   	     editor.putString("PASS", String.valueOf(myPass));
	   	     editor.putString("URL", String.valueOf(webservice_url));
	   	     editor.commit();*/
	   	     
	   	  System.out.println("CheckLogin myUser= "+ String.valueOf(myUser));
	   	  System.out.println("CheckLogin myPass= "+ String.valueOf(myPass));
	   	  System.out.println("CheckLogin webservice_url= "+ String.valueOf(webservice_url));
        }else{
          System.out.println("context null CheckLogin ------------------------------------- ");
          System.out.println("CheckLogin myUser= "+ String.valueOf(myUser));
  	   	  System.out.println("CheckLogin myPass= "+ String.valueOf(myPass));
  	   	  System.out.println("CheckLogin webservice_url= "+ String.valueOf(webservice_url));
        }
        
        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_secure_login, "GET", params);

     	
	   	 if (json != null) 
	   	 {    		 
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);
	
	            if (success == 1)
	            {
	            	String token = json.getString("ws_token");
	            	WSSession.getInstance().setString(token);
					update = json.getString(TAG_UPDATE);

	            	return true;
	            } else {
					last_error_nr = json.getString(TAG_ERROR);
	            	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
	            	return false;
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            
	            last_error = CONNECTION_ERROR;
	   		 	json_connected = false;
	            return false;
	        }
	   	 } else {
	   		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
	   		 return false;
	   	 }
    }

	public boolean CheckLogin(String login, String password, String app)
    {
		// clean variables
		last_error = "";
		json_connected = true;
		String myPassword = MD5_Hash(password);
		if (app.equals("DigiOrder")) myPassword = password;

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("user", login));
        params.add(new BasicNameValuePair("password", myPassword));
        params.add(new BasicNameValuePair("app", app));
        params.add(new BasicNameValuePair("module", appname));
        params.add(new BasicNameValuePair("device", android_id));

        myApp = app;
        myUser = login;
        myPass = myPassword;

        if(context != null){
        // save user and password for notifications
	      SharedPreferences.Editor editor = context.getSharedPreferences("PREF_USER", 0).edit();
	   	     editor.putString("USER", String.valueOf(myUser));
	   	     editor.putString("PASS", String.valueOf(myPass));
	   	     editor.putString("URL", String.valueOf(webservice_url));
	   	     editor.commit();

	   	  System.out.println("CheckLogin myUser= "+ String.valueOf(myUser));
	   	  System.out.println("CheckLogin myPass= "+ String.valueOf(myPass));
	   	  System.out.println("CheckLogin webservice_url= "+ String.valueOf(webservice_url));
        }else{
          System.out.println("context null CheckLogin ------------------------------------- ");
          System.out.println("CheckLogin myUser= "+ String.valueOf(myUser));
  	   	  System.out.println("CheckLogin myPass= "+ String.valueOf(myPass));
  	   	  System.out.println("CheckLogin webservice_url= "+ String.valueOf(webservice_url));
        }

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_login, "GET", params);


	   	 if (json != null)
	   	 {
	        // check for success tag
	        try {
	            int success = json.getInt(TAG_SUCCESS);

	            if (success == 1)
	            {
	            	String token = json.getString("ws_token");
	            	WSSession.getInstance().setString(token);

	            	return true;
	            } else {
					last_error_nr = json.getString(TAG_ERROR);
	            	String error = json.getString(TAG_MESSAGE);
		        	last_error = error;
	            	return false;
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();

	            last_error = CONNECTION_ERROR;
	   		 	json_connected = false;
	            return false;
	        }
	   	 } else {
	   		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
	   		 return false;
	   	 }
    }

	// General Method for calling new web service
	// gets a list of json items in array
	// when something goes wrong, an empty array is returned
	private JSONArray CallWebService(List<NameValuePair> new_params, String url, boolean doPost)
	{
		// clean variables
		last_error = "";
		json_connected = true;
			
		String currentSESSION = WSSession.getInstance().getString();
		
		 // Building header Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(context != null){
            if(myUser.contains("")){
          	SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
  	  		  String user = prefs.getString("USER", "");
  	  		  String pass = prefs.getString("PASS", "");
  	          System.out.println("user= " + user);
  	          System.out.println("pass= " + pass);
  	          
  	          myUser = user;
  	          myPass = pass;
            }
          }
        
        
		params.add(new BasicNameValuePair("user", myUser));
        params.add(new BasicNameValuePair("token", currentSESSION));
        params.add(new BasicNameValuePair("filtered", "true"));
        for (NameValuePair p : new_params) {
        	params.add(p);
        } 
       
    	// getting JSON Object
    	// Note that create product url accepts POST method
        String post_str = "GET"; 
        if (doPost)
        {
        	post_str = "POST";
        } 
        
        JSONObject json = jsonParser.makeHttpRequest(url, post_str, params);
     	JSONArray items = new JSONArray(); 
    	 if (json != null) 
    	 {    		 
			 try {

				 items = json.optJSONArray("items");
			    	
	    		// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());
			
				int success = json.getInt(TAG_SUCCESS);
	
		        if (success == 1)
		        {
		         	if (items == null){ 
		         			items = new JSONArray();
		         		}else{
		         			System.out.println("items= " + items);
		         		}
		        } else {
		        	// record error
		        	items = new JSONArray(); 
		        	String error = json.getString(TAG_MESSAGE);
		        	String error_nr = json.getString(TAG_ERROR);
					if (error.equals("")){
                        last_error = "unknown error";
                        last_error_nr = "0";
                    } else {
                        last_error = error;
                        last_error_nr = error_nr;
                    }
		        	
		        	/*if(error.contains("Security token incorrect.")){
		        		System.out.println("TOKEN EXPIRED");
		        		System.out.println("TOKEN EXPIRED myUser: " + myUser);
		        		System.out.println("TOKEN EXPIRED myPass: " + myPass);
		        		
		        		boolean bool = CheckLogin(myUser, myPass, myApp);
		        		
		        		System.out.println("bool: " + bool);
		        		
		        		if(bool){
		        			JSONArray items_again = CallWebService(new_params, url, false);
		        			items = items_again;
		        			
		        		}
		        	}*/
		        	
		        }
		     } catch (JSONException e) {
		         e.printStackTrace();
		        
		     }
    	            
    	 } else {
    		 
    		 last_error = CONNECTION_ERROR;
    		 json_connected = false;
    	 
    	 }
    	 return items;
	}

	private JSONArray CallWebServiceForPassword(List<NameValuePair> new_params, String url, boolean doPost)
	{
		// clean variables
		last_error = "";
		json_connected = true;

		String currentSESSION = WSSession.getInstance().getString();

		// Building header Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		if(context != null){
			if(myUser.contains("")){
				SharedPreferences prefs = context.getSharedPreferences("PREF_USER", Context.MODE_PRIVATE);
				String user = prefs.getString("USER", "");
				String pass = prefs.getString("PASS", "");

				myUser = user;
				myPass = pass;
			}
		}


		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("filtered", "true"));
		for (NameValuePair p : new_params) {
			params.add(p);
		}

		// getting JSON Object
		// Note that create product url accepts POST method
		String post_str = "GET";
		if (doPost)
		{
			post_str = "POST";
		}

		JSONObject json = jsonParserForPassword.makeHttpRequest(url, post_str, params);
		JSONArray items = new JSONArray();
		if (json != null)
		{
			try {

				items = json.optJSONArray("items");

				// Check your log cat for JSON reponse
				Log.d("All items: ", json.toString());

				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					if (items == null){
						items = new JSONArray();

							String error = json.getString(TAG_MESSAGE);
							items = new JSONArray();
							items.put(error);

					}else{
						System.out.println("items= " + items);
					}
				} else {
					// record error
					items = new JSONArray();
					String error = json.getString(TAG_MESSAGE);
					last_error = error;

					if(error.contains("Security token incorrect.")){
						System.out.println("TOKEN EXPIRED");
						System.out.println("TOKEN EXPIRED myUser= " + myUser);
						System.out.println("TOKEN EXPIRED myPass= " + myPass);

						boolean bool = CheckLogin(myUser, myPass, myApp);

						System.out.println("bool= " + bool);

						if(bool){
							JSONArray items_again = CallWebService(new_params, url, false);
							items = items_again;

						}
					}

				}
			} catch (JSONException e) {
				e.printStackTrace();

			}

		} else {

			last_error = CONNECTION_ERROR;
			json_connected = false;

		}
		return items;
	}

	// get user roles
	public ArrayList<String> getUserRoles(String id, String pharmacy_id)
	{
		 // Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user_role"));
		params.add(new BasicNameValuePair("role_user", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		ArrayList<String> roles = new ArrayList<String>();

		try {
			if (items.length() > 0)
			{
				 for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					String str_name = c.getString("name");
					roles.add(str_name);
				 }
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return roles;
	}
		
		
	// get addresses
	public List<cAddress> getAddresses(String id, String pharmacy_id)
	{
		 // Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "address"));
		params.add(new BasicNameValuePair("orderid", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cAddress> returnList = new ArrayList<cAddress>();

		try {
			if (items.length() > 0)
			{
				 for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cAddress address = new cAddress();

					address.id = c.getString("id");
					address.customer_surname = c.getString("customer_surname");
					address.customer_lastname = c.getString("customer_lastname");
					address.customer_birthdate = c.getString("customer_birthdate");
					address.customer_sex = c.getString("customer_sex");
					address.customer_street = c.getString("customer_street");
					address.customer_house = c.getString("customer_house");
					address.customer_zipcode = c.getString("customer_zipcode");
					address.customer_city = c.getString("customer_city");
					address.customer_country = c.getString("customer_country");
					address.longitude = c.getString("longitude");
					address.latitude = c.getString("latitude");

					returnList.add(address);
				 }
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	public List<cContact> getMyUsers(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "myusers"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cContact> returnList = new ArrayList<cContact>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cContact myContact = new cContact();

					myContact.id = c.getString("id");
					myContact.username = c.getString("username");
					myContact.name = c.getString("name");

					returnList.add(myContact);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public List<cContact> getPharmacyUsers(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "users"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cContact> returnList = new ArrayList<cContact>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cContact myContact = new cContact();

					myContact.id = c.getString("id");
					myContact.name = c.getString("name");
					myContact.username = c.getString("username");

					returnList.add(myContact);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}


	public List<cPackages> getPackagesByBarcode(String barcode, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "collect_package"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("barcode", barcode));


		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cPackages> returnList = new ArrayList<cPackages>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cPackages myPackage = new cPackages();

					myPackage.addressid = c.getString("id");
					myPackage.delivery_id = c.getString("delivery_id");
					myPackage.packageid = c.getString("packageid");
					myPackage.route = c.getString("route");
					myPackage.location = c.getString("location");
					myPackage.package_barcode = c.getString("barcode");
					myPackage.package_status = c.getString("status");
					myPackage.package_status_description = c.getString("status_description");
					myPackage.customer_script = c.getString("customer_script");
					myPackage.customer_name = c.getString("customer_name");
					myPackage.customer_address =
							c.getString("customer_street") + " " +
									c.getString("customer_house") + ", " +
									c.getString("customer_zipcode") + ", " +
									c.getString("customer_city") + ", " +
									c.getString("customer_country");
					myPackage.customer_street = c.getString("customer_street");
					myPackage.customer_house = c.getString("customer_house");
					myPackage.customer_zipcode = c.getString("customer_zipcode");
					myPackage.customer_city = c.getString("customer_city");
					myPackage.customer_country = c.getString("customer_country");


					returnList.add(myPackage);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public List<cPackages> getPackages(String collection_id, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "collect_package"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		if(!collection_id.equals("")){
			params.add(new BasicNameValuePair("collection_id", collection_id));
		}

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cPackages> returnList = new ArrayList<cPackages>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cPackages myPackage = new cPackages();

					myPackage.packageid = c.getString("packageid");
					myPackage.addressid = c.getString("id");
					myPackage.delivery_id = c.getString("delivery_id");
					myPackage.package_barcode = c.getString("barcode");
					myPackage.route = c.getString("route");
					myPackage.location = c.getString("location");
					myPackage.status_id = c.getString("status");
					myPackage.package_status_description = c.getString("status_description");
					myPackage.customer_script = c.getString("customer_script");
					myPackage.customer_address =
							c.getString("customer_street") + " " +
							c.getString("customer_house") + ", " +
							c.getString("customer_zipcode") + ", " +
							c.getString("customer_city") + ", " +
							c.getString("customer_country");
					myPackage.customer_street = c.getString("customer_street");
					myPackage.customer_house = c.getString("customer_house");
					myPackage.customer_zipcode = c.getString("customer_zipcode");
					myPackage.customer_city = c.getString("customer_city");
					myPackage.customer_country = c.getString("customer_country");


					returnList.add(myPackage);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public List<cPackages> getAllPackages(String routeid, String addressid, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package"));
		params.add(new BasicNameValuePair("route", routeid));
		if(!addressid.equals("")) params.add(new BasicNameValuePair("addressid", addressid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cPackages> returnList = new ArrayList<cPackages>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cPackages myPackage = new cPackages();

					myPackage.packageid = c.getString("id");
					myPackage.delivery_id = c.getString("delivery_id");
					myPackage.scriptid = c.getString("scriptid");
					myPackage.package_barcode = c.getString("packageid");
					myPackage.addressid = c.getString("addressid");
					myPackage.status_id = c.getString("status_id");
					myPackage.package_status = c.getString("status_id");
					myPackage.package_status_description = c.getString("status_description");

					returnList.add(myPackage);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	// packages route
	public List<cPackageRoute> getPackagesOfRoute(String routeid, String addressid, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package"));
		params.add(new BasicNameValuePair("route", routeid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		if(!addressid.equals("")){
			params.add(new BasicNameValuePair("addressid", addressid));
		}

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cPackageRoute> returnList = new ArrayList<cPackageRoute>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {

					JSONObject c = items.getJSONObject(i);

					cPackageRoute myPackage = new cPackageRoute();

					myPackage.id = c.getString("id");
					myPackage.scriptid = c.getString("scriptid");
					myPackage.package_barcode = c.getString("packageid");
					myPackage.addressid = c.getString("addressid");
					myPackage.package_status = c.getString("status_id");
					myPackage.status_description = c.getString("status_description");

					returnList.add(myPackage);
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// packages for deliver_remark, contact_patient, signature_person
	public List<cPackagesbyDeliveryId> getPackagesbyDeliveryId(String delivery_id, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package_status"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("delivery_id", delivery_id));


		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		List<cPackagesbyDeliveryId> returnList = new ArrayList<cPackagesbyDeliveryId>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {

					JSONObject c = items.getJSONObject(i);

					cPackagesbyDeliveryId myPackage = new cPackagesbyDeliveryId();

					myPackage.datetime = c.getString("datetime");
					myPackage.script_id = c.getString("script_id");
					myPackage.delivery_id = c.getString("delivery_id");
					myPackage.user = c.getString("user");
					myPackage.routeid = c.getString("routeid");
					myPackage.route = c.getString("route");
					myPackage.start_time = c.getString("start_time");
					myPackage.end_time = c.getString("end_time");
					myPackage.time_scheduled = c.getString("time_scheduled");
					myPackage.distance_scheduled = c.getString("distance_scheduled");
					myPackage.sequence_scheduled = c.getString("sequence_scheduled");
					myPackage.time_driven = c.getString("time_driven");
					myPackage.distance_driven = c.getString("distance_driven");
					myPackage.signature = c.getString("signature");
					myPackage.signature_person = c.getString("signature_person");
					myPackage.patient_remark = c.getString("patient_remark");
					myPackage.deliver_remark = c.getString("deliver_remark");
					myPackage.contact_patient = c.getString("contact_patient");
					myPackage.status = c.getString("status");
					myPackage.status_description = c.getString("status_description");
					myPackage.processed = c.getString("processed");

					returnList.add(myPackage);
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get routes
	public List<cRoutes> getRoutes(String location, String pharmacy_id, String startDate, String endDate)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route"));
		params.add(new BasicNameValuePair("location", location));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		if(!(startDate.equals(""))){
			params.add(new BasicNameValuePair("startdate", startDate));
			params.add(new BasicNameValuePair("enddate", endDate));
		}

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cRoutes> returnList = new ArrayList<cRoutes>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cRoutes route = new cRoutes();

					route.id = c.getString("id");
					route.datetime = c.getString("datetime");
					route.description = c.getString("description");
					route.algorithm = c.getString("algorithm");
					route.user = c.getString("user");
					route.location = c.getString("location");

					returnList.add(route);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get collections
	public List<cCollections> getCollections(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "collection"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cCollections> returnList = new ArrayList<cCollections>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cCollections collection = new cCollections();

					collection.id = c.getString("id");
					collection.collection = c.getString("collection");

					returnList.add(collection);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get route reports
	public List<cRouteReport> getRouteReports(String route, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_report"));
		params.add(new BasicNameValuePair("route", route));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cRouteReport> returnList = new ArrayList<cRouteReport>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cRouteReport report = new cRouteReport();

					report.id = c.getString("id");
					report.customer_script = c.getString("customer_script");
					report.customer_name = c.getString("customer_name");
					report.customer_birthdate = c.getString("customer_birthdate");
					report.customer_sex = c.getString("customer_sex");
					report.customer_street = c.getString("customer_street");
					report.customer_house = c.getString("customer_house");
					report.customer_zipcode = c.getString("customer_zipcode");
					report.customer_city = c.getString("customer_city");
					report.customer_country = c.getString("customer_country");
					report.longitude = c.getString("longitude");
					report.latitude = c.getString("latitude");
					report.sequence = c.getString("sequence");
					report.distance = c.getString("distance");
					if(report.distance.equals("") || report.distance.equals("null")){
						report.distance = "0";
					}
					report.real_time = c.getString("realtime");
					report.time = c.getString("time");
					if(report.real_time.equals("") || report.real_time.equals("null")) {

						if (report.time.equals("") || report.time.equals("null")) {
							report.time = "0";
						}
					} else {
						report.time = report.real_time;
					}
					report.datetime_start = c.getString("datetime_start");
					if(report.datetime_start.equals("") || report.datetime_start.equals("null")){
						report.datetime_start = "0000-00-00 00:00:00";
					}
					report.datetime_end = c.getString("datetime_end");
					if(report.datetime_end.equals("") || report.datetime_end.equals("null")){
						report.datetime_end = "0000-00-00 00:00:00";
					}

					report.user = c.getString("user");
					report.status = c.getString("status");
					report.barcode = c.getString("barcode");

					returnList.add(report);
				}
			}else{

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get my routes
	public List<cMyRoute> getMyRoutes(String myReportUserID, String pharmacy_id, String startDate, String endDate, String status)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "myroute"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		if (!myReportUserID.equals("")) params.add(new BasicNameValuePair("report_user", myReportUserID));
		if (!status.equals("")) params.add(new BasicNameValuePair("status", status));
		if(!(startDate.equals(""))) params.add(new BasicNameValuePair("startdate", startDate));
		if(!(endDate.equals(""))) params.add(new BasicNameValuePair("enddate", endDate));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cMyRoute> returnList = new ArrayList<cMyRoute>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cMyRoute route = new cMyRoute();

					route.id = c.getString("id");
					route.datetime = c.getString("datetime");
					route.description = c.getString("description");
					route.algorithm = c.getString("algorithm");
					route.user = c.getString("user");
					route.location = c.getString("location");
					route.status = c.getString("status");
					route.status_description = c.getString("status_description");

					returnList.add(route);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get user roles
	public List<cRouteAddress> getRouteAddress(String routeid, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_address"));
		params.add(new BasicNameValuePair("route", routeid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cRouteAddress> returnList = new ArrayList<cRouteAddress>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cRouteAddress address = new cRouteAddress();

					address.id = c.getString("id");
					address.customer_script = c.getString("customer_script");
					address.customer_name = c.getString("customer_name");
					address.customer_birthdate = c.getString("customer_birthdate");
					address.customer_sex = c.getString("customer_sex");
					address.customer_street = c.getString("customer_street");
					address.customer_house = c.getString("customer_house");
					address.customer_zipcode = c.getString("customer_zipcode");
					address.customer_city = c.getString("customer_city");
					address.customer_country = c.getString("customer_country");
					address.longitude = c.getString("longitude");
					address.latitude = c.getString("latitude");
					address.sequence = c.getString("sequence");
					address.distance = c.getString("distance");
					address.time = c.getString("time");
					address.status = c.getString("status");
					address.barcode = c.getString("barcode");

					returnList.add(address);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// get default stops
	public List<cDefaultStop> getDefaultStops(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "default_stop"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		List<cDefaultStop> returnList = new ArrayList<cDefaultStop>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					cDefaultStop address = new cDefaultStop();

					address.id = c.getString("id");
					address.name = c.getString("name");
					address.street = c.getString("street");
					address.house = c.getString("house");
					address.zipcode = c.getString("zipcode");
					address.city = c.getString("city");
					address.country = c.getString("country");
					address.longitude = c.getString("longitude");
					address.latitude = c.getString("latitude");

					returnList.add(address);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	// route detail
	public cRoute getRoute(String routeid, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route"));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cRoute myRoute = new cRoute();

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myRoute.id = c.getString("id");
				myRoute.status = c.getString("status");
				myRoute.description = c.getString("description");
				myRoute.algorithm = c.getString("algorithm");
				myRoute.user = c.getString("user");
				myRoute.location = c.getString("location");
				myRoute.description = c.getString("description");
				myRoute.return_distance = c.getString("return_distance");
				myRoute.return_time = c.getString("return_time");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myRoute;
	}

	// route report (last address - pharmacy)
	public cRoute getRouteReport(String routeid, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_report"));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cRoute myRoute = new cRoute();

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myRoute.id = c.getString("id");
				myRoute.datetime_start = c.getString("datetime_start");
				if(myRoute.datetime_start.equals("") || myRoute.datetime_start.equals("null")){
					myRoute.datetime_start = "0000-00-00 00:00:00";
				}
				myRoute.datetime_end = c.getString("datetime_end");
				if(myRoute.datetime_end.equals("") || myRoute.datetime_end.equals("null")){
					myRoute.datetime_end = "0000-00-00 00:00:00";
				}
				myRoute.user = c.getString("user");
				myRoute.return_distance = c.getString("return_distance");
				if(myRoute.return_distance.equals("") || myRoute.return_distance.equals("null")){
					myRoute.return_distance = "0";
				}
				myRoute.return_time = c.getString("return_time");
				if(myRoute.return_time.equals("") || myRoute.return_time.equals("null")){
					myRoute.return_time = "0";
				}
			}else{
				if(myRoute.datetime_start.equals("")){
					myRoute.datetime_start = "0000-00-00 00:00:00";
				}
				if(myRoute.datetime_end.equals("")){
					myRoute.datetime_end = "0000-00-00 00:00:00";
				}
				if(myRoute.return_distance.equals("")){
					myRoute.return_distance = "0";
				}
				if(myRoute.return_time.equals("")){
					myRoute.return_time = "0";
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myRoute;
	}

	// address detail
	public cAddress getAddress(String id, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "address"));
		params.add(new BasicNameValuePair("id", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cAddress myAddress = new cAddress();

		try {
			if (items.length() > 0)
			{

				JSONObject c = items.getJSONObject(0);

				myAddress.id = c.getString("id");
				myAddress.customer_surname = c.getString("customer_surname");
				myAddress.customer_lastname = c.getString("customer_lastname");
				myAddress.customer_birthdate = c.getString("customer_birthdate");
				myAddress.customer_sex = c.getString("customer_sex");
				myAddress.customer_street = c.getString("customer_street");
				myAddress.customer_house = c.getString("customer_house");
				myAddress.customer_zipcode = c.getString("customer_zipcode");
				myAddress.customer_city = c.getString("customer_city");
				myAddress.customer_country = c.getString("customer_country");
				myAddress.free_text = c.getString("free_text");
				myAddress.longitude = c.getString("longitude");
				myAddress.latitude = c.getString("latitude");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myAddress;
	}

	// pharmacy address
	public cPharmacyAddress getPharmacyAddress(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "pharmacy"));
		params.add(new BasicNameValuePair("id", pharmacy_id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cPharmacyAddress myPharmacyAddress = new cPharmacyAddress();

		try {
			if (items.length() > 0)
			{

				JSONObject c = items.getJSONObject(0);

				myPharmacyAddress.id = c.getString("id");
				myPharmacyAddress.name = c.getString("name");
				myPharmacyAddress.address1 = c.getString("address1");
				myPharmacyAddress.address2 = c.getString("address2");
				myPharmacyAddress.address_house = c.getString("address_house");
				myPharmacyAddress.address_zipcode = c.getString("address_zipcode");
				myPharmacyAddress.address_city = c.getString("address_city");
				myPharmacyAddress.address_country = c.getString("address_country");
				myPharmacyAddress.longitude = c.getString("longitude");
				myPharmacyAddress.latitude = c.getString("latitude");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myPharmacyAddress;
	}

	// package detail
	public cPackage getPackage(String id, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package"));
		params.add(new BasicNameValuePair("scriptid", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		// getting JSON Object
		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cPackage myPackage = new cPackage();

		try {
			if (items.length() > 0)
			{

				JSONObject c = items.getJSONObject(0);

				myPackage.scriptid = c.getString("scriptid");
				myPackage.package_barcode = c.getString("packageid");
				myPackage.route = c.getString("route");
				myPackage.package_status = c.getString("status");
				myPackage.status_description = c.getString("status_description");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myPackage;
	}

	// contact detail
	public cContact getContact(String username, String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user"));
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("userid", "0"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cContact myContact = new cContact();

		// check for success tag
		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myContact.id = c.getString("id");
				myContact.username = c.getString("username");
				myContact.surname = c.getString("surname");
				myContact.lastname = c.getString("lastname");
				myContact.login = c.getString("login");
				myContact.picture = c.getString("picture");
				myContact.image_id = c.getString("image_id");
				myContact.auth_level = c.getInt("auth_level");
				myContact.function = c.getString("function");
				myContact.phone = c.getString("phone");
				myContact.email = c.getString("email");
				myContact.status_id = c.getString("status");
				myContact.description = c.getString("description");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myContact;

	}

    // contact detail
    public cUserCurrectLocation getUserCurrentLocation(String userid, String pharmacy_id) {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "user_current_location"));
        params.add(new BasicNameValuePair("user_id", userid));
        params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

        // getting JSON Object
        JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

        cUserCurrectLocation myLocation = new cUserCurrectLocation();

        // check for success tag
        try {
            if (items.length() > 0) {
                JSONObject c = items.getJSONObject(0);

                myLocation.id = c.getString("id");
                myLocation.user_id = c.getString("user_id");
                myLocation.location_id = c.getString("location_id");
                myLocation.location_name = c.getString("location_name");
                myLocation.date_time = c.getString("date_time");
                myLocation.latitude = c.getString("latitude");
                myLocation.longitude = c.getString("longitude");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return myLocation;

    }

    public boolean create_report(String date, String starttime, String name, String email, String project, String description, String image_id, String pharmacy_id)
	 {
		// Building Parameters
	   List<NameValuePair> params = new ArrayList<NameValuePair>();
	   params.add(new BasicNameValuePair("object", "report"));

	   String datetime = "";

		try
		{
			 Date d = new Date();
			 datetime = new SimpleDateFormat("yyyy-MM-dd").format(d);
		} catch (Exception e)
		{
		}

		System.out.println("datetime= " + datetime);

	   params.add(new BasicNameValuePair("date", datetime));
	   params.add(new BasicNameValuePair("starttime", datetime));
	   params.add(new BasicNameValuePair("name", name));
	   params.add(new BasicNameValuePair("email", email));
	   params.add(new BasicNameValuePair("project", project));
		 if(project.equals("HomeDelivery")) params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
	   params.add(new BasicNameValuePair("description", description));
	   params.add(new BasicNameValuePair("image_id", image_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	 }

	public boolean createRouteReport(String pharmacy_id, String routeid, String addressid)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_report"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("addressid", addressid));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public int createRoute(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();
		params.add(new BasicNameValuePair("object", "route"));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

		int routeID = 0;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					routeID = json.getInt("inserted_id");
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return routeID;
	}

	public boolean addPackageToRoute(String pharmacy_id, String delivery_id, String route_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_package"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("delivery_id", delivery_id));
		params.add(new BasicNameValuePair("route", route_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean addStopToRoute(String pharmacy_id, String stop_id, String route_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_stop"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("stop_id", stop_id));
		params.add(new BasicNameValuePair("route", route_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateRouteReport(String pharmacy_id, String routeid, String addressid)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_report"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("addressid", addressid));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updatePackageStatus(String pharmacy_id, String delivery_id, String statusid, Double latitude, Double longitude)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		//params.add(new BasicNameValuePair("packageid", packageid));
		params.add(new BasicNameValuePair("delivery_id", delivery_id));
		params.add(new BasicNameValuePair("status", statusid));
		params.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
		params.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateRouteStatus(String pharmacy_id, String routeid, String statusid)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_status"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("status", statusid));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateRouteAddress(String pharmacy_id, String routeid, String addressid, String sequence )
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "route_address"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("routeid", routeid));
		params.add(new BasicNameValuePair("addressid", addressid));
		params.add(new BasicNameValuePair("sequence", sequence));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateSignature(String pharmacy_id, String delivery_id, String status_signature, String signature_person, String deliver_remark, String status_contact_patient)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package_note"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("delivery_id", delivery_id));
		params.add(new BasicNameValuePair("signature", status_signature));
		params.add(new BasicNameValuePair("signature_person", signature_person));
		params.add(new BasicNameValuePair("deliver_remark", deliver_remark));
		params.add(new BasicNameValuePair("contact_patient", status_contact_patient));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateUserCurrentLocation(String pharmacy_id, double latitude, double longitude)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "current_user_location"));

		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		//params.add(new BasicNameValuePair("new_location_id", location_id));
		params.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
		params.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {
			boolean update = createUserCurrentLocation(pharmacy_id, latitude, longitude);
			return update;
		}
	}

	public boolean updateImageUser(String pharmacy_id, String userid, String image_id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("userid", userid));
		params.add(new BasicNameValuePair("image_id", image_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public int createUserImage(String pharmacy_id, String userid, byte[] myImage, byte[] myImageThumbnail)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();

		params.add(new BasicNameValuePair("object", "image"));
		params.add(new BasicNameValuePair("image_type", "user_image"));
		params.add(new BasicNameValuePair("userid", userid));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
		//params.add(new BasicNameValuePair("image", ""));
		//params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

		JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

		int imageID = 0;
		boolean update = false;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					imageID = json.getInt("inserted_id");

					/*if(imageID > 0){
						update = updateImageUser(pharmacy_id, userid, String.valueOf(imageID));
					}*/
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return imageID;
	}

	public int createPackageImage(String pharmacy_id, String packageid, byte[] myImage, byte[] myImageThumbnail)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String currentSESSION = WSSession.getInstance().getString();

		params.add(new BasicNameValuePair("object", "image"));
		params.add(new BasicNameValuePair("image_type", "package_image"));
		params.add(new BasicNameValuePair("packageid", packageid));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("token", currentSESSION));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("image", Base64.encodeToString(myImage, Base64.NO_WRAP)));
		//params.add(new BasicNameValuePair("image", ""));
		//params.add(new BasicNameValuePair("thumbnailImage", Base64.encodeToString(myImageThumbnail, Base64.NO_WRAP)));

		JSONObject json = jsonParser.makeHttpRequest(webservice_url + "/" + url_create_item, "MULTIPOST", params);

		int imageID = 0;
		if (json != null)
		{
			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1)
				{
					imageID = json.getInt("inserted_id");
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		return imageID;
	}

	public cPackageImage getPackageImage(String pharmacy_id, String id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "package_image"));
		params.add(new BasicNameValuePair("packageid", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("fullimage", "true"));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cPackageImage img = new cPackageImage();

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				img.id = c.getString("id");
				img.datetime = c.getString("datetime");
				img.user = c.getString("user");
				img.image = c.getString("full_image");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return img;
	}

	public cUserImage getUserImage(String pharmacy_id, String id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user_image"));
		params.add(new BasicNameValuePair("userid", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("fullimage", "true"));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cUserImage img = new cUserImage();

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				img.id = c.getString("id");
				img.user = c.getString("user");
				img.image = c.getString("full_image");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return img;
	}

	public String getImageFull(String id, String pharmacy_id)
	{
		String result = "";

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "image"));
		params.add(new BasicNameValuePair("imageid", id));
		params.add(new BasicNameValuePair("fullimage", "true"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);
				result = c.getString("full_image");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public String getImageThumnail(String id, String pharmacy_id)
	{
		String result = "";

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "image"));
		params.add(new BasicNameValuePair("imageid", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);
				result = c.getString("image");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public boolean createAddress(String script_id, String barcode, String name, String gender, String birthdate, String phone,
								 String address_id, String address_1, String address_2, String house_number, String zipcode,
								 String city, String province, String country, String package_id,
								 String nr_packages, String location, String collection_id, String free_text, String user, String pharmacy_id)
	{

		/*String url = create_script_url;
		url = url + "script_id=" + script_id;
		url = url + "&barcode=" + barcode;
		url = url + "&name=" + name;
		url = url + "&gender=" + gender;
		url = url + "&birthdate=" + birthdate;
		url = url + "&phone=" + phone;
		url = url + "&address_id=" + address_id;
		url = url + "&address_1=" + address_1;
		url = url + "&address_2=" + address_2;
		url = url + "&house_number=" + house_number;
		url = url + "&zipcode=" + zipcode;
		url = url + "&city=" + city;
		url = url + "&province=" + province;
		url = url + "&country=" + country;
		url = url + "&package_id=" + package_id;
		url = url + "&nr_packages=" + nr_packages;
		url = url + "&location=" + location;*/

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("object", "package")); // object was missing, but why?
		params.add(new BasicNameValuePair("script_id", script_id));
		params.add(new BasicNameValuePair("barcode", barcode));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("gender", gender));
		params.add(new BasicNameValuePair("birthdate", birthdate));
		params.add(new BasicNameValuePair("phone", phone));
		params.add(new BasicNameValuePair("address_id", address_id));
		params.add(new BasicNameValuePair("address_1", address_1));
		params.add(new BasicNameValuePair("address_2", address_2));
		params.add(new BasicNameValuePair("house_number", house_number));
		params.add(new BasicNameValuePair("zipcode", zipcode));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("province", province));
		params.add(new BasicNameValuePair("country", country));
		params.add(new BasicNameValuePair("package_id", package_id));
		params.add(new BasicNameValuePair("nr_packages", nr_packages));
		params.add(new BasicNameValuePair("location", location));
		params.add(new BasicNameValuePair("collection_id", collection_id));
		params.add(new BasicNameValuePair("free_text", free_text));
		params.add(new BasicNameValuePair("user", user));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + create_script_url, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

    public boolean createDefaultStop(String name, String street, String house, String zipcode, String city, String country, String pharmacy_id){

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("object", "default_stop"));
        params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("street", street));
        params.add(new BasicNameValuePair("house", house));
        params.add(new BasicNameValuePair("zipcode", zipcode));
        params.add(new BasicNameValuePair("city", city));
        params.add(new BasicNameValuePair("country", country));

        JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }
    }

	public boolean editDefaultStop(String stopid, String name, String street, String house, String zipcode, String city, String country, String pharmacy_id){

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "default_stop"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("stopid", stopid));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("street", street));
		params.add(new BasicNameValuePair("house", house));
		params.add(new BasicNameValuePair("zipcode", zipcode));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("country", country));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean deleteDefaultStop(String stopid, String pharmacy_id){

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "default_stop"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("stopid", stopid));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_delete_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public cUserStats getUserStatistics(String pharmacy_id, String userid)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "user_stats"));
		params.add(new BasicNameValuePair("report_user", userid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cUserStats stats = new cUserStats();

		try
		{
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				stats.id = c.getString("id");
				stats.created = c.getString("created");
				stats.user = c.getString("user");
				stats.days_active = c.getString("days_active");
				stats.packages_collected = c.getString("packages_collected");
				stats.packages_delivered = c.getString("packages_delivered");
				stats.packages_notathome = c.getString("packages_notathome");
				stats.routes = c.getString("routes");
				stats.address_visited = c.getString("address_visited");
				stats.address_navigated = c.getString("address_navigated");
				stats.signatures = c.getString("signatures");
				stats.total_distance = c.getString("total_distance");
				stats.total_time = c.getString("total_time");
				//stats.lvl = c.getString("lvl");
				//stats.lvl_xp = c.getString("lvl_xp");
				//stats.total_xp = c.getString("total_xp");
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return stats;
	}

	public Boolean getSecurityCode(String username, String email)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("email", email));

		JSONArray items = null;

		items = CallWebServiceForPassword(params, webservice_url + "/" + request_reset_password, false);
		System.out.println("items= " + items);

		String items_string = items.toString();

		try
		{
			if (items.length() > 0)
			{
				Matcher m = Pattern.compile("\"([^\"]*)\"").matcher(items_string);
				while(m.find()) {
					String x = m.group(1);
					if(x.equals("reset request successfully created."))result = true;
					System.out.println("getSecurityCode= " + m.group(1));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public Boolean registerDevice(String username, String email, String device_id, String device_name)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("device", device_id));
		params.add(new BasicNameValuePair("device_name", device_name));


		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + register_device, false);
		System.out.println("items= " + items);

        if (last_error.equals(""))
        {
            return true;

        }else {

            return false;
        }

	}

	public Boolean getApproveNewPassword(String username, String email, String password, String token)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("token", token));

		JSONArray items = null;

		items = CallWebServiceForPassword(params, webservice_url + "/" + process_reset_password, false);
		System.out.println("items= " + items);

		String items_string = items.toString();

		try
		{
			if (items.length() > 0)
			{
				Matcher m = Pattern.compile("\"([^\"]*)\"").matcher(items_string);
				while(m.find()) {
					String x = m.group(1);
					if(x.equals("password successfully reset."))result = true;
					System.out.println(m.group(1));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public boolean createUserCurrentLocation(String pharmacy_id, double latitude, double longitude)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "user_current_location"));

		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		//params.add(new BasicNameValuePair("new_location_id", location_id));
		params.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
		params.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;
		}else {
			if(last_error.contains("Duplicate entry")){
				boolean update = updateUserCurrentLocation(pharmacy_id, latitude, longitude);
				return update;
			}else{
				return false;
			}
		}

	}

	// temporary
	public String driving_route(String map, String address_start, String address_return, String route, String pharmacyId, int width_map, int height_map, Double startlat, Double startlong, Double endlat, Double endlong, Context mContext){
		String map_url = "";

		// address_return = 1 (using pharmacy when return)=> MyRoutes
		if(address_return.equals("1")){
			// return pharmacy and waypoints
			if(width_map != 0 && height_map != 0){
				//if(map.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid_copy.php (google)
					map_url = webservice_url + "/" + map + "?" + "routeid="+route+ "&pharmacyid="+pharmacyId+"&map_width="+width_map+
							"&map_height="+height_map+"&start="+address_start+"&return=1";
				//}

			}else{
				//if(map.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid_copy.php (google)
					map_url = webservice_url + "/" + map + "?" + "routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
				//}

			}

			/*if(width_map != 0 && height_map != 0){
				map_url = urlMap+"?"+"routeid="+route+ "&pharmacyid="+pharmacyId+"&map_width="+width_map+
						"&map_height="+height_map+"&start="+address_start+"&return=1";
			}else{
				map_url = urlMap+"?"+"routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
			}*/

		}else{
			// return address => AddressDetail
			if(width_map != 0 && height_map != 0){
				//if(map.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid.php (google)
					map_url = webservice_url + "/" + map + "?" + "map_width="+width_map+"&map_height="+height_map+"&start="+address_start+"&return="+address_return;
				//}

			}else{
				//if(map.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid.php (google)
					map_url = webservice_url + "/" + map + "?" +"start="+address_start+"&return="+address_return;
				//}

			}
		}

		Log.d("MAP URL", map_url);
		return map_url;
	}

	public String driving_route_original(String address_start, String address_return, String route, String pharmacyId, int width_map, int height_map, Double startlat, Double startlong, Double endlat, Double endlong, Context mContext){
		String map_url = "";
		SharedPreferences sharedPrefs = mContext.getSharedPreferences("defaults", mContext.MODE_PRIVATE );
		final String phpFileMap = sharedPrefs.getString("prefMapServiceServer", "");

		// address_return = 1 (using pharmacy when return)=> MyRoutes
		if(address_return.equals("1")){
			// return pharmacy and waypoints
			if(width_map != 0 && height_map != 0){
				if(phpFileMap.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid_copy.php (google)
					map_url = webservice_url + "/" + phpFileMap + "?" + "routeid="+route+ "&pharmacyid="+pharmacyId+"&map_width="+width_map+
							"&map_height="+height_map+"&start="+address_start+"&return=1";
				}else{
					// phpFileMap = bing_map_myroute.php (bing)
					map_url = webservice_url + "/" + phpFileMap + "?" + "routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
				}

			}else{
				if(phpFileMap.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid_copy.php (google)
					map_url = webservice_url + "/" + phpFileMap + "?" + "routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
				}else{
					// phpFileMap = bing_map_myroute.php (bing)
					map_url = webservice_url + "/" + phpFileMap + "?"+ "routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
				}

			}

			/*if(width_map != 0 && height_map != 0){
				map_url = urlMap+"?"+"routeid="+route+ "&pharmacyid="+pharmacyId+"&map_width="+width_map+
						"&map_height="+height_map+"&start="+address_start+"&return=1";
			}else{
				map_url = urlMap+"?"+"routeid="+route+"&pharmacyid="+pharmacyId+"&start="+address_start+"&return=1";
			}*/

		}else{
			// return address => AddressDetail
			if(width_map != 0 && height_map != 0){
				if(phpFileMap.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid.php (google)
					map_url = webservice_url + "/" + phpFileMap + "?" + "map_width="+width_map+"&map_height="+height_map+"&start="+address_start+"&return="+address_return;
				}else{
					// phpFileMap = bing_map_driving_route.php (bing) -> for detail
					map_url = webservice_url + "/" + "bing_map_driving_route.php" + "?" +
							"start_lat=" + startlat + "&start_long=" + startlong + "&return_lat=" + endlat + "&return_long=" + endlong +
							"&map_width=" + width_map + "&map_height=" + height_map;
				}

			}else{
				if(phpFileMap.equals("bing_map_route_byid_copy.php")){
					// phpFileMap = bing_map_route_byid.php (google)
					map_url = webservice_url + "/" + phpFileMap + "?" +"start="+address_start+"&return="+address_return;
				}else{
					// phpFileMap = bing_map_driving_route.php (bing) -> for detail
					map_url = webservice_url + "/" + "bing_map_driving_route.php" + "?" +
							"start_lat=" + startlat + "&start_long=" + startlong + "&return_lat=" + endlat + "&return_long=" + endlong;
				}

			}
		}

		Log.d("MAP URL", map_url);
		return map_url;
	}

		/*public String driving_route(Double startlat, Double startlong, Double endlat, Double endlong, int width_map, int height_map){
			String map_url = "";
			if(width_map != 0 && height_map != 0){
				map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php?" +
						"start_lat=" + startlat + "&start_long=" + startlong + "&return_lat=" + endlat + "&return_long=" + endlong +
						"&map_width=" + width_map + "&map_height=" + height_map;
			}else{
				map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php?" +
						"start_lat=" + startlat + "&start_long=" + startlong + "&return_lat=" + endlat + "&return_long=" + endlong;
			}

			return map_url;
		}

		public String driving_routebyid(String address, String route, String pharmacyId, int width_map, int height_map){
			String map_url = "";
			if(address != ""){
				if(width_map != 0 && height_map != 0){
					map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_route_byid.php?routeid="+route+
							"&pharmacyid="+pharmacyId+"&map_width="+width_map+"&map_height="+height_map+"&start="+address
							+ "&return=1";
				}else{
					map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_route_byid.php?routeid="+route+
							"&pharmacyid="+pharmacyId+"&start="+address+"&return=1";
				}
			}else{
				if(width_map != 0 && height_map != 0){
					map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_route_byid.php?routeid="+route+
							"&pharmacyid="+pharmacyId+"&map_width="+width_map+"&map_height="+height_map;
				}else{
					map_url = "http://www.sharethatdata.com/devlogistics-ws/bing_map_route_byid.php?routeid="+route+
							"&pharmacyid="+pharmacyId;
				}
			}


			return map_url;
		}*/

	public boolean createMessage(String pharmacy_id, String user_to, String message)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("object", "message"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("to_user", user_to));
		params.add(new BasicNameValuePair("message", message));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_create_item, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}

	}

	public List<cMessage> getMessagesInbox(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "message_inbox"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_items_new, false);

		// result list
		List<cMessage> returnList = new ArrayList<cMessage>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cMessage myMessage = new cMessage();

					myMessage.id = c.getLong("id");
					myMessage.message = c.getString("message");
					myMessage.user_from = c.getString("messagefrom");
					myMessage.datetime = c.getString("created");
					myMessage.status_read = c.getString("status");

					SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

					try {
						Date date = json_format.parse(myMessage.datetime);
						Date today = new Date();
						today=new Date(today.getTime()-(24*60*60*1000));
						if (date.before(today))
						{
							new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
						}
						myMessage.datetime = new_format.format(date);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					returnList.add(myMessage);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public boolean updateMessageStatusRead(String messageid, String pharmacy_id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "message"));
		params.add(new BasicNameValuePair("id", messageid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item,false);
		System.out.println("Message update= " + items);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateMessageStatusNotify(String messageid, String pharmacy_id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "notification"));
		params.add(new BasicNameValuePair("notificationid", messageid));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));
		params.add(new BasicNameValuePair("status_read", "1"));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		System.out.println("Message update= " + items);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateMessageStatusNotification(String id, String pharmacy_id)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "notify_message"));
		params.add(new BasicNameValuePair("id", id));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		System.out.println("Message update= " + items);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public cMessage getNewMessage(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "inbox_message"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cMessage myMessage = new cMessage();

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myMessage.id = c.getLong("id");
				myMessage.message = c.getString("message");
				myMessage.user_from = c.getString("messagefrom");
				myMessage.datetime = c.getString("created");
				myMessage.status_read = c.getString("status");

				SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

				try {
					Date date = json_format.parse(myMessage.datetime);
					Date today = new Date();
					today=new Date(today.getTime()-(24*60*60*1000));
					if (date.before(today))
					{
						new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					}
					myMessage.datetime = new_format.format(date);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myMessage;

	}

	public cMessage getNewMessageNotify(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "notify_message"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cMessage myMessage = new cMessage();

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myMessage.id = c.getLong("id");
				myMessage.message = c.getString("message");
				myMessage.user_from = c.getString("messagefrom");
				myMessage.datetime = c.getString("created");
				myMessage.status_read = c.getString("status");

				SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

				try {
					Date date = json_format.parse(myMessage.datetime);
					Date today = new Date();
					today=new Date(today.getTime()-(24*60*60*1000));
					if (date.before(today))
					{
						new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					}
					myMessage.datetime = new_format.format(date);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myMessage;

	}

	public cMessage getNotifyMessage(String pharmacy_id)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "notification_inbox"));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacy_id));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + url_get_item_new, false);

		cMessage myMessage = new cMessage();

		try {
			if (items.length() > 0)
			{
				JSONObject c = items.getJSONObject(0);

				myMessage.id = c.getLong("id");
				myMessage.message = c.getString("description");
				myMessage.user_to = c.getString("id_user");
				myMessage.datetime = c.getString("datetime");
				myMessage.status_read = c.getString("status_read");

				SimpleDateFormat json_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

				try {
					Date date = json_format.parse(myMessage.datetime);
					Date today = new Date();
					today=new Date(today.getTime()-(24*60*60*1000));
					if (date.before(today))
					{
						new_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					}
					myMessage.datetime = new_format.format(date);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myMessage;

	}



	/*  *** CERBERUS *** */

	public Boolean registerDeviceCerberus(String apikey, String device_id, String device_name)
	{

		boolean result = false;

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", device_id));
		params.add(new BasicNameValuePair("device_name", device_name));


		JSONArray items = null;

		items = CallWebService(params, webservice_url + "/" + register_device_cerberus, false);
		System.out.println("items= " + items);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public List<cWebService> getAllWebServices(String apikey, String deviceid, String ws_type)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", deviceid));
		if(ws_type != "") params.add(new BasicNameValuePair("ws_type", ws_type));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + get_webservices_cerberus, false);

		// result list
		List<cWebService> returnList = new ArrayList<cWebService>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cWebService myWebService = new cWebService();

					myWebService.id = c.getString("id");
					myWebService.name = c.getString("name");
					myWebService.type = c.getString("type");
					myWebService.url = c.getString("url");
					myWebService.default_url = c.getString("default");
					myWebService.access_key = c.getString("access_key");

					returnList.add(myWebService);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	public List<cDeviceSetting> getDeviceSettings(String apikey, String deviceid, String ws_type, String webservice)
	{
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", deviceid));
		params.add(new BasicNameValuePair("ws_type", ws_type));
		params.add(new BasicNameValuePair("webservice", webservice));

		// getting JSON Object
		JSONArray items = CallWebService(params, webservice_url + "/" + get_device_setting_cerberus, false);

		// result list
		List<cDeviceSetting> returnList = new ArrayList<cDeviceSetting>();

		try {
			if (items.length() > 0)
			{
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);

					cDeviceSetting myDeviceSetting = new cDeviceSetting();

					myDeviceSetting.id = c.getString("id");
					myDeviceSetting.webservice = c.getString("webservice");
					myDeviceSetting.ws_type = c.getString("ws_type");
					myDeviceSetting.setting = c.getString("setting");
					myDeviceSetting.value = c.getString("value");

					returnList.add(myDeviceSetting);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	public boolean updateDeviceSettings(String apikey, String device, String id, String setting, String value)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("apikey", apikey));
		params.add(new BasicNameValuePair("device", device));
		params.add(new BasicNameValuePair("setting", setting));
		params.add(new BasicNameValuePair("value", value));
		params.add(new BasicNameValuePair("id", id));

		JSONArray items = CallWebService(params, webservice_url + "/" + update_device_setting_cerberus, false);

		if (last_error.equals(""))
		{
			return true;

		}else {

			return false;
		}
	}

	public boolean updateDistanceRangeFromPharmacy(String pharmacyid, String distance_range_value)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("object", "distance_range_from_pharmacy"));
		params.add(new BasicNameValuePair("user", myUser));
		params.add(new BasicNameValuePair("pharmacy_id", pharmacyid));
		params.add(new BasicNameValuePair("distance_range_value", distance_range_value));

		JSONArray items = CallWebService(params, webservice_url + "/" + url_update_item, false);
		System.out.println("Message update= " + items);

		if (last_error.equals(""))
		{
			return true;
		}else {
			return false;
		}
	}



}
