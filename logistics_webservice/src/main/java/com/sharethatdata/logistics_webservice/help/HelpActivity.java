package com.sharethatdata.logistics_webservice.help;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.sharethatdata.logistics_webservice.R;

public class HelpActivity extends Activity{
	
	private TextView myVersionView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		myVersionView = (TextView) findViewById(R.id.help_version);

		PackageManager manager = this.getPackageManager();
		try
		{
			PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
			myVersionView.setText("version " + info.versionName);

		} catch (Exception ex)
		{
		
		}
		
	}
	
}
