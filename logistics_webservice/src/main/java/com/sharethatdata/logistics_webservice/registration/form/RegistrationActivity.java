package com.sharethatdata.logistics_webservice.registration.form;

import java.io.ByteArrayOutputStream;

import com.sharethatdata.logistics_webservice.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends Activity{
	
	String LOG_USER_REGISTRATION = "LOG_USER_REGISTRATION";
	
    String myEmployee ;

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_form);
		
		//Globals = ((MyGlobals)getApplication());
		
		//MyProvider = new WSDataProvider(myUser, myPass);			
	   // MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		
		Button submit = (Button) findViewById(R.id.btnSubmit);
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    TextView SurNameView = (TextView) findViewById(R.id.textSurName);
				myEmployee = "Surname: " + SurNameView.getText().toString();

				TextView LastNameView = (TextView) findViewById(R.id.textLastName);
				myEmployee += "\nLastname: " + LastNameView.getText().toString();
				
				TextView CompanyView = (TextView) findViewById(R.id.textCompany);
				myEmployee += "\nCompany: " + CompanyView.getText().toString();

				TextView UserNameView = (TextView) findViewById(R.id.textUserName);
				myEmployee += "\nUsername: " + UserNameView.getText().toString();

				TextView FunctionView = (TextView) findViewById(R.id.textFunction);
				myEmployee += "\nFunction: " + FunctionView.getText().toString();

				TextView EmailView = (TextView) findViewById(R.id.textEmail);
				myEmployee += "\nEmail: " + EmailView.getText().toString();
				
				TextView PhoneView = (TextView) findViewById(R.id.textPhone);
				myEmployee += "\nPhone: " + PhoneView.getText().toString();

				
				sendEmail(myEmployee);
			}
		});
		
	}
	
    public void sendEmail(String bodyContent) {

        final Intent emailIntent = new Intent(
                Intent.ACTION_SEND);

        // open with email supported apps
        emailIntent.setType("message/rfc822");

        // support mail from string.xml
        emailIntent.putExtra(Intent.EXTRA_EMAIL,
                new String[] { getString(R.string.crash_user_email_label) });

        // AppName + Subject text from string.xml
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                getString(R.string.app_name) + " User Register ");

        // Inital body text from string.xml + crash log 
        emailIntent.putExtra(Intent.EXTRA_TEXT,
        		"--------- Informations from new user ---------\n\n" + bodyContent);
        
        Toast.makeText(RegistrationActivity.this, getString(R.string.register_form_info_toast), Toast.LENGTH_LONG).show();

        finish();
        
        startActivity(Intent.createChooser(emailIntent, "Send Request"));
        
    }

}
