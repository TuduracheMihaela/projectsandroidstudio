package com.sharethatdata.logistics_webservice.report;


import com.sharethatdata.logistics_webservice.R;
import com.sharethatdata.logistics_webservice.UIHelper;
import com.sharethatdata.logistics_webservice.WSDataProvider;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CustomCrashReportDialog extends Activity implements OnClickListener{
	
	 	String VersionName;
	    String PackageName;
	    String FilePath;
	    String PhoneModel;
	    String AndroidVersion;
	    String Board;
	    String Brand;
	    String Device;
	    String Display;
	    String FingerPrint;
	    String Host;
	    String ID;
	    String Manufacturer;
	    String Model;
	    String Product;
	    String Tags;
	    long Time;
	    String Type;
	    String User;
	    
	    Context mContext;
	    String crashLog="TEXT";
	    Button buttonSubmitReport;
	    EditText editTextReport;
		TextView textViewReport;
	    
	
	    WSDataProvider MyProvider = null;
	    
	    String user = "";
	    String pass = "";
	    String webservice_url = "";
	    String pharmacyId = "";

		private String ip = "";
		private String android_id = "";
		private String appname = "";

	    String error_message = "";

	    String description = "";

		String report = "";
	    
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        setContentView(R.layout.custom_dialog);

   	    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
	        user = prefUser.getString("USER", ""); 
	        pass = prefUser.getString("PASS", ""); 
	        webservice_url = prefUser.getString("URL", "");

			SharedPreferences prefPharmacy = getSharedPreferences("PREF_Pharmacy", 0);
			pharmacyId = prefPharmacy.getString("Pharmacy","");

			SharedPreferences prefDevice = getSharedPreferences("PREF_DEVICE", 0);
			ip = prefDevice.getString("DEVICE_IP","");
			android_id = prefDevice.getString("DEVICE_ID","");
			appname = prefDevice.getString("DEVICE_NAME","");

	        mContext = this;

	        if (getIntent().getStringExtra("CRASH_LOG") != null) {
	            crashLog = getIntent().getStringExtra("CRASH_LOG");
	        }

			String CurrentString = crashLog;
			String[] separated = CurrentString.split("--------- Stack trace ---------");
			String cause = separated[0]; // this will contain description error, small info for user
			String error_report = separated[1]; // this will contain "Stack trace and Cause" - hide cause from user


			TextView tv = (TextView)findViewById(R.id.textReport);
			tv.setText(cause.toString());

			// showDialog(text);
	        crashLog += CreateInformationString();

			textViewReport = (TextView) findViewById(R.id.textReport);
	        //((TextView) findViewById(R.id.textReport)).setText(crashLog);
	        buttonSubmitReport = (Button) findViewById(R.id.buttonSumbmitReport);
	        buttonSubmitReport.setOnClickListener(this);
	        editTextReport = (EditText) findViewById(R.id.editTextReport);
	        
	        Toast.makeText(CustomCrashReportDialog.this, getString(R.string.crash_dialog_text), Toast.LENGTH_LONG).show();

	    }

	    @Override
	    public void onClick(View v) {
	        int id = v.getId();
			if (id == R.id.buttonSumbmitReport) {
				//sendEmailErrorReport(crashLog);
				
				description = "--------- Report from user ---------\n" +
		        		editTextReport.getText().toString() 
		        		+ "\n------------------------------------------------------------------------\n"
		        + crashLog;
				
				new RegisterTask(description).execute();
			}
	    }

	    public void sendEmailErrorReport(String bodyContent) {

	        final Intent emailIntent = new Intent(
	                Intent.ACTION_SEND);

	        // open with email supported apps
	        emailIntent.setType("message/rfc822");

	        // support mail from string.xml
	        emailIntent.putExtra(Intent.EXTRA_EMAIL,
	                new String[] { getString(R.string.crash_user_email_label) });

	        // AppName + Subject text from string.xml
	        emailIntent.putExtra(Intent.EXTRA_SUBJECT,
	                getString(R.string.app_name) + " Crash Report ");

	        // Inital body text from string.xml + crash log 
	        emailIntent.putExtra(Intent.EXTRA_TEXT,
	        		"--------- Report from user ---------\n" +
	        		editTextReport.getText().toString() 
	        		+ "\n------------------------------------------------------------------------\n"
	        + bodyContent);

	        finish();
	        
	        startActivity(Intent.createChooser(emailIntent, "Send Report"));

	        // restart app
	        // restartApp();
	    	
	    	
	    	/*String customText = getString(R.string.app_name) + "\n" + "Custom report from user: " + "\n" +
	    	editTextReport.getText().toString() + "\n" + bodyContent;
	    	
	    	new RegisterTask(customText).execute();*/
	    	
	        
	        
	    }

	    public void restartApp() {
	        Intent i = getBaseContext().getPackageManager()
	                .getLaunchIntentForPackage(getBaseContext().getPackageName());
	        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        
	        finish();
	        startActivity(i);
	    }
	    

	    void RecoltInformations(Context context) {
	        try {
	            PackageManager pm = context.getPackageManager();
	            PackageInfo pi;
	            // Version
	            pi = pm.getPackageInfo(context.getPackageName(), 0);
	            VersionName = pi.versionName;
	            // Package name
	            PackageName = pi.packageName;
	            // Device model
	            PhoneModel = android.os.Build.MODEL;
	            // Android version
	            AndroidVersion = android.os.Build.VERSION.RELEASE;

	            Board = android.os.Build.BOARD;
	            Brand = android.os.Build.BRAND;
	            Device = android.os.Build.DEVICE;
	            Display = android.os.Build.DISPLAY;
	            FingerPrint = android.os.Build.FINGERPRINT;
	            Host = android.os.Build.HOST;
	            ID = android.os.Build.ID;
	            Model = android.os.Build.MODEL;
	            Product = android.os.Build.PRODUCT;
	            Tags = android.os.Build.TAGS;
	            Time = android.os.Build.TIME;
	            Type = android.os.Build.TYPE;
	            User = android.os.Build.USER;

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    public String CreateInformationString() {
	        RecoltInformations(mContext);

	        String ReturnVal = "";
	        ReturnVal += "Version : " + VersionName;
	        ReturnVal += "\n";
	        ReturnVal += "Package : " + PackageName;
	        ReturnVal += "\n";
	        ReturnVal += "FilePath : " + FilePath;
	        ReturnVal += "\n";
	        ReturnVal += "Phone Model" + PhoneModel;
	        ReturnVal += "\n";
	        ReturnVal += "Android Version : " + AndroidVersion;
	        ReturnVal += "\n";
	        ReturnVal += "Board : " + Board;
	        ReturnVal += "\n";
	        ReturnVal += "Brand : " + Brand;
	        ReturnVal += "\n";
	        ReturnVal += "Device : " + Device;
	        ReturnVal += "\n";
	        ReturnVal += "Display : " + Display;
	        ReturnVal += "\n";
	        ReturnVal += "Finger Print : " + FingerPrint;
	        ReturnVal += "\n";
	        ReturnVal += "Host : " + Host;
	        ReturnVal += "\n";
	        ReturnVal += "ID : " + ID;
	        ReturnVal += "\n";
	        ReturnVal += "Model : " + Model;
	        ReturnVal += "\n";
	        ReturnVal += "Product : " + Product;
	        ReturnVal += "\n";
	        ReturnVal += "Tags : " + Tags;
	        ReturnVal += "\n";
	        ReturnVal += "Time : " + Time;
	        ReturnVal += "\n";
	        ReturnVal += "Type : " + Type;
	        ReturnVal += "\n";
	        ReturnVal += "User : " + User;
	        ReturnVal += "\n";
	        // ReturnVal += "Total Internal memory : " +
	        // getTotalInternalMemorySize();
	        // ReturnVal += "\n";
	        // ReturnVal += "Available Internal memory : "
	        // + getAvailableInternalMemorySize();
	        ReturnVal += "\n";

	        return ReturnVal;
	    }

	/** AsyncTask register time record  */
	    private class RegisterTask extends AsyncTask<String, String, Boolean>{

			  String description;
	    	
	    	public RegisterTask(String description){
	    		this.description = description;
	    	}
	 
	        @Override
	        protected Boolean doInBackground(String... args) {
	        	
	        	  // extract data
	        	 System.out.println(" MyProvider: " + webservice_url + " " + user + " " + pass);
				  
				  // call web method - Data Provider
	        	 	MyProvider = new WSDataProvider(getApplication());
	        	 	MyProvider = new WSDataProvider(user , pass);
			        MyProvider.SetWebServiceUrl(webservice_url);
			        
				  //MyProvider = new WSDataProvider(getApplication());		       
				  
				  boolean suc = false;

					  suc = MyProvider.create_report("", "", user, getString(R.string.crash_user_email_label), getString(R.string.app_name), description, "0", pharmacyId);
			  
	        	  return suc;
	        }
	           
	        /**
		     * After completing background task Dismiss the progress dialog
		     * **/
	        protected void onPostExecute(final Boolean success) {
	        	
		    	// updating UI from Background Thread
		        runOnUiThread(new Runnable() {
		        	
		            public void run() {
		            
		            	if (success == false)
		            	{
		            		 error_message = MyProvider.last_error;
		            		 String error_message_nr = MyProvider.last_error_nr;

		            		 UIHelper myUIHelper = new UIHelper();
			    			    
			    			 //myUIHelper.ShowDialog(CustomCrashReportDialog.this, getString(R.string.text_error), error_message);
		            		 
		            		 System.out.println(" CustomCrashReportDialog error_message: " + error_message);
							 if(error_message.equals("required field (token) missing") || error_message_nr.equals("10001")){
								 new UserLoginTask().execute();
							 }
		            		 finish();
			    		}else{
			    			System.out.println(" CustomCrashReportDialog: " + error_message);
			    			Toast.makeText(CustomCrashReportDialog.this, getString(R.string.crash_dialog_submit_toast), Toast.LENGTH_LONG).show();
			    			finish();
			    		}       	   	
		              }
		        });    
		    }
	    }


	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			//MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			String mEmail = user;
			String mPassword = pass;

			if(mEmail.length() == 0 || mPassword.length() == 0 ){
			    	/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
			        String prefEmail = pref.getString("Name", "");
			        String prefPass = pref.getString("Pass", "");*/

				SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
				String prefEmail = prefUser.getString("USER", "");
				String prefPass = prefUser.getString("PASS", "");
				webservice_url = prefUser.getString("URL", "");

				mEmail = prefEmail;
				mPassword = prefPass;

				System.out.println("mEmail mPassword from pref: " + mEmail + " " + mPassword);
			}else {
				System.out.println("mEmail mPassword from global: " + mEmail + " " + mPassword);
			}

			MyProvider = new WSDataProvider(user, pass);
			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider.SetDeviceIdentification(ip, android_id, appname);
			boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "app service");

			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				System.out.println("Login successfully");
				new RegisterTask(description).execute();
			} else {
				System.out.println("Login failed");
			}
		}

		@Override
		protected void onCancelled() {

		}
	}


}


