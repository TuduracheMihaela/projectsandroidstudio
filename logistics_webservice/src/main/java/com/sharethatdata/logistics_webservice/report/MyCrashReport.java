package com.sharethatdata.logistics_webservice.report;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.sharethatdata.logistics_webservice.report.CustomCrashReportDialog;

public class MyCrashReport extends Application{
	
	@Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
    }

    public class TopExceptionHandler implements Thread.UncaughtExceptionHandler {

        private Application app = null;
        
        public TopExceptionHandler(Application app) {
            Thread.setDefaultUncaughtExceptionHandler(this);
            this.app = app;
        }

        public void uncaughtException(Thread t, Throwable e) {
            
            Log.v("tagas","uncaught exception ");
            
            StackTraceElement[] arr = e.getStackTrace();
            String report = e.toString() + "\n\n";
            report += "--------- Stack trace ---------\n\n";
            for (int i = 0; i < arr.length; i++) {
                report += "    " + arr[i].toString() + "\n";
            }
            report += "-------------------------------\n\n";

            // If the exception was thrown in a background thread inside
            // AsyncTask, then the actual exception can be found with getCause
            report += "--------- Cause ---------\n\n";
            Throwable cause = e.getCause();
            if (cause != null) {
                report += cause.toString() + "\n\n";
                arr = cause.getStackTrace();
                for (int i = 0; i < arr.length; i++) {
                    report += "    " + arr[i].toString() + "\n";
                }
            }
            report += "-------------------------------\n\n";

            final String reportText=report;

            Log.d("atag","report["+report+"]");

            notifyDialog(reportText);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
           
        }
        
        void notifyDialog(String reportText) {
            try {
                Log.d("atg", "Creating Dialog for " + reportText);
                Intent dialogIntent = new Intent(app.getApplicationContext(), CustomCrashReportDialog.class);
                dialogIntent.putExtra("CRASH_LOG", reportText);
                dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                app.getApplicationContext().startActivity(dialogIntent);
                
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
