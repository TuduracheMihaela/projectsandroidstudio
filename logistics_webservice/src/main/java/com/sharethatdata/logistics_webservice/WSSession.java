package com.sharethatdata.logistics_webservice;

public class WSSession {

    private static WSSession mInstance = null;

    private String mString;
    private String mUser;
    private String mPassword;
    private String mApp;
    private String mDevice;
    private String mIP;
 
    private WSSession(){
        mString = "";
    }
 
    public static WSSession getInstance(){
        if(mInstance == null)
        {
            mInstance = new WSSession();
        }
        return mInstance;
    }

    public String getString(){
        return this.mString;
    }

    public String getUser(){
        return this.mUser;
    }

    public String getPassword(){
        return this.mPassword;
    }

    public String getAppName(){
        return this.mApp;
    }

    public String getDevice(){
        return this.mDevice;
    }

    public String getIP(){
        return this.mIP;
    }

    public void setString(String value){
        mString = value;
    }

    public void setUserPass(String user, String pass)
    {
        mUser = user;
        mPassword = pass;
    }


    public void setAppName(String app, String device)
    {
        mApp = app;
        mDevice = device;
    }
}
