package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 3/21/2017.
 */

public class cRoute {

    public String id;
    public String datetime_start;
    public String datetime_end;
    public String description;
    public String algorithm;
    public String user;
    public String location;
    public String status;
    public String return_distance;
    public String return_time;

    public cRoute(){
        id = "";
        datetime_start = "";
        datetime_end = "";
        description = "";
        algorithm = "";
        user = "";
        location = "";
        status = "";
        return_distance = "";
        return_time = "";
    }
}
