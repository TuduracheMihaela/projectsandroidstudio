package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by miha on 8/23/2017.
 */

public class cPackagesbyDeliveryId {

    public String datetime;
    public String delivery_id;
    public String script_id;
    public String user_id;
    public String user;
    public String routeid;
    public String route;
    public String start_time;
    public String end_time;
    public String time_scheduled;
    public String distance_scheduled;
    public String sequence_scheduled;
    public String time_driven;
    public String distance_driven;
    public String signature;
    public String signature_person;
    public String patient_remark;
    public String deliver_remark;
    public String contact_patient;
    public String status;
    public String status_description;
    public String processed;

    public cPackagesbyDeliveryId(){
        datetime = "";
        delivery_id = "";
        script_id = "";
        user_id = "";
        user = "";
        routeid = "";
        route = "";
        start_time = "";
        end_time = "";
        time_scheduled = "";
        distance_scheduled = "";
        sequence_scheduled = "";
        time_driven = "";
        distance_driven = "";
        signature = "";
        signature_person = "";
        patient_remark = "";
        deliver_remark = "";
        contact_patient = "";
        status = "";
        status_description = "";
        processed = "";
    }
}
