package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mverbeek on 5-9-2017.
 */

public class cUserStats {

    public String id;
    public String user;
    public String created;
    public String days_active;
    //public String lvl;
    //public String lvl_xp;
    //public String total_xp;
    public String packages_collected;
    public String packages_delivered;
    public String packages_notathome;
    public String routes;
    public String address_visited;
    public String address_navigated;
    public String signatures;
    public String total_distance;
    public String total_time;

    public cUserStats()
    {
        id = "";
        user = "";
        created = "";
        days_active = "";
        //lvl = "0";
        //lvl_xp = "0";
        //total_xp = "0";
        packages_collected = "0";
        packages_delivered = "0";
        packages_notathome = "0";
        routes = "0";
        address_visited = "0";
        address_navigated = "0";
        signatures = "0";
        total_distance = "0";
        total_time = "0";
    }

}


