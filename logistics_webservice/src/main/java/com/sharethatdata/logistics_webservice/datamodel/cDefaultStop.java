package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by miha on 8/24/2017.
 */

public class cDefaultStop {

    public String id;
    public String name;
    public String street;
    public String house;
    public String zipcode;
    public String city;
    public String country;
    public String longitude;
    public String latitude;

    public cDefaultStop(){
        id = "";
        name = "";
        street = "";
        house = "";
        zipcode = "";
        city = "";
        country = "";
        longitude = "";
        latitude = "";

    }
}
