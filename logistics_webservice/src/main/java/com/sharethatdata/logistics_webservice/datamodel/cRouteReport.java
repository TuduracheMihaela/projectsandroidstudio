package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 3/31/2017.
 */

public class cRouteReport {

    public String id;
    public String customer_script;
    public String customer_name;
    public String customer_birthdate;
    public String customer_sex;
    public String customer_street;
    public String customer_house;
    public String customer_zipcode;
    public String customer_city;
    public String customer_country;
    public String longitude;
    public String latitude;
    public String sequence;
    public String distance;
    public String time;
    public String real_distance;
    public String real_time;
    public String datetime_start;
    public String datetime_end;
    public String user;
    public String status;
    public String barcode;


    public cRouteReport(){
        id = "";
        customer_script = "";
        customer_name = "";
        customer_birthdate = "";
        customer_sex = "";
        customer_street = "";
        customer_house = "";
        customer_zipcode = "";
        customer_city = "";
        customer_country = "";
        longitude = "";
        latitude = "";
        sequence = "";
        distance = "";
        time = "";
        real_distance = "";
        real_time = "";
        datetime_start = "";
        datetime_end = "";
        user = "";
        status = "";
        barcode = "";
    }
}
