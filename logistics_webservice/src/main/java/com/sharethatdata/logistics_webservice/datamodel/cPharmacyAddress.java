package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 3/1/2017.
 */

public class cPharmacyAddress {

    public String id;
    public String name;
    public String address1;
    public String address2;
    public String address_house;
    public String address_zipcode;
    public String address_city;
    public String address_country;
    public String latitude;
    public String longitude;


    public cPharmacyAddress(){
        id = "";
        name = "";
        address1 = "";
        address2 = "";
        address_house = "";
        address_zipcode = "";
        address_city = "";
        address_country = "";
        latitude = "";
        longitude = "";
    }
}
