package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 2/2/2017.
 */

public class cPackage {
    public String scriptid;
    public String package_barcode;
    public String route;
    public String package_status;
    public String status_description;

    public cPackage(){
        scriptid = "";
        package_barcode = "";
        route = "";
        package_status = "";
        status_description = "";
    }
}
