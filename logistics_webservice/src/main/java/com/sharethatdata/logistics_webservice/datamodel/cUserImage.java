package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mverbeek on 5-9-2017.
 */

public class cUserImage {

    public String id;
    public String image;
    public String datetime;
    public String user;
    public String status;

    public cUserImage(){
        id = "";
        image = "";
        datetime = "";
        user = "";
        status = "0";
    }
}
