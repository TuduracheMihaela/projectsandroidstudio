package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by miha on 9/25/2017.
 */

public class cUserCurrectLocation {

    public String id;
    public String user_id;
    public String location_id;
    public String location_name;
    public String date_time;
    public String latitude;
    public String longitude;

    public cUserCurrectLocation(){
        id = "";
        user_id = "";
        location_id = "";
        location_name = "";
        date_time = "";
        latitude = "";
        longitude = "";
    }
}
