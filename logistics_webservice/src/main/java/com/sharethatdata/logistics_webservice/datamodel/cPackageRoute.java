package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 3/7/2017.
 */

public class cPackageRoute {

    public String id;
    public String scriptid;
    public String package_barcode;
    public String addressid;
    public String package_status;
    public String status_description;

    public cPackageRoute(){
        id = "";
        scriptid = "";
        package_barcode = "";
        addressid = "";
        package_status = "";
        status_description = "";
    }
}
