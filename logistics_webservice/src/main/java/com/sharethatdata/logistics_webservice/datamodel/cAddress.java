package com.sharethatdata.logistics_webservice.datamodel;

public class cAddress {
	
	public String id;
	public String customer_surname;
	public String customer_lastname;
	public String customer_birthdate;
	public String customer_sex;
	public String customer_street;
	public String customer_house;
	public String customer_zipcode;
	public String customer_city;
	public String customer_country;
	public String free_text;
	public String longitude;
	public String latitude;

	public cAddress(){
		id = "";
		customer_surname = "";
		customer_lastname = "";
		customer_birthdate = "";
		customer_sex = "";
		customer_street = "";
		customer_house = "";
		customer_zipcode = "";
		customer_city = "";
		customer_country = "";
		free_text = "";
		longitude = "";
		latitude = "";
	}
}
