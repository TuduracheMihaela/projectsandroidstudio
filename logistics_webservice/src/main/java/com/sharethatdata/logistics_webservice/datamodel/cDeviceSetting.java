package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by gotic_000 on 6/7/2018.
 */

public class cDeviceSetting {

    public String id;
    public String webservice;
    public String ws_type;
    public String setting;
    public String value;

    public cDeviceSetting(){
        id = "";
        webservice = "";
        ws_type = "";
        setting = "";
        value = "";
    }


}
