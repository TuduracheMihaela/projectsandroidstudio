package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mverbeek on 10-4-2017.
 */

    public class cPackageImage {

        public String id;
        public String image;
        public String datetime;
        public String user;
        public String status;

        public cPackageImage(){
            id = "";
            image = "";
            datetime = "";
            user = "";
            status = "0";
        }
    }