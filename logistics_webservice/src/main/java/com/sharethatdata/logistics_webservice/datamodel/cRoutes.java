package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 2/23/2017.
 */

public class cRoutes {

    public String id;
    public String datetime;
    public String description;
    public String algorithm;
    public String user;
    public String location;

    public cRoutes(){
        id = "";
        datetime = "";
        description = "";
        algorithm = "";
        user = "";
        location = "";
    }
}
