package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 2/2/2017.
 */

public class cContact {
    public String id;
    public String username;
    public String name;
    public String surname;
    public String lastname;
    public String login;
    public String password;
    public String picture;
    public String image_id;
    public String location_id;
    public int auth_level;
    public String function;
    public String phone;
    public String email;
    public String status_id;
    public String status_name;
    public String description;

    public cContact()
    {
        id = "";
        username = "";
        name = "";
        surname = "";
        lastname = "";
        login = "";
        password = "";
        picture = "";
        image_id = "";
        auth_level = 0;
        function = "";
        phone = "";
        email = "";
        status_id = "";
        status_name = "";
        description = "";
        location_id = "";

    }

}
