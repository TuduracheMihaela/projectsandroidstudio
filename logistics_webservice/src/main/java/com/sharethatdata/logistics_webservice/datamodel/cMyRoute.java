package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 2/24/2017.
 */

public class cMyRoute {
    public String id;
    public String datetime;
    public String description;
    public String algorithm;
    public String user;
    public String location;
    public String status;
    public String status_description;

    public cMyRoute(){
        id = "";
        datetime = "";
        description = "";
        algorithm = "";
        user = "";
        location = "";
        status = "";
        status_description = "";
    }
}
