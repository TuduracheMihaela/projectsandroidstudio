package com.sharethatdata.logistics_webservice.datamodel;

/**
 * Created by mihu__000 on 2/2/2017.
 */

public class cPackages {
    public String packageid;
    public String scriptid;
    public String package_barcode;
    public String addressid;
    public String package_status;
    public String package_status_description;
    public String status_id;
    public String route;
    public String location;

    public String delivery_id;
    public String customer_script;
    public String customer_name;
    public String customer_birthdate;
    public String customer_sex;
    public String customer_address;
    public String customer_street;
    public String customer_house;
    public String customer_zipcode;
    public String customer_city;
    public String customer_country;
    public String longitude;
    public String latitude;

    public cPackages(){
        packageid = "";
        scriptid = "";
        package_barcode = "";
        addressid = "";
        package_status = "";
        package_status_description = "";
        status_id = "";
        route = "";
        location = "";

        delivery_id = "";
        customer_script = "";
        customer_name = "";
        customer_birthdate = "";
        customer_sex = "";
        customer_street = "";
        customer_house = "";
        customer_zipcode = "";
        customer_city = "";
        customer_country = "";
        longitude = "";
        latitude = "";
    }
}
