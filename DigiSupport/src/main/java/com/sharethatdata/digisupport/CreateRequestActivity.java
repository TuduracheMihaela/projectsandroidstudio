package com.sharethatdata.digisupport;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digisupport.R.layout;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cRequestStatus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateRequestActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	//WSRoboDataProvider MyRoboProvider = null;
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_TAKE_PHOTO = 2;
	private static final int REQUEST__BROWSE_PHOTO = 3;
	
	private String mCurrentPhotoPathAbsolute;
	private String mCurrentPhotoPath;
	
	ArrayList<HashMap<String, String>> userList;
	ArrayList<HashMap<String, String>> statusList;
	
	List<String> userkeyArray =  new ArrayList<String>();
	List<String> statuskeyArray =  new ArrayList<String>();
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";
	private boolean no_network = false;
	private String error_message = "";
	private boolean input_correct = false;
	
	Spinner spinner = null;
	ProgressDialog progressDialog;
	
	String idUserLogged;
	String requestId = "";
	
	private final int PICK_IMAGE_MULTIPLE =1;
	private ArrayList<String> imagesPathList;
	private LinearLayout lnrImages;
	private Bitmap yourbitmap;
	
	ImageView imageView1;
	Button btnCreateImage;
	Button btnSearchImage;
	
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.create_request);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    //MyRoboProvider = new WSRoboDataProvider(myUser);		
	    //MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
	    imageView1 = (ImageView) findViewById(R.id.imageView1);
	    btnCreateImage = (Button) findViewById(R.id.btnCreateImage);
	    btnSearchImage = (Button) findViewById(R.id.btnSearchImage);
	    
	    //lnrImages = (LinearLayout)findViewById(R.id.lnrImages); // -> for multiple images
		
	    imageView1.setVisibility(View.GONE);
	    
		String edit = Globals.getValue("edit");
		String solve = Globals.getValue("solve");
		String id = Globals.getValue("id");
		String touser = Globals.getValue("touser");
		String status = Globals.getValue("status");
		String description = Globals.getValue("description");
		String myImageData = Globals.getValue("myImageData");
	    requestId = id;
	    
	    if(edit != ""){
	    	//Globals.setValue("edit", "");
	    	btnSubmit.setText("Edit");
	    	
	    	Bitmap d = ConvertByteArrayToBitmap(myImageData);
			  if (d != null)
			  {
				  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
				  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
				  imageView1.setVisibility(View.VISIBLE);
				  imageView1.setImageBitmap(scaled);
				  				  
			  }		
	    }
	    
		if(solve != ""){
			TextView userSpinnerLabel = (TextView) findViewById(R.id.headertouser);
			userSpinnerLabel.setVisibility(Spinner.GONE);
			Spinner userSpinner = (Spinner) findViewById(R.id.spinnertouser);
			userSpinner.setVisibility(Spinner.GONE);
			btnSubmit.setText("Solved");
			
			imageView1.setVisibility(View.GONE);
			btnCreateImage.setVisibility(View.GONE);
			btnSearchImage.setVisibility(View.GONE);
		}
		
		
		
		LoadList InitList;
		InitList = new LoadList();
		InitList.execute();
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
				  
				  input_correct = IsInputCorrect();
				  
				  String edit = Globals.getValue("edit");
				  String solve = Globals.getValue("solve");
				  if(edit != "" && input_correct){
					  Globals.setValue("edit", "");
					  Toast.makeText(getApplicationContext(), "Request updated", Toast.LENGTH_SHORT).show();
					  Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            	  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            	  startActivity(i);
					  UpdateTaskRequest update;
					  update = new UpdateTaskRequest();
					  update.execute();
				  }else if(solve != "" && input_correct){
					  Globals.setValue("solve", "");
					  Toast.makeText(getApplicationContext(), "Request solve updated", Toast.LENGTH_SHORT).show();
					  Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            	  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            	  startActivity(i);
					  UpdateTaskRequestSolved update;
					  update = new UpdateTaskRequestSolved();
					  update.execute();
				  }else if (input_correct){
					  Toast.makeText(getApplicationContext(), "Request created", Toast.LENGTH_SHORT).show();
					  RegisterTask register;
					  register = new RegisterTask();
					  register.execute();
					 // Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
				  }
		
			  }
		 
		});
		
	}
	
	private boolean IsInputCorrect()
	{
		// fields filled
		boolean fields_filled = true;
			
		// user selected
		return fields_filled;
		
	}
	
	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}
	
	// button create image
	public void onCreateImage(View v){
		Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();
		
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	    	  // Create the File where the photo should go
	        File photoFile = null;
	        try
	        {
	            photoFile = createImageFile();
	        } 
	        catch (IOException ex) 
	        {

	        }

	        if (photoFile != null) 
	        {
	            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
	                    Uri.fromFile(photoFile));
	            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
	        }

	       // startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
	    }
	}
	private File createImageFile() throws IOException {
	    // Create an image file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    String imageFileName = "JPEG_" + timeStamp + "_";
	    File storageDir = Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES);
	    File image = File.createTempFile(
	        imageFileName,  /* prefix */
	        ".jpg",         /* suffix */
	        storageDir      /* directory */
	    );

	    // Save a file: path for use with ACTION_VIEW intents
	    mCurrentPhotoPath = "file:" + image.getAbsolutePath();
	    mCurrentPhotoPathAbsolute = image.getAbsolutePath();
	    return image;
	}
	
	// button search image
	public void onSearchImage(View v){
		Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();
		
		Intent intent = new Intent();

		intent.setAction(Intent.ACTION_GET_CONTENT);

		intent.addCategory(Intent.CATEGORY_OPENABLE);

		intent.setType("image/*");

		startActivityForResult(intent, REQUEST__BROWSE_PHOTO);

       // startActivityForResult(intent,PICK_IMAGE_MULTIPLE); // -> for multiple images
	}
	
	// Methods - set visible the preview image
		private void AttachBitmapToReport()
		{
		    int photoW = myBitmap.getWidth();
		    int photoH = myBitmap.getHeight();   	
		    int scaleFactor = Math.min(photoW/100, photoH/100);
		    int NewW = photoW/scaleFactor;
		    int NewH = photoH/scaleFactor;
		    
			imageView1.setVisibility(View.VISIBLE);		
	    	imageView1.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));		
		}
		
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) 
		{
			InputStream stream = null;

		    if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) 
		    {
		    	myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
		    	AttachBitmapToReport();
		    }
		    	    
	        if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK) 
	        {
	        	
	            try 
	            {
	            	stream = getContentResolver().openInputStream(data.getData());
	            	myBitmap = BitmapFactory.decodeStream(stream);
	            	AttachBitmapToReport();
	            } 
	            catch (Exception e)
	            {
	            	e.printStackTrace();
	            }

	            if (stream != null) 
	            {
	            	try
	            	{
	            		stream.close();
	            	} 
	            	catch (Exception e) 
	            	{
	            		e.printStackTrace();
	            	}
	            }            
	        }
		}
		
		// -> for multiple images
/*		@Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        if (resultCode == RESULT_OK) {
	            if(requestCode == PICK_IMAGE_MULTIPLE){
	                imagesPathList = new ArrayList<String>();
	                String[] imagesPath = data.getStringExtra("data").split("\\|");
	                try{
	                    lnrImages.removeAllViews();
	                }catch (Throwable e){
	                    e.printStackTrace();
	                }
	                for (int i=0;i<imagesPath.length;i++){
	                    imagesPathList.add(imagesPath[i]);
	                    yourbitmap = BitmapFactory.decodeFile(imagesPath[i]);
	                    ImageView imageView = new ImageView(this);
	                    imageView.setImageBitmap(yourbitmap);
	                    imageView.setAdjustViewBounds(true);
	                    lnrImages.addView(imageView);
	                }
	            }
	        }

	    }*/
	
	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
         progressDialog = new ProgressDialog(CreateRequestActivity.this);
         progressDialog.setMessage("Loading");
      
         progressDialog.setIndeterminate(false);
         progressDialog.setCancelable(false);
         progressDialog.setCanceledOnTouchOutside(false);
         progressDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
			Globals = ((MyGlobals)getApplication());
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    	
	    	userList = new ArrayList<HashMap<String, String>>();
	    	statusList = new ArrayList<HashMap<String, String>>();
	    	
	    	// load data from provider
    		List<cContact> uList = MyProvider.getContacts();
    		for(cContact entry : uList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                map.put("name", entry.name);
                map.put("username", entry.username);
              
                // adding HashList to ArrayList
                userList.add(map);
             }
    
    		List<cRequestStatus> sList = MyProvider.getRequestStatusList();
    		for(cRequestStatus entry : sList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                statusList.add(map);
             }
	
    			String id = "";
    			String name = "";
    			for(HashMap<String, String> user : userList){
    				
    				String usernameLogIn = myUser;
    				String username = user.get("username");
    				
    				if(usernameLogIn.equals(username)){
    					id = user.get("id");
    					name = user.get("name");
    					
    					idUserLogged = id;
    					
    					SharedPreferences.Editor pref = getSharedPreferences("PREF_ID_CONTACT", MODE_PRIVATE).edit();
    					pref.putString("id", id);
                    	pref.putString("name", name);
                    	pref.commit();
    					
    					System.out.println();
    				}
		
    			}
	   		
	    	//if (itemList.isEmpty())
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.error_network);
	         	
	    	}	
	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	progressDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	/*if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"
	    			 	            		   	 
	            	} else {*/
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
		            	
		            	// store the id of the item in a separate array
		             
		           	    List<String> userArray =  new ArrayList<String>();
		           	    List<String> statusArray =  new ArrayList<String>();
		           	    
		           	   
		           	    // load spinner data from dataprovider
		           	    for (HashMap<String, String> map : userList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") userkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
		           	        }
		           	    for (HashMap<String, String> map : statusList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") statusArray.add(entry.getValue());
		           	        }
		           		
	           	    	ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(CreateRequestActivity.this, android.R.layout.simple_spinner_item, userArray);
	           	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	Spinner sUser = (Spinner) findViewById(R.id.spinnertouser);
	          	        sUser.setAdapter(user_adapter);
	           	    
	           	    	final ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(CreateRequestActivity.this, android.R.layout.simple_spinner_item, statusArray);
	           	    	status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final Spinner sStatus = (Spinner) findViewById(R.id.spinnerStatus);
	           	        sStatus.setAdapter(status_adapter);
	           	    	
	           	    	
	           	    	String edit = Globals.getValue("edit");
	           	    	String solve = Globals.getValue("solve");
	           			String id = Globals.getValue("id");
	           			String touser = Globals.getValue("touser");
	           			String status = Globals.getValue("status");
	           			String description = Globals.getValue("description");
	           		    requestId = id;
	           		    
	           		    if(edit != ""){
	           		    	
/*	           		    	int spinnerPosition1 = user_adapter.getPosition(nameToUser);*/
	           		    	int spinnerPosition1 = user_adapter.getPosition(touser);
	           		    	//set the default according to value
	           		    	sUser.setSelection(spinnerPosition1);
	           		    	
	           		    	int spinnerPosition = status_adapter.getPosition(status);
				    		//set the default according to value
	           		    	sStatus.setSelection(spinnerPosition);
	           		    	
	           		    	EditText desc = (EditText) findViewById(R.id.textDescription);
	           		    	desc.setText(description);
	           		    	
	           		    }else if(solve != ""){
	           		    	
	           		    	int spinnerPosition = status_adapter.getPosition(status);
				    		//set the default according to value
	           		    	sStatus.setSelection(spinnerPosition);
	           		    	
	           		    	EditText desc = (EditText) findViewById(R.id.textDescription);
	           		    	desc.setText(description);
	           		    	
	           		    }
	           	       
	            	//} 	
	            	
	              }
	        });
	         
	    }

	}
	
	
	  /** AsyncTask register time record  */
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {
        	
        	  // extract data
		 
			  Spinner userSpinner = (Spinner) findViewById(R.id.spinnertouser);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myUserID = userkeyArray.get(myIndex);
			  			  			  
			  Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  myIndex = statusSpinner.getSelectedItemPosition();
			  String myStatus = statuskeyArray.get(myIndex);
			  			  
			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();	
			  
			  // call web method - Data Provider
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  // call web method - Robo Data Provider
			  //MyRoboProvider = new WSRoboDataProvider(myUser);
			  //MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_report", bitmapByteArray, bitmapByteArrayThumbnail);
			  }
			  
			  System.out.println("create request: " + idUserLogged + " " + myUserID + " " + myStatus + " " + description + " " + BitmapID);
			  
			  suc = true;
			  
			  if (suc)
			  {
				  suc = MyProvider.create_request(idUserLogged, myUserID, myStatus, description, "" + BitmapID);
			  }			  
		  
        	  return suc;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(IssueCreateActivity.this, getString(R.string.text_error), error_message);
	            		 Globals.setValue("create_request", "create_request");
	            		 finish();
		    		}else{
		    			Globals.setValue("create_request", "create_request");
		    			finish();
		    		}
	            	
	            	
	            		/*boolean d = duplicate;
						if(d == true){
							  Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
							  
						  }else{
							  Toast.makeText(getApplicationContext(), "Duplicate time", Toast.LENGTH_SHORT).show();
						  }*/
	            	
	            	         	   	
	              }
	        });
	         
	    }
    }
    
    
    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
    	 
    	int width = bm.getWidth();
    	 
    	int height = bm.getHeight();
    	 
    	float scaleWidth = ((float) newWidth) / width;
    	 
    	float scaleHeight = ((float) newHeight) / height;
    	 
    	// CREATE A MATRIX FOR THE MANIPULATION
    	 
    	Matrix matrix = new Matrix();
    	 
    	// RESIZE THE BIT MAP
    	 
    	matrix.postScale(scaleWidth, scaleHeight);
    	 
    	// RECREATE THE NEW BITMAP
    	 
    	Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    	 
    	return resizedBitmap;
    	 
    	}
    
	
	/** AsyncTask update request  */
	private class UpdateTaskRequest extends AsyncTask<String, String, Boolean>{
	
	    @Override
	    protected Boolean doInBackground(String... args) {	
			 
	    	// extract data
			 
			  Spinner userSpinner = (Spinner) findViewById(R.id.spinnertouser);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myUserID = userkeyArray.get(myIndex);
			  			  			  
			  Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  myIndex = statusSpinner.getSelectedItemPosition();
			  String myStatus = statuskeyArray.get(myIndex);
			  			  
			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();	
			  
			  // call web method
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			// call web method - Robo Data Provider
			 // MyRoboProvider = new WSRoboDataProvider(myUser);
			 // MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				 // Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  myBitmap = getResizedBitmap(myBitmap, 1, 1);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  
				  
				  BitmapID = MyProvider.createImage("image_report", bitmapByteArray, bitmapByteArrayThumbnail);
			  }
			  
			  System.out.println("update request: " + idUserLogged + " " + myUserID + " " + myStatus + " " + description + " " + BitmapID);
			  
			  suc = true;
			  
			  if (suc)
			  {
				  suc = MyProvider.editRequest(requestId, idUserLogged, myUserID, myStatus, description, "" + BitmapID);
			  }			  
		  
        	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	
	    	// updating UI from Background Thread
	    	
	    	runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(getContext(), getString(R.string.text_error), error_message);	
	            		 
	            		 Globals.setValue("update_request", "update_request");
	            		 Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
		    		}
					else
					{
						Globals.setValue("update_request", "update_request");
						Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
					}
 
				}
			});
	
	    }
	}
	
	
	
	
	/** AsyncTask update request solved  */
	private class UpdateTaskRequestSolved extends AsyncTask<String, String, Boolean>{
	
	    @Override
	    protected Boolean doInBackground(String... args) {	
			 
	    	// extract data
			  			  			  
			  Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  int myIndex = statusSpinner.getSelectedItemPosition();
			  String myStatus = statuskeyArray.get(myIndex);
			  			  
			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();
		      
		      Date d = new Date();
			  String datetime_solved = new SimpleDateFormat("yyyy-MM-dd").format(d);
			  
			  // call web method
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			// call web method - Robo Data Provider
			 // MyRoboProvider = new WSRoboDataProvider(myUser);
			//  MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  int BitmapID = 0;
			  boolean suc = false;
			  
			  if (myBitmap != null)
			  {
				  ByteArrayOutputStream stream = new ByteArrayOutputStream();
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArray = stream.toByteArray();
				  
				  byte[] bitmapByteArrayThumbnail;
				  bitmapByteArrayThumbnail = bitmapByteArray;
				  myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
				  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
				  bitmapByteArrayThumbnail = stream.toByteArray();
				  
				  BitmapID = MyProvider.createImage("image_report", bitmapByteArray, bitmapByteArrayThumbnail);
			  }
			  
			  
			  System.out.println("update request solved: " +  myStatus + " " + description + " " + datetime_solved + " " + BitmapID);
			  
			  suc = true;
			  
			  if (suc)
			  {
				  suc = MyProvider.solvedRequest(requestId, description, myStatus, datetime_solved, "" + BitmapID);
			  }
	
	    	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	
	    	// updating UI from Background Thread
	    	
	    	runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(getContext(), getString(R.string.text_error), error_message);	  
	            		 
	            		 Globals.setValue("update_request", "update_request");
	            		 Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
		    		}
					else
					{
						Globals.setValue("update_request", "update_request");
						Intent i = new Intent(CreateRequestActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
					}
 
				}
			});
	
	    }
	}
	
	
}
