package com.sharethatdata.digisupport;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.WSRoboDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cRequestList;
import com.sharethatdata.webservice.datamodel.cRequestStatus;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	WSRoboDataProvider MyRoboProvider = null;
	
	cRequestList myRequest;

	ArrayList<HashMap<String, String>> userList; // getContacts 
	
	private static ProgressDialog pDialog;
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";
	private String error_message = "";
	private String myName = "";
	private String requestId = "";
	private String description_done = "";
	private String status_done = "";
	
	private String fromUserId;
	private String toUserId;
	
	String myImageData = "";
	
	Button myButton1;
	Button myButton2;
	Button myButtonDelete;
	
	// For zoom image /////////////////////////
		// Hold a reference to the current animator,
	    // so that it can be canceled mid-way.
	    private Animator mCurrentAnimator;

	    // The system "short" animation time duration, in milliseconds. This
	    // duration is ideal for subtle animations or animations that occur
	    // very frequently.
	    private int mShortAnimationDuration;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_listview);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    MyRoboProvider = new WSRoboDataProvider(myUser, myPass);
	    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

	    String x = Globals.getValue("id");
	    requestId = x;
	    
	    new LoadUserList().execute();
	    
	    new LoadDataTask(x).execute();
	    
	    myButton1 = (Button) findViewById(R.id.button1);
	    myButton2 = (Button) findViewById(R.id.button2);
	    myButtonDelete = (Button) findViewById(R.id.buttonDelete);
	    final ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
	    
		imageView1.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Log.d("Request Detail Image", myImageData);
						String image_string = myImageData;
						if (TextUtils.isEmpty(image_string) || image_string.contains("null")){
							Log.d("Request Detail Image", myImageData);
						} else { 
							zoomImageFromThumb(imageView1, myImageData);
						}
					}
				});
		 
		 // Retrieve and cache the system's default "short" animation time.
	        mShortAnimationDuration = getResources().getInteger(
	                android.R.integer.config_shortAnimTime);
    
	    myButton1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					
					// if owner = edit
				if ( myName.equals(fromUserId) ) {
					//myButton1.setText("Edit");
					
					Globals.setValue("edit", "edit");
					Globals.setValue("id", String.valueOf(myRequest.id));
					Globals.setValue("touser", String.valueOf(myRequest.touser));
					Globals.setValue("status", String.valueOf(myRequest.status_name));
					Globals.setValue("description", String.valueOf(myRequest.description));
					Globals.setValue("myImageData", String.valueOf(myImageData));
					
					Intent intent = new Intent(DetailActivity.this, CreateRequestActivity.class);
					startActivity(intent);
				
					//  if assigned = solve
			} else if( myName.equals(toUserId) ) {
			    	//myButton1.setText("Solve");	
				
					Globals.setValue("solve", "solve");
					Globals.setValue("id", String.valueOf(myRequest.id));
					Globals.setValue("touser", String.valueOf(myRequest.touser));
					Globals.setValue("status", String.valueOf(myRequest.status_name));
					Globals.setValue("description", String.valueOf(myRequest.description));
					
					Intent intent = new Intent(DetailActivity.this, CreateRequestActivity.class);
					startActivity(intent);
			    }
					// if not owner = take
				 else if ( !myName.equals(fromUserId)  ) {
				    	//myButton1.setText("Take");
				    	
				 }	 else {

					}
				
			}
		});
	    
	    myButton2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			//  if assigned = done
				if( myName.equals(toUserId) ) {
				    	//myButton1.setText("Done");	
					
					requestId = String.valueOf(myRequest.id);
					status_done = String.valueOf(myRequest.status_name);
					description_done = String.valueOf(myRequest.description);
						
					new UpdateTaskRequestSolved().execute();
				    }
		
			}
		}); 
		
	    myButtonDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// if owner = delete
				if ( myName.equals(fromUserId) ) {
					//myButton1.setText("Delete");
					
					new DeleteRequestTask(requestId).execute();
			}
				
				
			}
		});
	    
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    Log.d(this.getClass().getName(), "back button pressed");
	    Globals.setValue("edit", "");
	    Globals.setValue("solve", "");
	}
	return super.onKeyDown(keyCode, event);
	}
	
	// when get and show the image
	private Bitmap ConvertByteArrayToBitmap(String b)
	{
		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		return decodedByte;		
	}
	
	private class LoadDataTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  /**
		      * Before starting background thread Show Progress Dialog
		      * */
		     @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		      // Showing progress dialog
		         pDialog = new ProgressDialog(DetailActivity.this);
		         pDialog.setTitle("Loading Request");
		         pDialog.setMessage("Please wait...");
		         pDialog.setIndeterminate(false);
		         pDialog.setCancelable(true);
		         pDialog.setCanceledOnTouchOutside(false);
		         pDialog.show();
		     }
		  
		  public LoadDataTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  
			  myRequest = MyProvider.getDetailRequest(myID);
			  if (myRequest != null)
			  {
				  if (myRequest.image_id != "0")
				  {
					  String aaaa = myRequest.image_id;
					  myImageData = MyRoboProvider.getImage(myRequest.image_id);
				  }
			  }
			  
			  String bbb = myImageData;
			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  if (myRequest != null)
			  {
				
				  TextView dateView = (TextView) findViewById(R.id.textViewDateTime);
				  dateView.setText(myRequest.datetime);
				  TextView statusView = (TextView) findViewById(R.id.textViewStatus);
				  statusView.setText(myRequest.status_name);
				  TextView descriptionView = (TextView) findViewById(R.id.textViewDescription);
				  descriptionView.setText(myRequest.description);
				  
				  ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
				  
				  Bitmap d = ConvertByteArrayToBitmap(myImageData);
				  if (d != null)
				  {
					  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
					  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
					  imageView1.setImageBitmap(scaled);
					  				  
				  }
				  
				  fromUserId = myRequest.fromuser ;
				  toUserId = myRequest.touser ;
				  
				// if owner && if assigned 
			 if( myName.equals(fromUserId) && myName.equals(toUserId)) {
						myButton1.setText("Edit");
				    	//myButton2.setText("Solve");
						myButton2.setVisibility(View.GONE);
					}
					 // if owner
			 else if ( myName.equals(fromUserId)  ) {
						myButton1.setText("Edit");
						myButton2.setVisibility(View.GONE);
					
						//  if assigned 
				} else if( myName.equals(toUserId) ) {
				    	myButton1.setText("Solve");	
				    	myButton2.setText("Done");
				    	myButtonDelete.setVisibility(View.GONE);
				    }
							// if not owner
						 else if ( !myName.equals(fromUserId) ) {
						    	myButton1.setText("Take");
						    	myButton2.setVisibility(View.GONE);
						    	myButtonDelete.setVisibility(View.GONE);
						    	
						 }	 else {
	
							}

			  }
			  
			  // Dismiss the progress dialog
	            if (pDialog.isShowing())
	                pDialog.dismiss();
		  }
	}
	
	
    /**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider = new WSDataProvider(myUser, myPass);
		
		Globals = ((MyGlobals)getApplication());
		
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		if (report_user == "") 
		{
			userList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider.getContacts();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("username", entry.username);
                map.put("name", entry.name);
                
                String usernameLogIn = myUser;
				String username = entry.username;
                
                if(usernameLogIn.equals(username)){
                	String id = entry.id;
					String name = entry.name;
					myName = name;
					
					SharedPreferences.Editor pref = getSharedPreferences("PREF_ID_CONTACT", MODE_PRIVATE).edit();
					pref.putString("name", name);
					pref.putString("id", id);
                	pref.commit();
				}
          
                // adding HashList to ArrayList
                userList.add(map);
             }
			
			report_user = myUser;
		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
       runOnUiThread(new Runnable() {
           public void run() {
           
        	   	
        	   
           }
       	});
 	      
    	}
	}

	
	/** AsyncTask update status sick day  */
	private class UpdateTaskRequestSolved extends AsyncTask<String, String, Boolean>{
	
	    @Override
	    protected Boolean doInBackground(String... args) {	
			 
	    	// extract data
		      
		      Date d = new Date();
			  String datetime_solved = new SimpleDateFormat("yyyy-MM-dd").format(d);
			  
	    		List<cRequestStatus> sList = MyProvider.getRequestStatusList();
	    		for(cRequestStatus entry : sList)
	         	{
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();

	                map.put("id",  Long.toString(entry.id));
	                map.put("name", entry.name);
	                
	                if(entry.name.contains("Done")){
	                	status_done = Long.toString(entry.id);
	                }

	             }
			  
			  // call web method
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			// call web method - Robo Data Provider
			 // MyRoboProvider = new WSRoboDataProvider(myUser);
			//  MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  int BitmapID = 0;
			  boolean suc = false;

				  suc = MyProvider.solvedRequest(requestId, description_done, status_done, datetime_solved, "" + BitmapID);		  
	
	    	  return suc;
	    }
	       
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(final Boolean success) {
	    	
	    	// updating UI from Background Thread
	    	
	    	runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(getContext(), getString(R.string.text_error), error_message);	  
	            		 Intent i = new Intent(DetailActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
		    		}
					else
					{
						Intent i = new Intent(DetailActivity.this, DashBoardActivity.class);
	            		 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            		 startActivity(i);
					}
 
				}
			});
	
	    }
	}
	
	private class DeleteRequestTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public DeleteRequestTask(String id) {
		     myID = id;
		  }

		  protected Boolean doInBackground(String... urls) {
			  			  
			  boolean suc = false;
			  
				suc = MyProvider.deleteRequest(myID);
			  
			  return suc;
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Request deleted", Toast.LENGTH_SHORT).show();
				  Globals.setValue("request_delete", "request_delete");
				  finish();
				  
			  }else{
				  Toast.makeText(getApplicationContext(), "Was not deleted the request", Toast.LENGTH_SHORT).show();
				  Globals.setValue("request_delete", "request_delete");
				  finish();
			  }
			  
			  
		  }
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
public String resizeBase64Image(String base64image){
	int IMG_WIDTH = 800;
	int IMG_HEIGHT = 600;
	byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT); 
	BitmapFactory.Options options=new BitmapFactory.Options();
	options.inPurgeable = true;
	Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
	
	
	if(image.getHeight() <= 400 && image.getWidth() <= 400){
		return base64image;
	}
	image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);
	
	ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	image.compress(Bitmap.CompressFormat.PNG,100, baos);
	
	byte [] b=baos.toByteArray();
	System.gc();
	return Base64.encodeToString(b, Base64.NO_WRAP);

} 

private void zoomImageFromThumb(final View thumbView, String imageResId) {
	// If there's an animation in progress, cancel it
	// immediately and proceed with this one.
	if (mCurrentAnimator != null) {
		mCurrentAnimator.cancel();
	}
	
	// Load the high-resolution "zoomed-in" image.
	final ImageView expandedImageView = (ImageView) findViewById(R.id.expanded_image_project);
	//
	byte[] decodedString;
	if(imageResId != ""){
		String resizeString =  resizeBase64Image(imageResId);
		decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
	}else{
		decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
	}
	
	
	Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
	if (decodedByte != null)
	{
		//int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
		//Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
		expandedImageView.setImageBitmap(decodedByte);
	
	}
	
	
	
	// Calculate the starting and ending bounds for the zoomed-in image.
	// This step involves lots of math. Yay, math.
	final Rect startBounds = new Rect();
	final Rect finalBounds = new Rect();
	final Point globalOffset = new Point();
	
	// The start bounds are the global visible rectangle of the thumbnail,
	// and the final bounds are the global visible rectangle of the container
	// view. Also set the container view's offset as the origin for the
	// bounds, since that's the origin for the positioning animation
	// properties (X, Y).
	thumbView.getGlobalVisibleRect(startBounds);
	findViewById(R.id.scrollViewProjects)// the container
	.getGlobalVisibleRect(finalBounds, globalOffset);
	startBounds.offset(-globalOffset.x, -globalOffset.y);
	finalBounds.offset(-globalOffset.x, -globalOffset.y);
	
	// Adjust the start bounds to be the same aspect ratio as the final
	// bounds using the "center crop" technique. This prevents undesirable
	// stretching during the animation. Also calculate the start scaling
	// factor (the end scaling factor is always 1.0).
	float startScale;
	if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds.width() / startBounds.height()) 
	{
		// Extend start bounds horizontally
		startScale = (float) startBounds.height() / finalBounds.height();
		float startWidth = startScale * finalBounds.width();
		float deltaWidth = (startWidth - startBounds.width()) / 2;
		startBounds.left -= deltaWidth;
		startBounds.right += deltaWidth;
	} else {
		// Extend start bounds vertically
		startScale = (float) startBounds.width() / finalBounds.width();
		float startHeight = startScale * finalBounds.height();
		float deltaHeight = (startHeight - startBounds.height()) / 2;
		startBounds.top -= deltaHeight;
		startBounds.bottom += deltaHeight;
	}
	
	// Hide the thumbnail and show the zoomed-in view. When the animation
	// begins, it will position the zoomed-in view in the place of the
	// thumbnail.
	thumbView.setAlpha(0f);
	expandedImageView.setVisibility(View.VISIBLE);
	
	// Set the pivot point for SCALE_X and SCALE_Y transformations
	// to the top-left corner of the zoomed-in view (the default
	// is the center of the view).
	expandedImageView.setPivotX(0f);
	expandedImageView.setPivotY(0f);
	
	// Construct and run the parallel animation of the four translation and
	// scale properties (X, Y, SCALE_X, and SCALE_Y).
	AnimatorSet set = new AnimatorSet();
	set
	.play(ObjectAnimator.ofFloat(expandedImageView, View.X,
	startBounds.left, finalBounds.left))
	.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
	startBounds.top, finalBounds.top))
	.with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
	startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
	View.SCALE_Y, startScale, 1f));
	set.setDuration(mShortAnimationDuration);
	set.setInterpolator(new DecelerateInterpolator());
	set.addListener(new AnimatorListenerAdapter() {
	@Override
	public void onAnimationEnd(Animator animation) {
		mCurrentAnimator = null;
	}
	
	@Override
	public void onAnimationCancel(Animator animation) {
		mCurrentAnimator = null;
	}
	});
		set.start();
		mCurrentAnimator = set;
	
	// Upon clicking the zoomed-in image, it should zoom back down
	// to the original bounds and show the thumbnail instead of
	// the expanded image.
	final float startScaleFinal = startScale;
		expandedImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
			if (mCurrentAnimator != null) {
				mCurrentAnimator.cancel();
			}
			
			// Animate the four positioning/sizing properties in parallel,
			// back to their original values.
			AnimatorSet set = new AnimatorSet();
			set.play(ObjectAnimator
			.ofFloat(expandedImageView, View.X, startBounds.left))
			.with(ObjectAnimator
			.ofFloat(expandedImageView, 
			View.Y,startBounds.top))
			.with(ObjectAnimator
			.ofFloat(expandedImageView, 
			View.SCALE_X, startScaleFinal))
			.with(ObjectAnimator
			.ofFloat(expandedImageView, 
			View.SCALE_Y, startScaleFinal));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				thumbView.setAlpha(1f);
				expandedImageView.setVisibility(View.GONE);
				mCurrentAnimator = null;
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
					thumbView.setAlpha(1f);
					expandedImageView.setVisibility(View.GONE);
					mCurrentAnimator = null;
				}
			});
			set.start();
			mCurrentAnimator = set;
		}
	});
}

}
