package com.sharethatdata.digisupport;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

public class MyRequestsActivity extends FragmentActivity implements ActionBar.TabListener{
	
	private ViewPager viewPager;
    private MyRequestsTabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    // Tab titles
    private String[] tabs = { "My sent", "My assigned" };
    
    private MyRequestsTabsPagerAdapter mSectionsPagerAdapter; 
 
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request);
        
        mSectionsPagerAdapter = new MyRequestsTabsPagerAdapter(getSupportFragmentManager());

 		mSectionsPagerAdapter.notifyDataSetChanged();
 		viewPager = (ViewPager) findViewById(R.id.pager);  
 		
 		viewPager.setAdapter(mSectionsPagerAdapter);
 
        // Initilization
        actionBar = getActionBar();
        mAdapter = new MyRequestsTabsPagerAdapter(getSupportFragmentManager());
 
        viewPager.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);        
 
        // Adding Tabs
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
 
        /**
         * on swiping the viewpager make respective tab selected
         * */
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
 
            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);
                
                switch (position) {
       	        case 0:
       	        	//Toast.makeText(getActivity(), "Inbox", Toast.LENGTH_SHORT).show();
       	        	RequestsSentFragment fragmentI = (RequestsSentFragment) mSectionsPagerAdapter.instantiateItem(viewPager, 0);
                    /*if (fragmentI != null) {
                   	 fragmentI.myRefresh();
                    }*/
       	            break;
       	        case 1: {
       	            break;
       	        	}
       	        }
            }
 
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
 
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

    }
    


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
        // show respected fragment view
		 viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	
	}
 
	void showToast(String msg) {
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}