package com.sharethatdata.digisupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digisupport.RequestsSentFragment.LoadListMyRequests;
import com.sharethatdata.digisupport.adapter.MyRequestAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cRequestMy;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import androidx.fragment.app.Fragment;

public class RequestAssignedFragment extends Fragment {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList;
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";
	private String error_message = "";
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_tabs, container, false);
		
		Globals = ((MyGlobals)getActivity().getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass); 
        
        lv = (ListView) rootView.findViewById(R.id.listView);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getActivity().getApplication());
			    Globals.setValue("id", pid);
				
			    // start detail activity
				Intent intent = new Intent(getActivity().getApplicationContext(), DetailActivity.class); startActivity(intent); 
				
			}

		});	
        
         new LoadListMyRequests().execute();
		
				return rootView;
	
	}
	
	   @Override
		public void onResume(){
			super.onResume();
			Log.d("TAG", "onResume Message Assigned"); 
			new LoadListMyRequests().execute();
		}
	
	
	/**
	 * Background Async Task to Load my assigned request by making HTTP Request
  	 * */
	class LoadListMyRequests extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
   /*      pDialog = new ProgressDialog(ListActivity.this);
         pDialog.setMessage( getString(R.string.loading_alert_dialog) + ".");
         //"Loading " // "Please wait..."
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();*/
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getActivity().getApplicationContext());	
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cRequestMy> sickdays = MyProvider.getMyRequestAssignedList();
	    	 for(cRequestMy entry : sickdays)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("fromuser",  String.valueOf(entry.fromuser));
                map.put("fromuser_name_surname", entry.fromuser_name_surname);
                map.put("touser",  String.valueOf(entry.touser));
                map.put("touser_name_surname", String.valueOf(entry.touser_name_surname));
                map.put("description", entry.description);
                map.put("datetime", entry.datetime);
                map.put("status_name", entry.status_name);
                 
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	    	getActivity().runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	MyRequestAdapter adapter = null;
	            	
	            	/*adapter = new SimpleAdapter(
		                		getActivity()
		                		, itemList
		                		, R.layout.fragment_tabs_listview_items, new String[] { "id", "fromuser_name_surname", "touser_name_surname", "status_name", "description" },
	                            new int[] { R.id.textViewID, R.id.textViewFromUser, R.id.textViewToUser, R.id.textViewStatus, R.id.textViewDescription });
	        			lv.setAdapter(adapter);*/
	            	
	            	adapter = new MyRequestAdapter(getActivity(), R.layout.fragment_tabs_listview_items, itemList);
	            	lv.setAdapter(adapter);
	        		
	            
	                // updating listview
	        		
	               	 
	            }
	        });
	  	      
	    }
	  

	}
	

}
