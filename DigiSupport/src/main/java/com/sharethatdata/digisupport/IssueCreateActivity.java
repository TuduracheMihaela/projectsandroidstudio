package com.sharethatdata.digisupport;/*package com.sharethatdata.digisupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digisupport.R.layout;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cActivity;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cCostplace;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cProject;
import com.sharethatdata.webservice.datamodel.cTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class IssueCreateActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> userList;
	ArrayList<HashMap<String, String>> statusList;
	
	List<String> userkeyArray =  new ArrayList<String>();
	List<String> statuskeyArray =  new ArrayList<String>();
	
	private String myUser = "";
	private String report_user = "";
	private boolean no_network = false;
	private String error_message = "";
	private boolean input_correct = false;
	
	Spinner spinner = null;
	ProgressDialog progressDialog;
	
	String idUserLogged;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.issue_create);
		
		Globals = ((MyGlobals) getApplicationContext());	
		myUser = Globals.getValue("user");
		
		Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		
		LoadList InitList;
		InitList = new LoadList();
		InitList.execute();
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
				
				  input_correct = IsInputCorrect();
				  
				  if (input_correct)
				  {
					  RegisterTask register;
					  register = new RegisterTask();
					  register.execute();
					 // Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
				  }
		
			  }
		 
		});
		
		
	}
	
	
	private boolean IsInputCorrect()
	{
		// fields filled
		boolean fields_filled = true;
			
		// user selected
		return fields_filled;
		
	}
	
	
	*//**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * *//*
	class LoadList extends AsyncTask<String, String, String> {

     *//**
      * Before starting background thread Show Progress Dialog
      * *//*
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
         progressDialog = new ProgressDialog(IssueCreateActivity.this);
         progressDialog.setMessage("Loading");
      
         progressDialog.setIndeterminate(false);
         progressDialog.setCancelable(false);
         progressDialog.show();
     }

	    *//**
	     * getting items from url
	     * *//*
	    protected String doInBackground(String... args) {
	    	
	    	MyProvider = new WSDataProvider(myUser);
			Globals = ((MyGlobals)getApplication());
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    	
	    	userList = new ArrayList<HashMap<String, String>>();
	    	statusList = new ArrayList<HashMap<String, String>>();
	    	
	    	// load data from provider
    		List<cContact> uList = MyProvider.getContacts();
    		for(cContact entry : uList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                map.put("name", entry.name);
                map.put("username", entry.username);
              
                // adding HashList to ArrayList
                userList.add(map);
             }
    
    		List<cIssueStatus> sList = MyProvider.getIssueStatusList();
    		for(cIssueStatus entry : sList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  Long.toString(entry.id));
                map.put("name", entry.name);
              
                // adding HashList to ArrayList
                statusList.add(map);
             }
	
    			String id = "";
    			String name = "";
    			for(HashMap<String, String> user : userList){
    				
    				String usernameLogIn = myUser;
    				String username = user.get("username");
    				
    				if(usernameLogIn.equals(username)){
    					id = user.get("id");
    					name = user.get("name");
    					
    					idUserLogged = id;
    					
    					SharedPreferences.Editor pref = getSharedPreferences("PREF_NAME_CONTACT", MODE_PRIVATE).edit();
    					pref.putString("id", id);
                    	pref.putString("name", name);
                    	pref.commit();
    					
    					System.out.println();
    				}
    			}
	   		
	    	//if (itemList.isEmpty())
	    	if (MyProvider.json_connected == false)
	    	{
	        	no_network = true;
	         	error_message = getString(R.string.error_network);
	         	
	    	}	
	    	return "";
	    }
	
	    *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
	    protected void onPostExecute(String file_url) {
	    	progressDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            	
	            	if (no_network == true)
	            	{
	            	    UIHelper myUIHelper = new UIHelper();
	    			    
	    			    myUIHelper.ShowDialog(RecordTimeActivity.this,getString(R.string.text_error), error_message);
	    			    //  "Error"
	    			 	            		   	 
	            	} else {
		            
		         	   *//**
		                 * Updating parsed JSON data into ListView
		                 * *//*
		            	
		            	// store the id of the item in a separate array
		             
		           	    List<String> userArray =  new ArrayList<String>();
		           	    List<String> statusArray =  new ArrayList<String>();
		           	    
		           	   
		           	    // load spinner data from dataprovider
		           	    for (HashMap<String, String> map : userList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") userkeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
		           	        }
		           	    for (HashMap<String, String> map : statusList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") statuskeyArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") statusArray.add(entry.getValue());
		           	        }
		           		
	           	    	ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(RequestCreateActivity.this, android.R.layout.simple_spinner_item, userArray);
	           	    	user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	Spinner sLocations = (Spinner) findViewById(R.id.spinnertouser);
	           	       	sLocations.setAdapter(user_adapter);
	           	    
	           	    	final ArrayAdapter<String> status_adapter = new ArrayAdapter<String>(RequestCreateActivity.this, android.R.layout.simple_spinner_item, statusArray);
	           	    	status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           	       	final Spinner sProjects = (Spinner) findViewById(R.id.spinnerStatus);
	           	    	sProjects.setAdapter(status_adapter);
	           	       
	            	//} 	
	            	
	              }
	        });
	         
	    }

	}
	
	
	  *//** AsyncTask register time record  *//*
    private class RegisterTask extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {
        	
        	  // extract data
		 
			  Spinner userSpinner = (Spinner) findViewById(R.id.spinnertouser);
			  int myIndex = userSpinner.getSelectedItemPosition();
			  String myUserID = userkeyArray.get(myIndex);
			  			  			  
			  Spinner statusSpinner = (Spinner) findViewById(R.id.spinnerStatus);
			  myIndex = statusSpinner.getSelectedItemPosition();
			  String myStatus = statuskeyArray.get(myIndex);
			  			  
			  TextView DescriptionView = (TextView) findViewById(R.id.textDescription);
		      String description = DescriptionView.getText().toString();	
			  
			  // call web method
			  MyProvider = new WSDataProvider(myUser);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  System.out.println("create issue: " + idUserLogged + " " + myUserID + " " + myStatus + " " + description);
			  
			  boolean suc = false;

				   suc = MyProvider.create_issue(idUserLogged, myUserID, myStatus, description);
		  
        	  return suc;
        }
           
        *//**
	     * After completing background task Dismiss the progress dialog
	     * **//*
        protected void onPostExecute(final Boolean success) {
        	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 error_message = MyProvider.last_error;
	            		  
	            		 UIHelper myUIHelper = new UIHelper();
		    			    
		    			 //myUIHelper.ShowDialog(IssueCreateActivity.this, getString(R.string.text_error), error_message);
	            		 finish();
		    		}else{
		    			finish();
		    		}
	            	
	            	
	            		boolean d = duplicate;
						if(d == true){
							  Intent intent = new Intent(getApplicationContext(), ListActivity.class); startActivity(intent); 
							  
						  }else{
							  Toast.makeText(getApplicationContext(), "Duplicate time", Toast.LENGTH_SHORT).show();
						  }
	            	
	            	         	   	
	              }
	        });
	         
	    }
    }
    
    
	
}
*/