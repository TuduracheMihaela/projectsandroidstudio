package com.sharethatdata.digisupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cFaq;
import com.sharethatdata.webservice.datamodel.cRequestList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FaqActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList; 
	
	ListView lv = null;
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq);
		
		Globals = ((MyGlobals) getApplicationContext());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		 lv = (ListView) findViewById(R.id.listView);
		 
		 lv.setOnItemClickListener(new OnItemClickListener() {
				
				@Override
				public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
				{
					HashMap<String, String> itemsList = (HashMap<String, String>) itemList.get(position);
					 String qid = (String) itemsList.get("id");
					 String question = (String) itemsList.get("question");
					 String solution = (String) itemsList.get("solution");
				    
				    // start detail activity
					Intent intent = new Intent(getApplicationContext(), FaqDetailActivity.class);  
					intent.putExtra("qid", qid);
					intent.putExtra("question", question);
					intent.putExtra("solution", solution);
					startActivity(intent);
					
					
				}

			});	
		 
		 new LoadListFaq().execute();
	}
	
	
	/**
	 * Background Async Task to Load faq by making HTTP Request
  	 * */
	class LoadListFaq extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cFaq> myFaq = MyProvider.getFaqList();
	    	 for(cFaq entry : myFaq)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("question",  String.valueOf(entry.question));
                map.put("solution", String.valueOf(entry.solution));
                
                if(entry.question.contains("How to install an application?")){
                	map.put("title", "FAQ");
                }
                
                if(entry.question.contains("I can not login the website or application")){
                	map.put("title", "ISSUES");
                }
                
                if(entry.question.contains("What devices are supported?")){
                	map.put("title", "OTHERS");
                }
                 
                // adding HashList to ArrayList
                itemList.add(map);
	 
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	    	runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	/*SimpleAdapter adapter = null;
	            	
	            	adapter = new SimpleAdapter(
		                		FaqActivity.this
		                		, itemList
		                		, R.layout.faq_listview_items, new String[] { "id", "question", "solution"},
	                            new int[] { R.id.textViewID, R.id.textViewQuestion, R.id.textViewSolution});
	            	
	            	SimpleAdapter.ViewBinder binder = new SimpleAdapter.ViewBinder() {
	            	    @Override
	            	    public boolean setViewValue(View view, Object object, String value) {
	            	        if (view instanceof TextView) {
	            	            ((TextView) view).setText(Html.fromHtml(value));
	            	            ((TextView) view).setAutoLinkMask(Linkify.WEB_URLS);;
	            	            return true;
	            	        }

	            	        return false;
	            	    }
	            	};
	            	
	            	adapter.setViewBinder(binder);
	        			lv.setAdapter(adapter);*/
	            	
	            	lv.setAdapter(new FaqAdapter(FaqActivity.this, R.layout.faq_listview_items, itemList));
	        		
	            
	                // updating listview
	        		
	               	 
	            }
	        });
	  	      
	    }
	  

	}

}


