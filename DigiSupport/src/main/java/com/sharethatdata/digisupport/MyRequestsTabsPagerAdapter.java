package com.sharethatdata.digisupport;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import  androidx.viewpager2.widget.ViewPager2;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MyRequestsTabsPagerAdapter extends FragmentPagerAdapter {
 
    public MyRequestsTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Holidays List fragment activity
            return new RequestsSentFragment();
        case 1:
            // Holiday Request fragment activity
            return new RequestAssignedFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
 
}
