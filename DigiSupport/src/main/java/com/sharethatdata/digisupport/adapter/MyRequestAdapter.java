package com.sharethatdata.digisupport.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digisupport.R;
import com.sharethatdata.webservice.UIHelper;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MyRequestAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	protected ListView mListView;

 	 public MyRequestAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textViewID);
        holder.user = (TextView)view.findViewById(R.id.textViewFromUser);
        holder.touser = (TextView)view.findViewById(R.id.textViewToUser);        
        holder.status_name = (TextView)view.findViewById(R.id.textViewStatus);
        holder.description = (TextView)view.findViewById(R.id.textViewDescription);
        holder.content = (LinearLayout) view.findViewById(R.id.layoutRequests);
      
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.user.setText(Html.fromHtml(hashmap_Current.get("fromuser_name_surname")));      
     holder.touser.setText(Html.fromHtml(hashmap_Current.get("touser_name_surname")));       
     holder.status_name.setText(Html.fromHtml(hashmap_Current.get("status_name")));       
     holder.description.setText(Html.fromHtml(hashmap_Current.get("description")));

     String myRow = "";
     for (Entry<String, String> entry : hashmap_Current.entrySet())
        {
        	if (entry.getKey() == "status_name")
        	{
        		
        		try {
        			myRow = entry.getValue().toString();
        			String row = String.valueOf(myRow);
        			
        			if(row.contains("Open")){
        				holder.content.setBackgroundResource(R.color.green1);
        			}
     	        if(row.contains("In Progress")){
     	        	holder.content.setBackgroundResource(R.color.yellow2);
     	       	}
        			
        		} catch(NumberFormatException nfe) {

        		}

        	}
        }
     
    return view; 
}

	final class ViewHolder {
	    public TextView id, user, touser, status_name, description;
	    public LinearLayout content;

	}

}

