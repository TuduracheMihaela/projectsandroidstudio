package com.sharethatdata.digisupport;

import java.text.SimpleDateFormat;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cReportsList;
import com.sharethatdata.webservice.datamodel.cRequestList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ReportsDetailActivity extends Activity {
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	cReportsList myReport;
	
	private String myUser = "";
	private String myPass = "";
	private String requestId = "";
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_listview);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    String x = Globals.getValue("id");
	    requestId = x;
	    
	    new LoadDataTask(x).execute();
		
	}
	
	private class LoadDataTask extends AsyncTask<String, String, String> {
		  String myID = "";
		  
		  /**
		      * Before starting background thread Show Progress Dialog
		      * */
		     @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		     }
		  
		  public LoadDataTask(String id) {
		     myID = id;
		  }

		  protected String doInBackground(String... urls) {
			  
			  myReport = MyProvider.getDetailReport(myID);

			  return "";
		  }

		  protected void onPostExecute(String result) {
			  
			  if (myReport != null)
			  {
				
				  TextView dateView = (TextView) findViewById(R.id.textViewDateTime);
				  String[] token = myReport.date.split("\\s");
	                String date = token[0];
	                dateView.setText(date);
				  TextView statusView = (TextView) findViewById(R.id.textViewStatus);
				  statusView.setText(myReport.name);
				  TextView descriptionView = (TextView) findViewById(R.id.textViewDescription);
				  descriptionView.setText(myReport.description);

			  }

		  }
	}

}
