package com.sharethatdata.digisupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.webservice.WSDataProvider;

public class FaqAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	protected ListView mListView;

 	 public FaqAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textviewID);
        holder.question = (TextView)view.findViewById(R.id.textViewQuestion);
        holder.solution = (TextView)view.findViewById(R.id.textViewSolution);        
        holder.title = (TextView)view.findViewById(R.id.textviewTitle);
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.question.setText(Html.fromHtml(hashmap_Current.get("question")));      
     holder.solution.setText(Html.fromHtml(hashmap_Current.get("solution")));  

     int myRow = 0;
     for (Entry<String, String> entry : hashmap_Current.entrySet())
        {
        	if (entry.getKey() == "title")
        	{
        		
        		try {
        			
        			holder.title.setVisibility(View.VISIBLE);
        			holder.title.setText(Html.fromHtml(hashmap_Current.get("title"))); 
        			
        			/*if(row.contains("0")){
        				// if was not approved
     	        	System.out.println("in if 0: " + myRow);
     	        	holder.title.setVisibility(View.VISIBLE);
        			}
     	        if(row.contains("1")){
     	        		// if was approved
     	        	System.out.println("in if 1: " + myRow);
     	        	holder.title.setVisibility(View.INVISIBLE);
     	       	}*/
        			
        		} catch(NumberFormatException nfe) {

        		}

        	}
        }

    return view; 
}

	final class ViewHolder {
	    public TextView id, question, solution, title;
	    public LinearLayout content;
	}
}
