package com.sharethatdata.digisupport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cReportsList;
import com.sharethatdata.webservice.datamodel.cRequestList;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ReportsActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> itemList; 
	ArrayList<HashMap<String, String>> userList;
	
	private int mStartYear,mStartMonth,mStartDay;
	private int mEndYear,mEndMonth,mEndDay;
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";
	private String report_user_id = "";
	
	ListView lv = null;
	TextView startDateView;
	TextView endDateView;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		
		Globals = ((MyGlobals) getApplicationContext());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		lv = (ListView) findViewById(R.id.listView);
		startDateView = (TextView) findViewById(R.id.dateStartText);
        endDateView = (TextView) findViewById(R.id.dateEndText);
        
     // set calendar
		Calendar c = Calendar.getInstance();
	    mStartYear = c.get(Calendar.YEAR);
	    mStartMonth = c.get(Calendar.MONTH);
	    mStartDay = 1;
	    
	    mEndYear = c.get(Calendar.YEAR);
	    mEndMonth = c.get(Calendar.MONTH);
	    mEndDay = c.get(Calendar.DAY_OF_MONTH);
	    
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
        endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("/").append(pad(mEndMonth + 1)).append("/").append(pad(mEndYear)));
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("id", pid);
				
			    // start detail activity
				Intent intent = new Intent(getApplicationContext(), ReportsDetailActivity.class); startActivity(intent); 
				
			}

		});	
		
        LoadReports();
        
	}
	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	private void LoadReports()
	{

   	  	String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();
	 	  
		Globals.setValue("report_date_start", myDateTime);
		Globals.setValue("report_date_end", myDateTimeEnd);
			
		LoadListReports RefreshList;
		RefreshList = new LoadListReports();
		RefreshList.execute();
		
	}
	
	private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
			  
			    mStartYear = year;
	            mStartMonth = month;
	            mStartDay = day; 
	            		
	          startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("/")
		                .append(pad(mStartMonth + 1)).append("/")
		                .append(pad(mStartYear)));
	                
		  
	          LoadReports();
	          
			  
		  }

	};
	
	private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
			  
			    mEndYear = year;
	            mEndMonth = month;
	            mEndDay = day; 
	            		
	          endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("/")
		                .append(pad(mEndMonth + 1)).append("/")
		                .append(pad(mEndYear)));
	          
	          LoadReports();
	          
			  
		  }

	};
	
	public void onStartDayClick(View v) 
	{	
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
	    dp1.show();
	    
	}
	
	public void onEndDayClick(View v){
		DatePickerDialog dp2 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp2.setTitle(sdf.format(d));
		dp2.show();
	}
	
	/**
	 * Background Async Task to Load my sent request by making HTTP Request
  	 * */
	class LoadListReports extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
        /* pDialog = new ProgressDialog(DashBoardActivity.this);
         pDialog.setTitle("Loading requests");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();*/
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
		    //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
			
			SharedPreferences pref = getSharedPreferences("PREF_ID_CONTACT", 0); 
	        String id = pref.getString("id", "");
	        
	        if(report_user_id == ""){
	        	report_user_id = id;
	        }
			
	        Globals = ((MyGlobals)getApplication());
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    
			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cReportsList> myReport = MyProvider.getReportsList(report_date_start, report_date_end, "", "");
	    	 for(cReportsList entry : myReport)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("userid",  String.valueOf(entry.userid));
                map.put("user", String.valueOf(entry.user));
                map.put("projectid", String.valueOf(entry.projectid));
                map.put("project",  String.valueOf(entry.project));
                
                String[] token = entry.date.split("\\s");
                String date = token[0];
                map.put("date", String.valueOf(date));
                
                map.put("starttime", String.valueOf(entry.starttime));
                map.put("name", String.valueOf(entry.name));
                map.put("email",  String.valueOf(entry.email));
                map.put("description", String.valueOf(entry.description));
                 
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	    	runOnUiThread(new Runnable() {
	            public void run() {
	            	//pDialog.dismiss();
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;
	            	
	            	adapter = new SimpleAdapter(
		                		ReportsActivity.this
		                		, itemList
		                		, R.layout.report_listview_items, new String[] { "id", "user", "project", "name", "description", "date" },
	                            new int[] { R.id.textViewID, R.id.textViewFromUser, R.id.textViewToUser, R.id.textViewStatus, R.id.textViewDescription, R.id.textViewDatetime });
	        			lv.setAdapter(adapter);
	        		
	            
	                // updating listview
	        		
	        			/*if (pDialog.isShowing())
			                pDialog.dismiss();*/
	            }
	        });
	  	      
	    }
	  

	}

}
