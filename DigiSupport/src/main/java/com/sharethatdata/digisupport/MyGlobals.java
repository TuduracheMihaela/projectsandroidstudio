package com.sharethatdata.digisupport;

import android.app.Application;
import android.content.Context;

public class MyGlobals extends com.sharethatdata.crash.report.MyCrashReport {

	 private static Context mContext;
	
	  public class myValue {
		  public String name;
		  public String value;
		  
		  public myValue()
		  {
			name = "";
			value = "";
		  }
	  }
		
	  private myValue[] myValues;
	  private int numberOfValues = 0;
	  
	  
	  public MyGlobals()
	  {
		  myValues = new myValue[100];
		  
		  this.mContext = this;
	  }
	  

	  public static Context getContext(){
	        return mContext;
	  }
	  
	  public String getValue(String n){

		  for(int i=0; i < numberOfValues; i++)
		  {
			  if (myValues[i].name == n)
			  {
				  return myValues[i].value;
			  }
		  }
		  return "";
	  }
	  
	  public void setValue(String n, String s){
		  // find existing
		  boolean found = false;
		  if (numberOfValues > 0)
		  {
			  for(int i=0; i < numberOfValues; i++)
			  {
				  if (myValues[i].name == n)
				  {
					  myValues[i].value = s;
					  found = true;
				  }
			  }
		  }
		  if (found == false)
		  {
			  int values_len = numberOfValues;
			  if (values_len < 0) values_len = 0;
			  myValue value = new myValue();
			  value.name = n;
			  value.value = s;
			  myValues[numberOfValues] = value;
			  numberOfValues++;
		  }
		
	  }
	
}
