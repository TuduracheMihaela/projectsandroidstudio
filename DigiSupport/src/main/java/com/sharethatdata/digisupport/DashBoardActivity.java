package com.sharethatdata.digisupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digisupport.adapter.RequestAdapter;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cContact;
import com.sharethatdata.webservice.datamodel.cRequestList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class DashBoardActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	ArrayList<HashMap<String, String>> contactsList; 
	ArrayList<HashMap<String, String>> itemList; 

    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userArray =  new ArrayList<String>();
	
    ProgressDialog pDialog;
    
	ListView lv = null;
	Spinner userSpinner = null;
	
	private String myUser = "";
	private String myPass = "";
	private String report_user = "";
	private String report_user_id = "";
	
	private int spinnerPositionAfterRotateScreen = 0;
	boolean setFirstTimePositionSpinner = false;
	
	RequestAdapter adapter;
	
	private SwipeRefreshLayout swipeContainer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		
		Globals = ((MyGlobals) getApplicationContext());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
		 lv = (ListView) findViewById(R.id.listView);
		 userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		 swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
	    
	    String isManager = Globals.getValue("manager");
	    String isEmployee = Globals.getValue("employee");
	    String isAdministrator = Globals.getValue("administrator");
	    String isDeveloper = Globals.getValue("developer");
	    
	    System.out.println("Role M: " + isManager);
	    System.out.println("Role E: " + isEmployee);
	    System.out.println("Role A: " + isAdministrator);
	    System.out.println("Role D: " + isDeveloper);
	    
	    new LoadIdAndNamebyUsername().execute();
	    
	    //if (isEmployee == "yes")
	    if (isManager == "yes")
 		{	
 			 Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
				sUsers.setVisibility(Spinner.VISIBLE);
				
				LoadUserList InitUserList;
				
				InitUserList = new LoadUserList();
				InitUserList.execute();
				
				System.out.println("Role: " + isManager);
 			
 		} else {
 			Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
			sUsers.setVisibility(Spinner.GONE);
			
			System.out.println("Role: " + isManager);
 		}

        // if screen was rotate
        if (isManager == "yes")
		{
	        if (savedInstanceState != null) 
	        {
	        	LoadUserList InitUserList;
				
				InitUserList = new LoadUserList();
				InitUserList.execute();

	        	int value = savedInstanceState.getInt("spinnerPosition");
	        	spinnerPositionAfterRotateScreen = value;
	        	
	        	setFirstTimePositionSpinner = true;
	        }
		}
        
        userSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	
		    	//if (userIsInteracting) {
			  		
		    		Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
		  	   		int myIndex = userSpinner.getSelectedItemPosition();
		  	   		report_user_id = userkeyArray.get(myIndex);
		  	   		
		  	   	    spinnerPositionAfterRotateScreen = myIndex;
		  	   	    lv.setAdapter(null);
		  	   		
		  	   		System.out.println(report_user_id);

		  	   		new LoadListAllRequests().execute();
		    	//}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		     
		    }

		});
        
         
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
			    String pid = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
			    
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("id", pid);
				
			    // start detail activity
				Intent intent = new Intent(getApplicationContext(), DetailActivity.class); startActivity(intent); 
				
			}

		});	
        
        // allow to scroll listview in SwipeRefreshLayout 
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ?
                                0 : lv.getChildAt(0).getTop();
                swipeContainer.setEnabled((topRowVerticalPosition >= 0));
            }
        });
        
        // SwipeRefreshLayout - Pull to refresh
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			
			@Override
			public void onRefresh() {
				adapter.clear();
				lv.setAdapter(null);	
				new LoadListAllRequests().execute();
				swipeContainer.setRefreshing(false);
			}
		});

        
        new LoadListAllRequests().execute(); 

        
	}
	
	 @Override
	 public void onSaveInstanceState(Bundle savedInstanceState) {
	   super.onSaveInstanceState(savedInstanceState);
	   Globals = ((MyGlobals) getApplicationContext());
       myUser = Globals.getValue("user");
       MyProvider = new WSDataProvider(myUser, myPass); 
       
	   String isManager = Globals.getValue("manager");
		 
		if (isManager == "yes")
		{
			userSpinner = (Spinner) findViewById(R.id.spinnerUser);
			int value = userSpinner.getSelectedItemPosition();
			spinnerPositionAfterRotateScreen = value;
			savedInstanceState.putInt("spinnerPosition", userSpinner.getSelectedItemPosition());
		}
	 }
	
	 @Override
	 public void onConfigurationChanged(Configuration newConfig) {
	     super.onConfigurationChanged(newConfig);

	     // Checks the orientation of the screen
	     if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	         
	     } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	        
	     }
	 }
	 
	@Override
	protected void onResume(){
		super.onResume();
		
		String create = Globals.getValue("create_request");
		String update = Globals.getValue("update_request");
		String delete = Globals.getValue("request_delete");
		
		if(create != "" || update != "")
		{
			Globals.setValue("create_request", "");
			Globals.setValue("update_request", "");
			
			try {
				adapter.clear();
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			lv.setAdapter(null);	
		}
		
		if(delete.equals("request_delete")){
			Globals.setValue("request_delete", "");
			try {
				adapter.clear();
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			lv.setAdapter(null);
			new LoadListAllRequests().execute();
		}
		
		new LoadListAllRequests().execute();

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dashboard, menu);
		return true;
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
            return true;
        case R.id.action_create_request:
            Intent intent2 = new Intent(this, CreateRequestActivity.class); startActivity(intent2); 
            return true;
        case R.id.action_my_requests:
            Intent intent3 = new Intent(this, MyRequestsActivity.class); startActivity(intent3); 
            return true;
        case R.id.action_reports:
            Intent intent4 = new Intent(this, ReportsActivity.class); startActivity(intent4); 
            return true;
        case R.id.action_faq:
            Intent intent5 = new Intent(this, FaqActivity.class); startActivity(intent5); 
            return true;
        case R.id.action_help:
            Intent intent6 = new Intent(this, HelpActivity.class); startActivity(intent6); 
            return true;
        /*case R.id.action_refresh:
        	 new LoadListAllRequests().execute();
            return true;*/
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	class LoadIdAndNamebyUsername extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {

			List<cContact> List = MyProvider.getUserIdbyUsername(myUser);
	    	
    		for(cContact entry : List)
         	{
    			SharedPreferences.Editor editor = getSharedPreferences("PREF_ID_CONTACT", 0).edit();
		   	     editor.putString("id", String.valueOf(entry.id));
		   	     editor.putString("name", String.valueOf(entry.name));
		   	     editor.commit();
         	}
    		
    		SharedPreferences pref = getSharedPreferences("PREF_ID_CONTACT", 0); 
	        String id = pref.getString("id", "");
	        
	        if(report_user_id == ""){
	        	report_user_id = id;
	        }
			
			return null;
		}
		
	}
	
	/**
	 * Background Async Task to Load my sent request by making HTTP Request
  	 * */
	class LoadListAllRequests extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
        /* pDialog = new ProgressDialog(DashBoardActivity.this);
         pDialog.setTitle("Loading requests");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();*/
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
		    //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			if (report_user == "") 
			{
				report_user = myUser;
			}
			
	    	 // load data from provider
	    	 itemList = new ArrayList<HashMap<String, String>>();  
	    	
	    	 List<cRequestList> myRequest = MyProvider.getAllRequestList(report_user_id);
	    	 for(cRequestList entry : myRequest)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("user",  String.valueOf(entry.fromuser));
                map.put("touser", String.valueOf(entry.touser));
                map.put("datetime", String.valueOf(entry.datetime));
                map.put("description",  String.valueOf(entry.description));
                map.put("status_name", String.valueOf(entry.status_name));
                 
                // adding HashList to ArrayList
                itemList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	    	runOnUiThread(new Runnable() {
	            public void run() {
	            	//pDialog.dismiss();
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	/*ListAdapter adapter = null;
	            	
	            	adapter = new SimpleAdapter(
		                		DashBoardActivity.this
		                		, itemList
		                		, R.layout.dashboard_listview_items, new String[] { "id", "user", "touser", "status_name", "description", "datetime" },
	                            new int[] { R.id.textViewID, R.id.textViewFromUser, R.id.textViewToUser, R.id.textViewStatus, R.id.textViewDescription, R.id.textViewDatetime });
	        			lv.setAdapter(adapter);*/
	            	
	            	adapter = new RequestAdapter(DashBoardActivity.this, R.layout.dashboard_listview_items, itemList);

	            	if(lv.getAdapter()==null)
	            		lv.setAdapter(adapter);
	            		else{
	            		   adapter.notifyDataSetChanged();
	            		}

	        		
	        			/*if (pDialog.isShowing())
			                pDialog.dismiss();*/
	            }
	        });
	  	      
	    }
	  

	}
	
	
	/**
	 * Background Async Task to Load all users by making HTTP Request
  	 * */
	class LoadUserList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
 
     }

    /**
     * getting items from url
     * */
    protected String doInBackground(String... args) {
	   	    	
    	MyProvider = new WSDataProvider(myUser, myPass);
		
		Globals = ((MyGlobals)getApplicationContext());
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
	   // MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	    
	    String id = "";
		String name = "";

		contactsList = new ArrayList<HashMap<String, String>>();
			
			// convert objects to hashmap for list
    		List<cContact> List = MyProvider.getMyUsers();
    	
    		for(cContact entry : List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.name);
          
                // adding HashList to ArrayList
                contactsList.add(map);

		}
    	
		return "";
   }

   /**
    * After completing background task Dismiss the progress dialog
    * **/
   protected void onPostExecute(String file_url) {
       // dismiss the dialog after getting all products

   		// updating UI from Background Thread
	   runOnUiThread(new Runnable() {
           public void run() {
           
    	    
           	for (HashMap<String, String> map : contactsList)
          	        for (Entry<String, String> entry : map.entrySet())
          	        {
          	        	if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
          	        	if (entry.getKey() == "name") userArray.add(entry.getValue());
          	        }
           	
           		ArrayAdapter<String> user_adapter = new ArrayAdapter<String>( getApplicationContext(), R.layout.spinner_item, userArray);
      	    	user_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
      	    	Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
      	    	sUsers.setAdapter(user_adapter);
      	    	
      	    	//if(userSpinnerIndex != -1) 
      	    	SharedPreferences pref = getSharedPreferences("PREF_ID_CONTACT", 0); 
    	        String name = pref.getString("name", "");
      	    	  // set the value of the spinner 
      	    		//sUsers.setSelection(userSpinnerIndex);
    	        
    	        if(setFirstTimePositionSpinner == false){
    	        	int spinnerPosition = user_adapter.getPosition(name);
		    		//set the default according to value
		    		sUsers.setSelection(spinnerPosition);
		    		
		    		setFirstTimePositionSpinner = true;
    	        }else{
    	        	// set value spinner if change rotation (portrait/landscape)
		    		int x = spinnerPositionAfterRotateScreen;
		    		userSpinner.setSelection(x);
    	        }
    	        
    	       

           }
       	});
 	      
    	}
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		//Handle the back button
	    if(keyCode == KeyEvent.KEYCODE_BACK) {
	        //Ask the user if they want to quit
	        new AlertDialog.Builder(this)
	        .setIcon(android.R.drawable.ic_dialog_alert)
	        .setTitle("Quit DigiSupport")
	        .setMessage("Are you sure?")
	        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	            @Override
	            public void onClick(DialogInterface dialog, int which) {

	                //Stop the activity
	                DashBoardActivity.this.finish();    
	            }

	        })
	        .setNegativeButton("No", null)
	        .show();

	        return true;
	    }
	    else {
	        return super.onKeyDown(keyCode, event);
	    }
		
	}
	

}
