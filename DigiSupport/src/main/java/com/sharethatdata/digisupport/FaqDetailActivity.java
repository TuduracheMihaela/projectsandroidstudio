package com.sharethatdata.digisupport;

import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class FaqDetailActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;
	
	private String myUser = "";
	private String myPass = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.faq_listview_items);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
	    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		
	    TextView textviewID = (TextView) findViewById(R.id.textviewID);
		TextView textViewQuestion = (TextView) findViewById(R.id.textViewQuestion);
		TextView textViewSolution = (TextView) findViewById(R.id.textViewSolution);
		ImageView arrow = (ImageView) findViewById(R.id.textViewArrow);
		
		/*String qid = Globals.getValue("qid");
		String question = Globals.getValue("question");
		String solution = Globals.getValue("solution");*/
		
		String qid, question, solution;
		    Bundle extras = getIntent().getExtras();
		    if(extras == null) {
		    	qid= null;
		    	question= null;
		    	solution= null;
		    } else {
		    	qid= extras.getString("qid");
		    	question= extras.getString("question");
		    	solution= extras.getString("solution");
		    }
		
		textviewID.setVisibility(View.GONE);
		textViewQuestion.setVisibility(View.VISIBLE);
		textViewSolution.setVisibility(View.VISIBLE);
		arrow.setVisibility(View.GONE);
		
		textViewSolution.setAutoLinkMask(Linkify.ALL);
		textViewSolution.setTextIsSelectable(true);
		
		textviewID.setText((Html.fromHtml(qid)));
		textViewQuestion.setText(Html.fromHtml(question));	
		textViewSolution.setText(Html.fromHtml(solution));
		
		/*Uri copyUri = Uri.parse(solution);
		ClipData clip = ClipData.newUri(getContentResolver(),"URI",copyUri);
		
		 ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
		 //ClipData clip = ClipData.newPlainText("label", solution);
		 clipboard.setPrimaryClip(clip);*/
		
		
	}

}
