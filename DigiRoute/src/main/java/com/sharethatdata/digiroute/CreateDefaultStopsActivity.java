package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;

/**
 * Created by miha on 8/24/2017.
 */

public class CreateDefaultStopsActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ProgressDialog progressDialog;

    String pharmacyId = "";

    String stop_id;
    String name;
    String street;
    String house;
    String zipcode;
    String city;
    String country;

    String update;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_default_stops);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateDefaultStopsActivity.this, CreateDefaultStopsActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            stop_id= null;
            name= null;
            street= null;
            house= null;
            zipcode= null;
            city= null;
            country= null;
            update= "";
        } else {
            stop_id= extras.getString("id");
            name= extras.getString("name");
            street= extras.getString("street");
            house= extras.getString("house");
            zipcode= extras.getString("zipcode");
            city= extras.getString("city");
            country= extras.getString("country");
            update= extras.getString("update");
        }

        final EditText editText_name = (EditText) findViewById(R.id.editText_name);
        final EditText editText_street = (EditText) findViewById(R.id.editText_street);
        final EditText editText_house = (EditText) findViewById(R.id.editText_house);
        final EditText editText_zipcode = (EditText) findViewById(R.id.editText_zipcode);
        final EditText editText_city = (EditText) findViewById(R.id.editText_city);
        final EditText editText_country = (EditText) findViewById(R.id.editText_country);
        Button addAddress = (Button) findViewById(R.id.btnAddDefaultStop);

        if(!update.equals("")){
            editText_name.setText(name);
            editText_street.setText(street);
            editText_house.setText(house);
            editText_zipcode.setText(zipcode);
            editText_city.setText(city);
            editText_country.setText(country);
            addAddress.setText(getString(R.string.btnEdit));
        }


        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if(editText_name.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    editText_name.setError(getString(R.string.required_name));
                }

                if( editText_street.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editText_street.setError(getString(R.string.required_street));
                }

                if( editText_house.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editText_house.setError(getString(R.string.required_house));
                }

                if(editText_zipcode.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editText_zipcode.setError(getString(R.string.required_zipcode));
                }

                if(editText_city.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editText_city.setError(getString(R.string.required_city));
                }

                if(editText_country.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    editText_country.setError(getString(R.string.required_country));
                }

                if(failFlag == false){
                    name = editText_name.getText().toString();
                    street = editText_street.getText().toString();
                    house = editText_house.getText().toString();
                    zipcode = editText_zipcode.getText().toString();
                    city = editText_city.getText().toString();
                    country = editText_country.getText().toString();

                    alertDialog();
                }
            }
        });
    }

    /** AsyncTask create address  */
    private class CreateAddress extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.createDefaultStop(name, street, house , zipcode, city, country, pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(CreateDefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateDefaultStopsActivity.this, getString(R.string.toast_text_failed_create_default_address) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        /*String error_message = MyProvider.last_error;
                        if(error_message.equals("unknown address, can not lookup gps coordinates.")){
                            Toast.makeText(CreateDefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateDefaultStopsActivity.this, "Failed to add default stop", Toast.LENGTH_SHORT).show();
                        }*/

                        Log.d("create address", "false");
                    }
                    else
                    {
                        Log.d("create address", "true");
                        Toast.makeText(CreateDefaultStopsActivity.this, getString(R.string.toast_text_create_default_address), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update address  */
    private class EditAddress extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.editDefaultStop(stop_id,name, street, house , zipcode, city, country, pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(CreateDefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateDefaultStopsActivity.this, getString(R.string.toast_text_failed_update_default_address) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                       /* String error_message = MyProvider.last_error;
                        if(error_message.equals("unknown address, can not lookup gps coordinates.")){
                            Toast.makeText(CreateDefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateDefaultStopsActivity.this, "Failed to edit default stop", Toast.LENGTH_SHORT).show();
                        }*/

                        Log.d("create address", "false");
                    }
                    else
                    {
                        Log.d("create address", "true");
                        Toast.makeText(CreateDefaultStopsActivity.this, getString(R.string.toast_text_update_default_address), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    private void alertDialog(){
        if(!update.equals("")){
            // edit
            new AlertDialog.Builder(CreateDefaultStopsActivity.this)
                    .setTitle(getString(R.string.alert_dialog_title_address))
                    .setMessage(getString(R.string.alert_dialog_text_update_address))
                    .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new EditAddress().execute();
                        }
                    })
                    .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else{
            // create
            new AlertDialog.Builder(CreateDefaultStopsActivity.this)
                    .setTitle(getString(R.string.alert_dialog_title_address))
                    .setMessage(getString(R.string.alert_dialog_text_add_address_as_default))
                    .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new CreateAddress().execute();
                        }
                    })
                    .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CreateDefaultStopsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
