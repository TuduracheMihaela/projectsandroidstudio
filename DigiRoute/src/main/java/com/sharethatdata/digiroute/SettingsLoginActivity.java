package com.sharethatdata.digiroute;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cWebService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by miha on 9/12/2017.
 */

public class SettingsLoginActivity extends PreferenceActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String PharmacyId = "";
    private String apikey = "";
    private String ws_url = "";

    ProgressDialog progressDialog;

    ArrayList<String> entriesAccessKey, entryValuesURL;

    static final String PREFS_NAME = "defaults";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
        addPreferencesFromResource(R.xml.url_settings);

        // Display the fragment as the main content.
        /*if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();*/

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SettingsLoginActivity.this, SettingsLoginActivity.this);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        apikey = prefs.getString("apikey","");
        ws_url = prefs.getString("ws_url", "");

        findPreference("prefWebServiceServer").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                String access_key = "";
                for(int i = 0; i < entryValuesURL.size(); i++){
                    if(entryValuesURL.get(i).equals(newValue.toString())){
                        access_key = entriesAccessKey.get(i);
                    }
                }

                Globals.setValue("ws_url", newValue.toString());
                Globals.setValue("access_key", access_key);

                // get list of web services one time and get,set the default url -> if the device was activated by the administrator
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("ws_url", newValue.toString());
                editor.putString("access_key", access_key);
                editor.commit();

                return true;
            }
        });

        ListPreference listPreferenceURL = (ListPreference) findPreference("prefWebServiceServer");

        new LoadListURL(listPreferenceURL).execute();
    }

    /*public static class PrefFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);

            getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
            addPreferencesFromResource(R.xml.url_settings);
        }
    }*/

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListURL extends AsyncTask<String, String, String> {

        ArrayList<String> entries;
        ListPreference lp;

        public LoadListURL(ListPreference lp){
            this.lp = lp;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            String myDeviceID = Globals.getValue("device");
            String myDeviceName = Globals.getValue("device_name");

            // load data from provider
            entries = new ArrayList<>();
            entryValuesURL = new ArrayList<>();
            entriesAccessKey = new ArrayList<>();

            List<cWebService> myService = MyProvider.getAllWebServices(apikey, myDeviceID, "3");
            for(cWebService entry : myService)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("type",  entry.type);
                map.put("url",  entry.url);
                map.put("access_key",  entry.access_key);

                // adding HashList to ArrayList
                entries.add(entry.name); // name = what user see, name of the url
                entryValuesURL.add(entry.url); // values = url to connect to service
                entriesAccessKey.add(entry.access_key); // access key = for secure login
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListPreference
                     * */
                    lp.setEntries(entries.toArray(new String[entries.size()]));
                    lp.setEntryValues(entryValuesURL.toArray(new String[entryValuesURL.size()]));

                    lp.setValue(ws_url);

                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }

}
