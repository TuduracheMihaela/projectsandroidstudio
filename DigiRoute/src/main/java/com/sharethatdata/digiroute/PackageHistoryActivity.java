package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sharethatdata.digiroute.Adapter.PackageHistoryAdapter;
import com.sharethatdata.digiroute.Profile.Helper;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cAddress;
import com.sharethatdata.logistics_webservice.datamodel.cPackageImage;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPackagesbyDeliveryId;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mihu__000 on 4/13/2017.
 */

public class PackageHistoryActivity extends Activity {
    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String idPharmacy = "";
    private String pharmacyAddress;

    ArrayList<HashMap<String, String>> packageList; // List Packages

    ProgressDialog progressDialog;

    TextView textViewCustomerScript;
    TextView textViewCustomerAddress;
    ImageView marker_address;

    ListView lv = null;
    String idAddress;
    String idRoute;
    String idScript;
    String total_distance;
    String total_time_driving;
    String total_time_stopped;

    cPackageImage packageImage;
    private String URL_ART = "/art";

    protected void onCreate(Bundle savedInstanceSaved) {
        super.onCreate(savedInstanceSaved);
        setContentView(R.layout.activity_package_history);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(PackageHistoryActivity.this, PackageHistoryActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        idPharmacy = sharedPrefs.getString("prefPharmacyId", "1");

        lv = (ListView) findViewById(R.id.listView);
        textViewCustomerScript = (TextView) findViewById(R.id.textViewScript);
        textViewCustomerAddress = (TextView) findViewById(R.id.textViewAddress);
        marker_address = (ImageView) findViewById(R.id.marker_address);
        TextView tv_total_distance = (TextView) findViewById(R.id.total_distance);
        TextView tv_total_time_driving = (TextView) findViewById(R.id.total_time_driving);
        TextView tv_total_time_stopped = (TextView) findViewById(R.id.total_time_stopped);
        TextView tv_total_time = (TextView) findViewById(R.id.total_time);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            idRoute= null;
            idAddress = null;
            idScript = null;
            total_distance = null;
            total_time_driving = null;
            total_time_stopped = null;
            pharmacyAddress = null;
        } else {
            idRoute = extras.getString("idRoute");
            idAddress = extras.getString("address_id");
            idScript = extras.getString("script");
            total_distance = extras.getString("total_distance");
            total_time_driving = extras.getString("total_time_driving");
            total_time_stopped = extras.getString("total_time_stopped");
            pharmacyAddress = extras.getString("pharmacyAddress");
        }

        //if(idAddress.equals("0")){
            // is pharmacy

           // LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);
           // linearLayout2.setVisibility(View.GONE);
           // TextView textViewLabelAddresses = (TextView) findViewById(R.id.textViewLabelAddresses);
           // textViewLabelAddresses.setVisibility(View.GONE);
        //}else{
            // is an address

            String time1 = "0";
            String time2 = "0";
            int total_time = 0;
            try{
                String[] tokens1 = total_time_driving.split("\\s");
                String values1 = tokens1[0]+tokens1[1];
                time1 = tokens1[0];

                String[] tokens2 = total_time_stopped.split("\\s");
                String values2 = tokens2[0]+tokens2[1];
                time2 = tokens2[0];

                total_time = Integer.valueOf(time1) + Integer.valueOf(time2);
            }catch (Exception e){

            }

            tv_total_distance.setText(total_distance);
            tv_total_time_driving.setText(total_time_driving);
            tv_total_time_stopped.setText(total_time_stopped);
            tv_total_time.setText(String.valueOf(total_time) + " min");
        //}


        new LoadAddressItem(idPharmacy).execute();
        new LoadListPackage(idPharmacy).execute();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadAddressItem extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadAddressItem(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            if(idAddress.equals("0")){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewCustomerAddress.setText(pharmacyAddress);

                    }
                });
            }else{
                final cAddress address = MyProvider.getAddress(idAddress, pharmacyId);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewCustomerScript.setVisibility(View.VISIBLE);
                        textViewCustomerScript.setText(Html.fromHtml(getString(R.string.textview_customer_script) + " " + idScript));
                        //textViewCustomerAddress.setText(Html.fromHtml(address.customer_street + "  " + address.customer_house + " , " + address.customer_zipcode + " , " + address.customer_city + " , " + address.customer_country));
                        List<String> slist = new ArrayList<String> ();
                        slist.add(address.customer_street);
                        slist.add(address.customer_house);
                        slist.add(address.customer_zipcode);
                        slist.add(address.customer_city);
                        slist.add(address.customer_country);

                        StringBuilder rString = new StringBuilder();
                        int count = 0;
                        for (String line : slist) {
                            ++count;
                            if (!line.equals("")) {
                                if(count != slist.size()){
                                    rString.append(line + ", ");
                                }else{
                                    rString.append(line);
                                }
                            }
                        }

                        textViewCustomerAddress.setText(rString.toString());
                    }
                });
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListPackage extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadListPackage(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            URL_ART = MyProvider.getWebServiceUrl() + URL_ART;

            // load data from provider
            packageList = new ArrayList<HashMap<String, String>>();

            List<cPackages> myPackage = MyProvider.getAllPackages(idRoute, idAddress, pharmacyId);
            for(cPackages entry : myPackage)
            {
                // creating new HashMap
                final HashMap<String, String> map = new HashMap<String, String>();

                map.put("packageid",  entry.packageid);
                map.put("deliveryid",  entry.delivery_id);
                map.put("barcode",  entry.package_barcode);
                map.put("status_description",  entry.package_status_description);

                packageImage = new cPackageImage();
                packageImage = MyProvider.getPackageImage(idPharmacy, entry.packageid);

                if (!packageImage.image.equals("")) {
                    // yes signature
                    map.put("signature",  getString(R.string.textView_text_yes));
                    map.put("image_url",  URL_ART);
                    map.put("image",  packageImage.image);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            marker_address.setImageResource(R.drawable.marker_green);
                        }
                    });

                    //ImageView imageView = (ImageView) findViewById(R.id.packageSignatureImage);
                    //new DownloadImageTask(imageView).execute(URL_ART + "/" + packageImage.image);
                }else {
                    // no signature
                    map.put("signature", getString(R.string.textView_text_no));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            marker_address.setImageResource(R.drawable.marker_gray);
                        }
                    });

                    // ImageView imageView = (ImageView) findViewById(R.id.packageSignatureImage);
                    // new DownloadImageTask(imageView).execute(URL_ART + "/" + packageImage.image);
                }

                List<cPackagesbyDeliveryId> packagesbyDeliveryId = MyProvider.getPackagesbyDeliveryId(entry.delivery_id, idPharmacy);
                for(cPackagesbyDeliveryId entry1 : packagesbyDeliveryId){
                    map.put("signature_person", entry1.signature_person);
                    map.put("deliver_remark", entry1.deliver_remark);
                    map.put("notify_patient", entry1.contact_patient);
                }

                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                    }
                });*/

                // adding HashList to ArrayList
                packageList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    PackageHistoryAdapter adapter = new PackageHistoryAdapter(PackageHistoryActivity.this, R.layout.activity_package_history_listview_items, packageList);
                    lv.setAdapter(adapter);

                    double size_tablet = MyTimeUtils.tabletSize(getApplicationContext());
                    if (size_tablet < 6) {
                        // set scroll listview for phone portrait
                        //MyTimeUtils.setListViewHeightBasedOnChildren(lv);
                        Helper.getListViewSizeBigCells(lv);
                    }

                    loadMapByPharmacyAddress(idRoute,idPharmacy);

                }
            });
            hideProgressDialog();
        }
    }

    public void loadMapByPharmacyAddress(String routeid, String pharmacy){
        String startpoint = pharmacyAddress;

        //	Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
        Log.d("startpoint", startpoint);

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        String map = getString(R.string.google_map_single); // is google map
        double size_tablet = MyTimeUtils.tabletSize(PackageHistoryActivity.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 950, 650 , 0.0, 0.0, 0.0, 0.0, PackageHistoryActivity.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 650, 450 , 0.0, 0.0, 0.0, 0.0, PackageHistoryActivity.this));

        }else{
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1",routeid, pharmacy, 340, 180 , 0.0, 0.0, 0.0, 0.0, PackageHistoryActivity.this));
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }
}
