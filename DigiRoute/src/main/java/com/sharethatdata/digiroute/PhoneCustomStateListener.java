package com.sharethatdata.digiroute;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.util.Log;

/**
 * Created by gotic_000 on 9/20/2018.
 */

public class PhoneCustomStateListener extends PhoneStateListener {

    public int signalSupport = 0;

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        signalSupport = signalStrength.getGsmSignalStrength();
        Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

        if (signalSupport > 30) {
            Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


        } else if (signalSupport > 20 && signalSupport < 30) {
            Log.d(getClass().getCanonicalName(), "Signal GSM : Average");


        } else if (signalSupport < 20 && signalSupport > 3) {
            Log.d(getClass().getCanonicalName(), "Signal GSM : Weak");


        } else if (signalSupport < 3) {
            Log.d(getClass().getCanonicalName(), "Signal GSM : Very weak");


        }
    }
}
