package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.logistics_webservice.datamodel.cUserCurrectLocation;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by miha on 9/20/2017.
 */

public class UserLocationActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> userList; // getContacts
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userNameArray =  new ArrayList<String>();

    private String myUser = "";
    private String myPass = "";
    private String ws_url = "";
    String pharmacyId = "";

    private String mySelectedUser = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_location);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        ws_url = Globals.getValue("ws_url");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(ws_url);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(UserLocationActivity.this, UserLocationActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        mySelectedUser = myUser;

        new LoadUserListSpinner(pharmacyId).execute();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadUserListSpinner extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadUserListSpinner(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            //addressList = new ArrayList<HashMap<String, String>>();
            userList = new ArrayList<HashMap<String, String>>();

            //List<cContact> myContact = MyProvider.getPharmacyUsers(pharmacyId);
            List<cContact> myContact = MyProvider.getMyUsers(pharmacyId);
            for(cContact entry : myContact)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("username", entry.username);
                map.put("name", entry.name);

                // adding HashList to ArrayList
                userList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (UserLocationActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    String isManager = Globals.getValue("manager");
                    if (isManager == "yes")
                    {
                        List<String> userArray =  new ArrayList<String>();
                        userArray.add(0, getString(R.string.select_user));
                        userkeyArray.add(0,"");
                        userNameArray.add(0,"");

                        if(userList.size()>0){
                            //userArray.add(0, getString(R.string.select_user));
                            //userkeyArray.add(0, getString(R.string.select_user));
                        }else{
                            userArray.add(0, getString(R.string.select_no_user));
                            userkeyArray.add(0, getString(R.string.select_no_user));
                        }

                        for (HashMap<String, String> map : userList)
                            for (Map.Entry<String, String> entry : map.entrySet())
                            {
                                if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                                if (entry.getKey() == "username") userNameArray.add(entry.getValue());
                                if (entry.getKey() == "name") userArray.add(entry.getValue());
                            }

                        //ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyRoutesActivity.this, android.R.layout.simple_spinner_item, userArray);
                        //user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.VISIBLE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.VISIBLE);
                        //sUsers.setAdapter(user_adapter);

                        SpinnerAdapter user_adapter = new SpinnerAdapter(
                                UserLocationActivity.this,
                                R.layout.spinner_adapter,
                                userArray
                        );
                        sUsers.setAdapter(user_adapter);

                        sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                                String mIndex = sUsers.getSelectedItem().toString();

                                if(mIndex.contains(getString(R.string.select_user))){
                                    // do nothing

                                }else{

                                    int myIndex = sUsers.getSelectedItemPosition();
                                    String userid = userkeyArray.get(myIndex);
                                    mySelectedUser = userid;

                                    new LoadUserCurrentLocation(mySelectedUser, pharmacyId).execute();


                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // auto select
                        int selectUserIndex = userNameArray.indexOf(myUser.toLowerCase());
                        sUsers.setSelection(selectUserIndex);
                        mySelectedUser = String.valueOf(selectUserIndex);

                    }else{
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.GONE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.GONE);

                        mySelectedUser = "";
                    }

                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadUserCurrentLocation extends AsyncTask<String, String, String> {

        String pharmacyId;
        String userId;

        public LoadUserCurrentLocation(String userId, String pharmacyId){
            this.pharmacyId = pharmacyId;
            this.userId = userId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cUserCurrectLocation location = MyProvider.getUserCurrentLocation(userId, pharmacyId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(!location.date_time.equals("")){
                        String previous_time = location.date_time; // format 2017-09-25 20:29:31

                        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Calendar c = Calendar.getInstance();
                        String current_time = date_format.format(c.getTime());

                        Date previous = null;
                        Date now = null;

                        try{
                            previous = date_format.parse(previous_time);
                            now = date_format.parse(current_time);

                            if (now.getTime() - previous.getTime() >= 70*60*1000) {
                                // more than 10 min ago - location unknown

                                TextView location = (TextView) findViewById(R.id.textViewLocation);
                                location.setVisibility(View.VISIBLE);

                                WebView webView = (WebView)findViewById(R.id.webView);
                                webView.setVisibility(View.GONE);
                            }else{
                                // show map

                                TextView location = (TextView) findViewById(R.id.textViewLocation);
                                location.setVisibility(View.GONE);

                                WebView webView = (WebView)findViewById(R.id.webView);
                                webView.setVisibility(View.VISIBLE);

                                loadMapWhereUserIs(mySelectedUser, pharmacyId);
                            }

                        }catch (ParseException e){
                            e.printStackTrace();
                        }
                    }else{
                        TextView location = (TextView) findViewById(R.id.textViewLocation);
                        location.setVisibility(View.VISIBLE);

                        WebView webView = (WebView)findViewById(R.id.webView);
                        webView.setVisibility(View.GONE);
                    }



                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    public void loadMapWhereUserIs(String userid, String pharmacy){

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        double size_tablet = MyTimeUtils.tabletSize(UserLocationActivity.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            /*webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/google_map_user_location.php?" +
                    "userid=" + userid + "&pharmacyid=" + pharmacy + "&map_width=960&map_height=450");*/

            webView.loadUrl( ws_url + "/" + "google_map_user_location.php" + "?" +
                    "userid=" + userid + "&pharmacyid=" + pharmacy + "&map_width=960&map_height=450");
        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(ws_url + "/" + "google_map_user_location.php" + "?" +
                    "userid=" + userid + "&pharmacyid=" + pharmacy + "&map_width=940&map_height=400");
        }else if (size_tablet > 4.98) {
            // Device is a bigger phone
            webView.loadUrl(ws_url + "/" + "google_map_user_location.php" + "?" +
                    "userid=" + userid + "&pharmacyid=" + pharmacy + "&map_width=390&map_height=540");
        }else{
            webView.loadUrl(ws_url + "/" + "google_map_user_location.php" + "?" +
                    "userid=" + userid + "&pharmacyid=" + pharmacy + "&map_width=350&map_height=480");
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = null;
    }
}
