package com.sharethatdata.digiroute;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Service.MessageService;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cMessage;

import java.util.ArrayList;
import java.util.HashMap;

public class NewMessageActivity extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    String message_from_user = "";
    String message_text = "";
    String message_id = "";
    String message_datetime = "";

    private String myUser = "";
    private String myPass = "";
    private String webservice_url = "";
    //private String myUserName = "";
    //private String myUserID = "";
    private String idPharmacy;

    private Button btnSubmit1;
    private TextView text_message;
    private TextView datetime_message;
    private TextView from_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        Globals = ((MyGlobals)getApplication());
        /*myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // id of user logged*/

        /*SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        pharmacyId = PharmacyId;*/

        SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
        myUser = prefUser.getString("USER", "");
        myPass = prefUser.getString("PASS", "");
        webservice_url = prefUser.getString("URL", "");

        SharedPreferences prefPharmacy = getSharedPreferences("PREF_Pharmacy", 0);
        idPharmacy = prefPharmacy.getString("Pharmacy","");


        // load message
        new LoadMessage().execute();

        text_message = (TextView) findViewById(R.id.textMessage);
        datetime_message = (TextView) findViewById(R.id.message_datetime);
        from_message = (TextView) findViewById(R.id.message_from);

        btnSubmit1 = (Button) findViewById(R.id.buttonSend);

        btnSubmit1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            finish();
            }

        });
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Background Async Task to Load new message by making HTTP Request
     * */
    class LoadMessage extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         */
        protected Boolean doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(webservice_url);
            cMessage message = MyProvider.getNewMessage(idPharmacy);

            boolean suc = false;
            if (message.id > 0)
            {
                message_from_user = message.user_from;
                message_text = message.message;
                message_datetime = message.datetime;
                message_id = String.valueOf(message.id);
                suc = true;

                MyProvider = new WSDataProvider(myUser, myPass);
                MyProvider.SetWebServiceUrl(webservice_url);
                MyProvider.updateMessageStatusRead(message_id, idPharmacy);
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(Boolean result) {


            if (result == true) {
                runOnUiThread(new Runnable() {
                    public void run() {

                        text_message.setText( message_text);
                        datetime_message.setText( message_datetime);
                        from_message.setText( message_from_user);
                    }
                });
            }else{
                String error_message_id = MyProvider.last_error_nr;
                String error_message = MyProvider.last_error;
                if(error_message_id.equals("1")){
                    Toast.makeText(NewMessageActivity.this, error_message, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(NewMessageActivity.this, getString(R.string.textView_text_error) + " " + error_message, Toast.LENGTH_SHORT).show();
                }
            }
            hideProgressDialog();

        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }

}
