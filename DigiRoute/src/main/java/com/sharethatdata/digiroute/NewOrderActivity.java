package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.sharethatdata.digiroute.Adapter.NewOrderAdapter;
import com.sharethatdata.digiroute.BarcodeReader.Barcode.BarcodeCaptureActivity;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.digiroute.Scanner.IntentResult;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewEditTextPhone;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewEditTextTablet;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseView;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.ViewTarget;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cCollections;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 7/4/2017.
 */

public class NewOrderActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ProgressDialog progressDialog;

    private static final int BARCODE_READER_REQUEST_CODE = 9001;

    ArrayList<HashMap<String, String>> routeList; // List Routes
    ArrayList<HashMap<String, String>> collectionList; // List Collections
    ArrayList<String> collectionkeyArray =  new ArrayList<String>();
    ArrayList<HashMap<String, String>> packageList; // List Packages

    ArrayList<String> packagesBarcodeOfRouteSelectedArray =  new ArrayList<String>();
    ArrayList<String> packagesUniqueIdOfRouteSelectedArray =  new ArrayList<String>();
    ArrayList<String> packagesDeliveryIdOfRouteSelectedArray =  new ArrayList<String>();
    ArrayList<String> packagesAddressOfRouteSelectedArray =  new ArrayList<String>();

    RelativeLayout spinnerLayout;
    ListView lv = null;

    String pharmacyAddress = "";
    String collectionId = "";
    String pharmacyId = "";
    String routeId = "";

    boolean confirm_collect = false;
    boolean blUpdateStatusPackage = false;
    boolean blUpdateStatusRoute = false;
    boolean processing_scan = false;
    Boolean isloading = false;

    ShowcaseView showcaseView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(NewOrderActivity.this, NewOrderActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        final String locationPrefix = sharedPrefs.getString("prefScannerLocationPrefix", "");
        final String locationLength = sharedPrefs.getString("prefScannerLocationLength", "1");
        final String packageLength = sharedPrefs.getString("prefScannerPackageLength", "1");
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        confirm_collect = sharedPrefs.getBoolean("prefConfirmCollect", false);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        spinnerLayout = (RelativeLayout) findViewById(R.id.spinnerLayout);
        ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit1);
        EditText editText = (EditText) findViewById(R.id.editText_barcode);
        editText.setEnabled(true);
        lv = (ListView) findViewById(R.id.listView);

        // if checkbox camera checked from settings = show button scan
        if (blCameraScanner)
        {
            editText.setHint(getString(R.string.select_action_text3));
            imageViewSubmit1.setVisibility(View.VISIBLE);
        } else {
            //editText.setHint(getString(R.string.select_action_text4));
            editText.setHint(getString(R.string.select_action_text3));
            imageViewSubmit1.setVisibility(View.GONE);
        }

        imageViewSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(NewOrderActivity.this, BarcodeCaptureActivity.class);
                startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);

                // create barcode intent
                /*IntentIntegrator integrator = new IntentIntegrator(SelectActionActivity.this);
                integrator.initiateScan();*/
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                    String bc = EditText.getText().toString();

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit1);
                    EditText editText = (EditText) findViewById(R.id.editText_barcode);
                    lv = (ListView) findViewById(R.id.listView);
                    imageViewSubmit1.setEnabled(false);
                    editText.setEnabled(false);
                    lv.setEnabled(false);

                    //new CheckBarcodeIfExist(bc, "").execute();
                    return true;
                }
                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                String bc = EditText.getText().toString();

                if (!processing_scan) {

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    if (bc.length() == Integer.valueOf(packageLength)) {
                        processing_scan = true;
                        new CheckBarcodeIfExist(bc, "").execute();
                    }
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                //String idPackage = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
                String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewAdressID)).getText().toString();
                String idDelivery = ((TextView) itemClicked.findViewById(R.id.textViewDeliveryID)).getText().toString();
                String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
                String barcode = ((TextView) itemClicked.findViewById(R.id.packageBarcode)).getText().toString();

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), AddressDetailActivity.class);
                intent.putExtra("idAddress", idAddress);
                intent.putExtra("idDelivery", idDelivery);
                intent.putExtra("idRoute", routeId);
                intent.putExtra("idScript", idScript);
                intent.putExtra("barcode", barcode);
                intent.putExtra("distance", "");
                intent.putExtra("time", "");
                startActivity(intent);

            }

        });

        new LoadListCollection(pharmacyId).execute();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("9", false)) {
            // <---- run one time code here
            showcaseView = new ShowcaseView.Builder(this)
                    .setTarget(new ViewTarget(R.id.editText_barcode, this))
                    .setContentTitle(getString(R.string.title_scan_step))
                    .setContentText(getString(R.string.text_scan_step))
                    .setShowcaseDrawer(new CustomShowcaseViewEditTextPhone(getResources()))
                    .singleShot(9)
                    .build();

            // mark first time has runned.
            SharedPreferences.Editor editor9 = prefs.edit();
            editor9.putBoolean("9", true);
            editor9.commit();
        }


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String UPCScanned = barcode.displayValue.toString();
                    EditText editText = (EditText) findViewById(R.id.editText_barcode);
                    editText.setText(UPCScanned);

                    String bc = editText.getText().toString();
                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    new CheckBarcodeIfExist(bc, "").execute();
                } else
                    Toast.makeText(NewOrderActivity.this, getString(R.string.toast_text_no_barcode_captured), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(NewOrderActivity.this, getString(R.string.textView_text_error) + " " + CommonStatusCodes.getStatusCodeString(resultCode), Toast.LENGTH_SHORT).show();
        } else
            super.onActivityResult(requestCode, resultCode, data);

        /*IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);
            }
        }*/
    }

    @Override
    public void onBackPressed() {
        // back → dialog end collecting → yes, set route status to in transport
        //                                  no, do nothing, keep current route open.

        new AlertDialog.Builder(NewOrderActivity.this)
                .setTitle(getString(R.string.alert_dialog_title_packages))
                .setMessage(getString(R.string.alert_dialog_text_packages))
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new CheckPackagesToUpdateRoute().execute();

                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListCollection extends AsyncTask<String, String, String> {

        public String pharmacyId;

        public LoadListCollection(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            routeList = new ArrayList<HashMap<String, String>>();
            collectionList = new ArrayList<HashMap<String, String>>();

            List<cMyRoute> myRoute = MyProvider.getMyRoutes("", pharmacyId,"","","");
            for(cMyRoute entry : myRoute)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id/status/status_description/description", entry.id+"/"+entry.status+"/"+entry.status_description+
                "/"+entry.description);

                // adding HashList to ArrayList
                routeList.add(map);
            }

            List<cCollections> myCollection = MyProvider.getCollections(pharmacyId);
            for(cCollections entry : myCollection)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("collection",  entry.collection);

                // adding HashList to ArrayList
                collectionList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    // check if route exist
                    for (HashMap<String, String> map1 : routeList){
                        for (Map.Entry<String, String> entry1 : map1.entrySet())
                        {
                            String row = String.valueOf(entry1.getValue().toString());
                            String[] parts = row.split("/");
                            String id = parts[0];
                            String status = parts[1];
                            String status_description = parts[2];
                            String description = parts[3];

                            // 1 = Loaded for delivery
                            if(status.equals("1")){
                                LinearLayout routeLayout = (LinearLayout) findViewById(R.id.routeIdLayout);
                                routeLayout.setVisibility(View.VISIBLE);
                                TextView routeIdTV = (TextView) findViewById(R.id.routeId);
                                TextView routeNameTV = (TextView) findViewById(R.id.routeName);
                                routeIdTV.setText(id);
                                routeNameTV.setText(description);

                                // route exist
                                routeId = id;
                            }
                        }
                    }

                    if(routeId.equals("")){
                        // route doesn't exist, so create new route
                        new CreateRoute(pharmacyId).execute();
                    }

                    // get collections
                    final List<String> collectionArray =  new ArrayList<String>();
                    collectionkeyArray =  new ArrayList<String>();
                    for (HashMap<String, String> map : collectionList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") collectionkeyArray.add(entry.getValue());
                            if (entry.getKey() == "collection") collectionArray.add(entry.getValue());
                        }

                    if(collectionList.size()>0){
                        if(collectionList.size()==1){
                            // 1 collection - Spinner visible gone
                            String collectionid = collectionArray.get(0);
                            Log.d("collectionid",collectionid);
                            collectionId = collectionid;

                            spinnerLayout.setVisibility(View.VISIBLE);
                            collectionArray.add(0, getString(R.string.select_collection));
                            collectionkeyArray.add(0, getString(R.string.select_collection));

                            //new LoadPackages(collectionid, pharmacyId).execute();
                            new LoadPackages("", pharmacyId).execute();

                        }else{
                            spinnerLayout.setVisibility(View.VISIBLE);
                            collectionArray.add(0, getString(R.string.select_collection));
                            collectionkeyArray.add(0, getString(R.string.select_collection));

                        }
                    }else{
                        // No collection - Spinner gone

                        new LoadPackages("", pharmacyId).execute();
                    }


                    final Spinner sCollection = (Spinner) findViewById(R.id.spinnerCollection);
                    SpinnerAdapter route_adapter = new SpinnerAdapter(
                            NewOrderActivity.this,
                            R.layout.spinner_adapter,
                            collectionArray
                    );
                    sCollection.setAdapter(route_adapter);

                    sCollection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Spinner sRoutes = (Spinner) findViewById(R.id.spinnerCollection);
                            String mIndex = sRoutes.getSelectedItem().toString();

                            if(mIndex.contains(getString(R.string.select_collection))){
                                new LoadPackages("", pharmacyId).execute();
                            }else{
                                int myIndex = sRoutes.getSelectedItemPosition();
                                String collectionid = collectionArray.get(myIndex);
                                Log.d("collectionid",collectionid);
                                collectionId = collectionid;

                                blUpdateStatusPackage = false;

                                    new LoadPackages(collectionid, pharmacyId).execute();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
            });

            hideProgressDialog();
        }
    }


    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadPackages extends AsyncTask<String, String, String> {

        public String collectionid, pharmacyId;

        public LoadPackages(String collectionid, String pharmacyId){
            this.collectionid = collectionid;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            packageList = new ArrayList<HashMap<String, String>>();
            if(!blUpdateStatusPackage){
                packagesBarcodeOfRouteSelectedArray = new ArrayList<String>();
                packagesUniqueIdOfRouteSelectedArray = new ArrayList<String>();
                packagesDeliveryIdOfRouteSelectedArray = new ArrayList<String>();
                packagesAddressOfRouteSelectedArray = new ArrayList<String>();
            }

            List<cPackages> myPackages = MyProvider.getPackages(collectionid, pharmacyId);
            for(cPackages entry : myPackages)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                // finish update scan packages, in special with same barcode
                if(!blUpdateStatusPackage){
                    // 1 = Loaded for delivery
                    if(entry.status_id.equals("1")){
                        packagesBarcodeOfRouteSelectedArray.add(entry.package_barcode);
                        packagesUniqueIdOfRouteSelectedArray.add(entry.packageid);
                        packagesDeliveryIdOfRouteSelectedArray.add(entry.delivery_id);
                        packagesAddressOfRouteSelectedArray.add(entry.customer_address);
                    }
                }

                map.put("packageid",  entry.packageid);
                map.put("addressid",  entry.addressid);
                map.put("delivery_id",  entry.delivery_id);
                map.put("package_barcode",  entry.package_barcode);
                map.put("route",  entry.route);
                map.put("location",  entry.location);
                map.put("scriptid",  entry.customer_script);
                //map.put("package_address", entry.customer_address);
                map.put("customer_street", entry.customer_street);
                map.put("customer_house", entry.customer_house);
                map.put("customer_zipcode", entry.customer_zipcode);
                map.put("customer_city", entry.customer_city);
                map.put("customer_country", entry.customer_country);
                map.put("package_status",  entry.status_id);
                map.put("package_status_description",  entry.package_status_description);

                // adding HashList to ArrayList
                packageList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ListAdapter adapter = null;

                    adapter = new NewOrderAdapter(NewOrderActivity.this, R.layout.activity_new_order_listview_items, packageList);
                    lv.setAdapter(adapter);
                }
            });

            hideProgressDialog();
        }
    }

    class CheckPackagesToUpdateRoute extends AsyncTask<String, String, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            List<cPackageRoute> myPackages = MyProvider.getPackagesOfRoute(routeId, "", pharmacyId);
            for(cPackageRoute entry : myPackages)
            {
                // 3 = In Transport
                if(entry.package_status.equals("3")){
                    blUpdateStatusRoute = true;
                    break;
                }else {
                    blUpdateStatusRoute = false;
                }
            }
            return blUpdateStatusRoute;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean){
                new UpdateRouteStatus(pharmacyId, routeId, "3").execute();
            }else {
                finish();
            }

        }
    }

    class CheckBarcodeIfExist extends AsyncTask<String, String, String> {

        String barcodePackage;
        String packageExits;
        int x;
        int loading;

        public CheckBarcodeIfExist( String barcodePackage, String packageExits){
            this.barcodePackage = barcodePackage;
            this.packageExits = packageExits;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            isloading = true;
            loading = 0;
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected String doInBackground(String... args) {

            //boolean packageExits = false;
            //String packageExits = "";
            if(isloading) {
                if (loading == 0){
                    loading++;
                    if (packageExits.equals("")) {
                        for (int i = 0; i < packagesBarcodeOfRouteSelectedArray.size(); i++) {
                            String bcFromArray = packagesBarcodeOfRouteSelectedArray.get(i).toString();
                            if (!barcodePackage.equals("")) {
                                if (bcFromArray.equals(barcodePackage)) {
                                    // update package as scanned
                                    x = i;
                                    packageExits = "true";
                                } else {
                                    // barcode scanned doesn't belong to this route
                                    Log.d("TAG BARCODE", "barcode scanned doesn't belong to this route");
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            isloading = false;
            return packageExits;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            if(result.equals("true")){
                final int y = x;
                final String bcUniqueIdFromArray = packagesUniqueIdOfRouteSelectedArray.get(y).toString();
                final String bcDeliveryIdFromArray = packagesDeliveryIdOfRouteSelectedArray.get(y).toString();
                final String bcAddressFromArray = packagesAddressOfRouteSelectedArray.get(y).toString();

                // confirm collection dialog
                if(confirm_collect){

                    String alert0 = getString(R.string.alert_dialog_text_collecting_question);
                    String alert1 = getString(R.string.alert_dialog_text_collecting_barcode) + " " + barcodePackage;
                    String alert2 = getString(R.string.alert_dialog_text_collecting_address) + " " + bcAddressFromArray;
                    String alert3 = getString(R.string.alert_dialog_text_collecting_route) + " " + routeId;

                    new AlertDialog.Builder(NewOrderActivity.this)
                            .setTitle(getString(R.string.alert_dialog_title_collecting))
                            .setMessage(alert0 +"\n\n"+alert1 +"\n"+ alert2 +"\n"+ alert3)
                            .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    /*blUpdateStatusPackage = true;
                                    packagesBarcodeOfRouteSelectedArray.remove(y);
                                    packagesUniqueIdOfRouteSelectedArray.remove(y);
                                    packagesDeliveryIdOfRouteSelectedArray.remove(y);
                                    packagesAddressOfRouteSelectedArray.remove(y);*/
                                    new AddPackageToRoute(pharmacyId, bcDeliveryIdFromArray, routeId, 0).execute();

                                }
                            })
                            .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                                    EditText.setText(null);

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }else{

                    /*blUpdateStatusPackage = true;
                    packagesBarcodeOfRouteSelectedArray.remove(y);
                    packagesUniqueIdOfRouteSelectedArray.remove(y);
                    packagesDeliveryIdOfRouteSelectedArray.remove(y);
                    packagesAddressOfRouteSelectedArray.remove(y);*/
                    new AddPackageToRoute(pharmacyId, bcDeliveryIdFromArray, routeId, y).execute();
                }


            }else{
                if(barcodePackage.equals("")){

                }else{
                    //if(packagesBarcodeOfRouteSelectedArray.size() == 0){
                    //    Toast.makeText(NewOrderActivity.this, "No more packages to be scanned!", Toast.LENGTH_SHORT).show();
                    //}else{
                        Toast.makeText(NewOrderActivity.this, getString(R.string.toast_text_no_package) + " " + barcodePackage, Toast.LENGTH_LONG).show();
                    //}

                }

            }

            // always clear
            EditText EditText = (EditText) findViewById(R.id.editText_barcode);
            EditText.setText(null);

            processing_scan = false;
            isloading = false;

            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class AddPackageToRoute extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String deliveryId = "";
        String routeId = "";
        int y;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public AddPackageToRoute(String pharmacyId, String deliveryId, String routeId, int y){
            this.pharmacyId = pharmacyId;
            this.deliveryId = deliveryId;
            this.routeId = routeId;
            this.y = y;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.addPackageToRoute(pharmacyId, deliveryId, routeId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(NewOrderActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(NewOrderActivity.this, getString(R.string.toast_text_failed_add_package_to_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                        EditText.setText(null);
                    }
                    else
                    {
                        Globals.setValue("add_package_to_route","add_package_to_route");
                        try{
                            showcaseView.hide();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        new UpdatePackageStatus(pharmacyId, deliveryId, "3", y).execute();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdatePackageStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String deliveryId = "";
        String statusId = "";
        int y;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdatePackageStatus(String pharmacyId, String deliveryId, String statusId, int y){
            this.pharmacyId = pharmacyId;
            this.deliveryId = deliveryId;
            this.statusId = statusId;
            this.y = y;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updatePackageStatus(pharmacyId, deliveryId, statusId, 0.0, 0.0);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Package Status", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(NewOrderActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(NewOrderActivity.this, getString(R.string.toast_text_failed_update_package) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        EditText editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setText(null);
                    }
                    else
                    {
                        blUpdateStatusPackage = true;
                        packagesBarcodeOfRouteSelectedArray.remove(y);
                        packagesUniqueIdOfRouteSelectedArray.remove(y);
                        packagesDeliveryIdOfRouteSelectedArray.remove(y);
                        packagesAddressOfRouteSelectedArray.remove(y);


                        Log.d("Update Package Status", "true");
                        if(collectionList.size()>0){
                            new LoadPackages(collectionId, pharmacyId).execute();
                        }else{
                            new LoadPackages("", pharmacyId).execute();
                        }

                        EditText editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setText(null);
                    }

                    ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit1);
                    EditText editText = (EditText) findViewById(R.id.editText_barcode);
                    lv = (ListView) findViewById(R.id.listView);
                    imageViewSubmit1.setEnabled(true);
                    editText.setEnabled(true);
                    lv.setEnabled(true);
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status route  */
    private class UpdateRouteStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String routeId = "";
        String statusId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdateRouteStatus(String pharmacyId, String routeId, String statusId){
            this.pharmacyId = pharmacyId;
            this.routeId = routeId;
            this.statusId = statusId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateRouteStatus(pharmacyId, routeId, statusId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (NewOrderActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Route Status", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(NewOrderActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(NewOrderActivity.this, getString(R.string.toast_text_failed_update_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }
                    else
                    {
                        Log.d("Update Route Status", "true");

                        finish();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask create route report  */
    private class CreateRoute extends AsyncTask<String, String, Integer>{
        String pharmacyId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public CreateRoute(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        @Override
        protected Integer doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            int suc = MyProvider.createRoute(pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Integer success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success != 0)
                    {
                        Log.d("create route", "true");

                        LinearLayout routeLayout = (LinearLayout) findViewById(R.id.routeIdLayout);
                        routeLayout.setVisibility(View.VISIBLE);
                        TextView routeIdTV = (TextView) findViewById(R.id.routeId);
                        routeIdTV.setText(String.valueOf(success));
                        routeId = String.valueOf(success);
                    }
                    else
                    {
                        Log.d("create route", "false");
                    }
                }
            });
            hideProgressDialog();
        }
    }


    /*public void confirmCollection(){
        if(confirm_collect){
            new AlertDialog.Builder(NewOrderActivity.this)
                    .setTitle("Collecting")
                    .setMessage("Confirm package to route?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {



                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else{
            
        }
    }*/

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(NewOrderActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
