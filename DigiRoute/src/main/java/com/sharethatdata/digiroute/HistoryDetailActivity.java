package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sharethatdata.digiroute.Adapter.HistoryAdapter;
import com.sharethatdata.digiroute.Adapter.HistoryDetailAdapter;
import com.sharethatdata.digiroute.Profile.Helper;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPharmacyAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRoute;
import com.sharethatdata.logistics_webservice.datamodel.cRouteReport;
import com.sharethatdata.logistics_webservice.datamodel.cRoutes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by mihu__000 on 4/3/2017.
 */

public class HistoryDetailActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ArrayList<HashMap<String, String>> itemList;
    ArrayList<HashMap<String, String>> itemListTest;

    //private char[] characters = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    ProgressDialog pDialog;

    ListView lv = null;
    TextView distance;
    TextView time;

    String idPharmacy, distanceUnit;
    String idRoute, nameRoute;
    String pharmacyAddress, pharmacyStreet, pharmacyHouse, pharmacyZipcode, pharmacyCity, pharmacyCountry;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(HistoryDetailActivity.this, HistoryDetailActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        final String DistanceUnit = sharedPrefs.getString("prefDistanceUnit", "");
        idPharmacy = PharmacyId;
        distanceUnit = DistanceUnit;

        lv = (ListView) findViewById(R.id.listView);
        distance = (TextView)findViewById(R.id.total_distance);
        time = (TextView)findViewById(R.id.total_time);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            idRoute= null;
            nameRoute= null;
        } else {
            idRoute= extras.getString("idRoute");
            nameRoute= extras.getString("nameRoute");
        }

        TextView tv_idRoute = (TextView) findViewById(R.id.tv_idRoute) ;
        tv_idRoute.setText(idRoute);
        TextView tv_nameRoute = (TextView) findViewById(R.id.tv_nameRoute) ;
        tv_nameRoute.setText(nameRoute);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String address_id = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
                String script = ((TextView) itemClicked.findViewById(R.id.textViewScript)).getText().toString();
                String total_distance = ((TextView) itemClicked.findViewById(R.id.total_distance)).getText().toString();
                String total_time_driving = ((TextView) itemClicked.findViewById(R.id.total_time)).getText().toString();
                String total_time_stopped = ((TextView) itemClicked.findViewById(R.id.total_time_stopped)).getText().toString();

                // start detail activity
                Intent intent = new Intent(HistoryDetailActivity.this, PackageHistoryActivity.class);
                intent.putExtra("idRoute", idRoute);
                intent.putExtra("address_id", address_id);
                intent.putExtra("script", script);
                intent.putExtra("total_distance", total_distance);
                intent.putExtra("total_time_driving", total_time_driving);
                intent.putExtra("total_time_stopped", total_time_stopped);
                intent.putExtra("pharmacyAddress", pharmacyAddress);

                startActivity(intent);

            }

        });

        new LoadPharmacyLocation(PharmacyId).execute();

    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        String time_return = "0";
        List<String> strArray;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {
            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            itemList = new ArrayList<HashMap<String, String>>();
            itemListTest = new ArrayList<HashMap<String, String>>();

            //char letter='B';
            String last_sequence = "0";

            strArray = new ArrayList<String>();
            List<cRouteReport> routeReport = MyProvider.getRouteReports(idRoute, idPharmacy);

                for(cRouteReport entry : routeReport)
                {
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();
                    HashMap<String, String> map1 = new HashMap<String, String>();

                    String time1 = "0";
                    String date1 = "0";
                    try {
                        String[] tokens1 = entry.datetime_start.split("\\s");
                        String values1 = tokens1[0]+tokens1[1];
                        date1 = tokens1[0];
                        time1 = tokens1[1];
                    }catch (Exception e){
                        System.out.println(e.toString());
                    }

                    String time2 = "0";
                    String date2 = "0";
                    try {
                        String[] tokens2 = entry.datetime_end.split("\\s");
                        String values2 = tokens2[0]+tokens2[1];
                        date2 = tokens2[0];
                        time2 = tokens2[1];
                    }catch (Exception e){
                        System.out.println(e.toString());
                    }



                    // map.put("date", date);
                    map.put("id",  entry.id);
                    map.put("script",  entry.customer_script);
                    map.put("dateStart", date1);
                    map.put("timeStart" , time1);
                    map.put("dateEnd", date2);
                    map.put("timeEnd" , time2);
                    //map.put("street", entry.customer_street +" "+ entry.customer_house +" , "+ entry.customer_zipcode +" , "+entry.customer_city +" , "+ entry.customer_country);

                    List<String> slist = new ArrayList<String> ();
                    slist.add(entry.customer_street);
                    slist.add(entry.customer_house);
                    slist.add(entry.customer_zipcode);
                    slist.add(entry.customer_city);
                    slist.add(entry.customer_country);

                    StringBuilder rString = new StringBuilder();
                    int count = 0;
                    for (String line : slist) {
                        ++count;
                        if (!line.equals("")) {
                            if(count != slist.size()){
                                rString.append(line + ", ");
                            }else{
                                rString.append(line);
                            }
                        }
                    }
                    map.put("street", rString.toString());

                    map.put("distance", entry.distance);
                    map.put("time", entry.time); // seconds
                    //map.put("letter", String.valueOf(characters[Integer.valueOf(entry.sequence)]));
                   // int sequence = Integer.valueOf(entry.sequence) + 65;
                    int sequence = Integer.valueOf(entry.sequence);
                   // map.put("letter", String.valueOf(Character.toString ((char)(int)Integer.valueOf(sequence))));
                    //map.put("letter", getLetterFromNoddeNR(sequence));
                    map.put("letter", String.format("%02d", sequence));

                    strArray.add(time1+"/"+time2);
                    map.put("timeStart/timeEnd", time1+"/"+time2);
                    map1.put("timeStart/timeEnd", time1+"/"+time2);

                    if(Integer.valueOf(last_sequence) < Integer.valueOf(entry.sequence))
                    last_sequence = entry.sequence;

                    map.put("status_id",  entry.status);
                    map.put("barcode",  entry.barcode);

                    // get packages for color
                    /*List<cPackages> myPackage = MyProvider.getAllPackages(idRoute, entry.id, idPharmacy);
                    for(cPackages entry2 : myPackage)
                    {
                        //HashMap<String, String> map2 = new HashMap<String, String>();
                        // 1 = Loaded for delivery
                        // 3 = In Transport
                        // 5 = Delivered
                        // 6 = Not at home
                        // 7 = Denied by customer
                        // 8 = Cancelled
                        // status package for color
                        if((entry2.status_id.equals("6"))){
                            map.put("status_description_priority",  entry2.status_description);
                            map.put("status_id_priority",  entry2.status_id);
                        }else{
                            map.put("status_description",  entry2.status_description);
                            map.put("status_id",  entry2.status_id);
                        }

                        map.put("barcode",  entry2.package_barcode);
                    }*/


                    // adding HashList to ArrayList
                    itemList.add(map);
                    itemListTest.add(map1);
                }

            // add last address = pharmacy
            if(itemList.size() > 0){
                final cRoute pharmacy = MyProvider.getRouteReport(idRoute, idPharmacy);
                HashMap<String, String> map = new HashMap<String, String>();
                HashMap<String, String> map1 = new HashMap<String, String>();

                String date1 = "0";
                String time1 = "0";
                String date2 = "0";
                String time2 = "0";
                try{
                    String[] tokens1 = pharmacy.datetime_start.split("\\s");
                    String values1 = tokens1[0]+tokens1[1];
                    date1 = tokens1[0];
                    time1 = tokens1[1];

                    String[] tokens2 = pharmacy.datetime_end.split("\\s");
                    String values2 = tokens2[0]+tokens2[1];
                    date2 = tokens2[0];
                    time2 = tokens2[1];
                }catch (Exception e){
                    e.printStackTrace();
                }

                map.put("id",  "0");
                //map.put("customer_script",  "Pharmacy");
                map.put("script",  "Pharmacy");
                map.put("dateStart", date1);
                map.put("timeStart" , time1);
                map.put("dateEnd", date2);
                map.put("timeEnd" , time2);
               // map.put("street", pharmacyAddress);

                List<String> slist = new ArrayList<String> ();
                slist.add(pharmacyStreet);
                slist.add(pharmacyHouse);
                slist.add(pharmacyZipcode);
                slist.add(pharmacyCity);
                slist.add(pharmacyCountry);

                StringBuilder rString = new StringBuilder();
                int count = 0;
                for (String line : slist) {
                    ++count;
                    if (!line.equals("")) {
                        if(count != slist.size()){
                            rString.append(line + ", ");
                        }else{
                            rString.append(line);
                        }
                    }
                }
                map.put("street", rString.toString());

                map.put("distance", pharmacy.return_distance);
                map.put("time", pharmacy.return_time); // seconds
                //map.put("letter", String.valueOf(characters[Integer.valueOf(last_sequence) + 1]));
                //int sequence = Integer.valueOf(last_sequence) + 65 + 1;
                //map.put("letter", String.valueOf(Character.toString ((char)(int)Integer.valueOf(sequence))));
                //map.put("letter", getLetterFromNoddeNR(Integer.valueOf(last_sequence) + 1));
                map.put("letter", String.format("%02d", (Integer.valueOf(last_sequence) + 1)));

                strArray.add(time1+"/"+time2); // add return time
                map.put("timeStart/timeEnd", time1+"/"+time2);
                map1.put("timeStart/timeEnd", time1+"/"+time2);

                map.put("barcode",  "Pharmacy");

                time_return = pharmacy.return_time;

                itemList.add(map);
                itemListTest.add(map1);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    if(itemList.size() > 0){

                        ListAdapter adapter = null;

                        adapter = new HistoryDetailAdapter(HistoryDetailActivity.this, R.layout.activity_history_detail_listview_item, itemList);
                        lv.setAdapter(adapter);

                        /*ListAdapter adapter = null;
                        adapter = new SimpleAdapter(
                                HistoryDetailActivity.this
                                , itemList
                                , R.layout.activity_history_detail_listview_item, new String[]{"id", "script", "dateStart", "dateEnd", "timeStart", "timeEnd", "street", "distance", "time", "timeStart/timeEnd", "letter"},
                                new int[]{R.id.textViewID, R.id.textViewScript, R.id.customerDateStart, R.id.customerDateEnd, R.id.customerTimeStart, R.id.customerTimeEnd, R.id.customerAddress, R.id.total_distance, R.id.total_time, R.id.total_time_stopped, R.id.textViewLetterAddress});
                        lv.setAdapter(adapter);*/

                        double size_tablet = MyTimeUtils.tabletSize(getApplicationContext());
                        if (size_tablet < 6) {
                            // set scroll listview for phone portrait
                            Helper.getListViewSizeBigCells(lv);
                        }

                        // total distance
                        int myNum_distance = 0;
                        int total_distance = 0;

                        if(distanceUnit.equals(getResources().getString(R.string.settings_kilometers))) {
                            // Kilometers

                            for (HashMap<String, String> map : itemList)
                                for (Map.Entry<String, String> entry : map.entrySet()) {
                                    if (entry.getKey() == "distance") {
                                        try {
                                            myNum_distance = Integer.parseInt(entry.getValue().toString());
                                            entry.setValue(MyTimeUtils.getFormattedKilometers(myNum_distance));

                                        } catch (NumberFormatException nfe) {
                                            myNum_distance = 0;
                                        }
                                        total_distance = total_distance + myNum_distance;
                                    }
                                }

                            distance.setText(MyTimeUtils.getFormattedKilometers(total_distance));

                        }else if(distanceUnit.equals(getResources().getString(R.string.settings_miles))) {
                            // Miles

                            for (HashMap<String, String> map : itemList)
                                for (Map.Entry<String, String> entry : map.entrySet()) {
                                    if (entry.getKey() == "distance") {
                                        try {
                                            myNum_distance = Integer.parseInt(entry.getValue().toString());
                                            entry.setValue(MyTimeUtils.getFormattedMiles(myNum_distance));

                                        } catch (NumberFormatException nfe) {
                                            myNum_distance = 0;
                                        }
                                        total_distance = total_distance + myNum_distance;
                                    }
                                }

                            distance.setText(MyTimeUtils.getFormattedMiles(total_distance));

                        }

                        // total_time (trip + return)
                        int myNum_time = 0;
                        int total_time_driving_and_return = 0;
                        for (HashMap<String, String> map : itemList)
                            for (Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey() == "time") {
                                    try {
                                        myNum_time = Integer.parseInt(entry.getValue().toString());
                                        entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum_time));

                                    } catch (NumberFormatException nfe) {
                                        myNum_time = 0;
                                    }
                                    total_time_driving_and_return = total_time_driving_and_return + myNum_time;
                                }
                            }

                        TextView time_driving = (TextView) findViewById(R.id.total_time_driving);
                        time_driving.setText(MyTimeUtils.getFormattedHourMinutes(total_time_driving_and_return)); // seconds to minutes or hours

                        //////////////////////////////////////////////////////////
                        ///// total time stopped per address /////

                        int ii = 0; // remember i
                        long time_stopped1 = 0;
                        int time_stopped_seconds1 = 0;
                        long myNum_time_stopped1 = 0;
                        boolean last_item1 = false;

                        for (HashMap<String, String> map : itemList) {
                            for (Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey() == "timeStart/timeEnd") {
                                    for (int i = 0; i < strArray.size(); i++) {
                                        i = ii;
                                        for (int j = i + 1; j < i + 2; j++) {

                                            String row1 = String.valueOf(strArray.get(i).toString());
                                            String[] parts1 = row1.split("/");
                                            String timeStart1 = parts1[0];
                                            String timeEnd1 = parts1[1];

                                            if (strArray.get(strArray.size() - 1).contains(row1)) {
                                                last_item1 = true;
                                                // to break the loop, if all date time are the same
                                                i = strArray.size();

                                                entry.setValue("0 min");
                                                break;
                                            }

                                            String row2 = String.valueOf(strArray.get(j).toString());
                                            String[] parts2 = row2.split("/");
                                            String timeStart2 = parts2[0];
                                            String timeEnd2 = parts2[1];

                                            // compare value1 and value2;
                                            myNum_time_stopped1 = MyTimeUtils.getDifferenceStoppedTime(timeStart2, timeEnd1);
                                        }
                                        if (!last_item1 && entry.getKey() == "timeStart/timeEnd") {
                                            time_stopped1 =  myNum_time_stopped1; // miliseconds
                                            time_stopped_seconds1 = MyTimeUtils.getFormattedMilisecondsToSeconds(time_stopped1);

                                            ii++;

                                            entry.setValue(MyTimeUtils.getFormattedHourMinutes(time_stopped_seconds1));
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        //////////////////////////////////////////////////////////////


                        // total_time_stop
                        long total_time_stopped = 0;
                        int total_time_stopped_seconds = 0;
                        long myNum_time_stopped = 0;
                        boolean last_item = false;

                        // move values from hash map to array list
                        String strArray[] = new String[itemListTest.size()];
                        for (int i = 0; i < itemListTest.size(); i++) {
                            strArray[i] = itemListTest.get(i).get("timeStart/timeEnd");
                        }

                        for (int i = 0; i < strArray.length; i++) {
                            for (int j = i + 1; j < i + 2; j++) {
                                String row1 = String.valueOf(strArray[i].toString());
                                String[] parts1 = row1.split("/");
                                String timeStart1 = parts1[0];
                                String timeEnd1 = parts1[1];

                                if (itemListTest.get(itemListTest.size() - 1).containsValue(row1)) {
                                    last_item = true;
                                    // to break the loop, if all date time are the same
                                    i = strArray.length;

                                    break;
                                }

                                String row2 = String.valueOf(strArray[j].toString());
                                String[] parts2 = row2.split("/");
                                String timeStart2 = parts2[0];
                                String timeEnd2 = parts2[1];

                                // compare value1 and value2;
                                myNum_time_stopped = MyTimeUtils.getDifferenceStoppedTime(timeStart2, timeEnd1);
                            }
                            if (!last_item) {
                                total_time_stopped = total_time_stopped + myNum_time_stopped; // miliseconds
                                total_time_stopped_seconds = MyTimeUtils.getFormattedMilisecondsToSeconds(total_time_stopped);
                            }
                        }

                        TextView time_stopped = (TextView) findViewById(R.id.total_time_stopped);
                        time_stopped.setText(MyTimeUtils.getTotalTimeStoppedInMiliseconds(total_time_stopped));

                        int total_time = total_time_driving_and_return + total_time_stopped_seconds ;

                        System.out.println(total_time_driving_and_return + " ... " + MyTimeUtils.getFormattedHourMinutes(total_time_driving_and_return));
                        System.out.println(total_time_stopped_seconds + " ... " + MyTimeUtils.getTotalTimeStoppedInMiliseconds(total_time_stopped));
                        System.out.println(time_return + " ... " + MyTimeUtils.getFormattedHourMinutes(Integer.valueOf(time_return)));

                        time.setText(MyTimeUtils.getFormattedHourMinutes(total_time)); // from seconds to minutes or hours

                    }

                }
            });
            hideProgressDialog();
        }
    }

    public String getLetterFromNoddeNR(int num)
    {
        int letter1 = (num / 26);
        int letter2 = (num % 26) + 65;

        String letter = Character.toString((char) letter2);
        if (letter1 > 0) {
            letter = Character.toString((char) (letter1 + 64)) + Character.toString((char) letter2);
        }
        return letter;
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadPharmacyLocation extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadPharmacyLocation(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cPharmacyAddress address = MyProvider.getPharmacyAddress(pharmacyId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pharmacyAddress = address.address1 + "  " + address.address_house + " , " + address.address_zipcode + " , " + address.address_city + " , " + address.address_country;
                    pharmacyStreet = address.address1;
                    pharmacyHouse = address.address_house;
                    pharmacyZipcode = address.address_zipcode;
                    pharmacyCity = address.address_city;
                    pharmacyCountry = address.address_country;
                }
            });
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (HistoryDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            new LoadList().execute();
            loadMapByPharmacyAddress(idRoute,idPharmacy);
            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    public void loadMapByPharmacyAddress(String routeid, String pharmacy){
        String startpoint = pharmacyAddress;

        //	Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
        Log.d("startpoint", startpoint);

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        //String map = "bing_map_route_byid_copy.php"; // is google map
        String map = getString(R.string.google_map_multiple); // is google map
        double size_tablet = MyTimeUtils.tabletSize(HistoryDetailActivity.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 950, 650 , 0.0, 0.0, 0.0, 0.0, HistoryDetailActivity.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 450, 350 , 0.0, 0.0, 0.0, 0.0, HistoryDetailActivity.this));

        }else{
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1",routeid, pharmacy, 340, 180 , 0.0, 0.0, 0.0, 0.0, HistoryDetailActivity.this));
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }
}
