package com.sharethatdata.digiroute;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Adapter.SelectActionAdapter;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.digiroute.Scanner.IntentResult;
import com.sharethatdata.digiroute.Service.LocationReceiver;
import com.sharethatdata.digiroute.Service.MessageReceiver;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewButton;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseView;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.Target;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.ViewTarget;
import com.sharethatdata.digiroute.notification.MyReceiver;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cMessage;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPackagesbyDeliveryId;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.sharethatdata.digiroute.BarcodeReader.Barcode.BarcodeCaptureActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
//import android.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.sharethatdata.digiroute.R.id.toolbar;


/**
 * Created by mihu__000 on 1/24/2017.
 */

public class SelectActionActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    ProgressDialog progressDialog;

    final String TAG = "GPS";
    private static final int REQUEST_PERMISSIONS_LOCATION = 100;
    private static final int REQUEST_PERMISSIONS_CAMERA = 102;
    private static final int BARCODE_READER_REQUEST_CODE = 9001;
    private PendingIntent pendingIntent;
    protected LocationManager locationManager;
    boolean boolean_permission_camera = false;
    boolean boolean_permission_location = false;

    ArrayList<HashMap<String, String>> routeList; // List Collections
    ArrayList<HashMap<String, String>> packageList; // List Packages
    //String delivery_ids[];
    //String addresses[];

    SharedPreferences sharedPrefs;

    private String myUser = "";
    private String myPass = "";
    String pharmacyId = "";
    String routeId = "";
    String intervalLocationService = "";

    boolean isGPS = false;
    boolean isNetwork = false;
    boolean isWiFi = false;
    boolean one_time = false;
    boolean processing_scan = false;

    Dialog dialog;
    ListView modeList;
    EditText editText;

    private View view;
    Menu mmenu;
    ShowcaseView showcaseView;
    ShowcaseView showcaseViewCreateAddress;
    ShowcaseView showcaseViewScanAddress;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_action);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        String url_ws = Globals.getValue("ws_url");
        MyProvider.SetWebServiceUrl(url_ws);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SelectActionActivity.this, SelectActionActivity.this);

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);

        sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        final String packageLength = sharedPrefs.getString("prefScannerPackageLength", "1");
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        intervalLocationService = sharedPrefs.getString("prefLocationServiceInterval", "1"); // minutes
        boolean blLocationService = sharedPrefs.getBoolean("prefLocationService", false);
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        SharedPreferences.Editor editor = getSharedPreferences("PREF_Pharmacy", 0).edit();
        editor.putString("Pharmacy", String.valueOf(pharmacyId));
        editor.commit();

        Button btnSubmit1 = (Button) findViewById(R.id.btnSubmit1); // New Route
        Button btnSubmit2 = (Button) findViewById(R.id.btnSubmit2); // My Routes
        //Button btnSubmit3 = (Button) findViewById(R.id.btnSubmit3);
        editText = (EditText) findViewById(R.id.editText_barcode);
        ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit3); // scan package
        //modeList = new ListView(SelectActionActivity.this);

        TextView tv1=(TextView)findViewById(R.id.textDigiroute);
        TextView tv2=(TextView)findViewById(R.id.textDigiroute1);
        TextView tv3=(TextView)findViewById(R.id.textDigiroute2);
        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Regular.ttf");
        Typeface face1=Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.otf");
        Typeface face2=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
        tv1.setTypeface(face1);
        tv2.setTypeface(face1);
        tv3.setTypeface(face1);

        btnSubmit1.setTypeface(face2);
        btnSubmit2.setTypeface(face2);
        editText.setTypeface(face2);

        if (blCameraScanner)
        {
            imageViewSubmit1.setVisibility(View.VISIBLE);
            editText.setHint(getString(R.string.select_action_text3));
        } else {
            imageViewSubmit1.setVisibility(View.GONE);
           // editText.setHint(getString(R.string.select_action_text4));
            editText.setHint(getString(R.string.select_action_text3));
        }

        imageViewSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                permission_camera();

                if(boolean_permission_camera){
                    Log.d(TAG, "Camera on");

                    Intent intent = new Intent(SelectActionActivity.this, BarcodeCaptureActivity.class);
                    startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);

                }else{
                    Log.d(TAG, "Camera off");
                    //showSettingsAlert();

                    //Toast.makeText(getApplicationContext(), "Please enable the camera permission", Toast.LENGTH_SHORT).show();
                }

                // create barcode intent
                /*IntentIntegrator integrator = new IntentIntegrator(SelectActionActivity.this);
                integrator.initiateScan();*/
            }
        });

        btnSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectActionActivity.this, NewOrderActivity.class);
                startActivity(intent);

            }
        });

        btnSubmit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set empty value update_status_package, because when I go to AddressDetail from SelectAction
                // (through MyRoutes)
                // is loading the Address with empty values, and cannot end route
                // update_status_package value is available if you go back from Deliver to AddressDetail
                Globals.setValue("update_status_package","");

                Intent intent = new Intent(SelectActionActivity.this, MyRoutesActivity.class);
                startActivity(intent);

            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    editText = (EditText) findViewById(R.id.editText_barcode);
                    String bc = editText.getText().toString();

                        // strip control chars
                        bc = bc.replace("*", "");
                        bc = bc.replace("\n", "");
                        bc = bc.replace("\r", "");

                    new LoadPackages(bc, pharmacyId).execute();
                    return true;
                }
                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                editText = (EditText) findViewById(R.id.editText_barcode);
                String bc = editText.getText().toString();

                if (!processing_scan) {

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    if (bc.length() == Integer.valueOf(packageLength)) {
                        processing_scan = true;
                        new LoadPackages(bc, pharmacyId).execute();
                    }
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        // check for messages
        new LoadMessage().execute();

        if(!isMessageServiceRunning()){

            startServiceMessage();
        } else {
             //Toast.makeText(getApplicationContext(), "Message Service is already running", Toast.LENGTH_SHORT).show();
        }


        if(blLocationService){
            permission_location();

            isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if(boolean_permission_location){
                if (!isGPS && !isNetwork) {
                    Log.d(TAG, "Connection off");
                    showSettingsAlertLocation();
                } else {
                    Log.d(TAG, "Connection on");

                    if(!isServiceRunning()){

                        long interval = Integer.valueOf(intervalLocationService) * 60 * 1000;


                        TimeZone tz = TimeZone.getDefault();
                        Calendar c = Calendar.getInstance(tz);
                        long when = c.getTimeInMillis();
                        String timee = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+":"+
                                String.format("%02d" , c.get(Calendar.MINUTE))+":"+
                                String.format("%02d" , c.get(Calendar.SECOND))+":"+
                                String.format("%03d" , c.get(Calendar.MILLISECOND));

                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(SelectActionActivity.this, LocationReceiver.class);
                        intent.putExtra("service","start");
                        sendBroadcast(intent);
                        //pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                            pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_MUTABLE);
                        }else{
                            pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }

                        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_text_location_service), Toast.LENGTH_SHORT).show();
                    }
                }
            }else{
                Toast.makeText(getApplicationContext(), getString(R.string.toast_text_enable_gps), Toast.LENGTH_SHORT).show();
            }

        }else{
            try {
                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                manager.cancel(pendingIntent);
                System.out.println("Alarm canceled ");
            } catch (Exception e) {
                System.out.println("Error when cancelling: "+e.toString());
            }
        }

        if(!isNotificationServiceRunning()){

            long interval = 1 * 60 * 1000;

            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(SelectActionActivity.this, MessageReceiver.class);
            intent.putExtra("service","start");
            sendBroadcast(intent);
            //pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_MUTABLE);
            }else{
                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        } else {
            // Toast.makeText(getApplicationContext(), "Message Service is already running", Toast.LENGTH_SHORT).show();
        }

        new LoadRoute(pharmacyId).execute();



        //Tutorial
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setLogo(R.drawable.logo_rpchh);

        /*view = getActionBarView();
        Target viewTarget = new ViewTarget(view);
        new ShowcaseView.Builder(this)
                .setTarget(viewTarget)
                .setContentTitle("Action Bar")
                .setContentText("Here you can ...")
                .build();*/

        /*Target viewTarget1 = new ViewTarget(R.id.action_add_address, this);
        new ShowcaseView.Builder(this)
                .setTarget(viewTarget1)
                .setContentTitle("Button")
                .setContentText("Here you can ...")
                .build();*/


        /*try {
            ViewTarget navigationButtonViewTarget = ViewTargets.navigationButtonViewTarget(toolbar);
            new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(navigationButtonViewTarget)
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .setContentText("Here's how to highlight items on a toolbar")
                    .build()
                    .show();
        } catch (ViewTargets.MissingViewException e) {
            e.printStackTrace();
        }*/

        /*Target viewTarget = new ViewTarget(mOptionsMenu, this);
        new ShowcaseView.Builder(this)
                .setTarget(viewTarget)
                .setContentTitle("Button")
                .setContentText("Here you can ...")
                .singleShot(42)
                .build();*/


        initializeShowCaseView(); // initialise the tutorial with show case view


        //throw new RuntimeException("Test report");

}

    public View getActionBarView() {
        Window window = getWindow();
        View v = window.getDecorView();
        int resId = getResources().getIdentifier("action_bar", "id", "android");
        //int resId = getResources().getIdentifier("ic_delete", "drawable", this.getPackageName());
        return v.findViewById(resId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String UPCScanned = barcode.displayValue.toString();
                    editText = (EditText) findViewById(R.id.editText_barcode);
                    editText.setText(UPCScanned);

                    String bc = editText.getText().toString();
                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    new LoadPackages(bc, pharmacyId).execute();
                } else
                    Toast.makeText(SelectActionActivity.this, getString(R.string.toast_text_no_barcode_captured), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(SelectActionActivity.this, getString(R.string.textView_text_error) + " " + CommonStatusCodes.getStatusCodeString(resultCode), Toast.LENGTH_SHORT).show();
        }

        /*IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null)
        {
            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                editText = (EditText) findViewById(R.id.editText_barcode);
                editText.setText(UPCScanned);

                String bc = editText.getText().toString();
                // strip control chars
                bc = bc.replace("*", "");
                bc = bc.replace("\n", "");
                bc = bc.replace("\r", "");

                new LoadPackages(bc, pharmacyId).execute();

            }
        }*/

        if (requestCode == 1) {
            switch (requestCode) {
                case 1:
                    isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (!isGPS && !isNetwork) {
                        Log.d(TAG, "Connection off");
                        showSettingsAlertLocation();
                    } else {
                        Log.d(TAG, "Connection on");

                        if (!isServiceRunning()){
                            long interval = Integer.valueOf(intervalLocationService) * 60 * 1000;

                            TimeZone tz = TimeZone.getDefault();
                            Calendar c = Calendar.getInstance(tz);
                            long when = c.getTimeInMillis();
                            String timee = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+":"+
                                    String.format("%02d" , c.get(Calendar.MINUTE))+":"+
                                    String.format("%02d" , c.get(Calendar.SECOND))+":"+
                                    String.format("%03d" , c.get(Calendar.MILLISECOND));

                            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            Intent intent = new Intent(SelectActionActivity.this, LocationReceiver.class);
                            intent.putExtra("service","start");
                            sendBroadcast(intent);
                            //pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_MUTABLE);
                            }else{
                                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                            }

                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                        }
                    }
                    break;
            }
        }
    }

    public void initializeShowCaseView(){
        double size_tablet = MyTimeUtils.tabletSize(getApplicationContext());
        if (size_tablet > 9) {
            //Device is a 10" tablet
            Log.d("size_tablet", String.valueOf(size_tablet));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("1", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(this)
                        .setTarget(Target.NONE_MENU_TOOLBAR_LANDSCAPE_10)
                        .setStyle(R.style.CustomShowcaseTheme2)
                        .setContentTitle(getString(R.string.title_welcome))
                        .setContentText(getString(R.string.text_welcome))
                        .singleShot(1)
                        .build();
                // mark first time has runned.
                SharedPreferences.Editor editor1 = prefs.edit();
                editor1.putBoolean("1", true);
                editor1.commit();
            }

        }else if (size_tablet > 7) {
            //Device is a 8" tablet
            Log.d("size_tablet", String.valueOf(size_tablet));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("2", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(this)
                        .setTarget(Target.NONE_MENU_TOOLBAR_LANDSCAPE_8)
                        .setStyle(R.style.CustomShowcaseTheme2)
                        .setContentTitle(getString(R.string.title_welcome))
                        .setContentText(getString(R.string.text_welcome))
                        .singleShot(2)
                        .build();

                // mark first time has runned.
                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putBoolean("2", true);
                editor2.commit();
            }

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            Log.d("size_tablet", String.valueOf(size_tablet));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("3", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(this)
                        .setTarget(Target.NONE_MENU_TOOLBAR_LANDSCAPE_7)
                        .setStyle(R.style.CustomShowcaseTheme2)
                        .setContentTitle(getString(R.string.title_welcome))
                        .setContentText(getString(R.string.text_welcome))
                        .singleShot(3)
                        .build();

                // mark first time has runned.
                SharedPreferences.Editor editor3 = prefs.edit();
                editor3.putBoolean("3", true);
                editor3.commit();
            }

        }else{
            // Device is a phone
            if(size_tablet < 4.70){
                // Device is a phone with 5 inch
                Log.d("size_tablet", String.valueOf(size_tablet));
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (!prefs.getBoolean("4", false)) {
                    // <---- run one time code here
                    showcaseView = new ShowcaseView.Builder(this)
                            .setTarget(Target.NONE_MENU_TOOLBAR_PORTRAIT_5)
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .setContentTitle(getString(R.string.title_welcome))
                            .setContentText(getString(R.string.text_welcome))
                            .singleShot(4)
                            .build();

                    // mark first time has runned.
                    SharedPreferences.Editor editor4 = prefs.edit();
                    editor4.putBoolean("4", true);
                    editor4.commit();
                }

            } else if(size_tablet < 4.99){
                // Device is a phone with 5.5 inch
                Log.d("size_tablet", String.valueOf(size_tablet));
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (!prefs.getBoolean("5", false)) {
                    // <---- run one time code here
                    showcaseView = new ShowcaseView.Builder(this)
                            .setTarget(Target.NONE_MENU_TOOLBAR_PORTRAIT_5_5)
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .setContentTitle(getString(R.string.title_welcome))
                            .setContentText(getString(R.string.text_welcome))
                            .singleShot(5)
                            .build();

                    // mark first time has runned.
                    SharedPreferences.Editor editor5 = prefs.edit();
                    editor5.putBoolean("5", true);
                    editor5.commit();
                }

            }else {
                // Device is a phone with more than 5.5 inch
                Log.d("size_tablet", String.valueOf(size_tablet));
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (!prefs.getBoolean("6", false)) {
                    // <---- run one time code here
                    showcaseView = new ShowcaseView.Builder(this)
                            .setTarget(Target.NONE_MENU_TOOLBAR_PORTRAIT_6)
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .setContentTitle(getString(R.string.title_welcome))
                            .setContentText(getString(R.string.text_welcome))
                            .singleShot(6)
                            .build();

                    // mark first time has runned.
                    SharedPreferences.Editor editor6 = prefs.edit();
                    editor6.putBoolean("6", true);
                    editor6.commit();
                }

            }
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadRoute extends AsyncTask<String, String, String> {

        public String pharmacyId;

        public LoadRoute(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            // load data from provider
            routeList = new ArrayList<HashMap<String, String>>();

            List<cMyRoute> myRoute = MyProvider.getMyRoutes("", pharmacyId,"","","");
            for(cMyRoute entry : myRoute)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id/status/status_description/description", entry.id+"/"+entry.status+"/"+entry.status_description+
                        "/"+entry.description);

                // adding HashList to ArrayList
                routeList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (SelectActionActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    // check if route exist
                    for (HashMap<String, String> map1 : routeList){
                        for (Map.Entry<String, String> entry1 : map1.entrySet())
                        {
                            String row = String.valueOf(entry1.getValue().toString());
                            String[] parts = row.split("/");
                            String id = parts[0];
                            String status = parts[1];
                            String status_description = parts[2];
                            String description = parts[3];

                            // COMMENT_MV 2017-08-31 this is not good, compare with id, when status name change in database this breaks.

                            // 1 = Loaded for delivery
                            if(status.equals("1")){
                                // route exist
                                routeId = id;
                            }
                        }
                    }

                    if(routeId.equals("")){
                        // route doesn't exist, so create new route
                        new CreateRoute(pharmacyId).execute();
                    }
                }
            });

            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadPackages extends AsyncTask<String, String, String> {

        public String barcode, pharmacyId;
        List<String> slist = new ArrayList<String> ();

        public LoadPackages(String barcode, String pharmacyId){
            this.barcode = barcode;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            packageList = new ArrayList<HashMap<String, String>>();

            List<cPackages> myPackages = MyProvider.getPackagesByBarcode(barcode, pharmacyId);
            for(cPackages entry : myPackages)
            {
                // creating new HashMap
                final HashMap<String, String> map = new HashMap<String, String>();

                map.put("packageid",  entry.packageid);
                map.put("addressid",  entry.addressid);
                map.put("route",  routeId);
                map.put("location",  entry.location);
                map.put("delivery_id",  entry.delivery_id);
                map.put("package_barcode",  entry.package_barcode);
                map.put("scriptid",  entry.customer_script);
                map.put("customer_street", entry.customer_street);
                map.put("customer_house", entry.customer_house);
                map.put("customer_zipcode", entry.customer_zipcode);
                map.put("customer_city", entry.customer_city);
                map.put("customer_country", entry.customer_country);
                map.put("customer_name", entry.customer_name);
                map.put("package_status",  entry.package_status);
                map.put("package_status_description",  entry.package_status_description);

                slist.add(entry.customer_street);
                slist.add(entry.customer_house);
                slist.add(entry.customer_zipcode);
                slist.add(entry.customer_city);
                slist.add(entry.customer_country);

                List<cPackagesbyDeliveryId> myPackage = MyProvider.getPackagesbyDeliveryId(entry.delivery_id, pharmacyId);
                for(final cPackagesbyDeliveryId entry1 : myPackage){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String status_id = "";
                            if (entry1 != null) {
                                status_id = entry1.status;
                                map.put("status_id",  entry1.status);
                                if(!entry1.routeid.equals("null")){
                                    map.put("route",  entry1.routeid);
                                }

                            }
                        }
                    });
                }

                // adding HashList to ArrayList
                packageList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (SelectActionActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ListAdapter adapter = null;
                    adapter = new SelectActionAdapter(SelectActionActivity.this, R.layout.activity_select_action_listview_items, packageList);

                    /*if(packageList.size() > 0){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SelectActionActivity.this);
                        alertDialog.setTitle("Choose a package");
                        alertDialog.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(SelectActionActivity.this, String.valueOf(which), Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.show();
                    }else{
                        Toast.makeText(SelectActionActivity.this, "No packages", Toast.LENGTH_SHORT).show();
                    }*/

                    if(packageList.size() > 0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SelectActionActivity.this);
                        builder.setTitle(getString(R.string.alert_dialog_title_choose_package));

                        modeList = new ListView(SelectActionActivity.this);
                        modeList.setAdapter(adapter);
                        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
                            {
                                final String idPackage = ((TextView) itemClicked.findViewById(R.id.textViewPackageID)).getText().toString();
                                final String idDelivery = ((TextView) itemClicked.findViewById(R.id.textViewDeliveryID)).getText().toString();
                                final String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
                                final String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewAddressID)).getText().toString();
                                final String idRoute = ((TextView) itemClicked.findViewById(R.id.textViewRoute)).getText().toString();
                                final String barcode = ((TextView) itemClicked.findViewById(R.id.packageBarcode)).getText().toString();
                                final String address = ((TextView) itemClicked.findViewById(R.id.packageAddress)).getText().toString();
                                final String status_description = ((TextView) itemClicked.findViewById(R.id.packageStatusDescription)).getText().toString();
                                final String status_id = ((TextView) itemClicked.findViewById(R.id.textViewPackageStatusId)).getText().toString();

                                // 1 = Loaded for delivery
                                if(status_id.equals("1")){
                                    new AlertDialog.Builder(SelectActionActivity.this)
                                            .setTitle(getString(R.string.alert_dialog_title_package))
                                            .setMessage(getString(R.string.alert_dialog_text_choose_package))
                                            .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    new AddPackageToRoute(pharmacyId, idDelivery, idRoute, idPackage, idScript, idAddress, barcode, address, status_id).execute();

                                                }
                                            })
                                            .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // do nothing
                                                    editText = (EditText) findViewById(R.id.editText_barcode);
                                                    editText.setText(null);
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }else{
                                    // start detail activity
                                    Intent intent = new Intent(getApplicationContext(), DeliverActivity.class);
                                    intent.putExtra("quick_scan", true);
                                    intent.putExtra("idAddress", idAddress);
                                    intent.putExtra("idPackage", idPackage);
                                    intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("packageIdCode", barcode);
                                    intent.putExtra("packageStatus", status_description);
                                    intent.putExtra("packageStatusID", status_id);
                                    intent.putExtra("packageAddress", address);
                                    //intent.putExtra("idRoute", idRoute);
                                    intent.putExtra("idScript", idScript);
                                    intent.putExtra("idDelivery", idDelivery);
                                    startActivity(intent);
                                }

                                dialog.dismiss();

                            }

                        });

                        builder.setView(modeList);

                        dialog = new Dialog(SelectActionActivity.this);
                        dialog = builder.create();
                        dialog.show();
                    }else{
                        Toast.makeText(SelectActionActivity.this, getString(R.string.toast_text_no_package) + " " + barcode, Toast.LENGTH_LONG).show();


                    }

                    // always clear the textbox
                    editText = (EditText) findViewById(R.id.editText_barcode);
                    editText.setText(null);


                }
            });

            processing_scan = false;
            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class AddPackageToRoute extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String deliveryId = "";
        String routeId = "";
        String idPackage = "";
        String idScript = "";
        String idAddress = "";
        String barcode = "";
        String address = "";
        String status = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public AddPackageToRoute(String pharmacyId, String deliveryId, String routeId, String idPackage, String idScript,
                                 String idAddress, String barcode, String address, String status){
            this.pharmacyId = pharmacyId;
            this.deliveryId = deliveryId;
            this.routeId = routeId;
            this.idPackage = idPackage;
            this.idScript = idScript;
            this.idAddress = idAddress;
            this.barcode = barcode;
            this.address = address;
            this.status = status;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            boolean suc = false;

            suc = MyProvider.addPackageToRoute(pharmacyId, deliveryId, routeId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (SelectActionActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Add package to route", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SelectActionActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SelectActionActivity.this, getString(R.string.toast_text_failed_package_add_to_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setText(null);
                    }
                    else
                    {
                        Log.d("Add package to route", "true");
                        new UpdatePackageStatus(pharmacyId, deliveryId, "3", routeId, idPackage, idScript,
                                 idAddress, barcode, address, status).execute();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdatePackageStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String deliveryId = "";
        String statusId = "";
        String idRoute = "";
        String idPackage = "";
        String idScript = "";
        String idAddress = "";
        String barcode = "";
        String address = "";
        String status = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdatePackageStatus(String pharmacyId, String deliveryId, String statusId, String idRoute, String idPackage, String idScript,
                                   String idAddress, String barcode, String address, String status){
            this.pharmacyId = pharmacyId;
            this.deliveryId = deliveryId;
            this.statusId = statusId;
            this.idRoute = idRoute;
            this.idPackage = idPackage;
            this.idScript = idScript;
            this.idAddress = idAddress;
            this.barcode = barcode;
            this.address = address;
            this.status = status;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            boolean suc = false;

            suc = MyProvider.updatePackageStatus(pharmacyId, deliveryId, statusId, 0.0, 0.0);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (SelectActionActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Package Status", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SelectActionActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SelectActionActivity.this, getString(R.string.toast_text_failed_update_package) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setText(null);
                    }
                    else
                    {
                        Log.d("Update Package Status", "true");

                            //new LoadPackages(barcode, pharmacyId).execute();

                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setText(null);

                        // start deliver activity
                        /*Intent intent = new Intent(getApplicationContext(), DeliverActivity.class);
                        intent.putExtra("quick_scan", true);
                        intent.putExtra("idAddress", idAddress);
                        intent.putExtra("idPackage", idPackage);
                        intent.putExtra("idRoute", idRoute);
                        intent.putExtra("packageIdCode", barcode);
                        intent.putExtra("packageStatus", status);
                        intent.putExtra("packageAddress", address);
                        intent.putExtra("idScript", idScript);
                        intent.putExtra("idDelivery", deliveryId);
                        startActivity(intent);*/
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask create route report  */
    private class CreateRoute extends AsyncTask<String, String, Integer>{
        String pharmacyId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public CreateRoute(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        @Override
        protected Integer doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            int suc = MyProvider.createRoute(pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Integer success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success != 0)
                    {
                        Log.d("create route", "true");
                        routeId = String.valueOf(success);
                    }
                    else
                    {
                        Log.d("create route", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SelectActionActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SelectActionActivity.this, getString(R.string.toast_text_failed_create_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            hideProgressDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mmenu = menu;
        String isManager = Globals.getValue("manager");
        if (isManager == "yes") {
            getMenuInflater().inflate(R.menu.select_action_manager, menu);
        }else{
            getMenuInflater().inflate(R.menu.select_action, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent);
                return true;
            case R.id.action_history:
                Intent intent1 = new Intent(this, HistoryActivity.class); startActivity(intent1);
                return true;
            case R.id.action_profile:
                Intent intent2 = new Intent(this, ProfileActivity.class); startActivity(intent2);
                return true;
            case R.id.action_add_address:
                Intent intent3 = new Intent(this, CreateAddressActivity.class); startActivity(intent3);
                return true;
            case R.id.action_default_stops:
                Intent intent4 = new Intent(this, DefaultStopsActivity.class); startActivity(intent4);
                return true;
            case R.id.action_send_message:
                Intent intent5 = new Intent(this, MessageActivity.class); startActivity(intent5);
                return true;
            case R.id.action_user_location:
                Intent intent6 = new Intent(this, UserLocationActivity.class); startActivity(intent6);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // camera //
    private void permission_camera() {
        if (ContextCompat.checkSelfPermission(SelectActionActivity.this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(SelectActionActivity.this,
                    android.Manifest.permission.CAMERA))) {


            } else {
                ActivityCompat.requestPermissions(SelectActionActivity.this,
                        new String[]{android.Manifest.permission.CAMERA
                }, REQUEST_PERMISSIONS_CAMERA);
            }
        } else {
            boolean_permission_camera = true;
        }

    }


    // gps //
    private void permission_location() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(SelectActionActivity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION))) {


            } else {
                ActivityCompat.requestPermissions(SelectActionActivity.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION
                }, REQUEST_PERMISSIONS_LOCATION);
            }
        } else {
            boolean_permission_location = true;
        }

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission_location = true;

                    isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (!isGPS && !isNetwork) {
                        Log.d(TAG, "Connection off");
                        showSettingsAlertLocation();
                    } else {
                        Log.d(TAG, "Connection on");

                        if (!isServiceRunning()){
                            long interval = Integer.valueOf(intervalLocationService) * 60 * 1000;

                            TimeZone tz = TimeZone.getDefault();
                            Calendar c = Calendar.getInstance(tz);
                            long when = c.getTimeInMillis();
                            String timee = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+":"+
                                    String.format("%02d" , c.get(Calendar.MINUTE))+":"+
                                    String.format("%02d" , c.get(Calendar.SECOND))+":"+
                                    String.format("%03d" , c.get(Calendar.MILLISECOND));

                            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            Intent intent = new Intent(SelectActionActivity.this, LocationReceiver.class);
                            intent.putExtra("service","start");
                            sendBroadcast(intent);
                            //pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_MUTABLE);
                            }else{
                                pendingIntent = PendingIntent.getBroadcast(SelectActionActivity.this, 5, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            }

                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_text_permission_location), Toast.LENGTH_LONG).show();
                }

            case REQUEST_PERMISSIONS_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission_camera = true;

                        //showSettingsAlert();
                        Log.d(TAG, "Camera on");

                } else {
                    Log.d(TAG, "Camera off");
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_text_permission_camera), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if("com.sharethatdata.digiroute.Service.LocationService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isMessageServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if("com.sharethatdata.digiroute.notification.MyService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isNotificationServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if("com.sharethatdata.digiroute.Service.MessageService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /*private boolean isMessageServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if("com.sharethatdata.digiroute.Service.MessageService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }*/

    public void startServiceMessage(){

   	 /*  Intent intent=new Intent(context, MyService.class);
   	   context.startService(intent);*/

        AlarmManager alarmManagerMessage = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent intentMessage = new Intent(getApplicationContext(), MyReceiver.class);
        //PendingIntent pendingIntent= PendingIntent.getBroadcast(getApplicationContext(), 0, intentMessage, 0);
        PendingIntent pendingIntent = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent= PendingIntent.getBroadcast(getApplicationContext(), 0, intentMessage, PendingIntent.FLAG_IMMUTABLE);
        }else{
            pendingIntent= PendingIntent.getBroadcast(getApplicationContext(), 0, intentMessage, 0);
        }

        //SharedPreferences sharedPrefs = context.getSharedPreferences("defaults", context.MODE_PRIVATE );
        //boolean blNotifications = sharedPrefs.getBoolean("prefNotifications", true);
        //if(blNotifications){
        // true - start service
        alarmManagerMessage.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),1000 * 60 * 3,
                pendingIntent); // 60000 1 min - 1000 * 60 * 3 = 180000 3 min - 300000 5 min
        //}else{
        // false - stop service
        //AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //manager.cancel(pendingIntent);
        //}

    }

    /**
     * Background Async Task to Load new message by making HTTP Request
     * */
    class LoadMessage extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         */
        protected Boolean doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());

            MyProvider = new WSDataProvider(myUser, myPass);
            String url_ws = Globals.getValue("ws_url");
            MyProvider.SetWebServiceUrl(url_ws);

            cMessage message = MyProvider.getNewMessage(pharmacyId);

            boolean suc = false;
            if (message.id > 0)
            {
                suc = true;
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(Boolean result) {

            if (result == true) {

                // load new message activity
                Intent intent = new Intent(SelectActionActivity.this, NewMessageActivity.class);
                startActivity(intent);
            }

        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onResume(){
        super.onResume();

        //initializeShowCaseView(); // initialise the tutorial with show case view

        if(one_time){
            String refresh_route = Globals.getValue("refresh_route");
            if(!refresh_route.equals("")){
                // hide tutorial when come back from MyRoutes
                try
                {
                    if(!Globals.getValue("tutorial_scanned_package").equals("")){
                        try{
                            //if(showcaseViewScanAddress != null){
                                showcaseViewScanAddress.hide();
                            //}
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Globals.setValue("tutorial_scanned_package","");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                editText = (EditText) findViewById(R.id.editText_barcode);
                editText.setEnabled(true);

                routeId = "";
                Globals.setValue("refresh_route","");
                new LoadRoute(pharmacyId).execute();
            }else{
                //routeId = "";
                new LoadRoute(pharmacyId).execute();
            }

            try{
                // check first to don't be null
                showcaseView.hide();
            }catch (Exception e){
                e.printStackTrace();
            }

            // the address was created, next step - the user must scan the address
            if(!Globals.getValue("create_address").equals("")){
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (!prefs.getBoolean("7", false)) {
                    // <---- run one time code here
                    showcaseViewCreateAddress = new ShowcaseView.Builder(this)
                            .setTarget(new ViewTarget(R.id.btnSubmit1, this))
                            .setContentTitle(getString(R.string.title_next_step))
                            .setContentText(getString(R.string.text_next_step))
                            .setShowcaseDrawer(new CustomShowcaseViewButton(getResources()))
                            .singleShot(7)
                            .build();

                    // mark first time has runned.
                    SharedPreferences.Editor editor7 = prefs.edit();
                    editor7.putBoolean("7", true);
                    editor7.commit();
                }

                try{
                    if(showcaseViewCreateAddress.isShowing()){
                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setEnabled(false);
                    }else{
                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setEnabled(true);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                Globals.setValue("create_address","");
            }else {
                editText = (EditText) findViewById(R.id.editText_barcode);
                editText.setEnabled(true);
            }

            // the address was scanned, next step - the user must go to MyRoutes, to deliver the package from address
            if(!Globals.getValue("add_package_to_route").equals("")){
                try{
                    showcaseViewCreateAddress.hide();
                }catch (Exception e){
                    e.printStackTrace();
                }
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (!prefs.getBoolean("8", false)) {
                    // <---- run one time code here
                    showcaseViewScanAddress = new ShowcaseView.Builder(this)
                            .setTarget(new ViewTarget(R.id.btnSubmit2, this))
                            .setContentTitle(getString(R.string.title_next_step_2))
                            .setContentText(getString(R.string.text_next_step_2))
                            .setShowcaseDrawer(new CustomShowcaseViewButton(getResources()))
                            .singleShot(8)
                            .build();

                    // mark first time has runned.
                    SharedPreferences.Editor editor8 = prefs.edit();
                    editor8.putBoolean("8", true);
                    editor8.commit();
                }

                try{
                    if(showcaseViewScanAddress.isShowing()){
                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setEnabled(false);
                    }else{
                        editText = (EditText) findViewById(R.id.editText_barcode);
                        editText.setEnabled(true);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                Globals.setValue("add_package_to_route","");
            }else {
                editText = (EditText) findViewById(R.id.editText_barcode);
                editText.setEnabled(true);
            }

            //editText = (EditText) findViewById(R.id.editText_barcode);
            //editText.setText("");

        }else {
            one_time = true;

            editText = (EditText) findViewById(R.id.editText_barcode);
            editText.setText("");
        }



    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask the user if they want to quit
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.alert_dialog_title_quit))
                    .setMessage(getString(R.string.alert_dialog_text_quit))
                    .setPositiveButton(R.string.btnYes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Stop the activity
                            SelectActionActivity.this.finish();
                        }
                    })
                    .setNegativeButton(R.string.btnNo, null)
                    .show();

            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public void showSettingsAlertLocation() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.alert_dialog_title_gps_not_enabled));
        alertDialog.setMessage(getString(R.string.alert_dialog_text_gps_not_enabled));
        alertDialog.setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(intent);
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(SelectActionActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

/*    @Override
    protected void onStop(){
        prefEditor.putString("service", "").commit();

        Intent intent = new Intent(SelectActionActivity.this, LocationService.class);
        stopService(intent);

        Log.d("Stop Service","Stop Service");

        hideProgressDialog();
        super.onStop();
    }*/

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(SelectActionActivity.this, LocationReceiver.class);
        intent.putExtra("service","stop");
        sendBroadcast(intent);
        stopService(intent);

        Log.d("Stop Service","Stop Service");

        hideProgressDialog();
        super.onDestroy();
    }



}
