package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mihu__000 on 3/7/2017.
 */

public class HistoryAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    public HistoryAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.id = (TextView)view.findViewById(R.id.textViewID);
            holder.date = (TextView)view.findViewById(R.id.textViewDate);
            holder.time = (TextView)view.findViewById(R.id.textViewTime);
            holder.name = (TextView)view.findViewById(R.id.textViewName);
            holder.subtitle = (TextView)view.findViewById(R.id.textViewFunc);
            holder.total_time = (TextView)view.findViewById(R.id.total_time);
            holder.total_time_textView = (TextView)view.findViewById(R.id.total_time_textView);
            holder.total_distance = (TextView)view.findViewById(R.id.total_distance);
            holder.total_distance_textView = (TextView)view.findViewById(R.id.total_distance_textView);
            holder.content = (RelativeLayout)view.findViewById(R.id.layoutRoutes);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        String timeString = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date dt;
        try {
            dt = sdf.parse(hashmap_Current.get("time"));
            timeString = sdf.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.id.setText(hashmap_Current.get("id"));
        holder.date.setText(Html.fromHtml(hashmap_Current.get("date")));
        holder.time.setText(Html.fromHtml(timeString));
        holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
        holder.subtitle.setText(Html.fromHtml(hashmap_Current.get("subtitle")));
        holder.total_time.setText(Html.fromHtml(hashmap_Current.get("total_time")));
        holder.total_distance.setText(Html.fromHtml(hashmap_Current.get("total_distance")));

        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "subtitle")
            {

                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    holder.subtitle.setTextColor(context.getResources().getColor(R.color.orange7button));

                    /*if(row.equals("new route ") || row.equals("New route ")){
                        holder.subtitle.setTextColor(context.getResources().getColor(R.color.orange7button));
                    }else {
                        holder.subtitle.setTextColor(context.getResources().getColor(R.color.grey08));
                    }*/

                } catch(NumberFormatException nfe) {

                }

            }
        }



/*        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "unscanned_packages")
            {

                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if(row.equals("0")){
                        // scanned - green - in transport
                        holder.content.setBackgroundResource(R.color.green_transparent80);
                    }else {
                        // unscanned - white - loaded for delivery
                        holder.content.setBackgroundResource(R.color.white1);
                    }

                } catch(NumberFormatException nfe) {

                }

            }
        }

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "customer_script")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);

                    if((row.equals("Pharmacy"))){
                        // white
                        holder.customer_total_packages.setVisibility(View.GONE);
                        holder.customer_scanned_packages.setVisibility(View.GONE);
                    }

                } catch(NumberFormatException nfe) {

                }
            }
        }*/

        return view;
    }

    final class ViewHolder {

        public TextView id, date, time, name, subtitle, total_time, total_distance, total_time_textView, total_distance_textView;
        public RelativeLayout content;



    }

}

