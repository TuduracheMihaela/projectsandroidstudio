package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mihu__000 on 5/16/2017.
 */

public class AddressDetailPackageAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;

    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    public AddressDetailPackageAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {

            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.packageid = (TextView)view.findViewById(R.id.textViewID);
            holder.delivery_id_invisible = (TextView)view.findViewById(R.id.tv_delivery_id);
            holder.delivery_id = (TextView)view.findViewById(R.id.packageDeliveryId);
            holder.script = (TextView)view.findViewById(R.id.textViewScript);
            holder.packageBarcode = (TextView)view.findViewById(R.id.packageBarcode);
            holder.packageBarcodeLabel = (TextView)view.findViewById(R.id.tv_package_barcode_label);
            holder.packageStatus = (TextView)view.findViewById(R.id.packageStatusDescription);
            holder.packageStatusID = (TextView)view.findViewById(R.id.tv_status_id);
            holder.packageStatusLabel = (TextView)view.findViewById(R.id.tv_package_status_label);
            holder.markerPackage = (ImageView) view.findViewById(R.id.marker_package);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.packageid.setText(Html.fromHtml(hashmap_Current.get("packageid")));
        holder.delivery_id_invisible.setText(Html.fromHtml(hashmap_Current.get("delivery_id")));
        holder.delivery_id.setText(Html.fromHtml(hashmap_Current.get("delivery_id")));
        holder.script.setText(Html.fromHtml(hashmap_Current.get("script_id")));
        holder.packageBarcode.setText(Html.fromHtml(hashmap_Current.get("barcode")));
        holder.packageStatus.setText(Html.fromHtml(hashmap_Current.get("status_description")));
        holder.packageStatusID.setText(Html.fromHtml(hashmap_Current.get("status_id")));

        holder.packageBarcode.setTypeface(font1);
        holder.packageBarcodeLabel.setTypeface(font1);
        holder.packageStatus.setTypeface(font1);
        holder.packageStatusLabel.setTypeface(font1);

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "status_id")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);

                    // 1 = Loaded for delivery ; 3 = in transport
                    if((row.equals("1")) || row.equals("3")){
                        // check mark gray
                        holder.markerPackage.setImageResource(R.drawable.checkmark_gray);

                    // 5 = delivered
                    }else if(row.equals("5")){
                        // check mark green
                        holder.markerPackage.setImageResource(R.drawable.checkmark_green);
                        holder.packageStatus.setTextColor(context.getResources().getColor(R.color.green));
                    } else{
                        // check mark red
                        holder.markerPackage.setImageResource(R.drawable.checkmark_red);
                        holder.packageStatus.setTextColor(context.getResources().getColor(R.color.red));
                    }
                } catch(NumberFormatException nfe) {

                }
            }
        }

        return view;
    }

    final class ViewHolder {

        public TextView packageid, delivery_id_invisible,delivery_id, script, packageBarcode, packageStatus, packageStatusID;
        public TextView packageBarcodeLabel, packageStatusLabel;
        public ImageView markerPackage;
        public LinearLayout content;

    }
}
