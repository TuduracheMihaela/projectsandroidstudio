package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mihu__000 on 3/7/2017.
 */

public class zNewOrderAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    public zNewOrderAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.id = (TextView)view.findViewById(R.id.textViewID);
            holder.customer_script = (TextView)view.findViewById(R.id.customerScript);
            holder.customer_barcode = (TextView)view.findViewById(R.id.customerBarcode);
            holder.customer_total_packages = (TextView)view.findViewById(R.id.customerTotalPackages);
            holder.customer_scanned_packages = (TextView)view.findViewById(R.id.customerScannedPackages);
            holder.customer_street = (TextView)view.findViewById(R.id.customerStreet);
            holder.letter = (TextView)view.findViewById(R.id.textViewLetterAddress);
            holder.customer_distance = (TextView)view.findViewById(R.id.customerDistance);
            holder.customer_time = (TextView)view.findViewById(R.id.customerTime);
            holder.content = (LinearLayout)view.findViewById(R.id.layoutAddresses);

            holder.customer_barcode_label = (TextView) view.findViewById(R.id.customerBarcodeLabel);
            holder.customer_distance_label = (TextView) view.findViewById(R.id.customerDistanceLabel);
            holder.customer_time_label = (TextView) view.findViewById(R.id.customerTimeLabel);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
        holder.customer_script.setText(Html.fromHtml(hashmap_Current.get("customer_script")));
        //holder.customer_barcode.setText(Html.fromHtml(hashmap_Current.get("package_barcode")));
        holder.customer_street.setText(Html.fromHtml(hashmap_Current.get("customer_street")));
        holder.customer_total_packages.setText(context.getString(R.string.textView_total_packages)+" "+Html.fromHtml(hashmap_Current.get("total_packages")));
        holder.customer_scanned_packages.setText(context.getString(R.string.textView_total_scanned_packages)+ " " + Html.fromHtml(hashmap_Current.get("scanned_packages")));
        holder.letter.setText(Html.fromHtml(hashmap_Current.get("letter")));
        holder.customer_distance.setText(Html.fromHtml(hashmap_Current.get("distance")));
        holder.customer_time.setText(Html.fromHtml(hashmap_Current.get("time")));

        holder.customer_barcode.setTypeface(font1);
        holder.customer_barcode_label.setTypeface(font1);
        holder.customer_street.setTypeface(font2);
        holder.customer_total_packages.setTypeface(font2);
        holder.customer_scanned_packages.setTypeface(font2);
        holder.customer_distance.setTypeface(font2);
        holder.customer_distance_label.setTypeface(font2);
        holder.customer_time.setTypeface(font2);
        holder.customer_time_label.setTypeface(font2);

        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "unscanned_packages")
            {

                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if(row.equals("0")){
                        // scanned - green - in transport
                        holder.content.setBackgroundResource(R.color.green_transparent80);
                    }else {
                        // unscanned - white - loaded for delivery
                        holder.content.setBackgroundResource(R.color.white1);
                    }

                } catch(NumberFormatException nfe) {

                }

            }
        }

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "customer_script")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);

                    if((row.equals("Pharmacy"))){
                        // white
                        holder.customer_total_packages.setVisibility(View.GONE);
                        holder.customer_scanned_packages.setVisibility(View.GONE);
                    }

                } catch(NumberFormatException nfe) {

                }
            }
        }

        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "package_barcode_priority")
            {
                try {

                    holder.customer_barcode.setText((Html.fromHtml(hashmap_Current.get("package_barcode")))+ " and " + "more.." );
                    break;

                } catch(NumberFormatException nfe) {

                }

            }
            else if (entry.getKey() == "package_barcode")
            {
                try {

                    holder.customer_barcode.setText(Html.fromHtml(hashmap_Current.get("package_barcode")));

                } catch(NumberFormatException nfe) {

                }
            }
        }

        return view;
    }

    final class ViewHolder {

        public TextView id, customer_script, customer_barcode, customer_total_packages, customer_scanned_packages, customer_street, letter,
                customer_distance, customer_time;
        public TextView customer_barcode_label, customer_distance_label, customer_time_label;
        public LinearLayout content;

    }

}

