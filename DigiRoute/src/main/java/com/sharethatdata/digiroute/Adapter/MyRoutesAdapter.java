package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 3/3/2017.
 */

public class MyRoutesAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    public MyRoutesAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {

            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.id = (TextView)view.findViewById(R.id.textViewID);
            holder.customer_script = (TextView)view.findViewById(R.id.customerScript);
            holder.customer_barcode = (TextView)view.findViewById(R.id.customerBarcode);
            holder.customer_total_packages = (TextView)view.findViewById(R.id.customerTotalPackages);
            holder.customer_scanned_packages = (TextView)view.findViewById(R.id.customerScannedPackages);
            holder.customer_address = (TextView)view.findViewById(R.id.customerStreet);
            holder.letter = (TextView)view.findViewById(R.id.textViewLetterAddress);
            holder.customer_distance = (TextView)view.findViewById(R.id.customerDistance);
            holder.customer_time = (TextView)view.findViewById(R.id.customerTime);
            holder.content = (LinearLayout)view.findViewById(R.id.layoutAddresses);

            holder.customer_barcode_label = (TextView) view.findViewById(R.id.customerBarcodeLabel);
            holder.customer_distance_label = (TextView) view.findViewById(R.id.customerDistanceLabel);
            holder.customer_time_label = (TextView) view.findViewById(R.id.customerTimeLabel);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
        holder.customer_script.setText(Html.fromHtml(hashmap_Current.get("customer_script")));
        //holder.customer_address.setText(Html.fromHtml(hashmap_Current.get("customer_address")));
        holder.customer_total_packages.setText(context.getString(R.string.textView_total_packages)+" "+Html.fromHtml(hashmap_Current.get("total_packages")));
        holder.customer_scanned_packages.setText(context.getString(R.string.textView_total_scanned_packages)+ " " + Html.fromHtml(hashmap_Current.get("scanned_packages")));
        holder.letter.setText(Html.fromHtml(hashmap_Current.get("letter")));
        holder.customer_distance.setText(Html.fromHtml(hashmap_Current.get("distance")));
        holder.customer_time.setText(Html.fromHtml(hashmap_Current.get("time")));

        String customer_street = hashmap_Current.get("customer_street");
        String customer_house = hashmap_Current.get("customer_house");
        String customer_zipcode = hashmap_Current.get("customer_zipcode");
        String customer_city = hashmap_Current.get("customer_city");
        String customer_country = hashmap_Current.get("customer_country");

        List<String> slist = new ArrayList<String> ();
        slist.add(customer_street);
        slist.add(customer_house);
        slist.add(customer_zipcode);
        slist.add(customer_city);
        slist.add(customer_country);

        StringBuilder rString = new StringBuilder();
        int count = 0;
        for (String line : slist) {
            ++count;
            if (!line.equals("")) {
                if(count != slist.size()){
                    rString.append(line + ", ");
                }else{
                    rString.append(line);
                }
            }
        }

        holder.customer_address.setText(rString.toString());

        holder.customer_barcode.setTypeface(font1);
        holder.customer_barcode_label.setTypeface(font1);
        holder.customer_address.setTypeface(font2);
        holder.customer_total_packages.setTypeface(font2);
        holder.customer_scanned_packages.setTypeface(font2);
        holder.customer_distance.setTypeface(font2);
        holder.customer_distance_label.setTypeface(font2);
        holder.customer_time.setTypeface(font2);
        holder.customer_time_label.setTypeface(font2);

        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if(entry.getKey() == "status_description_priority")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if((row.equals("In Transport")) || (row.equals("Loaded for delivery"))){
                        // white
                        holder.content.setBackgroundResource(R.color.white1);
                        break;
                    }

                } catch(NumberFormatException nfe) {

                }
            }
            else if (entry.getKey() == "status_description")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if((row.equals("In Transport")) || (row.equals("Loaded for delivery")) || (row.equals("Pharmacy"))){
                        // white
                        holder.content.setBackgroundResource(R.color.white1);
                    }else if(row.equals("Delivered")){
                        // green
                        holder.content.setBackgroundResource(R.color.green_transparent80);
                    }else if(row.equals("Not at home")){
                        // red
                        holder.content.setBackgroundResource(R.color.red_transparent80);
                    }else{
                        // orange - other status
                        holder.content.setBackgroundResource(R.color.orange_transparent80);
                    }

                } catch(NumberFormatException nfe) {

                }

            }

        }

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "customer_script")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);

                //    if((row.equals("Pharmacy"))){
                        // white
                        holder.customer_total_packages.setVisibility(View.GONE);
                        holder.customer_scanned_packages.setVisibility(View.GONE);
                //    }
                } catch(NumberFormatException nfe) {

                }
            }
        }

        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "package_barcode_priority")
            {
                try {

                    holder.customer_barcode.setText((Html.fromHtml(hashmap_Current.get("package_barcode")))+ " and " + "more.." );
                    break;

                } catch(NumberFormatException nfe) {

                }

            }
            else if (entry.getKey() == "package_barcode")
            {
                try {

                    holder.customer_barcode.setText(Html.fromHtml(hashmap_Current.get("package_barcode")));

                } catch(NumberFormatException nfe) {

                }
            }
        }
        return view;
    }

    final class ViewHolder {

        public TextView id, customer_script, customer_barcode, customer_total_packages, customer_scanned_packages, customer_address, letter,
        customer_distance, customer_time;
        public TextView customer_barcode_label, customer_distance_label, customer_time_label;
        public LinearLayout content;

    }

}

