package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;
import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gotic_000 on 4/13/2018.
 */

public class HistoryDetailAdapter extends ArrayAdapter<HashMap<String, String>> {

    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    public HistoryDetailAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.id = (TextView)view.findViewById(R.id.textViewID);
            holder.script = (TextView)view.findViewById(R.id.textViewScript);
            holder.barcode = (TextView)view.findViewById(R.id.barcode);
            holder.date_start = (TextView)view.findViewById(R.id.customerDateStart);
            holder.date_end = (TextView)view.findViewById(R.id.customerDateEnd);
            holder.time_start = (TextView)view.findViewById(R.id.customerTimeStart);
            holder.time_end = (TextView)view.findViewById(R.id.customerTimeEnd);
            holder.customer_address = (TextView)view.findViewById(R.id.customerAddress);
            holder.total_distance = (TextView)view.findViewById(R.id.total_distance);
            holder.total_time = (TextView)view.findViewById(R.id.total_time);
            holder.total_time_stopped = (TextView)view.findViewById(R.id.total_time_stopped);
            holder.letter = (TextView)view.findViewById(R.id.textViewLetterAddress);
            holder.content = (LinearLayout) view.findViewById(R.id.layoutAddresses);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        /*String timeString = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date dt;
        try {
            dt = sdf.parse(hashmap_Current.get("time"));
            timeString = sdf.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        holder.id.setText(hashmap_Current.get("id"));
        holder.script.setText(Html.fromHtml(hashmap_Current.get("script")));
        holder.barcode.setText(Html.fromHtml(hashmap_Current.get("barcode")));
        holder.date_start.setText(Html.fromHtml(hashmap_Current.get("dateStart")));
        holder.date_end.setText(Html.fromHtml(hashmap_Current.get("dateEnd")));
        holder.time_start.setText(Html.fromHtml(hashmap_Current.get("timeStart")));
        holder.time_end.setText(Html.fromHtml(hashmap_Current.get("timeEnd")));
        holder.customer_address.setText(Html.fromHtml(hashmap_Current.get("street")));
        holder.total_distance.setText(Html.fromHtml(hashmap_Current.get("distance")));
        holder.total_time.setText(Html.fromHtml(hashmap_Current.get("time")));
        holder.total_time_stopped.setText(Html.fromHtml(hashmap_Current.get("timeStart/timeEnd")));
        holder.letter.setText(Html.fromHtml(hashmap_Current.get("letter")));

        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if(entry.getKey() == "status_id_priority")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    // 6 = Not at home
                    if((row.equals("6"))){
                        // white
                        holder.content.setBackgroundResource(R.color.red_transparent80);
                        break;
                    }
                } catch(NumberFormatException nfe) {}
            }
            else if (entry.getKey() == "status_id")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    // 1 = Loaded for delivery ; 3 = In Transport
                    if((row.equals("1")) || (row.equals("3"))){
                        // white
                        holder.content.setBackgroundResource(R.color.white1);

                        // 6 = Not at home
                    }else if(row.equals("6")){
                        // red
                        holder.content.setBackgroundResource(R.color.red_transparent80);

                        // 5 = Delivered
                    }else if(row.equals("5")){
                        // green
                        holder.content.setBackgroundResource(R.color.green_transparent80);

                    }else{
                        // orange - other status
                        // 7 = Denied by customer
                        // 8 = Cancelled
                        holder.content.setBackgroundResource(R.color.orange_transparent80);
                    }
                } catch(NumberFormatException nfe) {}
            }
        }

        return view;
    }

    final class ViewHolder {

        public TextView id, script, barcode, date_start, date_end, time_start, time_end, customer_address, total_distance, total_time, total_time_stopped, letter;
        public LinearLayout content;

    }


}
