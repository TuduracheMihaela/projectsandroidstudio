package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


/**
 * Created by mihu__000 on 3/3/2017.
 */

public class PackageHistoryAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;
    Bitmap mSignature;

    public PackageHistoryAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {

            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.packageid = (TextView)view.findViewById(R.id.textViewID);
            holder.deliveryid = (TextView)view.findViewById(R.id.packageDeliveryId);
            holder.packageBarcode = (TextView)view.findViewById(R.id.packageBarcode);
            holder.packageStatus = (TextView)view.findViewById(R.id.packageStatusDescription);
            holder.packageSignatureImage = (ImageView)view.findViewById(R.id.packageSignatureImage);
            holder.packageSignatureText = (TextView) view.findViewById(R.id.packageSignatureText);
            holder.packageSignatureTextView = (TextView) view.findViewById(R.id.packageSignatureTextView);
            holder.packageSignatureName = (TextView) view.findViewById(R.id.packageSignatureNameText);
            holder.packageDeliverRemark = (TextView) view.findViewById(R.id.packageDeliverRemarkText);
            holder.packageNotifyPatient = (TextView) view.findViewById(R.id.packageNotifyPatientText);
            holder.content = (LinearLayout)view.findViewById(R.id.layoutAddresses);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.packageid.setText(hashmap_Current.get("packageid"));
        holder.deliveryid.setText(hashmap_Current.get("deliveryid"));
        holder.packageBarcode.setText(hashmap_Current.get("barcode"));
        holder.packageStatus.setText(hashmap_Current.get("status_description"));
        holder.packageDeliverRemark.setText(hashmap_Current.get("deliver_remark"));
        holder.packageSignatureName.setText(hashmap_Current.get("signature_person"));

        holder.packageDeliverRemark.setTextColor(context.getResources().getColor(R.color.red));


        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "signature")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if((row.equals(context.getString(R.string.textView_text_yes)))){

                        try {
                            String image_url = hashmap_Current.get("image_url");
                            String image = hashmap_Current.get("image");
                            Bitmap result = new DownloadImageTask(holder.packageSignatureImage).execute(image_url + "/" + image).get();
                            mSignature = result;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                        holder.packageSignatureText.setVisibility(View.GONE);
                        holder.packageSignatureTextView.setVisibility(View.GONE);
                        holder.packageSignatureImage.setVisibility(View.VISIBLE);
                        holder.packageSignatureImage.setImageBitmap(mSignature);

                    }else if((row.equals(context.getString(R.string.textView_text_no)))){

                        holder.packageSignatureImage.setVisibility(View.GONE);
                        holder.packageSignatureText.setVisibility(View.VISIBLE);
                        holder.packageSignatureText.setTextColor(context.getResources().getColor(R.color.grey07));
                        holder.packageSignatureText.setText(hashmap_Current.get("signature"));
                    }
                } catch(NumberFormatException nfe) {

                }
            }
        }

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "notify_patient")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);

                    if((row.equals("1"))){

                        holder.packageNotifyPatient.setText(context.getString(R.string.textView_text_yes));

                    }else if((row.equals("0"))){

                        holder.packageNotifyPatient.setText(context.getString(R.string.textView_text_no));
                    }
                } catch(NumberFormatException nfe) {

                }
            }
        }


        return view;
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            Log.d("digiroute_logs","downloading image from url = " + urldisplay);

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                // remember image
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                String myImageData = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if (result != null) {
                //bmImage.setImageBitmap(result);
            }
        }
    }

    final class ViewHolder {

        public TextView packageid, deliveryid, packageBarcode, packageStatus, packageSignatureText, packageSignatureTextView,
        packageDeliverRemark, packageNotifyPatient, packageSignatureName;
        public ImageView packageSignatureImage;
        public LinearLayout content;

    }
}


