package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 5/7/2017.
 */

public class NewOrderAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    public NewOrderAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.packageid = (TextView)view.findViewById(R.id.textViewID);
            holder.addressid = (TextView)view.findViewById(R.id.textViewAdressID);
            holder.delivery_id_invisible = (TextView)view.findViewById(R.id.textViewDeliveryID);
            holder.delivery_id = (TextView)view.findViewById(R.id.packageDeliveryId);
            holder.script_id = (TextView)view.findViewById(R.id.customerScript);
            holder.customer_barcode = (TextView)view.findViewById(R.id.packageBarcode);
            holder.route = (TextView)view.findViewById(R.id.route);
            holder.location = (TextView)view.findViewById(R.id.location);
            holder.customer_address = (TextView)view.findViewById(R.id.packageAddress);
            holder.status = (TextView)view.findViewById(R.id.packageStatusDescription);
            holder.imageView_marker = (ImageView) view.findViewById(R.id.marker_package);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.packageid.setText(Html.fromHtml(hashmap_Current.get("packageid")));
        holder.addressid.setText(Html.fromHtml(hashmap_Current.get("addressid")));
        holder.delivery_id_invisible.setText(Html.fromHtml(hashmap_Current.get("delivery_id")));
        holder.delivery_id.setText(Html.fromHtml(hashmap_Current.get("delivery_id")));
        holder.script_id.setText(Html.fromHtml(hashmap_Current.get("scriptid")));
        holder.customer_barcode.setText(Html.fromHtml(hashmap_Current.get("package_barcode")));
        holder.route.setText(Html.fromHtml(hashmap_Current.get("route")));
        holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));
        //holder.customer_address.setText(Html.fromHtml(hashmap_Current.get("package_address")));
        holder.status.setText(Html.fromHtml(hashmap_Current.get("package_status_description")));

        String customer_street = hashmap_Current.get("customer_street");
        String customer_house = hashmap_Current.get("customer_house");
        String customer_zipcode = hashmap_Current.get("customer_zipcode");
        String customer_city = hashmap_Current.get("customer_city");
        String customer_country = hashmap_Current.get("customer_country");

        List<String> slist = new ArrayList<String> ();
        slist.add(customer_street);
        slist.add(customer_house);
        slist.add(customer_zipcode);
        slist.add(customer_city);
        slist.add(customer_country);

        StringBuilder rString = new StringBuilder();
        int count = 0;
        for (String line : slist) {
            ++count;
            if (!line.equals("")) {
                if(count != slist.size()){
                    rString.append(line + ", ");
                }else{
                    rString.append(line);
                }
            }
        }

        holder.customer_address.setText(rString.toString());

        /*holder.customer_barcode.setTypeface(font1);
        holder.customer_barcode_label.setTypeface(font1);
        holder.customer_street.setTypeface(font2);
        holder.customer_total_packages.setTypeface(font2);
        holder.customer_scanned_packages.setTypeface(font2);
        holder.customer_distance.setTypeface(font2);
        holder.customer_distance_label.setTypeface(font2);
        holder.customer_time.setTypeface(font2);
        holder.customer_time_label.setTypeface(font2);*/

        String myRow = "";
        for (Map.Entry<String, String> entry : hashmap_Current.entrySet())
        {
            if (entry.getKey() == "package_status")
            {

                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    if(row.equals("1")){
                        // unscanned - white - loaded for delivery
                        holder.imageView_marker.setImageResource(R.drawable.checkmark_gray);
                    }else {
                        // scanned - green - in transport
                        holder.imageView_marker.setImageResource(R.drawable.checkmark_green);
                    }

                } catch(NumberFormatException nfe) {

                }
            }
        }

        return view;
    }

    final class ViewHolder {

        public TextView packageid, addressid, delivery_id_invisible, delivery_id, script_id, customer_barcode, route, location, customer_address, status;
        public ImageView imageView_marker;

    }

}

