package com.sharethatdata.digiroute.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.R;
import com.sharethatdata.digiroute.SelectActionActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 7/18/2017.
 */

public class SelectActionAdapter extends ArrayAdapter<HashMap<String, String>>{

    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutResourceId;
    protected ListView mListView;

    Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
    Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");

    public SelectActionAdapter(Activity context, int layoutResourceId, ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutResourceId=layoutResourceId;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutResourceId, null);
            holder=new ViewHolder();
            holder.package_id = (TextView)view.findViewById(R.id.textViewPackageID);
            holder.delivery_id = (TextView)view.findViewById(R.id.textViewDeliveryID);
            holder.script_id = (TextView)view.findViewById(R.id.customerScript);
            holder.address_id = (TextView)view.findViewById(R.id.textViewAddressID);
            holder.route = (TextView)view.findViewById(R.id.textViewRoute);
            holder.location = (TextView)view.findViewById(R.id.textViewLocation);
            holder.customer_barcode = (TextView)view.findViewById(R.id.packageBarcode);
            holder.customer_address = (TextView)view.findViewById(R.id.packageAddress);
            holder.customer_name = (TextView)view.findViewById(R.id.customerName);
            holder.status = (TextView)view.findViewById(R.id.packageStatusDescription);
            holder.status_id = (TextView)view.findViewById(R.id.textViewPackageStatusId);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        holder.package_id.setText(Html.fromHtml(hashmap_Current.get("packageid")));
        holder.delivery_id.setText(Html.fromHtml(hashmap_Current.get("delivery_id")));
        holder.script_id.setText(Html.fromHtml(hashmap_Current.get("scriptid")));
        holder.address_id.setText(Html.fromHtml(hashmap_Current.get("addressid")));
        holder.route.setText(Html.fromHtml(hashmap_Current.get("route")));
        holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));
        holder.customer_barcode.setText(Html.fromHtml(hashmap_Current.get("package_barcode")));
        //holder.customer_address.setText(Html.fromHtml(hashmap_Current.get("package_address")));
        holder.status.setText(Html.fromHtml(hashmap_Current.get("package_status_description")));
        holder.status_id.setText(Html.fromHtml(hashmap_Current.get("status_id")));
        holder.customer_name.setText(Html.fromHtml(hashmap_Current.get("customer_name")));

        String customer_street = hashmap_Current.get("customer_street");
        String customer_house = hashmap_Current.get("customer_house");
        String customer_zipcode = hashmap_Current.get("customer_zipcode");
        String customer_city = hashmap_Current.get("customer_city");
        String customer_country = hashmap_Current.get("customer_country");

        List<String> slist = new ArrayList<String> ();
        slist.add(customer_street);
        slist.add(customer_house);
        slist.add(customer_zipcode);
        slist.add(customer_city);
        slist.add(customer_country);

        StringBuilder rString = new StringBuilder();
        int count = 0;
        for (String line : slist) {
            ++count;
            if (!line.equals("")) {
                if(count != slist.size()){
                    rString.append(line + ", ");
                }else{
                    rString.append(line);
                }
            }
        }

        holder.customer_address.setText(rString.toString());



        /*holder.customer_barcode.setTypeface(font1);
        holder.customer_barcode_label.setTypeface(font1);
        holder.customer_street.setTypeface(font2);
        holder.customer_total_packages.setTypeface(font2);
        holder.customer_scanned_packages.setTypeface(font2);
        holder.customer_distance.setTypeface(font2);
        holder.customer_distance_label.setTypeface(font2);
        holder.customer_time.setTypeface(font2);
        holder.customer_time_label.setTypeface(font2);*/

        return view;
    }

    final class ViewHolder {

        public TextView package_id, delivery_id, script_id, address_id, route, location, customer_barcode, customer_address, customer_name, status, status_id;

    }

}


