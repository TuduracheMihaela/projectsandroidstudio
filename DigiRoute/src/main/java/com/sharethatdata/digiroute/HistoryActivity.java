package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sharethatdata.digiroute.Adapter.HistoryAdapter;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cRoute;
import com.sharethatdata.logistics_webservice.datamodel.cRouteReport;
import com.sharethatdata.logistics_webservice.datamodel.cRoutes;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 3/28/2017.
 */

public class HistoryActivity extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private int mStartHour,mStartMinute;
    private int mStartYear,mStartMonth,mStartDay;

    ArrayList<HashMap<String, String>> itemList;
    ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userNameArray =  new ArrayList<String>();

    ListView lv = null;
    ImageView prevButton = null;
    ImageView nextButton = null;

    private boolean no_network = false;
    private String error_message = "";

    private String list_name = "";
    private String myUser = "";
    private String myPass = "";
    private String myUserName = "";
    private String myUserID = "";
    private String mySelectedUser = "";

    // vars for remember report
    private String report_user = "";
    private String report_user_id = "";

    TextView startDateView;

    String idPharmacy, distanceUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        lv = (ListView) findViewById(R.id.listView);

        prevButton = (ImageView) findViewById(R.id.btnPrev);
        nextButton = (ImageView) findViewById(R.id.btnNext);

        Globals = ((MyGlobals)getApplication());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myUserID = Globals.getValue("idContact"); // id of user logged
        myUserName = Globals.getValue("nameContact"); // id of user logged

        mySelectedUser = myUserID;

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        final String DistanceUnit = sharedPrefs.getString("prefDistanceUnit", "");
        idPharmacy = PharmacyId;
        distanceUnit = DistanceUnit;

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(HistoryActivity.this, HistoryActivity.this);

        Typeface font1 = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        Typeface font2 = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Quicksand-Regular.otf");

      //  RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);


        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
        String weekdayname;
        Calendar c = Calendar.getInstance();
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            mStartYear = Integer.valueOf(extras.getString("year"));
            mStartMonth = Integer.valueOf(extras.getString("month"));
            mStartDay = Integer.valueOf(extras.getString("day"));
            c.set(mStartYear, mStartMonth, mStartDay);
            weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
        }else{
            // set calendar
            mStartYear = c.get(Calendar.YEAR);
            mStartMonth = c.get(Calendar.MONTH);
            mStartDay = c.get(Calendar.DAY_OF_MONTH);
            weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];
        }

        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);

        startDateView = (TextView) findViewById(R.id.dateStartText);
        startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
        TextView startDayView = (TextView) findViewById(R.id.dateDayText);
        startDayView.setText(weekdayname);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String route_id = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
                String route_name = ((TextView) itemClicked.findViewById(R.id.textViewFunc)).getText().toString();

                // start detail activity
                Intent intent = new Intent(HistoryActivity.this, HistoryDetailActivity.class);
                intent.putExtra("idRoute", route_id);
                intent.putExtra("nameRoute", route_name);

                startActivity(intent);

            }

        });



        prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                startDateView = (TextView) findViewById(R.id.dateStartText);
                TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                String weekdayname = "";

                try {
                    Date myDate = dateFormat.parse(startDateView.getText().toString());

                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, -1);
                    mStartYear = c.get(Calendar.YEAR);
                    mStartMonth = c.get(Calendar.MONTH);
                    mStartDay = c.get(Calendar.DAY_OF_MONTH);
                    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
                    weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];


                } catch (ParseException e) {
                    System.out.println(e.toString());
                }

                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);

                LoadReports();

            }

        });

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                startDateView = (TextView) findViewById(R.id.dateStartText);
                TextView startDayView = (TextView) findViewById(R.id.dateDayText);
                String weekdayname = "";

                try {
                    Date myDate = dateFormat.parse(startDateView.getText().toString());

                    Calendar c = Calendar.getInstance();
                    c.setTime(myDate);
                    c.add(Calendar.DATE, 1);
                    mStartYear = c.get(Calendar.YEAR);
                    mStartMonth = c.get(Calendar.MONTH);
                    mStartDay = c.get(Calendar.DAY_OF_MONTH);
                    String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names
                    weekdayname = weekdays[c.get(Calendar.DAY_OF_WEEK)];


                } catch (ParseException e) {
                    System.out.println(e.toString());
                }

                startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartYear)));
                startDayView.setText(weekdayname);

                LoadReports();
            }

        });

        // check manager
        String isManager = Globals.getValue("manager");
        if (isManager == "yes")
        {
            Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
            sUsers.setVisibility(View.VISIBLE);
            RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
            userSpinnerLayout.setVisibility(View.VISIBLE);

            new LoadUserListSpinner().execute();

        } else {
            Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
            sUsers.setVisibility(View.GONE);
            RelativeLayout userSpinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
            userSpinnerLayout.setVisibility(View.GONE);

            LoadReports();
        }

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onResume() {
        super.onResume();

        //LoadReports();

    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }

    private void LoadReports()
    {

        String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
        String myDateTimeEnd = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();

        Globals.setValue("report_date_start", myDateTime);
        Globals.setValue("report_date_end", myDateTimeEnd);

        LoadList RefreshList;
        RefreshList = new LoadList();
        RefreshList.execute();

    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mStartYear = year;
            mStartMonth = month;
            mStartDay = day;

            TextView startDateView = (TextView) findViewById(R.id.dateStartText);
            TextView startDayView = (TextView) findViewById(R.id.dateDayText);

            String dayOfWeekStart = DateFormat.format("EEEE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();

            startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("-")
                    .append(pad(mStartMonth + 1)).append("-")
                    .append(pad(mStartYear)));

            startDayView.setText(dayOfWeekStart);


            LoadReports();


        }
    };

    public void startDateClick(View v)
    {
        DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mStartYear, mStartMonth, mStartDay);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dp1.setTitle(sdf.format(d));
        dp1.show();


    }

    // set time from mysql datetime
    private String SetTime(String mysqldate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        StringBuilder setText = null;
        try {
            Date myDate = dateFormat.parse(mysqldate);

            Calendar c = Calendar.getInstance();
            c.setTime(myDate);
            mStartHour = c.get(Calendar.HOUR_OF_DAY);
            mStartMinute = c.get(Calendar.MINUTE);

            setText = new StringBuilder().append(pad(mStartHour)).append(":").append(pad(mStartMinute));

        } catch (ParseException e) {

        }

        return setText.toString();

    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadUserListSpinner extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            Globals = ((MyGlobals)getApplication());

            // load data from provider
            //addressList = new ArrayList<HashMap<String, String>>();
            userList = new ArrayList<HashMap<String, String>>();

            //List<cContact> List = MyProvider.getPharmacyUsers(idPharmacy);
            List<cContact> List = MyProvider.getMyUsers(idPharmacy);
            for(cContact entry : List)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("username", entry.username);
                map.put("name", entry.name);

                // adding HashList to ArrayList
                userList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (HistoryActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    String isManager = Globals.getValue("manager");
                    if (isManager == "yes")
                    {
                        List<String> userArray =  new ArrayList<String>();
                        userArray.add(0, getString(R.string.select_user));
                        userkeyArray.add(0,"");
                        userNameArray.add(0,"");

                        if(userList.size()>0){
                            //userArray.add(0, getString(R.string.select_user));
                            //userkeyArray.add(0, getString(R.string.select_user));
                        }else{
                            userArray.add(0, getString(R.string.select_no_user));
                            userkeyArray.add(0, getString(R.string.select_no_user));
                        }

                        for (HashMap<String, String> map : userList)
                            for (Map.Entry<String, String> entry : map.entrySet())
                            {
                                if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                                if (entry.getKey() == "username") userNameArray.add(entry.getValue());
                                if (entry.getKey() == "name") userArray.add(entry.getValue());
                            }

                        //ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyRoutesActivity.this, android.R.layout.simple_spinner_item, userArray);
                        //user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.VISIBLE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.VISIBLE);
                        //sUsers.setAdapter(user_adapter);

                        SpinnerAdapter user_adapter = new SpinnerAdapter(
                                HistoryActivity.this,
                                R.layout.spinner_adapter,
                                userArray
                        );
                        sUsers.setAdapter(user_adapter);

                        sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                                String mIndex = sUsers.getSelectedItem().toString();

                                if(mIndex.contains(getString(R.string.select_user))){
                                    // do nothing

                                }else{

                                    int myIndex = sUsers.getSelectedItemPosition();
                                    String userid = userkeyArray.get(myIndex);
                                    mySelectedUser = userid;

                                    LoadReports();

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // auto select
                        int selectUserIndex = userNameArray.indexOf(myUser.toLowerCase());
                        sUsers.setSelection(selectUserIndex);
                        mySelectedUser = String.valueOf(selectUserIndex);

                    }else{
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.GONE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.GONE);

                        mySelectedUser = "";
                    }

                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load all time records by making HTTP Request
     * */
    class LoadList extends AsyncTask<String, String, String> {

        // total_time
        int total_time = 0;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {
            Globals = ((MyGlobals)getApplication());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String report_date_start = Globals.getValue("report_date_start");
            String report_date_end = Globals.getValue("report_date_end");

            // total distance
            int myNum_distance = 0;
            int total_distance = 0;

            // total_time
            int myNum_time = 0;
            total_time = 0;

            // load data from provider
            itemList = new ArrayList<HashMap<String, String>>();

            //List<cRoutes> routes = MyProvider.getRoutes("", idPharmacy, report_date_start, report_date_end);
            List<cMyRoute> routes = MyProvider.getMyRoutes(mySelectedUser, idPharmacy, report_date_start, report_date_end, "6");

            for (cMyRoute entry : routes) {

                // show only routes with status 5 - Delivered
                if(entry.status.equals("5")){

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    String[] tokens = entry.datetime.split("\\s");
                    String values = tokens[0] + tokens[1];
                    String date = tokens[0];
                    String time = tokens[1];

                    System.out.println("Values: " + values);

                    // map.put("date", date);
                    map.put("id", entry.id);
                    map.put("date", date);
                    map.put("time", time);
                    map.put("name", entry.id);
                    map.put("subtitle", entry.description);

                    List<String> strArray = new ArrayList<String>();
                    if (routes.size() > 0) {
                        List<cRouteReport> routeReport = MyProvider.getRouteReports(entry.id, idPharmacy);
                        for (cRouteReport entry1 : routeReport) {

                            // total distance trip
                            try {
                                myNum_distance = Integer.parseInt(entry1.distance.toString());
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                            total_distance = total_distance + myNum_distance;

                            // total time trip
                            try {
                                myNum_time = Integer.parseInt(entry1.time.toString());
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                            total_time = total_time + myNum_time;

                            // total time stop
                            String time1 = "00:00:00";
                            try {
                                String[] tokens1 = entry1.datetime_start.split("\\s");
                                String values1 = tokens1[0] + tokens1[1];
                                String date1 = tokens1[0];
                                time1 = tokens1[1];
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }

                            String time2 = "00:00:00";
                            try {
                                String[] tokens2 = entry1.datetime_end.split("\\s");
                                String values2 = tokens2[0] + tokens2[1];
                                String date2 = tokens2[0];
                                time2 = tokens2[1];
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }

                            strArray.add(time1 + "/" + time2);
                            //map.put("timeStart/timeEnd", time1+"/"+time2);
                        }

                        // total distance return
                        final cRoute pharmacy = MyProvider.getRouteReport(entry.id, idPharmacy);
                        try {
                            myNum_distance = Integer.parseInt(pharmacy.return_distance.toString());
                        } catch (NumberFormatException ex) {
                            myNum_distance = 0;
                        }
                        total_distance = total_distance + myNum_distance;

                        // total time return
                        try {
                            myNum_time = Integer.parseInt(pharmacy.return_time.toString());
                        } catch (NumberFormatException ex) {
                            myNum_time = 0;
                        }
                        total_time = total_time + myNum_time;

                        String[] tokens1 = new String[0];
                        String values1 = "";
                        String date1 = "";
                        String time1 = "";

                        if (pharmacy.datetime_start != "") {
                            tokens1 = pharmacy.datetime_start.split("\\s");
                            values1 = tokens1[0] + tokens1[1];
                            date1 = tokens1[0];
                            time1 = tokens1[1];
                        } else {
                            time1 = "00:00:00";
                        }


                        strArray.add(time1 + "/" + "00:00:00"); // add return time

                        // calculate total time stop
                        long total_time_stopped = 0;
                        int total_time_stopped_seconds = 0;
                        long myNum_time_stopped = 0;
                        boolean last_item = false;

                        for (int i = 0; i < strArray.size(); i++) {
                            for (int j = i + 1; j < i + 2; j++) {
                                String row1 = String.valueOf(strArray.get(i).toString());
                                String[] parts1 = row1.split("/");
                                String timeStart1 = parts1[0];
                                String timeEnd1 = parts1[1];

                                if (strArray.get(strArray.size() - 1).contains(row1)) {
                                    last_item = true;
                                    break;
                                }

                                String row2 = String.valueOf(strArray.get(j).toString());
                                String[] parts2 = row2.split("/");
                                String timeStart2 = parts2[0];
                                String timeEnd2 = parts2[1];

                                // compare value1 and value2;
                                myNum_time_stopped = MyTimeUtils.getDifferenceStoppedTime(timeStart2, timeEnd1);
                            }
                            if (!last_item) {
                                total_time_stopped = total_time_stopped + myNum_time_stopped; // miliseconds
                                total_time_stopped_seconds = MyTimeUtils.getFormattedMilisecondsToSeconds(total_time_stopped);
                            }
                        }

                        total_time = total_time + total_time_stopped_seconds;

                        if(distanceUnit.equals(getResources().getString(R.string.settings_kilometers))) {
                            // Kilometers
                            map.put("total_distance", MyTimeUtils.getFormattedKilometers(total_distance));
                        }else if(distanceUnit.equals(getResources().getString(R.string.settings_miles))) {
                            // Miles
                            map.put("total_distance", MyTimeUtils.getFormattedMiles(total_distance));
                        }

                        map.put("total_time", MyTimeUtils.getFormattedHourMinutes(total_time));



                    }

                    // total distance
                    myNum_distance = 0;
                    total_distance = 0;

                    // total_time
                    myNum_time = 0;
                    total_time = 0;

                    // adding HashList to ArrayList
                    itemList.add(map);

                }
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                   /* adapter = new SimpleAdapter(
                            HistoryActivity.this
                            , itemList
                            , R.layout.activity_history_listview_item, new String[] { "date", "time", "id", "name", "subtitle", "hours" },
                            new int[] { R.id.textViewDate, R.id.textViewTime, R.id.textViewID, R.id.textViewName, R.id.textViewFunc, R.id.textViewHours });
                    */

                    ListAdapter adapter = null;

                    adapter = new HistoryAdapter(HistoryActivity.this, R.layout.activity_history_listview_item, itemList);
                    lv.setAdapter(adapter);

                }
            });

            hideProgressDialog();

        }


    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }


}
