package com.sharethatdata.digiroute;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.sharethatdata.digiroute.notification.MyReceiver;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cWebService;
import com.sharethatdata.logistics_webservice.help.HelpActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;
    WSDataProvider MyProvider2 = null;

	TimeUtils MyTimeUtils = null;

    JSONArray order = null;

    ArrayList<String> roles = null;

//	public String override_url = "";

	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.11
	 */
//	private static final String[] DUMMY_CREDENTIALS = new String[] {
//			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
//	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserRegisterDeviceTask mRegDevTask = null;
	private UserLoginTask mAuthTask = null;
	private UserRolesTask mRolesTask = null;

	// Values for email and password at the time of the login attempt.
	private String mApikey;
	private String mEmail;
	private String mPassword;

	private String myUser = "";
	private String myPass = "";

	private String webservice_url = "";

	private String PharmacyId;

	private String mVersion;

	// UI references.
	private EditText mApikeyView;
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private TextView mVersionView;
	private Button btnSignIn;

	private PendingIntent pendingIntent;

	SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_new);

		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		// avoid automatically appear android keyboard when activity start
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(LoginActivity.this, LoginActivity.this);

		// Set up the login form.
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );

		String current_user = sharedPrefs.getString("prefUsername", "");
		String current_password = sharedPrefs.getString("prefUserPassword", "");
		boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
		PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

		// get webservice url
		webservice_url = sharedPrefs.getString("prefWebServiceServer", "");

		myUser = current_user;
		myPass = current_password;

		boolean suc = InitApp();

		if (!suc) Toast.makeText(LoginActivity.this, getString(R.string.toast_text_failed_initializing), Toast.LENGTH_SHORT).show();

		TextView tv1=(TextView)findViewById(R.id.textDigiroute1);
		TextView tv2=(TextView)findViewById(R.id.textDigiroute2);
		TextView tv3=(TextView)findViewById(R.id.sign_in_button);
		Typeface face=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Regular.ttf");
		Typeface face1=Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.otf");
		Typeface face2=Typeface.createFromAsset(getAssets(),"fonts/OpenSans-Regular.ttf");
		tv1.setTypeface(face1);
		tv2.setTypeface(face1);
		tv3.setTypeface(face2);

		mEmailView = (EditText) findViewById(R.id.email);
		mPasswordView = (EditText) findViewById(R.id.password);
		mApikeyView = (EditText) findViewById(R.id.apikey);
		btnSignIn = (Button) findViewById(R.id.sign_in_button);

		mEmailView.setTypeface(face1);
		mPasswordView.setTypeface(face1);
		mApikeyView.setTypeface(face1);

		//if (!prefs.getString("apikey","").equals("")) {
		if (!prefs.getBoolean("apikey_saved", false)){
			// <---- run one time code here

			btnSignIn.setText(getString(R.string.action_sign_in_register_device));

			mEmailView.setVisibility(View.GONE);
			mPasswordView.setVisibility(View.GONE);

		}else{
			// Set up the login form.
			mEmail = current_user;
			mEmailView.setText(mEmail);

			mPassword = current_password;
			mPasswordView.setText(mPassword);

			btnSignIn.setText(getString(R.string.action_sign_in_register));

			mApikeyView.setVisibility(View.GONE);
		}

		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		mVersionView = (TextView) findViewById(R.id.textVersion);
		mVersion = BuildConfig.VERSION_NAME;
		mVersionView.setText("version " + mVersion);

		btnSignIn.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						if(isNetworkAvailable() == true){
							//if (!prefs.getString("apikey","").equals("")) {
							if (!prefs.getBoolean("apikey_saved", false)){
								// <---- run one time code here
								attemptRegisterDevice();
								// register device
							}else{
								if(prefs.getString("ws_url", "").equals("")){
									// the list of web services NOT taken yet -> the device is NOT yet registered by the administrator
									new LoadListURL().execute();
								}else {
									// the default url was saved -> if the device was activated by the administrator
									attemptLogin();
								}
							}

                        }else {
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_no_internet), Toast.LENGTH_SHORT).show();
                        }
					}
				});

		if (blAutoLogin)
		{
            if(isNetworkAvailable() == true){
                attemptLogin();
            }else {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_text_no_internet), Toast.LENGTH_SHORT).show();
            }
		}



		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifiManager.getConnectionInfo();
		String address = info.getMacAddress();

		    System.out.println("Adresse MAC enabled: " + address);

	    WifiManager wifiManagerSSID = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
	    WifiInfo infoSSID = wifiManagerSSID.getConnectionInfo ();
	    String ssid = infoSSID.getBSSID ();

	    	System.out.println("Adresse SSID enabled: " + ssid);


	}

	private boolean InitApp()
	{
		Globals = ((MyGlobals)getApplication());

		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider2 = new WSDataProvider(myUser, myPass);

		if (webservice_url != "")
		{
			// store in globals
			Globals.setValue("ws_url", webservice_url);

			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider2.SetWebServiceUrl(webservice_url);
		}

		try
		{
			// read device values
			WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInf = wifiMan.getConnectionInfo();
			int ipAddress = wifiInf.getIpAddress();
			String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
			Globals.setValue("ip", ip);

			String android_id = Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID);
			String manufacturer = Build.MANUFACTURER;
			String model = Build.MODEL;
			String device_name = manufacturer + " " + model;

			Globals.setValue("device", android_id);
			Globals.setValue("device_name", device_name);

			String appname = (String) getApplicationInfo().loadLabel(getBaseContext().getPackageManager());
			Globals.setValue("module", appname);

			MyProvider.SetDeviceIdentification(ip, android_id, appname);
			MyProvider2.SetDeviceIdentification(ip, android_id, appname);

			// save device ip, id and name for service
			SharedPreferences.Editor editor = getSharedPreferences("PREF_DEVICE", 0).edit();
			editor.putString("DEVICE_IP", String.valueOf(ip));
			editor.putString("DEVICE_ID", String.valueOf(android_id));
			editor.putString("DEVICE_NAME", String.valueOf(appname));
			editor.commit();

			// add here also mac address

			return  true;
		} catch (Exception e) {
            e.printStackTrace();
            return false;
        }

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.action_settings:
				Intent intent_settings = new Intent(this, SettingsLoginActivity.class); startActivity(intent_settings);
				return true;
			case R.id.action_help:
				Intent intent_help = new Intent(this, HelpActivity.class); startActivity(intent_help);
				return true;
			case R.id.action_reset_password:
				Intent intent_reset = new Intent(this, ResetPasswordActivity.class); startActivity(intent_reset);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */


	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	public void attemptRegisterDevice() {
		if (mRegDevTask != null) {
			return;
		}

		// Reset errors.
		mApikeyView.setError(null);

		// Store values at the time of the login attempt.
		mApikey = mApikeyView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid apikey.
		if (TextUtils.isEmpty(mApikey)) {
			mApikeyView.setError(getString(R.string.error_field_required));
			focusView = mApikeyView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_registration);
			showProgress(true);
			mRegDevTask = new UserRegisterDeviceTask(mApikey);
			mRegDevTask.execute((Void) null);
		}
	}

	protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			String apikey = prefs.getString("apikey", "");
			webservice_url = prefs.getString("ws_url", "");
			String access_key = prefs.getString("access_key", "");

			String myIp = Globals.getValue("ip");
			String myDeviceID = Globals.getValue("device");
			String myDeviceName = Globals.getValue("device_name");
			//String myAccessKey = Globals.getValue("access_key");
			//webservice_url = Globals.getValue("ws_url");

			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl(webservice_url);
			MyProvider.SetDeviceIdentification(myIp, myDeviceID, myDeviceName);
			boolean suc = MyProvider.CheckSecurekLogin(mEmail, mPassword, "DigiRoute", mVersion ,access_key);
			 // boolean suc = MyProvider.CheckLogin(mEmail, mPassword, "DigiRoute");
			if (suc)
			{
				// store current user in globals
				MyGlobals Globals = ((MyGlobals)getApplication());
			    Globals.setValue("user", mEmail);
			    Globals.setValue("pass", mPassword);
			    Globals.setValue("ws_url", webservice_url);
			    Globals.setValue("access_key", access_key);

			    MyProvider.LogEvent(getString(R.string.text6_log_event));
			    // "user logged in."

				// save user, pass and url for service
				SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
				editor.putString("USER", String.valueOf(mEmail));
				editor.putString("PASS", String.valueOf(mPassword));
				editor.putString("URL", String.valueOf(webservice_url));
				editor.commit();

			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {

				/*
				 * Name for profile
				 */
				SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
				SharedPreferences.Editor editor = pref.edit();
				editor.putString("Name",mEmail);
				editor.putString("Pass",mPassword);
				editor.apply();


				if(MyProvider.update.equals("1")){

					//String link1 = "https://www.sharethatdata.com/downloads/DigiRoute-dev.apk";
					//Spanned myMessage = Html.fromHtml(link1);

					AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
					LayoutInflater inflater = getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.prompt_update_apk, null);
					dialogBuilder.setView(dialogView);

					dialogBuilder.setTitle(getString(R.string.alert_dialog_title_update_apk));
					//dialogBuilder.setMessage(getString(R.string.alert_dialog_text_update_apk) + " \n\n" + myMessage);
					dialogBuilder.setCancelable(true);

					dialogBuilder.setPositiveButton(
							"OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();

									// register roles
									mRolesTask = new UserRolesTask();
									mRolesTask.execute((Void) null);
								}
							});

					AlertDialog alert11 = dialogBuilder.create();
					alert11.show();

					TextView link_update_apk = (TextView) dialogView.findViewById(R.id.link_update_apk);
					final SpannableString string_link = new SpannableString(getText(R.string.link_update_apk));
					Linkify.addLinks(string_link, Linkify.ALL);
					link_update_apk.setText(string_link);
					link_update_apk.setMovementMethod(LinkMovementMethod.getInstance());


					//TextView alertText = (TextView) alert11.findViewById(android.R.id.message);
					//alertText.setMovementMethod(LinkMovementMethod.getInstance());

				}else{
					// register roles
					mRolesTask = new UserRolesTask();
					mRolesTask.execute((Void) null);
				}


				// call service to check new message
				//MyProvider = new WSDataProvider(LoginActivity.this);
				//MyProvider.startServicefromNonActivity();

			} else {
			//	mPasswordView
			//			.setError(getString(R.string.error_incorrect_password));
				mPasswordView.setError(MyProvider.last_error);
				mPasswordView.requestFocus();

				/*if (MyProvider.last_error_nr.equals("10007"))
				{
					// show register device
					Intent intent = new Intent(LoginActivity.this, RegisterDeviceActivity.class);
					startActivity(intent);

				}*/

				if (MyProvider.last_error_nr.equals("10003"))
				{
					// show register device
					Toast.makeText(LoginActivity.this, getString(R.string.toast_please_activate), Toast.LENGTH_SHORT).show();

				}
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous task used to check the roles of
	 * the user.
	 */
	public class UserRolesTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

		    MyProvider2 = new WSDataProvider(mEmail, mPassword);
			String url_ws = Globals.getValue("ws_url");
			MyProvider2.SetWebServiceUrl(url_ws);
			roles = MyProvider2.getUserRoles(mEmail, PharmacyId);

			if (roles != null)
			{
				MyGlobals Globals = ((MyGlobals)getApplication());

				if (roles.contains("Administrator"))
				{
				    Globals.setValue("administrator", "yes");
				} else {
				    Globals.setValue("administrator", "no");
				}
				if (roles.contains("Developer"))
				{
				    Globals.setValue("developer", "yes");
				} else {
				    Globals.setValue("developer", "no");
				}
				if (roles.contains("Manager"))
				{
				    Globals.setValue("manager", "yes");
				} else {
				    Globals.setValue("manager", "no");
				}
				if (roles.contains("Employee"))
				{
				    Globals.setValue("employee", "yes");
				} else {
				    Globals.setValue("employee", "no");
				}
			}

			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			runOnUiThread(new Runnable() {
			      @Override
			          public void run() {


			    	  	if (roles.size() == 0)
						{
							String data_error = MyProvider2.last_error;
							if(!data_error.equals("")){
								Toast.makeText(getApplication(), data_error, Toast.LENGTH_SHORT).show();
							}
						}

			    	  	showProgress(false);

					  finish();

					  Intent intent = new Intent(LoginActivity.this, SelectActionActivity.class);
					  startActivity(intent);
				  }
			});


			if (success) {


			} else {

			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous registration task used to register device
	 * of the user.
	 */
	public class UserRegisterDeviceTask extends AsyncTask<Void, Void, Boolean> {

		String apikey;

		public UserRegisterDeviceTask(String apikey){
			this.apikey = apikey;
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

			String myDeviceID = Globals.getValue("device");
			String myDeviceName = Globals.getValue("device_name");

			boolean suc = MyProvider.registerDeviceCerberus(apikey, myDeviceID, myDeviceName);
			if (suc)
			{
				MyProvider.LogEvent(getString(R.string.text6_log_event));
				// "user logged in."
				SharedPreferences.Editor editor1 = prefs.edit();
				editor1.putBoolean("apikey_saved", true);
				editor1.commit();

				// save apikey
				// mark first time has runned.
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("apikey", apikey);
				editor.commit();

				// save user, pass and url for service
				/*SharedPreferences.Editor editor = getSharedPreferences("PREF_USER", 0).edit();
				editor.putString("USER", String.valueOf(mEmail));
				editor.putString("PASS", String.valueOf(mPassword));
				editor.putString("URL", String.valueOf(webservice_url));
				editor.commit();*/


			}
			return suc;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mRegDevTask = null;
			showProgress(false);

			if (success) {

				/*
				 * Name for profile
				 */
				/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
				SharedPreferences.Editor editor = pref.edit();
				editor.putString("Name",mEmail);
				editor.putString("Pass",mPassword);
				editor.apply();*/


				// Set up the login form.
				mEmailView.setVisibility(View.VISIBLE);
				mPasswordView.setVisibility(View.VISIBLE);
				mApikeyView.setVisibility(View.GONE);

				btnSignIn.setText(getString(R.string.action_sign_in_register));

				mApikeyView.setVisibility(View.GONE);

				Toast.makeText(LoginActivity.this, getString(R.string.toast_please_activate), Toast.LENGTH_LONG).show();

				// call service to check new message
				//MyProvider = new WSDataProvider(LoginActivity.this);
				//MyProvider.startServicefromNonActivity();

			} else {
				//	mPasswordView
				//			.setError(getString(R.string.error_incorrect_password));


                // 20003 = Device is already registered.
                if(MyProvider.last_error_nr.equals("20003")){
                    mEmailView.setVisibility(View.VISIBLE);
                    mPasswordView.setVisibility(View.VISIBLE);
                    mApikeyView.setVisibility(View.GONE);

                    btnSignIn.setText(getString(R.string.action_sign_in_register));

                    mApikeyView.setVisibility(View.GONE);

                    Toast.makeText(LoginActivity.this, MyProvider.last_error, Toast.LENGTH_SHORT).show();

					// save api key
					MyProvider.LogEvent(getString(R.string.text6_log_event));
					// "user logged in."
					SharedPreferences.Editor editor1 = prefs.edit();
					editor1.putBoolean("apikey_saved", true);
					editor1.commit();

					// save apikey
					// mark first time has runned.
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("apikey", apikey);
					editor.commit();
                }else {
                    mApikeyView.setError(MyProvider.last_error);
                    mApikeyView.requestFocus();
                }

				/*if (MyProvider.last_error_nr.equals("10007"))
				{
					// show register device
					Intent intent = new Intent(LoginActivity.this, RegisterDeviceActivity.class);
					startActivity(intent);
				}*/
			}
		}

		@Override
		protected void onCancelled() {
			mRegDevTask = null;
			showProgress(false);
		}
	}


	/**
	 * Background Async Task to Load my sent request by making HTTP Request
	 * */
	class LoadListURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			MyProvider = new WSDataProvider(getApplication());
			MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

			String myDeviceID = Globals.getValue("device");
			String myDeviceName = Globals.getValue("device_name");

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
			String apikey = prefs.getString("apikey","");

			List<cWebService> myService = MyProvider.getAllWebServices(apikey, myDeviceID, "3");
			for(cWebService entry : myService)
			{
				if(entry.default_url.equals("1")){
					Globals.setValue("ws_url_id", entry.id);
					Globals.setValue("ws_url", entry.url);
					Globals.setValue("access_key", entry.access_key);

					// get list of web services one time and get,set the default url -> if the device was activated by the administrator
					SharedPreferences.Editor editor = prefs.edit();
					Globals.setValue("ws_url_id", entry.id);
					editor.putString("ws_url", entry.url);
					editor.putString("access_key", entry.access_key);
					editor.commit();

					break;
				}else{
					// it will crash if I receive list urls with all default = 0
					// which web service should I set in this situation????

					Globals.setValue("ws_url_id", entry.id);
					Globals.setValue("ws_url", entry.url);
					Globals.setValue("access_key", entry.access_key);

					// get list of web services one time and set the default url -> if the device was activated by the administrator
					SharedPreferences.Editor editor = prefs.edit();
					Globals.setValue("ws_url_id", entry.id);
					editor.putString("ws_url", entry.url);
					editor.putString("access_key", entry.access_key);
					editor.commit();

					break;
				}
					/*HashMap<String, String> map = new HashMap<String, String>();

					map.put("id",  entry.id);
					map.put("name",  entry.name);
					map.put("type",  entry.type);
					map.put("url",  entry.url);
					map.put("access_key",  entry.access_key);*/
			}

			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					//pDialog.dismiss();

					if(MyProvider.last_error_nr.equals("10003")){
						Toast.makeText(LoginActivity.this, getString(R.string.toast_please_activate), Toast.LENGTH_LONG).show();
					}else{
						attemptLogin();
					}

				}
			});
			//hideProgressDialog();
		}
	}




}

