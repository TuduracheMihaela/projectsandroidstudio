package com.sharethatdata.digiroute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sharethatdata.digiroute.DragDropListview.draglistview.DragItem;
import com.sharethatdata.digiroute.DragDropListview.draglistview.DragListView;
import com.sharethatdata.digiroute.DragDropListview.draglistview.ItemAdapter;
import com.sharethatdata.digiroute.DragDropListview.draglistview.swipe.ListSwipeHelper;
import com.sharethatdata.digiroute.DragDropListview.draglistview.swipe.ListSwipeItem;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewListViewPhone;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewListViewTablet;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseView;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.ViewTarget;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPharmacyAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRoute;
import com.sharethatdata.logistics_webservice.datamodel.cRouteAddress;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

public class MyRoutesActivity extends Activity{
	
	MyGlobals Globals;	
	WSDataProvider MyProvider = null;

	TimeUtils MyTimeUtils = null;

	//MyRoutesAdapter adapter;

	ArrayList<HashMap<String, String>> packageRouteList; // List Packages
	ArrayList<HashMap<String, String>> addressList; // List Addresses
	ArrayList<HashMap<String, String>> userList; // getContacts
	List<String> userkeyArray =  new ArrayList<String>();
	ArrayList<HashMap<String, String>> routeList; // getRoutes
	List<String> routekeyArray =  new ArrayList<String>();
	ArrayList<HashMap<String, String>> orderList; // getOrders
	List<String> orderkeyArray =  new ArrayList<String>();
	List<String> userNameArray =  new ArrayList<String>();
	
	private String myUser = "";
	private String myPass = "";
	private String mySelectedUser = "";
	ProgressDialog progressDialog;
	
	//ListView lv = null;
	DragListView mDragListView = null;
	TextView tvRouteStatus;
	TextView distance;
	TextView time;

	String idRoute;
	String idPharmacy, distanceUnit;
	String pharmacyAddress, pharmacyStreet, pharmacyHouse, pharmacyZipcode, pharmacyCity, pharmacyCountry;

	// UNDO
	ListSwipeItem itemm;
	int poss;

	//Tutorial
	ShowcaseView showcaseView;
	boolean tutorial_scanned_package = false;
	Animation animSlide;
	ImageView imageViewHandTutorial;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Log.d("onCreate","onCreate");
		setContentView(R.layout.activity_routes);

		Globals = ((MyGlobals) getApplicationContext());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyRoutesActivity.this, MyRoutesActivity.this);

		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
		final String DistanceUnit = sharedPrefs.getString("prefDistanceUnit", "");
		idPharmacy = PharmacyId;
		distanceUnit = DistanceUnit;

		//lv = (ListView) findViewById(R.id.listView);
		mDragListView = (DragListView) findViewById(R.id.listView);
		tvRouteStatus = (TextView) findViewById(R.id.status_route);
		distance = (TextView)findViewById(R.id.total_distance);
		time = (TextView)findViewById(R.id.total_time);

		mySelectedUser = myUser;

		/*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
				String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
				String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
				String barcode = ((TextView) itemClicked.findViewById(R.id.customerBarcode)).getText().toString();

				Globals.setValue("remember_idRoute",idRoute);

				// start detail activity
				Intent intent = new Intent(getApplicationContext(), AddressDetailActivity.class);
				intent.putExtra("idAddress", idAddress);
				intent.putExtra("idRoute", idRoute);
				intent.putExtra("idScript", idScript);
				intent.putExtra("barcode", barcode);
				startActivity(intent);
			}
		});*/

		// drag and drop and reorder the listview
		mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
		mDragListView.setDragListListener(new DragListView.DragListListenerAdapter() {
			@Override
			public void onItemDragStarted(int position) {
				//Toast.makeText(getApplicationContext(), "Start - position: " + position, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onItemDragEnded(int fromPosition, int toPosition) {
				if (fromPosition != toPosition) {

					HashMap<String, String> hashMap_item = addressList.get(toPosition);
					String idAddress = Html.fromHtml(hashMap_item.get("id")).toString();
					String sequenceAddress = Html.fromHtml(hashMap_item.get("letter")).toString();
					//String sequenceAddress = Html.fromHtml(hashMap_item.get("sequence")).toString();

					new UpdateAddressSequence(PharmacyId, idRoute, idAddress, String.valueOf(toPosition + 1)).execute();
					//Toast.makeText(getApplicationContext(), "End - position: " + (toPosition + 1) + " sequence: " + sequenceAddress, Toast.LENGTH_SHORT).show();
				}
			}
		});



		// swipe the listview
		mDragListView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
			@Override
			public void onItemSwipeStarted(ListSwipeItem item) {
				//mRefreshLayout.setEnabled(false);
			}

			@Override
			public void onItemSwipeEnded(final ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {
				//mRefreshLayout.setEnabled(true);

				// Swipe to delete on left
				if (swipedDirection == ListSwipeItem.SwipeDirection.RIGHT) {

					AlertDialog.Builder builder = new AlertDialog.Builder(MyRoutesActivity.this);

					builder.setTitle(getString(R.string.alert_dialog_title_delete_address))
							.setMessage(getString(R.string.alert_dialog_text_delete_address))
							.setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {

									// continue delete
									HashMap<Long, String> adapterItem = (HashMap<Long, String>) item.getTag();
									int pos = mDragListView.getAdapter().getPositionForItem(adapterItem);
									mDragListView.getAdapter().removeItem(pos);

									itemm = item;
									poss = pos;

									// Gets called when the user deletes an item.
									mDragListView.performDismiss(pos);
								}
							})
							.setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {

									// do nothing
									View view = item;
									((ListSwipeItem) view).resetSwipe(true);
								}
							})
							.setIcon(android.R.drawable.ic_dialog_alert)
							.show();
				}
			}
		});


		DragListView.OnDismissCallback callback = new DragListView.OnDismissCallback() {
			// Gets called whenever the user deletes an item.
			public DragListView.Undoable onDismiss(DragListView listView, final int position) {

				// Return an Undoable implementing every method
				return new DragListView.Undoable() {

					// Method is called when user undoes this deletion
					public void undo() {
						// Reinsert item to list
						//itemList.add(positionItem, map);
						//mAdapter.notifyDataSetChanged();

						HashMap<Long, String> adapterItem = (HashMap<Long, String>) itemm.getTag();
						mDragListView.getAdapter().addItem(poss, adapterItem);
					}

					// Return an undo message for that item
					public String getTitle() {
						return " " + getString(R.string.text_deleted);
					}

					// Called when user cannot undo the action anymore
					public void discard() {
						// Use this place to e.g. delete the item from database
						Toast.makeText(MyRoutesActivity.this, getString(R.string.text_discarded), Toast.LENGTH_SHORT).show();
					}
				};
			}
		};

		DragListView.UndoMode mode = DragListView.UndoMode.SINGLE_UNDO;
		mDragListView.SwipeUndo(mDragListView, callback, mode);

		new LoadPharmacyLocation(PharmacyId).execute();
		new LoadUserListSpinner(PharmacyId).execute();


		// Tutorial
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		if(tabletSize){
			// tablet
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			if (!prefs.getBoolean("10", false)) {
				// <---- run one time code here
				showcaseView = new ShowcaseView.Builder(this)
						.setTarget(new ViewTarget(R.id.listView, this))
						.setContentTitle(getString(R.string.title_list_address))
						.setContentText(getString(R.string.text_list_address))
						.setShowcaseDrawer(new CustomShowcaseViewListViewTablet(getResources()))
						.singleShot(10)
						.build();
				RelativeLayout.LayoutParams buttonViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
				buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_TOP);
				buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				buttonViewLayoutParams.setMargins(0, 50, 50, 0);
				showcaseView.setButtonPosition(buttonViewLayoutParams);
				showcaseView.forceTextPosition(ShowcaseView.RIGHT_OF_SHOWCASE);

				imageViewHandTutorial = (ImageView) findViewById(R.id.hand_swipe_right);
				imageViewHandTutorial.setVisibility(View.VISIBLE);

				animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
						R.anim.slide_image);

				imageViewHandTutorial.startAnimation(animSlide);

				showcaseView.overrideButtonClick(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try{
							imageViewHandTutorial.clearAnimation();
							imageViewHandTutorial.setVisibility(View.GONE);
							showcaseView.hide();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
				});

				// mark first time has runned.
				SharedPreferences.Editor editor10 = prefs.edit();
				editor10.putBoolean("10", true);
				editor10.commit();
			}

		}else{
			// phone
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			if (!prefs.getBoolean("11", false)) {
				// <---- run one time code here
				showcaseView = new ShowcaseView.Builder(this)
						.setTarget(new ViewTarget(R.id.listView, this))
						.setContentTitle(getString(R.string.title_list_address))
						.setContentText(getString(R.string.text_list_address))
						.setShowcaseDrawer(new CustomShowcaseViewListViewPhone(getResources()))
						.singleShot(11)
						.build();
				RelativeLayout.LayoutParams buttonViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
				buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_TOP);
				buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				buttonViewLayoutParams.setMargins(0, 50, 50, 0);
				showcaseView.setButtonPosition(buttonViewLayoutParams);
				showcaseView.forceTextPosition(ShowcaseView.ABOVE_SHOWCASE);

				imageViewHandTutorial = (ImageView) findViewById(R.id.hand_swipe_right);
				imageViewHandTutorial.setVisibility(View.VISIBLE);

				animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
						R.anim.slide_image);

				imageViewHandTutorial.startAnimation(animSlide);

				showcaseView.overrideButtonClick(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try{
							imageViewHandTutorial.clearAnimation();
							imageViewHandTutorial.setVisibility(View.GONE);
							showcaseView.hide();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
				});

				// mark first time has runned.
				SharedPreferences.Editor editor11 = prefs.edit();
				editor11.putBoolean("11", true);
				editor11.commit();
			}

		}



	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			// if a route is "End Route", and if go back to SelectAction (main screen),
			// then refresh to create a new route if needed
			Globals.setValue("refresh_route","refresh_route");
			if(tutorial_scanned_package){
				Globals.setValue("tutorial_scanned_package","tutorial_scanned_package");
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onResume(){
		super.onResume();
		Log.d("onResume","onResume");
		String update_status_package = Globals.getValue("update_addresses");
		String update_status_route = Globals.getValue("update_route");
		String tutorial_update_status_package = Globals.getValue("tutorial_update_status_package");

		if(!tutorial_update_status_package.equals("")){
			try{
				showcaseView.hide();
			}catch (Exception e){
				e.printStackTrace();
			}
		}

		try{
			if(!showcaseView.isShowing()){
				imageViewHandTutorial.clearAnimation();
				imageViewHandTutorial.setVisibility(View.GONE);
			}
		}catch (Exception e){
			e.printStackTrace();
		}

		if(update_status_package != ""){

			new LoadListRouteAddress(idRoute, idPharmacy).execute();
			//new LoadRouteListSpinner(idPharmacy).execute();

			Globals.setValue("update_addresses","");
		}

		if(update_status_route != ""){

			new LoadRouteListSpinner(idPharmacy).execute();
			ItemAdapter listAdapter = new ItemAdapter(MyRoutesActivity.this, null, R.layout.list_item, R.id.item_layout, false, idRoute);
			mDragListView.setAdapter(listAdapter, true);
			//lv.setAdapter(null);
			tvRouteStatus.setText(null);
			distance.setText(null);
			time.setText(null);

			Globals.setValue("remember_idRoute","");
			Globals.setValue("update_route","");
		}
	}

	@Override
	protected void onStop() {
		Log.d("onStop","onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.d("onDestroy","onDestroy");
		hideProgressDialog();
		super.onDestroy();
	}

	/**
	 * Background Async Task to Load my sent request by making HTTP Request
  	 * */
	class LoadUserListSpinner extends AsyncTask<String, String, String> {

		String pharmacyId;

		public LoadUserListSpinner(String pharmacyId){
			this.pharmacyId = pharmacyId;
		}

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
        showProgressDialog(getString(R.string.loading_alert_dialog));
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {

			Globals = ((MyGlobals)getApplication());

			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
	    	 // load data from provider
		    //addressList = new ArrayList<HashMap<String, String>>();
			userList = new ArrayList<HashMap<String, String>>();

			//List<cContact> myContact = MyProvider.getPharmacyUsers(pharmacyId);
			List<cContact> myContact = MyProvider.getMyUsers(pharmacyId);
			for(cContact entry : myContact)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("pid", entry.id);
				map.put("username", entry.username);
				map.put("name", entry.name);

				// adding HashList to ArrayList
				userList.add(map);
			}

	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
	    	runOnUiThread(new Runnable() {
	            public void run() {

					String isManager = Globals.getValue("manager");
					if (isManager == "yes")
					{
						List<String> userArray =  new ArrayList<String>();
						userArray.add(0, getString(R.string.select_user));
						userkeyArray.add(0,"");
						userNameArray.add(0,"");

						if(userList.size()>0){
							//userArray.add(0, getString(R.string.select_user));
							//userkeyArray.add(0, getString(R.string.select_user));
						}else{
							userArray.add(0, getString(R.string.select_no_user));
							userkeyArray.add(0, getString(R.string.select_no_user));
						}

						for (HashMap<String, String> map : userList)
							for (Map.Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
								if (entry.getKey() == "username") userNameArray.add(entry.getValue());
								if (entry.getKey() == "name") userArray.add(entry.getValue());
							}

						//ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyRoutesActivity.this, android.R.layout.simple_spinner_item, userArray);
						//user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
						spinnerLayout.setVisibility(View.VISIBLE);
						Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
						sUsers.setVisibility(View.VISIBLE);
						//sUsers.setAdapter(user_adapter);

						SpinnerAdapter user_adapter = new SpinnerAdapter(
								MyRoutesActivity.this,
								R.layout.spinner_adapter,
								userArray
						);
						sUsers.setAdapter(user_adapter);

						sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
								Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
								String mIndex = sUsers.getSelectedItem().toString();

								if(mIndex.contains(getString(R.string.select_user))){
									// do nothing

								}else{

									int myIndex = sUsers.getSelectedItemPosition();
									String userid = userkeyArray.get(myIndex);
									mySelectedUser = userid;

									new LoadRouteListSpinner(pharmacyId).execute();

								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent) {

							}
						});

						// auto select
						int selectUserIndex = userNameArray.indexOf(myUser.toLowerCase());
						sUsers.setSelection(selectUserIndex);
						mySelectedUser = String.valueOf(selectUserIndex);

					}else{
						RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
						spinnerLayout.setVisibility(View.GONE);
						Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
						sUsers.setVisibility(View.GONE);

						mySelectedUser = "";
						new LoadRouteListSpinner(pharmacyId).execute();
					}

	            } 
	        });
	    	hideProgressDialog();
	    }
	}


	class LoadRouteListSpinner extends AsyncTask<String, String, String> {

		String pharmacyId;

		public LoadRouteListSpinner(String pharmacyId){
			this.pharmacyId = pharmacyId;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			// load data from provider
			routeList = new ArrayList<HashMap<String, String>>();

			List<cMyRoute> myRoute = MyProvider.getMyRoutes(mySelectedUser,pharmacyId,"","","");
			for(cMyRoute entry : myRoute)
			{

				List<cRouteAddress> myAddress = MyProvider.getRouteAddress(entry.id, pharmacyId);
				if(myAddress.size() > 0){
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put("id", entry.id);
					map.put("location", entry.location);
					map.put("status_description", entry.status_description);

					// adding HashList to ArrayList
					routeList.add(map);
				}

			}

			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {
				public void run() {

					/**
					 * Updating parsed JSON data into ListView
					 * */
					List<String> routeArray =  new ArrayList<String>();
					routeArray.add(0, getString(R.string.select_route));
					routekeyArray =  new ArrayList<String>();
					final List<String> routestatusArray =  new ArrayList<String>();

					if(routeList.size()>0){
						routeArray.add(0, getString(R.string.select_route));
						routekeyArray.add(0, getString(R.string.select_route));
						routestatusArray.add(0, "");
					}else{
						routeArray.add(0, getString(R.string.select_no_route));
						routekeyArray.add(0, getString(R.string.select_no_route));
						routestatusArray.add(0, "");
					}

					for (HashMap<String, String> map : routeList)
						for (Map.Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "id") routekeyArray.add(entry.getValue());
							if (entry.getKey() == "location") routeArray.add(entry.getValue());
							if (entry.getKey() == "status_description") routestatusArray.add(entry.getValue());
						}

					Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);

					SpinnerAdapter route_adapter = new SpinnerAdapter(
							MyRoutesActivity.this,
							R.layout.spinner_adapter,
							routekeyArray
					);
					sRoutes.setAdapter(route_adapter);

					sRoutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
							Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
							String mIndex = sRoutes.getSelectedItem().toString();

							if(mIndex.contains(getString(R.string.select_route))){
								// do nothing

							}else{

								int myIndex = sRoutes.getSelectedItemPosition();
								String routeid = routekeyArray.get(myIndex);
								String routestatus = routestatusArray.get(myIndex);
								Log.d("routeid",routeid);

								idRoute = routeid;

								if(routeList.size() > 0){
									loadMapByPharmacyAddress(routeid, pharmacyId);
									new LoadListRouteAddress(routeid, pharmacyId).execute();
									//new LoadPackageOfRoute(routeid, pharmacyId).execute();
								}else{
									//ListView lv = (ListView) findViewById(R.id.listView);
									//lv.setAdapter(null);

									DragListView lv = (DragListView) findViewById(R.id.listView);
									ItemAdapter listAdapter = new ItemAdapter(MyRoutesActivity.this, null, R.layout.list_item, R.id.textViewLetterAddress, false, idRoute);
									lv.setAdapter(listAdapter, true);


									WebView webView = (WebView)findViewById(R.id.webView);
									if (Build.VERSION.SDK_INT < 18) {
										webView.clearView();
									} else {
										webView.loadUrl("about:blank");
									}
								}

								tvRouteStatus.setText(routestatus);
							}
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {

						}
					});

					String start_route = Globals.getValue("start_route");
					if(start_route != ""){
						int spinnerPosition = route_adapter.getPosition(start_route);
						sRoutes.setSelection(spinnerPosition);
						Globals.setValue("start_route","");
					} else {

						if (sRoutes.getCount() == 2){
							sRoutes.setSelection(1);
						}else {
							String id_route = Globals.getValue("remember_idRoute");
							if(id_route != ""){
								int spinnerPosition = route_adapter.getPosition(id_route);
								sRoutes.setSelection(spinnerPosition);
								idRoute = id_route;
							}
						}
					}

				}
			});
			hideProgressDialog();
		}
	}

	/**
	 * Background Async Task to Load my sent request by making HTTP Request
	 * */
	class LoadListRouteAddress extends AsyncTask<String, String, String> {

		public String routeid, pharmacyId;

		public LoadListRouteAddress(String routeid, String pharmacyId){
			this.routeid = routeid;
			this.pharmacyId = pharmacyId;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			// load data from provider
			addressList = new ArrayList<HashMap<String, String>>();

			//char letter='B';
			//int node = 1;
			String last_sequence = "0";

			//idRoute = Globals.getValue("remember_idRoute");

			List<cRouteAddress> myAddress = MyProvider.getRouteAddress(routeid, pharmacyId);
			for(cRouteAddress entry : myAddress)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  entry.id);
				map.put("customer_name",  entry.customer_name);
				map.put("customer_birthdate", entry.customer_birthdate);
				map.put("customer_sex",  entry.customer_sex);
				//map.put("customer_address", entry.customer_street +" "+ entry.customer_house +" , "+ entry.customer_zipcode +" , "+entry.customer_city +" , "+ entry.customer_country);
				map.put("customer_street", entry.customer_street);
				map.put("customer_house", entry.customer_house);
				map.put("customer_zipcode", entry.customer_zipcode);
				map.put("customer_city", entry.customer_city);
				map.put("customer_country", entry.customer_country);
				map.put("longitude", entry.longitude);
				map.put("latitude", entry.latitude);
				map.put("distance", entry.distance);
				map.put("time", entry.time);
				//map.put("sequence", entry.sequence);
				map.put("letter", String.format("%02d", Integer.valueOf(entry.sequence)));
				if(Integer.valueOf(last_sequence) < Integer.valueOf(entry.sequence)) last_sequence = entry.sequence;

				//map.put("letter", String.format("%02d", node++));
				//map.put("letter", getLetterFromNoddeNR(node++));
				//map.put("letter", String.valueOf((letter % 26)++));
				if(entry.customer_script.equals("0")){
					map.put("package_barcode", "Stop");
					map.put("customer_script", "Stop");
				}else{
					map.put("customer_script",  entry.customer_script);
				}

				// get Packages
				int unscanned_packages = 0;
				int scanned_packages = 0;
				int total_packages = 0;

				map.put("status_id", entry.status);
				map.put("package_barcode", entry.barcode);



				/*List<cPackages> myPackage = MyProvider.getAllPackages(routeid, entry.id, pharmacyId);
				for(cPackages entry1 : myPackage)
				{
					// nr of total packages and scanned packages and unscanned packages
						total_packages = total_packages + 1;

					// 1 = Loaded for delivery
					// 3 = In Transport
					// 5 = Delivered
					// status package for color
					if((entry1.status_id.equals("1")) || (entry1.status_id.equals("3"))){
						map.put("status_description_priority",  entry1.status_description);
						map.put("status_id_priority",  entry1.status_id);
						unscanned_packages = unscanned_packages + 1;
					}else{
						map.put("status_description",  entry1.status_description);
						map.put("status_id",  entry1.status_id);
						scanned_packages = scanned_packages + 1;

						tutorial_scanned_package = true;
					}

					if(total_packages > 1){
						// barcode package
						map.put("package_barcode_priority", entry1.package_barcode);
					}else{
						// barcode package
						map.put("package_barcode", entry1.package_barcode);
					}

				}*/

				map.put("unscanned_packages",  String.valueOf(unscanned_packages));
				map.put("scanned_packages",  String.valueOf(scanned_packages));
				map.put("total_packages",  String.valueOf(total_packages));

				// adding HashList to ArrayList
				addressList.add(map);
			}

			// add last address = pharmacy
			if(routeList.size() > 0){
				final cRoute pharmacy = MyProvider.getRoute(routeid, idPharmacy);
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("id", "0");
				map.put("customer_script",  "Pharmacy");
				map.put("customer_name",  "");
				map.put("customer_address", pharmacyAddress);
				map.put("customer_street", pharmacyStreet);
				map.put("customer_house", pharmacyHouse);
				map.put("customer_zipcode", pharmacyZipcode);
				map.put("customer_city", pharmacyCity);
				map.put("customer_country", pharmacyCountry);
				map.put("letter", String.format("%02d", (Integer.valueOf(last_sequence) + 1)));
				//map.put("letter", getLetterFromNoddeNR(node++));
				//map.put("letter", String.valueOf(letter++));
				map.put("status_description",  "Pharmacy");
				map.put("status_id",  "Pharmacy");
				map.put("unscanned_packages",  "");
				map.put("scanned_packages",  "");
				map.put("total_packages",  "");
				map.put("package_barcode", "Pharmacy");
				map.put("distance", pharmacy.return_distance);
				map.put("time", pharmacy.return_time); // seconds
				map.put("sequence", "Pharmacy");

				addressList.add(map);
			}
			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {
				public void run() {

					//adapter = new MyRoutesAdapter(MyRoutesActivity.this, R.layout.activity_routes_listview_items, addressList);
					//lv.setAdapter(adapter);

					mDragListView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
					ItemAdapter listAdapter = new ItemAdapter(MyRoutesActivity.this, addressList, R.layout.list_item, R.id.textViewLetterAddress, false, idRoute);
					mDragListView.setAdapter(listAdapter, true);
					mDragListView.setCanDragHorizontally(false);
					//mDragListView.setCustomDragItem(new MyDragItem(getApplicationContext(), R.layout.list_item));



					distance = (TextView)findViewById(R.id.total_distance);
					time = (TextView)findViewById(R.id.total_time);

					// total distance
					int myNum_distance = 0;
					int total_distance = 0;


					if(distanceUnit.equals(getResources().getString(R.string.settings_kilometers))){
						// Kilometers

						for (HashMap<String, String> map : addressList)
							for (Map.Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "distance")
								{
									try {
										myNum_distance = Integer.parseInt(entry.getValue().toString());
										entry.setValue(MyTimeUtils.getFormattedKilometers(myNum_distance));

									} catch(NumberFormatException nfe) {
										myNum_distance = 0;
									}
									total_distance = total_distance + myNum_distance;
								}
							}

						distance.setText(MyTimeUtils.getFormattedKilometers(total_distance));

					}else if(distanceUnit.equals(getResources().getString(R.string.settings_miles))){
						// Miles

						for (HashMap<String, String> map : addressList)
							for (Map.Entry<String, String> entry : map.entrySet())
							{
								if (entry.getKey() == "distance")
								{
									try {
										myNum_distance = Integer.parseInt(entry.getValue().toString());
										entry.setValue(MyTimeUtils.getFormattedMiles(myNum_distance));

									} catch(NumberFormatException nfe) {
										myNum_distance = 0;
									}
									total_distance = total_distance + myNum_distance;
								}
							}

						distance.setText(MyTimeUtils.getFormattedMiles(total_distance));
					}




					// total_time
					int myNum_time = 0;
					int total_time = 0;
					for (HashMap<String, String> map : addressList)
						for (Map.Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "time")
							{
								try {
									myNum_time = Integer.parseInt(entry.getValue().toString());
									entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum_time));

								} catch(NumberFormatException nfe) {
									myNum_time = 0;
								}
								total_time = total_time + myNum_time;
							}
						}

					//time.setText(String.valueOf(total_time) + " min");
					time.setText(MyTimeUtils.getFormattedHourMinutes(total_time));

                    /*ListAdapter adapter = null;

                    adapter = new SimpleAdapter(
                            MyRoutesActivity.this
                            , addressList
                            , R.layout.activity_new_order_listview_items, new String[] { "id", "customer_script" ,"customer_name", "customer_street", "letter" },
                            new int[] { R.id.textViewID, R.id.customerScript, R.id.customerName, R.id.customerStreet, R.id.textViewLetterAddress});
                    lv.setAdapter(adapter);*/

				}
			});

			hideProgressDialog();
		}
	}

	/**
	 * Background Async Task to Load my sent request by making HTTP Request
	 * */
	/*class LoadPackageOfRoute extends AsyncTask<String, String, String> {

		public String routeid, pharmacyId;

		public LoadPackageOfRoute(String routeid, String pharmacyId){
			this.routeid = routeid;
			this.pharmacyId = pharmacyId;
		}

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		*//**
		 * getting items from url
		 * *//*
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			packageRouteList = new ArrayList<HashMap<String, String>>();

			List<cPackageRoute> myPackage = MyProvider.getPackagesOfRoute(routeid, "", pharmacyId);
			for(cPackageRoute entry : myPackage)
			{
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("id",  entry.id);
				map.put("scriptid",  entry.scriptid);
				map.put("packageid",  entry.package_barcode);
				map.put("addressid", entry.addressid);
				map.put("package_status",  entry.package_status);
				map.put("status_description",  entry.status_description);

				// adding HashList to ArrayList
				packageRouteList.add(map);
			}

			return "";
		}

		*//**
		 * After completing background task Dismiss the progress dialog
		 * **//*
		protected void onPostExecute(String file_url) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {
				public void run() {
					int totalScanned = 0;
					int totalUnScanned = 0;
					String myRow = "";
					for (HashMap<String, String> map : packageRouteList)
						for (Map.Entry<String, String> entry : map.entrySet())
						{
							if (entry.getKey() == "status_description")
							{
								try {
									myRow = entry.getValue().toString();
									String row = String.valueOf(myRow);

									if(!row.equals("Delivered") && !row.equals("Not at home")){
										totalUnScanned++;
									}else{
										totalScanned++;
									}
								} catch(NumberFormatException nfe) {

								}
							}
						}

					final int totalScanned1 = totalScanned;

					// if are all packages scanned , update route status
					if(totalUnScanned == 0) {

						// if still have status route 3
						new LoadRoute(routeid, pharmacyId).execute();

						// alert - end route?
						*//*new AlertDialog.Builder(MyRoutesActivity.this)
								.setTitle("Route")
								.setMessage("End route?")
								.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										new UpdateRouteStatus(pharmacyId, idRoute, "5").execute();
									}
								})
								.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										// do nothing
									}
								})
								.setIcon(android.R.drawable.ic_dialog_alert)
								.show();*//*

					}
				}
			});

			hideProgressDialog();
		}
	}*/

	/** AsyncTask update status route  */
	private class LoadRoute extends AsyncTask<String, String, String>{
		String pharmacyId = "";
		String routeId = "";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		public LoadRoute(String routeId, String pharmacyId){
			this.pharmacyId = pharmacyId;
			this.routeId = routeId;
		}

		@Override
		protected String doInBackground(String... args) {

			// call web method
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			cRoute route = MyProvider.getRoute(routeId, pharmacyId);

			return route.status;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(final String status) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
				if(status.equals("3")){
					// alert - end route?
					//new AlertDialog.Builder(MyRoutesActivity.this)
					//		.setTitle("Route")
					//		.setMessage("End route?")
					//		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					//			public void onClick(DialogInterface dialog, int which) {
					//				new UpdateRouteStatus(pharmacyId, idRoute, "5").execute();
					//			}
					//		})
					//		.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					//			public void onClick(DialogInterface dialog, int which) {
					//				// do nothing
					//			}
					//		})
					//		.setIcon(android.R.drawable.ic_dialog_alert)
					//		.show();
				}

				}
			});
			hideProgressDialog();
		}
	}

	/** AsyncTask update status route  */
	/*private class UpdateRouteStatus extends AsyncTask<String, String, Boolean>{
		String pharmacyId = "";
		String routeId = "";
		String statusId = "";

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		public UpdateRouteStatus(String pharmacyId, String routeId, String statusId){
			this.pharmacyId = pharmacyId;
			this.routeId = routeId;
			this.statusId = statusId;
		}

		@Override
		protected Boolean doInBackground(String... args) {

			// call web method
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			boolean suc = false;

			suc = MyProvider.updateRouteStatus(pharmacyId, routeId, statusId);

			return suc;
		}

		*//**
		 * After completing background task Dismiss the progress dialog
		 * **//*
		protected void onPostExecute(final Boolean success) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (success == false)
					{
						Log.d("Update Route Status", "false");
						Toast.makeText(MyRoutesActivity.this, "Failed to update route", Toast.LENGTH_SHORT).show();

						String error_message = MyProvider.last_error;
						if(error_message.equals("1")){
							Toast.makeText(MyRoutesActivity.this, error_message, Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(MyRoutesActivity.this, "Route didn't update: " + error_message, Toast.LENGTH_SHORT).show();
						}
					}
					else
					{
						Log.d("Update Route Status", "true");
					}
				}
			});
			hideProgressDialog();
		}
	}*/

	/** AsyncTask update status route  */
	private class UpdateAddressSequence extends AsyncTask<String, String, Boolean>{
		String pharmacyId = "";
		String routeId = "";
		String addressId = "";
		String sequence = "";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		public UpdateAddressSequence(String pharmacyId, String routeId, String addressId, String sequence){
			this.pharmacyId = pharmacyId;
			this.routeId = routeId;
			this.addressId = addressId;
			this.sequence = sequence;
		}

		@Override
		protected Boolean doInBackground(String... args) {

			// call web method
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			boolean suc = false;

			suc = MyProvider.updateRouteAddress(pharmacyId, routeId, addressId, sequence);

			return suc;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(final Boolean success) {
			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (success == false)
					{
						Log.d("Update Address List", "false");

						String error_message_id = MyProvider.last_error_nr;
						String error_message = MyProvider.last_error;
						if(error_message_id.equals("1")){
							Toast.makeText(MyRoutesActivity.this, error_message, Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(MyRoutesActivity.this, getString(R.string.toast_text_failed_update_list) + " " + error_message, Toast.LENGTH_SHORT).show();
						}
					}
					else
					{
						Log.d("Update Address List", "true");
						new LoadListRouteAddress(idRoute, idPharmacy).execute();
						loadMapByPharmacyAddress(idRoute, pharmacyId);
					}
				}
			});
			hideProgressDialog();
		}
	}

	/**
	 * Background Async Task to Load Address Item by making HTTP Request
	 * */
	class LoadPharmacyLocation extends AsyncTask<String, String, String> {

		String pharmacyId;

		public LoadPharmacyLocation(String pharmacyId){
			this.pharmacyId = pharmacyId;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			showProgressDialog(getString(R.string.loading_alert_dialog));
		}

		/**
		 * getting items from url
		 * */
		protected String doInBackground(String... args) {

			Globals = ((MyGlobals) getApplicationContext());
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			final cPharmacyAddress address = MyProvider.getPharmacyAddress(pharmacyId);

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					pharmacyAddress = address.address1 + "  " + address.address_house + " , " + address.address_zipcode + " , " + address.address_city + " , " + address.address_country;
					pharmacyStreet = address.address1;
					pharmacyHouse = address.address_house;
					pharmacyZipcode = address.address_zipcode;
					pharmacyCity = address.address_city;
					pharmacyCountry = address.address_country;
				}
			});
			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (MyRoutesActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
				return;
			}
			// Dismiss the progress dialog
			hideProgressDialog();
		}
	}

	public void loadMapByPharmacyAddress(String routeid, String pharmacy){
		String startpoint = pharmacyAddress;

		//	Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
			Log.d("startpoint", startpoint);

		WebView webView = (WebView)findViewById(R.id.webView);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		//webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
		String map = getString(R.string.google_map_multiple); // is google map
		double size_tablet = MyTimeUtils.tabletSize(MyRoutesActivity.this);
		if (size_tablet > 9) {
			//Device is a 10" tablet
			webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 950, 650 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));
			//webView.loadUrl(MyProvider.driving_route(startpoint, "1", routeid, pharmacy, 950, 650 , 0.0, 0.0, 0.0, 0.0, url_map));

		}else if (size_tablet > 6) {
			//Device is a 7" tablet
			webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 450, 350 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));
			//webView.loadUrl(MyProvider.driving_route(startpoint, "1", routeid, pharmacy, 650, 450 , 0.0, 0.0, 0.0, 0.0, url_map));

		}else if (size_tablet > 4.98) {
			// Device is a bigger phone
			webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 395, 185 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));

		}else{
			webView.loadUrl(MyProvider.driving_route(map, startpoint, "1",routeid, pharmacy, 340, 180 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));
			//webView.loadUrl(MyProvider.driving_route(startpoint, "1",routeid, pharmacy, 0, 0 , 0.0, 0.0, 0.0, 0.0, url_map));
		}
	}

	public void loadMapByGPSLocation(String routeid, String pharmacy){

		//Get GPS coordinates
		GPSTracker gps = new GPSTracker(MyRoutesActivity.this);
		String startpoint = "";
		double start_latitude = gps.getLatitude();
		double start_longitude = gps.getLongitude();
		//double end_latitude = Double.valueOf(endLatitude);
		//double end_longitude = Double.valueOf(endLongitude);

		// check if GPS enabled
		if(gps.canGetLocation()){

			start_latitude = gps.getLatitude();
			start_longitude = gps.getLongitude();
			startpoint = gps.getAddress();

		//	Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
			Log.d("startpoint", startpoint);

			// Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

		}else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			// gps.displayLocationSettingsRequest(AddressDetailActivity.this);
			gps.showSettingsAlert();
		}

		WebView webView = (WebView)findViewById(R.id.webView);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		//webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
		double size_tablet = MyTimeUtils.tabletSize(MyRoutesActivity.this);
		if (size_tablet > 9) {
			//Device is a 10" tablet
			webView.loadUrl(MyProvider.driving_route("", startpoint, "", routeid, pharmacy, 950, 650 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));

		}else if (size_tablet > 6) {
			//Device is a 7" tablet
			webView.loadUrl(MyProvider.driving_route("", startpoint, "", routeid, pharmacy, 450, 350 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));

		}else{
			webView.loadUrl(MyProvider.driving_route("", startpoint, "", routeid, pharmacy, 340, 180 , 0.0, 0.0, 0.0, 0.0, MyRoutesActivity.this));
		}
	}

	public String getLetterFromNoddeNR(int num)
	{
		int letter1 = (num / 26);
		int letter2 = (num % 26) + 65;

		String letter = Character.toString((char) letter2);
		if (letter1 > 0) {
			letter = Character.toString((char) (letter1 + 64)) + Character.toString((char) letter2);
		}
		return letter;
	}

	/**
	 * Shows a Progress Dialog 
	 *  
	 * @param msg
	 */
	public void showProgressDialog(String msg) 
	{
		
		// check for existing progressDialog
		if (progressDialog == null) {
			// create a progress Dialog
			progressDialog = new ProgressDialog(this);

			// remove the ability to hide it by tapping back button
			progressDialog.setIndeterminate(true);
			
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			
			progressDialog.setMessage(msg);

		}

		// now display it.
		progressDialog.show();		
	}	
	
	
	/**
	 * Hides the Progress Dialog
	 */
	public void hideProgressDialog() {
		
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		
		progressDialog = null;
	}

	// drag and drop adn reorder listview
	private static class MyDragItem extends DragItem {

		MyDragItem(Context context, int layoutId) {
			super(context, layoutId);
		}

		@Override
		public void onBindDragView(View clickedView, View dragView) {
			CharSequence text = ((TextView) clickedView.findViewById(R.id.textViewLetterAddress)).getText();
			((TextView) dragView.findViewById(R.id.textViewLetterAddress)).setText(text);

			dragView.findViewById(R.id.item_layout).setBackgroundColor(dragView.getResources().getColor(R.color.white1));
		}
	}



	private void selectSpinnerValue(Spinner spinner, Object value) {
		for (int i = 0; i < spinner.getCount(); i++) {
			if (spinner.getItemAtPosition(i).equals(value)) {
				spinner.setSelection(i);
				break;
			}
		}
	}
}
