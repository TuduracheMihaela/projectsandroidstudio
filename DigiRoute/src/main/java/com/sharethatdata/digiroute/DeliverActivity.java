package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Bitmap;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;


import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.sharethatdata.digiroute.BarcodeReader.Barcode.BarcodeCaptureActivity;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.digiroute.Scanner.IntentResult;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewEditTextPhone;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewEditTextTablet;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseView;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.ViewTarget;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cAddress;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackageImage;
import com.sharethatdata.logistics_webservice.datamodel.cPackagesbyDeliveryId;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sharethatdata.digiroute.MyGlobals.getContext;


public class DeliverActivity extends Activity implements LocationListener {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String pharmacy_id = "";

    private static final int BARCODE_READER_REQUEST_CODE = 9001;

    ArrayList<HashMap<String, String>> packageList; // List Packages by route
    ArrayList<HashMap<String, String>> packageRouteList; // List Packages
    List<String> statuskeyArray;

    ProgressDialog progressDialog;
    private byte[] bitmapByteArray;
    private Bitmap myBitmap;
    private String myImageData = "";

    DrawingView dv ;

    TextView textViewPackageId;
    TextView textViewCustomerScript;
    TextView textViewCustomerAddress;
    TextView textViewPackageBarcode;
    TextView textViewPackageStatus;
    TextView textViewPackageDeliverRemark;
    TextView textViewPackageNotifyPatient;
    TextView textViewPackageSignatureName;
    TextView textViewPackagedeliverRemarkLabel;
    TextView textViewPackageNotifyPatientLabel;
    TextView textViewPackageSignatureNameLabel;
    Button btnClear;
    Button btnSave;
    //Button btnDelivered;
    //Button btnNotHome;
    //Button btnCancel;
    EditText signature_person;
    EditText remark;
    CheckBox notify_patient;
    ImageView imageViewMarker, imageViewCheck;
    LinearLayout layoutDrawing, layoutSignature;
    RelativeLayout spinnerStatusLayout;
    Spinner sStatus;

    String idPackage, idDelivery, packageBarcode, packageAddressId, packageStatus, packageStatusID, idRoute, idScript, packageAddress;

    boolean blUseCamera = false;
    boolean blFirstTimeLoadActivity = false;
    boolean blQuickScan = false;

    cPackageImage packageImage;

    private static ProgressDialog pDialog1;
    private String URL_ART = "/art";

    // Tutorial
    ShowcaseView showcaseView;
    ShowcaseView showcaseViewSpinner;

    boolean processing_scan = false;

    /*
    Location
     */
    protected LocationManager locationManager;
    Location loc;
    boolean canGetLocation = true;
    private static final String TAG = "GPS";
    private static final String DEBUG_TAG = "NetworkStatusExample";
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    Double latitude = 0.0;
    Double longitude = 0.0;

    TelephonyManager telephony;
    PhoneCustomStateListener phoneCustomStateListener;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver);

        //checkWifi();

        //phoneCustomStateListener = new PhoneCustomStateListener();
        //telephony = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        //telephony.listen(phoneCustomStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(DeliverActivity.this, DeliverActivity.this);

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        getLocation();

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        final String packageLength = sharedPrefs.getString("prefScannerPackageLength", "1");
        pharmacy_id = PharmacyId;

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            blQuickScan = false;
            packageAddress = null;

            idPackage= null;
            idRoute= null;
            packageBarcode = null;
            packageStatus = null;
            packageStatusID = null;
            packageAddressId = null;
            idScript = null;
            idDelivery = null;
        } else {
            blQuickScan = extras.getBoolean("quick_scan");
            packageAddress = extras.getString("packageAddress");

            idPackage= extras.getString("idPackage");
            idRoute= extras.getString("idRoute");
            packageBarcode= extras.getString("packageIdCode");
            packageStatus= extras.getString("packageStatus");
            packageStatusID= extras.getString("packageStatusID");
            packageAddressId= extras.getString("idAddress");
            idScript= extras.getString("idScript");
            idDelivery= extras.getString("idDelivery");
        }

        dv = (DrawingView) findViewById(R.id.drawing);
       // dv.label_text = "Signature for receiving script " + idScript + ", x packages";
        dv.label_text = "";
        dv.patient_text = "";
        //dv.datetime_text = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        dv.datetime_text = "";
        dv.setVisibility(View.GONE);


        TextView textViewCustomerScriptLabel = (TextView) findViewById(R.id.textview_customer_script);
        TextView textViewPackageBarcodeLabel = (TextView) findViewById(R.id.textview_package_barcode);
        TextView textViewPackageStatusLabel = (TextView) findViewById(R.id.textview_package_status);
        textViewPackagedeliverRemarkLabel = (TextView) findViewById(R.id.packageDeliverRemarkLabel);
        textViewPackageNotifyPatientLabel = (TextView) findViewById(R.id.packageNotifyPatientLabel);
        textViewPackageSignatureNameLabel = (TextView) findViewById(R.id.packageSignatureNameLabel);
        textViewPackageId = (TextView) findViewById(R.id.textViewPackageId);
        textViewCustomerScript = (TextView) findViewById(R.id.textViewScript);
        textViewPackageBarcode = (TextView) findViewById(R.id.package_barcode);
        textViewCustomerAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewPackageStatus = (TextView) findViewById(R.id.package_status);
        textViewPackageDeliverRemark = (TextView) findViewById(R.id.packageDeliverRemarkText);
        textViewPackageNotifyPatient = (TextView) findViewById(R.id.packageNotifyPatientText);
        textViewPackageSignatureName = (TextView) findViewById(R.id.packageSignatureNameText);
        /*btnDelivered = (Button) findViewById(R.id.btnConfirmDelivered);
        btnNotHome = (Button) findViewById(R.id.btnConfirmNotHome);
        btnCancel = (Button) findViewById(R.id.btnConfirmCancel);*/

        ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit1);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnClear = (Button) findViewById(R.id.btnClear);
        EditText editText = (EditText) findViewById(R.id.editText_barcode);
        signature_person = (EditText) findViewById(R.id.signature_person);
        remark = (EditText) findViewById(R.id.remark);
        notify_patient = (CheckBox) findViewById(R.id.notify_patient);
        imageViewMarker = (ImageView) findViewById(R.id.marker_package);
        imageViewCheck = (ImageView) findViewById(R.id.check_package);
        layoutDrawing = (LinearLayout) findViewById(R.id.layoutDrawing);
        layoutSignature = (LinearLayout) findViewById(R.id.layoutSignature);
        spinnerStatusLayout = (RelativeLayout) findViewById(R.id.statusSpinnerLayout);
        sStatus = (Spinner) findViewById(R.id.spinnerStatus);

        Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");
        textViewPackageBarcode.setTypeface(font1);
        textViewPackageBarcodeLabel.setTypeface(font1);
        textViewCustomerScript.setTypeface(font2);
        textViewCustomerScriptLabel.setTypeface(font2);
        textViewCustomerAddress.setTypeface(font2);
        textViewPackageStatus.setTypeface(font2);
        textViewPackageStatusLabel.setTypeface(font2);
        textViewPackageDeliverRemark.setTypeface(font2);
        textViewPackageDeliverRemark.setTextColor(getResources().getColor(R.color.red));
        textViewPackagedeliverRemarkLabel.setTypeface(font2);
        textViewPackageNotifyPatient.setTypeface(font2);
        textViewPackageNotifyPatientLabel.setTypeface(font2);
        textViewPackageSignatureName.setTypeface(font2);
        textViewPackageSignatureNameLabel.setTypeface(font2);
        editText.setTypeface(font2);
        btnSave.setTypeface(font1);
        btnClear.setTypeface(font1);

        // set text package
        textViewPackageId.setText(idPackage);
        textViewPackageBarcode.setText(packageBarcode);
        textViewPackageStatus.setText(packageStatus);
        textViewCustomerScript.setText(Html.fromHtml(idScript));

        // Delivered, Not at home, Denied : show spinner , user can reselect., show signature
        // Cancelled: show dialog question: are you sure to cancel this package?
        List<String> statusArray =  new ArrayList<String>();
        statusArray.add(0, getString(R.string.select_status));
        statusArray.add(1, getString(R.string.select_status_delivered));
        statusArray.add(2, getString(R.string.select_status_not_at_home));
        statusArray.add(3, getString(R.string.select_status_denied)); // is denied from the customer
        statusArray.add(4, getString(R.string.select_status_cancelled)); // is cancelled from the driver

        statuskeyArray =  new ArrayList<String>();
        statuskeyArray.add(0,"0");
        statuskeyArray.add(1,"5");
        statuskeyArray.add(2,"6");
        statuskeyArray.add(3,"7");
        statuskeyArray.add(4,"8");

        SpinnerAdapter status_adapter = new SpinnerAdapter(
                DeliverActivity.this,
                R.layout.spinner_adapter,
                statusArray
        );
        sStatus.setAdapter(status_adapter);


        // 5 = Delivered
        // 6 = Not at home
        // 7 = Denied
        // 8 = Cancelled
        if (packageStatusID.equals("5") || packageStatusID.equals("6") || packageStatusID.equals("7") || packageStatusID.equals("8")){
            imageViewSubmit1.setVisibility(View.GONE);
            editText.setVisibility(View.GONE);
        }else{
            if (blCameraScanner)
            {
                blUseCamera = true;
                imageViewSubmit1.setVisibility(View.VISIBLE);
                editText.setHint(getString(R.string.select_action_text3));
            } else {
                imageViewSubmit1.setVisibility(View.GONE);
               // editText.setHint(getString(R.string.select_action_text4));
                editText.setHint(getString(R.string.select_action_text3));
            }
        }

        if(blQuickScan){
            imageViewSubmit1.setVisibility(View.GONE);
            editText.setVisibility(View.GONE);

            spinnerStatusLayout.setVisibility(View.VISIBLE);
            sStatus.setVisibility(View.VISIBLE);
        }

        imageViewSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(DeliverActivity.this, BarcodeCaptureActivity.class);
                startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);

                // create barcode intent
                /*IntentIntegrator integrator = new IntentIntegrator(SelectActionActivity.this);
                integrator.initiateScan();*/

            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    EditText editText = (EditText) findViewById(R.id.editText_barcode);
                    String bc = editText.getText().toString();

                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    phoneCustomStateListener = new PhoneCustomStateListener();
                    telephony = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                    telephony.listen(phoneCustomStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                    int signal = phoneCustomStateListener.signalSupport;

                    if(bc.length() > 0) {
                        if (bc.length() == Integer.valueOf(packageLength)) {
                            if (bc.equals(packageBarcode)) {
                                spinnerStatusLayout.setVisibility(View.VISIBLE);
                                sStatus.setVisibility(View.VISIBLE);

                                // Check if no view has focus:
                                View view = getCurrentFocus();
                                if (view != null) {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                }

                                try {
                                    showcaseView.hide();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(DeliverActivity.this);
                                if (!prefs.getBoolean("14", false)) {
                                    // <---- run one time code here
                                    showcaseViewSpinner = new ShowcaseView.Builder(DeliverActivity.this)
                                            .setTarget(new ViewTarget(R.id.spinnerStatus, DeliverActivity.this))
                                            .setContentTitle(getString(R.string.title_status_package))
                                            .setContentText(getString(R.string.text_status_package))
                                            .setShowcaseDrawer(new CustomShowcaseViewEditTextTablet(getResources()))
                                            .singleShot(14)
                                            .build();

                                    // mark first time has runned.
                                    SharedPreferences.Editor editor14 = prefs.edit();
                                    editor14.putBoolean("14", true);
                                    editor14.commit();
                                }


                                processing_scan = false;
                            } else {
                                spinnerStatusLayout.setVisibility(View.GONE);
                                sStatus.setVisibility(View.GONE);

                                Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_wrong_barcode), Toast.LENGTH_SHORT).show();

                                processing_scan = true;

                                // always clear the textbox
                                editText = (EditText) findViewById(R.id.editText_barcode);
                                editText.setText(null);
                            }
                        }
                    }

                    return true;
                }
                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                EditText editText = (EditText) findViewById(R.id.editText_barcode);
                String bc = editText.getText().toString();

                // strip control chars
                bc = bc.replace("*", "");
                bc = bc.replace("\n", "");
                bc = bc.replace("\r", "");

                phoneCustomStateListener = new PhoneCustomStateListener();
                telephony = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                telephony.listen(phoneCustomStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                int signal = phoneCustomStateListener.signalSupport;

                if (bc.length() > 0) {
                    if (bc.length() == Integer.valueOf(packageLength)) {
                        if (bc.equals(packageBarcode)) {
                            spinnerStatusLayout.setVisibility(View.VISIBLE);
                            sStatus.setVisibility(View.VISIBLE);

                            // Check if no view has focus:
                            View view = getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }

                            try {
                                showcaseView.hide();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(DeliverActivity.this);
                            if (!prefs.getBoolean("15", false)) {
                                // <---- run one time code here
                                showcaseViewSpinner = new ShowcaseView.Builder(DeliverActivity.this)
                                        .setTarget(new ViewTarget(R.id.spinnerStatus, DeliverActivity.this))
                                        .setContentTitle(getString(R.string.title_status_package))
                                        .setContentText(getString(R.string.text_status_package))
                                        .setShowcaseDrawer(new CustomShowcaseViewEditTextTablet(getResources()))
                                        .singleShot(15)
                                        .build();

                                // mark first time has runned.
                                SharedPreferences.Editor editor15 = prefs.edit();
                                editor15.putBoolean("15", true);
                                editor15.commit();
                            }

                            processing_scan = false;
                        } else {
                            spinnerStatusLayout.setVisibility(View.GONE);
                            sStatus.setVisibility(View.GONE);

                            Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_wrong_barcode), Toast.LENGTH_SHORT).show();

                            processing_scan = true;

                            // always clear the textbox
                            //editText = (EditText) findViewById(R.id.editText_barcode);
                            //editText.setText(null);
                        }
                    }
                }
            }


            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        /*btnDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 5 = Delivered
                new UpdatePackageStatus(PharmacyId, idDelivery, "5").execute();
                new UpdateRouteReport(PharmacyId, idRoute, packageAddressId).execute();
            }
        });

        btnNotHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 6 = Not at home
                new UpdatePackageStatus(PharmacyId, idDelivery, "6").execute();
                new UpdateRouteReport(PharmacyId, idRoute, packageAddressId).execute();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 8 = Cancelled
                new UpdatePackageStatus(PharmacyId, idDelivery, "8").execute();
                new UpdateRouteReport(PharmacyId, idRoute, packageAddressId).execute();
            }
        });*/

        sStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String mIndex = sStatus.getSelectedItem().toString();

                if(mIndex.contains(getString(R.string.select_status))){
                    // do nothing

                }else{

                    int myIndex = sStatus.getSelectedItemPosition();
                    final String statusid = statuskeyArray.get(myIndex);

                    // if deliver selected status = cancel
                    if(statusid.equals("8")){
                        new AlertDialog.Builder(DeliverActivity.this)
                                .setTitle(getString(R.string.alert_dialog_title_package))
                                .setMessage(getString(R.string.alert_dialog_text_cancel_package))
                                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        new UpdatePackageStatus(PharmacyId, idDelivery, String.valueOf(statusid)).execute();
                                        new UpdateRouteReport(PharmacyId, idRoute, packageAddressId).execute();

                                    }
                                })
                                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }else{

                        try{
                            showcaseViewSpinner.hide();
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                        new UpdatePackageStatus(PharmacyId, idDelivery, String.valueOf(statusid)).execute();
                        new UpdateRouteReport(PharmacyId, idRoute, packageAddressId).execute();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnSave.setVisibility(View.GONE);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                try {
                    // get bitmap from canvas
                    myBitmap = dv.getBitmap();

                    if (myBitmap != null)
                    {
                        if(isNetworkAvailable()){
                            new RegisterImageTask().execute();
                        }else{
                            Toast.makeText(DeliverActivity.this, getResources().getString(R.string.toast_text_no_internet), Toast.LENGTH_SHORT).show();
                        }

                    }


                }catch (Exception e){
                    e.printStackTrace();
                }


            }

        });


        btnClear.setVisibility(View.GONE);
        btnClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {

                dv.clear();
                /*Intent intent = getIntent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                startActivity(intent);*/

            }

        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if(!blQuickScan){
            new LoadAddress(PharmacyId).execute();
        }else{
            textViewCustomerAddress.setText(packageAddress);
            dv.patient_text = packageAddress;
        }



        // load signature
        // 5 = Delivered
        // 6 = Not at home
        // 7 = Denied
        if (packageStatusID.equals("5") || packageStatusID.equals("6") || packageStatusID.equals("7")) {
            new LoadImage().execute();
        }else{
            if(!blQuickScan){
                new LoadPackageOfRoute(idRoute, PharmacyId).execute();
            }else{
                imageViewMarker.setImageResource(R.drawable.marker_gray_big);
                imageViewCheck.setImageResource(R.drawable.checkmark_gray);
            }
        }

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(tabletSize) {
            // tablet
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("16", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(DeliverActivity.this)
                        .setTarget(new ViewTarget(R.id.editText_barcode, DeliverActivity.this))
                        .setContentTitle(getString(R.string.title_deliver_package))
                        .setContentText(getString(R.string.text_deliver_package))
                        .setShowcaseDrawer(new CustomShowcaseViewEditTextTablet(getResources()))
                        .singleShot(16)
                        .build();

                // mark first time has runned.
                SharedPreferences.Editor editor16 = prefs.edit();
                editor16.putBoolean("16", true);
                editor16.commit();
            }

        }else{
            // phone
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("17", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(DeliverActivity.this)
                        .setTarget(new ViewTarget(R.id.editText_barcode, DeliverActivity.this))
                        .setContentTitle(getString(R.string.title_deliver_package))
                        .setContentText(getString(R.string.text_deliver_package))
                        .setShowcaseDrawer(new CustomShowcaseViewEditTextPhone(getResources()))
                        .singleShot(17)
                        .build();

                // mark first time has runned.
                SharedPreferences.Editor editor17 = prefs.edit();
                editor17.putBoolean("17", true);
                editor17.commit();
            }
        }



    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String UPCScanned = barcode.displayValue.toString();
                    EditText editText = (EditText) findViewById(R.id.editText_barcode);
                    editText.setText(UPCScanned);

                    String bc = editText.getText().toString();
                    // strip control chars
                    bc = bc.replace("*", "");
                    bc = bc.replace("\n", "");
                    bc = bc.replace("\r", "");

                    if(bc.length() > 0){
                        if (bc.equals(packageBarcode)) {
                            spinnerStatusLayout.setVisibility(View.VISIBLE);
                            sStatus.setVisibility(View.VISIBLE);

                            // Check if no view has focus:
                            View view = getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }

                            try{
                                showcaseView.hide();
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                            if (!prefs.getBoolean("18", false)) {
                                // <---- run one time code here
                                showcaseViewSpinner = new ShowcaseView.Builder(DeliverActivity.this)
                                        .setTarget(new ViewTarget(R.id.spinnerStatus, DeliverActivity.this))
                                        .setContentTitle(getString(R.string.title_status_package))
                                        .setContentText(getString(R.string.text_status_package))
                                        .setShowcaseDrawer(new CustomShowcaseViewEditTextTablet(getResources()))
                                        .singleShot(18)
                                        .build();

                                // mark first time has runned.
                                SharedPreferences.Editor editor18 = prefs.edit();
                                editor18.putBoolean("18", true);
                                editor18.commit();
                            }

                            processing_scan = false;
                        } else {
                            spinnerStatusLayout.setVisibility(View.GONE);
                            sStatus.setVisibility(View.GONE);

                            Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_wrong_barcode), Toast.LENGTH_SHORT).show();

                            processing_scan = true;

                            // always clear the textbox
                            editText = (EditText) findViewById(R.id.editText_barcode);
                            editText.setText(null);


                        }
                    }
                } else
                    Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_no_barcode_captured), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(DeliverActivity.this, getString(R.string.textView_text_error) + " " + CommonStatusCodes.getStatusCodeString(resultCode), Toast.LENGTH_SHORT).show();
        } else
            super.onActivityResult(requestCode, resultCode, data);


        /*IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);

                String bc = EditText.getText().toString();
                // strip control chars
                bc = bc.replace("*", "");
                bc = bc.replace("\n", "");
                bc = bc.replace("\r", "");

                if(bc.length() > 0){
                    if (bc.equals(packageBarcode)) {
                        spinnerStatusLayout.setVisibility(View.VISIBLE);
                        sStatus.setVisibility(View.VISIBLE);

                        // Check if no view has focus:
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }

                        showcaseView.hide();
                        showcaseViewSpinner = new ShowcaseView.Builder(DeliverActivity.this)
                                .setTarget(new ViewTarget(R.id.spinnerStatus, DeliverActivity.this))
                                .setContentTitle("Status package")
                                .setContentText("Let's say the customer receive the package." + "\n" +
                                        "Select the status 'Delivered' for the package." + "\n" +
                                        "Then let the customer to sign for confirmation and save.")
                                .setShowcaseDrawer(new CustomShowcaseViewEditTextTablet(getResources()))
                                .build();

                        processing_scan = false;
                    } else {
                        spinnerStatusLayout.setVisibility(View.GONE);
                        sStatus.setVisibility(View.GONE);

                        Toast.makeText(DeliverActivity.this, "Barcode not found!", Toast.LENGTH_SHORT).show();

                        processing_scan = true;

                        // always clear the textbox
                        EditText = (EditText) findViewById(R.id.editText_barcode);
                        EditText.setText(null);


                    }
                }

            }

        }*/
    }

    /**
     * Background Async Task to Load Package Item by making HTTP Request
     * */
    class LoadAddress extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadAddress(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cAddress address = MyProvider.getAddress(packageAddressId, pharmacyId);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    List<String> slist = new ArrayList<String> ();
                    slist.add(address.customer_street);
                    slist.add(address.customer_house);
                    slist.add(address.customer_zipcode);
                    slist.add(address.customer_city);
                    slist.add(address.customer_country);

                    StringBuilder rString = new StringBuilder();
                    int count = 0;
                    for (String line : slist) {
                        ++count;
                        if (!line.equals("")) {
                            if(count != slist.size()){
                                rString.append(line + ", ");
                            }else{
                                rString.append(line);
                            }
                        }
                    }

                    textViewCustomerAddress.setText(rString.toString());
                    dv.patient_text = rString.toString();
                    //if((address.customer_surname.equals("") && address.customer_lastname.equals(" "))){
                    if((address.customer_lastname.equals(" "))){
                        signature_person.setHint(getString(R.string.textView_hint_pacient));
                        //textViewPackageSignatureName.setText(getResources().getString(R.string.person_name));
                        textViewPackageSignatureName.setVisibility(View.GONE);
                        textViewPackageSignatureNameLabel.setVisibility(View.GONE);
                    }else{
                        signature_person.setText(address.customer_surname + " " + address.customer_lastname);
                        textViewPackageSignatureName.setText(address.customer_surname + " " + address.customer_lastname);
                    }


                }
            });

            List<cPackagesbyDeliveryId> myPackage = MyProvider.getPackagesbyDeliveryId(idDelivery, pharmacyId);
            for(final cPackagesbyDeliveryId entry : myPackage){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String pStatus = "";
                        if (entry != null)
                        {
                            pStatus = entry.status;
                        }

                        // 5 = Delivered
                        if (pStatus.equals("5")) {
                            textViewPackageDeliverRemark.setVisibility(View.VISIBLE);
                            textViewPackageNotifyPatient.setVisibility(View.VISIBLE);
                            textViewPackagedeliverRemarkLabel.setVisibility(View.VISIBLE);
                            textViewPackageNotifyPatientLabel.setVisibility(View.VISIBLE);

                            if(!entry.signature_person.equals("")){
                                textViewPackageSignatureName.setVisibility(View.VISIBLE);
                                textViewPackageSignatureNameLabel.setVisibility(View.VISIBLE);
                                textViewPackageSignatureName.setText(entry.signature_person);
                            }else{
                                textViewPackageSignatureName.setVisibility(View.GONE);
                                textViewPackageSignatureNameLabel.setVisibility(View.GONE);
                                //textViewPackageSignatureName.setText(getResources().getString(R.string.person_name));
                            }

                            if (!entry.deliver_remark.equals("")) {
                                textViewPackageDeliverRemark.setText(entry.deliver_remark);
                            } else {
                                textViewPackageDeliverRemark.setText(getString(R.string.textView_text_no));
                            }

                            if (entry.contact_patient.equals("0")) {
                                textViewPackageNotifyPatient.setText(getString(R.string.textView_text_no));
                            } else if (entry.contact_patient.equals("1")) {
                                textViewPackageNotifyPatient.setText(getString(R.string.textView_text_yes));
                            }
                        } else {
                            textViewPackageDeliverRemark.setVisibility(View.GONE);
                            textViewPackageNotifyPatient.setVisibility(View.GONE);
                            textViewPackagedeliverRemarkLabel.setVisibility(View.GONE);
                            textViewPackageNotifyPatientLabel.setVisibility(View.GONE);
                        }
                    }
                });
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadPackageOfRoute extends AsyncTask<String, String, String> {

        public String routeid, pharmacyId;

        public LoadPackageOfRoute(String routeid, String pharmacyId){
            this.routeid = routeid;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            packageRouteList = new ArrayList<HashMap<String, String>>();

            List<cPackageRoute> myPackage = MyProvider.getPackagesOfRoute(routeid, packageAddressId, pharmacyId);
            for(cPackageRoute entry : myPackage)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                //map.put("id",  entry.id);
                map.put("scriptid/status_description", entry.scriptid+"/"+entry.package_status);
                //map.put("scriptid",  entry.scriptid);
                //map.put("packageid",  entry.package_barcode);
                //map.put("addressid", entry.addressid);
                //map.put("package_status",  entry.package_status);
                //map.put("status_description",  entry.status_description);

                // adding HashList to ArrayList
                packageRouteList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                public void run() {

                    imageViewMarker.setImageResource(R.drawable.marker_gray_big);
                    imageViewCheck.setImageResource(R.drawable.checkmark_gray);

                    int totalScanned = 0;
                    int totalUnScanned = 0;
                    String myRow = "";
                    for (HashMap<String, String> map : packageRouteList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            String row = String.valueOf(entry.getValue().toString());
                            String[] parts = row.split("/");
                            String scriptid = parts[0];
                            String status_id = parts[1];

                            if(scriptid.equals(idScript)){
                                // 5 = Delivered
                                // 6 = Not at home
                                // 7 = Denied by customer
                                if(!status_id.equals("5")
                                        && !status_id.equals("6")
                                        && !status_id.equals("7")){
                                    totalUnScanned++;
                                }else{
                                    totalScanned++;
                                }
                            }
                        }

                    final int totalScanned1 = totalScanned;

                    // if are all packages scanned , customer can have signature
                    if(totalUnScanned == 0) {
                        if(totalScanned > 1){

                            dv.label_text = getString(R.string.textView_signature_receiving_script) + " " + idScript + " , " + totalScanned + " " + getString(R.string.textView_signature_packages);
                            dv.datetime_text = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            dv.setVisibility(View.VISIBLE);
                            layoutDrawing.setVisibility(View.VISIBLE);
                            layoutSignature.setVisibility(View.VISIBLE);
                            btnSave.setVisibility(View.VISIBLE);
                            btnClear.setVisibility(View.VISIBLE);
                            //spinnerStatusLayout.setVisibility(View.GONE);
                            //sStatus.setVisibility(View.GONE);

                            // hide keyboard
                            hideKeyboard(DeliverActivity.this);
                        }else{

                            dv.label_text = getString(R.string.textView_signature_receiving_script) + " " + idScript + " , " + totalScanned + " " + getString(R.string.textView_signature_package);
                            dv.datetime_text = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            dv.setVisibility(View.VISIBLE);
                            layoutDrawing.setVisibility(View.VISIBLE);
                            layoutSignature.setVisibility(View.VISIBLE);
                            btnSave.setVisibility(View.VISIBLE);
                            btnClear.setVisibility(View.VISIBLE);
                            //spinnerStatusLayout.setVisibility(View.GONE);
                            //sStatus.setVisibility(View.GONE);

                            // hide keyboard
                            hideKeyboard(DeliverActivity.this);
                        }


                    }
                    else{
                        if(blFirstTimeLoadActivity){
                            finish();
                        }
                        blFirstTimeLoadActivity = true;
                    }
                }
            });

            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdatePackageStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String idDelivery = "";
        String statusId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdatePackageStatus(String pharmacyId, String deliveryId, String statusId){
            this.pharmacyId = pharmacyId;
            this.idDelivery = deliveryId;
            this.statusId = statusId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updatePackageStatus(pharmacyId, idDelivery, statusId, latitude, longitude);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Package Status", "false");
                        //Toast.makeText(DeliverActivity.this, "Failed to set deliver status", Toast.LENGTH_SHORT).show();

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DeliverActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_failed_set_deliver_status) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Log.d("Update Package Status", "true");
                        Globals.setValue("update_status_package","update_status_package");
                        Globals.setValue("tutorial_update_status_package","tutorial_update_status_package");
                        new LoadPackageOfRoute(idRoute, pharmacyId).execute();
                        //finish();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdateRouteReport extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String routeId = "";
        String addressId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdateRouteReport(String pharmacyId, String routeId, String addressId){
            this.pharmacyId = pharmacyId;
            this.routeId = routeId;
            this.addressId = addressId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateRouteReport(pharmacyId, routeId, addressId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("update route report", "false");

                        /*String error_message = MyProvider.last_error;
                        if(error_message.equals("1")){
                            Toast.makeText(DeliverActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DeliverActivity.this, "Didn't update route report: " + error_message, Toast.LENGTH_SHORT).show();
                        }*/
                    }
                    else
                    {
                        Log.d("update route report", "true");
                    }
                }
            });
            hideProgressDialog();
        }
    }

    private class UpdateSignature extends AsyncTask<String, String, Boolean>{

        String pharmacyId = "";
        String deliveryId = "";
        String status_signature = "";
        String signature_person = "";
        String deliver_remark = "";
        String status_notify_patient = "";

        public UpdateSignature(String pharmacyId, String deliveryId, String status_signature, String signature_person, String deliver_remark , String status_notify_patient ){
            this.pharmacyId = pharmacyId;
            this.deliveryId = deliveryId;
            this.status_signature = status_signature;
            this.signature_person = signature_person;
            this.deliver_remark = deliver_remark;
            this.status_notify_patient = status_notify_patient;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(DeliverActivity.this);
            pDialog1.setTitle(getString(R.string.alert_dialog_title_save_signature));
            pDialog1.setMessage(getString(R.string.alert_dialog_text_wait));
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.setCanceledOnTouchOutside(false);
            pDialog1.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateSignature(pharmacyId, deliveryId, status_signature, signature_person, deliver_remark, status_notify_patient);

            return suc;

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            pDialog1.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if(success){
                        Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_saved_signature), Toast.LENGTH_SHORT).show();
                        finish();
                    }else{

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DeliverActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_not_saved_signature) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    /** AsyncTask register image  */
    private class RegisterImageTask extends AsyncTask<String, String, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(DeliverActivity.this);
            pDialog1.setTitle(getString(R.string.alert_dialog_title_save_image));
            pDialog1.setMessage(getString(R.string.alert_dialog_text_wait));
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.setCanceledOnTouchOutside(false);
            pDialog1.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {

            int BitmapID = 0;
            boolean suc = false;

            if (myBitmap != null)
            {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
              //  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArray = stream.toByteArray();

                byte[] bitmapByteArrayThumbnail;
                bitmapByteArrayThumbnail = bitmapByteArray;
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
             //   Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArrayThumbnail = stream.toByteArray();

                if(isNetworkAvailable()){
                    BitmapID = MyProvider.createPackageImage(pharmacy_id, idPackage, bitmapByteArray, bitmapByteArrayThumbnail);
                    if (BitmapID > 0)
                        suc = true;
                    else
                        suc = false;
                }else{
                    Toast.makeText(DeliverActivity.this, getResources().getString(R.string.toast_text_no_internet), Toast.LENGTH_SHORT).show();
                }


            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            pDialog1.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DeliverActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_not_saved_signature) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_saved_signature), Toast.LENGTH_SHORT).show();

                        if(notify_patient.isChecked()){
                            new UpdateSignature(pharmacy_id, idDelivery, "1", signature_person.getText().toString(), remark.getText().toString(), "1").execute();
                        }else{
                            new UpdateSignature(pharmacy_id, idDelivery, "1", signature_person.getText().toString(), remark.getText().toString(), "0").execute();
                        }
                    }

                }
            });
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Background Async Task to Load image by making HTTP Request
     * */
    private class LoadImage extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(DeliverActivity.this);
            pDialog1.setTitle(getString(R.string.alert_dialog_title_load_signature));
            pDialog1.setMessage(getString(R.string.alert_dialog_text_wait));
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.setCanceledOnTouchOutside(false);
            pDialog1.show();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            String ws_type = Globals.getValue("warehouse_type");

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
            packageImage = new cPackageImage();

            packageImage = MyProvider.getPackageImage(pharmacy_id, idPackage);

            URL_ART = MyProvider.getWebServiceUrl() + URL_ART;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!packageImage.image.equals("")) {
                       // imageView = (ImageView) findViewById(R.id.imageView);

                        //dv.label_text = "";
                        //dv.datetime_text = "";
                        //dv.setVisibility(View.VISIBLE);
                        layoutDrawing.setVisibility(View.VISIBLE);
                        ImageView img = (ImageView) findViewById(R.id.packageSignatureImage);
                        img.setVisibility(View.VISIBLE);

                        imageViewMarker.setImageResource(R.drawable.marker_green_big);
                        imageViewCheck.setImageResource(R.drawable.checkmark_green);

                        new DownloadImageTask(img).execute(URL_ART + "/" + packageImage.image);

                    }else{
                        new LoadPackageOfRoute(idRoute, pharmacy_id).execute();
                    }
                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

            // dismiss the dialog after getting image
            pDialog1.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    //if (!packageImage.image.equals("")) dv.resetImage();

                }
            });

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            Log.d("digiroute_logs","downloading image from url = " + urldisplay);

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                // remember image
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                myImageData = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

            } catch (Exception e) {
                //    Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if (result != null) bmImage.setImageBitmap(result);
        }
    }

    /*
    Get Location - latitude and longitude
     */
    private void getLocation() {
        boolean isGPS = false;
        boolean isNetwork = false;
        boolean isOtherNetwork = false;
        boolean gpsCouldNotGetLocation = false;
        boolean networkCouldNotGetLocation = false;

        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        isOtherNetwork = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        //isGPS = false;


        boolean isWifiConn = false;
        boolean isMobileConn = false;

        ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        try
        {
            if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected())
                {
                    NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    isWifiConn = networkInfo.isConnected();

                    networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                    isMobileConn = networkInfo.isConnected();

                    Log.d(DEBUG_TAG, "Wifi connected: " + isWifiConn);
                    Log.d(DEBUG_TAG, "Mobile connected: " + isMobileConn);
                }
            else
                {
                    Log.d(DEBUG_TAG, "Wifi connected: " + isWifiConn);
                    Log.d(DEBUG_TAG, "Mobile connected: " + isMobileConn);
                }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        NetworkInfo[] nwInfos = connMgr.getAllNetworkInfo();
        for (NetworkInfo nwInfo : nwInfos) {
            Log.d(TAG, "Network Type Name: " + nwInfo.getTypeName());
            Log.d(TAG, "Network available: " + nwInfo.isAvailable());
            Log.d(TAG, "Network c_or-c: " + nwInfo.isConnectedOrConnecting());
            Log.d(TAG, "Network connected: " + nwInfo.isConnected());
        }

        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS || isNetwork) {
                    // from GPS
                    if(isGPS){
                        Log.d(TAG, "GPS on");
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (loc != null)
                                System.out.println(loc);

                            try{
                                latitude = loc.getLatitude();
                                longitude = loc.getLongitude();
                                //new RegisterTaskLocation(latitude, longitude).execute();
                            }catch (Exception e){
                                System.out.println(e.toString());
                                gpsCouldNotGetLocation = true;
                            }

                        }
                    } else {

                        if (isNetwork) {
                            // from Network Provider
                            Log.d(TAG, "NETWORK_PROVIDER on");
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                            if (locationManager != null) {
                                loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null)
                                    System.out.println(loc);

                                try {
                                    latitude = loc.getLatitude();
                                    longitude = loc.getLongitude();
                                    //new RegisterTaskLocation(latitude, longitude).execute();
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                }
                            }
                        }
                    }

                    //////////////////////////////////////////////////////////////////
                    // if gps location didn't get location, then try with network
                    if(gpsCouldNotGetLocation){
                        // from Network Provider
                        Log.d(TAG, "NETWORK_PROVIDER on");
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (loc != null)
                                System.out.println(loc);

                            try {
                                latitude = loc.getLatitude();
                                longitude = loc.getLongitude();
                                //new RegisterTaskLocation(latitude, longitude).execute();
                            } catch (Exception e) {
                                System.out.println(e.toString());
                                networkCouldNotGetLocation = true;
                            }
                        }
                    }

                }else{
                    // ask again for permission, if the user stopped location or/and network from phone
                    // and try to aproximate location until the user activate back the location or/and network

                    Toast.makeText(DeliverActivity.this, getString(R.string.toast_text_turn_on_gps_network), Toast.LENGTH_SHORT).show();

                    try{
                        if(isWifiConn || isMobileConn){
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                            if (locationManager != null) {
                                loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null)
                                    System.out.println(loc);

                                try {
                                    latitude = loc.getLatitude();
                                    longitude = loc.getLongitude();
                                    //new RegisterTaskLocation(latitude, longitude).execute();
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                    networkCouldNotGetLocation = true;
                                }
                            }

                        }else if(isOtherNetwork){
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                            if (locationManager != null) {
                                loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null)
                                    System.out.println(loc);

                                try {
                                    latitude = loc.getLatitude();
                                    longitude = loc.getLongitude();
                                    //new RegisterTaskLocation(latitude, longitude).execute();
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                    networkCouldNotGetLocation = true;
                                }
                            }
                        }

                        //latitude = loc.getLatitude();
                        //longitude = loc.getLongitude();
                        //new RegisterTaskLocation(latitude, longitude).execute();
                    }catch (Exception e){
                        System.out.println(e.toString());
                    }
                }
                System.out.println(loc);

            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        String x = "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude();
        System.out.println(x);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {Log.d("Latitude","status");}



    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }


}

