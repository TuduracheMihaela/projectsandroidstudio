package com.sharethatdata.digiroute.Profile;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by mihu__000 on 4/19/2017.
 */

public interface Item {
    public int getViewType();
    public View getView(LayoutInflater inflater, View convertView);
}
