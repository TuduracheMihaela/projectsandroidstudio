package com.sharethatdata.digiroute.Profile;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

/**
 * Created by mihu__000 on 4/19/2017.
 */

public class Header implements Item {
    private final String name;

    public Header(String name) {
        this.name = name;
    }

    @Override
    public int getViewType() {
        return TwoTextArrayAdapter.RowType.HEADER_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = (View) inflater.inflate(R.layout.activity_profile_header, null);
            // Do some initialization
        } else {
            view = convertView;
        }

        TextView text = (TextView) view.findViewById(R.id.separator);
        text.setText(name);

        return view;
    }

}