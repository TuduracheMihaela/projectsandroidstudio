package com.sharethatdata.digiroute.Profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sharethatdata.digiroute.R;

/**
 * Created by mihu__000 on 4/19/2017.
 */

public class ListItem implements Item {
    private final String str1;
    private final String str2;
    int imgResID;

    Context context;

    public ListItem(String text1, String text2, String imgResID, Context context) {
        this.str1 = text1;
        this.str2 = text2;
        this.imgResID = context.getResources().getIdentifier( imgResID, "drawable", context.getPackageName());
        this.context = context;
    }

    @Override
    public int getViewType() {
        return TwoTextArrayAdapter.RowType.LIST_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = (View) inflater.inflate(R.layout.activity_profile_list_item, null);
            // Do some initialization
        } else {
            view = convertView;
        }

        TextView text1 = (TextView) view.findViewById(R.id.list_content1);
        TextView text2 = (TextView) view.findViewById(R.id.list_content2);
        ImageView imageView = (ImageView) view.findViewById(R.id.list_content3);
        text1.setText(str1);
        text2.setText(str2);
        if(str1.equals(context.getResources().getString(R.string.textView_reset_password))){
            imageView.setImageResource(imgResID);
        }else if(str1.equals(context.getResources().getString(R.string.textView_picture))){
            imageView.setImageResource(imgResID);
        }else{
            imageView.setVisibility(View.GONE);
        }

        return view;
    }

}
