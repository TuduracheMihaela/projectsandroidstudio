package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;

/**
 * Created by mihu__000 on 4/26/2017.
 */

public class CreateAddressActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String PharmacyId = "";

    ProgressDialog progressDialog;

    String script_id_text;
    String barcode_text;
    String first_name_text;
    String last_name_text;
    String gender_text;
    String birthdate_text;
    String phone_text;
    String address_id_text;
    String address_1_text;
    String address_2_text;
    String house_number_text;
    String zipcode_text;
    String city_text;
    String province_text;
    String country_text;
    String package_id_text;
    String nr_packages_text;
    String location_text;
    String collection_text;
    String free_text;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(CreateAddressActivity.this, CreateAddressActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        final EditText script_id = (EditText) findViewById(R.id.editText_script_id);
        final EditText barcode = (EditText) findViewById(R.id.editText_barcode);
        final EditText first_name = (EditText) findViewById(R.id.editText_first_name);
        final EditText last_name = (EditText) findViewById(R.id.editText_last_name);
        final EditText gender = (EditText) findViewById(R.id.editText_gender);
        final EditText birthdate = (EditText) findViewById(R.id.editText_birthdate);
        final EditText phone = (EditText) findViewById(R.id.editText_phone);
        final EditText address_id = (EditText) findViewById(R.id.editText_address_id);
        final EditText address_1 = (EditText) findViewById(R.id.editText_address_1);
        final EditText address_2 = (EditText) findViewById(R.id.editText_address_2);
        final EditText house_number = (EditText) findViewById(R.id.editText_house_number);
        final EditText zipcode = (EditText) findViewById(R.id.editText_zipcode);
        final EditText city = (EditText) findViewById(R.id.editText_city);
        final EditText province = (EditText) findViewById(R.id.editText_province);
        final EditText country = (EditText) findViewById(R.id.editText_country);
        final EditText package_id = (EditText) findViewById(R.id.editText_package_id);
        final EditText nr_packages = (EditText) findViewById(R.id.editText_packages);
        final EditText location = (EditText) findViewById(R.id.editText_location);
        final EditText collection = (EditText) findViewById(R.id.editText_collection);
        final EditText free = (EditText) findViewById(R.id.editText_free_text);

        String isAdministrator = Globals.getValue("administrator");
        if (isAdministrator == "yes") {
            address_id.setVisibility(View.VISIBLE);
        }

        Button addAddress = (Button) findViewById(R.id.btnAddAddress);
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean failFlag = false;
                if(script_id.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    script_id.setError(getString(R.string.required_script));
                }

                if( address_1.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    address_1.setError(getString(R.string.required_address));
                }

                /*if( city.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    city.setError("City is required!");
                }*/

                /*if(country.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    country.setError("Country is required!");
                }*/

                if(package_id.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    package_id.setError(getString(R.string.required_package));

                }

                if(nr_packages.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    nr_packages.setError(getString(R.string.required_package_nr));
                }

                if(location.getText().toString().trim().length() == 0)
                {
                    failFlag = true;
                    location.setError(getString(R.string.required_location));
                }

                if(failFlag == false){
                    script_id_text = script_id.getText().toString();
                    barcode_text = barcode.getText().toString();
                    first_name_text = first_name.getText().toString();
                    last_name_text = last_name.getText().toString();
                    gender_text = gender.getText().toString();
                    birthdate_text = birthdate.getText().toString();
                    phone_text = phone.getText().toString();
                    address_id_text = address_id.getText().toString();
                    address_1_text = address_1.getText().toString();
                    address_2_text = address_2.getText().toString();
                    house_number_text = house_number.getText().toString();
                    zipcode_text = zipcode.getText().toString();
                    city_text = city.getText().toString();
                    province_text = province.getText().toString();
                    country_text = country.getText().toString();
                    package_id_text = package_id.getText().toString();
                    nr_packages_text = nr_packages.getText().toString();
                    location_text = location.getText().toString();
                    collection_text = collection.getText().toString();
                    free_text = free.getText().toString();

                    alertDialog();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private void alertDialog(){
        new AlertDialog.Builder(CreateAddressActivity.this)
                .setTitle(getString(R.string.alert_dialog_title_address))
                .setMessage(getString(R.string.alert_dialog_text_add_address_as_package))
                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new CreateAddress().execute();
                    }
                })
                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /** AsyncTask create address  */
    private class CreateAddress extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.createAddress(script_id_text, barcode_text, first_name_text + " " + last_name_text, gender_text,
                    birthdate_text, phone_text, address_id_text, address_1_text, address_2_text, house_number_text,
                    zipcode_text, city_text, province_text, country_text,
                    package_id_text, nr_packages_text, location_text, collection_text, free_text, myUser, PharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(CreateAddressActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateAddressActivity.this, getString(R.string.toast_text_failed_create_address) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }


                        /*String error_message = MyProvider.last_error;
                        if(error_message.equals("Script already in delivery.")){
                            Toast.makeText(CreateAddressActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreateAddressActivity.this, "Address didn't create: " + error_message, Toast.LENGTH_SHORT).show();
                        }*/
                    }
                    else
                    {
                        Globals.setValue("create_address","create_address");
                        Toast.makeText(CreateAddressActivity.this, getString(R.string.toast_text_create_address), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }
}
