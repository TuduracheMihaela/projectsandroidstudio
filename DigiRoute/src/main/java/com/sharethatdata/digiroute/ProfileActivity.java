package com.sharethatdata.digiroute;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Profile.Header;
import com.sharethatdata.digiroute.Profile.Helper;
import com.sharethatdata.digiroute.Profile.Image.MyImageLoader;
import com.sharethatdata.digiroute.Profile.Item;
import com.sharethatdata.digiroute.Profile.ListItem;
import com.sharethatdata.digiroute.Profile.NonScrollListView;
import com.sharethatdata.digiroute.Profile.TwoTextArrayAdapter;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.logistics_webservice.datamodel.cPackageImage;
import com.sharethatdata.logistics_webservice.datamodel.cUserImage;
import com.sharethatdata.logistics_webservice.datamodel.cUserStats;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by mihu__000 on 4/19/2017.
 */

public class ProfileActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {

    // Progress Dialog
    private ProgressDialog pDialog;

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    private String userIdSelected = "";
    private String usernameSelected = "";
    private String myUser = "";
    private String myPass = "";
    private String PharmacyId = "";

    TimeUtils MyTimeUtils = null;

    ArrayList<HashMap<String, String>> userList; // getContacts
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userNameArray =  new ArrayList<String>();

    public MyImageLoader loadMyImg;
    TwoTextArrayAdapter adapter;

    cUserImage myImage;
    String image_string;

    private String URL_ART = "/art";
    private String myImageData = "";

    ListView listview;
    //NonScrollListView listview;
    ImageView userImage;
    ImageView imageViewNew;

    ///////////////////////////////
    private String mCurrentPhotoPathAbsolute;
    private String mCurrentPhotoPath;
    //private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_BROWSE_PHOTO = 2;
    private static final int PERMISSION_TAKE_PHOTO = 3;
    private Bitmap myBitmap;
    private byte[] bitmapByteArray;

    String full_description;

    boolean permission_camera = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        loadMyImg = new MyImageLoader(getApplicationContext());

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
        PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(ProfileActivity.this, ProfileActivity.this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listview = (ListView) findViewById(R.id.listview);
        //listview = (NonScrollListView) findViewById(R.id.listview);
        userImage = (ImageView) findViewById(R.id.imageViewPic);

        /*final ScrollView scrollview = ((ScrollView) findViewById(R.id.scrollview)); scrollview.post(new Runnable() {
            @Override public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });*/

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String item = ((TextView) itemClicked.findViewById(R.id.list_content1)).getText().toString();

                if (item.equals(getResources().getString(R.string.textView_reset_password))) {
                    // change password
                    alertResetPassword();
                } else if (item.equals(getResources().getString(R.string.textView_picture))) {
                    // change picture
                    alertChangePicture();
                } else if (item.equals(getResources().getString(R.string.textView_description))){
                    // show full description
                    alertShowFullDescription(full_description);
                }

            }

        });

        String isManager = Globals.getValue("manager");
        if (isManager == "yes")
        {
            new LoadUserListSpinner(PharmacyId).execute();
        }else{
            new LoadContact(myUser, PharmacyId).execute();
        }


    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadUserListSpinner extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadUserListSpinner(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            //addressList = new ArrayList<HashMap<String, String>>();
            userList = new ArrayList<HashMap<String, String>>();

            //List<cContact> myContact = MyProvider.getPharmacyUsers(pharmacyId);
            List<cContact> myContact = MyProvider.getMyUsers(pharmacyId);
            for(cContact entry : myContact)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("username", entry.username);
                map.put("name", entry.name);

                // adding HashList to ArrayList
                userList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (ProfileActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    String isManager = Globals.getValue("manager");
                    if (isManager == "yes")
                    {
                        List<String> userArray =  new ArrayList<String>();
                        userArray.add(0, getString(R.string.select_user));
                        userkeyArray.add(0,"");
                        userNameArray.add(0,"");

                        if(userList.size()>0){
                            //userArray.add(0, getString(R.string.select_user));
                            //userkeyArray.add(0, getString(R.string.select_user));
                        }else{
                            userArray.add(0, getString(R.string.select_no_user));
                            userkeyArray.add(0, getString(R.string.select_no_user));
                        }

                        for (HashMap<String, String> map : userList)
                            for (Map.Entry<String, String> entry : map.entrySet())
                            {
                                if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                                if (entry.getKey() == "username") userNameArray.add(entry.getValue());
                                if (entry.getKey() == "name") userArray.add(entry.getValue());
                            }

                        //ArrayAdapter<String> user_adapter = new ArrayAdapter<String>(MyRoutesActivity.this, android.R.layout.simple_spinner_item, userArray);
                        //user_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.VISIBLE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.VISIBLE);
                        //sUsers.setAdapter(user_adapter);

                        SpinnerAdapter user_adapter = new SpinnerAdapter(
                                ProfileActivity.this,
                                R.layout.spinner_adapter,
                                userArray
                        );
                        sUsers.setAdapter(user_adapter);

                        sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                                String mIndex = sUsers.getSelectedItem().toString();

                                if(mIndex.contains(getString(R.string.select_user))){
                                    // do nothing

                                }else{

                                    int myIndex = sUsers.getSelectedItemPosition();
                                    String userid = userkeyArray.get(myIndex);
                                    String username = userNameArray.get(myIndex);
                                    //mySelectedUser = userid;

                                    new LoadContact(username, PharmacyId).execute();

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // auto select
                        int selectUserIndex = userNameArray.indexOf(myUser.toLowerCase());
                        sUsers.setSelection(selectUserIndex);
                        //mySelectedUser = String.valueOf(selectUserIndex);

                    }else{
                        RelativeLayout spinnerLayout = (RelativeLayout) findViewById(R.id.userSpinnerLayout);
                        spinnerLayout.setVisibility(View.GONE);
                        Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                        sUsers.setVisibility(View.GONE);

                        //mySelectedUser = "";
                        //new LoadRouteListSpinner(pharmacyId).execute();
                    }

                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load a image by making HTTP Request
     * */
    class LoadContact extends AsyncTask<String, String, String> {
        String user;
        String pharmacy;
        String image_id;

        public LoadContact(String user, String pharmacy){
            this.user = user;
            this.pharmacy = pharmacy;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cContact contact = MyProvider.getContact(user, pharmacy);
            final cUserStats stats = MyProvider.getUserStatistics(pharmacy, user);

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    userIdSelected = contact.id;
                    usernameSelected = user;
                    image_id = contact.image_id;

                    full_description = contact.description;
                    String description;
                    if (contact.description.length() >= 30) {
                        description = contact.description.substring(0, 30) + "...";
                    } else {
                        description = contact.description;
                    }

                    TextView textViewFullName = (TextView) findViewById(R.id.fullname);
                    textViewFullName.setText(contact.surname + " " + contact.lastname);

                    TextView textViewDaysActive = (TextView) findViewById(R.id.daysActive);
                    textViewDaysActive.setText(stats.days_active);

                    TextView textViewPharmacy = (TextView) findViewById(R.id.pharmacy);
                    textViewPharmacy.setText(PharmacyId);

                    List<Item> items = new ArrayList<Item>();
                    items.add(new Header(getResources().getString(R.string.textView_profile)));
                    items.add(new ListItem(getResources().getString(R.string.textView_profile_username), contact.username, "", ProfileActivity.this));
                    items.add(new ListItem(getResources().getString(R.string.textView_fullname), contact.surname + " " + contact.lastname, "", ProfileActivity.this));
                    items.add(new ListItem(getResources().getString(R.string.textView_description), description, "", ProfileActivity.this));
                    items.add(new Header(getResources().getString(R.string.textView_settings)));
                    items.add(new ListItem(getResources().getString(R.string.textView_reset_password), "", "edit", ProfileActivity.this));
                    items.add(new ListItem(getResources().getString(R.string.textView_picture), "", "edit_image", ProfileActivity.this));
                    items.add(new Header(getString(R.string.statistics_header)));
                    items.add(new ListItem(getString(R.string.statistics_nr_packages_collected), String.valueOf(stats.packages_collected), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_packages_delivered), String.valueOf(stats.packages_delivered), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_packages_notathome), String.valueOf(stats.packages_notathome), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_routes), String.valueOf(stats.routes), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_addresses_visited), String.valueOf(stats.address_visited), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_addresses_navigated), String.valueOf(stats.address_navigated), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_nr_signatures), String.valueOf(stats.signatures), "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_total_time), String.valueOf(stats.total_time) + " h", "", ProfileActivity.this));
                    items.add(new ListItem(getString(R.string.statistics_total_distance), String.valueOf(stats.total_distance) + " km", "", ProfileActivity.this));
                    //items.add(new ListItem(getString(R.string.statistics_experience_level), String.valueOf(stats.lvl), "", ProfileActivity.this));
                    //items.add(new ListItem(getString(R.string.statistics_lvl_xp), String.valueOf(stats.lvl_xp), "", ProfileActivity.this));
                    //items.add(new ListItem(getString(R.string.statistics_total_xp), String.valueOf(stats.total_xp), "", ProfileActivity.this));


                    adapter = new TwoTextArrayAdapter(ProfileActivity.this, items);
                    listview.setAdapter(adapter);
                    boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                    if (tabletSize) {
                        // do something
                    } else {
                        Helper.getListViewSizeSmall(listview);
                        //Helper.setListViewHeightBasedOnChildren(listview);
                    }


                }
            });


            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            new LoadImage(image_id).execute();
        }
    }

    /**
     * Background Async Task to Load image from intr_package_image by making HTTP Request
     * */
    private class LoadImage extends AsyncTask<String, String, String> {
        String image_id;

        public LoadImage(String image_id){
            this.image_id = image_id;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            if(myUser.equals(usernameSelected)){
                myImage = new cUserImage();
                myImage = MyProvider.getUserImage(PharmacyId, userIdSelected);
                image_string = myImage.image;
            }else{
                image_string = MyProvider.getImageFull(image_id, PharmacyId);
            }


            URL_ART = MyProvider.getWebServiceUrl() + URL_ART;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!image_string.equals("")) {
                        ImageView imageView = (ImageView) findViewById(R.id.imageViewPic);

                   //     new DownloadImageTask(imageView).execute(URL_ART + "/" + myImage.image);

                        Bitmap d = ConvertByteArrayToBitmap(image_string);
                        if (d != null)
                        {
                            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                            imageView.setImageBitmap(scaled);

                        }

                       // loadMyImg.DisplayImage(myImage.image, imageView); // image from database

                    }else{
                        userImage.setImageResource(R.drawable.user);
                       // loadMyImg.DisplayImage(packageImage.image, userImage); // image from database
                    }
                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

            hideProgressDialog();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    //if (!packageImage.image.equals("")) dv.resetImage();

                }
            });

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            Log.d("digiroute_logs","downloading image from url = " + urldisplay);

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                // remember image
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                myImageData = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

            } catch (Exception e) {
                //    Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if (result != null) bmImage.setImageBitmap(result);
        }
    }

    /**
     * Background Async Task to Load a image from intr_image tabel by making HTTP Request
     * */
   // class LoadImage extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
     //   @Override
     //   protected void onPreExecute() {super.onPreExecute(); }

        /**
         * getting items from url
         * */
    //    protected String doInBackground(String... args) {

    //        Globals = ((MyGlobals) getApplicationContext());

    //        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            //cImage image = MyProvider.getImage(idImage);
            //String Base64image = image.image;

    //        final String image = MyProvider.getImageThumnail("", PharmacyId);

     //       System.out.println("Base64image " + image);

    //        runOnUiThread(new Runnable() {
    //            @Override
    //            public void run() {
    //                ImageView userImage = (ImageView)findViewById(R.id.pic);
    //                    loadMyImg.DisplayImage(image, userImage); // image from database
    //            }
     //       });


     //       return "";
     //   }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
    //    protected void onPostExecute(String result) {super.onPostExecute(result); }
    //}


    /** AsyncTask register image  */
    private class RegisterImageTask extends AsyncTask<String, String, Boolean>{
        int imageID = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            boolean suc = false;

            if (myBitmap != null)
            {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                //  Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArray = stream.toByteArray();

                byte[] bitmapByteArrayThumbnail;
                bitmapByteArrayThumbnail = bitmapByteArray;
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                //   Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArrayThumbnail = stream.toByteArray();

                imageID = MyProvider.createUserImage(PharmacyId, userIdSelected, bitmapByteArray, bitmapByteArrayThumbnail);
                if(imageID > 0){
                    suc = MyProvider.updateImageUser(PharmacyId, userIdSelected, String.valueOf(imageID));
                }
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            hideProgressDialog();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(ProfileActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ProfileActivity.this, getString(R.string.toast_text_image_was_not_saved) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(ProfileActivity.this, getResources().getString(R.string.toast_text_image_was_saved), Toast.LENGTH_SHORT).show();
                        new LoadImage(String.valueOf(imageID)).execute();
                    }

                }
            });
        }
    }

    private void alertResetPassword() {

        // get prompt_password_reset view
        LayoutInflater li = LayoutInflater.from(ProfileActivity.this);
        View promptView = li.inflate(R.layout.prompt_password_reset, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptView);

        final EditText inputUsername = (EditText) promptView.findViewById(R.id.editTextUsername);
        final EditText inputEmail = (EditText) promptView.findViewById(R.id.editTextEmail);

        //inputUsername.setText(myUser);
        //inputEmail.setText("mtudurache@sharethatdata.com");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.alert_dialog_text_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //Do nothing here because we override this button later to change the close behaviour.
                                //However, we still need this because on older versions of Android unless we
                                //pass a handler the button doesn't get instantiated

                            }
                        })
                .setNegativeButton(getResources().getString(R.string.alert_dialog_text_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        //Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alertDialog.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.

                boolean failFlag = false;
                if(inputUsername.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    inputUsername.setError(getResources().getString(R.string.textView_username_required));
                }

                if(inputEmail.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    inputEmail.setError(getResources().getString(R.string.textView_email_required));
                }

                if(failFlag == false){

                    boolean security_code = MyProvider.getSecurityCode(inputUsername.getText().toString(), inputEmail.getText().toString());

                    if(security_code){
                        Toast.makeText(ProfileActivity.this, getResources().getString(R.string.toast_text_receive_security_code), Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                        alertConfirmPassword(inputUsername.getText().toString(), inputEmail.getText().toString());
                    }else{
                        Toast.makeText(ProfileActivity.this, MyProvider.last_error, Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

    private void alertConfirmPassword(String username, String email) {

        final String final_username = username;
        final String final_email = email;

        // get prompt_password_confirm view
        LayoutInflater li = LayoutInflater.from(ProfileActivity.this);
        View promptView = li.inflate(R.layout.prompt_password_confirm, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptView);

        final EditText inputPass1 = (EditText) promptView.findViewById(R.id.editTextPassword1);
        final EditText inputPass2 = (EditText) promptView.findViewById(R.id.editTextPassword2);
        final EditText inputCode = (EditText) promptView.findViewById(R.id.editTextSecurityCode);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.alert_dialog_text_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                 //Do nothing here because we override this button later to change the close behaviour.
                                //However, we still need this because on older versions of Android unless we
                                // pass a handler the button doesn't get instantiated

                            }
                        })
                .setNegativeButton(getResources().getString(R.string.alert_dialog_text_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        //Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alertDialog.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.

                boolean failFlag = false;
                if(inputPass1.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    inputPass1.setError(getResources().getString(R.string.textView_password_required));
                }

                if(inputPass2.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    inputPass2.setError(getResources().getString(R.string.textView_password_required));
                }
                if(inputCode.getText().toString().trim().length() == 0 )
                {
                    failFlag = true;
                    inputCode.setError(getResources().getString(R.string.textView_security_code_required));
                }

                if(failFlag == false){

                    if(inputPass1.getText().toString().equals(inputPass2.getText().toString())){
                        boolean security_code = MyProvider.getApproveNewPassword(final_username, final_email, inputPass1.getText().toString(), inputCode.getText().toString());

                        if(security_code){

                            SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
                            String password = sharedPrefs.getString("prefUserPassword", "");

                            SharedPreferences.Editor editor1 = sharedPrefs.edit();
                            editor1.putString("prefUserPassword",inputPass1.getText().toString());
                            editor1.commit();

                            alertDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.toast_text_password_successfully_reset), Toast.LENGTH_LONG).show();
                        }else{
                            String error_message = MyProvider.last_error;
                            Toast.makeText(ProfileActivity.this, error_message, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

    }

    private void alertChangePicture(){

        // get prompt_picture.xml view
        LayoutInflater li = LayoutInflater.from(ProfileActivity.this);
        View promptView = li.inflate(R.layout.prompt_picture, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptView);

        final Button btnCreateImage = (Button) promptView.findViewById(R.id.pictureFromPhone);
        final Button btnSearchImage = (Button) promptView.findViewById(R.id.pictureCamera);
        imageViewNew = (ImageView) promptView.findViewById(R.id.imageView1);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.alert_dialog_text_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // update or create database with new image profile user
                                if (!image_string.equals("")) {
                                    // update image
                                    new RegisterImageTask().execute();
                                }else{
                                    // create image
                                    new RegisterImageTask().execute();
                                }
                               // Toast.makeText(ProfileActivity.this, myBitmap.toString(), Toast.LENGTH_SHORT).show();
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.alert_dialog_text_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
    }

    private void alertShowFullDescription(String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        alertDialogBuilder.setTitle(getString(R.string.textView_description));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton(
                getString(R.string.alert_dialog_text_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = alertDialogBuilder.create();
        alert11.show();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    // when get and show the image
    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    // button create image
    public void onCreateImage(View v){
        Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED
                &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED  ){
            ActivityCompat.requestPermissions(ProfileActivity.this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
        }else{
            permission_camera = true;
        }

        if(permission_camera){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    Log.d("IMAGE onCreate", ex.toString());
                }

                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }


                //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
             }

        }
            else {
                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.toast_text_no_permission_take_camera), Toast.LENGTH_SHORT).show();
            }
    }

    private File createImageFile() throws IOException {

        if(permission_camera ){
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            mCurrentPhotoPathAbsolute = image.getAbsolutePath();
            return image;
        }else {
            return null;
        }

    }

    // button search image
    public void onSearchImage(View v){
        Toast.makeText(getApplicationContext(), "onSearchImage", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_GET_CONTENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setType("image/*");

        startActivityForResult(intent, REQUEST_BROWSE_PHOTO);
    }

    // Methods - set visible the preview image
    private void AttachBitmapToReport()
    {
        int photoW = myBitmap.getWidth();
        int photoH = myBitmap.getHeight();
        int scaleFactor = Math.min(photoW/100, photoH/100);
        int NewW = photoW/scaleFactor;
        int NewH = photoH/scaleFactor;

        imageViewNew.setVisibility(View.VISIBLE);
        imageViewNew.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        InputStream stream = null;

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
            AttachBitmapToReport();
        }

        if (requestCode == REQUEST_BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
        {

            try
            {
                stream = getContentResolver().openInputStream(data.getData());
                myBitmap = BitmapFactory.decodeStream(stream);
                AttachBitmapToReport();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_TAKE_PHOTO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    Log.d("Permission camera","granted");

                    permission_camera = true;

                } else {

                    // permission denied, boo!
                    Log.d("Permission camera","denied");

                    permission_camera = false;
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }
}
