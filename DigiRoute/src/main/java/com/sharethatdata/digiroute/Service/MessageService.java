package com.sharethatdata.digiroute.Service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.sharethatdata.digiroute.R;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cMessage;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


/**
 * Created by mv on 28/8/2017.
 */

public class MessageService extends Service {

    WSDataProvider MyProvider = null;

    private String user = "";
    private String pass = "";
    private String webservice_url = "";
    private String pharmacyId = "";
    private String access_key = "";
    private String ip = "";
    private String android_id = "";
    private String appname = "";

    Context context;

    String statusReadMessage = "";
    int nrUnreadMessage = 0;

    String message_to_user = "";
    String message_text = "";
    String message_id = "";

    String statusReadNews = "";
    int nrUnreadNews = 0;

    NotificationCompat.InboxStyle inboxStyle ;
    NotificationManagerCompat notificationManagerCompact ;
    NotificationCompat.Builder builder ;

    ArrayList<HashMap<String, String>> messageList;

    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    int LOLLIPOP = 21;
    int MARSHMALLOW = 23;

    /** indicates how to behave if the service is killed */
    int mStartMode;

    /** interface for clients that bind */
    IBinder mBinder;

    /** indicates whether onRebind should be used */
    boolean mAllowRebind;

    int notify_id = 0;

    /** Called when the service is being created. */
    @Override
    public void onCreate() {

    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (android.os.Debug.isDebuggerConnected()) Toast.makeText(MessageService.this, getString(R.string.toast_text_message_checking), Toast.LENGTH_SHORT).show();

    //    Toast.makeText(MessageService.this, getString(R.string.toast_text_message_checking), Toast.LENGTH_SHORT).show();

        SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
        user = prefUser.getString("USER", "");
        pass = prefUser.getString("PASS", "");
        webservice_url = prefUser.getString("URL", "");

     //   Toast.makeText(MessageService.this, "MessageService: " + webservice_url, Toast.LENGTH_SHORT).show();

        SharedPreferences prefPharmacy = getSharedPreferences("PREF_Pharmacy", 0);
        pharmacyId = prefPharmacy.getString("Pharmacy", "");

        SharedPreferences prefDevice = getSharedPreferences("PREF_DEVICE", 0);
        ip = prefDevice.getString("DEVICE_IP","");
        android_id = prefDevice.getString("DEVICE_ID","");
        appname = prefDevice.getString("DEVICE_NAME","");


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        access_key = prefs.getString("access_key", "");

        //if(webservice_url.equals("")){
        //    webservice_url = "https://www.sharethatdata.com/devlogistics-ws";
        //}

        Log.d("TAG", "user == null: " + user);
        Log.d("TAG", "pass == null: " + pass);
        Log.d("TAG", "webservice_url == null: " + webservice_url);

        MyProvider = new WSDataProvider(user, pass);

        context = getApplicationContext();

        new LoadListMessages().execute();

        return mStartMode;
    }

    /** A client is binding to the service with bindService() */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {

    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        System.out.println("onDestroy Message Service");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        System.out.println("onTaskRemoved Message Service");

        //   Intent intent = new Intent(getApplicationContext(), LocationReceiver.class);
        //   PendingIntent pendingIntent = PendingIntent.getBroadcast(
        //           getApplicationContext(), 5, intent, 0);

        //    AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //         manager.cancel(pendingIntent);

        // Destroy the service
        //   stopSelf();
    }

    /**
     * Background Async Task to Load all messages by making HTTP Request
     * */
    class LoadListMessages extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         */
        protected Boolean doInBackground(String... args) {

            boolean more_messages = true;

            // load data from provider
            messageList = new ArrayList<HashMap<String, String>>();

            boolean suc = false;

            while (more_messages) {
                MyProvider = new WSDataProvider(user, pass);
                MyProvider.SetWebServiceUrl(webservice_url);
                cMessage message = MyProvider.getNotifyMessage(pharmacyId);

                if (message.id > 0) {
                    message_to_user = message.user_to;
                    message_text = message.datetime + ": " + message.message;
                    message_id = String.valueOf(message.id);
                    suc = true;

                    // register
                    showMessageNotification(message_to_user, message_text);

                        // update message
                        /*SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
                        user = prefUser.getString("USER", "");
                        pass = prefUser.getString("PASS", "");
                        webservice_url = prefUser.getString("URL", "");*/

                        MyProvider = new WSDataProvider(user, pass);
                        MyProvider.SetWebServiceUrl(webservice_url);
                        MyProvider.updateMessageStatusNotify(message_id, pharmacyId);

                } else {
                    more_messages = false;
                }
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(Boolean result) {

            System.out.println("Result boolean: " + result);

            if(result){
                //showMessageNotification();
            }
            else{
                System.out.println("Result last_error_message: " + MyProvider.last_error);
                if(MyProvider.last_error.equals("required field (token) missing")){
                    new UserLoginTask().execute();
                }
            }

        }
    }

    /**
     * Background Async Task to Load all messages by making HTTP Request
     * */
    class UpdateStatusMessages extends AsyncTask<String, String, Boolean> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting items from url
         */
        protected Boolean doInBackground(String... args) {

            boolean more_messages = true;

            // load data from provider
            messageList = new ArrayList<HashMap<String, String>>();

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            boolean suc = false;

            if(webservice_url == ""){
                SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
                user = prefUser.getString("USER", "");
                pass = prefUser.getString("PASS", "");
                webservice_url = prefUser.getString("URL", "");

                MyProvider = new WSDataProvider(user, pass);
                MyProvider.SetWebServiceUrl(webservice_url);

                MyProvider.updateMessageStatusNotify(message_id, pharmacyId);
            }else{
                MyProvider.updateMessageStatusNotify(message_id, pharmacyId);
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(Boolean result) {

            System.out.println("Result boolean: " + result);

            if(result){
                //showMessageNotification();
            }
            else{
                System.out.println("Result last_error_message: " + MyProvider.last_error);
                if(MyProvider.last_error.equals("required field (token) missing")){
                    new UserLoginTask().execute();
                }
            }

        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String mEmail = user;
            String mPassword = pass;
            String mAccessKey = access_key;


            if(mEmail.length() == 0 || mPassword.length() == 0 ){
			    	/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
			        String prefEmail = pref.getString("Name", "");
			        String prefPass = pref.getString("Pass", "");*/

                SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
                String prefEmail = prefUser.getString("USER", "");
                String prefPass = prefUser.getString("PASS", "");
                webservice_url = prefUser.getString("URL", "");

                mEmail = prefEmail;
                mPassword = prefPass;

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                access_key = prefs.getString("access_key", "");

                System.out.println("mEmail mPassword from pref: " + mEmail + " " + mPassword);
            }else {
                System.out.println("mEmail mPassword from global: " + mEmail + " " + mPassword);
            }

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            MyProvider.SetDeviceIdentification(ip, android_id, appname);
            boolean suc = MyProvider.CheckSecurekLogin(mEmail, mPassword, "app service", "" ,mAccessKey);

            return suc;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                System.out.println("Login successfully");
                new LoadListMessages().execute();
            } else {
                System.out.println("Login failed");
            }
        }

        @Override
        protected void onCancelled() {

        }
    }

    public void showMessageNotification(String message_from, String message){

        Intent launchIntentDigiPortal = getPackageManager().getLaunchIntentForPackage("com.sharethatdata.digiroute");

        // if DigiPortal installed then open application when click on notification
        if(launchIntentDigiPortal != null){

            launchIntentDigiPortal.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, launchIntentDigiPortal, PendingIntent.FLAG_ONE_SHOT);

            PendingIntent pendingIntent = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                //checking if alarm is working - yesterday
                pendingIntent = PendingIntent.getActivity(this, 1, launchIntentDigiPortal, PendingIntent.FLAG_MUTABLE);
            }
            else
            {
                //checking if alarm is working - yesterday
                pendingIntent = PendingIntent.getActivity(this, 1, launchIntentDigiPortal, PendingIntent.FLAG_ONE_SHOT);
            }

            notificationManagerCompact = NotificationManagerCompat.from(MessageService.this);
            builder = new NotificationCompat.Builder(MessageService.this);

            builder.setSmallIcon(com.sharethatdata.webservice.R.drawable.launcher)
                    .setContentTitle("HomeDelivery: " + getString(R.string.notification_title_message_for) + " " + message_from)
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setTicker("Notification!")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setVibrate(new long[] {0,100,0,100});

            notify_id++;

            // Sets an ID for the notification
            int mNotificationId = notify_id;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, builder.build());


        }
    }

    private class RegisterMessageNotify extends AsyncTask<String, String, Boolean> {

        public RegisterMessageNotify(){

        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            boolean suc = false;

            MyProvider.updateMessageStatusNotify(message_id, pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            // updating UI from Background Thread

        }
    }

}