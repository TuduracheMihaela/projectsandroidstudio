package com.sharethatdata.digiroute.Service;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.sharethatdata.digiroute.R;
import com.sharethatdata.digiroute.SelectActionActivity;
import com.sharethatdata.digiroute.notification.MyService;
import com.sharethatdata.logistics_webservice.WSDataProvider;

import java.util.ArrayList;

/**
 * Created by miha on 8/7/2017.
 */

public class LocationService extends Service implements LocationListener {

    WSDataProvider MyProvider = null;

    private String user = "";
    private String pass = "";
    private String webservice_url = "";
    private String pharmacyId = "";
    private String ip = "";
    private String android_id = "";
    private String appname = "";
    private String access_key = "";

    Context context;

    protected LocationManager locationManager;
    Location loc;

    ArrayList<String> permissions = new ArrayList<String>();

    boolean canGetLocation = true;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    int LOLLIPOP = 21;
    int MARSHMALLOW = 23;
    final String TAG = "GPS";

    /** indicates how to behave if the service is killed */
    int mStartMode;

    /** interface for clients that bind */
    IBinder mBinder;

    /** indicates whether onRebind should be used */
    boolean mAllowRebind;

    /** Called when the service is being created. */
    @Override
    public void onCreate() {

    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (android.os.Debug.isDebuggerConnected()) Toast.makeText(LocationService.this, getString(R.string.toast_text_location_checking), Toast.LENGTH_SHORT).show();

     //   Toast.makeText(LocationService.this, getString(R.string.toast_text_location_checking), Toast.LENGTH_SHORT).show();

        SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
        user = prefUser.getString("USER", "");
        pass = prefUser.getString("PASS", "");
        webservice_url = prefUser.getString("URL", "");

        SharedPreferences prefPharmacy = getSharedPreferences("PREF_Pharmacy", 0);
        pharmacyId = prefPharmacy.getString("Pharmacy", "");

        SharedPreferences prefDevice = getSharedPreferences("PREF_DEVICE", 0);
        ip = prefDevice.getString("DEVICE_IP","");
        android_id = prefDevice.getString("DEVICE_ID","");
        appname = prefDevice.getString("DEVICE_NAME","");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        access_key = prefs.getString("access_key", "");

        //if(webservice_url.equals("")){
         //   webservice_url = "http://www.sharethatdata.com/devlogistics-ws";
        //}

        Log.d("TAG", "user == null: " + user);
        Log.d("TAG", "pass == null: " + pass);
        Log.d("TAG", "webservice_url == null: " + webservice_url);

        MyProvider = new WSDataProvider(user, pass);
        MyProvider.SetWebServiceUrl(webservice_url);

        context = getApplicationContext();
        //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);

        //getLastLocation();

        getLocation();

        return mStartMode;
    }

    /** A client is binding to the service with bindService() */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {

    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        System.out.println("onDestroy Location Service");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        System.out.println("onTaskRemoved Location Service");

        Intent intent = new Intent(getApplicationContext(), LocationReceiver.class);
        //PendingIntent pendingIntent = PendingIntent.getBroadcast(
                //getApplicationContext(), 5, intent, 0);

        PendingIntent pendingIntent = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), 5, intent, PendingIntent.FLAG_MUTABLE);
        }else{
            pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), 5, intent, 0);
        }

         AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
              manager.cancel(pendingIntent);

        // Destroy the service
        stopSelf();
    }

    ////////////////////////////////////////////////////////////////////
    /*
    Location
     */
    @Override
    public void onLocationChanged(Location location) {
        String x = "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude();
        System.out.println(x);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }

    private void getLocation() {
        boolean isGPS = false;
        boolean isNetwork = false;
        boolean gpsCouldNotGetLocation = false;
        boolean networkCouldNotGetLocation = false;

        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //isGPS = false;

        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS || isNetwork) {
                    // from GPS
                    if(isGPS){
                        Log.d(TAG, "GPS on");
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (loc != null)
                                System.out.println(loc);

                            try{
                                Double latitude = loc.getLatitude();
                                Double longitude = loc.getLongitude();
                                new RegisterTaskLocation(latitude, longitude).execute();
                            }catch (Exception e){
                                System.out.println(e.toString());
                                gpsCouldNotGetLocation = true;
                            }

                        }
                    } else {

                        if (isNetwork) {
                            // from Network Provider
                            Log.d(TAG, "NETWORK_PROVIDER on");
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                            if (locationManager != null) {
                                loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null)
                                    System.out.println(loc);

                                try {
                                    Double latitude = loc.getLatitude();
                                    Double longitude = loc.getLongitude();
                                    new RegisterTaskLocation(latitude, longitude).execute();
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                }
                            }
                        }
                    }

                    //////////////////////////////////////////////////////////////////
                    // if gps location didn't get location, then try with network
                    if(gpsCouldNotGetLocation){
                        // from Network Provider
                        Log.d(TAG, "NETWORK_PROVIDER on");
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (loc != null)
                                System.out.println(loc);

                            try {
                                Double latitude = loc.getLatitude();
                                Double longitude = loc.getLongitude();
                                new RegisterTaskLocation(latitude, longitude).execute();
                            } catch (Exception e) {
                                System.out.println(e.toString());
                                networkCouldNotGetLocation = true;
                            }
                        }
                    }

                }else{
                    // ask again for permission, if the user stopped location or/and network from phone
                    // and try to aproximate location until the user activate back the location or/and network

                    Toast.makeText(context, getString(R.string.toast_text_turn_on_gps_network), Toast.LENGTH_SHORT).show();

                    try{
                        Double latitude = loc.getLatitude();
                        Double longitude = loc.getLongitude();
                        new RegisterTaskLocation(latitude, longitude).execute();
                    }catch (Exception e){
                        System.out.println(e.toString());
                    }
                }
                System.out.println(loc);

            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    /*private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }*/


    /**
     * Background Async Task to detect the Wifi or Mobile network location and register in webservice
     * */

    private class RegisterTaskLocation extends AsyncTask<String, String, Boolean> {
        double latitude, longitude;

        public RegisterTaskLocation(double latitude, double longitude){
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);

            boolean suc = false;

            suc = MyProvider.updateUserCurrentLocation(pharmacyId, latitude, longitude);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            // updating UI from Background Thread
            System.out.println("Result boolean: " + success);

            if(success){

            }
            else{
                System.out.println("Result last_error_message: " + MyProvider.last_error);
                if(MyProvider.last_error.equals("required field (token) missing")){
                    new UserLoginTask(latitude, longitude).execute();
                }
            }

        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        double latitude, longitude;

        public UserLoginTask(double latitude, double longitude){
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            String mEmail = user;
            String mPassword = pass;
            String mAccessKey = access_key;

            if(mEmail.length() == 0 || mPassword.length() == 0 ){
			    	/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE );
			        String prefEmail = pref.getString("Name", "");
			        String prefPass = pref.getString("Pass", "");*/

                SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0);
                String prefEmail = prefUser.getString("USER", "");
                String prefPass = prefUser.getString("PASS", "");
                webservice_url = prefUser.getString("URL", "");

                mEmail = prefEmail;
                mPassword = prefPass;

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                access_key = prefs.getString("access_key", "");

                System.out.println("mEmail mPassword from pref: " + mEmail + " " + mPassword);
            }else {
                System.out.println("mEmail mPassword from global: " + mEmail + " " + mPassword);
            }

            MyProvider = new WSDataProvider(user, pass);
            MyProvider.SetWebServiceUrl(webservice_url);
            MyProvider.SetDeviceIdentification(ip, android_id, appname);
            boolean suc = MyProvider.CheckSecurekLogin(mEmail, mPassword, "app service", "", access_key);

            return suc;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                System.out.println("Login successfully");
                new RegisterTaskLocation(latitude, longitude).execute();
            } else {
                System.out.println("Login failed");
            }
        }

        @Override
        protected void onCancelled() {

        }
    }

}
