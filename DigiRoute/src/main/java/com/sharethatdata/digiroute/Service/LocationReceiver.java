package com.sharethatdata.digiroute.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

/**
 * Created by miha on 8/7/2017.
 */

public class LocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Toast.makeText(context, "MyReceiver Detected.", Toast.LENGTH_LONG).show();

        Log.d("Received : ", "LocationReceiver");

        Intent service = new Intent(context, LocationService.class);
        String stop = "";
        try{
            stop = intent.getExtras().getString("service");
            System.out.println("service: " + stop);
            if(stop.equals("stop")){
                System.out.println("stop service");
                context.stopService(service);
                context.unregisterReceiver(LocationReceiver.this);
            }else if(stop.equals("start")) {
                System.out.println("start service");
                context.startService(service);
            }
        }catch (Exception e){
            System.out.println("service exception: " + stop);
        }
    }
}
