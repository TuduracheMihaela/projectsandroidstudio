package com.sharethatdata.digiroute.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by mv on 28/8/2017.
 */

public class MessageReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Toast.makeText(context, "MyReceiver Detected.", Toast.LENGTH_LONG).show();

        Log.d("Received : ", "MessageReceiver");

        Intent service = new Intent(context, MessageService.class);
        String stop = "";
        try{
            stop = intent.getExtras().getString("service");
            System.out.println("service: " + stop);
            if(stop.equals("stop")){
                System.out.println("stop message service");
                context.stopService(service);
                context.unregisterReceiver(MessageReceiver.this);
            }else if(stop.equals("start")) {
                System.out.println("start message service");
                context.startService(service);
            }
        }catch (Exception e){
            System.out.println("service exception: " + stop);
        }
    }
}
