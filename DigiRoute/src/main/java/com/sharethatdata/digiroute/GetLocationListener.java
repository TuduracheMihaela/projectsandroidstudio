package com.sharethatdata.digiroute;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by mihu__000 on 2/10/2017.
 */

/*----------Listener class to get coordinates ------------- */
class GetLocationListener implements LocationListener {
    Context context;
    private static final String TAG = "Debug";

    public GetLocationListener(Context context){
        this.context = context;

    }

    @Override
    public void onLocationChanged(Location loc) {

        Toast.makeText(context.getApplicationContext(), context.getString(R.string.toast_text_location_changed) + " : Lat: " +
                        loc.getLatitude()+ " Lng: " + loc.getLongitude(),
                Toast.LENGTH_SHORT).show();
        String longitude = "Longitude: " +loc.getLongitude();
        Log.v(TAG, longitude);
        String latitude = "Latitude: " +loc.getLatitude();
        Log.v(TAG, latitude);

    /*----------to get Address from coordinates ------------- */
        String addressName=null;
        String cityName=null;
        String countryName=null;
        Geocoder gcd = new Geocoder(context.getApplicationContext(),
                Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(loc.getLatitude(), loc
                    .getLongitude(), 1);
            if (addresses.size() > 0)
                addressName=addresses.get(0).getAddressLine(0);
                cityName=addresses.get(0).getLocality();
                countryName=addresses.get(0).getCountryName();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String s = longitude+"\n"+latitude +
                "\n\nMy Address is: "+addressName +
                "\n\nMy Currrent City is: "+cityName +
                "\n\nMy Currrent Country is: "+countryName;

        Log.v(TAG, s);
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider,
                                int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
}

