package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Adapter.AddressDetailPackageAdapter;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewListViewPhone;
import com.sharethatdata.digiroute.Tutorial.customshowcaseview.CustomShowcaseViewListViewTablet;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseView;
import com.sharethatdata.digiroute.Tutorial.showcaseview.targets.ViewTarget;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cAddress;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPharmacyAddress;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sharethatdata.digiroute.MyGlobals.getContext;

/**
 * Created by mihu__000 on 1/24/2017.
 */

public class AddressDetailActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String PharmacyId = "";

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> packageList; // List Packages
    ArrayList<HashMap<String, String>> packageRouteList; // List Packages

    TextView textViewId;
    TextView textViewCustomerStreetLabel;
    TextView textViewCustomerNameLabel;
    TextView textViewCustomerName;
    TextView textViewCustomerScript;
    TextView textViewCustomerFreeText;
    TextView textViewCustomerSex;
    TextView textViewCustomerStreet;
    TextView textViewCustomerHouse;
    TextView textViewCustomerZipcode;
    TextView textViewCustomerCity;
    TextView textViewCustomerCountry;
    TextView textViewCustomerDistanceLabel;
    TextView textViewCustomerDistance;
    TextView textViewCustomerTimeLabel;
    TextView textViewCustomerTime;

    ImageView marker_address, marker_package;

    String endLongitude;
    String endLatitude;

    ListView lv = null;

    String idAddress;
    String idDelivery;
    String idRoute;
    String idScript;
    String barcode;
    String distance;
    String time;
    String pharmacyAddress, pharmacyLatitude, pharmacyLongitude;

    boolean loadMap = false;

    // GetCurrentLocation getLocation;
   // GPSTracker class
   GPSTracker gps;

    //Tutorial
    ShowcaseView showcaseView;


    protected void onCreate(Bundle savedInstanceSaved){
        super.onCreate(savedInstanceSaved);
        setContentView(R.layout.activity_detail_address);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(AddressDetailActivity.this, AddressDetailActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        textViewCustomerNameLabel = (TextView) findViewById(R.id.textViewNameLabel);
        textViewCustomerName = (TextView) findViewById(R.id.textViewName);
        textViewCustomerScript = (TextView) findViewById(R.id.textViewScript);
        textViewCustomerFreeText = (TextView) findViewById(R.id.textViewFreeText);
        TextView textViewCustomerFreeTextLabel = (TextView) findViewById(R.id.textViewFreeTextLabel);
        textViewCustomerStreet = (TextView) findViewById(R.id.textViewAddress);
        textViewCustomerStreetLabel = (TextView) findViewById(R.id.textViewAddressLabel);
        textViewCustomerDistanceLabel = (TextView) findViewById(R.id.customerDistanceLabel);
        textViewCustomerDistance = (TextView) findViewById(R.id.customerDistance);
        textViewCustomerTimeLabel = (TextView) findViewById(R.id.customerTimeLabel);
        textViewCustomerTime = (TextView) findViewById(R.id.customerTime);
        marker_address = (ImageView) findViewById(R.id.marker_address);
        marker_package = (ImageView) findViewById(R.id.marker_package);

        Typeface font1 = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        Typeface font2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/Quicksand-Regular.otf");
        textViewCustomerScript.setTypeface(font1);
        textViewCustomerNameLabel.setTypeface(font2);
        textViewCustomerName.setTypeface(font2);
        textViewCustomerFreeTextLabel.setTypeface(font2);
        textViewCustomerFreeText.setTypeface(font2);
        textViewCustomerStreet.setTypeface(font2);
        textViewCustomerStreetLabel.setTypeface(font2);
        textViewCustomerDistanceLabel.setTypeface(font2);
        textViewCustomerDistance.setTypeface(font2);
        textViewCustomerTimeLabel.setTypeface(font2);
        textViewCustomerTime.setTypeface(font2);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            idAddress= null;
            idDelivery= null;
            idRoute= null;
            idScript = null;
            barcode = null;
            distance = "";
            time = "";
        } else {
            idAddress= extras.getString("idAddress");
            idDelivery= extras.getString("idDelivery");
            idRoute= extras.getString("idRoute");
            idScript= extras.getString("idScript");
            barcode= extras.getString("barcode");
            distance= extras.getString("distance");
            time= extras.getString("time");
        }

        Button btnEndRoute = (Button) findViewById(R.id.btnEndRoute);
        btnEndRoute.setTypeface(font1);

        Button buttonNavigate = (Button) findViewById(R.id.btnNavigate);
        buttonNavigate.setTypeface(font1);
        buttonNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 new CreateRouteReport(PharmacyId, idRoute, idAddress).execute();

                // source : https://developers.google.com/maps/documentation/android-api/intents

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:" + endLatitude + ", " + endLongitude + "?q=" + textViewCustomerStreet.getText()));
               //          Uri.parse("google.navigation:q=" + endLatitude + ", " + endLongitude + " (" + textViewCustomerStreet.getText() + ")"));
               //         Uri.parse("google.navigation:q=" + textViewCustomerStreet.getText() + ""));
               // intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });

        lv = (ListView) findViewById(R.id.listView);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String idPackage = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
                String idDelivery = ((TextView) itemClicked.findViewById(R.id.tv_delivery_id)).getText().toString();
                String idScript = ((TextView) itemClicked.findViewById(R.id.textViewScript)).getText().toString();
                String packageIdCode = ((TextView) itemClicked.findViewById(R.id.packageBarcode)).getText().toString();
                String packageStatus = ((TextView) itemClicked.findViewById(R.id.packageStatusDescription)).getText().toString();
                String packageStatusID = ((TextView) itemClicked.findViewById(R.id.tv_status_id)).getText().toString();

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), DeliverActivity.class);
                intent.putExtra("idPackage", idPackage);
                intent.putExtra("packageIdCode", packageIdCode);
                intent.putExtra("packageStatus", packageStatus);
                intent.putExtra("packageStatusID", packageStatusID);
                intent.putExtra("idAddress", idAddress);
                intent.putExtra("idRoute", idRoute);
                intent.putExtra("idScript", idScript);
                intent.putExtra("idDelivery", idDelivery);

                startActivity(intent);

            }

        });

        btnEndRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check if all packages are scanned
                new LoadPackageOfRoute(idRoute, PharmacyId).execute();
            }
        });

        btnEndRoute.setVisibility(View.GONE);

        if(idScript.equals("Pharmacy")){

            //textViewPackages.setVisibility(View.GONE);
            lv.setVisibility(View.GONE);
            textViewCustomerScript.setVisibility(View.GONE);
            textViewCustomerNameLabel.setVisibility(View.GONE);
            textViewCustomerName.setVisibility(View.GONE);
            textViewCustomerFreeTextLabel.setVisibility(View.GONE);
            textViewCustomerFreeText.setVisibility(View.GONE);
            textViewCustomerDistanceLabel.setVisibility(View.GONE);
            textViewCustomerTimeLabel.setVisibility(View.GONE);
            btnEndRoute.setVisibility(View.VISIBLE);

            WebView webView = (WebView)findViewById(R.id.webView);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            webView.setLayoutParams(param);

            new LoadPharmacyLocation(PharmacyId).execute();
        }else{
            new LoadPharmacyLocation(PharmacyId).execute();
            new LoadAddressItem(PharmacyId).execute();
            new LoadListPackage(PharmacyId).execute();
        }

        loadMap = true;


        // Tutorial
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(tabletSize) {
            // tablet
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("12", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(this)
                        .setTarget(new ViewTarget(R.id.listView, this))
                        .setContentTitle(getString(R.string.title_list_packages))
                        .setContentText(getString(R.string.text_list_packages))
                        .setShowcaseDrawer(new CustomShowcaseViewListViewTablet(getResources()))
                        .singleShot(12)
                        .build();
                RelativeLayout.LayoutParams buttonViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
                buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_TOP);
                buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                buttonViewLayoutParams.setMargins(0, 50, 50, 0);
                showcaseView.setButtonPosition(buttonViewLayoutParams);
                showcaseView.forceTextPosition(ShowcaseView.ABOVE_SHOWCASE);

                // mark first time has runned.
                SharedPreferences.Editor editor12 = prefs.edit();
                editor12.putBoolean("12", true);
                editor12.commit();
            }

        }else {
            // phone
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (!prefs.getBoolean("13", false)) {
                // <---- run one time code here
                showcaseView = new ShowcaseView.Builder(this)
                        .setTarget(new ViewTarget(R.id.listView, this))
                        .setContentTitle(getString(R.string.title_list_packages))
                        .setContentText(getString(R.string.text_list_packages))
                        .setShowcaseDrawer(new CustomShowcaseViewListViewPhone(getResources()))
                        .singleShot(13)
                        .build();
                RelativeLayout.LayoutParams buttonViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
                buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_TOP);
                buttonViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                buttonViewLayoutParams.setMargins(0, 50, 50, 0);
                showcaseView.setButtonPosition(buttonViewLayoutParams);
                showcaseView.forceTextPosition(ShowcaseView.ABOVE_SHOWCASE);

                // mark first time has runned.
                SharedPreferences.Editor editor13 = prefs.edit();
                editor13.putBoolean("13", true);
                editor13.commit();
            }

        }

    }

    public void onResume(){
        super.onResume();
        String update_status_package = Globals.getValue("update_status_package");
        String tutorial_update_status_package = Globals.getValue("tutorial_update_status_package");

        if(!tutorial_update_status_package.equals("")){
            try{
                showcaseView.hide();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(update_status_package != ""){
            new LoadPharmacyLocation(PharmacyId).execute();
            new LoadAddressItem(PharmacyId).execute();
            new LoadListPackage(PharmacyId).execute();
            //new LoadPackageOfRoute(idRoute,PharmacyId).execute();

            loadMap = false;

            Globals.setValue("update_status_package","");
            Globals.setValue("update_addresses","update_addresses");
        }

    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    // onSaveInstanceState
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("isPharmacy", idScript);

        super.onSaveInstanceState(savedInstanceState);
    }

    // onRestoreInstanceState
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        idScript = savedInstanceState.getString("isPharmacy");

    }

    public void loadMapByPharmacyAddress(){

        String startpoint = pharmacyAddress;
        String endpoint = textViewCustomerStreet.getText().toString();
        double end_latitude = 0;
        double end_longitude = 0;
        double start_latitude = 0; //Double.parseDouble(pharmacyLatitude);
        double start_longitude = 0; //Double.parseDouble(pharmacyLongitude);
        try {
            end_latitude = Double.valueOf(endLatitude);
            end_longitude = Double.valueOf(endLongitude);
            start_latitude = Double.valueOf(pharmacyLatitude);
            start_longitude = Double.valueOf(pharmacyLongitude);
        }catch (Exception e){
            e.printStackTrace();
        }
        /*if(idScript.equals("Pharmacy")){
            end_latitude = 0; //Double.valueOf(pharmacyLatitude);
            end_longitude = 0; //Double.valueOf(pharmacyLongitude);
        }else{
            end_latitude = 0; //Double.valueOf(endLatitude);
            end_longitude = 0; //Double.valueOf(endLongitude);
        }*/


         //   Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
            Log.d("startpoint", startpoint);
            Log.d("endpoint", endpoint);

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        String map = getString(R.string.google_map_single); // is google map
        double size_tablet = MyTimeUtils.tabletSize(AddressDetailActivity.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, endpoint, "", "", 950, 650, start_latitude, start_longitude,
                    end_latitude, end_longitude,AddressDetailActivity.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, endpoint, "", "", 450, 350, start_latitude, start_longitude,
                    end_latitude, end_longitude,AddressDetailActivity.this));

        }else{
            if (size_tablet > 5.5) {
                webView.loadUrl(MyProvider.driving_route(map, startpoint, endpoint, "", "", 440, 300, start_latitude, start_longitude,
                        end_latitude, end_longitude, AddressDetailActivity.this));
            } else {
                webView.loadUrl(MyProvider.driving_route(map, startpoint, endpoint, "", "", 340, 200, start_latitude, start_longitude,
                        end_latitude, end_longitude, AddressDetailActivity.this));
            }
        }
    }

    public void loadMapByGPSLocation(){
         /*
         Get GPS coordinates
         */
        // create class object
        gps = new GPSTracker(AddressDetailActivity.this);
        String startpoint = "";
        String endpoint = textViewCustomerStreet.getText().toString();
        double start_latitude = gps.getLatitude();
        double start_longitude = gps.getLongitude();
        double end_latitude = Double.valueOf(endLatitude);
        double end_longitude = Double.valueOf(endLongitude);

        // check if GPS enabled
        if(gps.canGetLocation()){

            start_latitude = gps.getLatitude();
            start_longitude = gps.getLongitude();
            startpoint = gps.getAddress();

           // Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
            Log.d("startpoint", startpoint);

            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
           // gps.displayLocationSettingsRequest(AddressDetailActivity.this);
            gps.showSettingsAlert();
        }

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        double size_tablet = MyTimeUtils.tabletSize(AddressDetailActivity.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route("", startpoint, endpoint, "", "", 950, 650, start_latitude, start_longitude,
                    end_latitude, end_longitude, AddressDetailActivity.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route("", startpoint, endpoint, "", "", 450, 350, start_latitude, start_longitude,
                    end_latitude, end_longitude,AddressDetailActivity.this));

        }else{
            if (size_tablet > 5.5) {
                webView.loadUrl(MyProvider.driving_route("", startpoint, endpoint, "", "", 440, 300, start_latitude, start_longitude,
                        end_latitude, end_longitude, AddressDetailActivity.this));
            } else {
                webView.loadUrl(MyProvider.driving_route("", startpoint, endpoint, "", "", 340, 200, start_latitude, start_longitude,
                        end_latitude, end_longitude, AddressDetailActivity.this));
            }
        }
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadAddressItem extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadAddressItem(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cAddress address = MyProvider.getAddress(idAddress, pharmacyId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //textViewCustomerName.setText(Html.fromHtml(address.customer_surname + " " + address.customer_lastname));
                    textViewCustomerScript.setText(Html.fromHtml(getString(R.string.textview_customer_script) + " " + idScript));
                    textViewCustomerFreeText.setText(Html.fromHtml(address.free_text));
                    textViewCustomerFreeText.setTextColor(getResources().getColor(R.color.red));
                    if(!address.customer_surname.equals("") || !address.customer_lastname.equals(" ")){
                        textViewCustomerName.setText(address.customer_surname + " " + address.customer_lastname);
                    }else{
                        textViewCustomerNameLabel.setVisibility(View.GONE);
                        textViewCustomerName.setVisibility(View.GONE);
                    }

                    if(distance.length() > 0 || time.length() > 0){
                        textViewCustomerDistanceLabel.setVisibility(View.VISIBLE);
                        textViewCustomerTimeLabel.setVisibility(View.VISIBLE);
                        textViewCustomerDistance.setText(distance);
                        textViewCustomerTime.setText(time);
                    }else{
                        textViewCustomerDistanceLabel.setVisibility(View.GONE);
                        textViewCustomerTimeLabel.setVisibility(View.GONE);
                    }




                    //textViewCustomerStreet.setText(Html.fromHtml(address.customer_street + " " + address.customer_house + ", " + address.customer_zipcode + ", " + address.customer_city + ", " + address.customer_country));
                    endLatitude = address.latitude;
                    endLongitude = address.longitude;

                    List<String> slist = new ArrayList<String> ();
                    slist.add(address.customer_street);
                    slist.add(address.customer_house);
                    slist.add(address.customer_zipcode);
                    slist.add(address.customer_city);
                    slist.add(address.customer_country);

                    StringBuilder rString = new StringBuilder();
                    int count = 0;
                    for (String line : slist) {
                        ++count;
                        if (!line.equals("")) {
                            if(count != slist.size()){
                                rString.append(line + ", ");
                            }else{
                                rString.append(line);
                            }
                        }
                    }

                    textViewCustomerStreet.setText(rString.toString());
               }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(loadMap == true){
                loadMapByPharmacyAddress();
            }


            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListPackage extends AsyncTask<String, String, String> {

        String pharmacyId;
        boolean status;

        public LoadListPackage(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            packageList = new ArrayList<HashMap<String, String>>();

            status = false;

            List<cPackages> myPackage = MyProvider.getAllPackages(idRoute, idAddress, pharmacyId);
            //List<cPackagesbyDeliveryId> myPackage = MyProvider.getPackagesbyDeliveryId(idDelivery, pharmacyId);
            for(cPackages entry : myPackage)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("packageid",  entry.packageid);
                map.put("delivery_id",  entry.delivery_id);
                map.put("script_id",  entry.scriptid);
                map.put("barcode",  entry.package_barcode);
                map.put("status_description",  entry.package_status_description);
                map.put("status_id",  entry.status_id);

                // 1 = Loaded for delivery
                // 3 = In Transport
                if((entry.status_id.equals("3")) || (entry.status_id.equals("1"))){
                    status = true;
                }

                // adding HashList to ArrayList
                packageList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    if(status){
                        marker_address.setImageResource(R.drawable.marker_gray);
                    }else{
                        if(packageList.size() > 0){
                            marker_address.setImageResource(R.drawable.marker_green);
                        }else{
                            marker_address.setImageResource(R.drawable.marker_gray);
                        }

                    }

                    /* ListAdapter adapter = null;
                    adapter = new SimpleAdapter(
                            AddressDetailActivity.this
                            , packageList
                            , R.layout.activity_detail_package_listview_items, new String[] { "id", "script_id", "packageid","status_description"},
                            new int[] { R.id.textViewID, R.id.textViewScript, R.id.packageBarcode, R.id.packageStatus });*/
                    AddressDetailPackageAdapter adapter = new AddressDetailPackageAdapter(AddressDetailActivity.this,
                            R.layout.activity_detail_package_listview_items, packageList);
                    lv.setAdapter(adapter);

                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadPackageOfRoute extends AsyncTask<String, String, String> {

        public String routeid, pharmacyId;

        public LoadPackageOfRoute(String routeid, String pharmacyId){
            this.routeid = routeid;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            packageRouteList = new ArrayList<HashMap<String, String>>();

            List<cPackageRoute> myPackage = MyProvider.getPackagesOfRoute(routeid, "", pharmacyId);
            for(cPackageRoute entry : myPackage)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("scriptid",  entry.scriptid);
                map.put("packageid",  entry.package_barcode);
                map.put("addressid", entry.addressid);
                map.put("package_status",  entry.package_status);
                map.put("status_description",  entry.status_description);

                // adding HashList to ArrayList
                packageRouteList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                public void run() {
                    int totalScanned = 0;
                    int totalUnScanned = 0;
                    String myRow = "";
                    for (HashMap<String, String> map : packageRouteList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "package_status")
                            {
                                try {
                                    myRow = entry.getValue().toString();
                                    String row = String.valueOf(myRow);

                                    // 5 = Delivered
                                    // 6 = Not at home
                                    // 7 = Denied by customer
                                    if(!row.equals("5") && !row.equals("6") && !row.equals("7")){
                                        totalUnScanned++;
                                    }else{
                                        totalScanned++;
                                    }
                                } catch(NumberFormatException nfe) {

                                }
                            }
                        }

                    final int totalScanned1 = totalScanned;

                    // if are all packages scanned , update route status
                    if(totalUnScanned == 0) {
                        // alert - end route?
                        new AlertDialog.Builder(AddressDetailActivity.this)
                                .setTitle(getString(R.string.alert_dialog_title_route))
                                .setMessage(getString(R.string.alert_dialog_text_route_all_scanned))
                                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // end route
                                        Globals.setValue("update_route","update_route");
                                        new UpdateRouteStatus(pharmacyId, idRoute, "5").execute();
                                    }
                                })
                                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }else{
                        // NOT all packages are scanned
                        // alert - end route?
                        new AlertDialog.Builder(AddressDetailActivity.this)
                                .setTitle(getString(R.string.alert_dialog_title_route))
                                .setMessage(getString(R.string.alert_dialog_text_route_not_all_scanned))
                                .setPositiveButton(getString(R.string.btnYes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // end route even if not all packages scanned
                                        Globals.setValue("update_route","update_route");
                                        new UpdateRouteStatus(pharmacyId, idRoute, "5").execute();

                                         // extra warning
                                        // ask again: are you sure?
                                        /*new AlertDialog.Builder(AddressDetailActivity.this)
                                                .setTitle("Route")
                                                .setMessage("End route?")
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // end route with extra warning

                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // do nothing
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();*/

                                    }
                                })
                                .setNegativeButton(getString(R.string.btnNo), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
            });

            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadPharmacyLocation extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadPharmacyLocation(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cPharmacyAddress address = MyProvider.getPharmacyAddress(pharmacyId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pharmacyAddress = address.address1 + " " + address.address_house + " , " + address.address_zipcode + " , " + address.address_city + " , " + address.address_country;
                    pharmacyLatitude = address.latitude;
                    pharmacyLongitude = address.longitude;

                    if(idScript.equals("Pharmacy")){

                        endLatitude = pharmacyLatitude;
                        endLongitude = pharmacyLongitude;
                        textViewCustomerStreet.setText(pharmacyAddress);

                        loadMapByPharmacyAddress();
                    }
                }
            });
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    /** AsyncTask update status route  */
    private class UpdateRouteStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String routeId = "";
        String statusId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdateRouteStatus(String pharmacyId, String routeId, String statusId){
            this.pharmacyId = pharmacyId;
            this.routeId = routeId;
            this.statusId = statusId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateRouteStatus(pharmacyId, routeId, statusId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            final String update_status_route = Globals.getValue("update_route");
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Route Status", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(AddressDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(AddressDetailActivity.this, getString(R.string.toast_text_failed_update_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        if(update_status_route != ""){
                            finish();
                        }
                    }
                    else
                    {
                        Log.d("Update Route Status", "true");

                        if(update_status_route != ""){
                            finish();
                        }
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask create route report  */
    private class CreateRouteReport extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String routeId = "";
        String addressId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public CreateRouteReport(String pharmacyId, String routeId, String addressId){
            this.pharmacyId = pharmacyId;
            this.routeId = routeId;
            this.addressId = addressId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.createRouteReport(pharmacyId, routeId, addressId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("create route report", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(AddressDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(AddressDetailActivity.this, getString(R.string.toast_text_failed_create_route_report) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Log.d("create route report", "true");
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }
}
