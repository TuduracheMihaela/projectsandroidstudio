package com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview;


public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
