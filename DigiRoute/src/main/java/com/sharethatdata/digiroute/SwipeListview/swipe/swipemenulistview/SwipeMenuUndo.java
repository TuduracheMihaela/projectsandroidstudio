package com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview;

import android.os.Handler;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Created by miha on 10/25/2017.
 */

public class SwipeMenuUndo {

    // Fixed properties
    private AbsListView mListView;

    // Transient properties
    //private SortedSet<PendingDismissData> mPendingDismisses = new TreeSet<PendingDismissData>();
    private int mDismissAnimationRefCount = 0;

    private boolean mPaused;
    private float mDensity;

    private Handler mHandler;

    private PopupWindow mUndoPopup;
    private TextView mUndoText;
    private Button mUndoButton;

    private int mAutoHideDelay = 5000;
    private String mDeleteString = "Item deleted";
    private boolean mTouchBeforeAutoHide = true;

    private int mDelayedMsgId;

    ////////////////////////////////////////////////////////////////////













    /**
     * Returns an { android.widget.AbsListView.OnScrollListener} to be
     * added to the { ListView} using
     * { ListView#setOnScrollListener(android.widget.AbsListView.OnScrollListener)}.
     * If a scroll listener is already assigned, the caller should still pass
     * scroll changes through to this listener. This will ensure that this
     * { SwipeDismissListViewTouchListener} is paused during list view
     * scrolling.</p>
     *
     * @see { SwipeDismissListViewTouchListener}
     */
    private AbsListView.OnScrollListener makeScrollListener() {
        return new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                setEnabled(scrollState != AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL);
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            }
        };
    }

    /**
     * Enables or disables (pauses or resumes) watching for swipe-to-dismiss
     * gestures.
     *
     * @param enabled Whether or not to watch for gestures.
     */
    private void setEnabled(boolean enabled) {
        mPaused = !enabled;
    }







}
