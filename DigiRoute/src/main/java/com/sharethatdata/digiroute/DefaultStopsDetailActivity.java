package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cMyRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by miha on 8/24/2017.
 */

public class DefaultStopsDetailActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> routeList; // List Routes

    String pharmacyId = "";

    String routeId = "";
    String stop_id, name, address;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_stops_detail);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(DefaultStopsDetailActivity.this, DefaultStopsDetailActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            stop_id= null;
            name= null;
            address= null;
        } else {
            stop_id= extras.getString("id");
            name= extras.getString("name");
            address= extras.getString("address");
        }

        TextView textViewId = (TextView) findViewById(R.id.textViewId);
        TextView textViewName = (TextView) findViewById(R.id.name);
        TextView textViewAddress = (TextView) findViewById(R.id.address);
        Button btnAddDefaultStopToRoute = (Button) findViewById(R.id.btnAddDefaultStopToRoute);

        textViewId.setText(stop_id);
        textViewName.setText(name);
        textViewAddress.setText(address);

        btnAddDefaultStopToRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // call method to add package to route
                new AddStopToRoute(pharmacyId,stop_id,routeId).execute();

            }
        });

        new LoadRoute(pharmacyId).execute();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadRoute extends AsyncTask<String, String, String> {

        public String pharmacyId;

        public LoadRoute(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            routeList = new ArrayList<HashMap<String, String>>();

            List<cMyRoute> myRoute = MyProvider.getMyRoutes("", pharmacyId, "", "", "");
            for(cMyRoute entry : myRoute)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id/status/status_description/description", entry.id+"/"+entry.status+"/"+entry.status_description+
                        "/"+entry.description);

                // adding HashList to ArrayList
                routeList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (DefaultStopsDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    // check if route exist
                    for (HashMap<String, String> map1 : routeList){
                        for (Map.Entry<String, String> entry1 : map1.entrySet())
                        {
                            String row = String.valueOf(entry1.getValue().toString());
                            String[] parts = row.split("/");
                            String id = parts[0];
                            String status = parts[1];
                            String status_description = parts[2];
                            String description = parts[3];

                            // 1 = Loaded for delivery
                            if(status.equals("1")){
                                // route exist
                                routeId = id;
                            }
                        }
                    }

                    if(routeId.equals("")){
                        // route doesn't exist, so create new route
                        new CreateRoute(pharmacyId).execute();
                    }
                }
            });

            hideProgressDialog();
        }
    }

    /** AsyncTask create route report  */
    private class CreateRoute extends AsyncTask<String, String, Integer>{
        String pharmacyId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public CreateRoute(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        @Override
        protected Integer doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            int suc = MyProvider.createRoute(pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Integer success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success != 0)
                    {
                        Log.d("create route", "true");
                        routeId = String.valueOf(success);
                    }
                    else
                    {
                        Log.d("create route", "false");

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DefaultStopsDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DefaultStopsDetailActivity.this, getString(R.string.toast_text_failed_create_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask add default stop to route  */
    private class AddStopToRoute extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String stopId = "";
        String routeId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public AddStopToRoute(String pharmacyId, String stopId, String routeId){
            this.pharmacyId = pharmacyId;
            this.stopId = stopId;
            this.routeId = routeId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.addStopToRoute(pharmacyId, stopId, routeId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (DefaultStopsDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DefaultStopsDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DefaultStopsDetailActivity.this, getString(R.string.toast_text_failed_add_stop_route) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(DefaultStopsDetailActivity.this, getString(R.string.toast_text_add_stop_route), Toast.LENGTH_SHORT).show();
                        Log.d("Add stop to route", "true");
                    }
                }
            });
            hideProgressDialog();
            finish();

        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(DefaultStopsDetailActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

}
