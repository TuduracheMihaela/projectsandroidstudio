package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Adapter.zNewOrderAdapter;
import com.sharethatdata.digiroute.Scanner.IntentIntegrator;
import com.sharethatdata.digiroute.Scanner.IntentResult;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cPackageRoute;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;
import com.sharethatdata.logistics_webservice.datamodel.cPharmacyAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRoute;
import com.sharethatdata.logistics_webservice.datamodel.cRouteAddress;
import com.sharethatdata.logistics_webservice.datamodel.cRoutes;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihu__000 on 2/22/2017.
 */

public class zNewOrderActivityOLD extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> routeList; // List Routes
    ArrayList<String> routekeyArray =  new ArrayList<String>();
    ArrayList<HashMap<String, String>> addressList; // List Addresses by route
    ArrayList<HashMap<String, String>> packageList; // List Packages by route

    ArrayList<String> packagesBarcodeOfRouteSelectedArray =  new ArrayList<String>();
    ArrayList<String> packagesUniqueIdOfRouteSelectedArray =  new ArrayList<String>();

    ListView lv = null;
    TextView distance;
    TextView time;

    boolean blUseCamera = false;
    boolean blLoadingRoutes = false;
    boolean blRouteSelected = false;
    boolean blRouteSelectedFromPreviousState = false;
    boolean blPhoneRotate = true;
    boolean blBackToPreviousActivity = true;
    boolean blLoadOnceLoadPharmacyAddress = true;
    boolean blUpdateStatusPackage = false;

    String pharmacyAddress = "";
    String routeID = "";
    String pharmacyId = "";

    String check_string = "";

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zactivity_new_order);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(zNewOrderActivityOLD.this, zNewOrderActivityOLD.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
        final String locationPrefix = sharedPrefs.getString("prefScannerLocationPrefix", "");
        final String locationLength = sharedPrefs.getString("prefScannerLocationLength", "0");
        final String packageLength = sharedPrefs.getString("prefScannerPackageLength", "0");
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        check_string = getString(R.string.select_action_text1);

        final ImageView imageViewSubmit1 = (ImageView) findViewById(R.id.btnSubmit1);
        TextView textView1 = (TextView) findViewById(R.id.textView1);
        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
        lv = (ListView) findViewById(R.id.listView);
        distance = (TextView)findViewById(R.id.total_distance);
        time = (TextView)findViewById(R.id.total_time);
        WebView webView = (WebView)findViewById(R.id.webView);

        if (blCameraScanner)
        {
            EditText.setHint(getString(R.string.select_action_text3));
            imageViewSubmit1.setVisibility(View.VISIBLE);
        } else {
            EditText.setHint(getString(R.string.select_action_text4));
            imageViewSubmit1.setVisibility(View.GONE);
        }

        imageViewSubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                    // create barcode intent
                    IntentIntegrator integrator = new IntentIntegrator(zNewOrderActivityOLD.this);
                    integrator.initiateScan();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
            {
                String idAddress = ((TextView) itemClicked.findViewById(R.id.textViewID)).getText().toString();
                String idScript = ((TextView) itemClicked.findViewById(R.id.customerScript)).getText().toString();
                String barcode = ((TextView) itemClicked.findViewById(R.id.customerBarcode)).getText().toString();

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), AddressDetailActivity.class);
                intent.putExtra("idAddress", idAddress);
                intent.putExtra("idRoute", routeID);
                intent.putExtra("idScript", idScript);
                intent.putExtra("barcode", barcode);
                startActivity(intent);

            }

        });

        if(savedInstanceState != null){
           // addressList = new ArrayList<HashMap<String, String>>();
            addressList = (ArrayList<HashMap<String,String>>) savedInstanceState.getSerializable("addressList");
            packageList = (ArrayList<HashMap<String,String>>) savedInstanceState.getSerializable("packageList");
            routeList = (ArrayList<HashMap<String,String>>) savedInstanceState.getSerializable("routeList");
            routekeyArray = savedInstanceState.getStringArrayList("routekeyArray");
            packagesBarcodeOfRouteSelectedArray = savedInstanceState.getStringArrayList("packagesBarcodeOfRouteSelectedArray");
            packagesUniqueIdOfRouteSelectedArray = savedInstanceState.getStringArrayList("packagesUniqueIdOfRouteSelectedArray");

            blPhoneRotate = savedInstanceState.getBoolean("blPhoneRotate");
            blRouteSelected = savedInstanceState.getBoolean("blRouteSelected");
            blLoadOnceLoadPharmacyAddress = savedInstanceState.getBoolean("blLoadOnceLoadPharmacyAddress");
            blBackToPreviousActivity = savedInstanceState.getBoolean("backToPreviousActivity");
            pharmacyAddress = savedInstanceState.getString("pharmacyAddress");
            routeID = savedInstanceState.getString("routeid");

            if(!routeID.equals("")){
                if((!routeID.toString().equals(getString(R.string.select_no_route))) && (!routeID.toString().equals(getString(R.string.select_route)))){

                    EditText.setText(null);

                    blBackToPreviousActivity = false;
                }
            }

            //btnSubmit1.setText(savedInstanceState.getString("btnSubmit1"));
            check_string = savedInstanceState.getString("btnSubmit1");
        }

        if(blPhoneRotate){
            if(!blBackToPreviousActivity) {
                final Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                sRoutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                        String mIndex = sRoutes.getSelectedItem().toString();

                        if (mIndex.contains(getString(R.string.select_route))) {
                            // do nothing
                        } else {
                            int myIndex = sRoutes.getSelectedItemPosition();
                            String routeid = routekeyArray.get(myIndex);
                            Log.d("routeid", routeid);

                            if(routeID!=routeid){
                                // back from packages scanning, load Addresses if I select another route
                                routeID = routeid;

                                if (routeList.size() > 0) {

                                    blRouteSelected = true;
                                    loadMapByPharmacyAddress(routeid, pharmacyId);
                                    new LoadListRouteAddress(routeid, pharmacyId).execute();
                                }
                            }else{
                                // if same route selected, don't load again addresses from ws, load addresses from local
                                loadMapByPharmacyAddress(routeid, pharmacyId);
                                ListAdapter adapter = adapter = new zNewOrderAdapter(zNewOrderActivityOLD.this, R.layout.activity_routes_listview_items, addressList);
                                lv.setAdapter(adapter);
                            }

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        }

        EditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                String bc = EditText.getText().toString();

                // strip control chars
                bc = bc.replace("*", "");
                bc = bc.replace("\n", "");
                bc = bc.replace("\r", "");

                    // if button scan location, load routes
                    //if (btnSubmit1.getText().toString().equals(getString(R.string.select_action_text1))) {
                    if (check_string.equals(getString(R.string.select_action_text1))) {
                        if(bc.contains(locationPrefix)){
                            // if prefix
                            if(!locationPrefix.equals("")){
                                   // String[] bc_separated = bc.split(locationPrefix);
                                   //String prefix = bc_separated[0];
                                   // bc = bc_separated[1];
                                     bc = bc.replace(locationPrefix, "");
                                    if (!blRouteSelected) {
                                        if (!blLoadingRoutes) {
                                            if (!blRouteSelectedFromPreviousState) {
                                                new LoadListRoute(bc, pharmacyId).execute();
                                            }
                                        }
                                    }
                            }else{
                                if(bc.length() == Integer.valueOf(locationLength)){
                                    if (!blRouteSelected) {
                                        if (!blLoadingRoutes) {
                                            if (!blRouteSelectedFromPreviousState) {
                                                new LoadListRoute(bc, pharmacyId).execute();
                                            }
                                        }
                                    }
                                }
                            }

                        }else{
                            if(bc.length() == Integer.valueOf(locationLength)){
                                if (!blRouteSelected) {
                                    if (!blLoadingRoutes) {
                                        if (!blRouteSelectedFromPreviousState) {
                                            new LoadListRoute(bc, pharmacyId).execute();
                                        }
                                    }
                                }
                            }
                        }
                        // if button scan package, load packages
                    } else {
                        if(bc.length() == Integer.valueOf(packageLength)){
                            new CheckBarcodeIfExist(bc).execute();
                        }

                    }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });




        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if(blLoadOnceLoadPharmacyAddress){
            new LoadPharmacyLocation(pharmacyId).execute();
            blLoadOnceLoadPharmacyAddress = false;
        }


    }

    @Override
    public void onBackPressed() {
        if (blBackToPreviousActivity) {
            super.onBackPressed();
            return;
        }

        if(blBackToPreviousActivity == false){

            blRouteSelected = false;
            blRouteSelectedFromPreviousState = true;
            new LoadPackageOfRoute(routeID, pharmacyId).execute();

        }else if(blBackToPreviousActivity == true){
            // go to previous activity (select action activity)
            finish();
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {

            String UPCScanned = scanResult.getContents();
            if (UPCScanned != null)
            {
                Log.d("barcode scanned", UPCScanned);

                EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                EditText.setText(UPCScanned);

            }

        }
    }

    // onSaveInstanceState
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        blPhoneRotate = true;
        savedInstanceState.putBoolean("blPhoneRotate", blPhoneRotate);
        blRouteSelected = false;
        savedInstanceState.putBoolean("blRouteSelected", blRouteSelected);
        blLoadOnceLoadPharmacyAddress = false;
        savedInstanceState.putBoolean("blLoadOnceLoadPharmacyAddress", blLoadOnceLoadPharmacyAddress);
        savedInstanceState.putBoolean("backToPreviousActivity", blBackToPreviousActivity);
        savedInstanceState.putSerializable("addressList", addressList);
        savedInstanceState.putString("pharmacyAddress", pharmacyAddress);
        savedInstanceState.putString("btnSubmit1", check_string);

        savedInstanceState.putSerializable("routeList", routeList);
        savedInstanceState.putSerializable("packageList", packageList);
        savedInstanceState.putStringArrayList("routekeyArray", routekeyArray);
        savedInstanceState.putStringArrayList("packagesOfRouteSelectedArray", packagesBarcodeOfRouteSelectedArray);
        savedInstanceState.putStringArrayList("packagesUniqueIdOfRouteSelectedArray", packagesUniqueIdOfRouteSelectedArray);
        savedInstanceState.putString("routeid", routeID);

        super.onSaveInstanceState(savedInstanceState);
    }

    // onRestoreInstanceState
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);

            final ArrayAdapter<String> rot_adapter = new ArrayAdapter<String>(zNewOrderActivityOLD.this, android.R.layout.simple_spinner_item, routekeyArray);
            rot_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sRoutes.setAdapter(rot_adapter);
            int spinnerPosition = rot_adapter.getPosition(routeID);
            sRoutes.setSelection(spinnerPosition);

        if(check_string.equals(getString(R.string.select_action_text3))){
            sRoutes.setEnabled(false);
        }

        blPhoneRotate = false;
        savedInstanceState.putBoolean("blPhoneRotate", blPhoneRotate);
        blRouteSelected = false;
        savedInstanceState.putBoolean("blRouteSelected", blRouteSelected);
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListRoute extends AsyncTask<String, String, String> {

        public String location, pharmacyId;

        public LoadListRoute(String location, String pharmacyId){
            this.location = location;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            blLoadingRoutes = true;

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            routeList = new ArrayList<HashMap<String, String>>();

            List<cRoutes> myRoute = MyProvider.getRoutes(location, pharmacyId, "", "");
            for(cRoutes entry : myRoute)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("datetime",  entry.datetime);
                map.put("description", entry.description);
                map.put("algorithm",  entry.algorithm);
                map.put("user",  entry.user);
                map.put("location",  entry.location);

                // adding HashList to ArrayList
                routeList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    List<String> routeArray =  new ArrayList<String>();

                    routekeyArray =  new ArrayList<String>();

                    if(routeList.size()>0){
                        if(routeList.size()==1){

                        }else{
                            routeArray.add(0, getString(R.string.select_route));
                            routekeyArray.add(0, getString(R.string.select_route));
                        }
                    }else{
                        routeArray.add(0, getString(R.string.select_no_route));
                        routekeyArray.add(0, getString(R.string.select_no_route));
                    }

                    for (HashMap<String, String> map : routeList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "id") routekeyArray.add(entry.getValue());
                            if (entry.getKey() == "location") routeArray.add(entry.getValue());
                        }

                    //final ArrayAdapter<String> rot_adapter = new ArrayAdapter<String>(NewOrderActivity.this, android.R.layout.simple_spinner_item, routekeyArray);
                    //rot_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    final Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                    //sRoutes.setAdapter(rot_adapter);

                    SpinnerAdapter route_adapter = new SpinnerAdapter(
                            zNewOrderActivityOLD.this,
                            R.layout.spinner_adapter,
                            routekeyArray
                    );
                    sRoutes.setAdapter(route_adapter);

                    sRoutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                            String mIndex = sRoutes.getSelectedItem().toString();

                            if(mIndex.contains(getString(R.string.select_route))){
                                // do nothing
                            }else{
                                int myIndex = sRoutes.getSelectedItemPosition();
                                String routeid = routekeyArray.get(myIndex);
                                Log.d("routeid",routeid);
                                routeID = routeid;

                                if(routeList.size()>0){

                                    blRouteSelected = true;
                                    loadMapByPharmacyAddress(routeid, pharmacyId);
                                    new LoadListRouteAddress(routeid, pharmacyId).execute();
                                }

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
            });

            blLoadingRoutes = false;

            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListRouteAddress extends AsyncTask<String, String, String> {

        public String routeid, pharmacyId;

        public LoadListRouteAddress(String routeid, String pharmacyId){
            this.routeid = routeid;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            // load data from provider
            addressList = new ArrayList<HashMap<String, String>>();
            if(!blUpdateStatusPackage){
                packagesBarcodeOfRouteSelectedArray = new ArrayList<String>();
                packagesUniqueIdOfRouteSelectedArray = new ArrayList<String>();
            }

            char letter='B';

            List<cRouteAddress> myAddress = MyProvider.getRouteAddress(routeid, pharmacyId);
            for(cRouteAddress entry : myAddress)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("customer_script",  entry.customer_script);
                map.put("customer_name",  entry.customer_name);
                map.put("customer_birthdate", entry.customer_birthdate);
                map.put("customer_sex",  entry.customer_sex);
                map.put("customer_street", entry.customer_street +" "+ entry.customer_house +" , "+ entry.customer_zipcode +" , "+entry.customer_city +" , "+ entry.customer_country);
                map.put("customer_house",  entry.customer_house);
                map.put("customer_zipcode", entry.customer_zipcode);
                map.put("customer_city", entry.customer_city);
                map.put("customer_country",  entry.customer_country);
                map.put("longitude", entry.longitude);
                map.put("latitude", entry.latitude);
                map.put("distance", entry.distance);
                map.put("time", entry.time);
                map.put("letter", String.valueOf(letter++));

                int unscanned_packages = 0;
                int scanned_packages = 0;
                int total_packages = 0;
                List<cPackages> myPackageAddress = MyProvider.getAllPackages(routeid, entry.id, pharmacyId);
                for(final cPackages entry1 : myPackageAddress)
                {
                    // nr of total packages and scanned packages and unscanned packages
                        total_packages = total_packages + 1;

                    if(entry1.package_status_description.equals("In Transport")){
                        scanned_packages = scanned_packages + 1;
                    }else if(entry1.package_status_description.equals("Loaded for delivery")){
                        unscanned_packages = unscanned_packages + 1;
                    }

                    if(total_packages > 1){
                        // barcode package
                        map.put("package_barcode_priority", entry1.package_barcode);
                    }else{
                        // barcode package
                        map.put("package_barcode", entry1.package_barcode);
                    }

                    // finish update scan packages, in special with same barcode
                    if(!blUpdateStatusPackage){
                        if(entry1.package_status_description.equals("Loaded for delivery")){
                            packagesBarcodeOfRouteSelectedArray.add(entry1.package_barcode);
                            packagesUniqueIdOfRouteSelectedArray.add(entry1.packageid);
                        }
                    }
                }

                map.put("unscanned_packages",  String.valueOf(unscanned_packages));
                map.put("scanned_packages",  String.valueOf(scanned_packages));
                map.put("total_packages",  String.valueOf(total_packages));

                // adding HashList to ArrayList
                addressList.add(map);
            }

            // add last address = pharmacy
            // add last address = pharmacy
            if(addressList.size() > 0){
                //final cPharmacyAddress pharmacy = MyProvider.getPharmacyAddress(pharmacyId);
                final cRoute pharmacy = MyProvider.getRouteReport(routeid, pharmacyId);
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id",  pharmacy.id);
                map.put("customer_script",  "Pharmacy");
                map.put("customer_street", pharmacyAddress);
                map.put("letter", String.valueOf(letter++));
                map.put("status_description",  "Pharmacy");
                map.put("unscanned_packages",  "");
                map.put("scanned_packages",  "");
                map.put("total_packages",  "");
                map.put("package_barcode", "Pharmacy");
                map.put("distance", pharmacy.return_distance);
                map.put("time", pharmacy.return_time); // seconds

                addressList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {

                    ListAdapter adapter = null;

                    adapter = new zNewOrderAdapter(zNewOrderActivityOLD.this, R.layout.activity_new_order_listview_items, addressList);
                    lv.setAdapter(adapter);

                    distance = (TextView)findViewById(R.id.total_distance);
                    time = (TextView)findViewById(R.id.total_time);

                    // total distance
                    int myNum_distance = 0;
                    int total_distance = 0;
                    for (HashMap<String, String> map : addressList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "distance")
                            {
                                try {
                                    myNum_distance = Integer.parseInt(entry.getValue().toString());
                                    entry.setValue(MyTimeUtils.getFormattedKilometers(myNum_distance));

                                } catch(NumberFormatException nfe) {
                                    myNum_distance = 0;
                                }
                                total_distance = total_distance + myNum_distance;
                            }
                        }

                    //distance.setText(String.valueOf(total_distance) + " miles");
                    distance.setText(MyTimeUtils.getFormattedKilometers(total_distance));

                    // total_time
                    int myNum_time = 0;
                    int total_time = 0;
                    for (HashMap<String, String> map : addressList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "time")
                            {
                                try {
                                    myNum_time = Integer.parseInt(entry.getValue().toString());
                                    entry.setValue(MyTimeUtils.getFormattedHourMinutes(myNum_time));

                                } catch(NumberFormatException nfe) {
                                    myNum_time = 0;
                                }
                                total_time = total_time + myNum_time;
                            }
                        }

                    //time.setText(String.valueOf(total_time) + " min");
                    time.setText(MyTimeUtils.getFormattedHourMinutes(total_time));


                    if((!routeid.toString().equals(getString(R.string.select_no_route))) && (!routeid.toString().equals(getString(R.string.select_route)))){
                        // change button to scan package after scan order
                        check_string = getString(R.string.select_action_text3);

                        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                        EditText.setText(null);

                        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                        sRoutes.setEnabled(false);

                        blBackToPreviousActivity = false;
                    }

                    // finished updating status package
                    blUpdateStatusPackage = false;

                }
            });

            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadPackageOfRoute extends AsyncTask<String, String, String> {

        public String routeid, pharmacyId;

        public LoadPackageOfRoute(String routeid, String pharmacyId){
            this.routeid = routeid;
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            packageList = new ArrayList<HashMap<String, String>>();

            List<cPackageRoute> myPackage = MyProvider.getPackagesOfRoute(routeid, "", pharmacyId);
            for(cPackageRoute entry : myPackage)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("scriptid",  entry.scriptid);
                map.put("packageid",  entry.package_barcode);
                map.put("addressid", entry.addressid);
                map.put("package_status",  entry.package_status);
                map.put("status_description",  entry.status_description);

                // adding HashList to ArrayList
                packageList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    int totalScanned = 0;
                    int totalUnScanned = 0;
                    String myRow = "";
                    for (HashMap<String, String> map : packageList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "status_description")
                            {
                                try {
                                    myRow = entry.getValue().toString();
                                    String row = String.valueOf(myRow);

                                    if(!row.contains("In Transport")){
                                        totalUnScanned++;
                                    }else{
                                        totalScanned++;
                                    }
                                } catch(NumberFormatException nfe) {

                                }
                            }
                        }



                    final int totalScanned1 = totalScanned;

                    // if are all packages scanned , update route status
                    if(totalUnScanned == 0) {
                        // alert - start route?
                        new AlertDialog.Builder(zNewOrderActivityOLD.this)
                                .setTitle("Route")
                                .setMessage("Start route?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        new UpdateRouteStatus(pharmacyId, routeID, "3").execute();
                                        Globals.setValue("start_route", routeID);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        check_string = getString(R.string.select_action_text1);
                                        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                                        EditText.setText(null);
                                        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                                        sRoutes.setEnabled(true);
                                        blBackToPreviousActivity = true;
                                        blRouteSelectedFromPreviousState = false;

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }else {
                        // alert - finished with collecting packages?
                        new AlertDialog.Builder(zNewOrderActivityOLD.this)
                                .setTitle("Packages")
                                .setMessage("Not all packages are scanned, do you when to finish \n" +
                                        "collecting and drive this route?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        // if at least 1 package scanned , update route status
                                        if(totalScanned1 > 0){
                                            new UpdateRouteStatus(pharmacyId, routeID, "3").execute();
                                        }else{
                                            /*
                                            check_string = getString(R.string.select_action_text1);
                                            EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                                            EditText.setText(null);
                                            Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                                            sRoutes.setEnabled(true);
                                            blBackToPreviousActivity = true;
                                            blRouteSelectedFromPreviousState = false;*/
                                            finish();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
            });

            hideProgressDialog();
        }
    }

    class CheckBarcodeIfExist extends AsyncTask<String, String, Boolean> {

        String barcodePackage;
        int x;

        public CheckBarcodeIfExist( String barcodePackage){
            this.barcodePackage = barcodePackage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        protected Boolean doInBackground(String... args) {

            boolean packageExits = false;
            for (int i = 0; i < packagesBarcodeOfRouteSelectedArray.size(); i++) {
                String bcFromArray = packagesBarcodeOfRouteSelectedArray.get(i).toString();
                if(!barcodePackage.equals("")){
                    if(bcFromArray.equals(barcodePackage)){
                        // update package as scanned
                        x = i;
                        packageExits = true;
                    }else{
                        // barcode scanned doesn't belong to this route
                    }
                }else{
                    break;
                }

            }

            return packageExits;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            if(result){
                int y = x;
                String bcUniqueIdFromArray = packagesUniqueIdOfRouteSelectedArray.get(y).toString();
                blUpdateStatusPackage = true;
                packagesBarcodeOfRouteSelectedArray.remove(y);
                packagesUniqueIdOfRouteSelectedArray.remove(y);
                new UpdatePackageStatus(pharmacyId, bcUniqueIdFromArray, "3").execute();
            }else{
                if(barcodePackage.equals("")){

                }else{
                    if(packagesBarcodeOfRouteSelectedArray.size() == 0){
                        Toast.makeText(zNewOrderActivityOLD.this, "No more packages to be scanned!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(zNewOrderActivityOLD.this, "No package found on this route with barcode " + barcodePackage, Toast.LENGTH_SHORT).show();
                    }
                    EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                    EditText.setText(null);
                }

            }

            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdatePackageStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String packageId = "";
        String statusId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdatePackageStatus(String pharmacyId, String packageId, String statusId){
            this.pharmacyId = pharmacyId;
            this.packageId = packageId;
            this.statusId = statusId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updatePackageStatus(pharmacyId, packageId, statusId, 0.0, 0.0);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Package Status", "false");
                        Toast.makeText(zNewOrderActivityOLD.this, "Package didn't update", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Log.d("Update Package Status", "true");
                        new LoadListRouteAddress(routeID, pharmacyId).execute();
                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status route  */
    private class UpdateRouteStatus extends AsyncTask<String, String, Boolean>{
        String pharmacyId = "";
        String routeId = "";
        String statusId = "";

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        public UpdateRouteStatus(String pharmacyId, String routeId, String statusId){
            this.pharmacyId = pharmacyId;
            this.routeId = routeId;
            this.statusId = statusId;
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.updateRouteStatus(pharmacyId, routeId, statusId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Route Status", "false");
                        Toast.makeText(zNewOrderActivityOLD.this, "Route didn't update", Toast.LENGTH_SHORT).show();

                        /*
                        check_string = getString(R.string.select_action_text1);
                        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                        EditText.setText(null);
                        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                        sRoutes.setEnabled(true);
                        blBackToPreviousActivity = true;
                        blRouteSelectedFromPreviousState = false;*/
                        finish();
                    }
                    else
                    {
                        Log.d("Update Route Status", "true");

                        /*
                        check_string = getString(R.string.select_action_text1);
                        EditText EditText = (EditText) findViewById(R.id.editText_barcode);
                        EditText.setText(null);
                        Spinner sRoutes = (Spinner) findViewById(R.id.spinnerRoute);
                        sRoutes.setEnabled(true);
                        blBackToPreviousActivity = true;
                        blRouteSelectedFromPreviousState = false;*/
                        String start_route = Globals.getValue("start_route");
                        if(start_route != ""){
                            Intent i = new Intent(zNewOrderActivityOLD.this, MyRoutesActivity.class);
                            finish();
                            startActivity(i);
                        }else{
                            finish();
                        }

                    }
                }
            });
            hideProgressDialog();
        }
    }

    /**
     * Background Async Task to Load Address Item by making HTTP Request
     * */
    class LoadPharmacyLocation extends AsyncTask<String, String, String> {

        String pharmacyId;

        public LoadPharmacyLocation(String pharmacyId){
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            final cPharmacyAddress address = MyProvider.getPharmacyAddress(pharmacyId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pharmacyAddress = address.address1 + "  " + address.address_house + " , " + address.address_zipcode + " , " + address.address_city + " , " + address.address_country;
                }
            });
            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (zNewOrderActivityOLD.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            // Dismiss the progress dialog
            hideProgressDialog();
        }
    }

    public void loadMapByPharmacyAddress(String routeid, String pharmacy){

        String startpoint = pharmacyAddress;

        //    Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
            Log.d("startpoint", startpoint);


        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        String map = "bing_map_route_byid_copy.php"; // is google map
        double size_tablet = MyTimeUtils.tabletSize(zNewOrderActivityOLD.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 1150, 1050, 0.0, 0.0, 0.0, 0.0 ,zNewOrderActivityOLD.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 850, 650, 0.0, 0.0, 0.0, 0.0, zNewOrderActivityOLD.this));

        }else{
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "1", routeid, pharmacy, 400, 300 , 0.0, 0.0, 0.0, 0.0, zNewOrderActivityOLD.this));
        }

    }

    public void loadMapByGPSLocation(String routeid, String pharmacy){

         //Get GPS coordinates
        GPSTracker gps = new GPSTracker(zNewOrderActivityOLD.this);
        String startpoint = "";
        double start_latitude = gps.getLatitude();
        double start_longitude = gps.getLongitude();
        //double end_latitude = Double.valueOf(endLatitude);
        //double end_longitude = Double.valueOf(endLongitude);

        // check if GPS enabled
        if(gps.canGetLocation()){

            //start_latitude = gps.getLatitude();
            //start_longitude = gps.getLongitude();
            //startpoint = gps.getAddress();

          //  Toast.makeText(getApplicationContext(), "Your Location is: \n" + startpoint, Toast.LENGTH_LONG).show();
            Log.d("startpoint", startpoint);

            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            // gps.displayLocationSettingsRequest(AddressDetailActivity.this);
            gps.showSettingsAlert();
        }

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //webView.loadUrl("http://www.sharethatdata.com/devlogistics-ws/bing_map_driving_route.php");
        String map = "bing_map_route_byid_copy.php"; // is google map
        double size_tablet = MyTimeUtils.tabletSize(zNewOrderActivityOLD.this);
        if (size_tablet > 9) {
            //Device is a 10" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "", routeid, pharmacy, 1150, 1050 , 0.0, 0.0, 0.0, 0.0, zNewOrderActivityOLD.this));

        }else if (size_tablet > 6) {
            //Device is a 7" tablet
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "", routeid, pharmacy, 850, 650 , 0.0, 0.0, 0.0, 0.0, zNewOrderActivityOLD.this));

        }else{
            webView.loadUrl(MyProvider.driving_route(map, startpoint, "", routeid, pharmacy, 400, 300 , 0.0, 0.0, 0.0, 0.0, zNewOrderActivityOLD.this));
        }

    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(zNewOrderActivityOLD.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
