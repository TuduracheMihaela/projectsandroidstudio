package com.sharethatdata.digiroute.DragDropListview.draglistview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.AddressDetailActivity;
import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by miha on 11/17/2017.
 */

public class ItemAdapter extends DragItemAdapter<HashMap<String, String>, ItemAdapter.ViewHolder> {

    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;
    private final Activity context;
    private String idRoute;

    Typeface font1 ;
    Typeface font2 ;

    public ItemAdapter(Activity context, ArrayList<HashMap<String, String>> list, int layoutId, int grabHandleId, boolean dragOnLongPress, String idRoute) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        this.context=context;
        this.idRoute=idRoute;
        font1 = Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf");
        font2 = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Regular.otf");
        setItemList(list);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        HashMap<String, String> hashMap_item = mItemList.get(position);

        String id_address = Html.fromHtml(hashMap_item.get("id")).toString();
        String customer_name_text = Html.fromHtml(hashMap_item.get("customer_name")).toString();
        String customer_script_text = Html.fromHtml(hashMap_item.get("customer_script")).toString();
        String customer_total_packages_text = context.getString(R.string.textView_total_packages)+" "+Html.fromHtml(hashMap_item.get("total_packages"));
        String customer_scanned_packages_text = context.getString(R.string.textView_total_scanned_packages)+ " " + Html.fromHtml(hashMap_item.get("scanned_packages"));
        String letter_text = Html.fromHtml(hashMap_item.get("letter")).toString();
        String customer_distance_text = Html.fromHtml(hashMap_item.get("distance")).toString();
        String customer_time_text = Html.fromHtml(hashMap_item.get("time")).toString();

        String customer_street_text = hashMap_item.get("customer_street");
        String customer_house_text = hashMap_item.get("customer_house");
        String customer_zipcode_text = hashMap_item.get("customer_zipcode");
        String customer_city_text = hashMap_item.get("customer_city");
        String customer_country_text = hashMap_item.get("customer_country");

        List<String> slist = new ArrayList<String> ();
        slist.add(customer_street_text);
        slist.add(customer_house_text);
        slist.add(customer_zipcode_text);
        slist.add(customer_city_text);
        slist.add(customer_country_text);

        StringBuilder rString = new StringBuilder();
        int count = 0;
        for (String line : slist) {
            ++count;
            if (!line.equals("")) {
                if(count != slist.size()){
                    rString.append(line + ", ");
                }else{
                    rString.append(line);
                }
            }
        }

        holder.id.setText(id_address);
        holder.customer_address.setText(rString.toString());

        if(!customer_name_text.equals("")){
            holder.customer_name.setText(customer_name_text.toString());
        }else{
            holder.customer_name.setVisibility(View.GONE);
            holder.customer_name_label.setVisibility(View.GONE);
        }


        holder.customer_script.setText(customer_script_text);
        holder.customer_total_packages.setText(customer_total_packages_text);
        holder.customer_scanned_packages.setText(customer_scanned_packages_text);
        holder.letter.setText(letter_text);
        holder.customer_distance.setText(customer_distance_text);
        holder.customer_time.setText(customer_time_text);

        holder.customer_barcode.setTypeface(font1);
        holder.customer_barcode_label.setTypeface(font1);
        holder.customer_name.setTypeface(font2);
        holder.customer_name_label.setTypeface(font2);
        holder.customer_address.setTypeface(font2);
        holder.customer_total_packages.setTypeface(font2);
        holder.customer_scanned_packages.setTypeface(font2);
        holder.customer_distance.setTypeface(font1);
        holder.customer_distance_label.setTypeface(font2);
        holder.customer_time.setTypeface(font1);
        holder.customer_time_label.setTypeface(font2);

        String myRow = "";
        for (Map.Entry<String, String> entry : hashMap_item.entrySet())
        {
            if(entry.getKey() == "status_id_priority")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    // 1 = Loaded for delivery ; 3 = In Transport
                    if((row.equals("1")) || (row.equals("3"))){
                        // white
                        holder.content.setBackgroundResource(R.color.white1);
                        break;
                    }
                } catch(NumberFormatException nfe) {}
            }
            else if (entry.getKey() == "status_id")
            {
                try {
                    myRow = entry.getValue().toString();
                    String row = String.valueOf(myRow);

                    // 1 = Loaded for delivery ; 3 = In Transport
                    if((row.equals("1")) || (row.equals("3")) || (row.equals("Pharmacy"))){
                        // white
                        holder.content.setBackgroundResource(R.color.white1);

                        // 6 = Not at home
                    }else if(row.equals("6")){
                        // red
                        holder.content.setBackgroundResource(R.color.red_transparent80);

                    // 5 = Delivered
                    }else if(row.equals("5")){
                        // green
                        holder.content.setBackgroundResource(R.color.green_transparent80);


                    }else{
                        // orange - other status
                        // 7 = Denied by customer
                        holder.content.setBackgroundResource(R.color.orange_transparent80);
                    }
                } catch(NumberFormatException nfe) {}
            }
        }

        String myRow1 = "";
        for (Map.Entry<String, String> entry : hashMap_item.entrySet())
        {
            if (entry.getKey() == "customer_script")
            {
                try {
                    myRow1 = entry.getValue().toString();
                    String row = String.valueOf(myRow1);
                    //    if((row.equals("Pharmacy"))){
                    // white
                    holder.customer_total_packages.setVisibility(View.GONE);
                    holder.customer_scanned_packages.setVisibility(View.GONE);
                    //    }
                } catch(NumberFormatException nfe) {}
            }
        }

        for (Map.Entry<String, String> entry : hashMap_item.entrySet())
        {
            if (entry.getKey() == "package_barcode_priority")
            {
                try {
                    holder.customer_barcode.setText((Html.fromHtml(hashMap_item.get("package_barcode")))+ " and " + "more.." );
                    break;
                } catch(NumberFormatException nfe) {}
            }
            else if (entry.getKey() == "package_barcode")
            {
                try {
                    holder.customer_barcode.setText(Html.fromHtml(hashMap_item.get("package_barcode")));
                } catch(NumberFormatException nfe) {}
            }
        }


        holder.itemView.setTag(mItemList.get(position));
    }

    @Override
    public long getUniqueItemId(int position) {
        HashMap<String, String> item = mItemList.get(position);
        String id_text = Html.fromHtml(item.get("id")).toString();
        return Long.valueOf(id_text);
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {
        public TextView id, customer_name, customer_script, customer_barcode, customer_total_packages, customer_scanned_packages, customer_address, letter,
                customer_distance, customer_time;
        public TextView customer_barcode_label, customer_name_label, customer_distance_label, customer_time_label;
        public LinearLayout content;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            id = (TextView)itemView.findViewById(R.id.textViewID);
            customer_name = (TextView)itemView.findViewById(R.id.customerName);
            customer_script = (TextView)itemView.findViewById(R.id.customerScript);
            customer_barcode = (TextView)itemView.findViewById(R.id.customerBarcode);
            customer_total_packages = (TextView)itemView.findViewById(R.id.customerTotalPackages);
            customer_scanned_packages = (TextView)itemView.findViewById(R.id.customerScannedPackages);
            customer_address = (TextView)itemView.findViewById(R.id.customerStreet);
            letter = (TextView)itemView.findViewById(R.id.textViewLetterAddress);
            customer_distance = (TextView)itemView.findViewById(R.id.customerDistance);
            customer_time = (TextView)itemView.findViewById(R.id.customerTime);
            content = (LinearLayout)itemView.findViewById(R.id.item_layout);

            customer_barcode_label = (TextView) itemView.findViewById(R.id.customerBarcodeLabel);
            customer_name_label = (TextView) itemView.findViewById(R.id.customerNameLabel);
            customer_distance_label = (TextView) itemView.findViewById(R.id.customerDistanceLabel);
            customer_time_label = (TextView) itemView.findViewById(R.id.customerTimeLabel);
        }

        @Override
        public void onItemClicked(View view) {
            //Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();

            String idAddress = "";

            HashMap<String, String> adapterItem = (HashMap<String, String>) view.getTag();
            for (Map.Entry<String, String> entry : adapterItem.entrySet()) {
                if (entry.getKey() == "id") {
                    idAddress = entry.getValue().toString();
                }
            }



            String idScript = ((TextView) view.findViewById(R.id.customerScript)).getText().toString();
            String barcode = ((TextView) view.findViewById(R.id.customerBarcode)).getText().toString();
            String distance = ((TextView) view.findViewById(R.id.customerDistance)).getText().toString();
            String time = ((TextView) view.findViewById(R.id.customerTime)).getText().toString();

            // start detail activity
            Intent intent = new Intent(context, AddressDetailActivity.class);
            intent.putExtra("idAddress", idAddress);
            intent.putExtra("idRoute", idRoute);
            intent.putExtra("idScript", idScript);
            intent.putExtra("barcode", barcode);
            intent.putExtra("distance", distance);
            intent.putExtra("time", time);
            context.startActivity(intent);
        }

        @Override
        public boolean onItemLongClicked(View view) {
            //Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
