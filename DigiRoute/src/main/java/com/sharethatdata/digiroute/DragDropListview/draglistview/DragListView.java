/*
 * Copyright 2014 Magnus Woxblom
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sharethatdata.digiroute.DragDropListview.draglistview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sharethatdata.digiroute.DragDropListview.draglistview.swipe.ListSwipeHelper;
import com.sharethatdata.digiroute.R;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import static com.sharethatdata.digiroute.DragDropListview.draglistview.DragListView.UndoMode.SINGLE_UNDO;

public class DragListView extends FrameLayout {

    public interface DragListListener {
        void onItemDragStarted(int position);

        void onItemDragging(int itemPosition, float x, float y);

        void onItemDragEnded(int fromPosition, int toPosition);
    }

    public static abstract class DragListListenerAdapter implements DragListListener {
        @Override
        public void onItemDragStarted(int position) {
        }

        @Override
        public void onItemDragging(int itemPosition, float x, float y) {
        }

        @Override
        public void onItemDragEnded(int fromPosition, int toPosition) {
        }
    }

    public interface DragListCallback {
        boolean canDragItemAtPosition(int dragPosition);

        boolean canDropItemAtPosition(int dropPosition);
    }

    public static abstract class DragListCallbackAdapter implements DragListCallback {
        @Override
        public boolean canDragItemAtPosition(int dragPosition) {
            return true;
        }

        @Override
        public boolean canDropItemAtPosition(int dropPosition) {
            return true;
        }
    }

    private DragItemRecyclerView mRecyclerView;
    private DragListListener mDragListListener;
    private DragListCallback mDragListCallback;
    private DragItem mDragItem;
    private ListSwipeHelper mSwipeHelper;
    private float mTouchX;
    private float mTouchY;

    public DragListView(Context context) {
        super(context);
    }

    public DragListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DragListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDragItem = new DragItem(getContext());
        mRecyclerView = createRecyclerView();
        mRecyclerView.setDragItem(mDragItem);
        addView(mRecyclerView);
        addView(mDragItem.getDragItemView());
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean retValue = handleTouchEvent(event);
        return retValue || super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean retValue = handleTouchEvent(event);
        return retValue || super.onTouchEvent(event);
    }

    private boolean handleTouchEvent(MotionEvent event) {
        mTouchX = event.getX();
        mTouchY = event.getY();
        if (isDragging()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    mRecyclerView.onDragging(event.getX(), event.getY());
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    mRecyclerView.onDragEnded();
                    break;
            }
            return true;

        }else{
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:

                    if(mTouchBeforeAutoHide && mUndoPopup.isShowing()) {
                        // Send a delayed message to hide popup
                        mHandler.sendMessageDelayed(mHandler.obtainMessage(mDelayedMsgId),
                                mAutoHideDelay);
                    }

                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
            }
        }
        return false;
    }

    private DragItemRecyclerView createRecyclerView() {
        final DragItemRecyclerView recyclerView = (DragItemRecyclerView) LayoutInflater.from(getContext()).inflate(R.layout.drag_item_recycler_view, this, false);
        recyclerView.setMotionEventSplittingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVerticalScrollBarEnabled(false);
        recyclerView.setHorizontalScrollBarEnabled(false);
        recyclerView.setDragItemListener(new DragItemRecyclerView.DragItemListener() {
            private int mDragStartPosition;

            @Override
            public void onDragStarted(int itemPosition, float x, float y) {
                getParent().requestDisallowInterceptTouchEvent(true);
                mDragStartPosition = itemPosition;
                if (mDragListListener != null) {
                    mDragListListener.onItemDragStarted(itemPosition);
                }
            }

            @Override
            public void onDragging(int itemPosition, float x, float y) {
                if (mDragListListener != null) {
                    mDragListListener.onItemDragging(itemPosition, x, y);
                }
            }

            @Override
            public void onDragEnded(int newItemPosition) {
                if (mDragListListener != null) {
                    mDragListListener.onItemDragEnded(mDragStartPosition, newItemPosition);
                }
            }
        });
        recyclerView.setDragItemCallback(new DragItemRecyclerView.DragItemCallback() {
            @Override
            public boolean canDragItemAtPosition(int dragPosition) {
                return mDragListCallback == null || mDragListCallback.canDragItemAtPosition(dragPosition);
            }

            @Override
            public boolean canDropItemAtPosition(int dropPosition) {
                return mDragListCallback == null || mDragListCallback.canDropItemAtPosition(dropPosition);
            }
        });
        return recyclerView;
    }

    public void setSwipeListener(ListSwipeHelper.OnSwipeListener swipeListener) {
        if (mSwipeHelper == null) {
            mSwipeHelper = new ListSwipeHelper(getContext().getApplicationContext(), swipeListener);
        } else {
            mSwipeHelper.setSwipeListener(swipeListener);
        }

        // Always detach first so we don't get double listeners
        mSwipeHelper.detachFromRecyclerView();
        if (swipeListener != null) {
            mSwipeHelper.attachToRecyclerView(mRecyclerView);
        }
    }

    /**
     * Resets the swipe state of all list item views except the one that is passed as an exception view.
     *
     * @param exceptionView This view will not be reset.
     */
    public void resetSwipedViews(View exceptionView) {
        if (mSwipeHelper != null) {
            mSwipeHelper.resetSwipedViews(exceptionView);
        }
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public DragItemAdapter getAdapter() {
        if (mRecyclerView != null) {
            return (DragItemAdapter) mRecyclerView.getAdapter();
        }
        return null;
    }

    public void setAdapter(DragItemAdapter adapter, boolean hasFixedItemSize) {
        mRecyclerView.setHasFixedSize(hasFixedItemSize);
        mRecyclerView.setAdapter(adapter);
        adapter.setDragStartedListener(new DragItemAdapter.DragStartCallback() {
            @Override
            public boolean startDrag(View itemView, long itemId) {
                return mRecyclerView.startDrag(itemView, itemId, mTouchX, mTouchY);
            }

            @Override
            public boolean isDragging() {
                return mRecyclerView.isDragging();
            }
        });
    }

    public void setLayoutManager(RecyclerView.LayoutManager layout) {
        mRecyclerView.setLayoutManager(layout);
    }

    public void setDragListListener(DragListListener listener) {
        mDragListListener = listener;
    }

    public void setDragListCallback(DragListCallback callback) {
        mDragListCallback = callback;
    }

    public boolean isDragEnabled() {
        return mRecyclerView.isDragEnabled();
    }

    public void setDragEnabled(boolean enabled) {
        mRecyclerView.setDragEnabled(enabled);
    }

    public void setCustomDragItem(DragItem dragItem) {
        removeViewAt(1);

        DragItem newDragItem;
        if (dragItem != null) {
            newDragItem = dragItem;
        } else {
            newDragItem = new DragItem(getContext());
        }

        newDragItem.setCanDragHorizontally(mDragItem.canDragHorizontally());
        newDragItem.setSnapToTouch(mDragItem.isSnapToTouch());
        mDragItem = newDragItem;
        mRecyclerView.setDragItem(mDragItem);
        addView(mDragItem.getDragItemView());
    }

    public boolean isDragging() {
        return mRecyclerView.isDragging();
    }

    public void setCanDragHorizontally(boolean canDragHorizontally) {
        mDragItem.setCanDragHorizontally(canDragHorizontally);
    }

    public void setSnapDragItemToTouch(boolean snapToTouch) {
        mDragItem.setSnapToTouch(snapToTouch);
    }

    public void setCanNotDragAboveTopItem(boolean canNotDragAboveTop) {
        mRecyclerView.setCanNotDragAboveTopItem(canNotDragAboveTop);
    }

    public void setCanNotDragBelowBottomItem(boolean canNotDragBelowBottom) {
        mRecyclerView.setCanNotDragBelowBottomItem(canNotDragBelowBottom);
    }

    public void setScrollingEnabled(boolean scrollingEnabled) {
        mRecyclerView.setScrollingEnabled(scrollingEnabled);
    }

    /**
     * Set if items should not reorder automatically when dragging. If reorder is disabled, drop target
     * drawables can be set with {@link #setDropTargetDrawables} which will highlight the current item that
     * will be swapped when dropping. By default items will reorder automatically when dragging.
     *
     * @param disableReorder True if reorder of items should be disabled, false otherwise.
     */
    public void setDisableReorderWhenDragging(boolean disableReorder) {
        mRecyclerView.setDisableReorderWhenDragging(disableReorder);
    }

    /**
     * If {@link #setDisableReorderWhenDragging} has been set to True then a background and/or foreground drawable
     * can be provided to highlight the current item which will be swapped when dropping. These drawables
     * will be drawn as decorations in the RecyclerView and will not interfere with the items own background
     * and foreground drawables.
     *
     * @param backgroundDrawable The background drawable for the item that will be swapped.
     * @param foregroundDrawable The foreground drawable for the item that will be swapped.
     */
    public void setDropTargetDrawables(Drawable backgroundDrawable, Drawable foregroundDrawable) {
        mRecyclerView.setDropTargetDrawables(backgroundDrawable, foregroundDrawable);
    }



    ///////////////////////////////////////////////////////////////////////////////////////////
    // UNDO //

    // Fixed properties
    private DragListView mListView;
    private OnDismissCallback mCallback;

    // Transient properties
    private SortedSet<PendingDismissData> mPendingDismisses = new TreeSet<PendingDismissData>();

    private float mDensity;

    private UndoMode mMode;
    private List<Undoable> mUndoActions;
    private Handler mHandler;

    public PopupWindow mUndoPopup;
    private TextView mUndoText;
    public Button mUndoButton;

    private int mAutoHideDelay = 5000;
    private String mDeleteString = "Item deleted";
    private boolean mTouchBeforeAutoHide = true;

    public int mDelayedMsgId;

    class PendingDismissData implements Comparable<PendingDismissData> {

        public int position;
        //public View view;

        public PendingDismissData(int position) { // , View view
            this.position = position;
            //this.view = view;
        }

        @Override
        public int compareTo(PendingDismissData other) {
            // Sort by descending position
            return other.position - position;
        }
    }

    /**
     * Constructs a new swipe-to-dismiss touch listener for the given list view.
     *
     * @param listView The list view whose items should be dismissable.
     * she would like to dismiss one or more list items.
     */
    public void SwipeUndo(DragListView listView, OnDismissCallback callback, UndoMode mode) {

        if(listView == null) {
            throw new IllegalArgumentException("listview must not be null.");
        }

        mHandler = new HideUndoPopupHandler();
        mListView = listView;
        mCallback = callback;
        mMode = mode;

        mDensity = mListView.getResources().getDisplayMetrics().density;

        // -- Load undo popup --
        LayoutInflater inflater = (LayoutInflater) mListView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.undo_popup, null);
        mUndoButton = (Button)v.findViewById(R.id.undo);
        mUndoButton.setOnClickListener(new UndoHandler());
        mUndoButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // If user tabs "undo" button, reset delay time to remove popup
                mDelayedMsgId++;
                return false;
            }
        });
        mUndoText = (TextView)v.findViewById(R.id.text);

        mUndoPopup = new PopupWindow(v);
        mUndoPopup.setAnimationStyle(R.style.fade_animation);
        // Get scren width in dp and set width respectively
        int xdensity = (int)(mListView.getContext().getResources().getDisplayMetrics().widthPixels / mDensity);
        if(xdensity < 300) {
            mUndoPopup.setWidth((int)(mDensity * 280));
        } else if(xdensity < 350) {
            mUndoPopup.setWidth((int)(mDensity * 300));
        } else if(xdensity < 500) {
            mUndoPopup.setWidth((int)(mDensity * 330));
        } else {
            mUndoPopup.setWidth((int)(mDensity * 450));
        }
        mUndoPopup.setHeight((int)(mDensity * 56));
        // -- END Load undo popup --

        switch(mode) {
            case SINGLE_UNDO:
                mUndoActions = new ArrayList<Undoable>(1);
                break;
            default:
                //mUndoActions = new ArrayList<Undoable>(10);
                break;
        }
    }

    /**
     * Handler used to hide the undo popup after a special delay.
     */
    public class HideUndoPopupHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == mDelayedMsgId) {
                // Call discard on any element
                for(Undoable undo : mUndoActions) {
                    undo.discard();
                }
                mUndoActions.clear();
                mUndoPopup.dismiss();
            }
        }
    }

    /**
     * Takes care of undoing a dismiss. This will be added as a
     * { View.OnClickListener} to the undo button in the undo popup.
     */
    public class UndoHandler implements View.OnClickListener {

        public void onClick(View v) {

            if(!mUndoActions.isEmpty()) {
                switch(mMode) {
                    case SINGLE_UNDO:
                        mUndoActions.get(0).undo();
                        mUndoActions.clear();
                        break;
                }
            }

            // Dismiss dialog or change text
            if(mUndoActions.isEmpty()) {
                mUndoPopup.dismiss();
            }

            mDelayedMsgId++;
        }
    }

    public void performDismiss(final int dismissPosition) { // final View dismissView,
        // Animate the dismissed list item to zero-height and fire the dismiss callback when
        // all dismissed list item animations have completed. This triggers layout on each animation
        // frame; in the future we may want to do something smarter and more performant.

        mPendingDismisses.add(new PendingDismissData(dismissPosition)); // , dismissView

        //final ViewGroup.LayoutParams lp1 = dismissView.getLayoutParams();
        //final int originalHeight = dismissView.getHeight();

        for(PendingDismissData dismiss : mPendingDismisses) {
            if(mMode == UndoMode.SINGLE_UNDO) {
                for(Undoable undoable : mUndoActions) {
                    undoable.discard();
                }
                mUndoActions.clear();
            }
            Undoable undoable = mCallback.onDismiss(mListView, dismiss.position);
            if(undoable != null) {
                mUndoActions.add(undoable);
            }
            mDelayedMsgId++;
        }

        if(!mUndoActions.isEmpty()) {
            mUndoText.setText(mDeleteString);
            mUndoButton.setText("Undo");

            // Show undo popup
            mUndoPopup.showAtLocation(mListView,
                    Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
                    0, (int)(mDensity * 15));

            // Queue the dismiss only if required
            if(!mTouchBeforeAutoHide) {
                // Send a delayed message to hide popup
                mHandler.sendMessageDelayed(mHandler.obtainMessage(mDelayedMsgId),
                        mAutoHideDelay);
            }
        }

        ViewGroup.LayoutParams lp;
        for (PendingDismissData pendingDismiss : mPendingDismisses) {
            // Reset view presentation
            //lp = pendingDismiss.view.getLayoutParams();
            //lp.height = originalHeight;
            //pendingDismiss.view.setLayoutParams(lp);
        }

        mPendingDismisses.clear();
    }

    /**
     * Sets whether another touch on the view is required before the popup counts down to
     * dismiss. By default this is set to true.
     *
     * @param require Whether a touch is required before starting the auto-dismiss timer.
     */
    public void setRequireTouchBeforeDismiss(boolean require) {
        mTouchBeforeAutoHide = require;
    }

    /**
     * Sets the string shown in the undo popup. This will only show if
     * the { Undoable} returned by the OnDismissCallback} returns
     * {@code null} from its { Undoable#getTitle()} method.
     *
     * @param msg The string shown in the undo popup.
     */
    public void setUndoString(String msg) {
        mDeleteString = msg;
    }

    /**
     * Sets the time in milliseconds after which the undo popup automatically
     * disappears.
     *
     * @param delay Delay in milliseconds.
     */
    public void setAutoHideDelay(int delay) {
        mAutoHideDelay = delay;
    }

    /**
     * Discard all stored undos and hide the undo popup dialog.
     */
    public void discardUndo() {
        for(Undoable undoable : mUndoActions) {
            undoable.discard();
        }
        mUndoActions.clear();
        mUndoPopup.dismiss();
    }


    /**
     * The callback interface used by { SwipeDismissListViewTouchListener}
     * to inform its client about a successful dismissal of one or more list
     * item positions.
     */
    public interface OnDismissCallback {

        /**
         * Called when the user has indicated they she would like to dismiss one
         * or more list item positions.
         *
         * @param listView The originating { ListView}.
         * @param position The position of the item to dismiss.
         */
        Undoable onDismiss(DragListView listView, int position);
    }

    /**
     * An implementation of this abstract class must be returned by the
     * { OnDismissCallback#onDismiss(android.widget.ListView, int)} method,
     * if the user should be able to undo that dismiss. If the action will be undone
     * by the user { #undo()} will be called. That method should undo the previous
     * deletion of the item and add it back to the adapter. Read the README file for
     * more details. If you implement the { #getTitle()} method, the undo popup
     * will show an individual title for that item. Otherwise the default title
     * (set via { #setUndoString(java.lang.String)}) will be shown.
     */
    public abstract static class Undoable {

        /**
         * Returns the individual undo message for this item shown in the
         * popup dialog.
         *
         * @return The individual undo message.
         */
        public String getTitle() {
            return null;
        }

        /**
         * Undoes the deletion.
         */
        public abstract void undo();

        /**
         * Will be called when this Undoable won't be able to undo anymore,
         * meaning the undo popup has disappeared from the screen.
         */
        public void discard() { }

    }

    /**
     * Defines the mode a { SwipeDismissList} handles multiple undos.
     */
    public enum UndoMode {
        /**
         * Only give the user the possibility to undo the last action.
         * As soon as another item is deleted, there is no chance to undo
         * the previous deletion.
         */
        SINGLE_UNDO
    };


}
