package com.sharethatdata.digiroute;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.view.MotionEvent;
import android.view.View;
import android.util.AttributeSet;

/**
 * Created by mverbeek on 23-3-2017.
 */

public class DrawingView extends View {

    public int width;
    public  int height;
    public String label_text;
    public String patient_text;
    public String datetime_text;
    private Paint mPaint;
    private Paint textPaint;
    private Paint textPaintPatient;
    private Paint textPaintDateTime;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint;
    private Paint circlePaint;
    private Path circlePath;

    public DrawingView(Context context, AttributeSet attrs){
        super(context, attrs);

        this.setDrawingCacheEnabled(true);

        reset();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath( mPath,  mPaint);
        canvas.drawPath( circlePath,  circlePaint);
        canvas.drawText(label_text, 10, 35, textPaint);
        canvas.drawText(patient_text, 10, canvas.getHeight() - 20, textPaintPatient);
        canvas.drawText(datetime_text, canvas.getWidth() - 280, canvas.getHeight() - 20, textPaintDateTime);

        getParent().requestDisallowInterceptTouchEvent(true);
    }

    public void setImageBitmap(Bitmap newBitmap)
    {
        mBitmap = newBitmap;
        mCanvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);
        this.invalidate();
    }
    public void reset()
    {
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        circlePaint = new Paint();
        circlePath = new Path();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.BLUE);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.MITER);
        circlePaint.setStrokeWidth(4f);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(10);

        textPaint = new Paint();
        textPaint.setColor(getResources().getColor(R.color.grey07));
        textPaint.setTextSize(18);

        textPaintPatient = new Paint();
        textPaintPatient.setColor(getResources().getColor(R.color.grey07));
        textPaintPatient.setTextSize(18);

        textPaintDateTime = new Paint();
        textPaintDateTime.setColor(getResources().getColor(R.color.grey07));
        textPaintDateTime.setTextSize(18);
    }

     public void clear()
    {

        /*mBitmap.eraseColor(Color.TRANSPARENT);
        mCanvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);*/

        //mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        mBitmap = Bitmap.createBitmap(2000,2000 ,Bitmap.Config.ARGB_8888);

        mCanvas = new Canvas(mBitmap);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(10);
        invalidate();

    }

    public Bitmap getBitmap()
    {
        return this.getDrawingCache();
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;

            circlePath.reset();
            circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        circlePath.reset();
        // commit the path to our offscreen
        mCanvas.drawPath(mPath,  mPaint);
        // kill this so we don't double draw
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }
}