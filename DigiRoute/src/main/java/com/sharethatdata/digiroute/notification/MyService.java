package com.sharethatdata.digiroute.notification;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.sharethatdata.digiroute.NewMessageActivity;
import com.sharethatdata.digiroute.R;
import com.sharethatdata.digiroute.Service.MessageService;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class MyService extends Service{

	WSDataProvider MyProvider = null;
	
	NotificationManager notificationManager;
   	NotificationCompat.InboxStyle inboxStyle ;
   	NotificationManagerCompat notificationManagerCompact ;
    NotificationCompat.Builder builder ;
	
	private String user = "";
	private String pass = "";
	private String webservice_url = "";
	private String pharmacyId = "";
	private String ip = "";
	private String android_id = "";
	private String appname = "";
	private String access_key = "";

	 public static final String inputFormat = "yyyy-MM-dd HH:mm";
	 SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.ROOT);
		
	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		 Log.d("TAG", "onBind MyService ");
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
    
		 // Toast.makeText(MyService.this, "Service started!", Toast.LENGTH_SHORT).show();

		  Log.d("TAG", "onStartCommand MyService");

		Toast.makeText(MyService.this, "MyService", Toast.LENGTH_SHORT).show();

   	    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
    	        user = prefUser.getString("USER", ""); 
    	        pass = prefUser.getString("PASS", "");
    	        webservice_url = prefUser.getString("URL", "");

			SharedPreferences prefPharmacy = getSharedPreferences("PREF_Pharmacy", 0);
				pharmacyId = prefPharmacy.getString("Pharmacy","");

            SharedPreferences prefDevice = getSharedPreferences("PREF_DEVICE", 0);
                ip = prefDevice.getString("DEVICE_IP","");
                android_id = prefDevice.getString("DEVICE_ID","");
                appname = prefDevice.getString("DEVICE_NAME","");

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				access_key = prefs.getString("access_key", "");
    	       
   	    	Log.d("TAG", "user == null: " + user);
   	    	Log.d("TAG", "pass == null: " + pass);
   	    	Log.d("TAG", "webservice_url == null: " + webservice_url);
   	    	
   	    	MyProvider = new WSDataProvider(user, pass);

	        new LoadListMessages().execute();


		// If we get killed, after returning from here, restart
	  return Service.START_NOT_STICKY;
	  
  }
	
	@Override
    public void onCreate() 
    {
		Log.d("TAG", "onCreate MyService");
		Toast.makeText(MyService.this, "onCreate", Toast.LENGTH_SHORT).show();

       	super.onCreate();

    }

   /*@Override
   public void onStart(Intent intent, int startId)
   {
	   Log.d("TAG", "onStart MyService");
	   Toast.makeText(MyService.this, "onStart", Toast.LENGTH_SHORT).show();

       super.onStart(intent, startId);

    }*/
 
    @Override
    public void onDestroy() 
    {
		Log.d("TAG", "onDestroy MyService");
		Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();

        super.onDestroy();

    }


	/*class LoadMessage extends AsyncTask<String, String, Boolean> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		*//**
		 * getting items from url
		 *//*
		protected Boolean doInBackground(String... args) {

			MyProvider = new WSDataProvider(user, pass);
			MyProvider.SetWebServiceUrl(webservice_url);

			cMessage message = MyProvider.getNewMessage(pharmacyId);

			boolean suc = false;
			if (message.id > 0)
			{
				suc = true;
			}

			return suc;
		}

		*//**
		 * After completing background task Dismiss the progress dialog
		 **//*
		protected void onPostExecute(Boolean result) {

			if (result == true) {

				// load new message activity
				Intent intent = new Intent(getApplicationContext(), NewMessageActivity.class);
				startActivity(intent);
			}

		}
	}*/

	public void showMessageNotification(String message_from, String message){

			Intent intent = new Intent(this, NewMessageActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			//PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);

			PendingIntent pendingIntent = null;
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
				pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_MUTABLE);
			}
			else
			{
				pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
			}

			notificationManagerCompact = NotificationManagerCompat.from(MyService.this);
			builder = new NotificationCompat.Builder(MyService.this);

			builder.setSmallIcon(R.drawable.launcher)
					.setContentTitle(getString(R.string.notification_title_message_from) + " " + message_from)
					.setContentText(message)
					.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
					.setTicker("Notification!")
					.setWhen(System.currentTimeMillis())
					.setContentIntent(pendingIntent)
					.setAutoCancel(true)
					.setVibrate(new long[] {0,100,0,100});

			// Sets an ID for the notification
			int mNotificationId = 1;
			// Gets an instance of the NotificationManager service
			NotificationManager mNotifyMgr =
					(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			// Builds the notification and issues it.
			mNotifyMgr.notify(mNotificationId, builder.build());
	}

    /**
	 * Background Async Task to Load all messages by making HTTP Request
  	 * */
	class LoadListMessages extends AsyncTask<String, String, Boolean> {

		String message_id = "";
		String message_from_who = "";
		String message_text = "";

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
     }

	    /**
	     * getting items from url
	     * */
	    protected Boolean doInBackground(String... args) {

			 MyProvider = new WSDataProvider(user, pass);
			 MyProvider.SetWebServiceUrl(webservice_url);
			cMessage message = MyProvider.getNewMessageNotify(pharmacyId);

			boolean suc = false;
			if (message.id > 0)
			{
				suc = true;
				message_id = String.valueOf(message.id);
				message_from_who = message.user_from;
				message_text = message.message;

				// register
				showMessageNotification(message_from_who, message_text);

				MyProvider.updateMessageStatusNotification(message_id, pharmacyId);
			}
	    	 
	    	 return suc;
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(Boolean result) {
	    	 // here you check the value of getActivity() and break up if needed
	        /*if(getApplicationContext() == null)
	            return;*/
            /**
             * Updating parsed JSON data into ListView
             * */
	    	/*new Runnable() {
	            public void run() {*/
	    	
	    	 System.out.println("Result boolean: " + result);
	    	 
	    	 if(result){
				 //showMessageNotification();
	    	 }
	    	 else{
				 System.out.println("Result last_error_message: " + MyProvider.last_error);
				 if(MyProvider.last_error.equals("required field (token) missing")){
					 new UserLoginTask().execute();
				 }
	    	 }

	         /*   }
	        };*/    
	    }
	  

	}


		/**
		 * Represents an asynchronous login/registration task used to authenticate
		 * the user.
		 */
		public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
			@Override
			protected Boolean doInBackground(Void... params) {
				
			    //MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
				String mEmail = user;
				String mPassword = pass;
				String mAccessKey = access_key;

			    
			    if(mEmail.length() == 0 || mPassword.length() == 0 ){
			    	/*SharedPreferences pref = getSharedPreferences("defaults", MODE_PRIVATE ); 
			        String prefEmail = pref.getString("Name", ""); 
			        String prefPass = pref.getString("Pass", "");*/
			    	
			    	SharedPreferences prefUser = getSharedPreferences("PREF_USER", 0); 
			    	String prefEmail = prefUser.getString("USER", ""); 
			    	String prefPass = prefUser.getString("PASS", ""); 
		 	        webservice_url = prefUser.getString("URL", "");
			        
			        mEmail = prefEmail;
			        mPassword = prefPass;

					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					access_key = prefs.getString("access_key", "");
			        
			        System.out.println("mEmail mPassword from pref: " + mEmail + " " + mPassword);
			    }else {
			    	System.out.println("mEmail mPassword from global: " + mEmail + " " + mPassword);
			    }
			    
			    MyProvider = new WSDataProvider(user, pass);
		        MyProvider.SetWebServiceUrl(webservice_url);
                MyProvider.SetDeviceIdentification(ip, android_id, appname);
				boolean suc = MyProvider.CheckSecurekLogin(mEmail, mPassword, "app service", "" ,mAccessKey);

				return suc;
			}

			@Override
			protected void onPostExecute(final Boolean success) {

				if (success) {
					System.out.println("Login successfully");
					new LoadListMessages().execute();
				} else {
					System.out.println("Login failed");
				}
			}

			@Override
			protected void onCancelled() {

			}
		}

}


