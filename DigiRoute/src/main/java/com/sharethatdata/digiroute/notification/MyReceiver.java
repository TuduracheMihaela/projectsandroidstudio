package com.sharethatdata.digiroute.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//Toast.makeText(context, "MyReceiver Detected.", Toast.LENGTH_LONG).show();
		
		Log.d("Received : ", "MyReceiver");

		// disable debug messages
		//Toast.makeText(context, "Service message running", Toast.LENGTH_SHORT).show();

	       Intent service1 = new Intent(context, MyService.class);
	       context.startService(service1);

	 }

 } 
