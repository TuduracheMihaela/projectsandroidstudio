package com.sharethatdata.digiroute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiroute.Adapter.NewOrderAdapter;
import com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview.BaseSwipeListAdapter;
import com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview.SwipeMenu;
import com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview.SwipeMenuCreator;
import com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview.SwipeMenuItem;
import com.sharethatdata.digiroute.SwipeListview.swipe.swipemenulistview.SwipeMenuListView;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cDefaultStop;
import com.sharethatdata.logistics_webservice.datamodel.cPackages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by miha on 8/24/2017.
 */

public class DefaultStopsActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";

    ProgressDialog progressDialog;

    ArrayList<HashMap<String, String>> stopsList; // List Stops

    //ListView lv = null;
    private SwipeMenuListView lv;

    private AppAdapter mAdapter;

    String deletedItemId;
    String deletedItemName;
    String deletedItemAddress;
    int positionItem;

    String pharmacyId = "";

    static boolean active = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_stops);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(DefaultStopsActivity.this, DefaultStopsActivity.this);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE);
        pharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //lv = (ListView) findViewById(R.id.listView);
        lv = (SwipeMenuListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String stop_id = ((TextView) itemClicked.findViewById(R.id.textViewId)).getText().toString();
                String name = ((TextView) itemClicked.findViewById(R.id.textViewName)).getText().toString();
                String address = ((TextView) itemClicked.findViewById(R.id.textViewAddress)).getText().toString();

                // start detail activity
                Intent intent = new Intent(getApplicationContext(), DefaultStopsDetailActivity.class);
                intent.putExtra("id", stop_id);
                intent.putExtra("name", name);
                intent.putExtra("address", address);

                startActivity(intent);
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //Swipe with menu
        // 1.create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "edit" item
                SwipeMenuItem editItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                editItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                // set item width
                editItem.setWidth(dp2px(90));
                // set a icon
                editItem.setIcon(R.drawable.ic_edit);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        lv.setMenuCreator(creator);

        SwipeMenuListView.OnDismissCallback callback = new SwipeMenuListView.OnDismissCallback() {
            // Gets called whenever the user deletes an item.
            public SwipeMenuListView.Undoable onDismiss(AbsListView listView, final int position) {

                final HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", deletedItemId);
                map.put("name", deletedItemName);
                map.put("address", deletedItemAddress);

                // Return an Undoable implementing every method
                return new SwipeMenuListView.Undoable() {

                    // Method is called when user undoes this deletion
                    public void undo() {
                        // Reinsert item to list
                        stopsList.add(positionItem, map);
                        mAdapter.notifyDataSetChanged();
                    }

                    // Return an undo message for that item
                    public String getTitle() {
                        return deletedItemId.toString() + " " + getString(R.string.text_deleted);
                    }

                    // Called when user cannot undo the action anymore
                    public void discard() {
                        // Use this place to e.g. delete the item from database
                        //Toast.makeText(DefaultStopsActivity.this, "DISCARD, item " + " " + " now finally discarded", Toast.LENGTH_SHORT).show();
                        new DeleteAddress(deletedItemId).execute();
                    }
                };
            }
        };

        SwipeMenuListView.UndoMode mode = SwipeMenuListView.UndoMode.SINGLE_UNDO;
        lv.SwipeUndo(lv, callback, mode);
        lv.setRequireTouchBeforeDismiss(false);

        // 2. listener item click event
        lv.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            //public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
            public boolean onMenuItemClick(View view, final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // edit
                        Toast.makeText(DefaultStopsActivity.this, getString(R.string.text_edit), Toast.LENGTH_SHORT).show();
                        String stop_id = stopsList.get(position).get("id");
                        String name = stopsList.get(position).get("name");
                        String street = stopsList.get(position).get("street");
                        String house = stopsList.get(position).get("house");
                        String zipcode = stopsList.get(position).get("zipcode");
                        String city = stopsList.get(position).get("city");
                        String country = stopsList.get(position).get("country");

                        // start detail activity
                        Intent intent = new Intent(getApplicationContext(), CreateDefaultStopsActivity.class);
                        intent.putExtra("id", stop_id);
                        intent.putExtra("name", name);
                        intent.putExtra("street", street);
                        intent.putExtra("house", house);
                        intent.putExtra("zipcode", zipcode);
                        intent.putExtra("city", city);
                        intent.putExtra("country", country);
                        intent.putExtra("update", "update");

                        startActivity(intent);

                        break;
                    case 1:
                        // delete
                        positionItem = position;

                        deletedItemId = String.valueOf(mAdapter.getItemId(position));
                        deletedItemName = String.valueOf(mAdapter.getItem(position));
                        deletedItemAddress = String.valueOf(mAdapter.getItemAddress(position));

                        final HashMap<String, String> map = new HashMap<String, String>();
                        map.put("id", deletedItemId);
                        map.put("name", deletedItemName);
                        map.put("address", deletedItemAddress);

                        // Delete item from mAdapter
                        //Toast.makeText(AddOptionToUserActivity.this, "DISCARD, item " + position , Toast.LENGTH_SHORT).show();
                        //itemList.remove(position);
                        stopsList.remove(position);
                        mAdapter.notifyDataSetChanged();

                        // Gets called when the user deletes an item.
                        lv.performDismiss(view, position);

                        break;
                }
                return false;
            }
        });


        // Right directions
        lv.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        // set SwipeListener
        lv.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        lv.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });


        new LoadDefaultStops(pharmacyId).execute();

    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     */
    class LoadDefaultStops extends AsyncTask<String, String, String> {

        public String pharmacyId;

        public LoadDefaultStops(String pharmacyId) {
            this.pharmacyId = pharmacyId;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            stopsList = new ArrayList<HashMap<String, String>>();

            List<cDefaultStop> myPackages = MyProvider.getDefaultStops(pharmacyId);
            for (cDefaultStop entry : myPackages) {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                map.put("name", entry.name);

                map.put("street", entry.street);
                map.put("house", entry.house);
                map.put("zipcode", entry.zipcode);
                map.put("city", entry.city);
                map.put("country", entry.country);

                List<String> slist = new ArrayList<String>();
                slist.add(entry.street);
                slist.add(entry.house);
                slist.add(entry.zipcode);
                slist.add(entry.city);
                slist.add(entry.country);

                StringBuilder rString = new StringBuilder();
                int count = 0;
                for (String line : slist) {
                    ++count;
                    if (!line.equals("")) {
                        if (count != slist.size()) {
                            rString.append(line + ", ");
                        } else {
                            rString.append(line);
                        }
                    }
                }

                map.put("address", rString.toString());
                map.put("longitude", entry.longitude);
                map.put("latitude", entry.latitude);

                // adding HashList to ArrayList
                stopsList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            if (DefaultStopsActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*ListAdapter adapter = null;
                    adapter = new SimpleAdapter(
                            DefaultStopsActivity.this
                            , stopsList
                            , R.layout.activity_default_stops_listview_items, new String[]{"id", "name", "address"},
                            new int[]{R.id.textViewId, R.id.textViewName, R.id.textViewAddress});
                    lv.setAdapter(adapter);*/

                    Log.d("onPostExecute stops","test");

                    mAdapter = new AppAdapter();
                     lv.setAdapter(mAdapter);
                }
            });

            //hideProgressDialog();
        }
    }

    /** AsyncTask create address  */
    private class DeleteAddress extends AsyncTask<String, String, Boolean> {
        String stop_id;

        public DeleteAddress(String stop_id){
            this.stop_id = stop_id;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            //showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = false;

            suc = MyProvider.deleteDefaultStop(stop_id, pharmacyId);

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(DefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DefaultStopsActivity.this, getString(R.string.toast_text_failed_delete_default_address) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }

                        /*String error_message = MyProvider.last_error;
                        if(error_message.equals("unknown address, can not lookup gps coordinates.")){
                            Toast.makeText(DefaultStopsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(DefaultStopsActivity.this, "Failed to delete default stop", Toast.LENGTH_SHORT).show();
                        }*/

                        Log.d("delete address", "false");
                    }
                    else
                    {
                        Log.d("delete address", "true");
                        Toast.makeText(DefaultStopsActivity.this, getString(R.string.toast_text_delete_default_address), Toast.LENGTH_SHORT).show();

                        if(active){
                            new LoadDefaultStops(pharmacyId).execute();
                        }
                    }
                }
            });
            //hideProgressDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.select_action_default_stop, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_create_default_stops:
                Intent intent = new Intent(this, CreateDefaultStopsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg) {
        // check for existing progressDialog
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(DefaultStopsActivity.this); // create a progress Dialog
            progressDialog.setIndeterminate(true);  // remove the ability to hide it by tapping back button
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
        }
        progressDialog.show(); // now display it.
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        new LoadDefaultStops(pharmacyId).execute();
    }




    class AppAdapter extends BaseSwipeListAdapter {

        //final ArrayList<String> mSelectedItems = new ArrayList<String>();
        //final ArrayList<String> mUnSelectedItems = new ArrayList<String>();

        @Override
        public int getCount() {
            return stopsList.size();
        }

        @Override
        public long getItemId(int position) {
            HashMap<String, String> item = stopsList.get(position);
            String id = Html.fromHtml(item.get("id")).toString();
            return Long.valueOf(id);
        }

        @Override
        public String getItem(int position) {
            HashMap<String, String> item = stopsList.get(position);
            String name = Html.fromHtml(item.get("name")).toString();
            return name;

        }

        @Override
        public String getItemAddress(int position) {
            HashMap<String, String> item = stopsList.get(position);
            String address = Html.fromHtml(item.get("address")).toString();
            return address;
        }

        class ViewHolder {
            TextView id;
            TextView name;
            TextView address;

            public ViewHolder(View view) {
                id = (TextView) view.findViewById(R.id.textViewId);
                name = (TextView) view.findViewById(R.id.textViewName);
                address = (TextView) view.findViewById(R.id.textViewAddress);
                view.setTag(this);
            }
        }

    /*        public ArrayList getSelectedItems(){
                return mSelectedItems;
            }

            public ArrayList getUnSelectedItems(){
                return mUnSelectedItems;
            }*/

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(),
                        R.layout.activity_default_stops_listview_items, null);
                new ViewHolder(convertView);
            }
            final ViewHolder holder = (ViewHolder) convertView.getTag();

            String item_id = String.valueOf(getItemId(position));
            String item_name = getItem(position);
            String item_address = getItemAddress(position);

            Log.e("Zdit id", item_id.toString());
            Log.e("Zdit name", item_name.toString());
            Log.e("Zdit address", item_address.toString());

            holder.id.setText(item_id);
            holder.name.setText(item_name);
            holder.address.setText(item_address);

            HashMap<String, String> hashmap_Current;
            hashmap_Current = new HashMap<String, String>();
            hashmap_Current = stopsList.get(position);


            return convertView;
        }


        @Override
        public boolean getSwipeEnableByPosition(int position) {
            if (position % 1 == 0) {
                return true;
            }
            return false;
        }
    }











}