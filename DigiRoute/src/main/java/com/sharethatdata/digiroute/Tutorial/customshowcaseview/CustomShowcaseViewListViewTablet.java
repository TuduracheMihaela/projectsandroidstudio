package com.sharethatdata.digiroute.Tutorial.customshowcaseview;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;

import com.sharethatdata.digiroute.R;
import com.sharethatdata.digiroute.Tutorial.showcaseview.ShowcaseDrawer;

/**
 * Created by gotic_000 on 1/12/2018.
 */

public class CustomShowcaseViewListViewTablet implements ShowcaseDrawer {

    private final float width;
    private final float height;
    private final RectF renderRect;

    private final float radius;
    private final Paint basicPaint;
    private final Paint eraserPaint;
    private int backgroundColor;

    public CustomShowcaseViewListViewTablet(Resources resources) {
        width = resources.getDimension(R.dimen.custom_showcase_width_listview_tablet);
        height = resources.getDimension(R.dimen.custom_showcase_height_listview_tablet);
        renderRect = new RectF();

        this.radius = resources.getDimension(R.dimen.showcase_radius_material_listview);
        this.eraserPaint = new Paint();
        this.eraserPaint.setColor(0xFFFFFF);
        this.eraserPaint.setAlpha(0);
        this.eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        this.eraserPaint.setAntiAlias(true);
        this.basicPaint = new Paint();
    }

    @Override
    public void setShowcaseColour(int color) {
        // no-op
    }

    @Override
    public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
        Canvas bufferCanvas = new Canvas(buffer);
        renderRect.left = x - width / 2f;
        renderRect.right = x + width / 2f;
        renderRect.top = y - height / 2f;
        renderRect.bottom = y + height / 2f;
        //bufferCanvas.drawRect(renderRect, eraserPaint);
        bufferCanvas.drawRoundRect(renderRect, 50, 50, eraserPaint);
        /*Canvas bufferCanvas = new Canvas(buffer);
        bufferCanvas.drawCircle(x, y, radius, eraserPaint);*/
    }

    @Override
    public int getShowcaseWidth() {
        return (int) height;
        //return (int) (radius * 2);
    }

    @Override
    public int getShowcaseHeight() {
        return (int) height;
        //return (int) (radius * 2);
    }

    @Override
    public float getBlockedRadius() {
        //return width;
        return radius;
    }

    @Override
    public void setBackgroundColour(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public void erase(Bitmap bitmapBuffer) {
        bitmapBuffer.eraseColor(backgroundColor);
    }

    @Override
    public void drawToCanvas(Canvas canvas, Bitmap bitmapBuffer) {
        canvas.drawBitmap(bitmapBuffer, 0, 0, basicPaint);
    }

}
