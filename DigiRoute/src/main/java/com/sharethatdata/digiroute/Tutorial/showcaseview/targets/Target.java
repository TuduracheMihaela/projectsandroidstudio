/*
 * Copyright 2014 Alex Curran
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sharethatdata.digiroute.Tutorial.showcaseview.targets;

import android.graphics.Point;

public interface Target {
    Target NONE = new Target() {
        @Override
        public Point getPoint() {
            return new Point(1000000, 1000000);
        }
    };
    Target NONE_MENU_TOOLBAR_PORTRAIT_5 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(1023, 113);
        }
    };

    Target NONE_MENU_TOOLBAR_PORTRAIT_5_5 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(1323, 113);
        }
    };

    Target NONE_MENU_TOOLBAR_PORTRAIT_6 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(1623, 113);
        }
    };

    Target NONE_MENU_TOOLBAR_LANDSCAPE_7 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(1823, 113);
        }
    };

    Target NONE_MENU_TOOLBAR_LANDSCAPE_8 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(923, 63);
        }
    };

    Target NONE_MENU_TOOLBAR_LANDSCAPE_10 = new Target() {
        @Override
        public Point getPoint() {
            return new Point(2373, 113);
        }
    };

    Target NONE_MENU_ADD_ADDRESS_TOOLBAR_PORTRAIT = new Target() {
        @Override
        public Point getPoint() {
            return new Point(923, 513);
        }
    };

    Point getPoint();
}
