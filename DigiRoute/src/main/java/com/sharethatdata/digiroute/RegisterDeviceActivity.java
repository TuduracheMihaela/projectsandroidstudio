package com.sharethatdata.digiroute;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;

public class RegisterDeviceActivity extends Activity {

    MyGlobals Globals;

    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    public String myDeviceID = "";
    public String myDeviceName = "";


    private String myUser = "";
    private String myPass = "";

    private String webservice_url = "";

    String PharmacyId;
    private Button btnSubmit1;

    private String login_text;
    private String email_text;
    private String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_device);

        Globals = ((MyGlobals)getApplication());



        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(RegisterDeviceActivity.this, RegisterDeviceActivity.this);

        // Set up the login form.
        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );

        String current_user = sharedPrefs.getString("prefUsername", "");
        String current_password = sharedPrefs.getString("prefUserPassword", "");
        boolean blAutoLogin = sharedPrefs.getBoolean("prefAutoLogin", false);
        PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");

        // get webservice url
        webservice_url = sharedPrefs.getString("prefWebServiceServer", "");

        myUser = current_user;
        myPass = current_password;
        myDeviceID = Globals.getValue("device");
        myDeviceName = Globals.getValue("device_name");

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(webservice_url);

        btnSubmit1 = (Button) findViewById(R.id.sign_in_button);

        btnSubmit1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                final EditText text_login = (EditText) findViewById(R.id.login);
                login_text = text_login.getText().toString();
                final EditText text_email = (EditText) findViewById(R.id.email);
                email_text = text_email.getText().toString();


                new RegisterDeviceTask().execute();

            }

        });

    }

    public class RegisterDeviceTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            return MyProvider.registerDevice(login_text, email_text, myDeviceID, myDeviceName);

        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_text_registration), Toast.LENGTH_LONG).show();

                finish();
            } else {

                String error_message_id = MyProvider.last_error_nr;
                String error_message = MyProvider.last_error;
                if(error_message_id.equals("1")){
                    Toast.makeText(RegisterDeviceActivity.this, error_message, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_text_failed_registration) + " " + MyProvider.last_error + "\n" + getString(R.string.toast_text_failed_registration_contact), Toast.LENGTH_LONG).show();
                }
            }


        }

    }

}
