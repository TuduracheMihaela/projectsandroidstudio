package com.sharethatdata.digiroute;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cContact;
import com.sharethatdata.sharedactivity.Adapter.SpinnerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageActivity extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    TimeUtils MyTimeUtils = null;

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    ArrayList<HashMap<String, String>> userList;
    List<String> userkeyArray =  new ArrayList<String>();
    List<String> userNameArray =  new ArrayList<String>();

    private String myUser = "";
    private String myPass = "";
    private String idPharmacy;

    private Button btnSubmit1;
    private String message_text;
    private String selected_user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        // avoid automatically appear android keyboard when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Globals = ((MyGlobals)getApplication());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        final String PharmacyId = sharedPrefs.getString("prefPharmacyId", "1");
        idPharmacy = PharmacyId;

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(MessageActivity.this, MessageActivity.this);

        Typeface font1 = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/OpenSans-Regular.ttf");
        Typeface font2 = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Quicksand-Regular.otf");

        final EditText text_message = (EditText) findViewById(R.id.textMessage);
        text_message.setTypeface(font2);

        btnSubmit1 = (Button) findViewById(R.id.buttonSend);
        btnSubmit1.setTypeface(font1);
        btnSubmit1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                message_text = text_message.getText().toString();

                new SendMessageTask().execute();
            }

        });

        new LoadUserList().execute();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    public class SendMessageTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            SendMessage();

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_message_sent), Toast.LENGTH_LONG).show();

            finish();

        }

    }

    public void SendMessage()
    {
        try {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            boolean suc = MyProvider.createMessage(idPharmacy ,selected_user, message_text);

        } catch (Exception ex) {
            Toast.makeText(MessageActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Background Async Task to Load all users by making HTTP Request
     * */
    class LoadUserList extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals)getApplication());

            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));


            userList = new ArrayList<HashMap<String, String>>();

            // convert objects to hashmap for list
            List<cContact> List = MyProvider.getPharmacyUsers(idPharmacy);

            for(cContact entry : List)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("pid", entry.id);
                map.put("name", entry.name);
                map.put("username", entry.username);

                // adding HashList to ArrayList
                userList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            if (MessageActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    List<String> userArray =  new ArrayList<String>();
                    //userArray.add(0, getString(R.string.select_user));
                    //userkeyArray.add(0,"");
                    //userNameArray.add(0,"");

                    if(userList.size()>0){
                        //userArray.add(0, getString(R.string.select_user));
                        //userkeyArray.add(0, getString(R.string.select_user));
                    }else{
                        //userArray.add(0, getString(R.string.select_no_user));
                        //userkeyArray.add(0, getString(R.string.select_no_user));
                    }

                    for (HashMap<String, String> map : userList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "pid") userkeyArray.add(entry.getValue());
                            if (entry.getKey() == "name") userArray.add(entry.getValue());
                            if (entry.getKey() == "username") userNameArray.add(entry.getValue());
                        }

                    Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                    SpinnerAdapter user_adapter = new SpinnerAdapter(
                            MessageActivity.this,
                            R.layout.spinner_adapter,
                            userArray
                    );
                    sUsers.setAdapter(user_adapter);

                    sUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Spinner sUsers = (Spinner) findViewById(R.id.spinnerUser);
                            String mIndex = sUsers.getSelectedItem().toString();

                            //if(mIndex.contains(getString(R.string.select_user))){
                                // do nothing

                            //}else{
                                Spinner userSpinner = (Spinner) findViewById(R.id.spinnerUser);
                                int myIndex = userSpinner.getSelectedItemPosition();
                                selected_user = userkeyArray.get(myIndex);
                            //}
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    //int spinnerPosition = user_adapter.getPosition(myUserName);
                    //set the default according to value
                    //sUsers.setSelection(spinnerPosition);

                    // auto select
                    int selectUserIndex = userNameArray.indexOf(myUser);
                    sUsers.setSelection(selectUserIndex);
                    selected_user = String.valueOf(selectUserIndex);

                }
            });

            hideProgressDialog();

        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (pDialog == null) {
            // create a progress Dialog
            pDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            pDialog.setIndeterminate(true);

            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);

            pDialog.setMessage(msg);

        }

        // now display it.
        pDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (pDialog != null) {
            pDialog.dismiss();
        }

        pDialog = null;
    }



}
