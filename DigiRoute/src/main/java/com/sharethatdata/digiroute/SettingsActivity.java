package com.sharethatdata.digiroute;

import com.sharethatdata.digiroute.R;
import com.sharethatdata.logistics_webservice.WSDataProvider;
import com.sharethatdata.logistics_webservice.datamodel.cDeviceSetting;
import com.sharethatdata.logistics_webservice.datamodel.cWebService;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;

    TimeUtils MyTimeUtils = null;

    private String myUser = "";
    private String myPass = "";
    private String PharmacyId = "";
    private String apikey = "";
    private String ws_url = "";
    private String myDeviceID = "";
    private String myDeviceName = "";

    ArrayList<String> entriesId, entriesName, entriesAccessKey, entriesWebserviceId, entryValuesURL;

	SharedPreferences preferences;
	TextView textViewUser;
	TextView textViewPass;

    ProgressDialog progressDialog;

	static final String PREFS_NAME = "defaults";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
        addPreferencesFromResource(R.xml.settings);

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");
        myDeviceID = Globals.getValue("device");
        myDeviceName = Globals.getValue("device_name");
        MyProvider = new WSDataProvider(myUser, myPass);
        String url_ws = Globals.getValue("ws_url");
        MyProvider.SetWebServiceUrl(url_ws);

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(SettingsActivity.this, SettingsActivity.this);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        apikey = prefs.getString("apikey","");
        ws_url = prefs.getString("ws_url", "");

        //SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        //PharmacyId = sharedPrefs.getString("prefPharmacyId", "");

        /*findPreference("prefWebServiceServer").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("http://www.sharethatdata.com/devlogistics-ws")){
                            // correct
                            Log.e("E","CORRECT");
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                            builder.setTitle("Valid Url");
                            // builder.setMessage("Something's gone wrong...");
                            builder.setPositiveButton(android.R.string.ok, null);
                            builder.show();
                        }
                        return true;

                    }

                });*/

        findPreference("prefPharmacyId").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_pharmacy_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Pharmacy_id", newValue.toString());
                            return true;
                        }


                    }

                });

        findPreference("prefScannerLocationLength").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_location_length_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Location_length", newValue.toString());
                            return true;
                        }

                    }

                });

        findPreference("prefScannerPackageLength").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_package_length_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Package_length", newValue.toString());
                            return true;
                        }

                    }

                });

        findPreference("prefLocationServiceInterval").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_location_service_interval_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Location_service_interval", newValue.toString());
                            return true;
                        }

                    }

                });

        findPreference("prefCameraScanner").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                            if(newValue.toString().contains("true")){
                                updateSettings("Camera_as_scanner", "1");
                            }else if(newValue.toString().contains("false")){
                                updateSettings("Camera_as_scanner", "0");
                            }

                            return true;
                    }

                });

        findPreference("prefConfirmCollect").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                            if(newValue.toString().contains("true")){
                                updateSettings("Confirm_collect", "1");
                            }else if(newValue.toString().contains("false")){
                                updateSettings("Confirm_collect", "0");
                            }

                            return true;
                    }

                });

        findPreference("prefLocationService").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                            if(newValue.toString().contains("true")){
                                updateSettings("Location_service", "1");
                            }else if(newValue.toString().contains("false")){
                                updateSettings("Location_service", "0");
                            }

                            return true;
                    }

                });

        findPreference("prefDistanceUnit").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                                updateSettings("Distance_unit", newValue.toString());

                            return true;
                    }

                });

        findPreference("prefDistanceRange").setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {

                        Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                        if(newValue.equals("")){
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_text_distance_range_cannot_be_empty),
                                    Toast.LENGTH_SHORT).show();
                            return  false;
                        }else{
                            updateSettings("Distance_range", newValue.toString());
                            return true;
                        }
                    }
                });

        findPreference("prefWebServiceServer").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                
                String access_key = "";
                String id_webservice = "";
                for(int i = 0; i < entryValuesURL.size(); i++){
                    if(entryValuesURL.get(i).equals(newValue.toString())){
                        access_key = entriesAccessKey.get(i);
                        id_webservice = entriesWebserviceId.get(i);
                    }
                }
                
                Globals.setValue("ws_url", newValue.toString());
                Globals.setValue("access_key", access_key);
                Globals.setValue("ws_url_id", id_webservice);

                // get list of web services one time and get,set the default url -> if the device was activated by the administrator
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("ws_url", newValue.toString());
                editor.putString("access_key", access_key);
                editor.putString("ws_url_id", id_webservice);
                editor.commit();

                return true;
            }
        });


        // Display the fragment as the main content.
        //if (savedInstanceState == null)
            //getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();

        ListPreference listPreferenceURL = (ListPreference) findPreference("prefWebServiceServer");
        ListPreference listPreferenceDistanceUnit = (ListPreference) findPreference("prefDistanceUnit");
        //ListPreference listPreferenceMapServiceServer = (ListPreference) findPreference("prefMapServiceServer");
        EditTextPreference editTextPharmacyId = (EditTextPreference) findPreference("prefPharmacyId");
        EditTextPreference editTextLocationLenght = (EditTextPreference) findPreference("prefScannerLocationLength");
        EditTextPreference editTextPackageLenght = (EditTextPreference) findPreference("prefScannerPackageLength");
        EditTextPreference editTextLocationServiceInterval = (EditTextPreference) findPreference("prefLocationServiceInterval");
        EditTextPreference editTextPrefixLocation = (EditTextPreference) findPreference("prefScannerLocationPrefix");
        CheckBoxPreference checkBoxCameraScanner = (CheckBoxPreference) findPreference("prefCameraScanner");
        CheckBoxPreference checkBoxConfirmCollect = (CheckBoxPreference) findPreference("prefConfirmCollect");
        CheckBoxPreference checkBoxLocationService = (CheckBoxPreference) findPreference("prefLocationService");
        EditTextPreference editTextDistanceRange = (EditTextPreference) findPreference("prefDistanceRange");

        new LoadListURL(listPreferenceURL).execute();

        new LoadListSettings(editTextPharmacyId, checkBoxCameraScanner, editTextLocationLenght, editTextPackageLenght,
                checkBoxConfirmCollect, listPreferenceDistanceUnit,
                checkBoxLocationService,editTextLocationServiceInterval, editTextDistanceRange).execute();



        PreferenceCategory categoryUserProfile = (PreferenceCategory) findPreference("pref_category_user_profile");
        PreferenceCategory categoryBarcodecanner = (PreferenceCategory) findPreference("pref_category_barcode_scanner");
        PreferenceCategory categoryWebService = (PreferenceCategory) findPreference("pref_category_web_service");

        String isAdministrator = Globals.getValue("administrator");
        if (isAdministrator == "yes") {

        }else{
            categoryUserProfile.removePreference(editTextPharmacyId);

            categoryBarcodecanner.setTitle("");
            categoryBarcodecanner.removePreference(checkBoxCameraScanner);
            categoryBarcodecanner.removePreference(editTextLocationLenght);
            categoryBarcodecanner.removePreference(editTextPackageLenght);
            categoryBarcodecanner.removePreference(editTextPackageLenght);
            categoryBarcodecanner.removePreference(editTextPrefixLocation);
            categoryBarcodecanner.removePreference(checkBoxConfirmCollect);
            categoryBarcodecanner.removePreference(listPreferenceDistanceUnit);
            categoryBarcodecanner.removePreference(editTextDistanceRange);

            categoryWebService.setTitle("");
            categoryWebService.removePreference(listPreferenceURL);
            categoryWebService.removePreference(checkBoxLocationService);
            categoryWebService.removePreference(editTextLocationServiceInterval);
        }
    }

    /*public static class PrefFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            


        }
    }*/


	public void SaveSettings()
	{

	}

	public void updateSettings(String setting, String newValue){
        String id = "";
        String name = "";
        for(int i=0; i<entriesName.size(); i++) {
            if (entriesName.get(i).equals(setting)) {
                id = entriesId.get(i);
                name = entriesName.get(i);
            }
        }
        if(id != "" ) new UpdateDeviceSettings(apikey, myDeviceID, id, name, newValue.toString()).execute();
    }

    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListURL extends AsyncTask<String, String, String> {

        ArrayList<String> entries;
        ListPreference lp;

        public LoadListURL(ListPreference lp){
            this.lp = lp;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            // load data from provider
            entries = new ArrayList<>();
            entryValuesURL = new ArrayList<>();
            entriesAccessKey = new ArrayList<>();
            entriesWebserviceId = new ArrayList<>();

            List<cWebService> myService = MyProvider.getAllWebServices(apikey, myDeviceID, "3");
            for(cWebService entry : myService)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name",  entry.name);
                map.put("type",  entry.type);
                map.put("url",  entry.url);
                map.put("access_key",  entry.access_key);

                // adding HashList to ArrayList
                entries.add(entry.name); // name = what user see, name of the url
                entryValuesURL.add(entry.url); // values = url to connect to service
                entriesAccessKey.add(entry.access_key); // access key = for secure login
                entriesWebserviceId.add(entry.id); // values = url to connect to service
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListPreference
                     * */
                    lp.setEntries(entries.toArray(new String[entries.size()]));
                    lp.setEntryValues(entryValuesURL.toArray(new String[entryValuesURL.size()]));

                    lp.setValue(ws_url);

                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Background Async Task to Load my sent request by making HTTP Request
     * */
    class LoadListSettings extends AsyncTask<String, String, String> {

        //ArrayList<HashMap<String, String>> settingList;
        ArrayList<String>  entryValues;
        EditTextPreference editTextPharmacyId;
        EditTextPreference editTextLocationLenght;
        EditTextPreference editTextLocationServiceInterval;
        EditTextPreference editTextPackageLenght;
        CheckBoxPreference checkBoxCameraScanner;
        CheckBoxPreference checkBoxLocationService;
        CheckBoxPreference checkBoxConfirmCollect;
        ListPreference listPreferenceDistanceUnit;
        EditTextPreference editTextDistanceRange;

        public LoadListSettings(EditTextPreference editTextPharmacyId, CheckBoxPreference checkBoxCameraScanner,
                                EditTextPreference editTextLocationLenght, EditTextPreference editTextPackageLenght,
                                CheckBoxPreference checkBoxConfirmCollect, ListPreference listPreferenceDistanceUnit,
                                CheckBoxPreference checkBoxLocationService, EditTextPreference editTextLocationServiceInterval,
                                EditTextPreference editTextDistanceRange){
            this.editTextPharmacyId = editTextPharmacyId;
            this.editTextLocationLenght = editTextLocationLenght;
            this.editTextPackageLenght = editTextPackageLenght;
            this.editTextLocationServiceInterval = editTextLocationServiceInterval;
            this.checkBoxCameraScanner = checkBoxCameraScanner;
            this.checkBoxConfirmCollect = checkBoxConfirmCollect;
            this.checkBoxLocationService = checkBoxLocationService;
            this.listPreferenceDistanceUnit = listPreferenceDistanceUnit;
            this.editTextDistanceRange = editTextDistanceRange;

        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            //settingList = new ArrayList<HashMap<String, String>>();
            entriesId = new ArrayList<>();
            entriesName = new ArrayList<>();
            entryValues = new ArrayList<>();

            String myDeviceID = Globals.getValue("device");
            String myDeviceName = Globals.getValue("device_name");
            String webservice_id = Globals.getValue("ws_url_id");

            // load data from provider

            List<cDeviceSetting> mySetting = MyProvider.getDeviceSettings(apikey, myDeviceID, "3", webservice_id);
            for(cDeviceSetting entry : mySetting)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                // 1 = Dev Intranet newtonrp
                // 3 = Dev HomeDelivery
                if(entry.webservice.equals("3")){
                    entriesId.add(entry.id); // name = what user see, name of the url
                    entriesName.add(entry.setting); // name = what user see, name of the url
                    entryValues.add(entry.value); // values = url to connect to service
                }

                /*map.put("id",  entry.id);
                map.put("webservice",  entry.webservice);
                map.put("ws_type",  entry.ws_type);
                map.put("setting",  entry.setting);
                map.put("value",  entry.value);*/

                // adding HashList to ArrayList
                //settingList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    //pDialog.dismiss();

                    /**
                     * Updating parsed JSON data into ListPreference
                     * */
                    /*String myRow = "";
                    for (HashMap<String, String> map : settingList)
                        for (Map.Entry<String, String> entry : map.entrySet())
                        {
                            if (entry.getKey() == "webservice")
                            {
                                try {
                                    myRow = entry.getValue().toString();
                                    String row = String.valueOf(myRow);

                                    // 1 = Dev Intranet newtonrp
                                    // 3 = Dev HomeDelivery
                                    if(row.equals("3")){

                                    }
                                } catch(NumberFormatException nfe) {

                                }
                            }
                        }*/


                    for(int i=0; i<entriesName.size(); i++){
                        if(entriesName.get(i).equals("Pharmacy_id")){
                            editTextPharmacyId.setText(entryValues.get(i));
                            SharedPreferences.Editor editor = getSharedPreferences("pref_PharmacyId", 0).edit();
                            editor.putString("prefPharmacyId", String.valueOf(entryValues.get(i)));
                            editor.commit();
                        }

                        if(entriesName.get(i).equals("Location_length")){
                            editTextLocationLenght.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Package_length")){
                            editTextPackageLenght.setText(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Location_service_interval")){
                            editTextLocationServiceInterval.setText(entryValues.get(i));
                        }

                        //Prefix_location

                        if(entriesName.get(i).equals("Camera_as_scanner")){
                            if(entryValues.get(i).equals("1")){
                                checkBoxCameraScanner.setChecked(true);
                            }else {
                                checkBoxCameraScanner.setChecked(false);
                            }
                        }

                        if(entriesName.get(i).equals("Confirm_collect")){
                            if(entryValues.get(i).equals("1")){
                                checkBoxConfirmCollect.setChecked(true);
                            }else {
                                checkBoxConfirmCollect.setChecked(false);
                            }
                        }

                        if(entriesName.get(i).equals("Location_service")){
                            if(entryValues.get(i).equals("1")){
                                checkBoxLocationService.setChecked(true);
                            }else {
                                checkBoxLocationService.setChecked(false);
                            }
                        }

                        if(entriesName.get(i).equals("Distance_unit")){
                            listPreferenceDistanceUnit.setValue(entryValues.get(i));
                        }

                        if(entriesName.get(i).equals("Distance_range")){
                            editTextDistanceRange.setText(entryValues.get(i));
                        }

                    }
                }
            });
            hideProgressDialog();
        }
    }

    /** AsyncTask update status package  */
    private class UpdateDeviceSettings extends AsyncTask<String, String, Boolean>{
        String apikey = "";
        String device = "";
        String setting = "";
        String value = "";
        String id = "";

        public UpdateDeviceSettings(String apikey, String device, String id, String setting, String value){
            this.apikey = apikey;
            this.device = device;
            this.setting = setting;
            this.value = value;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            showProgressDialog(getString(R.string.loading_alert_dialog));
        }

        @Override
        protected Boolean doInBackground(String... args) {

            // call web method
            Globals = ((MyGlobals) getApplicationContext());
            MyProvider = new WSDataProvider(getApplication());
            MyProvider.SetWebServiceUrl("https://cerberus.sharethatdata.com");

            boolean suc = false;

            suc = MyProvider.updateDeviceSettings(apikey, device, id, setting, value);

            if(suc){

                if(setting.equals("Distance_range")) {
                    // update distance range - from pharmacy to new address
                    // call web method
                    Globals = ((MyGlobals) getApplicationContext());
                    MyProvider = new WSDataProvider(getApplication());
                    MyProvider = new WSDataProvider(myUser, myPass);
                    MyProvider.SetWebServiceUrl(ws_url);

                    SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
                    PharmacyId = sharedPrefs.getString("prefPharmacyId", "");

                    suc = MyProvider.updateDistanceRangeFromPharmacy(PharmacyId, value);
                }

            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (success == false)
                    {
                        Log.d("Update Settings", "false");
                        //Toast.makeText(DeliverActivity.this, "Failed to set deliver status", Toast.LENGTH_SHORT).show();

                        String error_message_id = MyProvider.last_error_nr;
                        String error_message = MyProvider.last_error;
                        if(error_message_id.equals("1")){
                            Toast.makeText(SettingsActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SettingsActivity.this, getString(R.string.toast_text_failed_update_settings) + " " + error_message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Log.d("Update Settings", "true");
                    }
                }
            });
            hideProgressDialog();
        }
    }


    /**
     * Shows a Progress Dialog
     *
     * @param msg
     */
    public void showProgressDialog(String msg)
    {

        // check for existing progressDialog
        if (progressDialog == null) {
            // create a progress Dialog
            progressDialog = new ProgressDialog(this);

            // remove the ability to hide it by tapping back button
            progressDialog.setIndeterminate(true);

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.setMessage(msg);

        }

        // now display it.
        progressDialog.show();
    }


    /**
     * Hides the Progress Dialog
     */
    public void hideProgressDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        //progressDialog = null;
    }
}

