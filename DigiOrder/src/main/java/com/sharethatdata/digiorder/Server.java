package com.sharethatdata.digiorder;

/**
 * Created by mverbeek on 18-5-2017.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Server extends Thread {
    private static ServerSocket serverSocket;
    public static Socket clientSocket;
    private static InputStreamReader inputStreamReader;
    private static BufferedReader bufferedReader;
    private static String message;
    static InputStream is;
    static OutputStream os;
    static byte[] buf;
    static BufferedReader reader;
    static BufferedWriter writer;
    static double ConsoleMessage;
    public static String output;
    static BufferedReader bufferedReader2;

    public void run() {

        try {
            serverSocket = new ServerSocket(12345);

        } catch (IOException e) {
            System.out.println("Could not listen on port: 12345");
        }

        System.out.println("Server started. Listening to the port 12345");

        while (true) {

            try {

                clientSocket = serverSocket.accept();
                inputStreamReader = new InputStreamReader(
                        clientSocket.getInputStream());
                bufferedReader = new BufferedReader(inputStreamReader);

                PrintWriter out = new PrintWriter(
                        clientSocket.getOutputStream(), true);
                InputStream inputStream = new ByteArrayInputStream(
                        bufferedReader.readLine().getBytes(
                                Charset.forName("UTF-8")));
                bufferedReader2 = new BufferedReader(new InputStreamReader(
                        inputStream));
                output = bufferedReader2.readLine();
                System.out.println(output);

                out.flush();
                out.close();
                inputStreamReader.close();
                clientSocket.close();

            } catch (IOException ex) {
                System.out.println("Problem in message reading");
            }
        }

    }
}