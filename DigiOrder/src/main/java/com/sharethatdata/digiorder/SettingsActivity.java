package com.sharethatdata.digiorder;

import com.sharethatdata.digiorder.R;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.TextView;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	
	SharedPreferences preferences;
	TextView textViewUser;
	TextView textViewPass;
	
	static final String PREFS_NAME = "defaults";    
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        // Display the fragment as the main content.
        if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(android.R.id.content, new PrefFragment()).commit();
    }

    public static class PrefFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            
            getPreferenceManager().setSharedPreferencesName(PREFS_NAME) ;
            addPreferencesFromResource(R.xml.settings);
            
            findPreference("prefWebServiceServer").setOnPreferenceChangeListener(
                    new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                    	
                    	 Log.d("TAG", "+ onPreferenceChange(preference: " + preference + ", newValue: " + newValue + ")");

                    	 if(newValue.equals("http://newtonrp-ws.sharethatdata.com")){
                    		 // correct
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Valid Url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }else if(newValue.equals("http://dev-ws.sharethatdata.com")){
                    		 // correct
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Valid Url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }/*else if(newValue.equals("")){
                    		 Log.e("E","CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Development url");
                            // builder.setMessage("Something's gone wrong...");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }else{
                    		 // not correct
                    		 Log.e("E","NOT CORRECT");
                    		 final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                             builder.setTitle("Invalid Url");
                             builder.setMessage("Please write correct url");
                             builder.setPositiveButton(android.R.string.ok, null);
                             builder.show();
                    	 }*/
                         
                         //return Pattern.matches("mailPattern", (String) newValue);
                    	 return true;

                    }

                });		
        }
    }
         
    
	public void SaveSettings()
	{
		
	}
	
     
}
