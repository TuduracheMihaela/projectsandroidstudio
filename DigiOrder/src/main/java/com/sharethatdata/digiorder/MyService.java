package com.sharethatdata.digiorder;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.sharethatdata.webservice.WSDataProvider;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class MyService extends Service {

		MyGlobals Globals;
	
		WSDataProvider myProvider = null;
		String myUser = "";
		String myPass = "";

		private static ServerSocket serverSocket;
		public static Socket clientSocket;
		private static InputStreamReader inputStreamReader;
		private static BufferedReader bufferedReader;
		private static String message;
		static InputStream is;
		static OutputStream os;
		static byte[] buf;
		static BufferedReader reader;
		static BufferedWriter writer;
		static double ConsoleMessage;
		public static String output;
		static BufferedReader bufferedReader2;
	
		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
	    
		//	  Toast.makeText(MyService.this, "Service started!", Toast.LENGTH_SHORT).show();
			  
			  Globals = ((MyGlobals)getApplication());
			  String pid = Globals.getValue("pid");
			  String action = Globals.getValue("action");
				
			  myUser = Globals.getValue("user");
			  myPass = Globals.getValue("pass");
					  
			  myProvider = new WSDataProvider(myUser, myPass);
		 
		    new Thread(new Runnable(){
		        public void run() {

					try {
						serverSocket = new ServerSocket(12345);

					} catch (IOException e) {
						System.out.println("Could not listen on port: 12345");
					}

					System.out.println("Server started. Listening to the port 12345");

					// TODO Auto-generated method stub
					while(true)
					{
					   try {
							Thread.sleep(5000);

						   clientSocket = serverSocket.accept();
						   inputStreamReader = new InputStreamReader(
								   clientSocket.getInputStream());
						   bufferedReader = new BufferedReader(inputStreamReader);

						   PrintWriter out = new PrintWriter(
								   clientSocket.getOutputStream(), true);
						   InputStream inputStream = new ByteArrayInputStream(
								   bufferedReader.readLine().getBytes(
										   Charset.forName("UTF-8")));
						   bufferedReader2 = new BufferedReader(new InputStreamReader(
								   inputStream));
						   output = bufferedReader2.readLine();
						   //System.out.println(output);

						   Intent i= new Intent(getBaseContext(), ShowMessageActivity.class);
						   // potentially add data to the intent
						   i.putExtra("message", output);
						   startActivity(i);

						   out.flush();
						   out.close();
						   inputStreamReader.close();
						   clientSocket.close();

					   } catch (Exception ex) {
						   System.out.println("Problem in message reading");
					   }
					}

				}
		    }).start();
		  
		  //TODO do something useful
		  return Service.START_NOT_STICKY;
		  
	  }

	  @Override
	  public IBinder onBind(Intent intent) {
	  
		  //TODO for communication return IBinder implementation
	    
		  return null;
	  }
	  
} 
