package com.sharethatdata.digiorder.orders;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.LocationsActivity;
import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.TimeUtils;
import com.sharethatdata.digiorder.adapter.NotesAdapter;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.digiorder.IntentIntegrator;
import com.sharethatdata.digiorder.IntentResult;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.SelectActionActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderProduct;

import com.sharethatdata.webservice.datamodel.cOrderState;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyOrdersDetailActivity extends Activity{
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;

	private String ws_type = "";
	private String myUser = "";
	private String myPass = "";
	
	cOrder order = null;
	List<cOrderState> statesList;

	TimeUtils MyTimeUtils = null;

	ArrayList<HashMap<String, String>> productList;
	
	private static ProgressDialog pDialog;
	private static AlertDialog alertDialog;
	private static AlertDialog alertDialog2;
	
	private String id_order = "";
	private String id = "";
	private boolean order_complete = false;
	
	TextView textViewBarcode;
    TextView textViewName;
	TextView textViewAddress;
    TextView textViewCode;
    TextView textViewRequest;
    TextView textViewDelivery;
    TextView textViewDeadline;
    TextView textViewDate;
    TextView textViewStarted;
    TextView textViewEnded;
    TextView textViewOrderType;
    TextView textViewOrderStatus;
    TextView textViewStatus;
    TextView textViewProcessed;
    TextView textViewPriority;
    TextView textViewPayMethod;
    TextView textViewError;
    TextView textViewProcesTime;
    TextView textViewWaitingTime;
    EditText TrayBCText;
    EditText LineBCText;
    TextView textViewTray;
	TextView textViewChecked;
    
    private String lastResID="0";
	private String lastLocID="0";
	
    String myBarcode = "";
    String myTrayBarcode = "";
    String myScannedBarcode = "";
	String myProduct = "";
	String myAmount = "";
	String orderline_id = "";
	String selected_orderline_id = "";
	boolean blUseCamera = false;

	String myPayMethod = "";
	String myLanguage = "";

	ListView lv = null;

	ImageView imageViewNote;
	private byte[] bitmapByteArray;
	private Bitmap myBitmap;


	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		
		Globals = ((MyGlobals)getApplication());
		
		String ws_type = Globals.getValue("warehouse_type");
		
		if (ws_type.equals("robot"))
	    {
			setContentView(R.layout.robotorder_view_detail);
			
			textViewBarcode = (TextView) findViewById(R.id.textViewBarcode);
			textViewName = (TextView) findViewById(R.id.textViewName);
			textViewAddress = (TextView) findViewById(R.id.textViewAddress);
			textViewCode = (TextView) findViewById(R.id.textViewCode);
			textViewRequest = (TextView) findViewById(R.id.textViewRequest);
			textViewDelivery = (TextView) findViewById(R.id.textViewDelivery);
			
			textViewDate = (TextView) findViewById(R.id.textViewDate);
			textViewStarted = (TextView) findViewById(R.id.textViewStarted);
			textViewEnded = (TextView) findViewById(R.id.textViewEnded);
			
			textViewOrderType = (TextView) findViewById(R.id.textViewOrderType);
			textViewOrderStatus = (TextView) findViewById(R.id.textViewOrderStatus);
			textViewProcessed = (TextView) findViewById(R.id.textViewProcessed);
			textViewPriority = (TextView) findViewById(R.id.textViewPriority);
			textViewError = (TextView) findViewById(R.id.textViewError);
			
			textViewProcesTime = (TextView) findViewById(R.id.textViewProcesTime);
			textViewWaitingTime = (TextView) findViewById(R.id.textViewWaitingTime);
			textViewChecked = (TextView) findViewById(R.id.textViewChecked);
			
	    
	    } else {
	    	setContentView(R.layout.grid_view_detail);
	    	
	    	textViewName = (TextView) findViewById(R.id.textViewName);
			textViewAddress = (TextView) findViewById(R.id.textViewAddress);
			textViewCode = (TextView) findViewById(R.id.textViewCode);
			textViewDeadline = (TextView) findViewById(R.id.textViewDeadline);
			textViewStatus = (TextView) findViewById(R.id.textViewStatus);
			textViewProcessed = (TextView) findViewById(R.id.textViewProcessed);
			textViewDelivery = (TextView) findViewById(R.id.textViewDelivery);
			textViewPayMethod = (TextView) findViewById(R.id.textViewPayMethod);
			
			textViewTray = (TextView) findViewById(R.id.textViewTray);
			textViewChecked = (TextView) findViewById(R.id.textViewChecked);
			
			// disable when no tray is scanned
			LinearLayout layout = (LinearLayout) findViewById(R.id.projectLayoutNoteName);
			layout.setVisibility(View.GONE);
			layout = (LinearLayout) findViewById(R.id.linearLayout7);
			layout.setVisibility(View.GONE);

			TrayBCText = (EditText) findViewById(R.id.textScanTrayBarcode);
			LineBCText = (EditText) findViewById(R.id.textScanBarcode);

			LineBCText.setFocusable(false);
	    }


		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyOrdersDetailActivity.this, MyOrdersDetailActivity.this);

		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);

		imageViewNote = (ImageView) findViewById(R.id.expanded_image);

		if (blCameraScanner)
		{
			blUseCamera = true;
		} 
		
		lv = (ListView) findViewById(R.id.listViewNotes);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String valueID = extras.getString("ID");    
		    	id_order = valueID;

				// store tray barcode
				myTrayBarcode = Globals.getValue("lastScanned");
				Globals.setValue("lastScanned", "");
		}
		
		new LoadOrderItem().execute();
		
		new LoadListProducts().execute();
		
		if (!ws_type.equals("robot"))
	    {
		
			lv.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					HashMap<String, String> itemsList = (HashMap<String, String>) productList.get(position);
					 String itemId = (String) itemsList.get("title");
					
					selected_orderline_id = (String) itemsList.get("id");
					LineBCText.setText(itemId);
					 				
				}
			});
		
			Button btnEdit = (Button) findViewById(R.id.buttonAddNote);
			if (blUseCamera) btnEdit.setText("Scan");
			
			btnEdit.setOnClickListener(new OnClickListener() {
				 
				  @Override
				  public void onClick(View v) 
				  {				 
					  // if scan with camera
					  if (blUseCamera)
					  {
						  StartScanBarcode();
							  
					  }
					  
					  //StartPickingAction();
				  }
			 
			});

			Button btnEnd = (Button) findViewById(R.id.buttonEnd);
			btnEnd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v)
				{
					// end order
					EndOrder("Do you want to end this order?");
				}

			});
		
		//		EditText EditText = (EditText) findViewById(R.id.textScanBarcode);
				// clear input bc field
			
				LineBCText.setText("");
	         
				LineBCText.addTextChangedListener(new TextWatcher() {
		
		          public void afterTextChanged(Editable s) {
		
		        	  StartPickingAction();
		        	  
		          }
		
		          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		
		          public void onTextChanged(CharSequence s, int start, int before, int count) {}
		    });
				
				TrayBCText.setText("");
		         
				TrayBCText.addTextChangedListener(new TextWatcher() {
		
		          public void afterTextChanged(Editable s) {
		
		        	  String UPCScanned = TrayBCText.getText().toString();
		        	  
		              Log.d("tray barcode scanned", UPCScanned);

					  if (!UPCScanned.equals("") && UPCScanned.length() > 6) AttachTrayBarcode(UPCScanned);
		        	  
		          }
		
		          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		
		          public void onTextChanged(CharSequence s, int start, int before, int count) {}
		    });
				
			 	 
				
		//		LineBCText.setText(myScannedBarcode);

			Button btnCheck = (Button) findViewById(R.id.buttonCheck);
			btnCheck.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v)
				{

					Intent intent2 = new Intent(MyOrdersDetailActivity.this, OrderStatusActivity.class);
					intent2.putExtra("ID", id_order);
					startActivity(intent2);

				}

			});
	
	    } else {
	    		    	
	    	Button btnEdit = (Button) findViewById(R.id.buttonStart);
			btnEdit.setOnClickListener(new OnClickListener() {
				 
				  @Override
				  public void onClick(View v) 
				  {				 
					  // start order
					  // ...
				  }
			 
			});
			
	    }



		// hide keyboard
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		 
		
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		InputStream stream = null;

		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		if (scanResult != null) {

			String UPCScanned = scanResult.getContents();
			if (UPCScanned != null)
			{
				if (myTrayBarcode.equals(""))
				{

					Log.d("tray barcode scanned", UPCScanned);

					//   TrayBCText.setText(UPCScanned);

					if (!UPCScanned.equals("") && UPCScanned.length() > 6) AttachTrayBarcode(UPCScanned);

				} else {

					// set product barcode
					Log.d("barcode scanned", UPCScanned);

					myScannedBarcode = UPCScanned;
					LineBCText = (EditText) findViewById(R.id.textScanBarcode);
					LineBCText.setText(UPCScanned);

					//	  StartPickingAction();

				}

			}

		}

	}
  
	private void StartScanBarcode()
	{
		 // create barcode intent
		  IntentIntegrator integrator = new IntentIntegrator(this);
		  integrator.initiateScan();
	}
	
	private void AttachTrayBarcode(String new_barcode)
	{
	    myTrayBarcode = new_barcode;
		  
		alertDialog = new AlertDialog.Builder(MyOrdersDetailActivity.this).create();
	         
	    AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersDetailActivity.this);
	    	
	    builder.setTitle("Start picking");
	    builder.setMessage("Attach " + myTrayBarcode + " to this order? ");

	    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

	        public void onClick(DialogInterface dialog, int which) {

	       	    // register in ws, attach to order
	        	new RegisterTrayOrder().execute();
	        	
	            dialog.dismiss();

				TrayBCText.setText("");
	    		
	        }
	    });

	    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	            // Do nothing
	            dialog.dismiss();

				TrayBCText.setText("");
	            
	        }
	    });

	    alertDialog = builder.create();
	    alertDialog.show();
				
	}
	
	private void StartPickingAction()
	{
		// get barcode from textview
	   if (!ws_type.equals("robot"))
	    {
		   String bc = myScannedBarcode;
		   try
		   {
			   EditText EditText = (EditText) findViewById(R.id.textScanBarcode);
			   bc = EditText.getText().toString();
		   } catch (Exception ex)
		   {			  
					   
		   }
			// test for tray barcode
			// assign barcode tray to order
			
			if (bc.length() > 5)
			{
		//		bc = "1103D02";
				myBarcode = bc;
				myProduct = "";
				
				// lookup location bc and product
				new LoadOrderLine().execute();
			
				
			}
	    }
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String updateOrder = Globals.getValue("order_update");
	    if(updateOrder != ""){
	    	Globals.setValue("order_update", "");
	    	new LoadOrderItem().execute();
	    }
	    Globals.setValue("edit","");
	    
	    // Note
	    String updateProduct = Globals.getValue("product_update");
	    if(updateProduct != ""){
	    	Globals.setValue("product_update", "");
	    	new LoadListProducts().execute();
	    }
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Globals.setValue("order_update", "order_update");
	    	finish();
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	 
	 /**
		 * Background Async Task to Load Order Item by making HTTP Request
	  	 * */
		class LoadOrderItem extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	      // Showing progress dialog
	         pDialog = new ProgressDialog(MyOrdersDetailActivity.this);
	         pDialog.setTitle("Loading order");
	         pDialog.setMessage("Please wait...");
	         pDialog.setIndeterminate(false);
	         pDialog.setCancelable(true);
	         pDialog.show();
	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		    	
		    	Globals = ((MyGlobals) getApplicationContext());	
		    	
		    	ws_type = Globals.getValue("warehouse_type");
			 	  
		    	myUser = Globals.getValue("user");
		 		myPass = Globals.getValue("pass");
		    	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
			    order = new cOrder();
			    		
			    if (ws_type.equals("robot"))
			    {
			    	MyRobotProvider = new DataProvider();
			    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
			    
		    	
			    	order = MyRobotProvider.getRobotOrder(myUser, id_order);
			     
			    } else {
			    	order = MyProvider.getOrder(id_order);

					statesList = MyProvider.getOrderStates(id_order);

					myPayMethod = order.betaalwijze;
					myLanguage = order.language;
			    }
			    
			    id = order.id;
			
		    	runOnUiThread(new Runnable() {
		               @Override
		               public void run() {
		            	   
		            	   if (ws_type.equals("robot"))
		            	   {
		            		   textViewBarcode.setText(Html.fromHtml(order.barcode));
		            		   textViewName.setText(Html.fromHtml(order.uname));
							   textViewAddress.setText(Html.fromHtml(order.address));
			            	   textViewCode.setText(Html.fromHtml(order.ordernr));
			            	   textViewRequest.setText(Html.fromHtml(order.req_location));
			            	   textViewDelivery.setText(Html.fromHtml(order.del_location));
			            	   
			            	   textViewDate.setText(Html.fromHtml(order.datum));
			            	   textViewStarted.setText(Html.fromHtml(order.started));
			            	   textViewEnded.setText(Html.fromHtml(order.ended));
			            	   
			            	   textViewOrderType.setText(Html.fromHtml(order.order_type)); 
			            	   textViewOrderStatus.setText(Html.fromHtml(order.status)); 
			            	   textViewPriority.setText(Html.fromHtml(order.priority)); 
			            	   
			            	   textViewError.setText(Html.fromHtml(order.error)); 
			            	   
			            	   textViewProcesTime.setText(Html.fromHtml(order.procestime) + " seconds"); 
			            	   textViewWaitingTime.setText(Html.fromHtml(order.waitingtime) + " seconds"); 
			            	   
			            	   if(order.processed.contains("1")){
			            		   textViewProcessed.setText(Html.fromHtml("Yes"));
			            	   }else {
			            		   textViewProcessed.setText(Html.fromHtml("No"));
			            	   }

							   if(order.checked.contains("1")){
								   textViewChecked.setText(Html.fromHtml("Yes"));
							   }else {
								   textViewChecked.setText(Html.fromHtml("No"));
							   }

						   } else {
				   			    
			            	   textViewName.setText(Html.fromHtml(order.uname));
							   textViewAddress.setText(Html.fromHtml(order.address));
			            	   textViewCode.setText(Html.fromHtml(order.ordernr));
			            	   textViewDeadline.setText(Html.fromHtml(order.datum));
			            	   if(order.payed.contains("1")){
			            		   textViewStatus.setText(Html.fromHtml("Yes"));
			            	   }else {
			            		   textViewStatus.setText(Html.fromHtml("No"));
			            	   }
			            	   textViewPayMethod.setText(Html.fromHtml(order.betaalwijze)); 
			            	   textViewDelivery.setText(Html.fromHtml(order.levering)); 
			            	   if(order.processed.contains("1")){
			            		   textViewProcessed.setText(Html.fromHtml("Yes"));
			            	   }else {
			            		   textViewProcessed.setText(Html.fromHtml("No"));
			            	   }
							   if(order.checked.contains("1")){
								   textViewChecked.setText(Html.fromHtml("Yes"));
							   }else {
								   textViewChecked.setText(Html.fromHtml("No"));
							   }
			            	   
			            	   if (!order.barcode.equals(""))
			            	   {
			            		   myTrayBarcode = order.barcode;
			            	   }
			            	   
			            	   // update tray
			            	   textViewTray.setText(myTrayBarcode);
			            	 
			            	   if (myTrayBarcode.equals("")){
			            		   // do nothing
			            	   } else {
				            	// show barcode scan view
				   	   	    	LinearLayout layout = (LinearLayout) findViewById(R.id.projectLayoutNoteName);
				   	   		    layout.setVisibility(View.VISIBLE);
				   	   		    layout = (LinearLayout) findViewById(R.id.linearLayout7);
				   	   		    layout.setVisibility(View.VISIBLE);
				   	   		    layout = (LinearLayout) findViewById(R.id.trayBarcodeLayout);
				   	   		    layout.setVisibility(View.GONE);
				   	   		    layout = (LinearLayout) findViewById(R.id.linearLayout6);
				   	   		    layout.setVisibility(View.GONE);
				   	   		    
				   	   		    // auto focus field
				   	   		    LineBCText.setText("");
				   	   		    LineBCText.requestFocus();
				   	   		
			            	   }
				            }
				        }
		            });
		    	 
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String result) {
		        // dismiss the dialog after getting all products
		
		    	super.onPostExecute(result);

	            // Dismiss the progress dialog
	            if (pDialog.isShowing())
	                pDialog.dismiss();
	        
		  	      
		    }
		}
		
		
	
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 /**
		 * Background Async Task to Load products by making HTTP Request
	  	 * */
		class LoadListProducts extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();

	     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		   	    	
		    	Globals = ((MyGlobals) getApplicationContext());	
		    	
		     	String ws_type = Globals.getValue("warehouse_type");
		 	   		    	
		    	myUser = Globals.getValue("user");
		    	myPass = Globals.getValue("pass");
		    	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
				
			    List<cOrderProduct> products = new ArrayList<cOrderProduct>();
			     
			    if (ws_type.equals("robot"))
			    {
			    	MyRobotProvider = new DataProvider();
			    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
			    
		    	
			    	products = MyRobotProvider.getRobotOrderProductList(id_order);
			     
			    } else {
			    	products = MyProvider.getProducts(id_order);
				    
			    }
			    
		    	 // load data from provider
				productList = new ArrayList<HashMap<String, String>>();  
		    	
				order_complete = true;
				
		    	for(cOrderProduct entry : products)
		    	{
		    		 
	    			// creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                
	                map.put("id",  entry.id);
	                map.put("title", entry.location);
	                map.put("owner", entry.product + System.getProperty("line.separator") + "(" + entry.amount + " * " + "xxx = " + (Integer.parseInt(entry.amount) * 10) + ")");
	                map.put("entry", entry.processed + "/" + entry.amount);
	                map.put("resid",  entry.resID);
	                map.put("groupid",  entry.groupID);
		              
	                
	           //     map.put("picked", entry.processed);
	                
	                // check processed
	                if (entry.amount.equals(entry.amount_done))
	                {
	                } else {
	                	order_complete = false;
	                }
	                	                 
	                // adding HashList to ArrayList
	                productList.add(map);
		    	 }
		    	 if (order.processed.equals("1"))
		    	 {
		    		 order_complete = false;
		    	 }
		    	
		    	 return "";
		    }
		
		    /**
		     * After completing background task Dismiss the progress dialog
		     * **/
		    protected void onPostExecute(String file_url) {
		        // dismiss the dialog after getting all products
		
		    	// updating UI from Background Thread
		    	runOnUiThread(new Runnable() {
		            public void run() {
		            
		         	   /**
		                 * Updating parsed JSON data into ListView
		                 * */
			            NotesAdapter adapter = null;
	            	
	        			adapter = new NotesAdapter(MyOrdersDetailActivity.this, R.layout.grid_view_detail_items_orderproduct, productList);
	        			lv.setAdapter(adapter);
	        			setListViewHeightBasedOnChildren(lv);
        	
		        			
        		      	 
		            }
		        });    
		    	if (order_complete) {
					EndOrder("All items are picked, end order?");
				}
		    }  
		}

		private void EndOrder(String message)
		{
			// alert that order is complete
			AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersDetailActivity.this);

			builder.setTitle("Finish order");
			builder.setMessage(message);

			builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					// Do nothing but close the dialog

					try {

						new RegisterOrder().execute();

						Toast.makeText(MyOrdersDetailActivity.this, "The order is completed.", Toast.LENGTH_LONG).show();

					} catch (Exception ex) {

						Toast.makeText(MyOrdersDetailActivity.this, "The order update failed.", Toast.LENGTH_LONG).show();
					}

					dialog.dismiss();

					new LoadOrderItem().execute();
				}
			});

			builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					// Do nothing
					dialog.dismiss();

				}
			});

			alertDialog = builder.create();
			alertDialog.show();

		}

	private void DisablePicking()
	{
		// alert that order is complete
		AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersDetailActivity.this);

		builder.setTitle("Error");
		builder.setMessage("This product can not be picked directly, please select detail and book manually on this product.");

		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// Do nothing but close the dialog

				dialog.dismiss();

				DoPicking("Register this product: " + myProduct + " as processed?", false);

			}
		});

		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				// Do nothing
				dialog.dismiss();

				DoPicking("Register this product: " + myProduct + " as processed?", false);

			}
		});

		alertDialog2 = builder.create();
		alertDialog2.show();

	}

	private void DoPicking(String message, final boolean book_stock)
	{
		LayoutInflater li = LayoutInflater.from(MyOrdersDetailActivity.this);
		View promptsView = li.inflate(R.layout.prompt_book_amount, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersDetailActivity.this);

		builder.setTitle("Confirm picking");
	//	builder.setMessage("Pick " + myAmount + " of this product: " + myProduct);
		builder.setMessage(message);

		builder.setView(promptsView);

		final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
		userInput.setText(String.valueOf(myAmount));

		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// Do nothing but close the dialog

				try {
					myAmount = userInput.getText().toString();

					// update orderline
					new RegisterOrderLine(book_stock).execute();

				} catch (Exception ex) {


					Toast.makeText(MyOrdersDetailActivity.this, "The order update failed.", Toast.LENGTH_LONG).show();
				}

				dialog.dismiss();

				new LoadListProducts().execute();

			}
		});

		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				// Do nothing
				dialog.dismiss();

			}
		});

		alertDialog = builder.create();
		alertDialog.show();

	}

	   public void setListViewHeightBasedOnChildren(ListView listView) {
		    ListAdapter listAdapter = listView.getAdapter();
		    if (listAdapter == null) {
		        // pre-condition
		        return;
		    }

		    int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
		    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.EXACTLY);
		    for (int i = 0; i < listAdapter.getCount(); i++) {
		        View listItem = listAdapter.getView(i, null, listView);

		        if(listItem != null){
		            // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
		            listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		            listItem.requestLayout();
		            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
		            totalHeight += listItem.getMeasuredHeight();

		        }
		        Log.w("HEIGHT" + i, String.valueOf(totalHeight));
		    }

		    ViewGroup.LayoutParams params = listView.getLayoutParams();
		    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		    listView.setLayoutParams(params);
		    listView.requestLayout();
		}

	   /**
		 * Background Async Task 
	  	 * */
		class LoadOrderLine extends AsyncTask<String, String, String> {

			  AlertDialog alertDialog;
			   
		     /**
		      * Before starting background thread Show Progress Dialog
		      * */
		     @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		         alertDialog = new AlertDialog.Builder(MyOrdersDetailActivity.this).create();
		     }

		    /**
		     * getting items from url
		     * */
		    protected String doInBackground(String... args) {
		   	    
		    	// lookup order line
            	
            	Globals = ((MyGlobals) getApplicationContext());	
		    	myUser = Globals.getValue("user");
		    	myPass = Globals.getValue("pass");
		    	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		//    	MyRoboProvider = new WSDataProvider(myUser, myPass);
	//		    MyRoboProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			    
			    // get product on location?
			    List<cResource> myResources = MyProvider.getResourcesbyLocationBarcode(myBarcode);
			    
    		 	List<cOrderProduct> products = MyProvider.getProducts(id_order);
    		 	
    		 	boolean found = false;
    		 	
    			for(cResource res : myResources)
    	    	{
    				for(cOrderProduct entry : products)
	    	    	{	
    					// if line selected by user
    					if (!selected_orderline_id.equals(""))
    					{
		    	    		if (entry.productnr.equals(res.code) && entry.id.equals(selected_orderline_id))
		    	    		{
		    	    			found = true;
		    	    		}
		    	    	// if scanned as barcode		
    					} else {
    						if (entry.productnr.equals(res.code))
		    	    		{
		    	    			found = true;
		    	    		}
    					}
    					if (found)
    					{
    						orderline_id = entry.id;
	    	    			myProduct = entry.product;
							myAmount = entry.amount;
	    	    			lastResID = res.id;
	    	    			break;
    					}
	    	    	}
    				
    				if (found)
					{
    					break;		
					}
    	    	}
    			
    			// disable last selected
    			selected_orderline_id = "";
    			    			
		    	return "";
		    }
			
		
		    protected void onPostExecute(String file_url) {
		       
		    		if (!myProduct.equals(""))
		    		{
					//	if (myProduct.substring(0, 3).equals("LF-")) {

							//DisablePicking();
							DoPicking("Pick " + myAmount + " of this product: " + myProduct, true);


					//	} else {

					//		DoPicking("Pick " + myAmount + " of this product: " + myProduct, true);

					//	}

	    		    
		    		} else {
		    			  Toast.makeText(MyOrdersDetailActivity.this, "No product in this order on scanned location!", Toast.LENGTH_LONG).show();
	    		             
		    		}
	               	 
	            }
		       
		}



//////////////////////////////////////////////////////////////////////////////////////////////////////////


	private class RegisterStockTask extends AsyncTask<String, String, Boolean>
	    {
	    	
	    	 
			@Override
	        protected Boolean doInBackground(String... args) 
	        {
				 // find location id
				lastLocID = "0";
				 List<cLocation> locations = MyProvider.getLocations(lastResID);
				 for(cLocation entry : locations)
		    	 {
					 if (entry.code.equals(myBarcode))
					 {
						 lastLocID = String.valueOf(entry.id);
		    	 	 }
		    	 }
				 
				 if (!lastLocID.equals("0"))
				 {
				
					 Log.d("digiorder_logs","updating stock on locationid = " + lastLocID + " , resourceid = " + lastResID);
				  
					 return MyProvider.updateStock(lastResID, lastLocID, -1 * Integer.parseInt(myAmount), false);
				 } else {
					 return false;
				 }
			}
				   
			protected void onPostExecute(Boolean result) 
			{
				
			}
	    }
	    
	    private class RegisterOrderLine extends AsyncTask<String, String, Boolean>
	    {
			boolean bl_book_stock = false;

			public RegisterOrderLine(boolean book_stock) {
				super();
				bl_book_stock = book_stock;
			}
	    	 
			@Override
	        protected Boolean doInBackground(String... args) 
	        {
				if (!orderline_id.equals(""))
				{				
					 Log.d("digiorder_logs","updating order line  = " + orderline_id);
				  
					 return MyProvider.updateOrderProduct(orderline_id, myAmount, myProduct);
									 
				 } else {
					 return false;
				 }
			}
				   
			protected void onPostExecute(Boolean result) 
			{
				if (result == false) {
					Toast.makeText(MyOrdersDetailActivity.this, "Failed to update order : " +  MyProvider.last_error, Toast.LENGTH_LONG).show();
				} else {
					// update stock
					if (bl_book_stock == true) new RegisterStockTask().execute();

					Toast.makeText(MyOrdersDetailActivity.this, "The order is updated.", Toast.LENGTH_LONG).show();
				}
			}
	    }
	    
	    private class RegisterOrder extends AsyncTask<String, String, Boolean>
	    {
	    	 
			@Override
	        protected Boolean doInBackground(String... args) 
	        {
				if (!id_order.equals(""))
				{				
					 Log.d("digiorder_logs","updating order  = " + id_order);
				  
					 return MyProvider.updateOrder(id);
					 
				 } else {
					 return false;
				 }
			}
				   
			protected void onPostExecute(Boolean result) 
			{
				
			}
	    }
	    
	    private class RegisterTrayOrder extends AsyncTask<String, String, Boolean>
	    {
		    
			  
		     /**
		      * Before starting background thread Show Progress Dialog
		      * */
		     @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
		       
		     }
		     
			@Override
	        protected Boolean doInBackground(String... args) 
	        {
				if (!myTrayBarcode.equals(""))
				{	
					Log.d("digiorder_logs","updating order with tray barcode  = " + myTrayBarcode);
					
					return MyProvider.assignOrder(id_order, myTrayBarcode, myPayMethod, myLanguage);
					 
				 } else {
					return false;
				 }
			}
			   
		   
			protected void onPostExecute(Boolean result) 
			{
				if (result == false)
				{
					Toast.makeText(MyOrdersDetailActivity.this, "The tray is already in use.", Toast.LENGTH_LONG).show();
					myTrayBarcode = "";
				} else {
			        Toast.makeText(MyOrdersDetailActivity.this, "The order is attached to tray.", Toast.LENGTH_LONG).show();
				}
				
				new LoadOrderItem().execute();
			}
	    }

}
