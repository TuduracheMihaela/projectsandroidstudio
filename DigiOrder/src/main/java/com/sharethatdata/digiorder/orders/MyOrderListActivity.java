package com.sharethatdata.digiorder.orders;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.SelectWarehouseActivity;
import com.sharethatdata.digiorder.TimeUtils;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.SettingsActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.robotservice.DataProvider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;

public class MyOrderListActivity extends Activity{
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;
	
	TimeUtils MyTimeUtils = null;
	
	private static ProgressDialog pDialog;

	private int mStartYear,mStartMonth,mStartDay;
	private int mEndYear,mEndMonth,mEndDay;
	private String myUser = "";
	private String myPass = "";

	Spinner orderTypeSpinner = null;

	ArrayList<HashMap<String, String>> orderlist;
	ArrayList<HashMap<String, String>> userList; // users

	TextView startDateView;
	TextView endDateView;
	private String myOrderType = "";
    private  boolean orders_loading = false;

	private boolean blLoadingSuccess = false;
	
	ListView lv = null;
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_list_orders);

		orderTypeSpinner = (Spinner) findViewById(R.id.spinnerOrderType);

		ArrayAdapter myAdap = (ArrayAdapter) orderTypeSpinner.getAdapter();
		int spinnerPosition = myAdap.getPosition("PICK");
		orderTypeSpinner.setSelection(spinnerPosition);

		lv = (ListView) findViewById(R.id.listViewOrders);

		startDateView = (TextView) findViewById(R.id.dateStartText);
		endDateView = (TextView) findViewById(R.id.dateEndText);

		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MyOrderListActivity.this, MyOrderListActivity.this);


		Calendar c1 = Calendar.getInstance();
		c1.add(Calendar.DATE, -90);
		mStartYear = c1.get(Calendar.YEAR);
		mStartMonth = c1.get(Calendar.MONTH);
		mStartDay = c1.get(Calendar.DAY_OF_MONTH);

		Calendar c2 = Calendar.getInstance();
		mEndYear = c2.get(Calendar.YEAR);
		mEndMonth = c2.get(Calendar.MONTH);
		mEndDay = c2.get(Calendar.DAY_OF_MONTH);

		startDateView.setText(new StringBuilder().append(pad(mStartDay)).append("/").append(pad(mStartMonth + 1)).append("/").append(pad(mStartYear)));
		endDateView.setText(new StringBuilder().append(pad(mEndDay)).append("/").append(pad(mEndMonth + 1)).append("/").append(pad(mEndYear)));

		myOrderType = "PICK";
		LoadOrders();
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
				if (orderlist.size() > 0)
				{
					HashMap<String, String> itemsList = (HashMap<String, String>) orderlist.get(position);
					 String itemId = (String) itemsList.get("id");
					 String itemName = (String) itemsList.get("uname");
					 String itemStartDate = (String) itemsList.get("datum");
					 String itemOrderNr = (String) itemsList.get("ordernr");
					 String itemBedrag = (String) itemsList.get("bedrag");
					 String itemProcessed = (String) itemsList.get("processed");
					 String itemPayed = (String) itemsList.get("payed");
					 
					 Intent intent = new Intent(MyOrderListActivity.this, MyOrdersDetailActivity.class);
					 intent.putExtra("edit_action", "edit_order");
					 
					 intent.putExtra("ID", itemId);

					 // reset scan
					 Globals.setValue("lastScanned", "");

					 startActivity(intent);
				}
			}

		});

		orderTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


					Spinner orderTypeSpinner = (Spinner) findViewById(R.id.spinnerOrderType);
					int myIndex = orderTypeSpinner.getSelectedItemPosition();

					// set order type
					myOrderType = orderTypeSpinner.getSelectedItem().toString();

				    LoadOrders();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}

		});
	}

	private Object pad(int mMinute2) {
		if (mMinute2 >= 10)
			return String.valueOf(mMinute2);
		else
			return "0" + String.valueOf(mMinute2);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.order_menu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_add_order:
            Intent intent = new Intent(this, AddOrderActivity.class); startActivity(intent); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private DatePickerDialog.OnDateSetListener mDateStartSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int month, int day) {

			mStartYear = year;
			mStartMonth = month;
			mStartDay = day;

			String dayOfWeekStart = DateFormat.format("EE", new Date(mStartYear, mStartMonth, mStartDay-1)).toString();

			startDateView.setText(new StringBuilder().append(dayOfWeekStart).append(" ").append(pad(mStartDay)).append("/")
					.append(pad(mStartMonth + 1)).append("/")
					.append(pad(mStartYear)));

			LoadOrders();

		}

	};

	private DatePickerDialog.OnDateSetListener mDateEndSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int month, int day) {

			mEndYear = year;
			mEndMonth = month;
			mEndDay = day;

			String dayOfWeekEnd = DateFormat.format("EE", new Date(mEndYear, mEndMonth, mEndDay-1)).toString();

			endDateView.setText(new StringBuilder().append(dayOfWeekEnd).append(" ").append(pad(mEndDay)).append("/")
					.append(pad(mEndMonth + 1)).append("/")
					.append(pad(mEndYear)));

			LoadOrders();

		}

	};

	private void LoadOrders()
	{
		if (!orders_loading) {
			orders_loading = true;

			String myDateTime = (new StringBuilder().append(pad(mStartYear)).append("-").append(pad(mStartMonth + 1)).append("-").append(pad(mStartDay))).toString();
			String myDateTimeEnd = (new StringBuilder().append(pad(mEndYear)).append("-").append(pad(mEndMonth + 1)).append("-").append(pad(mEndDay))).toString();

			Globals.setValue("report_date_start", myDateTime);
			Globals.setValue("report_date_end", myDateTimeEnd);

			new LoadList().execute();
		}
	}

	public void onStartDayClick(View v)	{
		DatePickerDialog dp1 = new DatePickerDialog(this, mDateStartSetListener, mStartYear, mStartMonth, mStartDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp1.setTitle(sdf.format(d));
		dp1.show();

	}

	public void onEndDayClick(View v){
		DatePickerDialog dp2 = new DatePickerDialog(this, mDateEndSetListener, mEndYear, mEndMonth, mEndDay);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		dp2.setTitle(sdf.format(d));
		dp2.show();
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String update = Globals.getValue("order_update");
	    if(update != ""){
	    	Globals.setValue("order_update", "");
	    	new LoadList().execute();
	    }
		Globals.setValue("edit_action","");
	}
	
	/**
	 * Background Async Task to Load all orders by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
         pDialog = new ProgressDialog(MyOrderListActivity.this);
         pDialog.setTitle("Loading orders");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals)getApplication());
	    	
	    	String ws_type = Globals.getValue("warehouse_type");
	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

			String report_date_start = Globals.getValue("report_date_start");
			String report_date_end = Globals.getValue("report_date_end");


			// load data from provider
		    orderlist = new ArrayList<HashMap<String, String>>();  
		    List<cOrder> orders = new ArrayList<cOrder>();
		     
		    if (ws_type.equals("robot"))
		    {
		    	MyRobotProvider = new DataProvider();
		    	MyRobotProvider.SetHost(Globals.getValue("ws_url"), "8002");
		    
	    	 	orders = MyRobotProvider.getRobotOrderList(myUser);
		     
		    } else {
		    	orders = MyProvider.getOrderList(myUser, report_date_start, report_date_end, myOrderType);
			}

			 if (MyProvider.last_error.equals("")) blLoadingSuccess = true;

	    	 for(cOrder entry : orders)
	    	 {
    		    HashMap<String, String> map = new HashMap<String, String>();

                String payed = "";
                String kind = "";
                String new_stock = "";
                
            	boolean blShowOrder = true;
                
                if (ws_type.equals("robot"))
    		    {
                	if (entry.processed.equals("1")) payed = payed + " finished";
    		    } else {
                	if (entry.payed.equals("1")) payed = "paid";
                	if (entry.processed.equals("1")) payed = payed + " picked";
					if (entry.checked.equals("1")) payed = payed + " checked";
                	// dont show failed ideal payments
                	if (entry.betaalwijze.equals("ideal") && entry.payed.equals("0")) blShowOrder = false;
                
    		    }
                map.put("id",  entry.ordernr);
                map.put("uname", entry.uname);
                map.put("ordernr", entry.ordernr + " " + payed);
                map.put("datum", entry.datum);
                
                if (ws_type.equals("robot"))
    		    {
                	map.put("bedrag", entry.bedrag);
    		    } else {
    		    	map.put("bedrag", entry.num_delivered + "/" + entry.num_products + " products => " + "\u20ac " + entry.bedrag);
    		    }
                map.put("processed", entry.processed);
                map.put("payed", entry.payed);
                
                // show kind of order
                if (entry.bedrag.equals("0")) kind = "sample";
                if (!entry.num_delivered.equals("0") && !entry.num_delivered.equals(entry.num_products)) kind = "backorder";
                map.put("kind", kind);
                
                new_stock = "";
                if (!entry.new_stock.equals("0")) new_stock = "Stock OK";
			 	if (entry.payed.equals("1") && entry.processed.equals("1") && !entry.checked.equals("1")) new_stock = "To Check";

                map.put("new_stock", new_stock);
              
                if (blShowOrder)
                {
					orderlist.add(map);
		 		}

			}

	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	  // Dismiss the progress dialog
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();

	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;

	            	adapter = new SimpleAdapter(
	                		MyOrderListActivity.this
	                		, orderlist
	                		, R.layout.listview_item_orders, new String[] { "uname" , "kind", "bedrag", "new_stock", "ordernr", "datum"},
                            new int[] { R.id.textViewName, R.id.textViewKind, R.id.textViewStatus, R.id.textViewStock, R.id.textViewProject, R.id.textViewDeadline });
	        	            
                
	                // updating listview
	                lv.setAdapter(adapter);
        			
	              
	            }
	        });
	  	      
	        
	        if (orderlist.size() == 0 && blLoadingSuccess == false) Toast.makeText(MyOrderListActivity.this, "Error, loading orders failed!", Toast.LENGTH_LONG).show();

			orders_loading = false;
	    }
	  

	}
		
}
