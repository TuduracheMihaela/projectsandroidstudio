package com.sharethatdata.digiorder.orders;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharethatdata.digiorder.IntentIntegrator;
import com.sharethatdata.digiorder.IntentResult;
import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.ScanResourceActivity;
import com.sharethatdata.digiorder.TimeUtils;
import com.sharethatdata.digiorder.adapter.ImagesAdapter;
import com.sharethatdata.digiorder.adapter.NotesAdapter;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderImage;
import com.sharethatdata.webservice.datamodel.cOrderProduct;
import com.sharethatdata.webservice.datamodel.cOrderState;
import com.sharethatdata.webservice.datamodel.cResource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OrderStatusActivity extends Activity {

    static final int REQUEST_TAKE_PHOTO = 2;
    private static final int REQUEST__BROWSE_PHOTO = 3;

    private String mCurrentPhotoPathAbsolute;
    private String mCurrentPhotoPath;

    ArrayList<HashMap<String, String>> imagesList;
    List<cOrderState> statesList;
    List<cOrderImage> imageslist;

    MyGlobals Globals;
    WSDataProvider MyProvider = null;
    DataProvider MyRobotProvider = null;

    TimeUtils MyTimeUtils = null;

    private String ws_type = "";

    private String myUser = "";
    private String myPass = "";

    private byte[] bitmapByteArray;
    private Bitmap myBitmap;
    private String myImageData = "";

    ImageView imageViewNote;
    private String id_order = "";
    private String id = "";
    cOrder order = null;

    private String str_picked_date = "";
    private String str_picked_user = "";

    private String str_checked_date = "";
    private String str_checked_user = "";
    private String str_total_time = "";

    private static ProgressDialog pDialog1;

    TextView textViewName;
    TextView textViewCode;
    TextView textViewCreated;
    TextView textViewPaid;
    TextView textViewPicked;
    TextView textViewChecked;
    TextView textViewTotalTime;

    ListView lv = null;


    // For zoom image /////////////////////////
    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        myUser = sharedPrefs.getString("prefUsername", "");

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        ws_type = Globals.getValue("warehouse_type");

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(OrderStatusActivity.this, OrderStatusActivity.this);

        imageViewNote = (ImageView) findViewById(R.id.expanded_image);

        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewCode = (TextView) findViewById(R.id.textViewCode);
        textViewCreated = (TextView) findViewById(R.id.textViewCreated);
        textViewPaid = (TextView) findViewById(R.id.textViewPaid);
        textViewPicked = (TextView) findViewById(R.id.textViewPicked);
        textViewChecked = (TextView) findViewById(R.id.textViewChecked);
        textViewTotalTime = (TextView) findViewById(R.id.textViewTotalTime);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String valueID = extras.getString("ID");
            id_order = valueID;
        }

        lv = (ListView) findViewById(R.id.listViewImages);

        Button btnCheck = (Button) findViewById(R.id.buttonCheck);
        btnCheck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                CreateImage();

            }

        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                HashMap<String, String> itemsList = (HashMap<String, String>) imagesList.get(position);
                String itemData = (String) itemsList.get("image");
                String itemID = (String) itemsList.get("id");

                ImageView im = (ImageView) view.findViewById(R.id.image);

                if (itemData != null) {

                    Intent intent = new Intent(OrderStatusActivity.this, OrderImageActivity.class);
                    intent.putExtra("ID", itemID);

                    startActivity(intent);
                }

            }
        });

    //    final ImageView imageView1 = (ImageView) findViewById(R.id.image1);

    //    imageView1.setOnClickListener(new View.OnClickListener() {

    //        @Override
    //        public void onClick(View v) {
   //         if (myImageData != null) zoomImageFromThumb(imageView1, myImageData);

    //        }
     //   });

        new LoadOrderItem().execute();

        new LoadListImages().execute();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String resizeBase64Image(String base64image){
        int IMG_WIDTH = 800;
        int IMG_HEIGHT = 600;
        byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


        if(image.getHeight() <= 400 && image.getWidth() <= 400){
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100, baos);

        byte [] b=baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void zoomImageFromThumb(final View thumbView, String imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);

        //
        byte[] decodedString;
        if(imageResId != ""){
            String resizeString =  resizeBase64Image(imageResId);
            decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
        }else{
            decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
        }


        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null)
        {
            //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
            //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
            expandedImageView.setImageBitmap(decodedByte);

        }

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.scrollView)// the container
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }

    private String GetDaysDifference(String start, String end) {

        String ret = "";

        try {

            Date date1 = null;
            Date date2 = null;
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            date1 = df.parse(start);
            date2 = df.parse(end);
            long diff = Math.abs(date1.getTime() - date2.getTime());
            long diffHours = diff / ( 60 * 60 * 1000);

            if (diffHours > 24)
            {
                diffHours = (diffHours / 24);
                ret = String.valueOf(diffHours) + " days";
            } else {
                ret = String.valueOf(diffHours) + " hours";
            }

        } catch (Exception e1) {
            System.out.println("exception " + e1);
        }

        return ret;
    }

    class LoadOrderItem extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(OrderStatusActivity.this);
            pDialog1.setTitle("Loading order");
            pDialog1.setMessage("Please wait...");
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.show();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            ws_type = Globals.getValue("warehouse_type");

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            order = new cOrder();

            if (ws_type.equals("robot"))
            {
                MyRobotProvider = new DataProvider();
                MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");

                order = MyRobotProvider.getRobotOrder(myUser, id_order);

            } else {
                order = MyProvider.getOrder(id_order);

                statesList = MyProvider.getOrderStates(id_order);

                for(cOrderState entry : statesList) {
                    if (entry.status.equals("Picked"))
                    {
                        str_picked_date = entry.datum;
                        str_picked_user = entry.user;
                    }
                    if (entry.status.equals("Checked"))
                    {
                        str_checked_date = entry.datum;
                        str_checked_user = entry.user;
                    }

                }

                // total time
                str_total_time = GetDaysDifference(order.datum, str_checked_date);

          //      List<String> returnList = new ArrayList<String>();
          //      returnList = MyProvider.getOrderImages(id_order);
          //      myBitmap = null;

          //      if (returnList.size() > 0) {
          //          String image_data = returnList.get(0);

          //          myImageData = image_data;

          //          myBitmap = ConvertByteArrayToBitmap(image_data);
          //      }

            }

            id = order.id;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (ws_type.equals("robot"))
                    {
                        textViewName.setText(Html.fromHtml(order.uname));
                        textViewCode.setText(Html.fromHtml(order.ordernr));
                        textViewCreated.setText(Html.fromHtml(order.datum));
                        textViewPaid.setText(Html.fromHtml(order.datum));
                        textViewPicked.setText(Html.fromHtml(order.datum));
                        textViewChecked.setText(Html.fromHtml(order.datum));
                        textViewTotalTime.setText(Html.fromHtml(""));

                    } else {

                        textViewName.setText(Html.fromHtml(order.uname));
                        textViewCode.setText(Html.fromHtml(order.ordernr));
                        textViewCreated.setText(Html.fromHtml(order.datum));
                        textViewPaid.setText(Html.fromHtml(order.datum));
                        textViewPicked.setText(Html.fromHtml(str_picked_date + " " + str_picked_user));
                        textViewChecked.setText(Html.fromHtml(str_checked_date + " " + str_checked_user));
                        textViewTotalTime.setText(Html.fromHtml(str_total_time));

                    }

                    //ShowImage();
                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            // dismiss the dialog after getting all products

            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog1.isShowing())
                pDialog1.dismiss();

        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Background Async Task to Load images by making HTTP Request
     * */
    class LoadListImages extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            String ws_type = Globals.getValue("warehouse_type");

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            List<cOrderImage> images = new ArrayList<cOrderImage>();

            images = MyProvider.getOrderImages(id_order);

            // load data from provider
            imagesList = new ArrayList<HashMap<String, String>>();

            for(cOrderImage entry : images)
            {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("datetime", entry.datetime);
                map.put("user", entry.user);
                map.put("image", entry.image);

                // adding HashList to ArrayList
                imagesList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all images

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ImagesAdapter adapter = null;

                    adapter = new ImagesAdapter(OrderStatusActivity.this, R.layout.listview_items_images, imagesList);
                    lv.setAdapter(adapter);
                    setListViewHeightBasedOnChildren(lv);

                }
            });

        }
    }


    private  void ShowImage()
    {

     //   int photoW = myBitmap.getWidth();
    //    int photoH = myBitmap.getHeight();
      //  int scaleFactor = Math.min(photoW/400, photoH/400);
     //   int NewW = photoW/scaleFactor;
     //   int NewH = photoH/scaleFactor;
        if (myBitmap != null) {
            imageViewNote = (ImageView) findViewById(R.id.image1);

            imageViewNote.setVisibility(View.VISIBLE);
            //  imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
            imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, 400, 400, true));
        }

    }

    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        Bitmap decodedByte = null;
        if(b.isEmpty() || b.contentEquals(""))
        {
            decodedByte = null;
        }else{
            byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        }

        return decodedByte;
    }

    private void CreateImage()
    {
        // check order
        Toast.makeText(getApplicationContext(), "onCreateImage", Toast.LENGTH_SHORT).show();

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try
            {
                photoFile = createImageFile();
            }
            catch (IOException ex)
            {

            }

            if (photoFile != null)
            {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }

            //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPathAbsolute = image.getAbsolutePath();
        return image;
    }

    // Methods - set visible the preview image
    private void AttachBitmapToReport()
    {
        int photoW = myBitmap.getWidth();
        int photoH = myBitmap.getHeight();
        int scaleFactor = Math.min(photoW/400, photoH/400);
        int NewW = photoW/scaleFactor;
        int NewH = photoH/scaleFactor;

     //   imageViewNote.setVisibility(View.VISIBLE);
    //    imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));

        // save in database
        // update stock
        new RegisterImageTask().execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        InputStream stream = null;

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAbsolute);
            AttachBitmapToReport();
        }

        if (requestCode == REQUEST__BROWSE_PHOTO && resultCode == Activity.RESULT_OK)
        {

            try
            {
                stream = getContentResolver().openInputStream(data.getData());
                myBitmap = BitmapFactory.decodeStream(stream);
                AttachBitmapToReport();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    /** AsyncTask register image  */
    private class RegisterImageTask extends AsyncTask<String, String, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(OrderStatusActivity.this);
            pDialog1.setTitle("Saving image");
            pDialog1.setMessage("Please wait...");
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.setCanceledOnTouchOutside(false);
            pDialog1.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {

            int BitmapID = 0;
            boolean suc = false;
            if (myBitmap != null)
            {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArray = stream.toByteArray();

                byte[] bitmapByteArrayThumbnail;
                bitmapByteArrayThumbnail = bitmapByteArray;
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 1, stream);
                Bitmap.createScaledBitmap(myBitmap, 1, 1, false);
                bitmapByteArrayThumbnail = stream.toByteArray();

                BitmapID = MyProvider.createOrderImage(id_order, bitmapByteArray, bitmapByteArrayThumbnail);
                suc = true;
            }

            return suc;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final Boolean success) {

            pDialog1.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {

                public void run() {

                    if (success == false)
                    {
                        Toast.makeText(OrderStatusActivity.this, "The image was not created", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(OrderStatusActivity.this, "The image was created", Toast.LENGTH_SHORT).show();

                        new LoadOrderItem().execute();

                        new LoadListImages().execute();
                    }

                }
            });
        }
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                listItem.requestLayout();
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
            Log.w("HEIGHT" + i, String.valueOf(totalHeight));
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


}
