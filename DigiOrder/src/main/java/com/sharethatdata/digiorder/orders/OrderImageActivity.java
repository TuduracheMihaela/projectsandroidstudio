package com.sharethatdata.digiorder.orders;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.ResourceDetailActivity;
import com.sharethatdata.digiorder.TimeUtils;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderImage;
import com.sharethatdata.webservice.datamodel.cOrderState;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class OrderImageActivity extends Activity {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;
    DataProvider MyRobotProvider = null;

    TimeUtils MyTimeUtils = null;

    private String ws_type = "";

    private String myUser = "";
    private String myPass = "";

    private byte[] bitmapByteArray;
    private Bitmap myBitmap;
    private String myImageData = "";

    ImageView imageView;
    private String id_image = "";
    cOrderImage orderImage;

    private static ProgressDialog pDialog1;
    private String URL_ART = "/stock-ws/art";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_image);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        myUser = sharedPrefs.getString("prefUsername", "");

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        ws_type = Globals.getValue("warehouse_type");

        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        URL_ART = Globals.getValue("warehouse_url") + "/art";

        MyTimeUtils = new TimeUtils();
        MyTimeUtils.setOrientation(OrderImageActivity.this, OrderImageActivity.this);

        imageView = (ImageView) findViewById(R.id.imageView);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String valueID = extras.getString("ID");
            id_image = valueID;
        }

        new LoadOrderImage().execute();

    }

    class LoadOrderImage extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog1 = new ProgressDialog(OrderImageActivity.this);
            pDialog1.setTitle("Loading image");
            pDialog1.setMessage("Please wait...");
            pDialog1.setIndeterminate(false);
            pDialog1.setCancelable(true);
            pDialog1.show();
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            ws_type = Globals.getValue("warehouse_type");

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            orderImage = new cOrderImage();

            orderImage = MyProvider.getOrderImage(id_image);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!orderImage.image.equals("")) {
                        imageView = (ImageView) findViewById(R.id.imageView);

                        new DownloadImageTask(imageView).execute(URL_ART + "/" + orderImage.image);

                        //   myBitmap = ConvertByteArrayToBitmap(orderImage.image);

                        //   imageView.setImageBitmap(myBitmap);
                        //   imageView.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, 400, 400, true));
                    }
                }
            });

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String result) {
            // dismiss the dialog after getting all products

            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog1.isShowing())
                pDialog1.dismiss();

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            Log.d("digiorder_logs","downloading image from url = " + urldisplay);

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                // remember image
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                myImageData = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

            } catch (Exception e) {
                //    Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if (result != null) bmImage.setImageBitmap(result); else bmImage.setVisibility(View.GONE);
        }
    }

    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        Bitmap decodedByte = null;
        if(b.isEmpty() || b.contentEquals(""))
        {
            decodedByte = null;
        }else{
            byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        }

        return decodedByte;
    }
}
