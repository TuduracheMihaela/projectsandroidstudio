package com.sharethatdata.digiorder.orders;

import java.util.Calendar;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

public class AddOrderActivity extends Activity{
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	
	private String myUser = "";
	private String myPass = "";
	
	ProgressDialog progressDialog;
	
	private int mYear,mMonth,mDay;
		
	TextView dateTextView;
	Button btnCreate;

	
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_edit_order);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
	
	
		Calendar c = Calendar.getInstance();
	    mYear = c.get(Calendar.YEAR);
	    mMonth = c.get(Calendar.MONTH);
	    mDay = c.get(Calendar.DAY_OF_MONTH);
	    
	    dateTextView.setText(new StringBuilder().append(pad(mDay)).append("-")
	            .append(pad(mMonth + 1)).append("-")
	            .append(pad(mYear)));
		
		//////////////////////////////////////////////////////////////
		/*
		 * EDIT
		 */
		
		String edit = Globals.getValue("edit");
        if(edit != ""){
       	    btnCreate.setVisibility(View.VISIBLE);
       	    btnCreate.setText("Edit order");
        }else{
        	btnCreate.setVisibility(View.VISIBLE);
        	btnCreate.setText("Add new order");
        }
		
	}

	
	private Object pad(int mMinute2) {
        if (mMinute2 >= 10)
            return String.valueOf(mMinute2);
        else
            return "0" + String.valueOf(mMinute2);
    }
	
	public void onAddProject(View v){
		String edit = Globals.getValue("edit");
        if(edit != ""){
       	    // edit task
        	String id = Globals.getValue("orderid");
        	new EditOrderTask(id).execute();
        }else{
        	new RegisterOrderTask().execute();
        }
		
	}
    
	public void onSelectDate(View v) 
	{
	    DatePickerDialog dp1 = new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    dp1.show();
	}
	
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		 
		  public void onDateSet(DatePicker view, int year, int month, int day) {
	            mYear = year;
	            mMonth = month;
	            mDay = day; 
	            		
	            TextView startDateView = (TextView) findViewById(R.id.textOrderDateTime);
	    		        
	            startDateView.setText(new StringBuilder().append(pad(mDay)).append("-")
	                .append(pad(mMonth + 1)).append("-")
	                .append(pad(mYear)));
	    }
	};
	
		
	  /** AsyncTask register new order  */
    private class RegisterOrderTask extends AsyncTask<String, String, Boolean>{
 
        @Override
        protected Boolean doInBackground(String... args) {
 
        	  String myDate = (new StringBuilder().append(pad(mYear)).append("-").append(pad(mMonth + 1)).append("-").append(pad(mDay))).toString();
     		  
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			  
			  // create order
        	  return true;
        }
           
        /**
	     * After completing background task Dismiss the progress dialog
	     * **/
        protected void onPostExecute(final Boolean success) {
        	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	        	
	            public void run() {
	            
	            	if (success == false)
	            	{
	            		 Toast.makeText(AddOrderActivity.this, "Failed to create order", Toast.LENGTH_SHORT).show();
		    		}else{
		    			Intent intent = new Intent(getApplicationContext(), MyOrderListActivity.class); startActivity(intent); 
		    		}
         	   	
	              }
	        });  
	    }
    }
    
    
    private class EditOrderTask extends AsyncTask<String, String, Boolean> {
		  String myID = "";
		  
		  public EditOrderTask(String id) {
		     myID = id;
		  }

		  protected Boolean doInBackground(String... urls) {
        	
			  // call web method
			  MyProvider = new WSDataProvider(myUser, myPass);
			  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			 
			  // update order
			  
			   return true;
  			  
		  }

		  protected void onPostExecute(Boolean result) {
			  
			  Globals.setValue("order_update", "order_update");
			  Globals.setValue("edit","");
			  if(result == true){
				  Toast.makeText(getApplicationContext(), "Order updated", Toast.LENGTH_SHORT).show();
			  }else{
				  Toast.makeText(getApplicationContext(), "Failed to update order", Toast.LENGTH_SHORT).show();
			  }
			  finish();
			  
		  }
	}
    	
}
