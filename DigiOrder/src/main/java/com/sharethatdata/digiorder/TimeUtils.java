package com.sharethatdata.digiorder;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;

public class TimeUtils {

	public static final int maxwork_hours = 8;
	
	public String getFormattedHourMinutes(int intNum)
	{
		String retString = "";
		
		float fNum = (intNum / 60);
		float fminute = (((float) intNum / 60) - fNum) * 60;
       	
		if (fminute > 0)
       		retString = String.format("%.0f", fNum) + "h" + String.format("%.0f", fminute) + "m";
       	else 
       		retString = String.format("%.0f", fNum) + "h";
       	
       	return retString;
	}
	
	public String getFormattedHourMinutesLeft(int intNum)
	{
		String retString = "";
		
		int ihour = (intNum / 60);
		float fminute = (((float) intNum / 60) - ihour) * 60;
		
		int ilefthour = (int) (maxwork_hours - ihour);
		
		if (intNum > 0)
		{
		
			float fleftminute = 60 - fminute;
					
			if (fleftminute == 60 && ilefthour == 0) 
				ilefthour = -1;
			else 
				if (fleftminute > 0 ) ilefthour = ilefthour - 1;	
			
			if (ilefthour >= 0)
			{
				float flefthour = ilefthour;
			  
				if (fleftminute > 0)
		       		retString = String.format("%.0f", flefthour) + " hours and " + String.format("%.0f", fleftminute) + " minutes";
		       	else 
		       		retString = String.format("%.0f", flefthour) + " hours";
		       	
		       	retString = retString + " not registered.";
			} else {
				retString = "";
				
			}
		}
       	return retString;
	}

	public double tabletSize(Context context) {
		double size = 0;
		try {
			// Compute screen size
			DisplayMetrics dm = context.getResources().getDisplayMetrics();
			float screenWidth  = dm.widthPixels / dm.xdpi;
			float screenHeight = dm.heightPixels / dm.ydpi;
			size = Math.sqrt(Math.pow(screenWidth, 2) +
					Math.pow(screenHeight, 2));
		} catch(Throwable t) {

		}
		return size;
	}

	public void setOrientation(Context context, Activity activity){
		double size_tablet = tabletSize(context);
		if (size_tablet > 9) {
			//Device is a 10" tablet
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		}else if (size_tablet > 6) {
			//Device is a 7" tablet
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		}else{
			// Device is a phone
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
	}

}
