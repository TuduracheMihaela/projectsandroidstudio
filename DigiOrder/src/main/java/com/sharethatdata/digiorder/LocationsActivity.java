package com.sharethatdata.digiorder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.sharethatdata.digiorder.adapter.EditLocationsAdapter;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.digiorder.adapter.EditLocationsAdapter.DeleteButtonAdapterCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LocationsActivity extends Activity implements DeleteButtonAdapterCallback {

    MyGlobals Globals;
    WSDataProvider MyProvider = null;
    DataProvider MyRobotProvider = null;

    private String ws_type = "";

    private String myUser = "";
    private String myPass = "";
    cResource resource;
    private String lastResID="0";
    private String lastLocID="0";
    private String lastLocName="";

    private List<cLocation> locations = null;
    private String myLocation = "";
    ArrayList<HashMap<String, String>> locationsList;
    ListView lv = null;
    Button btnEdit = null;

    private static ProgressDialog pDialog1;
    private static AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_detail);

        SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
        myUser = sharedPrefs.getString("prefUsername", "");

        Globals = ((MyGlobals) getApplicationContext());
        myUser = Globals.getValue("user");
        myPass = Globals.getValue("pass");

        ws_type = Globals.getValue("warehouse_type");


        resource = new cResource();
        MyProvider = new WSDataProvider(myUser, myPass);
        MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

        lastResID = Globals.getValue("lastResourceSelected");
        lastLocID = Globals.getValue("lastLocationSelected");

        Intent intent = getIntent();
        lastLocID = intent.getStringExtra("Location");

        lv = (ListView) findViewById(R.id.listViewLocations);

        new LoadResourceTask().execute();

        new LoadListLocations().execute();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnEdit = (Button) findViewById(R.id.buttonAddLocation);
        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                EditText EditText = (EditText) findViewById(R.id.textAddLocation);
                String locname = EditText.getText().toString();

                if (!locname.equals("")) {
                    lastLocName = locname;

                    AddLocation(locname, lastResID);
                }
            }

        });

    }

    private void DeleteLocation(String locid, String locname)
    {

        alertDialog = new AlertDialog.Builder(LocationsActivity.this).create();

        AlertDialog.Builder builder = new AlertDialog.Builder(LocationsActivity.this);

        builder.setTitle("Delete Location");
        builder.setMessage("Delete " + locname + "? All stock will be removed!");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    // register in ws, attach to order
                    new DeleteLocationTask().execute();

                    dialog.dismiss();

                 }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();

                }
            });

            alertDialog = builder.create();
            alertDialog.show();

    }

    private void AddLocation(String locname, String resid)
    {

        alertDialog = new AlertDialog.Builder(LocationsActivity.this).create();

        AlertDialog.Builder builder = new AlertDialog.Builder(LocationsActivity.this);

        builder.setTitle("Add Location");
        builder.setMessage("Add " + locname + " to this product?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                // register in ws, attach to order
                new AddLocationTask().execute();

                dialog.dismiss();

            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();

            }
        });

        alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void deletePressed(int position) {

        HashMap<String, String> itemsList = (HashMap<String, String>) locationsList.get(position);
        String itemId = (String) itemsList.get("id");

        lastLocID = itemId;

        String selected_location = (String) itemsList.get("location");
        DeleteLocation(itemId, selected_location);

    }

    private class LoadResourceTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... args)
        {
            String ws_type = Globals.getValue("warehouse_type");

            if (ws_type.equals("robot"))
            {
                resource = MyRobotProvider.getResource(lastResID);
            } else {
                resource = MyProvider.getResource(lastResID);
            }

            return "";
        }
        protected void onPostExecute(String file_url)
        {
            EditText resname = (EditText) findViewById(R.id.editText_resname);
            resname.setText(resource.code + ' ' + resource.name);

            Globals.setValue("lastResourceSelected", String.valueOf(resource.id));
        }
    }

    private class DeleteLocationTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... args)
        {
            String ws_type = Globals.getValue("warehouse_type");

            if (ws_type.equals("robot"))
            {

            } else {
                MyProvider.deleteProductLocation(lastLocID, lastResID);
            }

            return "";
        }
        protected void onPostExecute(String file_url)
        {
            Toast.makeText(LocationsActivity.this, "The location was removed.", Toast.LENGTH_LONG).show();

            new LoadListLocations().execute();
        }
    }

    private class AddLocationTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... args)
        {
            String ws_type = Globals.getValue("warehouse_type");

            if (ws_type.equals("robot"))
            {

            } else {
                MyProvider.addLocation(lastLocName, "WebShop Location");
                MyProvider.addProductLocation(lastLocName, lastResID);
            }

            return "";
        }
        protected void onPostExecute(String file_url)
        {
            Toast.makeText(LocationsActivity.this, "The location was added.", Toast.LENGTH_LONG).show();

            new LoadListLocations().execute();
        }
    }

    class LoadListLocations extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (pDialog1 == null)
            {
                pDialog1 = new ProgressDialog(LocationsActivity.this);
                pDialog1.setTitle("Loading locations");
                pDialog1.setMessage("Please wait...");
                pDialog1.setIndeterminate(false);
                pDialog1.setCancelable(true);
                pDialog1.show();
            }
        }

        /**
         * getting items from url
         * */
        protected String doInBackground(String... args) {

            Globals = ((MyGlobals) getApplicationContext());

            String ws_type = Globals.getValue("warehouse_type");

            myUser = Globals.getValue("user");
            myPass = Globals.getValue("pass");
            MyProvider = new WSDataProvider(myUser, myPass);
            MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

            List<cLocation> locations = new ArrayList<cLocation>();

            if (ws_type.equals("robot"))
            {
                MyRobotProvider = new DataProvider();
                MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");

                locations = MyRobotProvider.getLocations(lastResID);

            } else {
                locations = MyProvider.getLocations(lastResID);

            }

            // load data from provider
            locationsList = new ArrayList<HashMap<String, String>>();

            for(cLocation entry : locations)
            {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", String.valueOf(entry.id));
                map.put("location", entry.code);
                map.put("name", entry.name);
                map.put("stock", String.valueOf(entry.stock));

                // adding HashList to ArrayList
                locationsList.add(map);
            }

            return "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // Dismiss the progress dialog
            if (pDialog1 != null)
                pDialog1.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    EditLocationsAdapter adapter = null;

                    lv.setVisibility(View.VISIBLE);

                    adapter = new EditLocationsAdapter(LocationsActivity.this, R.layout.edit_items_locations, locationsList);
                    adapter.setCallback(LocationsActivity.this);
                    lv.setAdapter(adapter);
                    setListViewHeightBasedOnChildren(lv);

                }
            });
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

        listView.setLayoutParams(params);
        listView.requestLayout();

    }

}
