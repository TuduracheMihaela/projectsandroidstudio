package com.sharethatdata.digiorder;

import java.io.InputStream;
import java.util.List;

import com.sharethatdata.digiorder.R;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ScanResourceActivity extends Activity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;

	TimeUtils MyTimeUtils = null;

	private String ws_type = "";
		
	private String myUser = "";
	private String myPass = "";
	private String myUserID = "1";
	private String lastResID="0";
	private String lastLocCode="";
	private String lastLocID="0";
	private String lastOwnerID="0";
	private String lastGroupID="0";
	private int resGroupLastSelIndex=0;
	cResource resource;
	private long myAmount = 1;
	private long myBookAmount = 1;
	private String myLocation = "";
	
	private List<cLocation> locations = null;
	
	String myImageData = "";
    private String image_string = "";
    private String URL_ART = "http://www.pneumatiekvoordeel.nl/pages/art/large";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resource_scan_window);
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
						
		Globals = ((MyGlobals) getApplicationContext());	
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		ws_type = Globals.getValue("warehouse_type");
		
		resource = new cResource();
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(ScanResourceActivity.this, ScanResourceActivity.this);

		lastResID = Globals.getValue("lastResourceSelected");
		lastLocCode = Globals.getValue("lastLocationSelected");
		
		Intent intent = getIntent();
		lastLocCode = intent.getStringExtra("Location");
		
		if (ws_type.equals("robot"))
	    {
			URL_ART = "http://" + Globals.getValue("warehouse_url") + ":8002";
			
	    	MyRobotProvider = new DataProvider();
	    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
	    }
		
		Button btnEdit = (Button) findViewById(R.id.button_edit);
		btnEdit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) 
			  {
				  EditText resamount = (EditText) findViewById(R.id.editText_Amount);
				  myAmount = Integer.parseInt(resamount.getText().toString());
				  
				  AskUserConfirmation();
	
			  }
		 
		});
			
		EditText textamount = (EditText) findViewById(R.id.editText_Amount);
		textamount.addTextChangedListener(new TextWatcher() {
			 
			   @Override
			   public void afterTextChanged(Editable s) {}

			   @Override    
			   public void beforeTextChanged(CharSequence s, int start,
			     int count, int after) {
			   }

			   @Override    
			   public void onTextChanged(CharSequence s, int start,
			     int before, int count) {
			      if(s.length() != 0)
			      {
			    	  EditText bookamount = (EditText) findViewById(R.id.editText_BookAmount);
			    	  try {
			    		  
			    		  myAmount = Integer.parseInt(s.toString());
			    		  bookamount.setText(String.valueOf(myAmount * resource.bookamount));
			    		  
			    	  } catch(NumberFormatException nfe) {
						  
							
					  } 
			      }  
			    	  
			   }
		  });
		
		Button btnUp = (Button) findViewById(R.id.button_up);
		btnUp.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) 
			  {
				  EditText resamount = (EditText) findViewById(R.id.editText_Amount);
			//	  EditText bookamount = (EditText) findViewById(R.id.editText_BookAmount);
				  try {
					  myAmount = Integer.parseInt(resamount.getText().toString());
					
					  myAmount++;
					  
					  resamount.setText(String.valueOf(myAmount));
			//		  bookamount.setText(String.valueOf(myAmount * resource.bookamount));
					  
				  } catch(NumberFormatException nfe) {
					  
					
				  } 
			  }
		 
		});
		
		Button btnDown = (Button) findViewById(R.id.button_down);
		btnDown.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) 
			  {
				  EditText resamount = (EditText) findViewById(R.id.editText_Amount);
			//	  EditText bookamount = (EditText) findViewById(R.id.editText_BookAmount);
	        	  
				  try {
					  myAmount = Integer.parseInt(resamount.getText().toString());
					
					  myAmount--;
					  
					  resamount.setText(String.valueOf(myAmount));
					  
					//  bookamount.setText(String.valueOf(myAmount * resource.bookamount));
					  
				  } catch(NumberFormatException nfe) {
					  
					
				  } 
        	      
	
			  }
		 
		});

	 final ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
		 
//	 mBcr = new McBcrConnection(getBaseContext());
	 
	 new LoadResourceTask().execute();
		
	 new LoadStockTask().execute();
	 
	 this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
	 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.detail_resource, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void AskUserConfirmation()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
	    builder.setTitle("Confirm booking");
	    builder.setMessage("Book " + String.valueOf(myAmount * myBookAmount) + " on location " + myLocation + "?");
	    
	    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing but close the dialog
	    	    new RegisterStockTask().execute();
	            
	            Toast.makeText(getApplicationContext(), "The stock on location " + myLocation + " is updated with " + String.valueOf(myAmount * myBookAmount), Toast.LENGTH_LONG).show();
	            
	         //   Intent intent = new Intent(getApplicationContext(), SelectActionActivity.class);
			//	startActivity(intent);
		    	
	            dialog.dismiss();

				finish();
	            	                        
	        }
	    });

	    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	            // Do nothing
	            dialog.dismiss();
	            
	        }
	    });

	    AlertDialog alert = builder.create();
	    alert.show();
	}
    
    private class LoadResourceTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... args) 
        {
        	String ws_type = Globals.getValue("warehouse_type");
    		
    		if (ws_type.equals("robot"))
		    {
     			resource = MyRobotProvider.getResource(lastResID);
		    } else {
		    	resource = MyProvider.getResource(lastResID);
		    }
     		
    		if (!resource.image_id.equals("0"))
    		{
    			if (ws_type.equals("robot"))
    		    {
				  
				  new DownloadImageTask((ImageView) findViewById(R.id.imageView))
			        .execute(URL_ART + "/getImage?imageid="+ resource.id);
				  
    		    } else {
				  myImageData = MyProvider.getImage(resource.image_id);
    		    }
    			image_string = myImageData;
				  image_string = myImageData;
			} else {
				if (ws_type.equals("robot"))
    		    {
    				
    		    } else {
				  new DownloadImageTask((ImageView) findViewById(R.id.imageView))
			        .execute(URL_ART + "/" + resource.code + ".jpg");
    		    }
			}
        	
        	return ""; 
        }
		 protected void onPostExecute(String file_url) 
		 { 
	      	   TextView resname = (TextView) findViewById(R.id.editText_resname);
	           resname.setText(resource.code + ' ' + resource.name);
	            
	    	   EditText resbarcode = (EditText) findViewById(R.id.editText_barcode);
	    	   resbarcode.setText(resource.barcode);
	    	       	   	    	   	    	   
	    	   Globals.setValue("lastResourceSelected", String.valueOf(resource.id));
	    	   	    		
	      	   ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
  				  
        	   Bitmap d = ConvertByteArrayToBitmap(myImageData);
        	   if (d != null)
        	   {
        		   int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
        		   Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
        		   imageView1.setImageBitmap(scaled);
  					  				  
        	   }
        	   
        	   EditText resamount = (EditText) findViewById(R.id.editText_Amount);
        	   resamount.setText("1");
        	   
        	   TextView bookamount_view = (TextView) findViewById(R.id.text_BookAmount);
        	   bookamount_view.setText("x " + String.valueOf(resource.bookamount) + " pieces =");
	    	        	   
        	   EditText bookamount = (EditText) findViewById(R.id.editText_BookAmount);
        	   bookamount.setText(String.valueOf(resource.bookamount));
        	   
        	   myBookAmount = resource.bookamount;
	    	   
   //     	   mBcr.scan(true); // start scanning
		 }
    }
    
    private class LoadStockTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... args) 
        {
        	String ws_type = Globals.getValue("warehouse_type");
    		
    		if (ws_type.equals("robot"))
		    {
    			locations = MyRobotProvider.getLocations(lastResID);
		    } else {
		    
		    	locations = MyProvider.getLocations(lastResID);
		    }
    		
        	return ""; 
        }
		 protected void onPostExecute(String file_url) 
		 { 
				 long total_stock = -999999; 
				 String loc = lastLocCode;
				 
				 // always take first 
				 if (locations.size() > 0) loc = locations.get(0).code;
				 
				 for(cLocation entry : locations)
		    	 {
					 // if location is set, look for this loc
					 if (entry.code.equals(lastLocCode) && !lastLocCode.equals(""))
					 {
						 total_stock = entry.stock;
						 loc = entry.code;
					 
						 lastLocID = String.valueOf(entry.id);
		    	 
						 break;
					 } else {
						 if (entry.stock > total_stock)
						 {						
							 total_stock = entry.stock;
							 loc = entry.code;
							 
							 lastLocID = String.valueOf(entry.id);
						 }
					 }
		    	 }
			
			   TextView resbarcode = (TextView) findViewById(R.id.editText_barcode);
			   resbarcode.setText(loc);
			 
	      	   TextView reslocation = (TextView) findViewById(R.id.editText_location);
	      	   reslocation.setText(loc);
	            
	    	   EditText resstock = (EditText) findViewById(R.id.editText_stock);
	    	   resstock.setText(String.valueOf(total_stock));
	    	   
	    	   //register location code
	    	   myLocation = loc;
	    	   
	    	 
		 }
    }
    
    
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
  	  ImageView bmImage;

  	  public DownloadImageTask(ImageView bmImage) {
  	      this.bmImage = bmImage;
  	  }

  	  protected Bitmap doInBackground(String... urls) {
  	      String urldisplay = urls[0];
  	      
  	      Log.d("digiorder_logs","downloading image from url = " + urldisplay);
  			   
  	      Bitmap mIcon11 = null;
  	      try {
  	        InputStream in = new java.net.URL(urldisplay).openStream();
  	        mIcon11 = BitmapFactory.decodeStream(in);
  	      } catch (Exception e) {
  	          Log.e("Error", e.getMessage());
  	          e.printStackTrace();
  	      }
  	      return mIcon11;
  	  }

  	  protected void onPostExecute(Bitmap result) {
  	      bmImage.setImageBitmap(result);
  	  }
	}
    
    
    private class RegisterStockTask extends AsyncTask<String, String, Boolean>
    {
    	 
		@Override
        protected Boolean doInBackground(String... args) 
        {
		
		     Log.d("digiorder_logs","updating stock on locationid = " + lastLocID + " , resourceid = " + lastResID);
			  
		     String ws_type = Globals.getValue("warehouse_type");
    		
    		if (ws_type.equals("robot"))
		    {
    			return MyRobotProvider.updateStock(lastResID, lastLocID, myAmount);
		    } else {
		    
		    	return MyProvider.updateStock(lastResID, lastLocID, myAmount, false);
		    }
		}
			   
		protected void onPostExecute(Boolean result) 
		{
			
		}
    }
    
    private Bitmap ConvertByteArrayToBitmap(String b)
 	{
 		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
 		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
 		
 		return decodedByte;		
 	}
 	
    
  }
