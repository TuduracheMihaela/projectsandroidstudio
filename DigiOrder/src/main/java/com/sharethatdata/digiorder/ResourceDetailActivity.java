package com.sharethatdata.digiorder;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.adapter.LocationsAdapter;
import com.sharethatdata.digiorder.adapter.MutationsAdapter;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cMutation;
import com.sharethatdata.webservice.datamodel.cResource;
import com.sharethatdata.webservice.datamodel.cResourceGroup;
import com.sharethatdata.webservice.datamodel.cTimeRecord;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class ResourceDetailActivity extends Activity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	WSDataProvider MyProvider2 = null;
	WSDataProvider MyProvider3 = null;
	
	DataProvider MyRobotProvider = null;
	DataProvider MyRobotProvider2 = null;
	DataProvider MyRobotProvider3 = null;
	
	private static ProgressDialog pDialog1;
	private static ProgressDialog pDialog2;
	private static ProgressDialog pDialog3;
	private static ProgressDialog pDialog4;

	TimeUtils MyTimeUtils = null;

	private String ws_type = "";
	
	private String myUser = "";
	private String myPass = "";
	private String myUserID = "1";
	private String lastResID="0";
	private String lastOwnerID="0";
	private String lastGroupID="0";
	private String scanned = "";
	private int resGroupLastSelIndex=0;
	cTimeRecord myRecord;
	cResource resource;
	Spinner userSpinner = null;
	Spinner resourcesGroupSpinner = null;
	Spinner resourcesSpinner = null;
	
	ArrayList<HashMap<String, String>> resourceList;
	ArrayList<HashMap<String, String>> resourceGroupList;
	ArrayList<HashMap<String, String>> usersList;
	ArrayList<HashMap<String, String>> locationsList;
	ArrayList<HashMap<String, String>> mutationsList;
	
	List<String> resourcekeyArray =  new ArrayList<String>();
	List<String> userIdArray =  new ArrayList<String>();
	List<String> resGroupIdArray =  new ArrayList<String>();
	List<String> resIdArray =  new ArrayList<String>();
	List<String> resOwnerArray =  new ArrayList<String>();
	
	String myImageData = "";
    private String image_string = "";
    private String URL_ART = "/pages/art/large";
    
    ListView lv = null;
    ListView mv = null;
    Button btnEdit = null;

	private boolean blLoadingList = false;
    
	// For zoom image /////////////////////////
	// Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

	private boolean userIsInteracting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resource_details_window);
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
						
		Globals = ((MyGlobals) getApplicationContext());	
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		
		ws_type = Globals.getValue("warehouse_type");
		
	
		resource = new cResource();
		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		MyProvider2 = new WSDataProvider(myUser, myPass);
		MyProvider2.SetWebServiceUrl(Globals.getValue("ws_url"));	
		MyProvider3 = new WSDataProvider(myUser, myPass);
		MyProvider3.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(ResourceDetailActivity.this, ResourceDetailActivity.this);

		// reset url art
		URL_ART = Globals.getValue("warehouse_art_url");

		if (ws_type.equals("robot"))
	    {
			URL_ART = "http://" + Globals.getValue("warehouse_url") + ":8002";
			
	    	MyRobotProvider = new DataProvider();
	    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
	    	MyRobotProvider2 = new DataProvider();
	    	MyRobotProvider2.SetHost(Globals.getValue("warehouse_url"), "8002");
	    	MyRobotProvider3 = new DataProvider();
	    	MyRobotProvider3.SetHost(Globals.getValue("warehouse_url"), "8002");
		    
	    }
		
		// get last selected from last activity
		lastResID = Globals.getValue("lastResourceSelected");
		lastGroupID = Globals.getValue("lastResourceGroupSelected");
		scanned = Globals.getValue("lastScanned");
				
		
		resourcesGroupSpinner = (Spinner) findViewById(R.id.mainSpinnerResourceGroups);
		resourcesSpinner = (Spinner) findViewById(R.id.mainSpinnerResources);
	    
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		
		lv = (ListView) findViewById(R.id.listViewLocations);
		mv = (ListView) findViewById(R.id.listViewMutations);
		  		
		btnEdit = (Button) findViewById(R.id.button_edit);
		btnEdit.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) 
			  {
				  Intent intent = new Intent(v.getContext(), ScanResourceActivity.class);
					 
				  startActivity(intent); 
	
			  }
		 
		});
		

		// clear screen
  		ClearScreen();

  		final ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
		 
		 imageView1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 if (myImageData != null) zoomImageFromThumb(imageView1, myImageData);
				
			}
		});
		 
		 // Retrieve and cache the system's default "short" animation time.
	        mShortAnimationDuration = getResources().getInteger(
	                android.R.integer.config_shortAnimTime);
	

	     
	     	lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
				if (locationsList.size() > 0)
				{
					HashMap<String, String> itemsList = (HashMap<String, String>) locationsList.get(position);
					 String itemId = (String) itemsList.get("id");
					 String itemName = (String) itemsList.get("name");
					 String itemLocation = (String) itemsList.get("location");
					 String itemStock = (String) itemsList.get("stock");
					 Intent intent = new Intent(ResourceDetailActivity.this, ScanResourceActivity.class);
					 intent.putExtra("edit_action", "edit_location");
					 
					 intent.putExtra("ID", itemId);
					 intent.putExtra("Name", itemName);
					 intent.putExtra("Location", itemLocation);
					 intent.putExtra("Stock", itemStock);
					
					 startActivity(intent);
				}
			}

		});	
	}

	@Override
	public void onResume() {
		super.onResume();

		Log.e("digiorder_logs","on resume");

		lastResID = Globals.getValue("lastResourceSelected");

		if (resourcesSpinner.getCount() == 0)
			new LoadList().execute();

		if (!blLoadingList)
		   new LoadResourceTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resource, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (id == R.id.action_locations) {
			Intent intent2 = new Intent(this, LocationsActivity.class);
			startActivity(intent2);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		userIsInteracting = true;
	}

	/**
	 * Background Async Task to Load all spinner data by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() 
     	{
         super.onPreExecute();

		blLoadingList = true;
		Log.e("digiorder_logs","set loading list on");


	 	 pDialog1 = new ProgressDialog(ResourceDetailActivity.this);
         pDialog1.setTitle(getString(R.string.loading_resources));
         pDialog1.setMessage(getString(R.string.please_wait));
         pDialog1.setIndeterminate(false);
         pDialog1.setCancelable(true);
         pDialog1.show();
         
 		resourcesGroupSpinner = (Spinner) findViewById(R.id.mainSpinnerResourceGroups);
 		resGroupLastSelIndex=resourcesGroupSpinner.getSelectedItemPosition();
     	}

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) 
	    {
	    	
	    	// load data from provider
	    	
    		//resource groups
    		resourceGroupList = new ArrayList<HashMap<String, String>>();
    		
    		List<cResourceGroup> resGroup_List = new ArrayList<cResourceGroup>();
    		if (ws_type.equals("robot"))
		    {
    			resGroup_List = MyRobotProvider2.getResourceGroup();
		    } else {
		    	resGroup_List = MyProvider2.getResourceGroup();
		    }
    		
    		for(cResourceGroup entry : resGroup_List)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  String.valueOf(entry.id));
                map.put("name", Html.fromHtml(entry.name).toString());
                map.put("description", Html.fromHtml(entry.description).toString());
              
                // adding HashList to ArrayList
                resourceGroupList.add(map);
             }   
    		
    		//resources
        	resourceList = new ArrayList<HashMap<String, String>>();
        	
        	// load data from provider
        	List<cResource> resList = new ArrayList<cResource>();
        	if (ws_type.equals("robot"))
		    {
    			resList = MyRobotProvider3.getResources(lastGroupID);
		    } else {
		    	resList = MyProvider3.getResources(lastGroupID, "");
		    }

    		for(cResource entry : resList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("user", entry.User);
                // adding HashList to ArrayList
               resourceList.add(map);
             }

	    	return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {

	    	// Dismiss the progress dialog
            if (pDialog1 != null)
                pDialog1.dismiss();
	    	
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() 
	            {
	             	List<String> resGroupsArray =  new ArrayList<String>();
	            	List<String> resArray =  new ArrayList<String>();
	   
           			//resourceGroupsSpinner
	           	    for (HashMap<String, String> map : resourceGroupList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "id") resGroupIdArray.add(entry.getValue());
	           	        	if (entry.getKey() == "name") resGroupsArray.add(entry.getValue());
	           	        }

           	    	ArrayAdapter<String> resgroup_adapter = new ArrayAdapter<String>(ResourceDetailActivity.this, android.R.layout.simple_spinner_item, resGroupsArray);
           	    	resgroup_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           	    	resourcesGroupSpinner.setAdapter(resgroup_adapter);
           		       	
           	    	resourcesGroupSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
      			    @Override
      			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
	      			    {
	      			    	if (position!=resGroupLastSelIndex)
	      			    	{
								Log.e("digiorder_logs","loading list = " + blLoadingList);
								if (!blLoadingList) {
									// clear screen
									ClearScreen();

									if (scanned.equals("")) {
										int myIndex = resourcesGroupSpinner.getSelectedItemPosition();
										resGroupLastSelIndex = myIndex;
										lastGroupID = resGroupIdArray.get(myIndex);

										new ReloadDataTask().execute();
									} else {
										// auto select current
										int myIndex = resGroupIdArray.indexOf(lastGroupID);
										resGroupLastSelIndex = myIndex;
										resourcesGroupSpinner.setSelection(myIndex);
									}

								}
	      			    	}
	      			    }

          			    @Override
          			    public void onNothingSelected(AdapterView<?> parentView) {
          			     
          			    }

          			});
          			//end resourceGroupsSpinner
          			//resource spinner
	           	    for (HashMap<String, String> map : resourceList)
	           	        for (Entry<String, String> entry : map.entrySet())
	           	        {
	           	        	if (entry.getKey() == "id") resIdArray.add(entry.getValue());
	           	        	if (entry.getKey() == "name") resArray.add(entry.getValue());
	           	        	if (entry.getKey() == "user") resOwnerArray.add(entry.getValue());
	           	        }
           	    	ArrayAdapter<String> resources_adapter = new ArrayAdapter<String>(ResourceDetailActivity.this, android.R.layout.simple_spinner_item, resArray);
           	    	resources_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          	       	resourcesSpinner.setAdapter(resources_adapter);

          	       	// remember first id
          	        if (resIdArray.size() > 0 && lastResID.equals(""))
	           	    {
		           	    lastResID=resIdArray.get(0);
	           	    }
          	      
          	        resourcesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
          			    @Override
          			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
          			    {
							if (!blLoadingList)
          			    	{
          			    		if (scanned.equals(""))
	      	           	    	{
	          			    		int myIndex = resourcesSpinner.getSelectedItemPosition();
	          			    		lastResID = resIdArray.get(myIndex);
	          			    		lastOwnerID = resOwnerArray.get(myIndex);
	          			    		Log.e("digiorder_logs",String.valueOf(resOwnerArray.size())+" index sel : "+String.valueOf(myIndex)+"last owner id :"+lastOwnerID);
	              	          	    
	      	           	    	} else {
	      	           	    		// auto select current
	      	           	    		int myIndex = resIdArray.indexOf(lastResID);
	      	           	    		resourcesSpinner.setSelection(myIndex);
	      	           	    		Log.e("digiorder_logs",String.valueOf(resOwnerArray.size())+" index auto sel : "+String.valueOf(myIndex)+"last res id :"+lastResID);
          	          	    
	      	           	    	}
          			    		
          			    		// clear screen
          			    		ClearScreen();
          			    		
          			    		new LoadResourceTask().execute();
          			    		

          			  		
          			    	}
          			    }

          			    @Override
          			    public void onNothingSelected(AdapterView<?> parentView) {
          			     
          			    }

          			});
          	       	
          	       	
	        	    
	              }
	        });

			blLoadingList = false;
			Log.e("digiorder_logs","set loading list off");


	    }
	    

	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 /**
	 * Background Async Task to Load products by making HTTP Request
 	 * */
	class LoadListLocations extends AsyncTask<String, String, String> {

    /**
     * Before starting background thread Show Progress Dialog
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (pDialog2 == null)
        {
	        pDialog2 = new ProgressDialog(ResourceDetailActivity.this);
	        pDialog2.setTitle(getString(R.string.loading_locations));
	        pDialog2.setMessage(getString(R.string.please_wait));
	        pDialog2.setIndeterminate(false);
	        pDialog2.setCancelable(true);
	        pDialog2.show();
        }
    }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
	    	
	     	String ws_type = Globals.getValue("warehouse_type");
	 	   		    	
	    	myUser = Globals.getValue("user");
	    	myPass = Globals.getValue("pass");
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
		    List<cLocation> locations = new ArrayList<cLocation>();
		     
		    if (ws_type.equals("robot"))
		    {
		    	MyRobotProvider = new DataProvider();
		    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
		    	    	
		    	locations = MyRobotProvider.getLocations(lastResID);
		     
		    } else {
		    	locations = MyProvider.getLocations(lastResID);
			    
		    }
		    
	    	 // load data from provider
			locationsList = new ArrayList<HashMap<String, String>>();  
				
	    	 for(cLocation entry : locations)
	    	 {
   		       HashMap<String, String> map = new HashMap<String, String>();
               
               map.put("id", String.valueOf(entry.id));
               map.put("location", entry.code);
               map.put("name", entry.name);
               map.put("stock", String.valueOf(entry.stock));
               	                 
               // adding HashList to ArrayList
               locationsList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	    	 // Dismiss the progress dialog
            if (pDialog2 != null)
                pDialog2.dismiss();
            
	    	// updating UI from Background Thread
	    	runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
            	LocationsAdapter adapter = null;
       	
            	lv.setVisibility(View.VISIBLE);
           	  
	   			adapter = new LocationsAdapter(ResourceDetailActivity.this, R.layout.resource_items_locations, locationsList);
	   			lv.setAdapter(adapter);
	   			setListViewHeightBasedOnChildren(lv);
   		       			
	   	
	            }
	        });    
	    }  
	}
	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		* Background Async Task to Load products by making HTTP Request
		* */
		class LoadListMutations extends AsyncTask<String, String, String> {
		
		/**
		* Before starting background thread Show Progress Dialog
		* */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (pDialog3 == null)
			{
				pDialog3 = new ProgressDialog(ResourceDetailActivity.this);
				pDialog3.setTitle(getString(R.string.loading_mutations));
				pDialog3.setMessage(getString(R.string.please_wait));
				pDialog3.setIndeterminate(false);
				pDialog3.setCancelable(true);
				pDialog3.show();
			}
		}
		
		/**
		* getting items from url
		* */
		protected String doInBackground(String... args) {
		
			Globals = ((MyGlobals) getApplicationContext());	
			
			String ws_type = Globals.getValue("warehouse_type");
			
			myUser = Globals.getValue("user");
			myPass = Globals.getValue("pass");
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			List<cMutation> mutations = new ArrayList<cMutation>();
			
			if (ws_type.equals("robot"))
			{
				MyRobotProvider = new DataProvider();
				MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
				
				mutations = MyRobotProvider.getMutations(lastResID);
			
			} else {
				mutations = MyProvider.getMutations(lastResID);
			
			}
		
			// load data from provider
			mutationsList = new ArrayList<HashMap<String, String>>();  
			
			for(cMutation entry : mutations)
			{
				HashMap<String, String> map = new HashMap<String, String>();
				
				map.put("id", String.valueOf(entry.id));
				map.put("datetime", entry.datetime);
				map.put("location", entry.location);
				map.put("user", entry.user);
				map.put("product", entry.resource_code + " " + entry.resource);
                
                String reset_str = "";
                if (entry.mutation_type.equals("reset")) reset_str = " reset";
                
                map.put("amount", String.valueOf(entry.amount) + reset_str);
				
				// adding HashList to ArrayList
				mutationsList.add(map);
			}
			
		
			return "";
		}
		
		/**
		* After completing background task Dismiss the progress dialog
		* **/
		protected void onPostExecute(String file_url) {
			// Dismiss the progress dialog
			if (pDialog3 != null)
				pDialog3.dismiss();
				
				// updating UI from Background Thread
				runOnUiThread(new Runnable() {
				public void run() {
				
					/**
					* Updating parsed JSON data into ListView
					* */
					MutationsAdapter adapter = null;
					  
					mv.setVisibility(View.VISIBLE);
					   
					adapter = new MutationsAdapter(ResourceDetailActivity.this, R.layout.resource_items_mutations, mutationsList);
					mv.setAdapter(adapter);
					setListViewHeightBasedOnChildren(mv);
					
				
				}
			});    
		}  
	}
	
	
	// for listview in scrollview (without this method, the listview won't scroll)
	   public static void setListViewHeightBasedOnChildren(ListView listView) 
	   {
	       ListAdapter listAdapter = listView.getAdapter();
	       if (listAdapter == null)
	           return;

	       int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	       int totalHeight=0;
	       View view = null;

	       for (int i = 0; i < listAdapter.getCount(); i++) 
	       {
	           view = listAdapter.getView(i, view, listView);

	           if (i == 0)
	               view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,  
	                                         LayoutParams.MATCH_PARENT));

	           view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	           totalHeight += view.getMeasuredHeight();

	       }

	       ViewGroup.LayoutParams params = listView.getLayoutParams();
	       params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

	       listView.setLayoutParams(params);
	       listView.requestLayout();

	   }
	   
	
    private class ReloadDataTask extends AsyncTask<String, String, String>
    {
	   
        protected String doInBackground(String... args) 
        {
        	
        	resourceList = new ArrayList<HashMap<String, String>>();
        	
        	// load data from provider
        	Log.e("digiorder_logs","last group se :"+ lastGroupID);
        	        	
    		List<cResource> resList = new ArrayList<cResource>();
    				
    		if (ws_type.equals("robot"))
		    {
    			resList = MyRobotProvider3.getResources(lastGroupID);
		    } else {
		    	resList = MyProvider3.getResources(lastGroupID, "");
		    }
    		
    		for(cResource entry : resList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id",  entry.id);
                map.put("name", entry.name);
                map.put("code", entry.code);
                // adding HashList to ArrayList
               resourceList.add(map);
         	}
             

	    	return "";
        }
		 protected void onPostExecute(String file_url) 
		 {
			    
		        runOnUiThread(new Runnable() {
		            public void run() 
		            {
		            	resIdArray.clear();
		            	resOwnerArray.clear();
		            	List<String> resArray =  new ArrayList<String>();
		           	    for (HashMap<String, String> map : resourceList)
		           	        for (Entry<String, String> entry : map.entrySet())
		           	        {
		           	        	if (entry.getKey() == "id") resIdArray.add(entry.getValue());
		           	        	if (entry.getKey() == "name") resArray.add(entry.getValue());
		           	        	if (entry.getKey() == "code") resOwnerArray.add(entry.getValue());
		           	        }
		           	    if (resIdArray.size() > 0)
		           	    {
							if (scanned.equals("")) {
								lastResID = resIdArray.get(0);
							}
			       	    }
	           	    	ArrayAdapter<String> resources_adapter = new ArrayAdapter<String>(ResourceDetailActivity.this, android.R.layout.simple_spinner_item, resArray);
	           	    	resources_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	          	       	resourcesSpinner.setAdapter(resources_adapter);
		           	
	          	     

		            }
		            });
		 }
    }
    
    private class LoadResourceTask extends AsyncTask<String, String, String>
    {
    	  @Override
  	    protected void onPreExecute() {
  	        super.onPreExecute();
  	    
  	    }
    	  
        protected String doInBackground(String... args) 
        {
     		Log.e("digiorder_logs","loading resource with id : "+lastResID);


			if (ws_type.equals("robot"))
		    {
     			resource = MyRobotProvider3.getResource(lastResID);
		    } else {
		    	resource = MyProvider3.getResource(lastResID);
		    }
     		
    		if (!resource.image_id.equals("0"))
    		{
    			if (ws_type.equals("robot"))
    		    {
				  
				  new DownloadImageTask((ImageView) findViewById(R.id.imageView))
			        .execute(URL_ART + "/getImage?imageid="+ resource.id);
				  
    		    } else {
				  myImageData = MyProvider3.getImage(resource.image_id);
    		    }
    			image_string = myImageData;
			} else {
				if (ws_type.equals("robot"))
    		    {
    				
    		    } else {
				  new DownloadImageTask((ImageView) findViewById(R.id.imageView))
			        .execute(URL_ART + "/" + resource.code + ".jpg");
    		    }
			}
        	
    		scanned = "";
		    	
    		
			return ""; 
        }
		 protected void onPostExecute(String file_url) 
		 { 
			
	      	   TextView resname = (TextView) findViewById(R.id.editText_resname);
	           resname.setText(resource.name);
	            
	    	   TextView rescode = (TextView) findViewById(R.id.editText_rescode);
	    	   rescode.setText(resource.code);
	    	    
	    	   TextView resbarcode = (TextView) findViewById(R.id.editText_resbarcode);
	    	   resbarcode.setText(resource.barcode);
	    	   
	    	   EditText resdescription = (EditText) findViewById(R.id.editText_resdescription);
	    	   resdescription.setText(Html.fromHtml(resource.description));
	    	   
	    	   Log.e("digiorder_logs","setting loaded resource with id : "+ resource.code);
	    	   
	    	   Globals.setValue("lastResourceSelected", resource.id);
	    	   	    		
	    	   if (!resource.name.equals(""))
	    	   {  
	    		   btnEdit.setVisibility(View.VISIBLE);
	    	   }
	    	   
        	   ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
  			 	   
        	   if (myImageData != null)
        	   {
        		   imageView1.setVisibility(View.VISIBLE);
	        	   Bitmap d = ConvertByteArrayToBitmap(myImageData);
	        	   if (d != null)
	        	   {
	        		   int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
	        		   Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
	        		   imageView1.setImageBitmap(scaled);
	  					  				  
	        	   }
        	   }

			 new LoadListLocations().execute();

			 new LoadListMutations().execute();

        	  	// reset
          	  //  scanned = "";

		 }
    }
    
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    	  ImageView bmImage;

    	  public DownloadImageTask(ImageView bmImage) {
    	      this.bmImage = bmImage;
    	  }
    	  
    	  protected Bitmap doInBackground(String... urls) {
    	      String urldisplay = urls[0];
    	      
    	      Log.d("digiorder_logs","downloading image from url = " + urldisplay);
    			   
    	      Bitmap mIcon11 = null;
    	      try {
    	        InputStream in = new java.net.URL(urldisplay).openStream();
    	        mIcon11 = BitmapFactory.decodeStream(in);
    	        
    	        // remember image
    	        ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	        mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, stream);
    	        myImageData = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
    	        
    	      } catch (Exception e) {
    	      //    Log.e("Error", e.getMessage());
    	          e.printStackTrace();
    	      }
    	      return mIcon11;
    	  }

    	  protected void onPostExecute(Bitmap result) {
    		    
    		  if (result != null) bmImage.setImageBitmap(result); else bmImage.setVisibility(View.GONE);
    	  }
	}
    
    private class EditResourceTask extends AsyncTask<String, String, Boolean>
    {
    	 
		@Override
        protected Boolean doInBackground(String... args) 
        {
			 TextView resname = (TextView) findViewById(R.id.editText_resname);
		     String  res_name = resname.getText().toString();
		     
		     TextView rescode = (TextView) findViewById(R.id.editText_rescode);
		     String  res_code = rescode.getText().toString();
		     
		     EditText resdescription = (EditText) findViewById(R.id.editText_resdescription);
		     String  res_description = resdescription.getText().toString();
		     cResource new_res = new cResource();
		     
		     String res_id=String.valueOf(resource.id);
		     //String resource_group="0";
		     String image="";
		     Boolean inUse=false;
		     
		     new_res.id = res_id;
		     new_res.name = res_name;
		     new_res.code = res_code;
		     new_res.description = res_description;
		     new_res.groupID = lastGroupID;
		     new_res.InUse = inUse;
		     new_res.User=myUserID;
		     Log.d("digiorder_logs",res_id+" "+new_res.User+" "+myUserID);
		     Boolean editres = MyProvider.updateResource(new_res);
		     
        	resourceList = new ArrayList<HashMap<String, String>>();
        	
        	// load data from provider
    		List<cResource> resList = MyProvider2.getResources(lastGroupID, "");
    		for(cResource entry : resList)
         	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("id", entry.id);
                map.put("name", entry.name);
                map.put("user", entry.User);
                // adding HashList to ArrayList
               resourceList.add(map);
             }
			 return true; 
        }
		 protected void onPostExecute(Boolean result) 
		 {
			 
		 }
    }
    
    private void ClearScreen()
    {
	   TextView resname = (TextView) findViewById(R.id.editText_resname);
       resname.setText("");
   	   TextView rescode = (TextView) findViewById(R.id.editText_rescode);
   	   rescode.setText("");
   	   TextView resbarcode = (TextView) findViewById(R.id.editText_resbarcode);
   	   resbarcode.setText("");
   	   EditText resdescription = (EditText) findViewById(R.id.editText_resdescription);
   	   resdescription.setText("");
	   ImageView imageView1 = (ImageView) findViewById(R.id.imageView);
	   imageView1.setVisibility(View.GONE);
	   lv.setVisibility(View.GONE);
	   mv.setVisibility(View.GONE);
	   btnEdit.setVisibility(View.GONE);
    }
    
 // when get and show the image
 	private Bitmap ConvertByteArrayToBitmap(String b)
 	{
 		try
 		{
	 		byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
	 		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
	 		return decodedByte;		
 		} catch (Exception e)
 		{
 			return null;
 		}
		
 	}
 	
	public String resizeBase64Image(String base64image){
		int IMG_WIDTH = 800;
		int IMG_HEIGHT = 600;
	    byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT); 
	    BitmapFactory.Options options=new BitmapFactory.Options();
	    options.inPurgeable = true;
	    Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


	    if(image.getHeight() <= 400 && image.getWidth() <= 400){
	        return base64image;
	    }
	    image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

	    ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	    image.compress(Bitmap.CompressFormat.PNG,100, baos);

	    byte [] b=baos.toByteArray();
	    System.gc();
	    return Base64.encodeToString(b, Base64.NO_WRAP);

	} 
		
 	private void zoomImageFromThumb(final View thumbView, String imageResId) {
	    // If there's an animation in progress, cancel it
	    // immediately and proceed with this one.
	    if (mCurrentAnimator != null) {
	        mCurrentAnimator.cancel();
	    }

	    // Load the high-resolution "zoomed-in" image.
	    final ImageView expandedImageView = (ImageView) findViewById(
	            R.id.expanded_image);

	   //
	    byte[] decodedString;
	    if(imageResId != ""){
	    	String resizeString =  resizeBase64Image(imageResId);
	    	decodedString = Base64.decode(resizeString, Base64.NO_WRAP);
	    }else{
	    	decodedString = Base64.decode(imageResId, Base64.NO_WRAP);
	    }
	  
	  
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		  if (decodedByte != null)
		  {
			  //int nh = (int) ( decodedByte.getHeight() * (512.0 / decodedByte.getWidth()) );
			  //Bitmap scaled = Bitmap.createScaledBitmap(decodedByte, 512, nh, true);
			  expandedImageView.setImageBitmap(decodedByte);
			  				  
		  }
		
	    // Calculate the starting and ending bounds for the zoomed-in image.
	    // This step involves lots of math. Yay, math.
	    final Rect startBounds = new Rect();
	    final Rect finalBounds = new Rect();
	    final Point globalOffset = new Point();

	    // The start bounds are the global visible rectangle of the thumbnail,
	    // and the final bounds are the global visible rectangle of the container
	    // view. Also set the container view's offset as the origin for the
	    // bounds, since that's the origin for the positioning animation
	    // properties (X, Y).
	    thumbView.getGlobalVisibleRect(startBounds);
	    findViewById(R.id.scrollView)// the container
	            .getGlobalVisibleRect(finalBounds, globalOffset);
	    startBounds.offset(-globalOffset.x, -globalOffset.y);
	    finalBounds.offset(-globalOffset.x, -globalOffset.y);

	    // Adjust the start bounds to be the same aspect ratio as the final
	    // bounds using the "center crop" technique. This prevents undesirable
	    // stretching during the animation. Also calculate the start scaling
	    // factor (the end scaling factor is always 1.0).
	    float startScale;
	    if ((float) finalBounds.width() / finalBounds.height()
	            > (float) startBounds.width() / startBounds.height()) {
	        // Extend start bounds horizontally
	        startScale = (float) startBounds.height() / finalBounds.height();
	        float startWidth = startScale * finalBounds.width();
	        float deltaWidth = (startWidth - startBounds.width()) / 2;
	        startBounds.left -= deltaWidth;
	        startBounds.right += deltaWidth;
	    } else {
	        // Extend start bounds vertically
	        startScale = (float) startBounds.width() / finalBounds.width();
	        float startHeight = startScale * finalBounds.height();
	        float deltaHeight = (startHeight - startBounds.height()) / 2;
	        startBounds.top -= deltaHeight;
	        startBounds.bottom += deltaHeight;
	    }

	    // Hide the thumbnail and show the zoomed-in view. When the animation
	    // begins, it will position the zoomed-in view in the place of the
	    // thumbnail.
	    thumbView.setAlpha(0f);
	    expandedImageView.setVisibility(View.VISIBLE);

	    // Set the pivot point for SCALE_X and SCALE_Y transformations
	    // to the top-left corner of the zoomed-in view (the default
	    // is the center of the view).
	    expandedImageView.setPivotX(0f);
	    expandedImageView.setPivotY(0f);

	    // Construct and run the parallel animation of the four translation and
	    // scale properties (X, Y, SCALE_X, and SCALE_Y).
	    AnimatorSet set = new AnimatorSet();
	    set
	            .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
	                    startBounds.left, finalBounds.left))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
	                    startBounds.top, finalBounds.top))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
	            startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
	                    View.SCALE_Y, startScale, 1f));
	    set.setDuration(mShortAnimationDuration);
	    set.setInterpolator(new DecelerateInterpolator());
	    set.addListener(new AnimatorListenerAdapter() {
	        @Override
	        public void onAnimationEnd(Animator animation) {
	            mCurrentAnimator = null;
	        }

	        @Override
	        public void onAnimationCancel(Animator animation) {
	            mCurrentAnimator = null;
	        }
	    });
	    set.start();
	    mCurrentAnimator = set;

	    // Upon clicking the zoomed-in image, it should zoom back down
	    // to the original bounds and show the thumbnail instead of
	    // the expanded image.
	    final float startScaleFinal = startScale;
	    expandedImageView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	            if (mCurrentAnimator != null) {
	                mCurrentAnimator.cancel();
	            }

	            // Animate the four positioning/sizing properties in parallel,
	            // back to their original values.
	            AnimatorSet set = new AnimatorSet();
	            set.play(ObjectAnimator
	                        .ofFloat(expandedImageView, View.X, startBounds.left))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.Y,startBounds.top))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.SCALE_X, startScaleFinal))
	                        .with(ObjectAnimator
	                                .ofFloat(expandedImageView, 
	                                        View.SCALE_Y, startScaleFinal));
	            set.setDuration(mShortAnimationDuration);
	            set.setInterpolator(new DecelerateInterpolator());
	            set.addListener(new AnimatorListenerAdapter() {
	                @Override
	                public void onAnimationEnd(Animator animation) {
	                    thumbView.setAlpha(1f);
	                    expandedImageView.setVisibility(View.GONE);
	                    mCurrentAnimator = null;
	                }

	                @Override
	                public void onAnimationCancel(Animator animation) {
	                    thumbView.setAlpha(1f);
	                    expandedImageView.setVisibility(View.GONE);
	                    mCurrentAnimator = null;
	                }
	            });
	            set.start();
	            mCurrentAnimator = set;
	        }
	    });
	}
}
