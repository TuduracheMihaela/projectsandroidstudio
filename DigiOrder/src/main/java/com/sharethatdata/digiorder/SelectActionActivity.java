package com.sharethatdata.digiorder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.sharethatdata.digiorder.mutations.MutationListActivity;
import com.sharethatdata.digiorder.orders.MyOrderListActivity;
import com.sharethatdata.digiorder.orders.MyOrdersDetailActivity;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SelectActionActivity extends Activity {

	private static final String TAG = "";
    private Button btnSubmit1;
    private Button btnSubmit2;
    private Button btnSubmit3;
    private String myUser = "";
	private String myPass = "";
	private String myBarcode = "";

	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;

	TimeUtils MyTimeUtils = null;

	cResource resource;
	cOrder myOrder;
	boolean blUseCamera = false;
	boolean blLoadingResource = false;


	MyGlobals Globals;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Globals = ((MyGlobals)getApplication());
		
		String isManager = Globals.getValue("manager");
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");

		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
						
		Globals = ((MyGlobals) getApplicationContext());	
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		resource = new cResource();

		String ws_type = Globals.getValue("warehouse_type");

		if (ws_type.equals("robot")) {
			MyRobotProvider = new DataProvider();
			MyRobotProvider.SetHost(Globals.getValue("ws_url"), "8002");
		} else {
			MyProvider = new WSDataProvider(myUser, myPass);
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		}

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(SelectActionActivity.this, SelectActionActivity.this);

		boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
		
		if (blCameraScanner)
		{
			blUseCamera = true;
			setContentView(R.layout.activity_select_action_wcamera);
		} else {
			setContentView(R.layout.activity_select_action);
		}
				
		addListenerOnButton();
		
		// set visible buttons
		btnSubmit1.setVisibility(View.VISIBLE);
		
		btnSubmit2.setVisibility(View.VISIBLE); 
		
			EditText EditText = (EditText) findViewById(R.id.editText_barcode);

			EditText.addTextChangedListener(new TextWatcher() {

	          public void afterTextChanged(Editable s) {

	        	  // reset selected resource
	      	  	  Globals.setValue("lastResourceSelected", "0");
	      	  	  Globals.setValue("lastResourceGroupSelected", "0");
	      	  	  
	        	  
	        	  EditText EditText = (EditText) findViewById(R.id.editText_barcode);
	        	  String bc = EditText.getText().toString();
	        	  
	        	 // myBarcode = "";

				  // strip control chars
				  bc = bc.replace("*", "");
				  bc = bc.replace("\n", "");
				  bc = bc.replace("\r", "");

	        	  if (bc.length() > 6 && !blLoadingResource)
	        	  {
	        		  // found barcode
	        		  myBarcode = bc;

	        		  new LoadResourceByBarcodeTask().execute();

	        	  }

	        	  
	          }

	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	    });
		
			
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		
	}
	 
	  // get the selected dropdown list value
	  public void addListenerOnButton() {
	 
		btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
	 
		btnSubmit1.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  Globals = ((MyGlobals) getApplicationContext());
			  Globals.setValue("action", "orders");

			  Intent intent = new Intent(v.getContext(), MyOrderListActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
		
		btnSubmit2 = (Button) findViewById(R.id.btnSubmit2);
		 
		btnSubmit2.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
			  
			  Globals = ((MyGlobals) getApplicationContext());
			  Globals.setValue("action","products");
			  Globals.setValue("lastScanned", "");
			  
			  Intent intent = new Intent(v.getContext(), ResourceDetailActivity.class); 
			  startActivity(intent); 
		  }
	 
		});
		
		if (blUseCamera)
		{
			btnSubmit3 = (Button) findViewById(R.id.btnSubmitScan);
			 
			btnSubmit3.setOnClickListener(new OnClickListener() {
		 
			  @Override
			  public void onClick(View v) {
				  
				  	  Globals.setValue("lastScanned", "");
				  						  
					  // create barcode intent
					  IntentIntegrator integrator = new IntentIntegrator(SelectActionActivity.this);
					  integrator.initiateScan();
											 
				  }
				
			});
		}
				
		
	  }
	  
	  @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}

	@Override
	public void onResume()
	{
		super.onResume();
		EditText EditText = (EditText) findViewById(R.id.editText_barcode);
		EditText.setText("");
	}

	public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle item selection
	        switch (item.getItemId()) {
	        case R.id.action_settings:
	            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
	            return true;
	        case R.id.action_mutations:
	            Intent intent3 = new Intent(this, MutationListActivity.class); startActivity(intent3); 
	            return true;
			case R.id.action_status:
				Intent intent4 = new Intent(this, RobotStatusActivity.class); startActivity(intent4);
				return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
		    
	
	 
	   
		
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		  
	      IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		  if (scanResult != null) {
			
			  String UPCScanned = scanResult.getContents();
			  if (UPCScanned != null)
			  {
	              Log.d("barcode scanned", UPCScanned);
	              
	              EditText EditText = (EditText) findViewById(R.id.editText_barcode);
	        	  EditText.setText(UPCScanned);
	        	  
			  }	
              
           }
	  }
  
		
	  
	  private class LoadResourceByBarcodeTask extends AsyncTask<String, String, String>
	    {
	        protected String doInBackground(String... args) 
	        {
				blLoadingResource = true;

				List<cResource> myResources = new ArrayList<cResource>();
				String ws_type = Globals.getValue("warehouse_type");

				if (ws_type.equals("robot"))
				{
					MyRobotProvider = new DataProvider();
					MyRobotProvider.SetHost(Globals.getValue("ws_url"), "8002");

					cResource myresource = MyRobotProvider.getResourcebyLocationBarcode(myBarcode);

					myResources.add(myresource);

				} else {

					MyProvider = new WSDataProvider(myUser, myPass);
					MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

					myResources = MyProvider.getResourcesbyLocationBarcode(myBarcode);

				}
	        	if (myResources.size() > 0)
	        	{
	        		resource = myResources.get(0);
	        	 	Globals.setValue("lastLocationSelected", myBarcode);
	        		
	        	} else {
	     		  
	        	//	myResources = MyProvider.getResourcesbyBarcode(myBarcode);
	        	//	if (myResources.size() > 0)
	        	//	{
	        	//		resource = myResources.get(0);
	        	//	}
	        	}
	        	
				return ""; 
	        }
			
	        protected void onPostExecute(String file_url) 
			{ 	      	 
	        	if (resource.id != "0")
	        	{
	        		Log.e("digiorder","selected resource with id : "+ resource.id + ", " + resource.code);
		    	   
		    	  	Globals.setValue("lastResourceSelected", resource.id);
		    	  	Globals.setValue("lastResourceGroupSelected", resource.groupID);
		
		    	    Globals.setValue("lastScanned", myBarcode);
		    	  	
		    	  	resource = new cResource();
		    	  	
		    	  	// 	start resource detail
		    //	  	Intent intent = new Intent(getApplicationContext(), ScanResourceActivity.class); 
			//		startActivity(intent); 
		
		    	  	Intent intent = new Intent(getApplicationContext(), ResourceDetailActivity.class); 
		    	  	startActivity(intent); 
		    	  	
	        	} else {
	        		  	Toast.makeText(getApplicationContext(), getString(R.string.no_resource_found) + myBarcode, Toast.LENGTH_LONG).show();
	        		 
	        		  	Globals.setValue("lastScanned", "");

					// find tray barcode
						new LoadOrderByBarcodeTask().execute();

	        	}

				blLoadingResource = false;
			 }
	    }
	  
	  private class LoadOrderByBarcodeTask extends AsyncTask<String, String, String>
	    {
	        protected String doInBackground(String... args) 
	        {
				String myOrderID = MyProvider.getAssignedOrder(myBarcode);

				myOrder = MyProvider.getOrder(myOrderID);
	        	//myOrder = MyProvider.getOrderByBarcode(myBarcode);
	        	
				return ""; 
	        }
			
	        protected void onPostExecute(String file_url) 
			{ 	      	 
	        	if (myOrder.id != "")
	        	{
	        		Log.e("digiorder","selected order with id : "+ myOrder.ordernr + ", " + myOrder.uname);
		    	   
		    	  	Globals.setValue("lastOrderSelected", myOrder.ordernr);
		    	  	
		    	    Globals.setValue("lastScanned", myBarcode);
		    	  	
		    	  	Intent intent = new Intent(getApplicationContext(), MyOrdersDetailActivity.class); 
		    	  	intent.putExtra("edit_action", "edit_order");
					intent.putExtra("ID", myOrder.ordernr);
			
		    	  	startActivity(intent); 
		    	  	
	        	} else {
	        		//Toast.makeText(getApplicationContext(), "no order found with barcode " + myBarcode, Toast.LENGTH_LONG).show();
	        		 
	        		 Globals.setValue("lastScanned", "");

					EditText EditText = (EditText) findViewById(R.id.editText_barcode);
					EditText.setText("");
	        	}
			 }
	    }
	  
}


