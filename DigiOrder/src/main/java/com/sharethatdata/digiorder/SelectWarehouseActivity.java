package com.sharethatdata.digiorder;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.datamodel.cWebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class SelectWarehouseActivity extends Activity {

	private static final String TAG = "";
    private Button btnSubmit1;
    private String myUser = "";
	private String myPass = "";
	private String myBarcode = "";
	private String myIP = "";
	private String myAppName = "";
	private String myDevice = "";

	private static ProgressDialog pDialog;
	TimeUtils MyTimeUtils = null;

	Spinner wsSpinner = null;

	ArrayList<HashMap<String, String>> wsList;
	List<String> wskeyArray =  new ArrayList<String>();
	List<String> wsURLArray =  new ArrayList<String>();
	List<String> wsTypeArray =  new ArrayList<String>();
	List<String> wsArtURLArray = new ArrayList<String>();
	
	List<String> wsArray =  new ArrayList<String>();
	String selectWS = "";
	String selectURL = "";
	String selectType = "";
	String selectArtURL = "";

	private UserLoginTask mAuthTask = null;
	List<cWebService> ws_list =  new ArrayList<cWebService>();
	private int numTry = 0;

	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;

	MyGlobals Globals;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Globals = ((MyGlobals)getApplication());
		
		String isManager = Globals.getValue("manager");
		String isAdministrator = Globals.getValue("administrator");
		String isDeveloper = Globals.getValue("developer");
		String isEmployee = Globals.getValue("employee");

		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		myUser = sharedPrefs.getString("prefUsername", "");
						
		setContentView(R.layout.activity_select_warehouse);
		
		Globals = ((MyGlobals) getApplicationContext());	
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		myIP = Globals.getValue("ip");
		myAppName = Globals.getValue("module");
		myDevice = Globals.getValue("device");


		MyProvider = new WSDataProvider(myUser, myPass);
		MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

		MyProvider.SetDeviceIdentification(myIP, myDevice, myAppName);

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(SelectWarehouseActivity.this, SelectWarehouseActivity.this);

		// start service
		Intent i= new Intent(getBaseContext(), MyService.class);
		// potentially add data to the intent
		i.putExtra("WAIT", "100");
		getBaseContext().startService(i);

		addListenerOnButton();

		// set visible buttons
		btnSubmit1.setVisibility(View.VISIBLE);
					
		wsSpinner = (Spinner) findViewById(R.id.spinnerWS);

		numTry = 0;
		 
		new LoadWSList().execute();
		
	    new FindServicesTask().execute();
	    
	    final ImageView imageView1 = (ImageView) findViewById(R.id.refreshView);
		 
		 imageView1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 
				 numTry = 0;
				 new FindServicesTask().execute();
				
			}
		});
		
			
		 this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
	}
	 
	  // get the selected dropdown list value
	  public void addListenerOnButton() {
	 
		btnSubmit1 = (Button) findViewById(R.id.btnSubmit1);
	 
		btnSubmit1.setOnClickListener(new OnClickListener() {
	 
			  @Override
			  public void onClick(View v) {
				  
				  Globals = ((MyGlobals) getApplicationContext());
				  Globals.setValue("action", "warehouse");
				  Globals.setValue("warehouse", selectWS );
				  Globals.setValue("ws_url", selectURL );
				  Globals.setValue("warehouse_url", selectURL );
				  Globals.setValue("warehouse_type", selectType );
				  Globals.setValue("warehouse_art_url", selectArtURL );

				  myUser = Globals.getValue("user");
				  myPass = Globals.getValue("pass");

				  MyProvider = new WSDataProvider(myUser, myPass);
				  MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));

				  myIP = Globals.getValue("ip");
				  myAppName = Globals.getValue("module");
				  myDevice = Globals.getValue("device");

				  MyProvider.SetDeviceIdentification(myIP, myDevice, myAppName);

				  mAuthTask = new UserLoginTask();
				  mAuthTask.execute((Void) null);


			  }
		 
			});
		
		}
				
	  
	  @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
		
		public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle item selection
	        switch (item.getItemId()) {
	        case R.id.action_settings:
	            Intent intent = new Intent(this, SettingsActivity.class); startActivity(intent); 
	            return true;
			case R.id.action_message:
					Intent intent2 = new Intent(this, MessageActivity.class); startActivity(intent2);
					return true;
			case R.id.action_status:
					Intent intent3 = new Intent(this, RobotStatusActivity.class); startActivity(intent3);
					return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }

		/**
		 * Represents an asynchronous login/registration task used to authenticate
		 * the user.
		 */
		public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
			@Override
			protected Boolean doInBackground(Void... params) {

				boolean suc = false;

				if (Globals.getValue("warehouse_type").equals("robot")) {

					MyRobotProvider = new DataProvider();
					MyRobotProvider.SetHost(Globals.getValue("ws_url"), "8002");

					String myPassword = MyRobotProvider.MD5_Hash(Globals.getValue("pass"));

					suc = MyRobotProvider.AuthenticateUser(Globals.getValue("user"), myPassword);

				} else {
					suc = MyProvider.CheckLogin(Globals.getValue("user"), Globals.getValue("pass"), "DigiOrder");
				}
				if (suc)
				{

				}
				return suc;
			}

			@Override
			protected void onPostExecute(final Boolean success) {
				mAuthTask = null;

				if (success) {

					Intent intent = new Intent(SelectWarehouseActivity.this, SelectActionActivity.class);
					startActivity(intent);

				} else {

					if (MyProvider.json_connected == false)
					{
						String data_error = getString(R.string.error_network);
						Toast.makeText(getApplication(), data_error, Toast.LENGTH_LONG).show();

					} else {
						Toast.makeText(getApplication(), getString(R.string.error_incorrect_password), Toast.LENGTH_LONG).show();
					}
				}
			}

			@Override
			protected void onCancelled() {
				mAuthTask = null;

			}
		}
		

		/**
		 * Background Async Task to Load all webservices by making HTTP Request
	  	 * */
		class LoadWSList extends AsyncTask<String, String, String> {

	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	 
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
		   	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
			
			Globals = ((MyGlobals)getApplication());
			
			MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
			wsList = new ArrayList<HashMap<String, String>>();
				
			// convert objects to hashmap for list
			List<cWebService> List = MyProvider.getWebServices();
		
			for(cWebService entry : List)
	     	{
				// creating new HashMap
	            HashMap<String, String> map = new HashMap<String, String>();

	            map.put("pid", entry.id);
	            map.put("name", entry.name);
	            map.put("url", entry.url);
	            map.put("type", "webshop");
				map.put("art_url", entry.art_url);

	            // adding HashList to ArrayList
	            wsList.add(map);
	        }
	    	
			return "";
	   }

		   /**
		    * After completing background task Dismiss the progress dialog
		    * **/
		   protected void onPostExecute(String file_url) {
		       // dismiss the dialog after getting all products
		
		   		// updating UI from Background Thread
		       runOnUiThread(new Runnable() {
		           public void run() {
		           
		    	    List<String> userArray =  new ArrayList<String>();
		           	for (HashMap<String, String> map : wsList)
		          	        for (Entry<String, String> entry : map.entrySet())
		          	        {
		          	        	if (entry.getKey() == "pid") wskeyArray.add(entry.getValue());
		          	        	if (entry.getKey() == "name") wsArray.add(entry.getValue());
		          	        	if (entry.getKey() == "url") wsURLArray.add(entry.getValue());
								if (entry.getKey() == "art_url") wsArtURLArray.add(entry.getValue());
		          	        	if (entry.getKey() == "type") wsTypeArray.add(entry.getValue());
		          	        	
		          	        }
		           	
		           		ArrayAdapter<String> ws_adapter = new ArrayAdapter<String>(SelectWarehouseActivity.this, android.R.layout.simple_spinner_item, wsArray);
		           		ws_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		      	    	Spinner sws = (Spinner) findViewById(R.id.spinnerWS);
		      	    	sws.setAdapter(ws_adapter);
	      	       
		      	    	wsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		          			    @Override
		          			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
		          			    {
		          			    	
	          			    		int myIndex = wsSpinner.getSelectedItemPosition();
	          			    		selectWS = wsArray.get(myIndex);
	          			    		selectURL = wsURLArray.get(myIndex);
	          			    		selectType = wsTypeArray.get(myIndex);
	          			    		selectArtURL = wsArtURLArray.get(myIndex);
		          			    }

		          			    @Override
		          			    public void onNothingSelected(AdapterView<?> parentView) {
		          			    	selectWS = wsArray.get(0);
	          			    		selectURL = wsURLArray.get(0);
	          			    		selectType = wsTypeArray.get(0);
									selectArtURL = wsArtURLArray.get(0);
		          			    }

		          			});
		      	    	
		           }
		       	});
	 	      
	    	}
		}
		
		
		public class FindServicesTask extends AsyncTask<Void, Void, Boolean> {

		   @Override
		     protected void onPreExecute() {
		         super.onPreExecute();
	
		         pDialog = new ProgressDialog(SelectWarehouseActivity.this);
		         pDialog.setTitle(getString(R.string.loading_warehouses));
		         pDialog.setMessage(getString(R.string.please_wait));
		         pDialog.setIndeterminate(false);
		         pDialog.setCancelable(true);
		         pDialog.show();
		        
		        
		     }

			   
			@Override
		    protected Boolean doInBackground(Void... params) {
		      
				numTry++;
				boolean blerror = false;
				
				WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
				MulticastLock mLock = wifiManager.createMulticastLock("lock");
				mLock.acquire();
				
				// try udp broadcast, to find engine services
				// send
				try{
					 
					DatagramSocket s = new DatagramSocket();
					s.setBroadcast(true);
					s.setSoTimeout(5000);
					    		
					String messageStr="Hello, android";
					int server_port = 11000; //port that Im using
			   
					String addr = myIP.substring(0,myIP.lastIndexOf(".")) + ".255"; 
					 
				    InetAddress local = InetAddress.getByName(addr);//my broadcast ip
				    int msg_length=messageStr.length();
				    byte[] message = messageStr.getBytes();
				    DatagramPacket p = new DatagramPacket(message, msg_length,local,server_port);
				    s.send(p);
				    Log.d("digiorder udp cast","message send");
			   
				    while (!blerror && numTry < 2)
				    {
				    
					    //Wait for a response
					    byte[] recvBuf = new byte[15000];
						   
					    DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
					    
					    s.receive(receivePacket);
				   
					    //We have a response
				    
					    System.out.println(getClass().getName() + ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());
					    
					    String reply_message = new String(receivePacket.getData()).trim();
					    System.out.println(getClass().getName() + ">>> Broadcast data from server: " + reply_message);
					    
					    StringTokenizer st = new StringTokenizer(reply_message, "|");
				        String name = st.nextToken();
				        String url = st.nextToken(); 
					    
					    cWebService new_ws = new cWebService();
					    new_ws.name = name;
					    new_ws.description = name;
					    new_ws.url = url;
					    
					    // check if already exist
					    boolean blfound = false;
					    for(cWebService d : ws_list){
					        if(d.url.contains(new_ws.url))
					        {
					        	blfound = true;
					        }
					    }
					    
					    if (!blfound)
					    {
					    	ws_list.add(new_ws);
					    	wsArray.add(new_ws.name);
		          	        wsURLArray.add(new_ws.url);
		          	        wsTypeArray.add("robot");
							wsArtURLArray.add("");
					    	
					    }
				    }
				    //Close the port!
				    s.close();
				    
				    mLock.release();
			
				}catch(Exception e){
				   Log.d("digiorder udp cast","error  " + e.toString());
				   
				   blerror = true;
				   
				   mLock.release();
				}		    
			 	
				return true;
		    }

		    protected void onPostExecute(final Boolean success) {
		      
		  	  // Dismiss the progress dialog
	            if (pDialog != null)
	                pDialog.dismiss();

		    	if (numTry < 3 && ws_list.size() == 0)
		    	{
		    		// again
		    		new FindServicesTask().execute();
		    	} else {
		    		
		    	//	new LoadWSList().execute();	
		    		
		    	}
		    }
		}
		
	}


