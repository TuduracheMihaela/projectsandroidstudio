package com.sharethatdata.digiorder.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.webservice.WSDataProvider;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class GridViewAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	protected ListView mListView;
 	public MyImageLoader loadMyImg;
 	
 	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	private String myUser = "";
	private String myPass = "";
	private String error_message = "";
	private String holidayID = "";

 	 public GridViewAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;
	        
	        Globals = ((MyGlobals)context.getApplicationContext());
	        myUser = Globals.getValue("user");
	        myPass = Globals.getValue("pass");
	        MyProvider = new WSDataProvider(myUser, myPass);
	        
	        loadMyImg = new MyImageLoader(context.getApplicationContext());
	    } 

 	 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.grid_item_id);
        holder.titleTextView = (TextView)view.findViewById(R.id.grid_item_title);
        holder.imageView = (ImageView)view.findViewById(R.id.grid_item_image);        
        
        holder.titleTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { 
            	
            	HashMap<String, String> itemsList = (HashMap<String, String>) adapter.get(position);
    			String itemIdStatus = (String) itemsList.get("id");
    			holidayID = itemIdStatus;

            }
        });
        
        view.setTag(holder);
	}else{
	
	    holder=(ViewHolder)view.getTag();
	}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());
    
     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.titleTextView.setText(Html.fromHtml(hashmap_Current.get("name")));   
     
     //loadMyImg.DisplayImage(hashmap_Current.get("image_string"), holder.imageView); // image from database
     
     Bitmap d = ConvertByteArrayToBitmap(hashmap_Current.get("image_string"));
	  if (d != null)
	  {
		  int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
		  Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
		  holder.imageView.setImageBitmap(scaled);
		  				  
	  }else{
		  holder.imageView.setImageResource(R.drawable.ic_launcher); 
	  }

    return view; 
}



	final class ViewHolder {
		TextView id;
		TextView titleTextView;
        ImageView imageView;
	}
	
	 private Bitmap ConvertByteArrayToBitmap(String b)
		{
			byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			
			return decodedByte;		
		}

}
