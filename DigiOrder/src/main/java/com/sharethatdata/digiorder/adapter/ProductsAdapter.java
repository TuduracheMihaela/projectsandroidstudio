package com.sharethatdata.digiorder.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.ResourceDetailActivity;
import com.sharethatdata.digiorder.orders.MyOrderListActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ProductsAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	private MyGlobals Globals;
	WSDataProvider MyProvider = null;
	cResource resource;

 	 public ProductsAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textViewID);
                    
        holder.location = (TextView)view.findViewById(R.id.textViewLocation);
        holder.product = (TextView)view.findViewById(R.id.textViewProduct);
        holder.amount = (TextView)view.findViewById(R.id.textViewAmount);
       
        view.setTag(holder);
        
        Button button = (Button) view.findViewById(R.id.button_detail);
        
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do things Here 
            
             	Globals = ((MyGlobals) context.getApplicationContext());	
    		                 	
            	String scanned = holder.location.getText().toString();
            	String ResID = holder.product.getText().toString();
            	            	
	    	 	Globals.setValue("lastLocationSelected", scanned);
	    	  	Globals.setValue("lastResourceSelected", ResID);
	    	  	Globals.setValue("lastScanned", scanned);
        	    
    		    Intent intent = new Intent(v.getContext(), ResourceDetailActivity.class); 
			    context.startActivity(intent); 
            	
            }
        });
        
	}else{
	
	    holder=(ViewHolder)view.getTag();
	}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     
     holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));       
     holder.product.setText(Html.fromHtml(hashmap_Current.get("product")));
     holder.amount.setText(Html.fromHtml(hashmap_Current.get("amount")));
    
    return view; 
}

	final class ViewHolder {
	    public TextView id, location, product, amount;  
	}


}
