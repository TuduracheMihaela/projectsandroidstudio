package com.sharethatdata.digiorder.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.sharethatdata.digiorder.R;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LocationsAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutLocationId;

 	 public LocationsAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutLocationId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutLocationId, null);
        holder=new ViewHolder();
        holder.location = (TextView)view.findViewById(R.id.textViewLocation);
        holder.name = (TextView)view.findViewById(R.id.textViewName);
        holder.stock = (TextView)view.findViewById(R.id.textViewStock);
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

  //   holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));       
     holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
     holder.stock.setText(Html.fromHtml(hashmap_Current.get("stock")));
     
    return view; 
}

	final class ViewHolder {
	    public TextView location, name, stock;  
	}


}
