package com.sharethatdata.digiorder.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sharethatdata.digiorder.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mverbeek on 9-2-2017.
 */

public class ImagesAdapter extends ArrayAdapter<HashMap<String, String>> {
    private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
    private final Activity context;
    private final int layoutImageId;

    public ImagesAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
    {
        super(context, layoutResourceId, adapter);
        this.adapter=adapter;
        this.context=context;
        this.layoutImageId=layoutResourceId;

    }

    private Bitmap ConvertByteArrayToBitmap(String b)
    {
        Bitmap decodedByte = null;
        if(b.isEmpty() || b.contentEquals(""))
        {
            decodedByte = null;
        }else{
            byte[] decodedString = Base64.decode(b, Base64.NO_WRAP);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        }

        return decodedByte;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HashMap<String, String>hashmap_Current;
        View view=null;
        convertView = null;
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            view=layoutInflater.inflate(layoutImageId, null);
            holder=new ViewHolder();
            holder.image = (ImageView) view.findViewById(R.id.image);
            holder.datetime = (TextView)view.findViewById(R.id.textViewDateTime);
            holder.user = (TextView)view.findViewById(R.id.textViewUserName);

            view.setTag(holder);
        }else{

            holder=(ViewHolder)view.getTag();
        }

        hashmap_Current=new HashMap<String, String>();
        hashmap_Current=adapter.get(position);

        Log.e("Zdit", hashmap_Current.toString());

        //   holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
       // holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));
        Bitmap myBitmap = ConvertByteArrayToBitmap(hashmap_Current.get("image"));
        if (myBitmap != null) {
            holder.image.setVisibility(View.VISIBLE);
            //  imageViewNote.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, NewW, NewH, true));
            holder.image.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, 400, 400, true));
        }

        holder.datetime.setText(Html.fromHtml(hashmap_Current.get("datetime")));
        holder.user.setText(Html.fromHtml(hashmap_Current.get("user")));

        return view;
    }

    final class ViewHolder {
        public ImageView image;
        public TextView datetime;
        public TextView user;
    }


}
