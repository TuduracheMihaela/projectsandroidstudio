package com.sharethatdata.digiorder.adapter;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mverbeek on 2-2-2017.
 */

    public class EditLocationsAdapter extends ArrayAdapter<HashMap<String, String>> {
        private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
        private final Activity context;
        private final int layoutLocationId;
        private MyGlobals Globals;
        WSDataProvider MyProvider = null;
        cLocation location;
        private DeleteButtonAdapterCallback callback;

        public EditLocationsAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
        {
            super(context, layoutResourceId, adapter);
            this.adapter=adapter;
            this.context=context;
            this.layoutLocationId=layoutResourceId;

        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            HashMap<String, String>hashmap_Current;
            View view=null;
            convertView = null;
            final ViewHolder holder;

            if (convertView == null)
            {
                LayoutInflater layoutInflater=context.getLayoutInflater();
                view=layoutInflater.inflate(layoutLocationId, null);
                holder=new ViewHolder();
        //        holder.id = (TextView)view.findViewById(R.id.textViewID);
                holder.location = (TextView)view.findViewById(R.id.textViewLocation);
                holder.name = (TextView)view.findViewById(R.id.textViewName);
                holder.stock = (TextView)view.findViewById(R.id.textViewStock);

                view.setTag(holder);

                Button b = (Button) view.findViewById(R.id.button_delete);
                b.setTag(position);
                b.setOnClickListener(new View.OnClickListener() {
                     public void onClick(View v) {

                         if(callback != null) {

                            callback.deletePressed(position);
                         }
                     }
                });

            }else{

                holder=(ViewHolder)view.getTag();
            }

            hashmap_Current=new HashMap<String, String>();
            hashmap_Current=adapter.get(position);

            Log.e("Zdit", hashmap_Current.toString());

            //   holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
           // holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));
            holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));
            holder.name.setText(Html.fromHtml(hashmap_Current.get("name")));
            holder.stock.setText(Html.fromHtml(hashmap_Current.get("stock")));

            return view;
        }

        public void setCallback(DeleteButtonAdapterCallback callback){

            this.callback = callback;
        }


        public interface DeleteButtonAdapterCallback {

            public void deletePressed(int position);
        }

        final class ViewHolder {
            public TextView location, name, stock;
        }


    }
