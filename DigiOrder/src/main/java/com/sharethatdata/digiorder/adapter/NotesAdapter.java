package com.sharethatdata.digiorder.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.digiorder.ResourceDetailActivity;
import com.sharethatdata.digiorder.orders.MyOrderListActivity;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutResourceId;
 	private MyGlobals Globals;
	WSDataProvider MyProvider = null;
	cResource resource;

 	 public NotesAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutResourceId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutResourceId, null);
        holder=new ViewHolder();
        holder.id = (TextView)view.findViewById(R.id.textViewID);
        holder.resID = (TextView)view.findViewById(R.id.textViewResID);
        holder.groupID = (TextView)view.findViewById(R.id.textViewGroupID);
                
        holder.title = (TextView)view.findViewById(R.id.textViewTitle);
        holder.owner = (TextView)view.findViewById(R.id.textViewOwner);
        holder.entry = (TextView)view.findViewById(R.id.textViewEntry);
    //    holder.picked = (TextView)view.findViewById(R.id.textViewPicked);
        
        view.setTag(holder);
        
        Button button = (Button) view.findViewById(R.id.button_detail);
        
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do things Here 
            
             	Globals = ((MyGlobals) context.getApplicationContext());	
    		                 	
            	String scanned = holder.title.getText().toString();
            	String ResID = holder.resID.getText().toString();
            	String GroupID = holder.groupID.getText().toString();
            	            	
	    	 	Globals.setValue("lastLocationSelected", scanned);
	    	  	Globals.setValue("lastResourceSelected", ResID);
	    	  	Globals.setValue("lastResourceGroupSelected", GroupID);
        	  	Globals.setValue("lastScanned", scanned);
        	    
    		    Intent intent = new Intent(v.getContext(), ResourceDetailActivity.class); 
			    context.startActivity(intent); 
            	
            }
        });
        
	}else{
	
	    holder=(ViewHolder)view.getTag();
	}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

     holder.id.setText(Html.fromHtml(hashmap_Current.get("id")));  
     holder.resID.setText(Html.fromHtml(hashmap_Current.get("resid")));  
     holder.groupID.setText(Html.fromHtml(hashmap_Current.get("groupid")));  
     
     holder.title.setText(Html.fromHtml(hashmap_Current.get("title")));       
     holder.owner.setText(Html.fromHtml(hashmap_Current.get("owner")));
     holder.entry.setText(Html.fromHtml(hashmap_Current.get("entry")));
    
    return view; 
}

	final class ViewHolder {
	    public TextView id, resID, groupID, title, owner, entry;  
	}


}
