package com.sharethatdata.digiorder.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.sharethatdata.digiorder.R;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MutationsAdapter extends ArrayAdapter<HashMap<String, String>> { 
	private ArrayList<HashMap<String, String>> adapter=new ArrayList<HashMap<String,String>>();
 	private final Activity context;
 	private final int layoutMutationId;

 	 public MutationsAdapter(Activity context,int layoutResourceId,ArrayList<HashMap<String, String>> adapter)
	    {
	        super(context, layoutResourceId, adapter);
	        this.adapter=adapter;
	        this.context=context;
	        this.layoutMutationId=layoutResourceId;

	    } 


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
	HashMap<String, String>hashmap_Current;
    View view=null;
    convertView = null;
    final ViewHolder holder;
    
    if (convertView == null)
    {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        view=layoutInflater.inflate(layoutMutationId, null);
        holder=new ViewHolder();
        holder.datetime = (TextView)view.findViewById(R.id.textViewDateTime);
        holder.location = (TextView)view.findViewById(R.id.textViewLocation);
        holder.product =  (TextView)view.findViewById(R.id.textViewProduct);
        holder.user = (TextView)view.findViewById(R.id.textViewName);
        holder.amount = (TextView)view.findViewById(R.id.textViewAmount);
        
        view.setTag(holder);
}else{

    holder=(ViewHolder)view.getTag();
}
    
    hashmap_Current=new HashMap<String, String>();
    hashmap_Current=adapter.get(position);

    Log.e("Zdit", hashmap_Current.toString());

    holder.datetime.setText(Html.fromHtml(hashmap_Current.get("datetime")));  
	holder.location.setText(Html.fromHtml(hashmap_Current.get("location")));       
	holder.product.setText(Html.fromHtml(hashmap_Current.get("product")));       
	holder.user.setText(Html.fromHtml(hashmap_Current.get("user")));
	holder.amount.setText(Html.fromHtml(hashmap_Current.get("amount")));
 
    return view; 
}

	final class ViewHolder {
	    public TextView datetime, user, product, location, amount;  
	}


}
