package com.sharethatdata.digiorder;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.Socket;
import java.io.DataOutputStream;


public class MessageActivity extends Activity {

    private Button btnSubmit1;
    private String message_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        btnSubmit1 = (Button) findViewById(R.id.buttonSend);

        btnSubmit1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

              final EditText text_message = (EditText) findViewById(R.id.textMessage);
              message_text = text_message.getText().toString();

              new SendMessageTask().execute();

            }

        });

    }

    public class SendMessageTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            SendMessage();

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

        }

    }

    public void SendMessage()
    {
        try {
            Socket client = new Socket("192.168.0.36", 12345);

            DataOutputStream DOS = new DataOutputStream(client.getOutputStream());
            DOS.writeUTF(message_text);
            client.close();

        } catch (Exception ex) {
             Toast.makeText(MessageActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}


