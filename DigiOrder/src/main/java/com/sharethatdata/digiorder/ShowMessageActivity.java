package com.sharethatdata.digiorder;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ShowMessageActivity extends Activity {

    private String message_text;
    public TextView text_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_message);

        Bundle bundle = getIntent().getExtras();

        if(bundle.getString("message")!= null) {

            text_message = (TextView) findViewById(R.id.showMessage);
            message_text = bundle.getString("message");
            if (text_message != null) text_message.setText(message_text);
        }

    }
}
