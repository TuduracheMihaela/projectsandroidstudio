package com.sharethatdata.digiorder;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.robotservice.datamodel.robotInfo;

public class RobotStatusActivity extends Activity {


    DataProvider MyRobotProvider = null;

    MyGlobals Globals;
    robotInfo myInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot_status);

        Globals = ((MyGlobals)getApplication());

        MyRobotProvider = new DataProvider();
        MyRobotProvider.SetHost(Globals.getValue("ws_url"), "8002");

        new LoadStatusTask().execute();

    }



    private class LoadStatusTask extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args)
        {
            Log.e("digiorder_logs","loading robot info");

            myInfo = MyRobotProvider.getRobotInfo();

            return "";
        }
        protected void onPostExecute(String file_url)
        {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    TextView robotname = (TextView) findViewById(R.id.textRobotName);
                    robotname.setText(myInfo.Name);

                    TextView robotdescription = (TextView) findViewById(R.id.textRobotDescription);
                    robotdescription.setText(myInfo.Description);

                    TextView robotstatus = (TextView) findViewById(R.id.textRobotStatus);
                    robotstatus.setText("Status : " + myInfo.status);

                    TextView ordertype = (TextView) findViewById(R.id.textOrderType);
                    ordertype.setText(myInfo.orderType);

                    TextView ordernr = (TextView) findViewById(R.id.textViewOrderNr);
                    ordernr.setText(myInfo.order);

                    TextView barcode = (TextView) findViewById(R.id.textViewBarcode);
                    barcode.setText(myInfo.barcode);

                    TextView product = (TextView) findViewById(R.id.textViewProduct);
                    product.setText(myInfo.product);

                    TextView location = (TextView) findViewById(R.id.textViewLocation);
                    location.setText(myInfo.location);

                    TextView amount = (TextView) findViewById(R.id.textViewAmount);
                    amount.setText(String.valueOf(myInfo.amount));

                    TextView amount_done = (TextView) findViewById(R.id.textViewAmountDone);
                    amount_done.setText(String.valueOf(myInfo.amount_done));

                    TextView error = (TextView) findViewById(R.id.textViewError);
                    error.setText(myInfo.error);

                }
            });
        }
    }
}
