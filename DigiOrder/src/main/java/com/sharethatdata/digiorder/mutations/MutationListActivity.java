package com.sharethatdata.digiorder.mutations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.MyGlobals;
import com.sharethatdata.digiorder.TimeUtils;
import com.sharethatdata.digiorder.orders.MyOrdersDetailActivity;
import com.sharethatdata.digiorder.R;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cMutation;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.robotservice.DataProvider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MutationListActivity extends Activity{
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;
	
	TimeUtils MyTimeUtils = null;
	
	private static ProgressDialog pDialog;
	
	private String myUser = "";
	private String myPass = "";
	
	ArrayList<HashMap<String, String>> mutationlist;
	ArrayList<HashMap<String, String>> userList; // getContacts
	 
	private String report_user_id = "";
	private CheckBox chkAllOrders = null;
	
	ListView lv = null;
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_list_mutations);

		lv = (ListView) findViewById(R.id.listViewMutations);
		
		Globals = ((MyGlobals)getApplication());
		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");

		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(MutationListActivity.this, MutationListActivity.this);

		new LoadList().execute();
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id)
			{
				if (mutationlist.size() > 0)
				{
					HashMap<String, String> itemsList = (HashMap<String, String>) mutationlist.get(position);
					 String itemId = (String) itemsList.get("id");
					 String itemName = (String) itemsList.get("uname");
					 String itemStartDate = (String) itemsList.get("datum");
					 String itemOrderNr = (String) itemsList.get("ordernr");
					 String itemBedrag = (String) itemsList.get("bedrag");
					 String itemProcessed = (String) itemsList.get("processed");
					 String itemPayed = (String) itemsList.get("payed");
					 
				//	 Intent intent = new Intent(MutationListActivity.this, MutationDetailActivity.class);
				//	 intent.putExtra("edit_action", "edit_order");
					 
				//	 intent.putExtra("ID", itemId);
				
				//	 startActivity(intent);
				}
			}

		});	
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String update = Globals.getValue("order_update");
	    if(update != ""){
	    	Globals.setValue("order_update", "");
	    	new LoadList().execute();
	    }
		Globals.setValue("edit_action","");
	}
	
	/**
	 * Background Async Task to Load all mutations by making HTTP Request
  	 * */
	class LoadList extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         
         pDialog = new ProgressDialog(MutationListActivity.this);
         pDialog.setTitle("Loading mutations");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals)getApplication());
	    	
	    	String ws_type = Globals.getValue("warehouse_type");
	    	
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("warehouse_url"));
		   		  		
		    // load data from provider
		     mutationlist = new ArrayList<HashMap<String, String>>();  
		     List<cMutation> mutations = new ArrayList<cMutation>();
		     
		    if (ws_type.equals("robot"))
		    {
		    	MyRobotProvider = new DataProvider();
		    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
		    	    	
	    	 	mutations = MyRobotProvider.getMutations("");
		     
		    } else {
		    	mutations = MyProvider.getMutations("");
			    
		    }
		     
	    	 for(cMutation entry : mutations)
	    	 {
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
   
                map.put("id",  entry.id);
                map.put("name", entry.user);
                map.put("location", entry.location);
                map.put("datum", entry.datetime);
                map.put("product", entry.resource_code + " " + entry.resource);
                
                String reset_str = "";
                if (entry.mutation_type.equals("reset")) reset_str = " reset";
                
                map.put("amount", String.valueOf(entry.amount) + reset_str);
               
                // adding HashList to ArrayList
                mutationlist.add(map);
				
	    	 }

	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	  // Dismiss the progress dialog
            if (pDialog != null && pDialog.isShowing())
                pDialog.dismiss();

	    	
	    	// updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            public void run() {
	            
	         	   /**
	                 * Updating parsed JSON data into ListView
	                 * */
	            	ListAdapter adapter = null;

	            	adapter = new SimpleAdapter(
	                		MutationListActivity.this
	                		, mutationlist
	                		, R.layout.listview_item_mutations, new String[] { "datum" , "name", "product", "location", "amount"},
                            new int[] { R.id.textViewDateTime , R.id.textViewName, R.id.textViewProduct, R.id.textViewLocation, R.id.textViewAmount });
	        	            
                
	                // updating listview
	                lv.setAdapter(adapter);
        			
	              
	            }
	        });
	  	      
	        
	        if (mutationlist.size() == 0) Toast.makeText(MutationListActivity.this, "Error, loading mutations failed!", Toast.LENGTH_LONG).show();
	        
	    }
	  

	}
		
}
