package com.sharethatdata.digiorder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.sharethatdata.digiorder.adapter.NotesAdapter;
import com.sharethatdata.digiorder.adapter.ProductsAdapter;
import com.sharethatdata.robotservice.DataProvider;
import com.sharethatdata.webservice.WSDataProvider;
import com.sharethatdata.webservice.datamodel.cLocation;
import com.sharethatdata.webservice.datamodel.cOrder;
import com.sharethatdata.webservice.datamodel.cOrderProduct;
import com.sharethatdata.webservice.datamodel.cResource;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StockCheckActivity extends Activity {
	
	MyGlobals Globals;
	WSDataProvider MyProvider = null;
	DataProvider MyRobotProvider = null;
	
	private String ws_type = "";
	private String myUser = "";
	private String myPass = "";

	TimeUtils MyTimeUtils = null;

	cOrder order = null;
	
	ArrayList<HashMap<String, String>> productList;
	
	private static ProgressDialog pDialog;
	private static AlertDialog alertDialog;
	
	private String id_order = "";
  
	TextView textViewBarcode;
    TextView textViewName;
    TextView textViewCode;
    TextView textViewRequest;
    TextView textViewDelivery;
    TextView textViewDeadline;
    TextView textViewDate;
    TextView textViewStarted;
    TextView textViewEnded;
    TextView textViewOrderType;
    TextView textViewOrderStatus;
    TextView textViewStatus;
    TextView textViewProcessed;
    TextView textViewPriority;
    TextView textViewPayMethod;
    TextView textViewError;
    TextView textViewProcesTime;
    TextView textViewWaitingTime;
    EditText TrayBCText;
    EditText LineBCText;
    TextView textViewTray;
    
    
    private String lastResID="0";
	private String lastLocID="0";
	
    String myBarcode = "";
    String myTrayBarcode = "";
    String myScannedBarcode = "";
	String myProduct = "";
	String myProductNr = "";
	String myAmount = "";
	String orderline_id = "";
	boolean blUseCamera = false;
    
    ListView lv = null;
    
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		
		Globals = ((MyGlobals)getApplication());
		
		String ws_type = Globals.getValue("warehouse_type");
		
		if (ws_type.equals("robot"))
	    {
			setContentView(R.layout.robotorder_view_detail);
			
			textViewBarcode = (TextView) findViewById(R.id.textViewBarcode);
			textViewName = (TextView) findViewById(R.id.textViewName);
			textViewCode = (TextView) findViewById(R.id.textViewCode);
			textViewRequest = (TextView) findViewById(R.id.textViewRequest);
			textViewDelivery = (TextView) findViewById(R.id.textViewDelivery);
			
			textViewDate = (TextView) findViewById(R.id.textViewDate);
			textViewStarted = (TextView) findViewById(R.id.textViewStarted);
			textViewEnded = (TextView) findViewById(R.id.textViewEnded);
			
			textViewOrderType = (TextView) findViewById(R.id.textViewOrderType);
			textViewOrderStatus = (TextView) findViewById(R.id.textViewOrderStatus);
			textViewProcessed = (TextView) findViewById(R.id.textViewProcessed);
			textViewPriority = (TextView) findViewById(R.id.textViewPriority);
			textViewError = (TextView) findViewById(R.id.textViewError);
			
			textViewProcesTime = (TextView) findViewById(R.id.textViewProcesTime);
			textViewWaitingTime = (TextView) findViewById(R.id.textViewWaitingTime);
			
	    
	    } else {
	    	setContentView(R.layout.grid_view_detail_check);
	    	
	    	textViewName = (TextView) findViewById(R.id.textViewName);
			textViewCode = (TextView) findViewById(R.id.textViewCode);
			
			// disable when no tray is scanned
			LinearLayout layout = (LinearLayout) findViewById(R.id.projectLayoutNoteName);
			layout.setVisibility(View.GONE);
			layout = (LinearLayout) findViewById(R.id.linearLayout7);
			layout.setVisibility(View.GONE);
	    }
		
		TrayBCText = (EditText) findViewById(R.id.textScanTrayBarcode);
		LineBCText = (EditText) findViewById(R.id.textScanBarcode);


		MyTimeUtils = new TimeUtils();
		MyTimeUtils.setOrientation(StockCheckActivity.this, StockCheckActivity.this);

		myUser = Globals.getValue("user");
		myPass = Globals.getValue("pass");
		SharedPreferences sharedPrefs = getSharedPreferences("defaults", MODE_PRIVATE );
		boolean blCameraScanner = sharedPrefs.getBoolean("prefCameraScanner", false);
		
		if (blCameraScanner)
		{
			blUseCamera = true;
		} 
		
		lv = (ListView) findViewById(R.id.listViewNotes);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    String valueID = extras.getString("ID");    
		    	id_order = valueID; 
		}
		
	
		
		new LoadOrderItem().execute();
	
		if (id_order.equals(""))
		{
			// create new check order
			new CreateOrder().execute();
			
			
		}
		
		new LoadListProducts().execute();
		
		if (!ws_type.equals("robot"))
	    {
		
			lv.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					HashMap<String, String> itemsList = (HashMap<String, String>) productList.get(position);
					 String itemId = (String) itemsList.get("title");
					
				//	EditText EditText = (EditText) findViewById(R.id.textScanBarcode);
					LineBCText.setText(itemId);
					 
					// StartPickingAction();
				}
			});
		
			Button btnEdit = (Button) findViewById(R.id.buttonAddNote);
			if (blUseCamera) btnEdit.setText("Scan");
			
			btnEdit.setOnClickListener(new OnClickListener() {
				 
				  @Override
				  public void onClick(View v) 
				  {				 
					  // if scan with camera
					  if (blUseCamera)
					  {
						  StartScanBarcode();
							  
					  }
					  
					  //StartPickingAction();
				  }
			 
			});
		
				// clear input bc field
			
				LineBCText.setText("");
	         
				LineBCText.addTextChangedListener(new TextWatcher() {
		
		          public void afterTextChanged(Editable s) {
		
		        	  StartCountAction();
		        	  
		          }
		
		          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		
		          public void onTextChanged(CharSequence s, int start, int before, int count) {}
		    });
				
	    } else {
	    		    	
	    	Button btnEdit = (Button) findViewById(R.id.buttonStart);
			btnEdit.setOnClickListener(new OnClickListener() {
				 
				  @Override
				  public void onClick(View v) 
				  {				 
					  // start order
					  // ...
				  }
			 
			});
			
	    }
		
		// hide keyboard
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		 
		
		
	}
	
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			  
	      IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
		  if (scanResult != null) {
			
			  String UPCScanned = scanResult.getContents();
			  if (UPCScanned != null)
			  {
			 	 // set product barcode
				 Log.d("barcode scanned", UPCScanned);
	              		            
				 myScannedBarcode = UPCScanned;
				 LineBCText = (EditText) findViewById(R.id.textScanBarcode);
				 LineBCText.setText(UPCScanned);
			  
			  }	
              
           }
	  }
	 
	
  
	private void StartScanBarcode()
	{
		 // create barcode intent
		  IntentIntegrator integrator = new IntentIntegrator(this);
		  integrator.initiateScan();
	}
	
	private void StartCountAction()
	{
		// get barcode from textview
	   if (!ws_type.equals("robot"))
	    {
		   String bc = myScannedBarcode;
		   try
		   {
			   EditText EditText = (EditText) findViewById(R.id.textScanBarcode);
			   bc = EditText.getText().toString();
		   } catch (Exception ex)
		   {			  
					   
		   }
			
		   if (bc.length() > 5)
		   {
		//		bc = "1103D02";
				myBarcode = bc;
				myProduct = "";
				
				// lookup location bc and product
				new LoadOrderLine().execute();
				
		   }
	    }
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	 
	    String updateOrder = Globals.getValue("order_update");
	    if(updateOrder != ""){
	    	Globals.setValue("order_update", "");
	    	new LoadOrderItem().execute();
	    }
	    Globals.setValue("edit","");
	    
	    // Note
	    String updateProduct = Globals.getValue("product_update");
	    if(updateProduct != ""){
	    	Globals.setValue("product_update", "");
	    	new LoadListProducts().execute();
	    }
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Globals.setValue("order_update", "order_update");
	    	finish();
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

	 /**
	 * Background Async Task to Load Order Item by making HTTP Request
  	 * */
	class LoadOrderItem extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();
      // Showing progress dialog
         pDialog = new ProgressDialog(StockCheckActivity.this);
         pDialog.setTitle("Loading order");
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(true);
         pDialog.show();
     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
	    	
	    	ws_type = Globals.getValue("warehouse_type");
		 	  
	    	myUser = Globals.getValue("user");
	 		myPass = Globals.getValue("pass");
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
		    order = new cOrder();
		    		    
		    if (ws_type.equals("robot"))
		    {
		    	MyRobotProvider = new DataProvider();
		    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
		    
	    	
		    	order = MyRobotProvider.getRobotOrder(myUser, id_order);
		     
		    } else {
		    	order = MyProvider.getOrder(id_order);
			    
		    }
		
	    	runOnUiThread(new Runnable() {
	               @Override
	               public void run() {
	            	   
	        		   textViewName.setText("Stock count");
	            	   
	            	   if (ws_type.equals("robot"))
	            	   {
	            		   textViewBarcode.setText(Html.fromHtml(order.barcode));
	            		   textViewName.setText(Html.fromHtml(order.uname));
		            	   textViewCode.setText(Html.fromHtml(order.ordernr));
		            	   textViewRequest.setText(Html.fromHtml(order.req_location));
		            	   textViewDelivery.setText(Html.fromHtml(order.del_location));
		            	   
		            	   textViewDate.setText(Html.fromHtml(order.datum));
		            	   textViewStarted.setText(Html.fromHtml(order.started));
		            	   textViewEnded.setText(Html.fromHtml(order.ended));
		            	   
		            	   textViewOrderType.setText(Html.fromHtml(order.order_type)); 
		            	   textViewOrderStatus.setText(Html.fromHtml(order.status)); 
		            	   textViewPriority.setText(Html.fromHtml(order.priority)); 
		            	   
		            	   textViewError.setText(Html.fromHtml(order.error)); 
		            	   
		            	   textViewProcesTime.setText(Html.fromHtml(order.procestime) + " seconds"); 
		            	   textViewWaitingTime.setText(Html.fromHtml(order.waitingtime) + " seconds"); 
		            	   
		            	   if(order.processed.contains("1")){
		            		   textViewProcessed.setText(Html.fromHtml("Yes"));
		            	   }else {
		            		   textViewProcessed.setText(Html.fromHtml("No"));
		            	   }
	            	   } else {
			   			    
		            	   textViewName.setText(Html.fromHtml(order.uname));
		            	   textViewCode.setText(Html.fromHtml(order.ordernr));
		           		            
			            	// show barcode scan view
			   	   	    	LinearLayout layout = (LinearLayout) findViewById(R.id.projectLayoutNoteName);
			   	   		    layout.setVisibility(View.VISIBLE);
			   	   		    layout = (LinearLayout) findViewById(R.id.linearLayout7);
			   	   		    layout.setVisibility(View.VISIBLE);
			   	   		    layout = (LinearLayout) findViewById(R.id.trayBarcodeLayout);
			   	   		    layout.setVisibility(View.GONE);
			   	   		    layout = (LinearLayout) findViewById(R.id.linearLayout6);
			   	   		    layout.setVisibility(View.GONE);
		            	  
			            }
			        }
	            });
	    	 
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String result) {
	        // dismiss the dialog after getting all products
	
	    	super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        
	  	      
	    }
	}
	
	

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 /**
	 * Background Async Task to Load products by making HTTP Request
  	 * */
	class LoadListProducts extends AsyncTask<String, String, String> {

     /**
      * Before starting background thread Show Progress Dialog
      * */
     @Override
     protected void onPreExecute() {
         super.onPreExecute();

     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    	
	    	Globals = ((MyGlobals) getApplicationContext());	
	    	
	     	String ws_type = Globals.getValue("warehouse_type");
	 	   		    	
	    	myUser = Globals.getValue("user");
	    	myPass = Globals.getValue("pass");
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
			
		    List<cOrderProduct> products = new ArrayList<cOrderProduct>();
		     
		    if (ws_type.equals("robot"))
		    {
		    	MyRobotProvider = new DataProvider();
		    	MyRobotProvider.SetHost(Globals.getValue("warehouse_url"), "8002");
		    
	    	
		    	products = MyRobotProvider.getRobotOrderProductList(id_order);
		     
		    } else {
		    	products = MyProvider.getCheckProducts(id_order);
			    
		    }
		    
	    	 // load data from provider
			productList = new ArrayList<HashMap<String, String>>();  
	    	
			for(cOrderProduct entry : products)
	    	{
    			// creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                
                map.put("id",  entry.id);
                map.put("location", entry.location);
                map.put("amount", entry.amount);
                map.put("product", entry.product);
                	                 
                // adding HashList to ArrayList
                productList.add(map);
	    	 }
	    	
	    	 return "";
	    }
	
	    /**
	     * After completing background task Dismiss the progress dialog
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	
	    	// updating UI from Background Thread
	    	runOnUiThread(new Runnable() {
	            public void run() {
	            
     	   /**
             * Updating parsed JSON data into ListView
             * */
        	ProductsAdapter adapter = null;

   			adapter = new ProductsAdapter(StockCheckActivity.this, R.layout.grid_view_detail_items_product, productList);
   			lv.setAdapter(adapter);
   			setListViewHeightBasedOnChildren(lv);
   	
	        			
   		      	 
	            }
	        });    
	    }  
	}
	
   public void setListViewHeightBasedOnChildren(ListView listView) {
	    ListAdapter listAdapter = listView.getAdapter();
	    if (listAdapter == null) {
	        // pre-condition
	        return;
	    }

	    int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.EXACTLY);
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        View listItem = listAdapter.getView(i, null, listView);

	        if(listItem != null){
	            // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
	            listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	            listItem.requestLayout();
	            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	            totalHeight += listItem.getMeasuredHeight();

	        }
	        Log.w("HEIGHT" + i, String.valueOf(totalHeight));
	    }

	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}

   /**
	 * Background Async Task 
  	 * */
	class LoadOrderLine extends AsyncTask<String, String, String> {

		  AlertDialog alertDialog;
		   
	     /**
	      * Before starting background thread Show Progress Dialog
	      * */
	     @Override
	     protected void onPreExecute() {
	         super.onPreExecute();
	         alertDialog = new AlertDialog.Builder(StockCheckActivity.this).create();
	     }

	    /**
	     * getting items from url
	     * */
	    protected String doInBackground(String... args) {
	   	    
	    	// lookup order line
       	
	    	Globals = ((MyGlobals) getApplicationContext());	
	    	myUser = Globals.getValue("user");
	    	myPass = Globals.getValue("pass");
	    	MyProvider = new WSDataProvider(myUser, myPass);
		    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
		    myProduct = "";
		    
		    // get product on location?
		    List<cResource> myResources = MyProvider.getResourcesbyLocationBarcode(myBarcode);
		    cResource res = null;
			if (myResources.size() > 0)
        	{
        		res = myResources.get(0);
        	 	Globals.setValue("lastLocationSelected", myBarcode);
        		
        	} else {
     		  
        		myResources = MyProvider.getResourcesbyBarcode(myBarcode);
        		if (myResources.size() > 0)
        		{
        			res = myResources.get(0);
        		}
        	}
        	
		    
			if (res != null)
			{
	    		orderline_id = "0";
	    		myProduct = res.name;
	    		myProductNr = res.code;
	    		myAmount = res.stock;
	    		lastResID = res.id;
    	    }
    	    		    			
	    	 return "";
	    }
		
	
	    protected void onPostExecute(String file_url) {
	       
	    		if (!myProduct.equals(""))
	    		{

	    			LayoutInflater li = LayoutInflater.from(StockCheckActivity.this);
	    			View promptsView = li.inflate(R.layout.prompt_count_amount, null);

	    			AlertDialog.Builder builder = new AlertDialog.Builder(StockCheckActivity.this);
	    	
	    		    builder.setTitle("Confirm count");
	    		    builder.setMessage("Count this product: " + myProduct);
	    		    
	    		    builder.setView(promptsView);
	    		    
	    		    final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
	    		    userInput.setText(String.valueOf(myAmount));
	    		    
	    		    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	    	
	    		        public void onClick(DialogInterface dialog, int which) {
	    		            // Do nothing but close the dialog
	    	
    		        	  try {
    						  	myAmount = userInput.getText().toString();
    						  
    						    // update stock
		    		            new RegisterStockCountTask().execute();
		    		            
    						    // update orderline
		    		            new RegisterOrderLine().execute();
		    		            		    		        

		    		            Toast.makeText(StockCheckActivity.this, "The stock on location is updated.", Toast.LENGTH_LONG).show();    
    				        
    							
    		        	  } catch (Exception ex) {
    		        		  

		    		            Toast.makeText(StockCheckActivity.this, "The stock on location failed to update.", Toast.LENGTH_LONG).show();      
    		        	  }

	    		            dialog.dismiss();
	    		              
	    		        	new LoadListProducts().execute();
	    		    		
	    		        }
	    		    });
	    	
	    		    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	    	
	    		        @Override
	    		        public void onClick(DialogInterface dialog, int which) {
	    	
	    		            // Do nothing
	    		            dialog.dismiss();
	    		            
	    		        }
	    		    });
	    	
	    		    alertDialog = builder.create();
	    		    alertDialog.show();
    		    
	    		} else {
	    			  Toast.makeText(StockCheckActivity.this, "No product in this order on scanned location!", Toast.LENGTH_LONG).show();
    		             
	    		}
               	 
            }
	       
	}
	
	 private class CreateOrder extends AsyncTask<String, String, Boolean>
	 {	 
			@Override
	        protected Boolean doInBackground(String... args) 
	        {
				Globals = ((MyGlobals) getApplicationContext());	
		    	
		     	String ws_type = Globals.getValue("warehouse_type");
		 	   		    	
		    	myUser = Globals.getValue("user");
		    	myPass = Globals.getValue("pass");
		    	MyProvider = new WSDataProvider(myUser, myPass);
			    MyProvider.SetWebServiceUrl(Globals.getValue("ws_url"));
								
			    // generate unique order id
			    Date dNow = new Date();
		        SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmssMs");
		        id_order = ft.format(dNow);
			    
				Log.d("digiorder_logs", "Creating check order with id = " + id_order);
				  
			    return MyProvider.createOrder(myUser, id_order, "check");
				
			}
				   
			protected void onPostExecute(Boolean result) 
			{
				textViewName.setText("Stock Check");
				textViewCode.setText(id_order);
				
			}
     }  
	
    private class RegisterStockCountTask extends AsyncTask<String, String, Boolean>
    {
    	
    	 
		@Override
        protected Boolean doInBackground(String... args) 
        {
			 // find location id
			 lastLocID = "0";
			 List<cLocation> locations = MyProvider.getLocations(lastResID);
			 for(cLocation entry : locations)
	    	 {
				 if (entry.code.equals(myBarcode))
				 {
					 lastLocID = String.valueOf(entry.id);
	    	 	 }
	    	 }
			 
			 if (!lastLocID.equals("0"))
			 {
			
				 Log.d("digiorder_logs","resetting stock on locationid = " + lastLocID + " , resourceid = " + lastResID);
			  
				 return MyProvider.updateStock(lastResID, lastLocID, Integer.parseInt(myAmount), true);
			 } else {
				 return false;
			 }
		}
			   
		protected void onPostExecute(Boolean result) 
		{
			
		}
    }
    
    private class RegisterOrderLine extends AsyncTask<String, String, Boolean>
    {
    	 
		@Override
        protected Boolean doInBackground(String... args) 
        {
			if (!orderline_id.equals("0"))
			{				
				 Log.d("digiorder_logs","updating order line  = " + orderline_id);
			  
				 return MyProvider.updateOrderProduct(orderline_id, myAmount, myProduct);
				 
			 } else {
			
				 // create new order line
				 
				 Log.d("digiorder_logs","creating order line for  = " + myProduct + ", " + myAmount);
				  
				 return MyProvider.createOrderProduct(id_order, myProduct, myProductNr, lastLocID, myAmount);
				 			
			 }
			
			
			
		}
			   
		protected void onPostExecute(Boolean result) 
		{
			
		}
    }
    
	
}
